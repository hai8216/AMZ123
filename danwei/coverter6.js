function checkNum(str) {
	for (var i=0; i<str.length; i++) {
		var ch = str.substring(i, i + 1)
		if (ch!="." && ch!="+" && ch!="-" && ch!="e" && ch!="E" && (ch < "0" || ch > "9")) {
			alert("请输入有效的数字");
			return false;
		}
	}
	return true
}
function computeTempC(obj) {
	var tempC = parseFloat(obj.tempCelsius.value)
	if( (tempC >= 0) && (obj.tempCelsius.value.indexOf("-") != -1) ) {
		tempC = -tempC;
	}
	if(!(tempC < -273.15) ) {
		var tempK = tempC + 273.15
		var tempF = 32 + (tempC * 9 / 5)
		var tempRa = tempK*1.8
		var tempRe = tempC/1.25
		obj.tempKelvin.value = tempK
		obj.tempFahr.value = tempF
		obj.tempRankine.value = tempRa
		obj.tempReaumur.value = tempRe
	} else {
		obj.tempKelvin.value = "ERROR"
		obj.tempFahr.value = "ERROR"
		obj.tempRankine.value = "ERROR"
		obj.tempReaumur.value = "ERROR"
	}
}
function computeTempF(obj) {
	var tempF = parseFloat(obj.tempFahr.value)
	if( (tempF >= 0) && (obj.tempFahr.value.indexOf("-") != -1) ) tempF = -tempF;
	 if(!(tempF < -459.666666) ) {
		var tempC = (tempF - 32) * 5 / 9
		var tempK = tempC + 273.15
		var tempRa = tempK*1.8
		var tempRe = tempC/1.25
		obj.tempCelsius.value = tempC
		obj.tempKelvin.value = tempK
		obj.tempRankine.value = tempRa
		obj.tempReaumur.value = tempRe
	} else  {
		obj.tempCelsius.value = "ERROR"
		obj.tempKelvin.value = "ERROR"
		obj.tempRankine.value = "ERROR"
		obj.tempReaumur.value = "ERROR"
	}
}
function computeTempK(obj) {
	var tempK = parseFloat(obj.tempKelvin.value)
	if( (tempK >= 0) && (obj.tempKelvin.value.indexOf("-") != -1) ) {
		tempK = -tempK;
	}
	if(!(tempK < 0) ) {
		var tempC = tempK - 273.15
		var tempF = 32 + (tempC * 9 / 5)
		var tempRa = tempK*1.8
		var tempRe = tempC/1.25
		obj.tempCelsius.value = tempC
		obj.tempFahr.value = tempF
		obj.tempRankine.value = tempRa
		obj.tempReaumur.value = tempRe
	} else {
		obj.tempCelsius.value = "ERROR"
		obj.tempFahr.value = "ERROR"
		obj.tempRankine.value = "ERROR"
		obj.tempReaumur.value = "ERROR"
	}
}
function computeTempRa(obj) {
	var tempRa = parseFloat(obj.tempRankine.value)
	if( (tempRa >= 0) && (obj.tempRankine.value.indexOf("-") != -1) ) {
		tempRa = -tempRa;
	}
	if(!(tempRa < 0) ) {
		var tempK = tempRa/1.8
		var tempC = tempK  - 273.15
		var tempF = 32 + (tempC * 9 / 5)
		var tempRe = tempC/1.25
		obj.tempCelsius.value = tempC
		obj.tempFahr.value = tempF
		obj.tempKelvin.value = tempK
		obj.tempReaumur.value = tempRe
	} else {
		obj.tempCelsius.value = "ERROR"
		obj.tempFahr.value = "ERROR"
		obj.tempKelvin.value = "ERROR"
		obj.tempReaumur.value = "ERROR"
	}
}
function computeTempRe(obj) {
	var tempRe = parseFloat(obj.tempReaumur.value)
	if( (tempRe >= 0) && (obj.tempReaumur.value.indexOf("-") != -1) ) {
		tempRe = -tempRe;
	}
	if(!(tempRe < -218.5199999999) ) {
		var tempC = tempRe*1.25
		var tempK = tempC + 273.15
		var tempF = 32 + (tempC * 9 / 5)
		var tempRa = tempK*1.8
		obj.tempCelsius.value = tempC
		obj.tempFahr.value = tempF
		obj.tempKelvin.value = tempK
		obj.tempRankine.value = tempRa
	} else {
		obj.tempCelsius.value = "ERROR"
		obj.tempFahr.value = "ERROR"
		obj.tempKelvin.value = "ERROR"
		obj.tempRankine.value = "ERROR"
	}
}
function resetAll(form) {
	form.tempCelsius.value = ""
	form.tempKelvin.value = ""
	form.tempFahr.value = ""
	form.tempRankine.value = ""
	form.tempReaumur.value = ""
}
