<?php


function glob_recursive($pattern, $flags = 0) {
        $files = glob($pattern, $flags);
        foreach(glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
                 $files = array_merge($files, glob_recursive($dir.'/'.basename($pattern), $flags));
        }
        return $files;
}


// 
    function file_ext($filename, $max = 16) {
        $ext = strtolower(substr(strrchr($filename, '.'), 1));
        $ext = xn_urlencode($ext);
        strlen($ext) > $max AND $ext = substr($ext, 0, $max);
        if(!preg_match('#^\w+$#', $ext)) $ext = 'attach';
        return $ext;
}

function xn_urlencode($s) {
    $s = urlencode($s);
    $s = str_replace('_', '_5f', $s);
    $s = str_replace('-', '_2d', $s);
    $s = str_replace('.', '_2e', $s);
    $s = str_replace('+', '_2b', $s);
    $s = str_replace('=', '_3d', $s);
    $s = str_replace('%', '_', $s);
    return $s; 
}

$dir = empty($argv[1]) ? './*' : $argv[1] ;
$files = glob_recursive("$dir");
foreach ($files as $file) {
        $ext = file_ext($file);
        if(strpos($file, $_SERVER['PHP_SELF']) !== FALSE) continue;
        if($ext != 'htm' && $ext != 'php' && $ext != 'js') continue;
        $content = file_get_contents($file);
        $s = $content;

        
        $s = str_replace('ml-0', 'ml-0', $s);
        $s = str_replace('mr-0', 'mr-0', $s);
        $s = str_replace('mt-0', 'mt-0', $s);
        $s = str_replace('mb-0', 'mb-0', $s);
        $s = str_replace('mx-0', 'mx-0', $s);
        $s = str_replace('my-0', 'my-0', $s);

        //$s = str_replace('m-xs', 'm-1', $s);
        $s = str_replace('ml-1', 'ml-1', $s);
        $s = str_replace('mr-1', 'mr-1', $s);
        $s = str_replace('mt-1', 'mt-1', $s);
        $s = str_replace('mb-1', 'mb-1', $s);
        $s = str_replace('mx-1', 'mx-1', $s);
        $s = str_replace('my-1', 'my-1', $s);

        //$s = str_replace('m-sm', 'm-2', $s);
        $s = str_replace('ml-2', 'ml-2', $s);
        $s = str_replace('mr-2', 'mr-2', $s);
        $s = str_replace('mt-2', 'mt-2', $s);
        $s = str_replace('mb-2', 'mb-2', $s);
        $s = str_replace('mx-2', 'mx-2', $s);
        $s = str_replace('mx-2', 'my-2', $s);

       // $s = str_replace('m-1', 'm-3', $s);
        $s = str_replace('ml-3', 'ml-3', $s);
        $s = str_replace('mr-3', 'mr-3', $s);
        $s = str_replace('mt-3', 'mt-3', $s);
        $s = str_replace('mb-3', 'mb-3', $s);
        $s = str_replace('mx-3', 'mx-3', $s);
        $s = str_replace('my-3', 'my-3', $s);


        //$s = str_replace('p-xs', 'p-1', $s);
        $s = str_replace('pl-1', 'pl-1', $s);
        $s = str_replace('pr-1', 'pr-1', $s);
        $s = str_replace('pt-1', 'pt-1', $s);
        $s = str_replace('pb-1', 'pb-1', $s);
        $s = str_replace('px-1', 'px-1', $s);
        $s = str_replace('py-1', 'py-1', $s);

        //$s = str_replace('p-sm', 'p-2', $s);
        $s = str_replace('pl-2', 'pl-2', $s);
        $s = str_replace('pr-2', 'pr-2', $s);
        $s = str_replace('pt-2', 'pt-2', $s);
        $s = str_replace('pb-2', 'pb-2', $s);
        $s = str_replace('px-2', 'px-2', $s);
        $s = str_replace('px-2', 'py-2', $s);

       // $s = str_replace('p-1', 'p-3', $s);
        $s = str_replace('pl-3', 'pl-3', $s);
        $s = str_replace('pr-3', 'pr-3', $s);
        $s = str_replace('pt-3', 'pt-3', $s);
        $s = str_replace('pb-3', 'pb-3', $s);
        $s = str_replace('px-3', 'px-3', $s);
        $s = str_replace('py-3', 'py-3', $s);

        $s = str_replace('pl-0', 'pl-0', $s);
        $s = str_replace('pr-0', 'pr-0', $s);
        $s = str_replace('pt-0', 'pt-0', $s);
        $s = str_replace('pb-0', 'pb-0', $s);
        $s = str_replace('px-0', 'px-0', $s);
        $s = str_replace('py-0', 'py-0', $s);

        $s = str_replace('col-', 'col-', $s);

        $s = str_replace('mx-auto', 'mx-auto', $s);
        $s = str_replace('card-body', 'card-body', $s);
        $s = str_replace('font-weight-bold', 'font-weight-bold', $s);
        $s = str_replace('float-left', 'float-left', $s);
        $s = str_replace('float-right', 'float-right', $s);

        $s = str_replace('hidden-md hidden-lg', 'hidden-md hidden-lg', $s);
        $s = str_replace('hidden-sm hidden-md', 'hidden-sm hidden-md', $s);

       // $s = str_replace('d-block d-md-none', 'hidden-md hidden-lg', $s);
       // $s = str_replace('d-none d-md-block', 'hidden-sm hidden-md', $s);
        

        //$s = str_replace('.tab("show")', '.addClass("active")', $s);
        //$s = str_replace(".tab('show')", ".addClass('active')", $s);

        /*
        $s = str_replace("logo-xs", "logo-1", $s);
        $s = str_replace("logo-sm", "logo-2", $s);
        $s = str_replace("logo-lg", "logo-3", $s);
        $s = str_replace("avatar-xs", "avatar-1", $s);
        $s = str_replace("avatar-sm", "avatar-2", $s);
        $s = str_replace("avatar-lg", "avatar-3", $s);
        */

        if($s != $content) {
              echo $file."\r\n";
              file_put_contents($file, $s);
        }
}