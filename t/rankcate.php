<?php

$cateUrl = [
    'https://www.amazon.com/Best-Sellers-Home-Kitchen/zgbs/home-garden/',
    'https://www.amazon.com/Best-Sellers-Industrial-Scientific/zgbs/industrial/',
    'https://www.amazon.com/Best-Sellers-Kitchen-Dining/zgbs/kitchen/',
    'https://www.amazon.com/Best-Sellers-Musical-Instruments/zgbs/musical-instruments/',
    'https://www.amazon.com/Best-Sellers-Office-Products/zgbs/office-products/',
    'https://www.amazon.com/Best-Sellers-Patio-Lawn-Garden/zgbs/lawn-garden/',
    'https://www.amazon.com/Best-Sellers-Pet-Supplies/zgbs/pet-supplies/',
    'https://www.amazon.com/Best-Sellers-Sports-Outdoors/zgbs/sporting-goods/',
    'https://www.amazon.com/Best-Sellers-Tools-Home-Improvement/zgbs/hi/',
    'https://www.amazon.com/Best-Sellers-Toys-Games/zgbs/toys-and-games/',
    'https://www.amazon.com/Best-Sellers-Automotive/zgbs/automotive/',
    'https://www.amazon.com/Best-Sellers-Baby/zgbs/baby-products/',
    'https://www.amazon.com/Best-Sellers-Beauty-Personal-Care/zgbs/beauty/',
    'https://www.amazon.com/Best-Sellers-Camera-Photo-Products/zgbs/photo/',
    'https://www.amazon.com/Best-Sellers-Cell-Phones-Accessories/zgbs/wireless/',
    'https://www.amazon.com/Best-Sellers-Clothing-Shoes-Jewelry/zgbs/fashion/',
    'https://www.amazon.com/Best-Sellers-Computers-Accessories/zgbs/pc/',
    'https://www.amazon.com/Best-Sellers-Electronics/zgbs/electronics/',
    'https://www.amazon.com/Best-Sellers-Health-Household/zgbs/hpc/',
    'https://www.amazon.com/Best-Sellers-Appliances/zgbs/appliances/',
    'https://www.amazon.com/Best-Sellers-Arts-Crafts-Sewing/zgbs/arts-crafts/',
];

echo json_encode($cateUrl);