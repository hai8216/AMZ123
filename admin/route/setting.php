<?php

!defined('DEBUG') and exit('Access Denied.');

$action = param(1);

include _include(APP_PATH . 'model/smtp.func.php');
$smtplist = smtp_init(APP_PATH . 'conf/smtp.conf.php');
// hook admin_setting_start.php

if ($action == 'base') {

    // hook admin_setting_base_get_post.php

    if ($method == 'GET') {

        // hook admin_setting_base_get_start.php

        $input = array();
        $input['sitename'] = form_text('sitename', $conf['sitename']);
        $input['topusa'] = form_text('topusa', $conf['topusa']);
        $input['topjp'] = form_text('topjp', $conf['topjp']);
        $input['topuk'] = form_text('topuk', $conf['topuk']);
        $input['topde'] = form_text('topde', $conf['topde']);
        $input['topfr'] = form_text('topfr', $conf['topfr']);
        $input['usatopkeywords'] = form_text('usatopkeywords', $conf['usatopkeywords']);
        $input['special_bg'] = form_text('special_bg', $conf['special_bg']);
        $input['special_links'] = form_text('special_links', $conf['special_links']);
        $input['sitebrief'] = form_textarea('sitebrief', $conf['sitebrief'], '100%', 100);
        $input['hideqrcode'] = form_textarea('hideqrcode', $conf['hideqrcode'], '100%', 100);
        $input['hidemore'] = form_textarea('hidemore', $conf['hidemore'], '100%', 100);
        $input['runlevel'] = form_radio('runlevel', array(0 => lang('runlevel_0'), 1 => lang('runlevel_1'), 2 => lang('runlevel_2'), 3 => lang('runlevel_3'), 4 => lang('runlevel_4'), 5 => lang('runlevel_5')), $conf['runlevel']);
        $input['user_create_on'] = form_radio_yes_no('user_create_on', $conf['user_create_on']);
        $input['user_create_email_on'] = form_radio_yes_no('user_create_email_on', $conf['user_create_email_on']);
        $input['user_resetpw_on'] = form_radio_yes_no('user_resetpw_on', $conf['user_resetpw_on']);
        $input['lang'] = form_select('lang', array('zh-cn' => lang('lang_zh_cn'), 'zh-tw' => lang('lang_zh_tw'), 'en-us' => lang('lang_en_us'), 'ru-ru' => lang('lang_ru_ru'), 'th-th' => lang('lang_th_th')), $conf['lang']);

        $header['title'] = lang('admin_site_setting');
        $header['mobile_title'] = lang('admin_site_setting');

        // hook admin_setting_base_get_end.php

        include _include(ADMIN_PATH . 'view/htm/setting_base.htm');

    } else {

        $sitebrief = param('sitebrief', '', FALSE);
        $special_bg = param('special_bg', '', FALSE);
        $special_links = param('special_links', '', FALSE);
        $sitename = param('sitename', '', FALSE);
        $hideqrcode = param('hideqrcode', '', FALSE);
        $runlevel = param('runlevel', 0);
        $hidemore = param('hidemore', '', FALSE);
        $user_create_on = param('user_create_on', 0);
        $user_create_email_on = param('user_create_email_on', 0);
        $user_resetpw_on = param('user_resetpw_on', 0);

        $_lang = param('lang');

        // hook admin_setting_base_post_start.php

        $replace = array();
        $replace['sitename'] = $sitename;
        $replace['special_bg'] = $special_bg;
        $replace['special_links'] = $special_links;
        $replace['hideqrcode'] = $hideqrcode;
        $replace['sitebrief'] = $sitebrief;
        $replace['runlevel'] = $runlevel;
        $replace['hidemore'] = $hidemore;
        $replace['user_create_on'] = $user_create_on;
        $replace['user_create_email_on'] = $user_create_email_on;
        $replace['user_resetpw_on'] = $user_resetpw_on;
        $replace['lang'] = $_lang;
        $replace['topjp'] = param('topjp');
        $replace['topuk'] = param('topuk');
        $replace['topusa'] = param('topusa');
        $replace['topfr'] = param('topfr');
        $replace['topde'] = param('topde');
        $replace['usatopkeywords'] = param('usatopkeywords');

        file_replace_var(APP_PATH . 'conf/conf.php', $replace);

        // hook admin_setting_base_post_end.php
        kv_set('hideqrcode', $hideqrcode);
        kv_set('hidemore', $hidemore);
        message(0, lang('modify_successfully'));
    }

} elseif ($action == 'smtp') {

    // hook admin_setting_smtp_get_post.php

    if ($method == 'GET') {

        // hook admin_setting_smtp_get_start.php

        $header['title'] = lang('admin_setting_smtp');
        $header['mobile_title'] = lang('admin_setting_smtp');

        $smtplist = smtp_find();
        $maxid = smtp_maxid();

        // hook admin_setting_smtp_get_end.php

        include _include(ADMIN_PATH . "view/htm/setting_smtp.htm");

    } else {

        // hook admin_setting_smtp_post_start.php

        $email = param('email', array(''));
        $host = param('host', array(''));
        $port = param('port', array(0));
        $user = param('user', array(''));
        $pass = param('pass', array(''));

        $smtplist = array();
        foreach ($email as $k => $v) {
            $smtplist[$k] = array(
                'email' => $email[$k],
                'host' => $host[$k],
                'port' => $port[$k],
                'user' => $user[$k],
                'pass' => $pass[$k],
            );
        }
        $r = file_put_contents_try(APP_PATH . 'conf/smtp.conf.php', "<?php\r\nreturn " . var_export($smtplist, true) . ";\r\n?>");
        !$r and message(-1, lang('conf/smtp.conf.php', array('file' => 'conf/smtp.conf.php')));

        // hook admin_setting_smtp_post_end.php

        message(0, lang('save_successfully'));
    }
} elseif ($action == 'index_ads') {
    if ($method == 'GET') {
        $kvget = kv_get('index_ads');
        $input['title'] = form_text('title', $kvget['title']);
        $input['desc'] = form_textarea('desc', $kvget['desc']);
        $input['newid'] = form_radio('newid', $kvget['newid']);
        include _include(ADMIN_PATH . "view/htm/setting_index_ads.htm");
    } else {
        $ind['title'] = param('title', '', false);
        $ind['desc'] = param('desc', '', false);
        if (param('newid')) {
            $idsqid = uniqid();
            cache_set("index_ads_qid", $idsqid);
        }
        kv_set('index_ads', $ind);
        message(0, jump('success', url('setting-index_ads')));
    }
} elseif ($action == 'banip') {
    if ($method == 'GET') {
        $kvget = kv_get('banid');
        $input['banip'] = form_textarea('banip', $kvget['banip'], "100%", 400);
        include _include(ADMIN_PATH . "view/htm/setting_banip.htm");
    } else {
        $ind['banip'] = param('banip');
        kv_set('banid', $ind);
        message(0, jump('success', url('setting-banid')));
    }
} elseif ($action == 'indexpage') {
    if ($method == 'GET') {
        $data = db_find("setting_indexpage",[],[],1,200);
        foreach ($data as $v) {
            $value[$v['key']] = $v['value'];
        }
        $input['about'] = form_textarea('about', $value['about'], "100%", 200);
        $input['tab1'] = form_text('tab1', $value['tab1']);
        $input['tab2'] = form_text('tab2', $value['tab2']);
        $input['tab1_icon'] = form_text('tab1_icon', $value['tab1_icon']);
        $input['tab2_icon'] = form_text('tab2_icon', $value['tab2_icon']);
        $input['tab2_icon_2'] = form_text('tab2_icon_2', $value['tab2_icon_2']);
        $input['tab1_icon_2'] = form_text('tab1_icon_2', $value['tab1_icon_2']);
        $input['qrcode_title'] = form_text('qrcode_title', $value['qrcode_title']);
        $input['qrcode_url'] = form_text('qrcode_url', $value['qrcode_url']);
        $input['qrcode_bottom'] = form_text('qrcode_bottom', $value['qrcode_bottom']);
        $input['morelinks'] = form_textarea('morelinks', $value['morelinks'], "100%", 200);
        $input['morelinks2'] = form_textarea('morelinks2', $value['morelinks2'], "100%", 200);
        include _include(ADMIN_PATH . 'view/htm/setting_indexpage.htm');
    }else{
       if(param('about')){
//           db_delete("setting_indexpage",["key"=>"about"]);
           db_replace("setting_indexpage",["key"=>"about","value"=>param('about')]);
       }
        if(param('tab1')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"tab1","value"=>param('tab1')]);
        }
        if(param('tab2')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"tab2","value"=>param('tab2')]);
        }
        if(param('tab1_icon')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"tab1_icon","value"=>param('tab1_icon')]);
        }
        if(param('tab2_icon')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"tab2_icon","value"=>param('tab2_icon')]);
        }



        if(param('qrcode_title')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"qrcode_title","value"=>param('qrcode_title')]);
        }
        if(param('qrcode_url')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"qrcode_url","value"=>param('qrcode_url')]);
        }
        if(param('qrcode_bottom')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"qrcode_bottom","value"=>param('qrcode_bottom')]);
        }
        if(param('morelinks')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"morelinks","value"=>param('morelinks')]);
        }
        if(param('morelinks2')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"morelinks2","value"=>param('morelinks2')]);
        }
        if(param('tab2_icon_2')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"tab2_icon_2","value"=>param('tab2_icon_2')]);
        }
        if(param('tab1_icon_2')){
//           db_delete("setting_indexpage",["key"=>"about"]);
            db_replace("setting_indexpage",["key"=>"tab1_icon_2","value"=>param('tab1_icon_2')]);
        }

        $insertdata = db_find("setting_indexpage",[],[],1,200);
        foreach ($insertdata as $v) {
            $value[$v['key']] = $v['value'];
        }
        cache_set('footerCache', $value, 1800);
        message(0, lang('save_successfully'));
    }
}elseif ($action == 'logo') {
    if ($method == 'GET') {
        $map['key'] = ["logo","search"];
        $data = db_find("setting_indexpage",$map,[],1,200);
        foreach ($data as $v) {
            $value[$v['key']] = $v['value'];
        }
        $input['logo'] = form_text('logo', $value['logo']);
        $input['search'] = form_textarea('search', $value['search'], "100%", 200);
        include _include(ADMIN_PATH . 'view/htm/setting_logo.htm');
    }else{
        if(param('logo')){
            db_replace("setting_indexpage",["key"=>"logo","value"=>param('logo')]);
        }
        if(param('search')){
            db_replace("setting_indexpage",["key"=>"search","value"=>param('search')]);
        }
        $map['key'] = ["logo","search"];
        $insertdata = db_find("setting_indexpage",$map,[],1,200);
        foreach ($insertdata as $v) {
            $value[$v['key']] = $v['value'];
        }
        cache_set('logoSearch_setting', $value, 1800);
        message(0, lang('save_successfully'));
    }
}

// hook admin_setting_end.php

?>
