<?php

!defined('DEBUG') and exit('Access Denied.');

$action = param(1, 'list');


if ($action == 'add') {


    if ($method == 'GET') {
        $data = [];
        $input['desc'] = form_text("desc", $data['desc']);
        include _include(ADMIN_PATH . 'view/htm/img_add.htm');
    } else {
        $desc = param('desc');
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $result2)) {
//            print_r($result);exit;
            $type = $result2[2];
            $time = time();
            $wwwpath = "upload/imgs/static/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result2[1], '', param('hidden_img_tpl'))), $wwwpath . $new_file)) {
                $img = $wwwpath . $new_file;
            }
        }
        db_insert("imghouse",
            [
                "imgPath" => $img,
                "time"=>time(),
                "desc"=>$desc
            ]
        );
        message(0, lang('modify_successfully'));
    }

} elseif ($action == 'list') {
    $map = [];
    $page = param(2, 1);
    $pagesize = 40;
    $keyword = trim(xn_urldecode(param('key')));
    $imglist = db_find("imghouse", $map, ["id" => '-1'], $page, $pagesize);

    include _include(ADMIN_PATH . "view/htm/img_list.htm");


}elseif ($action == 'delete') {
    $id = param('id');
    db_delete("imghouse",["id"=>$id]);
    message(0, lang('delete_successfully'));

}


?>
