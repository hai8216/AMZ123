<?php

!defined('DEBUG') and exit('Access Denied.');

$action = param(1);

// hook admin_user_start.php

if (empty($action) || $action == 'list') {

    $header['title'] = lang('user_admin');
    $header['mobile_title'] = lang('user_admin');

    $pagesize = 20;
    $srchtype = param(2);
    $keyword = trim(xn_urldecode(param(3)));
    $page = param(4, 1);

    // hook admin_user_list_start.php

    $cond = array();
    $allowtype = array('uid', 'username', 'email', 'gid', 'create_ip');

    // hook admin_user_list_allow_type_after.php

    if ($keyword) {
        !in_array($srchtype, $allowtype) and $srchtype = 'username';
        $cond[$srchtype] = $srchtype == 'create_ip' ? sprintf('%u', ip2long($keyword)) : $keyword;
        if ($srchtype == 'username') {
            $cond['username'] = array('LIKE' => $keyword);
        }
    }

    // hook admin_user_list_cond_after.php
    $getgid = param('gid');
    if ($getgid) {
        $cond['gid'] = $getgid;
    }
    $n = user_count($cond);
    $userlist = user_find($cond, array('uid' => -1), $page, $pagesize);
    $pagination = pagination(url("user-list-$srchtype-" . urlencode($keyword) . '-{page}'), $n, $page, $pagesize);
    if ($getgid) {
        $pagination = pagination(url("user-list-$srchtype-" . urlencode($keyword) . '-{page}', array('gid' => $getgid)), $n, $page, $pagesize);
    } else {
        $pagination = pagination(url("user-list-$srchtype-" . urlencode($keyword) . '-{page}'), $n, $page, $pagesize);
    }
    $pager = pager(url("user-list-$srchtype-" . urlencode($keyword) . '-{page}'), $n, $page, $pagesize);

    foreach ($userlist as &$_user) {
        $_user['group'] = array_value($grouplist, $_user['gid'], '');
    }

    // hook admin_user_list_end.php

    include _include(ADMIN_PATH . "view/htm/user_list.htm");

} elseif($action=='vipgroup'){
    $header['title'] = lang('user_admin');
    $header['mobile_title'] = lang('user_admin');

    $pagesize = 20;
    $srchtype = param(2);
    $keyword = trim(xn_urldecode(param(3)));
    $page = param(4, 1);

    // hook admin_user_list_start.php

    $cond = array('vip'=>1);
    $allowtype = array('uid', 'username', 'email', 'gid', 'create_ip');

    // hook admin_user_list_allow_type_after.php

    if ($keyword) {
        !in_array($srchtype, $allowtype) and $srchtype = 'username';
        $cond[$srchtype] = $srchtype == 'create_ip' ? sprintf('%u', ip2long($keyword)) : $keyword;
        if ($srchtype == 'username') {
            $cond['username'] = array('LIKE' => $keyword);
        }
    }

    // hook admin_user_list_cond_after.php
    $getgid = param('gid');
    if ($getgid) {
        $cond['gid'] = $getgid;
    }
    $n = user_count($cond);
    $userlist = user_find($cond, array('uid' => -1), $page, $pagesize);
    $pagination = pagination(url("user-vipgroup-$srchtype-" . urlencode($keyword) . '-{page}'), $n, $page, $pagesize);
    if ($getgid) {
        $pagination = pagination(url("user-vipgroup-$srchtype-" . urlencode($keyword) . '-{page}', array('gid' => $getgid)), $n, $page, $pagesize);
    } else {
        $pagination = pagination(url("user-vipgroup-$srchtype-" . urlencode($keyword) . '-{page}'), $n, $page, $pagesize);
    }
    $pager = pager(url("user-list-$srchtype-" . urlencode($keyword) . '-{page}'), $n, $page, $pagesize);

    foreach ($userlist as &$_user) {
        $_user['group'] = array_value($grouplist, $_user['gid'], '');
    }
    include _include(ADMIN_PATH . "view/htm/user_viplist.htm");
}elseif ($action == 'create') {

    // hook admin_user_create_get_post.php

    if ($method == 'GET') {

        // hook admin_user_create_get_start.php

        $header['title'] = lang('admin_user_create');
        $header['mobile_title'] = lang('admin_user_create');

        $input['email'] = form_text('email', '');
        $input['username'] = form_text('username', '');
        $input['password'] = form_password('password', '');
        $grouparr = arrlist_key_values($grouplist, 'gid', 'name');
        $input['_gid'] = form_select('_gid', $grouparr, 0);

        // hook admin_user_create_get_end.php

        include _include(ADMIN_PATH . "view/htm/user_create.htm");

    } elseif ($method == 'POST') {

        $email = param('email');
        $username = param('username');
        $password = param('password');
        $_gid = param('_gid');

        // hook admin_user_create_post_start.php

        empty($email) and message('email', lang('please_input_email'));
        $email and !is_email($email, $err) and message('email', $err);
        $username and !is_username($username, $err) and message('username', $err);

        $_user = user_read_by_email($email);
        $_user and message('email', lang('email_is_in_use'));

        $_user = user_read_by_username($username);
        $_user and message('username', lang('user_already_exists'));

        $salt = xn_rand(16);
        $r = user_create(array(
            'username' => $username,
            'password' => md5($password),
            'salt' => $salt,
            'gid' => $_gid,
            'email' => $email,
            'create_ip' => $longip,
            'create_date' => $time
        ));
        $r === FALSE and message(-1, lang('create_failed'));

        // hook admin_user_create_post_end.php

        message(0, lang('create_successfully'));

    }

} elseif ($action == 'update') {

    $_uid = param(2, 0);

    // hook admin_user_update_get_post.php

    if ($method == 'GET') {

        // hook admin_user_update_get_start.php

        $header['title'] = lang('user_edit');
        $header['mobile_title'] = lang('user_edit');

        $_user = user_read($_uid);

        $input['email'] = form_text('email', $_user['email']);
        $input['username'] = form_text('username', $_user['username']);
        $input['password'] = form_password('password', '');
        $input['keywords'] = form_text('keywords', $_user['keywords']);
        $input['descs'] = form_textarea('descs', $_user['descs']);
        $input['seotitle'] = form_text('seotitle', $_user['seotitle']);
        $input['zl'] = form_radio_yes_no('zl', $_user['zl']);
        $input['vip'] = form_radio_yes_no('vip', $_user['vip']);
        $duser = db_find_one("users_vip",["uid"=>$_uid]);
        $input['endtime'] = form_time('endtime', $duser['endtime']?date('Y-m-d H:i',$duser['endtime']?:""):"");
        $grouparr = arrlist_key_values($grouplist, 'gid', 'name');

        $input['_gid'] = form_select('_gid', $grouparr, $_user['gid']);

        // hook admin_user_update_get_end.php

        include _include(ADMIN_PATH . "view/htm/user_update.htm");

    } elseif ($method == 'POST') {

        $email = param('email');
        $username = param('username');
        $password = param('password');
        $zl = param('zl');
        $vip = param('vip');
        $endtime = param('endtime');
        $_gid = param('_gid');
        $seotitle = param('seotitle');

        // hook admin_user_update_post_start.php
        if($vip && !$endtime){
            message('endtime', '请输入VIP过期时间');
            exit;
        }else{
            $endtime = strtotime($endtime);
        }
        $old = user_read($_uid);
        empty($old) and message('username', lang('uid_not_exists'));

        $email and !is_email($email, $err) and message(2, $err);
        if ($email and $old['email'] != $email) {
            $_user = user_read_by_email($email);
            $_user and $_user['uid'] != $_uid and message('email', lang('email_already_exists'));
        }
        if ($username and $old['username'] != $username) {
            $_user = user_read_by_username($username);
            $_user and $_user['uid'] != $_uid and message('username', lang('user_already_exists'));
        }

        $arr = array();
        $arr['email'] = $email;
        $arr['username'] = $username;
        $arr['gid'] = $_gid;
        $arr['zl'] = $zl;
        $arr['vip'] = $vip;
        $arr['seotitle'] = $seotitle;
        $arr['descs'] = param('descs');
        $arr['keywords'] = param('keywords');
        if ($password) {
            $salt = xn_rand(16);
            $arr['password'] = md5(md5($password) . $salt);
            $arr['salt'] = $salt;
        }

        // hook admin_user_update_post_exec_before.php

        // 仅仅更新发生变化的部分 / only update changed field
        $update = array_diff_value($arr, $old);
        if (!empty($update)) {
            $r = user_update($_uid, $update);
            $r === FALSE and message(-1, lang('update_failed'));
        }

        if($vip){
            db_replace("users_vip",["uid"=>$_uid,"startime"=>time(),"endtime"=>$endtime,"action"=>"admin"]);
        }else{
            db_delete("users_vip",["uid"=>$_uid]);
        }
        // hook admin_user_update_post_end.php

        message(0, lang('update_successfully'));
    }

} elseif ($action == 'delete') {

    if ($method != 'POST') message(-1, 'Method Error.');

    $_uid = param('uid', 0);

    // hook admin_user_delete_start.php

    $_user = user_read($_uid);
    empty($_user) and message(-1, lang('user_not_exists'));
    ($_user['gid'] == 1) and message(-1, 'admin_cant_be_deleted');

    $r = user_delete($_uid);
    $r === FALSE and message(-1, lang('delete_failed'));

    // hook admin_user_delete_end.php

    message(0, lang('delete_successfully'));

} elseif ($action == 'export') {

    if ($method == 'GET') {
        include _include(ADMIN_PATH . "view/htm/user_explor.htm");
    } else {
        $uid = param('uid');
        $username = param('username');
        $email = param('email');
        $mobile = param('mobile');
        $qq = param('qq');
        $credits = param('credits');
        $create_ip = param('create_ip');
        $create_date = param('create_date');
        $csv = "";
        $data = db_find("user");
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="订单数据.csv"');
        header('Cache-Control: max-age=0');
        // 打开PHP文件句柄，php://output 表示直接输出到浏览器
        $fp = fopen('php://output', 'a');
        $head = array('uid');
        fputcsv($fp, $head);
        foreach ($data as $i => $v) {
            $inr[$i] = $v['uid'];
        }
        fputcsv($fp, $inr);
        exit;


    }

} elseif ($action == 'doexport') {
    set_time_limit(0);
    ini_set('memory_limit', '128M');
    $c['uid'] = param('uid');
    $c['username'] = param('username');
    $c['email'] = param('email');
    $c['mobile'] = param('mobile');
    $c['qq'] = param('qq');
    $c['credits'] = param('credits');
    $c['create_ip'] = param('create_ip');
    $uidsize = param('uidsize');
    $size = param('size');
    $c['create_date'] = param('create_date');
    $csv = "";

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="订单数据.csv"');
    header('Cache-Control: max-age=0');
//     打开PHP文件句柄，php://output 表示直接输出到浏览器
    $fp = fopen('php://output', 'a');
    $headary = array(
        array('k' => 'uid','v'=>'uid'),
        array('k' => 'username','v'=>'用户名'),
        array('k' => 'email','v'=>'邮箱'),
        array('k' => 'mobile','v'=>'手机号码'),
        array('k' => 'qq','v'=>'qq'),
        array('k' => 'credits','v'=>'积分'),
        array('k' => 'create_ip','v'=>'注册IP'),
        array('k' => 'create_date','v'=>'注册时间'),
    );
    $head = [];
    foreach($headary as $k=>$v){
        if($c[$v['k']]==='true'){
            $head[]=$v['v'];
            $headK[]=$v['k'];
        }else{
            unset($headary[$k]);
        }
    }
    fputcsv($fp, $head);

    if($size){
        $nums = $size;
    }else{
        $nums = 10000;
    }
    if($nums>10000){
        $nums=10000;
    }
    $map=[];
    if($uidsize){
        list($min,$max) = explode("-",$uidsize);
        $map['uid'] = array('>='=>$min,'<='=>$max);
    }
    $count = db_count("user",$map);
    $step = ceil($count/ $nums);
    if($nums<=$count){
        $step=1;
    }
    for($s = 1; $s <= $step; $s++) {
        $data = db_find("user", $map, ["uid"=>1], $s, $nums);
        foreach ($data as $i => $v) {
            foreach ($headK as $va) {
                if ($va == 'create_ip') {
                    $inr[$va] = long2ip($v[$va]);
                } elseif ($va == 'create_date') {
                    $inr[$va] = date('Y-m-d H:i', $v[$va]);
                } else {
                    $inr[$va] = $v[$va];
                }

            }
            fputcsv($fp, $inr);
        }
        ob_flush();
        flush();
//        fputcsv($fp, $inr);
    }
    exit;


}


?>
