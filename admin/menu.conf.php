<?php

return array(
	'setting' => array(
		'url'=>url('setting-base'),
		'text'=>lang('setting'),
		'icon'=>'icon-cog',
		'tab'=> array (
			'base'=>array('url'=>url('setting-base'), 'text'=>lang('admin_setting_base')),
			'smtp'=>array('url'=>url('setting-smtp'), 'text'=>lang('admin_setting_smtp')),
            'index_ads'=>array('url'=>url('setting-index_ads'), 'text'=>'首页广告'),
            'banip'=>array('url'=>url('setting-banip'), 'text'=>'ip封禁'),
            'indexpage'=>array('url'=>url('setting-logo'), 'text'=>'首页设置'),
		)
	),
	'forum' => array(
		'url'=>url('forum-list'),
		'text'=>lang('forum'),
		'icon'=>'icon-comment',
		'tab'=> array (
		)
	),
//	'thread' => array(
//		'url'=>url('thread-list'),
//		'text'=>lang('thread'),
//		'icon'=>'icon-comment',
//		'tab'=> array (
//			'list'=>array('url'=>url('thread-list'), 'text'=>lang('admin_thread_batch')),
//		)
//	),
	'user' => array(
		'url'=>url('user-list'),
		'text'=>lang('user'),
		'icon'=>'icon-user',
		'tab'=> array (
			'list'=>array('url'=>url('user-list'), 'text'=>lang('admin_user_list')),
			'group'=>array('url'=>url('group-list'), 'text'=>lang('admin_user_group')),
			'create'=>array('url'=>url('user-create'), 'text'=>lang('admin_user_create')),
			'vipgroup'=>array('url'=>url('user-vipgroup'), 'text'=>'会员用户'),
		)
	),
	'other' => array(
		'url'=>url('other'),
		'text'=>lang('other'),
		'icon'=>'icon-wrench',
		'tab'=> array (
			'cache'=>array('url'=>url('other-cache'), 'text'=>lang('admin_other_cache')),
		)
	),
	'img' => array(
		'url'=>url('img'),
		'text'=>'图床',
		'icon'=>'icon-photo',
		'tab'=> array (
			'list'=>array('url'=>url('img-list'), 'text'=>'图床管理'),
			'add'=>array('url'=>url('img-add'), 'text'=>'增加图片'),
		)
	),
	'plugin' => array(
		'url'=>url('plugin'),
		'text'=>lang('plugin'),
		'icon'=>'icon-cogs',
		'tab'=> array (
			'local'=>array('url'=>url('plugin-local'), 'text'=>lang('admin_plugin_local_list')),
			'official_free'=>array('url'=>url('plugin-official_free'), 'text'=>lang('admin_plugin_official_free_list')),
			'official_fee'=>array('url'=>url('plugin-official_fee'), 'text'=>lang('admin_plugin_official_fee_list')),
		)
	)
);

?>
