<?php
!defined('DEBUG') AND define('DEBUG', 2);
define('APP_PATH', dirname(dirname(dirname(__FILE__))) . '/');
!defined('ADMIN_PATH') AND define('ADMIN_PATH', APP_PATH . 'admin/');
!defined('XIUNOPHP_PATH') AND define('XIUNOPHP_PATH', APP_PATH . 'xiunophp/');
$conf = (@include APP_PATH . 'conf/conf.php') OR exit('<script>window.location="install/"</script>');

!isset($conf['user_create_on']) AND $conf['user_create_on'] = 1;
!isset($conf['logo_mobile_url']) AND $conf['logo_mobile_url'] = 'view/img/logo.png';
!isset($conf['logo_pc_url']) AND $conf['logo_pc_url'] = 'view/img/logo.png';
!isset($conf['logo_water_url']) AND $conf['logo_water_url'] = 'view/img/water-small.png';
$conf['version'] = '4.0.4';

substr($conf['log_path'], 0, 2) == './' AND $conf['log_path'] = APP_PATH . $conf['log_path'];
substr($conf['tmp_path'], 0, 2) == './' AND $conf['tmp_path'] = APP_PATH . $conf['tmp_path'];
substr($conf['upload_path'], 0, 2) == './' AND $conf['upload_path'] = APP_PATH . $conf['upload_path'];
$_SERVER['conf'] = $conf;

if (DEBUG > 1) {
    include XIUNOPHP_PATH . 'xiunophp.php';
} else {
    include XIUNOPHP_PATH . 'xiunophp.min.php';
}


$otherExchange = array(
    'MXN', 'BRL', 'CLP', 'ARS', 'PEN', 'COP', 'UYU', 'PLN', 'RON', 'SEK', 'EUR', 'GBP','USD'
);

$otherExchangeData = [];
foreach ($otherExchange as $v) {
    $exchangeApiData = file_get_contents('http://op.juhe.cn/onebox/exchange/currency?key=b67c4a7117c0e3c64571663e2a3ad260&from=CNY&to=' . $v);
    $exchangeApiData = json_decode($exchangeApiData, true);
    if ($exchangeApiData['error_code'] == '0') {
        $otherExchangeData[$v] = $exchangeApiData['result'][1]['exchange'];
    }
}
$otherExchangeData['dateline'] = time();
db_insert('exchange_other', $otherExchangeData);
