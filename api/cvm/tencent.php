<?php
require_once '../../vendor/autoload.php';

use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Monitor\V20180724\MonitorClient;
use TencentCloud\Monitor\V20180724\Models\GetMonitorDataRequest;
date_default_timezone_set('PRC');
$action = isset($_GET['apiAction']) ?: 'cvm';
if ($action == 'cvm') {
    $cred = new Credential("AKIDud6LM47jUyLSKuqDX0BF4exyhfOq6Pec", "rTBeqa5LM6bQ1LSK1RZVeXBi04qKZy81");
    $httpProfile = new HttpProfile();
    $httpProfile->setEndpoint("monitor.tencentcloudapi.com");
    $clientProfile = new ClientProfile();
    $clientProfile->setHttpProfile($httpProfile);
    $client = new MonitorClient($cred, "ap-guangzhou", $clientProfile);
    $req = new GetMonitorDataRequest();
    $params = array(
        "Instances" => array(
            array(
                "Dimensions" => array(
                    array(
                        "Name" => "InstanceId",
                        "Value" => "ins-10bcm0ec"
                    )
                ),
                "Dimensions" => array(
                    array(
                        "Name" => "InstanceId",
                        "Value" => "ins-bepw1lwu"
                    ),
                ),
                "Dimensions" => array(
                    array(
                        "Name" => "InstanceId",
                        "Value" => "ins-dq2vdpym"
                    )
                )

            )
        ),
        "Namespace" => "QCE/CVM",
        "MetricName" => "MemUsage",
        "Period" => 10,
        "StartTime" =>date("c", strtotime("-1 min")),
        "EndTime" =>date("c", strtotime("now"))
    );
    $req->fromJsonString(json_encode($params));

    $resp = $client->GetMonitorData($req);

    print_r($resp->toJsonString());
}