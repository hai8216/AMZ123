<?php
if (isset($_GET['debug'])) {
    !defined('DEBUG') and define('DEBUG', 2);
} else {
    !defined('DEBUG') and define('DEBUG', 0);
}
define('APP_PATH', dirname(dirname(dirname(__FILE__))) . '/');
!defined('ADMIN_PATH') and define('ADMIN_PATH', APP_PATH . 'admin/');
!defined('XIUNOPHP_PATH') and define('XIUNOPHP_PATH', APP_PATH . 'xiunophp/');
$conf = (@include APP_PATH . 'conf/conf.php') or exit('<script>window.location="install/"</script>');

!isset($conf['user_create_on']) and $conf['user_create_on'] = 1;
!isset($conf['logo_mobile_url']) and $conf['logo_mobile_url'] = 'view/img/logo.png';
!isset($conf['logo_pc_url']) and $conf['logo_pc_url'] = 'view/img/logo.png';
!isset($conf['logo_water_url']) and $conf['logo_water_url'] = 'view/img/water-small.png';
$conf['version'] = '4.0.4';

substr($conf['log_path'], 0, 2) == './' and $conf['log_path'] = APP_PATH . $conf['log_path'];
substr($conf['tmp_path'], 0, 2) == './' and $conf['tmp_path'] = APP_PATH . $conf['tmp_path'];
substr($conf['upload_path'], 0, 2) == './' and $conf['upload_path'] = APP_PATH . $conf['upload_path'];
$_SERVER['conf'] = $conf;

include XIUNOPHP_PATH . 'xiunophp.min.php';

$token = param('sign');
$timestamp = param('timestamp');
//if (time() - $_GET['timestamp'] > 60) {
//    echo json_encode(["code" => 0, "message" => 'error']);
//    exit;
//}
//$code = md5("85a4478065a47473ebad59f97bd632ff&timestamp=" . $timestamp);
//if ($code != $token) {
//    echo json_encode(["code" => 0, "message" => 'sign error']);
//    exit;
//}



$tagsword = param('tag');
$token = param('sign');
$timestamp = param('timestamp');
if (time() - $timestamp > 60) {
    echo json_encode(["code"=>0,"message"=>'error']);
    exit;
}
$code = md5("85a4478065a47473ebad59f97bd632ff&timestamp=" . $timestamp);
if ($code != $token) {
    echo json_encode(["code"=>0,"message"=>'sign error']);
    exit;
}
//查询这个标签下的帖子
$page = param('page');
$pagesize = 20;
$start = ($page - 1) * $pagesize;
$thraedlist = db_find("thread", array('fid' => 1), array('create_date' => '-1'), $page, $pagesize);
$count = db_count("thread", array('fid' => 1, 'is_deleted'=>0));
foreach ($thraedlist as $k => $v) {
    $uids[] = $v['uid'];
}
$user = db_find("user", array('uid' => $uids), [], 1, $pagesize);
foreach ($user as $k => $v) {
    $uidinfo[$v['uid']] = $v;
}
$list = [];
foreach ($thraedlist as $k => $v) {
    $list[$k]['title'] = $v['subject'];
    $list[$k]['authorName'] = $uidinfo[$v['uid']]['username'];
    $list[$k]['articleMobileUrl'] = "https://www.amz123.com/thread-" . $v['tid'];
    $list[$k]['coverUrl'] = getPost_imgs($v['tid'], $v);
    $list[$k]['publishTime'] = date('Y-m-d H:i', $v['create_date']);
    $list[$k]['publishTimeStamp'] = $v['create_date'];
    $list[$k]['source'] = 'AMZ123';
    foreach(getThreadTag($v['tid']) as $value){
        $tagname[]=$value['tag_name'];
    }
    $list[$k]['tags'] = implode(",",$tagname);
    unset($tagname);
}
$data['count'] = $count;
$data['pageData'] = $list;
echo json_encode(["code" => 200, "msg" => $data]);
exit;
function getlistThread($tid)
{
    $thread_tag = db_find("thread_tag_keys", array('tid' => $tid), array(), 1, 5);
    if (!empty($thread_tag)) {
        foreach ($thread_tag as $v) {
            $tags_id[] = $v['tags_id'];
        }
        $taglist = db_find("thread_tag", array('tag_id' => $tags_id), array(), 1, 5);
        return $taglist;
    }else{
        return [];
    }
}

function getThreadTag($tid)
{
    $tagList = cache_get('cache_thread_taglist_' . $tid);
    if (empty($tagList) || !$tagList) {
        $tagList = getlistThread($tid);
        cache_set('cache_thread_taglist_' . $tid, $tagList, 1800);
    }
    return $tagList;
}

function getPost_imgs($tid, $threadinfo, $sumTemp = '100')
{
    $thumb = $threadinfo;
    if ($thumb['thumb']) {
        //兼容微信封面的设置
        $timgs = I($thumb['thumb']);
    } else if ($thumb['thumb_id']) {
        $timgs = getImgCloud() . "/static/defaut_thumb/img/" . $thumb['thumb_id'] . ".jpg";
    } else {
        $r = db_find_one('post', array('tid' => $tid, 'isfirst' => 1));
        $temp = mt_rand(1, $sumTemp); //随机图片个数
        $content = $r['message_fmt']; //文章内容
        preg_match_all("/<img.*src=[\'|\"](.*)[\'|\"]\s*.*>/iU", $content, $img);
        $imgArr = $img[1];
        if (isset($imgArr[0])) {
            //此处封面存在非腾讯COS，需要存到腾讯COS，一般业务在自助发帖时产生，补充函数，自主发帖本地化后会强制
            if (strstr($imgArr[0], 'http')) {
                if (strstr($imgArr[0], 'img.amz123.com')) {
                    db_update("thread", array('tid' => $tid), array('thumb' => $imgArr[0]));
                    thread_cache_clean($tid);
                    return $imgArr[0];
                } else {
                    $path = "./upload/thread_thumb/" . date('Ymd');
                    if (!file_exists($path)) mkdir($path, 0777, true);
                    $extname = substr($imgArr[0], strrpos($imgArr[0], '.'));
                    if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
                        $ext = $extname;
                    } else {
                        $ext = '.jpg';
                    }
                    $name = uniqid() . $ext;
                    if (saveTencent($imgArr[0], $path . "/" . $name)) {
                        //
                        $imgR = I($path . "/" . $name);
                        db_update("thread", array('tid' => $tid), array('thumb' => $imgR));
                        thread_cache_clean($tid);
                        return $imgR;
                    }
                }
            } else {
                db_update("thread", array('tid' => $tid), array('thumb' => I($imgArr[0])));
                return I($imgArr[0]);
            }

        } else {
            db_update("thread", array('tid' => $tid), array('thumb_id' => $temp));
            $timgs = getImgCloud() . "/static/defaut_thumb/img/" . $temp . ".jpg"; //随机图片
        }

    }
    return $timgs;
}


//获取图片地址
function I($path, $size = 'viewthumb')
{
    $signRadio = 0;
    $CloudHost = getImgCloud();
    if (strpos($path, 'www.amz123.com') !== false) {
        return str_replace("www.amz123.com", 'img.amz123.com');
    } else if (strpos($path, 'nosize') !== false) {
        return $path;
    } else if (strpos($path, 'img.amz123.com') !== false) {
        //需要重新鉴权，因为涉及刀图片编辑后的鉴权保留
        $defimg = getNosign($path) . "/" . $size;
        //老数据有一些是相对路径的
        $newUrl = str_replace("comupload", "com/upload", $defimg);
        return $newUrl;
    } else if (strpos($path, '/thread_') !== false) {
        $path = str_replace("./upload/", "/upload/", $path);
        $defimg = $path . "/" . $size;
        return str_replace("comupload/", "com/upload/", $CloudHost . $defimg);

    } else if (strpos($path, 'plugin/ax_posts_list/img') !== false) {
        //ax_posts_list默认封面
        return $CloudHost . "/static/defaut_thumb/img/" . str_replace('plugin/ax_posts_list/img', '', $path) . "/" . $size;
    } else if (strpos($path, 'index_icon') !== false) {
        //ax_posts_list默认封面
        $path = str_replace("./upload/", "/upload/", $path);
        return $CloudHost . $path . "/" . $size;
    } else {
        $imgPath = str_replace("./upload/", "/upload/", $path) . "/" . $size;
        $t = time();
        $newUrl = str_replace("comupload", "com/upload", $CloudHost . $imgPath);
        if ($size == 'pcthumb') {
            $newUrl = str_replace("def/pcthumb", "pcthumb", $CloudHost . $imgPath);
        }
        return $newUrl;
    }
}

function getCloudImg($path, $size = 'ad_right_thumb')
{
    $imgPath = str_replace("./upload/", "/upload/", $path);
    return getImgCloud() . "/" . $imgPath . "/" . $size;
}

function getImgCloud()
{
    return "https://img.amz123.com";
}

function getNosign($img)
{
    $url = parse_url($img);
    $url = $url['scheme'] . "://" . $url['host'] . $url['path'];
    $url = str_replace("/viewthumb", "", $url);
    $url = str_replace("/pcthumb", "", $url);
    $url = str_replace("/listthumb", "", $url);
    return $url;
}
