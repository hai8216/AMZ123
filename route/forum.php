<?php

!defined('DEBUG') AND exit('Access Denied.');

// hook forum_start.php
$fid = param(1, 0);
$sfid = $fid;
if (param('loadajax')) {
    $page = param('page', 1);
} else {
    $page = param(2, 1);
}

$orderby = param('orderby');
$extra = array(); // 给插件预留

$active = 'default';
!in_array($orderby, array('tid', 'lastpid')) AND $orderby = 'lastpid';
$extra['orderby'] = $orderby;
if (in_array($fid, array(1))) {
    $orderby = 'create_date';
}
if (in_array($fid, array(8))) {
    $orderby = 'last_date';
}

if (in_array($fid, [4])) {
    $orderby = 'ax_time_2';
}

$forum = forum_read($fid);
empty($forum) AND message(3, lang('forum_not_exists'));
forum_access_user($fid, $gid, 'allowread') OR message(-1, lang('insufficient_visit_forum_privilege'));
//$pagesize = $conf['pagesize'];
$pagesize = 39;
// hook forum_top_list_before.php

$toplist = $page == 1 ? thread_top_find($fid) : array();

// 从默认的地方读取主题列表
$thread_list_from_default = 1;

// hook forum_thread_list_before.php

if ($thread_list_from_default) {
    $pagination = pagination(url("forum-$fid-{page}", $extra), $forum['threads'], $page, $pagesize);
//    $orderby = 'lastpid';
    $conf['order_default'] = $orderby;
    $threadlist = thread_find_by_fid($fid, $page, $pagesize, $orderby);
}

$page == 1 && $pagesize -= count($toplist);
//if($_GET['debug']){
//    print_r($threadlist);exit;
//}
$header['title'] = $forum['seo_title'] ? $forum['seo_title'] : $forum['name'] . '-' . $conf['sitename'];
$header['mobile_title'] = $forum['name'];
$header['mobile_link'] = url("forum-$fid");
$header['keywords'] = '';
$header['description'] = $forum['brief'];

$_SESSION['fid'] = $fid;

if (param('loadajax')) {
    ob_start();
    if($sfid==15){
        require_once(APP_PATH . 'plugin/ax_posts_list/view/htm/list_15.htm');
    }else{
        require_once(APP_PATH . 'plugin/ax_posts_list/view/htm/list.htm');
    }

    $html = ob_get_clean();
    $ajaxdata['page'] = $page;
    $ajaxdata['code'] = 200;
    if (!empty($threadlist)) {
        $ajaxdata['c'] = 1;
        $ajaxdata['threadlist'] = $html;
    } else {
        $ajaxdata['c'] = 0;
        $ajaxdata['threadlist'] = "";
    }
    echo json_encode($ajaxdata);
    exit;
}
// hook forum_end.php

?>