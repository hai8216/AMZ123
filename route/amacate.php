<?php
$size = param(2, 1000);
$page = param(1, 1);
$group = param('group', 1);
if ($group == '1') {
    $map['local'] = ['pc', 'sporting-goods', 'fashion'];
} elseif ($group == '2') {
    $map['local'] = ['photo', 'industrial', 'hi', 'baby-products'];
} elseif ($group == '3') {
    $map['local'] = ['automotive', 'home-garden', 'hpc'];
} elseif ($group == '4') {
    $map['local'] = [
        'electronics',
        'kitchen',
        'lawn-garden',
        'office-products',
        'toys-and-games',
        'arts-crafts',
        'pet-supplies',
        'beauty',
        'wireless',
        'appliances
    '];
}
$datalist = db_find("amazon_cate_new", $map, [], $page, $size);
$count =  db_count("amazon_cate_new", $map);
$allpage = ceil($count / $size);

$data['data'] = ["p" => $page, "total" => $allpage, "data" => $datalist];
$data['code'] = 200;
echo json_encode($data);

