<?php

!defined('DEBUG') AND exit('Access Denied.');

$action = param(1);

// hook my_start.php

$user = user_read($uid);
if (!in_array($action, ['monitor', 'monitor_config'])) {
    user_login_check();
}

$header['mobile_title'] = $user['username'];
$header['mobile_linke'] = url("my");

is_numeric($action) AND $action = '';

$active = $action;

// hook my_action_before.php

if(empty($action)) {
	
	$header['title'] = lang('my_home');


//	if($uid=='2' && $_GET['debug']){
//        if(check_wap()){
//            include _include(APP_PATH . 'view/htm/newSpace_4g.htm');
//        }else{
//            include _include(APP_PATH . 'view/htm/newSpace.htm');
//        }
//    }else{
//        include _include(APP_PATH.'view/htm/my.htm');
//    }

//    $_group = group_read($user['gid']);
//    if ($_group['newSpace'] == '1') {
//        if(check_wap()){
//            include _include(APP_PATH . 'view/htm/newSpace_4g.htm');
//        }else{
//            include _include(APP_PATH . 'view/htm/newSpace.htm');
//        }
//    }else {
//        include _include(APP_PATH . 'view/htm/my.htm');
//    }
    include _include(APP_PATH . 'view/htm/my.htm');
	
/*	
} elseif($action == 'profile') {
	
	if($ajax) {
		// user_safe_info($user);
		message(0, $user);
	} else {
		include _include(APP_PATH.'view/htm/my_profile.htm');
	}
*/
	
} elseif($action == 'password') {
	
	if($method == 'GET') {
		
		// hook my_password_get_start.php
		
		include _include(APP_PATH.'view/htm/my_password.htm');
		
	} elseif($method == 'POST') {
		
		// hook my_password_post_start.php
		
		$password_old = param('password_old');
		$password_new = param('password_new');
		$password_new_repeat = param('password_new_repeat');
		$password_new_repeat != $password_new AND message(-1, lang('repeat_password_incorrect'));
		md5($password_old.$user['salt']) != $user['password'] AND message('password_old', lang('old_password_incorrect'));
		$password = md5($password_new.$user['salt']);
		$r = user_update($uid, array('password'=>$password));
		$r === FALSE AND message(-1, lang('password_modify_failed'));
		
		// hook my_password_post_end.php
        
        // 同步用户中心
        modify_user(['password' => $password_new], $uid);
		message(0, lang('password_modify_successfully'));
		
	}
	

} elseif($action == 'thread') {

	// hook my_thread_start.php
	
	$page = param(2, 1);
	$pagesize = 20;
	$totalnum = $user['threads'];
	
	// hook my_profile_thread_list_before.php
	
	$pagination = pagination(url('my-thread-{page}'), $totalnum, $page, $pagesize);
	$threadlist = mythread_find_by_uid($uid, $page, $pagesize);
	
	// hook my_thread_end.php
	
	include _include(APP_PATH.'view/htm/my_thread.htm');

	
} elseif($action == 'avatar') {
	
	if($method == 'GET') {
		
		// hook my_avatar_get_start.php
		
		include _include(APP_PATH.'view/htm/my_avatar.htm');
	
	} else {
		
		// hook my_avatar_post_start.php
		
		$width = param('width');
		$height = param('height');
		$data = param('data', '', FALSE);
		
		empty($data) AND message(-1, lang('data_is_empty'));
		$data = base64_decode_file_data($data);
		$size = strlen($data);
		$size > 40000 AND message(-1, lang('filesize_too_large', array('maxsize'=>'40K', 'size'=>$size)));
		
		$filename = "$uid.png";
		$dir = substr(sprintf("%09d", $uid), 0, 3).'/';
		$path = $conf['upload_path'].'avatar/'.$dir;
		$url = $conf['upload_url'].'avatar/'.$dir.$filename;
		!is_dir($path) AND (mkdir($path, 0777, TRUE) OR message(-2, lang('directory_create_failed')));
		
		// hook my_avatar_post_save_before.php
		file_put_contents($path.$filename, $data) OR message(-1, lang('write_to_file_failed'));
		
		user_update($uid, array('avatar'=>$time));

        $remoteFileName = $uid.'_'.time().'.png';
        $remotePath = 'upload/avatar/'.$dir.$remoteFileName;
        $res = saveImgToNewTable($remotePath,$data,1,$uid);
        if (!$res['status']){
            message(-1, $res['msg']);
        }

		// hook my_avatar_post_end.php
		message(0, array('url'=>$res['img_url']));
		
	}
} elseif($action == 'monitor') {
  	$header['title'] = lang('my_monitor');
    $monitor_url = 'http://api.amzhacker.com:8888/';
    $base_data = [
        "version" => "1.0",
    ];
    $config = db_find_one('monitor_config', ['id' => 1]);

  	if ($method == 'GET') {
        user_login_check();

  	    // 获取跟卖监控列表
        $data = array_merge($base_data, [
            'act_id' => 240,
            'data'   => [
                'user' => (string)$user['uid'],
                'monitor_type' => 2
            ]
        ]);
        $gm_monitor_result = _curl_post($monitor_url, $data);
        $gm_monitor_list = [];
        $gm_monitor_result['status'] == 1 && $gm_monitor_list = $gm_monitor_result['info'];
        foreach ($gm_monitor_list as $k => $v) {
            $gm_monitor_list[$k]['asin_id_info']['updated_time'] = date('Y-m-d H:i:s', $v['asin_id_info']['updated_time']);
            $gm_monitor_list[$k]['updated_time'] = date('Y-m-d H:i:s', $v['updated_time']);
            switch ($v['domain']) {
                case 'com':
                    $gm_monitor_list[$k]['domain_show'] = '美国';
                    break;
                case 'co.uk':
                    $gm_monitor_list[$k]['domain_show'] = '英国';
                    break;
                case 'de':
                    $gm_monitor_list[$k]['domain_show'] = '德国';
                    break;
                case 'fr':
                    $gm_monitor_list[$k]['domain_show'] = '法国';
                    break;
                case 'es':
                    $gm_monitor_list[$k]['domain_show'] = '西班牙';
                    break;
                case 'it':
                    $gm_monitor_list[$k]['domain_show'] = '意大利';
                    break;
                case 'ca':
                    $gm_monitor_list[$k]['domain-show'] = '加拿大';
                    break;
            }
            $gm_monitor_list[$k]['asin_id_info']['seller_count'] = count($gm_monitor_list[$k]['asin_id_info']['seller_dict']);
            $gm_monitor_list[$k]['asin_id_info']['is'] = $gm_monitor_list[$k]['asin_id_info']['seller_count'] ? '是' : '否';
        }

		// 获取差评监控列表
        $data['data']['monitor_type'] = 1;
        $cp_monitor_result = _curl_post($monitor_url, $data);
        $cp_monitor_list = [];
        $cp_monitor_result['status'] == 1 && $cp_monitor_list =$cp_monitor_result['info'];
        foreach ($cp_monitor_list as $k => $v) {
            $cp_monitor_list[$k]['asin_id_info']['updated_time'] = date('Y-m-d H:i:s', $v['asin_id_info']['updated_time']);
            $cp_monitor_list[$k]['updated_time'] = date('Y-m-d H:i:s', $v['updated_time']);
            switch ($v['domain']) {
                case 'com':
                    $cp_monitor_list[$k]['domain_show'] = '美国';
                    break;
                case 'co.uk':
                    $cp_monitor_list[$k]['domain_show'] = '英国';
                    break;
                case 'de':
                    $cp_monitor_list[$k]['domain_show'] = '德国';
                    break;
                case 'fr':
                    $cp_monitor_list[$k]['domain_show'] = '法国';
                    break;
                case 'es':
                    $cp_monitor_list[$k]['domain_show'] = '西班牙';
                    break;
                case 'it':
                    $cp_monitor_list[$k]['domain_show'] = '意大利';
                    break;
                case 'ca':
                    $cp_monitor_list[$k]['domain-show'] = '加拿大';
                    break;
            }
        }

        $qr_code_result = _curl_post($monitor_url, array_merge($base_data, [
            'act_id' => 241,
            'data' => [
                'user' => (string)$user['uid'],
                'nick_name' => $user['username']
            ]
        ]));
        $qr_code_url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={$qr_code_result['info']['ticket']}";                  // 二维码
        include _include(APP_PATH.'view/htm/my_monitor.htm');
    } else {
  	    if (empty($user)) {
  	        message(1, '请先登录');
  	    } else {
            $monitor_type = intval(param('monitor_type'));
            $asin_id_list = param('asin_id', []);
            $domain = param('domain');

            $add_data = [];
            if (intval(param('op_type')) == 0) {         // 添加
                // 获取数量设置
                if (count($asin_id_list) > $config['number']) {
                    message(1, "最多只能添加 {$config['number']} 个监控");
                }

                $data = array_merge($base_data, [
                    'act_id' => 240,
                    'data'   => [
                        'user' => (string)$user['uid'],
                        'monitor_type' => $monitor_type
                    ]
                ]);
                $monitor_result = _curl_post($monitor_url, $data);
                $monitor_list = [];
                $monitor_result['status'] == 1 && $monitor_list = $monitor_result['info'];
                if (count($asin_id_list) + count($monitor_list) > $config['number']) {
                    message(1, "最多只能添加 {$config['number']} 个监控");
                }

                foreach ($asin_id_list as $asin_id) {
                    $add_data[] = [
                        'asin_id' => $asin_id,
                        'domain' => $domain,
                        'is_active' => 1
                    ];
                }

                $act_id = 210;
                $data = array_merge($base_data, [
                    'act_id' => $act_id,
                    'user' => (string)$user['uid'],
                    'monitor_type' => $monitor_type,
                    'data' => $add_data
                ]);
            } else {                                    // 删除
                foreach ($asin_id_list as $asin_id) {
                    $add_data[] = [
                        'asin_id' => $asin_id,
                        'domain' => $domain
                    ];
                }
                $act_id = 220;
                $data = array_merge($base_data, [
                    'act_id' => $act_id,
                    'monitor_type' => $monitor_type,
                    'user' => (string)$user['uid'],
                    'data' => $add_data
                ]);
            }

            $op_result = _curl_post($monitor_url, $data);
            if ($op_result['status'] == 1) {
                $status = 0;
            } else {
                $status = 1;
            }
            message($status, $op_result['info']);
        }
    }
} elseif ($action == 'monitor_config') {
    if (in_array($user['uid'], [1, 9250])) {
        $header['title'] = lang('my_monitor_config');

        if ($method == 'GET') {
            $config = db_find_one('monitor_config', ['id' => 1]);
            include _include(APP_PATH.'view/htm/my_monitor_config.htm');
        } else {
            $config = [
                'integral' => intval(param('integral')),
                'number'   => intval(param('number'))
            ];
            if (db_update('monitor_config', ['id' => 1], $config)) {
                message(0, '保存成功');
            } else {
                message(1, '保存失败');
            }
        }
    } else {
        if ($method == 'GET') {
            http_location(url('my'));
        } else {
            message(1, '没有权限');
        }
    }
} elseif ($action == 'monitor_white_list') {
    if (empty($user)) {
        message(1, '请先登录');
    }

    $monitor_url = 'http://api.amzhacker.com:8888/';
    $base_data = [
        'version' => '1.0',
    ];

    $asin_id = param('asin_id');
    $domain = param('domain');

    if (intval(param('op_type'))) {
        $white_list = param('white_list', []);
        if (intval(param('op_type')) == 1) {        // 添加
            $act_id = 212;
        } else {                                    // 删除
            $act_id = 231;
        }

        $data = array_merge($base_data, [
            'act_id' => $act_id,
            'data' => [
                'user' => (string)$user['uid'],
                'asin_id' => $asin_id,
                'domain' => $domain,
                'white_list' => $white_list
            ]
        ]);
        $curl_result = _curl_post($monitor_url, $data);
        if ($curl_result['status'] != 1) {
            message(1, $curl_result['info']);
        }
    }

    // 获取跟卖监控列表
    $data = array_merge($base_data, [
        'act_id' => 240,
        'data'   => [
            'user' => (string)$user['uid'],
            'monitor_type' => 2
        ]
    ]);
    $gm_monitor_result = _curl_post($monitor_url, $data);
    $result = [];
    if ($gm_monitor_result['status'] == 1) {
        foreach ($gm_monitor_result['info'] as $gm_monitor) {
            if ($gm_monitor['asin_id'] == $asin_id && $gm_monitor['domain'] == $domain) {
                $result = $gm_monitor['white_list'];
                break;
            }
        }
    }
    message(0, $result);
}

// hook my_end.php

?>