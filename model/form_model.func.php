<?php
/**
 * Author ：colin <821336024@qq.com>
 * Date   ：2021-06-09
 * Version：1.0.0
 * Description：
 **/


function model__create($arr)
{
    $r = db_create('form_model', $arr);
    $r ? do_table($r) : '';
    return $r;
}

function model__update($id, $arr)
{
    $r = db_update('form_model', array('id' => $id), $arr);
    return $r;
}

function model__read($id)
{
    $model = db_find_one('form_model', array('id' => $id));
    return $model;
}
function model__read_key($key)
{
    $model = db_find_one('form_model', array('key' => $key));
    return $model;
}
function model__delete($id)
{
    $r = db_delete('form_model', array('id' => $id));
    do_table($id, $op = 'drop');
    return $r;
}

function model__find($cond = array(), $orderby = array(), $page = 1, $pagesize = 1000)
{
    $list = db_find('form_model', $cond, $orderby, $page, $pagesize, 'id');
    return $list;
}


function data__create($modelid, $arr)
{
    $table_name = 'form_model_data' . $modelid;
    $r = db_create($table_name, $arr);
    return $r;
}

function data__update($modelid, $id, $arr)
{
    $table_name = 'form_model_data' . $modelid;
    $r = db_update($table_name, array('id' => $id), $arr);
    return $r;
}

function data__read($modelid, $id)
{
    $table_name = 'form_model_data' . $modelid;
    $model = db_find_one($table_name, array('id' => $id));
    return $model;
}

function data__read_one($modelid, $cond = [])
{
    $table_name = 'form_model_data' . $modelid;
    $model = db_find_one($table_name, $cond);
    return $model;
}

function data__delete($modelid, $id)
{
    $table_name = 'form_model_data' . $modelid;
    $r = db_delete($table_name, array('id' => $id));
    return $r;
}

function data__find($modelid, $cond = array(), $orderby = array(), $page = 1, $pagesize = 1000)
{
    $table_name = 'form_model_data' . $modelid;
    $list = db_find($table_name, $cond, $orderby, $page, $pagesize, 'id');
    return $list;
}
function data__filter($modelid,$ip){
    $table_name = 'form_model_data' . $modelid;
    $modelformid = db_find_one($table_name,["ip"=>md5($ip)]);
    return $modelformid;
}
function data__filter_byuid($modelid,$uid){
    $table_name = 'form_model_data' . $modelid;
    $modelformid = db_find_one($table_name,["uid"=>$uid]);
    return $modelformid;
}
function get_fields($table_name = '')
{
    $fields = array();
    $sql = "SHOW COLUMNS FROM $table_name";
    $arr = db_sql_find($sql);
    foreach ($arr as $value) {
        $fields[$value['Field']] = $value['Type'];
    }
    return $fields;
}

function table_exists($table)
{
    $tables = list_tables();
    return in_array($table, $tables) ? 1 : 0;
}

function list_tables()
{
    $db = $_SERVER['db'];
    $tables = array();
    $sql = "SHOW TABLES";
    $arr = db_sql_find($sql);
    foreach ($arr as $value) {
        $tables[] = $value['Tables_in_' . $db->conf['master']['name']];
    }
    return $tables;
}

function do_table($modelid = '', $op = 'create')
{
    $db = $_SERVER['db'];
    $table_name = $db->tablepre . 'form_model_data' . $modelid;
    $sql = '';
    switch ($op) {
        case 'create':
            if (!table_exists($table_name)) {
                $sql = "CREATE TABLE IF NOT EXISTS `" . $table_name . "` 
                (
                `id` int(10) unsigned NOT NULL auto_increment,
                `ip` char(32) DEFAULT NULL,
                `uid` int(10) unsigned DEFAULT 0,
                `addtime` DATETIME,
                PRIMARY KEY  (`id`),
                KEY `ip` (`ip`),
                KEY `uid` (`uid`)
                );";
            }
            break;
        case 'drop':
            $sql = "DROP TABLE IF EXISTS $table_name";
            break;
        default:
            break;
    }
    $sql ? db_exec($sql) : '';
    return true;
}


function do_field($modelid = '', $json_data = '')
{
    $db = $_SERVER['db'];
    $table_name = $db->tablepre . 'form_model_data' . $modelid;
    $data = json_decode($json_data, true);
//    print_r($data);exit;
    $fields = $data['list'];
    $table_fields = get_fields($table_name);
    $all_new_fields = [];
    foreach ($fields as $value) {
        $defaultvalue = isset($value['options']['defaultValue']) ? $value['options']['defaultValue'] : '';
        //$valuedata = isset($value['options']['valueData']) ? $value['options']['valueData'] : '';
        $field = $value['model'];
        $all_new_fields[] = $field;
        if (in_array($field, array_keys($table_fields))) {
            continue;
        }
        switch ($value['type']) {
            case 'input':
                $sql = "ALTER TABLE `$table_name` ADD `$field` VARCHAR(250) DEFAULT '$defaultvalue'";
                db_exec($sql);
                break;
            case 'textarea':
            case 'text':
                $sql = "ALTER TABLE `$table_name` ADD `$field` TEXT";
                db_exec($sql);
                break;
            case 'timePicker':
                $sql = "ALTER TABLE `$table_name` ADD `$field` TIME DEFAULT '00:00:00'";
                db_exec($sql);
                break;
            case 'datePicker':
                $sql = "ALTER TABLE `$table_name` ADD `$field` DATE NULL";
                db_exec($sql);
                break;
            case 'inputNumber':
            case 'slider':
            case 'rate':
                $defaultvalue = $defaultvalue ?: 0;
                $sql = "ALTER TABLE `$table_name` ADD `$field` INT(10) DEFAULT '$defaultvalue'";
                db_exec($sql);
                break;
            case 'checkbox':
               // $defaultvalue = $defaultvalue ?implode(',',$defaultvalue): '';
                $sql = "ALTER TABLE `$table_name` ADD `$field` VARCHAR(250) DEFAULT '' ";
                db_exec($sql);
                break;
            case'address':
                //$defaultvalue = $defaultvalue ?implode(',',$defaultvalue): '';
                $sql = "ALTER TABLE `$table_name` ADD `$field` VARCHAR(250) DEFAULT ''";
                db_exec($sql);
                break;
            default:
                $sql = "ALTER TABLE `$table_name` ADD `$field` VARCHAR(250) DEFAULT '$defaultvalue'";
                db_exec($sql);
                break;
        }
    }
    $drop_fields = array_diff(array_keys($table_fields), array_intersect(array_keys($table_fields), $all_new_fields));
    foreach ($drop_fields as $v) {
        if (!in_array($v, ['id', 'uid', 'addtime','ip'])) {
            $sql = "ALTER TABLE `$table_name` DROP `$v`";
            db_exec($sql);
        }
    }
    return true;
}