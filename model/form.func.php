<?php

/*
* Copyright (C) 2015 xiuno.com
*/
function checkVip(){
    global $user;
    $vipinfo = db_find_one("users_vip",["uid"=>$user['uid']]);
    if(!$user['vip'] || $vipinfo['endtime']<time()){
        if(param(3, 1)>5){
            include _include(APP_PATH . 'plugin/xl_amazon_usseller/htm/vip.htm');
            exit;
        }
        if(param('f1') || param('f2') || param('f3') || param('fall') || param('pnum')){
            include _include(APP_PATH . 'plugin/xl_amazon_usseller/htm/vip.htm');
            exit;
        }
    }
}
function byTagsanduids($tags, $uids, $page, $dataType = 0)
{
    $offset = ($page - 1) * 20;
    $s = "";
    if ($tags && $uids) {
        $s .= "mf.tags_id in(" . $tags . ") and m.uid in(" . $uids . ")";
    } else if ($tags && !$uids) {
        $s .= "mf.tags_id in(" . $tags . ")";
    } else if (!$tags && $uids) {
        $s .= "m.uid in(" . $uids . ")";
    }
    $sql = "SELECT m.tid,m.*,mf.tags_id from bbs_thread m left join bbs_thread_tag_keys mf on m.tid=mf.tid where " . $s . "  group by m.tid  order by m.create_date desc limit " . $offset . ",20";
    $thraedlist = db_sql_find($sql);
    return $thraedlist;
}

function byTagsoruids($tags, $uids, $page, $dataType = 0)
{
    $offset = ($page - 1) * 20;
    if ($dataType == 1) {
        $where = '';
        foreach (explode(",", $tags) as $v) {
            $where .= "mf.tags_id = '$v' AND ";
        }
        $where .= "mf.tags_id>0";
        $count = count(explode(",",$tags));
        $sql = "SELECT m.tid,m.*,mf.tid from bbs_thread m left join bbs_thread_tag_keys mf on m.tid=mf.tid where mf.tags_id in(" . $tags . ")  or m.uid in(" . $uids . ")  GROUP BY mf.tid HAVING COUNT(mf.tid)='$count' order by m.create_date desc limit " . $offset . ",20";
    } else {
        if($tags){
            $sql = "SELECT m.tid,m.*,mf.tid from bbs_thread m left join bbs_thread_tag_keys mf on m.tid=mf.tid where mf.tags_id in(" . $tags . ") or m.uid in(" . $uids . ") group by m.tid order by m.create_date desc limit " . $offset . ",20";
        }else{
            $sql = "SELECT * from bbs_thread where uid in(" . $uids . ")   order by create_date desc limit " . $offset . ",20";
        }

    }

//    echo $sql;exit;
    $thraedlist = db_sql_find($sql);
    return $thraedlist;
}

function byTags($tags, $page, $dataType = 0)
{
    $offset = ($page - 1) * 20;
    if ($dataType == 1) {
        $count = count(explode(",",$tags));
        $sql = "SELECT m.tid,m.*,mf.tid from bbs_thread m left join bbs_thread_tag_keys mf on m.tid=mf.tid where mf.tags_id in(" . $tags . ")  GROUP BY mf.tid HAVING COUNT(mf.tid)='$count' order by m.create_date desc limit " . $offset . ",20";
    } else {
        $sql = "SELECT m.tid,m.*,mf.tid from bbs_thread m left join bbs_thread_tag_keys mf on m.tid=mf.tid where mf.tags_id in(" . $tags . ") group by m.tid  order by m.create_date desc limit " . $offset . ",20";
    }
//    echo $sql;exit;
    $thraedlist = db_sql_find($sql);
    return $thraedlist;
}
function byTags_2($tags, $tags2,$page, $dataType = 0)
{
    $offset = ($page - 1) * 20;
    $sql = "SELECT m.*,mf.* from bbs_thread m left join bbs_thread_tag_keys mf on m.tid=mf.tid where mf.tags_id in(" . $tags . ") and mf.tags_id in (" . $tags2 . ") order by m.create_date desc limit " . $offset . ",20";
//    echo $sql;exit;
    $thraedlist = db_sql_find($sql);
    return $thraedlist;
}


function byTagsoruids_2($tags, $uids,$tags2, $page, $dataType = 0)
{
    $offset = ($page - 1) * 20;
    $sql = "SELECT m.*,mf.* from bbs_thread m left join bbs_thread_tag_keys mf on m.tid=mf.tid where (mf.tags_id in(" . $tags . ") and  mf.tags_id in(" . $tags2 . ")) or m.uid in(" . $uids . ") order by m.create_date desc limit " . $offset . ",20";
    $thraedlist = db_sql_find($sql);
    return $thraedlist;
}
function check_wap()
{
    // 先检查是否为wap代理，准确度高
    if (strpos(strtoupper($_SERVER['HTTP_ACCEPT']), "VND.WAP.WML") > 0) {
        return true;
    } //检查USER_AGENT
    elseif (preg_match('/(blackberry|configuration\/cldc|hp |hp-|htc |htc_|htc-|iemobile|kindle|midp|mmp|motorola|mobile|nokia|opera mini|opera |Googlebot-Mobile|YahooSeeker\/M1A1-R2D2|android|iphone|ipod|mobi|palm|palmos|pocket|portalmmm|ppc;|smartphone|sonyericsson|sqh|spv|symbian|treo|up.browser|up.link|vodafone|windows ce|xda |xda_)/i', $_SERVER['HTTP_USER_AGENT'])) {
        return true;
    } else {
        return false;
    }
}

function formatNumber($number)
{
    if (empty($number) || !is_numeric($number)) return $number;
    $unit = "";
    if ($number > 10000) {
        $leftNumber = floor($number / 10000);
        $rightNumber = round(($number % 10000) / 10000, 2);
        // $rightNumber = bcmul(($number % 10000) / 10000, '1', 2);
        $number = floatval($leftNumber + $rightNumber);
        $unit = "万";
    } else {
        $decimals = $number > 1 ? 2 : 6;
        $number = (float)number_format($number, $decimals, '.', '');
    }
    return (string)$number . $unit;
}

function form_radio_yes_no($name, $checked = 0)
{
    $checked = intval($checked);
    return form_radio($name, array(1 => lang('yes'), 0 => lang('no')), $checked);
}

function form_radio($name, $arr, $checked = 0)
{
    empty($arr) && $arr = array(lang('no'), lang('yes'));
    $s = '';

    foreach ((array)$arr as $k => $v) {
        $add = $k == $checked ? ' checked="checked"' : '';
        $s .= "<label class=\"custom-input custom-radio\"><input type=\"radio\" name=\"$name\" value=\"$k\"$add /> $v</label> &nbsp; \r\n";
    }
    return $s;
}

function form_checkbox($name, $checked = 0, $txt = '', $val = 1)
{
    $add = $checked ? ' checked="checked"' : '';
    $s = "<label class=\"custom-input custom-checkbox mr-4\"><input type=\"checkbox\" name=\"$name\" value=\"$val\" $add /> $txt</label>";
    return $s;
}

/*
	form_multi_checkbox('cateid[]', array('value1'=>'text1', 'value2'=>'text2', 'value3'=>'text3'), array('value1', 'value2'));
*/
function form_multi_checkbox($name, $arr, $checked = array())
{
    $s = '';
    foreach ($arr as $value => $text) {
        $ischecked = in_array($value, $checked);
        $s .= form_checkbox($name, $ischecked, $text, $value);
    }
    return $s;
}

function form_select($name, $arr, $checked = 0, $id = TRUE)
{
    if (empty($arr)) return '';
    $idadd = $id === TRUE ? "id=\"$name\"" : ($id ? "id=\"$id\"" : '');
    $s = "<select name=\"$name\" class=\"custom-select\" $idadd> \r\n";
    $s .= form_options($arr, $checked);
    $s .= "</select> \r\n";
    return $s;
}

function form_options($arr, $checked = 0)
{
    $s = '';
    foreach ((array)$arr as $k => $v) {
        $add = $k == $checked ? ' selected="selected"' : '';
        $s .= "<option value=\"$k\"$add>$v</option> \r\n";
    }
    return $s;
}

function form_text($name, $value, $width = FALSE, $holdplacer = '')
{
    $style = '';
    if ($width !== FALSE) {
        is_numeric($width) and $width .= 'px';
        $style = " style=\"width: $width\"";
    }
    $s = "<input type=\"text\" name=\"$name\" id=\"$name\" placeholder=\"$holdplacer\" value=\"$value\" class=\"form-control\"$style />";
    return $s;
}

function form_hidden($name, $value)
{
    $s = "<input type=\"hidden\" name=\"$name\" id=\"$name\" value=\"$value\" />";
    return $s;
}

function form_textarea($name, $value, $width = FALSE, $height = FALSE)
{
    $style = '';
    if ($width !== FALSE) {
        is_numeric($width) and $width .= 'px';
        is_numeric($height) and $height .= 'px';
        $style = " style=\"width: $width; height: $height; \"";
    }
    $s = "<textarea name=\"$name\" id=\"$name\" class=\"form-control\" $style>$value</textarea>";
    return $s;
}

function form_password($name, $value, $width = FALSE)
{
    $style = '';
    if ($width !== FALSE) {
        is_numeric($width) and $width .= 'px';
        $style = " style=\"width: $width\"";
    }
    $s = "<input type=\"password\" name=\"$name\" id=\"$name\" class=\"form-control\" value=\"$value\" $style />";
    return $s;
}

function form_time($name, $value, $width = FALSE)
{
    $style = '';
    if ($width !== FALSE) {
        is_numeric($width) and $width .= 'px';
        $style = " style=\"width: $width\"";
    }
    $s = "<input type=\"text\" name=\"$name\" id=\"$name\" class=\"form-control\" value=\"$value\" $style />";
    return $s;
}


/**用法
 *
 * echo form_radio_yes_no('radio1', 0);
 * echo form_checkbox('aaa', array('无', '有'), 0);
 *
 * echo form_radio_yes_no('aaa', 0);
 * echo form_radio('aaa', array('无', '有'), 0);
 * echo form_radio('aaa', array('a'=>'aaa', 'b'=>'bbb', 'c'=>'ccc', ), 'b');
 *
 * echo form_select('aaa', array('a'=>'aaa', 'b'=>'bbb', 'c'=>'ccc', ), 'a');
 */

?>
