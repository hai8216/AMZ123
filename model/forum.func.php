<?php

// hook model_forum_start.php

//fid new

function getfooterCache()
{
    $cacheData = cache_get('footerCache');
    if ($cacheData) {
        return $cacheData;
    } else {
        $data = db_find("setting_indexpage", [], [], 1, 200);
        foreach ($data as $v) {
            $value[$v['key']] = $v['value'];
        }
        cache_set('footerCache', $value, 1800);
        return $value;
    }
}

function pagelogoSearch_setting($id)
{
//    $cacheData  =  cache_get('pagelogoSearch_setting_'.$id);
//    if($cacheData){
//        return $cacheData;
//    }else{
//        $map['key'] = ["logo","search"];
//        $insertdata = db_find("setting_indexpage",$map,[],1,200);
//        foreach ($insertdata as $v) {
//            $value[$v['key']] = $v['value'];
//        }
//        cache_set('logoSearch_setting', $value, 1800);
//        return $value;
//    }
    return db_find_one('domainpage_domain', array('id' => $id));
}

function logoSearch_setting()
{
    $cacheData = cache_get('logoSearch_setting');
    if ($cacheData) {
        return $cacheData;
    } else {
        $map['key'] = ["logo", "search"];
        $insertdata = db_find("setting_indexpage", $map, [], 1, 200);
        foreach ($insertdata as $v) {
            $value[$v['key']] = $v['value'];
        }
        cache_set('logoSearch_setting', $value, 1800);
        return $value;
    }
}

function getnewThread($fid = '1')
{
    $fid_news_cache = cache_get('fid_news_cache_' . $fid);
    if ($fid_news_cache) {
        return $fid_news_cache;
    } else {
        $data = db_find('thread', array('fid' => $fid, 'is_deleted' => 0), array('create_date' => -1), 1, 15);
        foreach ($data as $k => $v) {
            $data[$k]['url'] = url('thread-' . $v['tid']);
        }
        cache_set('fid_news_cache_', $data, 180);
        return $data;
    }
}

//index_news
function index_news_cache()
{
    $index_news_cache = cache_get('index_news_cache');
    if ($index_news_cache) {
        return $index_news_cache;
    } else {
        $data = db_find('thread', array('fid' => 1, 'is_deleted' => 0), array('create_date' => -1), 1, 15);;
        cache_set('index_news_cache', $data, 180);
        return $data;
    }
}


// ------------> 最原生的 CURD，无关联其他数据。
function bbx_cahce()
{
    $bbx_cache = cache_get('bbx_cache');
    if ($bbx_cache) {
        return $bbx_cache;
    } else {
        $data = db_find("bbx", array(), array("order" => -1), 1, 500);
        cache_set('bbx_cache', $data, 60 * 60 * 240 * 10);
        return $data;
    }

}

//index_cahce
function index_Nav_cache($navid, $limit = '500')
{
    $index_Nav_cache = cache_get('index_Nav_cache_navid_' . $navid);
    if ($index_Nav_cache) {
        return $index_Nav_cache;
    } else {
        $index_Nav_cache = db_find('index_navlist_view', array('navid' => $navid), array('order_id' => '1'), 1, $limit);
        cache_set('index_Nav_cache_navid_' . $navid, $index_Nav_cache, 60 * 60 * 240 * 10);
        return $index_Nav_cache;
    }

}

//domain_index_cahce
function domain_index_Nav_cache($navid, $limit = '500')
{
    $index_Nav_cache = cache_get('domainpage_index_Nav_cache_navid_' . $navid);
    if ($index_Nav_cache) {
        return $index_Nav_cache;
    } else {
        $index_Nav_cache = db_find('domainpage_index_navlist_view', array('navid' => $navid), array('order_id' => '1'), 1, $limit);
        cache_set('domainpage_index_Nav_cache_navid_' . $navid, $index_Nav_cache, 60 * 60 * 240 * 10);
        return $index_Nav_cache;
    }
}

//常用工具
function domain_index_cygj($fp, $limit = '500')
{

    $index_Nav_cache = db_find('domainpage_index_navlist_view', array('navid'=>999,'f1' => $fp), array('order_id' => '1'), 1, $limit);
    return $index_Nav_cache;

}

//footer_ad_cache
function footer_ad_cache()
{
    $footer_ad_cache = cache_get('footer_ad_cache');
    if ($footer_ad_cache == '9999') {
        return array();
    }
    if ($footer_ad_cache) {
        return $footer_ad_cache;
    } else {
        $footer_ad_cache = db_find_one("footer_ad", array('style' => 0, 'is_deleted' => 0, 'star_time' => array('<' => time()), 'end_time' => array('>' => time())), array('order_id' => '-1'));
        if (!$footer_ad_cache) {
            cache_set('footer_ad_cache', 9999, 180);
        } else {
            cache_set('footer_ad_cache', $footer_ad_cache, 180);
        }
        return $footer_ad_cache;
    }
}

function footer_ad2_cache()
{

    $footer_ad_cache = cache_get('footer_ad2_cache');
    if ($footer_ad_cache == '9999') {
        return array();
    }
    if ($footer_ad_cache) {
        return $footer_ad_cache;
    } else {
        $footer_ad_cache = db_find("footer_ad", array('style' => 1, 'is_deleted' => 0, 'star_time' => array('<' => time()), 'end_time' => array('>' => time())), array('order_id' => '-1'));
        if (!$footer_ad_cache) {
            cache_set('footer_ad2_cache', 9999, 180);
        } else {
            cache_set('footer_ad2_cache', $footer_ad_cache, 180);
        }
        return $footer_ad_cache;
    }
}

//ad_cache
function ads_cache($type)
{
    $ax_ads = db_find_one('ax_ads', array('type' => $type), array('linkid' => '-1'));
    return $ax_ads;
//    exit;
//    $ax_ads = cache_get('ax_ads_' . $type);
//    if($ax_ads=='9999'){
//        return array();
//    }
//    if ($ax_ads) {
//        return $ax_ads;
//    } else {
//        $ax_ads = db_find_one('ax_ads', array('type' => $type),array('linkid'=>'-1'));
//        if(!$ax_ads['id']){
//            cache_set('ax_ads_' . $type, 9999, 180);
//        }else{
//            cache_set('ax_ads_' . $type, $ax_ads, 180);
//        }
//        return $ax_ads;
//    }
}

//nav_cache
function ax_nav_sel_cache($note)
{
    $ax_nav_sel_cache = cache_get('ax_nav_sel_cache_' . $note);
    if ($ax_nav_sel_cache == '909090') {
        return false;
    }
    if ($ax_nav_sel_cache) {
        return $ax_nav_sel_cache;
    } else {
        $ax_nav_sel_cache = array(array());
        $ax_nav_sel_cache = db_find('ax_nav_sel', array('note' => $note), array('rank' => -1));
        if ($ax_nav_sel_cache[0]) {
            cache_set('ax_nav_sel_cache_' . $note, $ax_nav_sel_cache, 60 * 60 * 240 * 10);
            return $ax_nav_sel_cache;
        } else {
            cache_set('ax_nav_sel_cache_' . $note, "909090", 60 * 60 * 240 * 10);
        }
    }

}

function getYxzl()
{
    $uids = kv_get("list_ad_set")['re_zlzz'];
    $userlist = db_sql_find("SELECT uid,username,brief,avatar,threads FROM bbs_user where uid IN (" . $uids . ") ORDER BY RAND() LIMIT 9");
    $userlist = array_chunk($userlist, 3);
    return $userlist;
}

function getHotthraed()
{
    //缓存
    $cache_time = kv_get('hotThread_time');
//    $cache_time = 0;
    if (time() - $cache_time > 300 * 6) {
        $time = strtotime('-2 day');
        $list = db_find("thread", array('fid' => 1, 'create_date' => array('>' => $time)), array('views' => '-1'), 1, 10);
        kv_set('hotThread', $list);
        kv_set('hotThread_time', time());
        return $list;
    } else {

        return kv_get('hotThread');
    }

}

function posts_paper($url, $totalnum, $page, $pagesize = 20)
{
    $totalpage = ceil($totalnum / $pagesize);
    if ($totalpage < 2) return '';
    $page = min($totalpage, $page);
    $s = '';
    $page > 1 and $s .= '<li class="previous"><a href="' . str_replace('{page}', $page - 1, $url) . '"></a></li>';
    if ($page > 1) {
        $s .= "<span class='now-page'>" . " $page / $totalpage " . "</span>";
    }
    $totalnum >= $pagesize and $page != $totalpage and $s .= '<span class="next-page"  id="npage"><a href="' . str_replace('{page}', $page + 1, $url) . '"></a></span>';
    return $s;
}

function getPost_img($tid, $sumTemp = '100')
{
    $thumb = thread__read($tid);
    if ($thumb['thumb']) {
        //兼容微信封面的设置
        $timgs = I($thumb['thumb']);
    } else if ($thumb['thumb_id']) {
        $timgs = getImgCloud() . "/static/defaut_thumb/img/" . $thumb['thumb_id'] . ".jpg";
    } else {
        $r = db_find_one('post', array('tid' => $tid, 'isfirst' => 1));
        $temp = mt_rand(1, $sumTemp); //随机图片个数
        $content = $r['message_fmt']; //文章内容
        preg_match_all("/<img.*src=[\'|\"](.*)[\'|\"]\s*.*>/iU", $content, $img);
        $imgArr = $img[1];
        if (isset($imgArr[0])) {
            //此处封面存在非腾讯COS，需要存到腾讯COS，一般业务在自助发帖时产生，补充函数，自主发帖本地化后会强制
            if (strstr($imgArr[0], 'http')) {
                if (strstr($imgArr[0], 'img.amz123.com')) {
                    db_update("thread", array('tid' => $tid), array('thumb' => $imgArr[0]));
                    thread_cache_clean($tid);
                    return $imgArr[0];
                } else {
                    $path = "./upload/thread_thumb/" . date('Ymd');
                    if (!file_exists($path)) mkdir($path, 0777, true);
                    $extname = substr($imgArr[0], strrpos($imgArr[0], '.'));
                    if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
                        $ext = $extname;
                    } else {
                        $ext = '.jpg';
                    }
                    $name = uniqid() . $ext;
                    if (saveTencent($imgArr[0], $path . "/" . $name)) {
                        //
                        $imgR = I($path . "/" . $name);
                        db_update("thread", array('tid' => $tid), array('thumb' => $imgR));
                        thread_cache_clean($tid);
                        return $imgR;
                    }
                }
            } else {
                return $imgArr[0];
            }

        } else {
            db_update("thread", array('tid' => $tid), array('thumb_id' => $temp));
            $timgs = getImgCloud() . "/static/defaut_thumb/img/" . $temp . ".jpg"; //随机图片
        }

    }
    return $timgs;
}


function get15_img($tid, $sumTemp = '100')
{
    $thumb = thread__read($tid);
    if ($thumb['thumb_id']) {
        $timgs = getImgCloud() . "/static/defaut_thumb/img/" . $thumb['thumb_id'] . ".jpg";
    } else {
        $temp = mt_rand(1, $sumTemp);
        db_update("thread", array('tid' => $tid), array('thumb_id' => $temp));
        $timgs = getImgCloud() . "/static/defaut_thumb/img/" . $temp . ".jpg"; //随机图片
    }
    return $timgs;
}

function post_img($tid)
{
    return getPost_img($tid);
}

function post_message($tid)
{
    //
    $postThumbMessage = cache_get('thread_postthumbmessage_' . $tid);
    if ($postThumbMessage) {
        return $postThumbMessage['message'];;
    } else {
        $postFirst = db_find_one('post', array('tid' => $tid, 'isfirst' => 1));
        return strip_tags($postFirst['message_fmt']);
    }
}

function getlistThread($tid)
{
    $thread_tag = db_find("thread_tag_keys", array('tid' => $tid), array(), 1, 100);
    foreach ($thread_tag as $v) {
        $tags_id[] = $v['tags_id'];
    }
    $taglist = db_find("thread_tag", array('tag_id' => $tags_id), array(), 1, 3);
    return $taglist;
}

function getThreadTag($tid){
    $tagList = cache_get('thread_taglist_' . $tid);
    if(empty($tagList) || !$tagList){
        $tagList = getlistThread($tid);
        cache_set('thread_taglist_' . $tid,1800);
    }
    return $tagList;
}

function post_thumb_message($tid)
{
    $postThumbMessage = cache_get('thread_postthumbmessage_' . $tid);
    if ($postThumbMessage) {
        $return['thumb'] = $postThumbMessage['thumb'];
        $return['message'] = $postThumbMessage['message'];
        $return['lastreply'] = $postThumbMessage['lastreply'];
        if ($postThumbMessage['tagslist']) {
            $return['tagslist'] = $postThumbMessage['tagslist'];
        } else {
            $return['tagslist'] = getlistThread($tid);
        }
        return $return;
    } else {
        //不存在缓存，先读内容
        $postFirst = db_find_one('post', array('tid' => $tid, 'isfirst' => 1));
        $return['message'] = strip_tags($postFirst['message_fmt']);
        $return['tagslist'] = getlistThread($tid);
        $return['lastreply'] = db_find_one('post', array('isfirst' => 0, 'tid' => $tid), array('create_date' => -1))['message'];
        //读缩略图
        $thumb = db_find_one('thread', array('tid' => $tid));
        if ($thumb['thumb']) {
            //兼容微信封面的设置
            $return['thumb'] = $thumb['thumb'];
        } else if ($thumb['thumb_id']) {
            $return['thumb'] = getImgCloud() . "/static/defaut_thumb/img/" . $thumb['thumb_id'] . ".jpg";
        } else {
            $content = $postFirst['message'];
            preg_match_all("/<img.*src=[\'|\"](.*)[\'|\"]\s*.*>/iU", $content, $img);
            $imgArr = $img[1];
            if (isset($imgArr[0])) {
                //此处封面存在非腾讯COS，需要存到腾讯COS，一般业务在自助发帖时产生，补充函数，自主发帖本地化后会强制
                if (strstr($imgArr[0], 'http')) {
                    if (strstr($imgArr[0], 'img.amz123.com')) {
                        $return['thumb'] = $imgArr[0];
                    } else {
                        $path = "./upload/thread_thumb/" . date('Ymd');
                        $extname = substr($imgArr[0], strrpos($imgArr[0], '.'));
                        if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
                            $ext = $extname;
                        } else {
                            $ext = '.jpg';
                        }
                        $name = uniqid() . $ext;
                        if (saveTencent($imgArr[0], $path . "/" . $name)) {
                            $imgR = I($path . "/" . $name);
                            db_update("thread", array('tid' => $tid), array('thumb' => $imgR));
                            //更新thread缓存
                            $return['thumb'] = $imgR;
                        }
                    }
                } else {
                    $return['thumb'] = $imgArr[0];
                }

            } else {
                $temp = mt_rand(1, 100);
                db_update("thread", array('tid' => $tid), array('thumb_id' => $temp));
                $return['thumb'] = getImgCloud() . "/static/defaut_thumb/img/" . $temp . ".jpg";
            }
        }

        cache_set('thread_postthumbmessage_' . $tid, $return, 600);
        return $return;

    }

}

function getcreditRank()
{
    $conf = _SERVER('conf');
    $cache_time = kv_get('creditRank_time');
    if (time() - $cache_time > 300 * 6) {
        $list = db_find("user", array(), array('credits' => '-1'), 1, 10);
        foreach ($list as $k => $v) {
            $dir = substr(sprintf("%09d", $v['uid']), 0, 3);
            $list[$k]['avatar_url'] = $v['avatar'] ? $conf['upload_url'] . "avatar/$dir/$v[uid].png?" . $v['avatar'] : 'view/img/avatar.png';
        }
        kv_set('creditRan', $list);
        kv_set('creditRank_time', time());
        return $list;
    } else {
        return kv_get('creditRan');
    }
}

function datedc($v)
{
    $time = date('Y-m-d H:i', $v);
    return $time;
}

function forum__create($arr)
{
    // hook model_forum__create_start.php
    $r = db_create('forum', $arr);
    // hook model_forum__create_end.php
    return $r;
}

function forum__update($fid, $arr)
{
    // hook model_forum__update_start.php
    $r = db_update('forum', array('fid' => $fid), $arr);
    // hook model_forum__update_end.php
    return $r;
}

function forum__read($fid)
{
    // hook model_forum__read_start.php
    $forum = db_find_one('forum', array('fid' => $fid));
    // hook model_forum__read_end.php
    return $forum;
}

function forum__delete($fid)
{
    // hook model_forum__delete_start.php
    $r = db_delete('forum', array('fid' => $fid));
    // hook model_forum__delete_end.php
    return $r;
}

function forum__find($cond = array(), $orderby = array(), $page = 1, $pagesize = 1000)
{
    // hook model_forum__find_start.php
    $forumlist = db_find('forum', $cond, $orderby, $page, $pagesize, 'fid');
    // hook model_forum__find_end.php
    return $forumlist;
}

// ------------> 关联 CURD，主要是强相关的数据，比如缓存。弱相关的大量数据需要另外处理。

function forum_create($arr)
{
    // hook model_forum_create_start.php
    $r = forum__create($arr);
    forum_list_cache_delete();
    // hook model_forum_create_end.php
    return $r;
}

function forum_update($fid, $arr)
{
    // hook model_forum_update_start.php
    $r = forum__update($fid, $arr);
    forum_list_cache_delete();
    // hook model_forum_update_end.php
    return $r;
}

function forum_read($fid)
{
    // hook model_forum_read_start.php
    global $conf, $forumlist;
    if ($conf['cache']['enable']) {
        return empty($forumlist[$fid]) ? array() : $forumlist[$fid];
    } else {
        $forum = forum__read($fid);
        forum_format($forum);
        return $forum;
    }
    // hook model_forum_read_end.php
}

// 关联数据删除
function forum_delete($fid)
{
    //  把板块下所有的帖子都查找出来，此处数据量大可能会超时，所以不要删除帖子特别多的板块
    $cond = array('fid' => $fid);
    $threadlist = db_find('thread', $cond, array(), 1, 1000000, '', array('tid', 'uid'));

    // hook model_forum_delete_start.php

    foreach ($threadlist as $thread) {
        thread_delete($thread['tid']);
    }

    $r = forum__delete($fid);

    forum_access_delete_by_fid($fid);

    forum_list_cache_delete();
    // hook model_forum_delete_end.php
    return $r;
}

function forum_find($cond = array(), $orderby = array('rank' => -1), $page = 1, $pagesize = 1000)
{
    // hook model_forum_find_start.php
    $forumlist = forum__find($cond, $orderby, $page, $pagesize);
    if ($forumlist) foreach ($forumlist as &$forum) forum_format($forum);
    // hook model_forum_find_end.php
    return $forumlist;
}

// ------------> 其他方法

function forum_format(&$forum)
{
    global $conf;
    if (empty($forum)) return;

    // hook model_forum_format_start.php

    $forum['create_date_fmt'] = date('Y-n-j', $forum['create_date']);
    $forum['icon_url'] = $forum['icon'] ? $conf['upload_url'] . "forum/$forum[fid].png" : 'view/img/forum.png';
    $forum['accesslist'] = $forum['accesson'] ? forum_access_find_by_fid($forum['fid']) : array();
    $forum['modlist'] = array();
    if ($forum['moduids']) {
        $modlist = user_find_by_uids($forum['moduids']);
        foreach ($modlist as &$mod) $mod = user_safe_info($mod);
        $forum['modlist'] = $modlist;
    }
    // hook model_forum_format_end.php
}

function forum_count($cond = array())
{
    // hook model_forum_count_start.php
    $n = db_count('forum', $cond);
    // hook model_forum_count_end.php
    return $n;
}

function forum_maxid()
{
    // hook model_forum_maxid_start.php
    $n = db_maxid('forum', 'fid');
    // hook model_forum_maxid_end.php
    return $n;
}

// 从缓存中读取 forum_list 数据x
function forum_list_cache()
{
    global $conf, $forumlist;
    $forumlist = cache_get('forumlist');

    // hook model_forum_list_cache_start.php

    if ($forumlist === NULL) {
        $forumlist = forum_find();
        cache_set('forumlist', $forumlist, 60);
    }
    // hook model_forum_list_cache_end.php
    return $forumlist;
}

// 更新 forumlist 缓存
function forum_list_cache_delete()
{
    global $conf;
    static $deleted = FALSE;
    if ($deleted) return;

    // hook model_forum_list_cache_delete_start.php

    cache_delete('forumlist');
    $deleted = TRUE;
    // hook model_forum_list_cache_delete_end.php
}

// 对 $forumlist 权限过滤，查看权限没有，则隐藏
function forum_list_access_filter($forumlist, $gid, $allow = 'allowread')
{
    global $conf, $grouplist;
    if (empty($forumlist)) return array();
    if ($gid == 1) return $forumlist;
    $forumlist_filter = $forumlist;
    $group = $grouplist[$gid];

    // hook model_forum_list_access_filter_start.php

    foreach ($forumlist_filter as $fid => $forum) {
        if (empty($forum['accesson']) && empty($group[$allow]) || !empty($forum['accesson']) && empty($forum['accesslist'][$gid][$allow])) {
            unset($forumlist_filter[$fid]);
            unset($forumlist_filter[$fid]['modlist']);
        }
        unset($forumlist_filter[$fid]['accesslist']);
    }
    // hook model_forum_list_access_filter_end.php
    return $forumlist_filter;
}

function forum_filter_moduid($moduids)
{
    $moduids = trim($moduids);
    if (empty($moduids)) return '';
    $arr = explode(',', $moduids);
    $r = array();
    foreach ($arr as $_uid) {
        $_uid = intval($_uid);
        $_user = user_read($_uid);
        if (empty($_user)) continue;
        if ($_user['gid'] > 4) continue;
        $r[] = $_uid;
    }
    return implode(',', $r);
}


function forum_safe_info($forum)
{
    // hook model_forum_safe_info_start.php
    //unset($forum['moduids']);
    // hook model_forum_safe_info_end.php
    return $forum;
}

// hook model_forum_end.php

?>
