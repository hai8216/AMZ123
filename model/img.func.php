<?php


const table_img_relation = "img_relation";

/**
 * @param $remotePath //腾讯云图片路径
 * @param $data //图片内容
 * TODO 后续更多类型主义在这里补充
 * @param $type //业务类型 1:头像（avatar）
 * @param $id //业务类型对应ID  如帖子tid
 * @return array
 * 插入图片到新表
 */
function saveImgToNewTable($remotePath,$data,$type,$id){
    $result = [
        'status' => true,
        'img_url' => '',
        'msg' => ''
    ];
    $remoteUrl = saveTencentBase64V2($data,$remotePath);
//    $remoteUrl .= "?".time();

    if (!$remoteUrl){
        $result['status'] = false;
        $result['msg'] = "上传图片失败！";
        return $result;
    }

    $condition = [
        "type" => $type,
        "source_id" => $id,
        "is_deleted" => 0
    ];
    $data = db_find_one(table_img_relation,$condition);
    if (!$data){
        $data['type'] = $type;
        $data['source_id'] = $id;
        $data['img_url'] = $remoteUrl;
        $res = db_insert(table_img_relation,$data);
    }else{
        $updateData['img_url'] = $remoteUrl;
        $res = db_update(table_img_relation,$condition,$updateData);
    }

    if ($res === false){
        $result['status'] = false;
        $result['msg'] = "操作数据失败！";
        return $result;
    }

    $result['img_url'] = $remoteUrl;
    return  $result;

}


/**
 * @param $id
 * @param $type
 * @return string
 * 获取腾讯云图片链接
 */
function getImgUrlFromCdn($id,$type){
    $condition = [
        "type" => $type,
        "source_id" => $id,
        "is_deleted" => 0
    ];
    $data = db_find_one(table_img_relation,$condition);
    return $data ? $data['img_url']."?".time() : '';
}

?>