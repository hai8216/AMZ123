<?php

// hook model_attach_start.php
function saveImg_pachong($url, $name, $local = '1')
{
    saveTencent($url, $name);
    return true;
}

// ------------> 最原生的 CURD，无关联其他数据。
function saveImg($url, $name, $local = '1')
{
    $r = null;
    if (function_exists("curl_init") && function_exists('curl_exec')) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if (ini_get("safe_mode") == false && ini_get("open_basedir") == false) {
            curl_setopt($ch, CURLOPT_MAXREDIRS, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        }
        if (extension_loaded('zlib')) {
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        $r = curl_exec($ch);
        curl_close($ch);
    } elseif (ini_get("allow_url_fopen")) {
        if (function_exists('ini_set')) ini_set('default_socket_timeout', 300);
        $r = file_get_contents((extension_loaded('zlib') ? 'compress.zlib://' : '') . $url);
    }

    $fp = @fopen($name, "w");

    fwrite($fp, $r);

    fclose($fp);
    //云上保留
    saveTencent($url, $name);
    return true;
}


function getImgCloud()
{
    return "https://img.amz123.com";
}


//腾讯云
function saveTencentTest($path, $savepath)
{
    require APP_PATH . 'model/tencent/vendor/autoload.php';
    $cosClient = new Qcloud\Cos\Client(array(
        'region' => 'ap-guangzhou',
        'credentials' => array(
            'appId' => '1252719694',
            'secretId' => 'AKIDud6LM47jUyLSKuqDX0BF4exyhfOq6Pec',
            'secretKey' => 'rTBeqa5LM6bQ1LSK1RZVeXBi04qKZy81',
        ),
    ));
    $bucket = 'amzend123-1252719694';
    $result = $cosClient->putObject(array(
        'Bucket' => $bucket,
        'Key' => $savepath,
        'Body' => fopen($path, 'rb')
    ));
    if ($result['ObjectURL']) {
        return true;
    } else {
        return false;
    }
}

//腾讯云
function saveTencent($path, $savepath)
{
    require APP_PATH . 'model/tencent/vendor/autoload.php';
    $cosClient = new Qcloud\Cos\Client(array(
        'region' => 'ap-guangzhou',
        'credentials' => array(
            'appId' => '1252719694',
            'secretId' => 'AKIDud6LM47jUyLSKuqDX0BF4exyhfOq6Pec',
            'secretKey' => 'rTBeqa5LM6bQ1LSK1RZVeXBi04qKZy81',
        ),
    ));

    $bucket = 'amzend123-1252719694';
    $result = $cosClient->putObject(array(
        'Bucket' => $bucket,
        'Key' => $savepath,
        'Body' => fopen($path, 'rb')
    ));
    if ($result['ObjectURL']) {
        return true;
    } else {
        return false;
    }
}

//腾讯云
function saveTencent_input($path, $savepath)
{
    require APP_PATH . 'model/tencent/vendor/autoload.php';
    $cosClient = new Qcloud\Cos\Client(array(
        'region' => 'ap-guangzhou',
        'credentials' => array(
            'appId' => '1252719694',
            'secretId' => 'AKIDud6LM47jUyLSKuqDX0BF4exyhfOq6Pec',
            'secretKey' => 'rTBeqa5LM6bQ1LSK1RZVeXBi04qKZy81',
        ),
    ));

    $bucket = 'amzend123-1252719694';
    $result = $cosClient->putObject(array(
        'Bucket' => $bucket,
        'Key' => $savepath,
        'Body' => $path
    ));
    if ($result['ObjectURL']) {
        return true;
    } else {
        return false;
    }
}

//腾讯云
function saveTencentBase64($path, $savepath)
{
    require APP_PATH . 'model/tencent/vendor/autoload.php';

    $cosClient = new Qcloud\Cos\Client(array(
        'region' => 'ap-guangzhou',
        'credentials' => array(
            'appId' => '1252719694',
            'secretId' => 'AKIDud6LM47jUyLSKuqDX0BF4exyhfOq6Pec',
            'secretKey' => 'rTBeqa5LM6bQ1LSK1RZVeXBi04qKZy81',
        ),
    ));

    $bucket = 'amzend123-1252719694';
    $result = $cosClient->putObject(array(
        'Bucket' => $bucket,
        'Key' => $savepath,
        'Body' => $path
    ));
    if ($result['ObjectURL']) {
        return true;
    } else {
        return false;
    }
}

function saveTencentBase64V2($path, $savepath)
{
    require APP_PATH . 'model/tencent/vendor/autoload.php';

    $cosClient = new Qcloud\Cos\Client(array(
        'region' => 'ap-guangzhou',
        'credentials' => array(
            'appId' => '1252719694',
            'secretId' => 'AKIDud6LM47jUyLSKuqDX0BF4exyhfOq6Pec',
            'secretKey' => 'rTBeqa5LM6bQ1LSK1RZVeXBi04qKZy81',
        ),
    ));

    $bucket = 'amzend123-1252719694';
    $result = $cosClient->putObject(array(
        'Bucket' => $bucket,
        'Key' => $savepath,
        'Body' => $path
    ));
    return "https://img.amz123.com/".$savepath ?? '';
}



//获取没有鉴权的图片
function getNosign($img)
{
    $url = parse_url($img);
    $url = $url['scheme'] . "://" . $url['host'] . $url['path'];
    $url = str_replace("/viewthumb", "", $url);
    $url = str_replace("/pcthumb", "", $url);
    $url = str_replace("/listthumb", "", $url);
    return $url;
}

//图片鉴权
function imgSign($img)
{
    $url = parse_url($img);
    if (isset($url['host'])) {
        $t = time();
        $key = 'dg3g9v17ub367eo2esp8yvz4vt';
        $sign = md5($key . $url['path'] . $t);
        return array('sign' => $sign, 't' => $t);
    } else {

        $t = time();
        $key = 'dg3g9v17ub367eo2esp8yvz4vt';
        $sign = md5($key . $img . $t);
        return array('sign' => $sign, 't' => $t);
    }
}

//获取图片地址
function I($path, $size = 'viewthumb')
{
    $signRadio = 0;
    $CloudHost = getImgCloud();
    if (strpos($path, 'www.amz123.com') !== false) {
        return str_replace("www.amz123.com", 'img.amz123.com');
    } else if (strpos($path, 'nosize') !== false) {
        return $path;
    } else if (strpos($path, 'img.amz123.com') !== false) {
        //需要重新鉴权，因为涉及刀图片编辑后的鉴权保留
        $defimg = getNosign($path) . "/" . $size;
        //老数据有一些是相对路径的
        $newUrl = str_replace("comupload", "com/upload", $defimg);
        $sign = imgSign($newUrl);
        if ($signRadio) {
            return $newUrl . "?sign=" . $sign['sign'] . "&t=" . $sign['t'];
        } else {
            return $newUrl;
        }
    } else if (strpos($path, '/thread_') !== false) {
        $path = str_replace("./upload/", "/upload/", $path);
        $defimg = $path . "/" . $size;
        $sign = imgSign($defimg);
        if ($signRadio) {
            return $CloudHost . $defimg . "?sign=" . $sign['sign'] . "&t=" . $sign['t'];
        } else {
            return str_replace("comupload/", "com/upload/", $CloudHost . $defimg);
        }

    } else if (strpos($path, 'plugin/ax_posts_list/img') !== false) {
        //ax_posts_list默认封面
        return $CloudHost . "/static/defaut_thumb/img/" . str_replace('plugin/ax_posts_list/img', '', $path) . "/" . $size;
    } else if (strpos($path, 'index_icon') !== false) {
        //ax_posts_list默认封面
        $path = str_replace("./upload/", "/upload/", $path);
        return $CloudHost . $path . "/" . $size;
    } else {
        $imgPath = str_replace("./upload/", "/upload/", $path) . "/" . $size;
        $t = time();
        $newUrl = str_replace("comupload", "com/upload", $CloudHost . $imgPath);
        if ($size == 'pcthumb') {
            $newUrl = str_replace("def/pcthumb", "pcthumb", $CloudHost . $imgPath);
        }
        return $newUrl;
    }
}

function simpleCloudImg($m)
{
    return IHTML(str_replace('src="upload/"', 'src="https://img.amz123.com/upload/"', $m));
}

function getCloudImg($path, $size = 'ad_right_thumb')
{
    $imgPath = str_replace("./upload/", "/upload/", $path);
    return getImgCloud() . "/" . $imgPath . "/" . $size;
}

//若是HTML结构的内容，需要批量替换链接
function IHTML($message, $size = 'viewthumb')
{

    preg_match_all("/<img[^>]*src\s?=\s?[\'|\"]([^\'|\"]*)[\'|\"]/is", htmlspecialchars_decode($message), $img);
    if ($img[1]) {
        $img[1] = array_unique($img[1]);
        foreach ($img[1] as $k => $v) {
            $imgUrl = I($v, $size);
            $message = str_replace($v, $imgUrl, $message);
        }
    }
    return $message;
}

function attach__create($arr)
{
    // hook model_attach__create_start.php
    $r = db_create('attach', $arr);
    // hook model_attach__create_end.php
    return $r;
}

function attach__update($aid, $arr)
{
    // hook model_attach__update_start.php
    $r = db_update('attach', array('aid' => $aid), $arr);
    // hook model_attach__update_end.php
    return $r;
}

function attach__read($aid)
{
    // hook model_attach__read_start.php
    $attach = db_find_one('attach', array('aid' => $aid));
    // hook model_attach__read_end.php
    return $attach;
}

function attach__delete($aid)
{
    // hook model_attach__delete_start.php
    $r = db_delete('attach', array('aid' => $aid));
    // hook model_attach__delete_end.php
    return $r;
}

function attach__find($cond = array(), $orderby = array(), $page = 1, $pagesize = 20)
{
    // hook model_attach__find_start.php
    $attachlist = db_find('attach', $cond, $orderby, $page, $pagesize);
    // hook model_attach__find_end.php
    return $attachlist;
}

// ------------> 关联 CURD，主要是强相关的数据，比如缓存。弱相关的大量数据需要另外处理。

function attach_create($arr)
{
    // hook model_attach_create_start.php
    $r = attach__create($arr);
    // hook model_attach_create_end.php
    return $r;
}

function attach_update($aid, $arr)
{
    // hook model_attach_update_start.php
    $r = attach__update($aid, $arr);
    // hook model_attach_update_end.php
    return $r;
}

function attach_read($aid)
{
    // hook model_attach_read_start.php
    $attach = attach__read($aid);
    attach_format($attach);
    // hook model_attach_read_end.php
    return $attach;
}

function attach_delete($aid)
{
    // hook model_attach_delete_start.php
    global $conf;
    $attach = attach_read($aid);
    $path = $conf['upload_path'] . 'attach/' . $attach['filename'];
    file_exists($path) and unlink($path);

    $r = attach__delete($aid);
    // hook model_attach_delete_end.php
    return $r;
}

function attach_delete_by_pid($pid)
{
    global $conf;
    list($attachlist, $imagelist, $filelist) = attach_find_by_pid($pid);
    // hook model_attach_delete_by_pid_start.php
    foreach ($attachlist as $attach) {
        $path = $conf['upload_path'] . 'attach/' . $attach['filename'];
        file_exists($path) and unlink($path);
        attach__delete($attach['aid']);
    }
    // hook model_attach_delete_by_pid_end.php
    return count($attachlist);
}

function attach_delete_by_uid($uid)
{
    global $conf;
    // hook model_attach_delete_by_uid_start.php
    $attachlist = db_find('attach', array('uid' => $uid), array(), 1, 9000);
    foreach ($attachlist as $attach) {
        $path = $conf['upload_path'] . 'attach/' . $attach['filename'];
        file_exists($path) and unlink($path);
        attach__delete($attach['aid']);
    }
    // hook model_attach_delete_by_uid_end.php
}

function attach_find($cond = array(), $orderby = array(), $page = 1, $pagesize = 20)
{
    // hook model_attach_find_start.php
    $attachlist = attach__find($cond, $orderby, $page, $pagesize);
    if ($attachlist) foreach ($attachlist as &$attach) attach_format($attach);
    // hook model_attach_find_end.php
    return $attachlist;
}

// 获取 $filelist $imagelist
function attach_find_by_pid($pid)
{
    $attachlist = $imagelist = $filelist = array();
    // hook model_attach_find_by_pid_start.php
    $attachlist = attach__find(array('pid' => $pid), array(), 1, 1000);
    if ($attachlist) {
        foreach ($attachlist as $attach) {
            attach_format($attach);
            $attach['isimage'] ? ($imagelist[] = $attach) : ($filelist[] = $attach);
        }
    }
    // hook model_attach_find_by_pid_end.php
    return array($attachlist, $imagelist, $filelist);
}

// ------------> 其他方法

function attach_format(&$attach)
{
    global $conf;
    if (empty($attach)) return;
    // hook model_attach_format_start.php
    $attach['create_date_fmt'] = date('Y-n-j', $attach['create_date']);
    $attach['url'] = $conf['upload_url'] . 'attach/' . $attach['filename'];
    // hook model_attach_format_end.php
}

function attach_count($cond = array())
{
    // hook model_attach_count_start.php
    $cond = db_cond_to_sqladd($cond);
    $n = db_count('attach', $cond);
    // hook model_attach_count_end.php
    return $n;
}

function attach_type($name, $types)
{
    // hook model_attach_type_start.php
    $ext = file_ext($name);
    foreach ($types as $type => $exts) {
        if ($type == 'all') continue;
        if (in_array($ext, $exts)) {
            return $type;
        }
    }
    // hook model_attach_type_end.php
    return 'other';
}

// 扫描垃圾的附件，每日清理一次
function attach_gc()
{
    global $time, $conf;
    // hook model_attach_gc_start.php
    $tmpfiles = glob($conf['upload_path'] . 'tmp/*.*');
    if (is_array($tmpfiles)) {
        foreach ($tmpfiles as $file) {
            // 清理超过一天还没处理的临时文件
            if ($time - filemtime($file) > 86400) {
                unlink($file);
            }
        }
    }
    // hook model_attach_gc_end.php
}

// 关联 session 中的临时文件，并不会重新统计 images, files
function attach_assoc_post($pid)
{
    global $uid, $time, $conf;
    $sess_tmp_files = cache_get('session_tidai_tmp_files_' . $uid);
    //if(empty($tmp_files)) return;

    $post = post__read($pid);
    if (empty($post)) return;

    // hook attach_assoc_post_start.php

    $tid = $post['tid'];
    $post['message_old'] = $post['message_fmt'];

    // 把临时文件 upload/tmp/xxx.xxx 也处理了
    //preg_match_all('#src="upload/tmp/(\w+\.\w+)"#', $post['message_old'], $m);
    //$use_tmp_files = $m[1]; // 实际使用的临时文件，不用的全部删除？如果是两个帖子一起编辑？

    // 将 session 中的数据和 message 中的数据合并。
    //$tmp_files = array_unique(array_merge($sess_tmp_files, $use_tmp_files));

    $attach_dir_save_rule = array_value($conf, 'attach_dir_save_rule', 'Ym');

    $tmp_files = $sess_tmp_files;
//	if($tmp_files) {
//		foreach($tmp_files as $file) {
//
//			// 将文件移动到 upload/attach 目录
//			$filename = file_name($file['url']);
//
//			$day = date($attach_dir_save_rule, $time);
//			$path = $conf['upload_path'].'attach/'.$day;
//			$url = $conf['upload_url'].'attach/'.$day;
//			!is_dir($path) AND mkdir($path, 0777, TRUE);
//
//			$destfile = $path.'/'.$filename;
//			$desturl = $url.'/'.$filename;
//			$r = xn_copy($file['path'], $destfile);
//			!$r AND xn_log("xn_copy($file[path]), $destfile) failed, pid:$pid, tid:$tid", 'php_error');
////			if(is_file($destfile) && filesize($destfile) == filesize($file['path'])) {
////				@unlink($file['path']);
////			}
//			$arr = array(
//				'tid'=>$tid,
//				'pid'=>$pid,
//				'uid'=>$uid,
//				'filesize'=>$file['filesize'],
//				'width'=>$file['width'],
//				'height'=>$file['height'],
//				'filename'=>"$day/$filename",
//				'orgfilename'=>$file['orgfilename'],
//				'filetype'=>$file['filetype'],
//				'create_date'=>$time,
//				'comment'=>'',
//				'downloads'=>0,
//				'isimage'=>$file['isimage']
//			);
//
//			// 插入后，进行关联
//			$aid = attach_create($arr);
////			$post['message'] = str_replace($file['url'], $desturl, $post['message']);
////			$post['message_fmt'] = str_replace($file['url'], $desturl, $post['message_fmt']);
//
//		}
//	}

    // 清空 session
    $_SESSION['tmp_files'] = array();
    cache_set('session_tidai_tmp_files', '', time());
    $post['message_old'] != $post['message_fmt'] and post__update($pid, array('message' => $post['message'], 'message_fmt' => $post['message_fmt']));

    // 处理不在 message 中的图片，删除掉没有插入的图片附件
    /*
    list($attachlist, $imagelist, $filelist) = attach_find_by_pid($pid);
    foreach($imagelist as $k=>$attach) {
        $url = $conf['upload_url'].'attach/'.$attach['filename'];
        if(strpos($post['message_fmt'], $url) === FALSE) {
            unset($imagelist[$k]);
            attach_delete($attach['aid']);
        }
    }
    */

    // 更新 images files
    list($attachlist, $imagelist, $filelist) = attach_find_by_pid($pid);
    $images = count($imagelist);
    $files = count($filelist);
    $post['isfirst'] and thread__update($tid, array('images' => $images, 'files' => $files));
    post__update($pid, array('images' => $images, 'files' => $files));

    // hook attach_assoc_post_end.php

    return TRUE;
}


// hook model_attach_end.php

?>