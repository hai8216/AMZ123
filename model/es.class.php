<?php

use Elasticsearch\ClientBuilder;

class es
{
    public $client = NULL;
    public $TableName = NULL;

    public function __construct($TableName)
    {
        $hosts = "81.71.13.202:9200";
        $hosts ="172.16.0.14:9200";
        $this->TableName = $TableName;
        $this->client = ClientBuilder::create()->setHosts([$hosts])->build();
    }

    //qingklong


    public function deleteAll()
    {
        $params = [
            'index' => $this->TableName
        ];
        return $this->client->indices()->delete($params);
    }

    //获取Index

    public function getIndex()
    {
        $params = [
            'index' => $this->TableName
        ];
        return $this->client->indices()->exists($params);
    }

    //批量更新
    public function moreUpdate($list)
    {
        foreach ($list as $value) {
//            $params = [
//                'index' => $this->TableName,
//                'id' => md5($value['1']),
//                'body' => [
//
//                    'script' => [
//                        'source' => 'ctx._source.oldRank=ctx._source.newRank'
//                    ],
//                    'upsert' => [
//                        'name' => $value['1'],
//                        'newRank' => $value['2'],
//                        'oldRank' => 0,
//                        'up' => 0
//                    ],
//                ],
//            ];
//            $response = $this->client->update($params);
//            $params = [
//                'index' => $this->TableName,
//                'id' => md5($value['1']),
//                'body' => [
//
//                    'script' => [
//                        'source' => 'ctx._source.newRank=\''.$value['2'].'\''
//                    ],
//                    'upsert' => [
//                        'name' => $value['1'],
//                        'newRank' => $value['2'],
//                        'oldRank' => 0,
//                        'up' => 0
//                    ],
//                ],
//            ];
//            $response = $this->client->update($params);

            $params = [
                'index' => $this->TableName,
                'id' => md5($value['1']),
                'body' => [
                    'script' => [
                        'source' => 'ctx._source.up = params.newRank',
                        'params' => [
                            'newRank' => 'ctx._source.newRank - ctx._source.oldRank'
                        ],
                    ],
                    'upsert' => [
                        'name' => $value['1'],
                        'newRank' => $value['2'],
                        'oldRank' => 0,
                        'up' => 0
                    ],
                ],

            ];
            $response = $this->client->update($params);
        }

//        $response = $this->client->bulk($params);
    }

    public function delIndex()
    {
        $params = [
            'index' => $this->TableName,
            'body' => [
                'query' => [
                    'match_all' => (object)[]
                ]
            ]
        ];

// Delete doc at /my_index/_doc_/my_id
        $response = $this->client->deleteByQuery($params);
    }

    //删除
    public function del($id)
    {
        $params = [
            'index' => $this->TableName,
            'id' => $id
        ];

// Delete doc at /my_index/_doc_/my_id
        $response = $this->client->delete($params);
    }

    //count
    public function count()
    {
        $params = [
            'index' => $this->TableName
        ];
        return $this->client->count($params);
    }

    //批量bucket
    public function moreDate($sqlData)
    {
        $params = [];
        foreach ($sqlData as $k => $value) {
            $params['body'][] = [
                'index' => [
                    '_index' => $this->TableName,
                    '_id' => $value['md5Name']
                ]
            ];
            $params['body'][] = [
                'name' => $value['name'],
                'newRank' => $value['newRank'],
                'oldRank' => $value['oldRank']?$value['oldRank']:"250001",
                'up' =>  $value['oldRank'] - $value['newRank']
            ];
        }
        return $this->client->bulk($params);
    }

    public function search($k, $after, $order,$qj = '', $zf = '',$size='500')
    {
        $ks = [];
        if ($k) {
            $k = urldecode($k);
            $ks['wildcard'] = [
                "name" => "*$k*"
            ];
        }


        if ($qj) {
            list($star, $end) = explode("_", $qj);
            $qjary[]['range'] =
                [
                    "newRank" => [
                        "gte" => $star,
                        "lte" => $end
                    ]

                ];

        }
        if ($zf) {
            list($star, $end) = explode("_", $zf);
            $qjary[]['range'] =
                [
                    "up" => [
                        "gte" => $star,
                        "lte" => $end
                    ]

                ];

        }
//        $after = '';
        $params = [
            'index' => $this->TableName,
            'body' => [
                'from'=>($after-1)*500,
                'size' => $size,
//                'search_after'=>[$after],
                "sort" => [
                    $order => $order=="up"?"desc":"asc"
                ],
                "track_total_hits" => true,
                "query" => [

                    "bool" => [
                        "must" => $ks,
                        "filter" => $qjary,
                    ]
                ]
            ]
        ];
//        print_r($params);
//        exit;
        $results = $this->client->search($params);

        if (is_array($results)) {
            $list = $results['hits']['hits'];
        } else {
            $searchList = [];
        }

        foreach ($list as $k => $v) {
            $searchList[$k]['name'] = $v['_source']['name'];
            $searchList[$k]['newRank'] = $v['_source']['newRank'];
            $searchList[$k]['oldRank'] = $v['_source']['oldRank'];
            $searchList[$k]['up'] = $v['_source']['up'];
        }
        $output['total'] = $results['hits']['total']['value'];
        $output['data'] = $searchList;
//        $output['searchbefore'] = $v['_source'][$order] - count($list)*2;
        $output['searchAfter'] = $v['_source'][$order];
        $list = NULL;
        $searchList = NULL;
        return $output;
    }

    //建表
    public function addIndex()
    {
        $params = [
            'index' => $this->TableName, //索引名称
            'body' => [
                'settings' => [ //配置
                    'number_of_shards' => 5,//主分片数
                    'number_of_replicas' => 1 //主分片的副本数
                ],
                'mappings' => [  //映射
                    '_source' => [   //  存储原始文档
                        'enabled' => 'true'
                    ],
                    'properties' => [ //配置数据结构与类型
                        'name' => [ //字段1
                            'type' => 'keyword',//类型 text、integer、float、double、boolean、date
                            'index' => 'true',//是否索引
                        ],
                        'newRank' => [
                            'type' => 'integer',
                        ],
                        'oldRank' => [
                            'type' => 'integer',
                        ],
                        'up' => [
                            'type' => 'integer',
                        ],
                    ]

                ],
            ]
        ];
        return $this->client->indices()->create($params);
    }
}

//curl -u elastic:pass -XDELETE "http://81.71.13.202:9200/keyword_usa"
