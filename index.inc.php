<?php

//ini_set('display_errors',1); //错误信息
//ini_set('display_startup_errors',1); //php启动错误信息
//error_reporting(E_ALL);
!defined('DEBUG') AND exit('Access Denied.');

// hook index_inc_start.php

$sid = sess_start();

// 语言 / Language
$_SERVER['lang'] = $lang = include _include(APP_PATH . "lang/$conf[lang]/bbs.php");

// 用户组 / Group
$grouplist = group_list_cache();

// 支持 Token 接口（token 与 session 双重登陆机制，方便 REST 接口设计，也方便 $_SESSION 使用）
// Support Token interface (token and session dual match, to facilitate the design of the REST interface, but also to facilitate the use of $_SESSION)
$uid = intval(_SESSION('uid'));
empty($uid) AND $uid = user_token_get() AND $_SESSION['uid'] = $uid;
$user = user_read($uid);

$gid = empty($user) ? 0 : intval($user['gid']);
$group = isset($grouplist[$gid]) ? $grouplist[$gid] : $grouplist[0];

// 版块 / Forum
$fid = 0;
$forumlist = forum_list_cache();
$forumlist_show = forum_list_access_filter($forumlist, $gid);    // 有权限查看的板块 / filter no permission forum
$forumarr = arrlist_key_values($forumlist_show, 'fid', 'name');

// 头部 header.inc.htm 
$header = array(
    'title' => $conf['sitename'],
    'mobile_title' => '',
    'mobile_link' => './',
    'keywords' => '', // 搜索引擎自行分析 keywords, 自己指定没用 / Search engine automatic analysis of key words, so keep it empty.
    'description' => strip_tags($conf['sitebrief']),
    'navs' => array(),
);

// 运行时数据，存放于 cache_set() / runtime data
$runtime = runtime_init();

// 检测站点运行级别 / restricted access
check_runlevel();

// 全站的设置数据，站点名称，描述，关键词
// $setting = kv_get('setting');

$route = param(0);
$banip = kv_get('banid');
$iplist = explode("\r\n",$banip['banip']);
$myip = ip();
if(in_array($myip,$iplist)){
    echo "403";
    exit;
}

if(empty($route)){
    $route = 'index';
}
// hook index_inc_route_before.php
if (!defined('SKIP_ROUTE')) {

    // 按照使用的频次排序，增加命中率，提高效率

    // According to the frequency of the use of sorting, increase the hit rate, improve efficiency
    switch ($route) {

        // hook index_route_case_start.php
        case 'index':
            include _include(APP_PATH . 'route/index.php');
            break;
        case 'thread':
            include _include(APP_PATH . 'route/thread.php');
            break;
        case 'forum':
            include _include(APP_PATH . 'route/forum.php');
            break;
        case 'user':
            include _include(APP_PATH . 'route/user.php');
            break;
        case 'my':
            include _include(APP_PATH . 'route/my.php');
            break;
        case 'attach';
            include _include(APP_PATH . 'route/attach.php');
            break;
        case 'post':
            include _include(APP_PATH . 'route/post.php');
            break;
        case 'mod':
            include _include(APP_PATH . 'route/mod.php');
            break;
        case 'usseller': 	include _include(APP_PATH.'plugin/xl_amazon_usseller/route/index.php'); 	break;
        case 'frseller': 	include _include(APP_PATH.'plugin/xl_amazon_usseller/route/frseller.php'); 	break;
        case 'deseller': 	include _include(APP_PATH.'plugin/xl_amazon_usseller/route/deseller.php'); 	break;
        case 'ukseller': 	include _include(APP_PATH.'plugin/xl_amazon_usseller/route/ukseller.php'); 	break;
        case 'browser':
            include _include(APP_PATH . 'route/browser.php');
            break;
        case 'test':
            include _include(APP_PATH . 'route/test.php');
            break;
        case 'amacate':
            include _include(APP_PATH . 'route/amacate.php');
            break;
        case 'newamacate':
            include _include(APP_PATH . 'route/newamacate.php');
            break;
        case 'calendar':
            include _include(APP_PATH . 'plugin/xl_calendar/route/calendar.php');
            break;
        case 'calendar_ajaxdata':
            include _include(APP_PATH . 'plugin/xl_calendar/route/calendar_ajaxdata.php');
            break;
        case 'category':
            include _include(APP_PATH . 'plugin/xl_category/route/index.php');
            break;
        case 'tag':
            include _include(APP_PATH . 'plugin/xl_threadtag/route/index.php');
            break;
        case 'tagsetting':
            include _include(APP_PATH . 'plugin/xl_threadtag/route/tagsetting.php');
            break;
        case 'tagsearch':
            include _include(APP_PATH . 'plugin/xl_threadtag/route/tagsearch.php');
            break;
        case 'addtags':
            include _include(APP_PATH . 'plugin/xl_threadtag/route/addtags.php');
            break;
        case 'tags':
            include _include(APP_PATH . 'plugin/xl_threadtag/route/tags.php');
            break;
        case 'keys':
            include _include(APP_PATH . 'plugin/xl_keywordscheck/route/index.php');
            break;
        case 'author':
            include _include(APP_PATH . 'plugin/xl_threadtag/route/author.php');
            break;
        case 'searchproducts':
            include _include(APP_PATH . 'plugin/xl_category/route/searchproducts.php');
            break;
        case 'tiktokdown':
            include _include(APP_PATH . 'plugin/xl_videourl/route/videourl.php');
            break;
        case 'postsellers':
            include _include(APP_PATH . 'plugin/xl_wxpachong/route/postsellers.php');
            break;
        case 'usecode':
            include _include(APP_PATH . 'plugin/xl_CloudChrome/route/usecode.php');
            break;
        case 'guarantee':
            include _include(APP_PATH . '/plugin/xl_guarantee/route/guarantee.php');
            break;
        case 'guaranteeajax':
            include _include(APP_PATH . '/plugin/xl_guarantee/route/guaranteeajax.php');
            break;
        //支付宝认证回调
        case 'ailirealv':
            include _include(APP_PATH . 'plugin/xl_realv/route/ailirealv.php');
            break;
        case 'verifyurl':
            include _include(APP_PATH . 'plugin/xl_realv/route/verifyurl.php');
            break;
        case 'mjzj':
            include _include(APP_PATH . 'plugin/xl_realv/route/mjzj.php');
            break;
        case 'getKeywrods':
            include _include(APP_PATH . 'plugin/xl_viewtools/route/getAjax.php');
            break;
        case 'live': include _include(APP_PATH.'plugin/xl_live/route/live.php'); break;
        case 'liveroom': include _include(APP_PATH.'plugin/xl_live/route/liveroom.php'); break;
        //黑名单发布
        case 'automjzj':
            include _include(APP_PATH . 'plugin/xl_mjzj/route/automjzj.php');
            break;
        case 'blackpost':
            include _include(APP_PATH . 'plugin/xl_mjzj/route/blackpost.php');
            break;
        case 'autocomment':
            include _include(APP_PATH . 'plugin/xl_blackplus/route/autocomment.php');
            break;
        //国家单页
        case 'usatop':
            include _include(APP_PATH . 'plugin/xl_top/route/usatop.php');
            break;
        case 'digg': include _include(APP_PATH.'/plugin/xl_digg/router/digg.php'); break;

//        case 'usatopkeywords': include _include(APP_PATH.'plugin/xl_top/route/usatopkeywords.php'); break;
//        case 'uktopkeywords': include _include(APP_PATH.'plugin/xl_top/route/uktopkeywords.php'); break;
//
//        case 'frtopkeywords': include _include(APP_PATH.'plugin/xl_top/route/frtopkeywords.php'); break;
//
//        case 'detopkeywords': include _include(APP_PATH.'plugin/xl_top/route/detopkeywords.php'); break;
//
//        case 'jptopkeywords': include _include(APP_PATH.'plugin/xl_top/route/jptopkeywords.php'); break;
//
//
        case 'mnreg': include _include(APP_PATH.'/plugin/xl_mnreg/index.php'); break;
        case 'francetop':
            include _include(APP_PATH . 'plugin/xl_top/route/francetop.php');
            break;
        case 'japantop':
            include _include(APP_PATH . 'plugin/xl_top/route/japantop.php');
            break;
        case 'uktop':
            include _include(APP_PATH . 'plugin/xl_top/route/uktop.php');
            break;
        case 'germanytop':
            include _include(APP_PATH . 'plugin/xl_top/route/germanytop.php');
            break;
        case 'italytop':
            include _include(APP_PATH . 'plugin/xl_top/route/italytop.php');
            break;
        case 'spaintop':
            include _include(APP_PATH . 'plugin/xl_top/route/spaintop.php');
            break;


        case 'usatopkeywords':
            include _include(APP_PATH . 'plugin/xl_top/route/usatopkeywords.php');
            break;
        case 'ittopkeywords':
            include _include(APP_PATH . 'plugin/xl_top/route/ittopkeywords.php');
            break;
        case 'uktopkeywords':
            include _include(APP_PATH . 'plugin/xl_top/route/uktopkeywords.php');
            break;
        case 'esptopkeywords':
            include _include(APP_PATH . 'plugin/xl_top/route/esptopkeywords.php');
            break;
        case 'frtopkeywords':
            include _include(APP_PATH . 'plugin/xl_top/route/frtopkeywords.php');
            break;

        case 'detopkeywords':
            include _include(APP_PATH . 'plugin/xl_top/route/detopkeywords.php');
            break;

        case 'jptopkeywords':
            include _include(APP_PATH . 'plugin/xl_top/route/jptopkeywords.php');
            break;
        case 'mxtopkeywords': include _include(APP_PATH.'plugin/xl_top/route/mxtopkeywords.php'); break;
        case 'catopkeywords': include _include(APP_PATH.'plugin/xl_top/route/catopkeywords.php'); break;
        //问题
        case 'ask':
            include _include(APP_PATH . 'plugin/xl_ask_page/route/ask.php');
            break;
        case 'asks':
            include _include(APP_PATH . 'plugin/xl_ask_page/route/index.php');
            break;
        //
        case 'prouser':
            include _include(APP_PATH . 'plugin/xl_findpage/router/prouser.php');
            break;
        case 'fuwu':
            include _include(APP_PATH . 'plugin/xl_findpage/router/fuwu.php');
            break;
        case 'wuliu':
            include _include(APP_PATH . 'plugin/xl_findpage/router/wuliu.php');
            break;
        case 'haiwaicang':
            include _include(APP_PATH . 'plugin/xl_findpage/router/haiwaicang.php');
            break;
        case 'chanpin':
            include _include(APP_PATH . 'plugin/xl_findpage/router/chanping.php');
            break;
        case 'people': include _include(APP_PATH.'plugin/xl_renwu/people.php'); break;
        //amazon rank
//        case 'amazon_test': include _include(APP_PATH.'plugin/xl_amazon_rankdata/route/test.php'); break;
//        case 'amazon_test2': include _include(APP_PATH.'plugin/xl_amazon_rankdata/route/test2.php'); break;
        case 'bsr': include _include(APP_PATH.'plugin/xl_amazon_rankdata/route/rank.php'); break;
//        case 'amazon_post': include _include(APP_PATH.'plugin/xl_amazon_rankdata/route/pachong.php'); break;
//        case 'amazon_biaosheng_post': include _include(APP_PATH.'plugin/xl_amazon_biaosheng/route/pachong.php'); break;
//        case 'moversshakers': include _include(APP_PATH.'plugin/xl_amazon_biaosheng/route/rank.php'); break;

        case 'mip': include _include(APP_PATH.'plugin/tt_seo/mip.php'); break;
        case 'funding': include _include(APP_PATH.'plugin/xl_funding/funding.php'); break;
        case 'shangshi': include _include(APP_PATH.'plugin/xl_funding/shangshi.php'); break;
        case 'damaijia': include _include(APP_PATH.'plugin/xl_funding/damaijia.php'); break;
        case 'qinquan': include _include(APP_PATH.'plugin/xl_qinquan/index.php'); break;
        case 'brands': include _include(APP_PATH.'plugin/xl_keywordscheck/shangbiaoku.php'); break;
        case 'youhui': include _include(APP_PATH.'plugin/xl_yhjingxuan/youhui.php'); break;
        case 'video': include _include(APP_PATH.'plugin/xl_video/index.php'); break;
        case 'v': include _include(APP_PATH.'plugin/xl_video/view.php'); break;

        //肺炎

        case 'covid2019':
            include _include(APP_PATH . 'plugin/xl_viewtools/route/covid.php');
            break;
        case 'shopeefetch':
            include _include(APP_PATH . 'plugin/xl_viewtools/route/fetch.php');
            break;

        //tools_page
        case 'toolspage':
            include _include(APP_PATH . 'plugin/xl_tools_pages/ajax.php');
            break;

        //ax_post_list
        case 'ax_post_list':
            include _include(APP_PATH . 'plugin/ax_posts_list/route/ajax.php');
            break;
        case 'imgupload': include _include(APP_PATH.'plugin/xl_xcxapi/route/upload.php'); break;
        case 'form_model': include _include(APP_PATH.'/plugin/plugin_form/route/model.php'); break;
        case 'form_data': include _include(APP_PATH.'/plugin/plugin_form/route/data.php'); break;
        case 'form_index': include _include(APP_PATH.'/plugin/plugin_form/route/index.php'); break;

        case 'thwords':
            include _include(APP_PATH . 'plugin/xl_top/route/thwords.php');
            break;

        case 'pinpai':
            include _include(APP_PATH . 'plugin/xl_viewtools/route/pinpai.php');
            break;
        case 'bankajax':
            include _include(APP_PATH . 'plugin/xl_viewtools/route/bankajax.php');
            break;

        case 'getDiyajaxdata':
            include _include(APP_PATH . 'plugin/xl_index_diy/route/getDiyajaxdata.php');
            break;
        case 'indexdiyAjax':
            include _include(APP_PATH . 'plugin/xl_index_diy/route/indexdiy.php');
            break;
        case 'wechat_union':
            include _include(APP_PATH . 'plugin/xl_wechat_union/route/wechat.php');
            break;
        case 'autoTimepost':
            include _include(APP_PATH . 'plugin/xl_autoTime/route/autoTimepost.php');
            break;
        case 'company':
            include _include(APP_PATH . 'plugin/xl_company_amz/route/company.php');
            break;

        case 'imgcount':
            include _include(APP_PATH . 'route/imgcount.php');
            break;
        case 'autoTime':
            include _include(APP_PATH . 'plugin/xl_autoTime/route/autoTime.php');
            break;
        // hook index_route_case_end.php
        default:
            // hook index_route_case_default.php
//            include _include(APP_PATH . 'route/index.php');
//            break;/
            http_404();
        /*
        !is_word($route) AND http_404();
        $routefile = _include(APP_PATH."route/$route.php");
        !is_file($routefile) AND http_404();
        include $routefile;
        */
    }
}

// hook index_inc_end.php

?>