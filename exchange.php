<?php
/**
 * Created by xuxiaolong.
 * weibo:@Sinamfyoyo
 * User: 许小龙
 * Date: 2018-09-13
 * Time: 10:25
 * Developer:商业定制联系QQ95327294
 */

//xhprof_enable();

//$_SERVER['REQUEST_URI'] = '/?user-login.htm';
//$_SERVER['REQUEST_METHOD'] = 'POST';
//$_SERVER['HTTP_X_REQUESTED_WITH'] = 'xmlhttprequest';
//$_COOKIE['bbs_sid'] = 'e1d8c2790b9dd08267e6ea2595c3bc82';
//$postdata = 'email=admin&password=c4ca4238a0b923820dcc509a6f75849b';
//parse_str($postdata, $_POST);

// 0: Production mode; 1: Developer mode; 2: Plugin developement mode;
// 0: 线上模式; 1: 调试模式; 2: 插件开发模式;
!defined('DEBUG') and define('DEBUG', 0);
define('APP_PATH', dirname(__FILE__) . '/'); // __DIR__
!defined('ADMIN_PATH') and define('ADMIN_PATH', APP_PATH . 'admin/');
!defined('XIUNOPHP_PATH') and define('XIUNOPHP_PATH', APP_PATH . 'xiunophp/');

// !ini_get('zlib.output_compression') AND ob_start('ob_gzhandler');

//ob_start('ob_gzhandler');
$conf = (@include APP_PATH . 'conf/conf.php') or exit('<script>window.location="install/"</script>');

// 兼容 4.0.3 的配置文件
!isset($conf['user_create_on']) and $conf['user_create_on'] = 1;
!isset($conf['logo_mobile_url']) and $conf['logo_mobile_url'] = 'view/img/logo.png';
!isset($conf['logo_pc_url']) and $conf['logo_pc_url'] = 'view/img/logo.png';
!isset($conf['logo_water_url']) and $conf['logo_water_url'] = 'view/img/water-small.png';
$conf['version'] = '4.0.4';        // 定义版本号！避免手工修改 conf/conf.php

// 转换为绝对路径，防止被包含时出错。
substr($conf['log_path'], 0, 2) == './' and $conf['log_path'] = APP_PATH . $conf['log_path'];
substr($conf['tmp_path'], 0, 2) == './' and $conf['tmp_path'] = APP_PATH . $conf['tmp_path'];
substr($conf['upload_path'], 0, 2) == './' and $conf['upload_path'] = APP_PATH . $conf['upload_path'];
//print_r($_GET);exit;
$_SERVER['conf'] = $conf;

if (DEBUG > 1) {
    include XIUNOPHP_PATH . 'xiunophp.php';
} else {
    include XIUNOPHP_PATH . 'xiunophp.min.php';
}


if (param('api')) {
    $data['getTime'] = date('Y-m-d H:i:s', time());
    $rate = db_find_one('exchange', ["m"=>[">"=>0]],["id"=>'-1']);
    $data['dataList'] = [
        [
            "rate" => $rate['m'],
            "moneyName" => "美元"
        ],
        [
            "rate" => $rate['y'],
            "moneyName" => "英镑"
        ],
        [
            "rate" => $rate['o'],
            "moneyName" => "欧元"
        ],
        [
            "rate" => $rate['r'],
            "moneyName" => "日元"
        ],
        [
            "rate" => $rate['j'],
            "moneyName" => "加元"
        ],
        [
            "rate" => $rate['od'],
            "moneyName" => "澳大利亚元"
        ],
    ];
    echo json_encode($data);exit;
} else {


    $exchangeApiData = file_get_contents('http://web.juhe.cn:8080/finance/exchange/rmbquot?bank=2&key=fc248046d9b18020a5a74afb9b88be5c');
    $exchangeApiData = json_decode($exchangeApiData, true);
    $exchangeApiData = $exchangeApiData['result'][0];
    $data['m'] = $exchangeApiData['data1']['bankConversionPri'] / 100;
    $data['y'] = $exchangeApiData['data5']['bankConversionPri'] / 100;
    $data['o'] = $exchangeApiData['data2']['bankConversionPri'] / 100;
    $data['r'] = $exchangeApiData['data4']['bankConversionPri'] / 100;
    $data['j'] = $exchangeApiData['data7']['bankConversionPri'] / 100;
    $data['hk'] = $exchangeApiData['data3']['bankConversionPri'] / 100;
    $data['od'] = $exchangeApiData['data6']['bankConversionPri'] / 100;
    $data['dateline'] = time();
    db_insert('exchange', $data);


    $huilvdata = https_get('https://api.exchangerate-api.com/v4/latest/CNY');
    $huilvdata = json_decode($huilvdata, true);
    $huilv = $huilvdata['rates'];
    db_insert("domain_huilv", ["data" => serialize($huilv), "dateline" => time()]);
}
