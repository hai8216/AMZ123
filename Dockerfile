FROM php:7.4-fpm

RUN sed -i "s@http://\(deb\|security\).debian.org@https://mirrors.aliyun.com@g" /etc/apt/sources.list
RUN apt-get update -y \
    && apt-get install -y nginx \
    && apt-get install -y libzip-dev

# PHP_CPPFLAGS are used by the docker-php-ext-* scripts
ENV PHP_CPPFLAGS="$PHP_CPPFLAGS -std=c++11"

RUN docker-php-ext-install pdo_mysql

RUN pecl install redis && docker-php-ext-enable redis
RUN pecl install zip && docker-php-ext-enable zip

WORKDIR /www
COPY ./ /www
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./php.ini /usr/local/etc/php/php.ini
RUN mkdir "tmp"
RUN mkdir "nginx_log"
RUN chmod 777 -R /www
EXPOSE 8443
ENTRYPOINT ["./run.sh"]
