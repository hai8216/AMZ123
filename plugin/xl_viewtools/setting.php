<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_viewtools';
$action = param(3) ?: "list";
if ($action == 'list') {
    $tools_page_list = glob(APP_PATH . 'plugin/xl_viewtools/htm/*.htm');
    foreach ($tools_page_list as $k => $v) {
        $value = str_replace(".htm", "", str_replace(APP_PATH . "plugin/xl_viewtools/htm/", "", $v));
        $dirsd[$value] = $value;

    }

    $list = db_find("viewtools_seo", array(), array('id' => '-1'), 1, 500);
    $listData = array();
    foreach ($list as $v) {
        $listData[$v['viewname']] = $v;
    }
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/list.htm');
} elseif ($action == 'edit') {
    if ($method == 'GET') {
        $val = array();
        $viewpage = param(4);
        if ($viewpage) {
            $val = db_find_one("viewtools_seo", array('viewname' => $viewpage));
        } else {
            $val = array("title" => '', "keywords" => '', "description" => '');
        }
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/edit.htm');
    } else {

        $viewpage = param('viewpage');
        $val = db_find_one("viewtools_seo", array('viewname' => $viewpage));
        if ($val['id']) {
            db_update("viewtools_seo", array('viewname' => $viewpage), array("title" => param("title"), "keywords" => param("keywords"), "description" => param("description")));
            message(0, 'ok');
        } else {
            $insrt = array(
                "title" => param("title"),
                "keywords" => param("keywords"),
                "description" => param("description"),
                "viewname"=>$viewpage
            );
            db_insert("viewtools_seo", $insrt);
            message(0, 'ok');
        }
    }
}
?>