var CookieUtility = {};
CookieUtility.getCookie = function(name) {
    var array = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
    if (array != null) {
        return unescape(array[2]);
    }
    return null;
}
CookieUtility.setCookie = function(name, value, expire, path, domain) {
    var expireString = "";
    if (expire != undefined) {
        var date = new Date();
        date.setTime(date.getTime() + expire * 1000);
        expireString = "expires=" + date.toGMTString() + ";"
    }
    var pathString = "";
    if (path != undefined) {
        pathString = "path=" + path + ";"
    }
    var domainString = "";
    if (domain != undefined) {
        domainString = "domain=" + domain + ";"
    }
    document.cookie = name + "=" + escape(value) + ";" + expireString + pathString + domainString;
}
CookieUtility.delCookie = function(name) {
    var date = new Date();
    date.setTime(date.getTime() - 1);
    var value = getCookie(name);
    if (value != null) {
        document.cookie = name + "=" + value + ";expires=" + date.toGMTString();
    }
}
