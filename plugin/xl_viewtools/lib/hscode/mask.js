
function showMask(){	
	if($("body").find("div.bg").length == 0){		
		var arVersion = navigator.appVersion.split("MSIE") 
		var version = parseFloat(arVersion[1]) 
		if ((version <7) && (version >= 5.5)) // && (document.body.filters)
		{ 	
			$("body").css("height","100%");//解决IE6下只显示一条线问题
		}
	
		var bgIframeElements = '<div class="bg" style="background-color:#666;filter:alpha(opacity=30); opacity:0.3;width:100%; height:100%;z-index:100;position:fixed;_position:absolute;left:0; top:0;"></div>' +
								'<iframe class="popIframe" frameborder="0" style="background-color:#666;filter:alpha(opacity=30); opacity:0.3;width:100%; height:100%;z-index:100;position:fixed;_position:absolute;left:0; top:0;right:0;botton:0"></iframe>';		
		$(bgIframeElements).appendTo("body");	
		$(".bg,.popIframe").show();
	}else if($("body").find("div.bg").is(":hidden")){
		$(".bg,.popIframe").show();
	}
}

function closeMask(){
	if($("body").find("div.bg").length > 0 ){
		$(".bg,.popIframe").remove();
	}
}
