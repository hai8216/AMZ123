function searchEvent(event) {
	if (event.keyCode == 13) {
		searchHsCode();
	}
}

/**
 * 点击排序
 * 
 * @param orderName
 * @return
 */
function goOrder(orderName) {
	if ($("#orderName").val() == orderName) {
		if ($("#orderBy").val() == '0') {
			$("#orderBy").val(1);
		} else {
			$("#orderBy").val(0);
		}
	} else {
		$("#orderName").val(orderName);
		$("#orderBy").val(0);
	}
	go($("#pageIndexH").val());
}

/**
 * 精确查询
 */
function exactSearch() {
	if ($("#isExact").attr("checked")) {
		$("#isExactH").val(1);
	} else {
		$("#isExactH").val(0);
	}
	searchHsCode();
}

/**
 * 页码跳转
 * 
 * @param pageIndex
 * @return
 */
function go(pageIndex) {
	var regex = /^-?\d+$/;
	if (!regex.test(pageIndex)) {
		alert("页码只能输入数字");
		return;
	} else {
		$("#nameH").val(encodeURI($("#nameH").val()));
		$("#pageIndexH").val(pageIndex);
		$("#searchHsCodeForm").submit();
	}
};
var originalGo = go;
/**
 * 搜索查询
 * 
 * @return
 */
function searchHsCode() {
	var code = $.trim($("#code").val());
	var name = $.trim($("#name").val());

	code = code == "输入4-10位HS编码" ? "" : code;
	name = name == "输入1个汉字或以上的品名，智能匹配HSCODE" ? "" : name;
	
	if ((code == "") && (name == "")) {
		window.alert("请输入查询信息");
		return false;
	} else {
		if(code.length > 0 && isNaN(code)){
			window.alert("请输入正确的HS编码");
			return false;
		}
		if(code.length > 0 && code.length < 4){
			window.alert("至少输入4-10位HS编码");
			return false;
		}
	}

	$("#nameH").val(encodeURI(name));
	$("#codeH").val(code);
	$("#pageIndexH").val(1);
	$("#searchHsCodeForm").submit();
}

function setPageSize() {
	$("#pageSizeH").val($("#pageSize").val());
	go($("#pageIndexH").val());
}

function loadInstanceList(code, pageIndex) {
	var url = basePath + "/tools/hscode/loadInstanceList.html";
	$.ajax({
		url : url,
		data : {
			code : code,
			pageIndex : pageIndex
		},
		cache : false,
		async : true,
		success : function(html) {
			$("#instanceContent").html(html);
		},
		error : function() {
			$("#instanceContent").html("数据加载失败，请联系客服");
		},
		beforeSend : function() {
			$("#instanceContent").html("正在加载实例汇总，请稍后...");
		}
	});
	showInstanceWindow();
}

function showInstanceWindow() {
	document.getElementById('popDivInstance').style.display = 'block';
	document.getElementById('popIframeInstance').style.display = 'block';
	document.getElementById('bgInstance').style.display = 'block';
	showMask();
}

function closeInstanceWindow() {
	document.getElementById('popDivInstance').style.display = 'none';
	document.getElementById('bgInstance').style.display = 'none';
	document.getElementById('popIframeInstance').style.display = 'none';
	go = originalGo;
	closeMask();
}

function collect(caseHsId){
	var event = window.event || arguments.callee.caller.arguments[0];
	var srcObj = event.srcElement || event.target;
	$.ajax({
		url : basePath + "/tools/ajaxCollectCaseHs.html",
		data : {
			caseHsId : caseHsId
		},
		cache : false,
		async : true,
		success : function(html) {
			if(html != "false"){
				$(".collectlist").append("<li>"+($(".collectlist li").length + 1) + "." + html +"</li>");				
				//$(srcObj).parent().attr("onclick", "");
				$(srcObj).attr("class", "");
				$(srcObj).parent().parent().attr("class", "collectbgon");
				$(srcObj).parent().next()
					.fadeIn("slow")
					.fadeOut(2000, function(){   
					   //隐藏时把元素删除   
					  $(srcObj).parent().parent().html("收藏");
					  $(this).remove();   					   
					}); 
			}
		},
		error : function() {
			window.alert("编码收藏失败");
		},
		beforeSend : function() {
		}
	});
}
