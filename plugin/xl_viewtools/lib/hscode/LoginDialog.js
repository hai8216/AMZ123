// JavaScript Document
/*
popdialogid:对话框提示
popTip： 初始提示语；
postUrl:提交popURL
postBackUrl:返回刷新url
*/
function popLogin(popdialogid,popTip,postUrl,postBackUrl,ifrefresh)
{
var popid="popdialog";
var _poptip="";
var _postUrl="#";
var _ifrefresh=true;

if(typeof popdialogid != 'undefined'&&popdialogid!="") popid=popdialogid;
if(typeof popTip != 'undefined'&&popTip!="") _poptip=popTip;
if(typeof postUrl != 'undefined'&&postUrl!="") _postUrl=postUrl;
if(typeof ifrefresh =="boolean") _ifrefresh=ifrefresh;

var html1 = [];
html1.push('<form id="loginForm" method="post" action="'+_postUrl+'">');
html1.push('<div class="loginbox" id="'+popid+'">');
html1.push('  <div class="boxwrap">');
html1.push('    <div class="boxtop"> ');
html1.push('    <span class="headleft">已经是会员？立即登录</span> ');
html1.push('    <span onclick="$(&quot;#loginForm&quot;).remove();closeMask();" class="headright">关闭</span>');
html1.push('    <div class="clear"></div>');
html1.push('    </div>');
html1.push('    <div class="wrapper">');
html1.push('      <div class="wrapleft">');
html1.push('      <span class="logingrey"> <a href="'+ basePath +'/register.html">免费注册</a></span>');
html1.push('        <ul>');
html1.push('          <li><span class="loginname">&nbsp;</span><span id="propmt" class="loginwarm">'+_poptip+'</span></li>');
html1.push('          <li><span class="loginname">用户名</span>');
html1.push('            <input type="text" onkeypress="enterSubmit(event)" value="" id="loginName" name="loginName" class="loginput">');
html1.push('          </li>');
html1.push('          <li><span class="loginname">密码</span>');
html1.push('            <input type="password" onkeypress="enterSubmit(event)" value="" id="loginPwd" name="loginPwd" class="loginput">');
html1.push('          </li>');
html1.push('          <li class="warmli"><span class="loginname">&nbsp;</span></li>');
html1.push('          <li class="warmli"><span class="loginname">&nbsp;</span>');
html1.push('          <span ><a onclick="validateLogin('+_ifrefresh+',&quot;'+popid+'&quot;)" href="javascript:;"><img src="images/tools/hscode/loginbtn.gif" width="104" height="35" border="0"/></a></span></li>');
html1.push('        </ul>');
html1.push('      </div>');
html1.push('      <div class="clear"></div>');
html1.push('    </div>');
html1.push('    <div class="boxfoot"> <span>登录遇到问题？请拨打服务热线：0512-36809501<input name="auth" type="hidden" value="1"><input name="toUrl" type="hidden" value="'+postBackUrl+'"></span>');
html1.push('      <div class="clear"></div>');
html1.push('    </div>');
html1.push('    <div class="clear"></div>');
html1.push('  </div>');
html1.push(' </div>');
html1.push('</form>');	
var  out_html= html1.join('\n');	 
$("body").append(out_html);

var arVersion = navigator.appVersion.split("MSIE") 
var version = parseFloat(arVersion[1]) 
if ((version <8) && (version >= 5.5))
{ 	
 $("html, body").animate({ scrollTop: 0 }, 120);
}

showMask();		
	
}


function enterSubmit(event)
{
	if(event.keyCode == 13){
	validateLogin(true,"");
	}
} 

function validateLogin(ifrefresh,popid) {
	var loginName = $("#loginName").val();
	var loginPwd = $("#loginPwd").val();

	if ($.trim(loginName).length == 0) {
	$("#propmt").html("请输入用户名");
	$("#loginName").focus();
	return false;
	}
	if (loginPwd.length == 0) {
	$("#propmt").html("请输入密码");
	$("#loginPwd").focus();
	return false;
	}	

	$("#loginForm").ajaxSubmit({
		dataType : "json",
		success : function(html) {
			if (html.msg != "ok") {
				if (html.msg == '密码错误') {
					$("#loginPwd").select();
				} else {
					$("#loginName").select();
				}
				$("#propmt").html(html.msg);
			} else {
				var postBackUrl = $("input[name='toUrl']").val();
				if(postBackUrl!=undefined&&postBackUrl.length > 0){
					window.location.href = basePath + "/" + postBackUrl;
				}else {
				  if(ifrefresh)
				  {
					  window.location.reload();
				  }
				//  else
//				  {
//					$("#a_hslogin").replaceWith('<div class="div_userplace">下午好！'+loginName+', <a href="/" target="_blank" class="alikecha">[我的立刻查]</a><a href="#" onclick="ajax_logout();return false;">[退出]</a></div>');  
//					$("#"+popid).remove();closeMask();
//				  }
				}
			}
		},
		error : function() {
			alert('登录时发生异常');
		}
	});
}


function ajax_logout()
{
	$(".div_userplace").replaceWith("<a onclick=\"popLogin('','登录立刻查！','user/doLogin.html','',false);return false;\" href='javascript:;' id='a_hslogin'><div  class='div_hslogin'>登录立刻查</div></a>");
}