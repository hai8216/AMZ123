/***--------------Global Project level variable--------------------------------***/
var basePath = ""; //定义项目的ContextPath


String.prototype.trim = function(){
     //用正则表达式将前后空格  用空字符串替代。
    return  this.replace(/(^\s*)|(\s*$)/g,"");
};

String.prototype.repeat=function (n) {
	//n表示字符串重得的次数
	return new Array(n+1).join(this);
};

Number.prototype.inter=function (a,b) {//检测数字是否在区间之内
	var min=Math.min(a,b),max=Math.max(a,b);
	return this==a || this==b || (Math.max(this,min)==this && Math.min(this,max)==this);
};

var K = {

    log: function(){
		var msg = '[jabari-customize:Javascript Console Log] ' + Array.prototype.join.call(arguments,'');
	        /*
			var a = window.console, b = arguments;
	        if (a && a.log) {
	            a.log.apply ? a.log.apply(a, b) : a.log(b);
	        }
			*/
        if (window.console && window.console.log) {
			window.console.log(msg);
		}
		else if (window.opera && window.opera.postError) {
			window.opera.postError(msg);
		}
    },
	dir: function(){
        if (window.console && window.console.dir) {
			window.console.dir(arguments[0]);
		}
	}
    
};

function isValidEmail(strEmail) 
{
	if(typeof emailRegEx == 'undefined' || null == emailRegEx){
		emailRegEx = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	}
	if(strEmail.search(emailRegEx) != -1)
		return true;
	else
		false;
}

/**
 * 保留两位小数
 * @param number
 * @returns 保留两位小数后的数字
 */
function toDecimal(number) {  
    var f = parseFloat(number);  
    if (isNaN(f)) {  
        return;  
    }  
    f = Math.round(number*100)/100;  
    return f;  
}  

/**
 * 验证正浮点数，小数部分最多保留x位小数。
 * @param  valNum - 原值。
 * @param x - 小数部分保留的位数。
 */
function validateFloatWithXBitInFractionalPart(valNum,x){
//	var positiveFloatRegExp = /^(?:[1-9][0-9]*(?:\.[0-9]{0,2})?|0\.(?!0+$)[0-9]+)$/;   //正浮点数 
	var positiveFloatRegExp = new RegExp("^(?:[1-9][0-9]*(?:[\.][0-9]{0,"+ x + "})?|0[\.][0-9]{0,"+ x + "})$","g"); 
	//K.log("[validateFloatWithXBitInFractionalPart]reg="+positiveFloatRegExp.source);
	if ($.trim(valNum) == '' || positiveFloatRegExp.test(valNum)) {
		return true;
	}
	return false;
}
/*var oneValue = "0.0002";
alert("oneValue="+oneValue+"\n"+validateFloatWithXBitInFractionalPart(oneValue,4));*/
/*---------------  去除HTML 标签 ----------------*/
String.prototype.stripHTML = function(){
   var reTag = /<(?:.|\s)*?>/g;
   return this.replace(reTag,"");
};
/*
function removeHTMLTag(str) {
        str = str.replace(/</?[^>]*>/g,''); //去除HTML tag
        str = str.replace(/[ | ]*n/g,'n'); //去除行尾空白
        //str = str.replace(/n[s| | ]*r/g,'n'); //去除多余空行
        str=str.replace(/&nbsp;/ig,'');//去掉&nbsp;
        return str;
}
*/

/* -判断字符是否为中文字符- */
function isChinese(curChar)
{
   var chineseReg = /[^\u4E00-\u9FA5]/g;
   return !chineseReg.test(curChar);
}

/**
 * 判断字符串有没有超出最大长度
 * @param strVal - 字符串
 * @param maxLen - 最大长度
 */
function checkStrLength(strVal,maxLen)
 {
     var enNum = 0;//英文字符数
 	 var chNum = 0;//中文字符数
     var newarry = strVal.split("");
     for(var i = 0; i < newarry.length; i++)
     {
         if(isChinese(newarry[i]))
         {
             chNum = parseInt(chNum,10) + 2;
         }
         else
         {
             enNum = parseInt(enNum,10) + 1;
         }
     }
     if(enNum > maxLen || chNum > maxLen || (enNum+chNum) > maxLen )
     {
         //alert("你的字符终于超了");
         return false;
     }
     return true;
 }
 
 /* 计算字符个数，包括中文 */
 function countCharacters(str){
    var totalCount = 0;  
    for (var i=0; i<str.length; i++) {  
        var c = str.charCodeAt(i);  
    if ((c >= 0x0001 && c <= 0x007e) || (0xff60<=c && c<=0xff9f)) {  
           totalCount++;  
        }else {     
            totalCount+=2;  
        }  
    }
    return totalCount;
}


/**
 * 计算字符的实际长度（含中文GBK、UTF8格式）
 **/
function calStrRealLength(str,charset){
    var realLength = 0;
    var len = str.length;
    var charCode = -1;
    for(var i = 0; i < len; i++){
        charCode = str.charCodeAt(i);
        if(charCode >= 0 && charCode <= 128) { 
            realLength += 1;
        }else{ 
        	 //如果是中文GBK字符集,则长度加2
        	if("GBK" == charset){
        		realLength += 2;
        	}
        	//如果是中文UTF8字符集,则长度加3
            else if("UTF8" == charset){
            	realLength += 3;
            }
            
        }
    } 
    return realLength;
}

/**
 * String 添加原型方法maxLenCharDisplay()，来根据指定的最大长度，显示最多显示什么内容。
 * @param {} maxLength - 显示的最大长度
 * @param {} overMark - 超出最大长度后显示的字符
 * @return {String}
 */
String.prototype.maxLenCharDisplay = function(maxLength,overMark){ 
	var str = this;
	if(typeof str == 'undefined' || null == str || "" == str || "" == str.trim()){
		 return ""; 
	}else{   
	    var tempSubLength = maxLength;  
	    var subStrContent = str.substring(0, str.length<maxLength ? str.length : maxLength);
	    var subStrBytesLen = calStrRealLength(subStrContent,"GBK");//GBK格式的长度  
	    while(subStrBytesLen > tempSubLength){    
	        var maxLengthTemp = --maxLength;  
	        subStrContent = str.substring(0,maxLengthTemp>str.length ? str.length : maxLengthTemp);    
	        subStrBytesLen = calStrRealLength(subStrContent,"GBK");  
	    }
	    if(subStrContent.length < str.length){
	    	subStrContent = subStrContent.concat(overMark);
	    }
	    return subStrContent;   
	}  
}  

//alert("I'm 中国人。".maxLenCharDisplay(5,"..."));

	var Browser={
		isIE:!!window.ActiveXObject,
		isOpera:window.opera+"" =="[object Opera]"
	};
	
	var Base={};
	
	Base.extend=function (src,dest) {
		for (var i in src) {
			if (src[i]!=undefined) {
				dest[i]=src[i];
			}
		}
	};
	
	Base.init=function (Class,$this,args) {
		$this.originalArgs=args;
		for (var i in args) {
			$this[i]=args[i];
		}
		if (Class.defaultArgs) {
			for (i in Class.defaultArgs) {
				if (args[i]===undefined)
					$this[i]=Class.defaultArgs[i].valueOf($this);
			}
		}
		
		
	};
		
	Base.prefix=function (n,pos) {
		//接受一个数字，加前导0
		//pos 表示当数字少于pos位的时候，加前导0
		n=""+n;
		//n=12,pos=4; 0012
		if (n.length<pos) {
			n="0".repeat(pos-n.length)+n;
		}
		return n;
	};



	function _$(id) {
		return document.getElementById(id);
	}


	function getByClass(className,context) {
		context=context || document;
		if (context.getElementsByClassName) {
			return context.getElementsByClassName(className);
		}
		var nodes=context.getElementsByTagName('*'),
				ret=[];
		for (var i=0;i<nodes.length;i++) {
			if (hasClass(nodes[i],className)) ret.push(nodes[i]);
		}
		return ret;
	}


	function hasClass(node,className) {
		var names=node.className.split(/\s+/);
		for (var i=0;i<names.length;i++) {
			if (names[i]==className) return true;
		}
		return false;
	}

	function addClass(o,className) {
		if (!hasClass(o,className)) o.className += " "+className;
		return o;
	}
	function invoke(methodName,context) {
		var args=arguments;
		return function () {
			context[methodName].apply(context,[].slice.call(args,2));
		};
	}
	//invoke(fn,obj,1,2,3) 执行fn函数，并将obj设成fn内部的this指向的对象
	//1,2,3....都是传给fn的参数



	function delClass(o,className) {
		var names=o.className.split(/\s+/);
		for (var i=0;i<names.length;i++) {
			if (names[i]==className) delete names[i];
		}
		o.className=names.join(" ");
		return o;
	}




	function addEvent(obj,evt,fn) {
		if (obj.addEventListener && !Browser.isOpera) {
			obj.addEventListener(evt,fn,false);
			return obj;
		}
			
			
		if (!obj.functions) obj.functions={};
		if (!obj.functions[evt])
			obj.functions[evt]=[];
			
			//obj.functions["mousedown"]=[]
		var functions=obj.functions[evt];
		for (var i=0;i<functions.length;i++) {
			if (functions[i]===fn) return obj;
		}
		functions.push(fn);
		//fn.index=functions.length-1;
		
		
		if (typeof obj["on"+evt]=="function") {
			//alert(obj["on"+evt]);//当这一行执行到时，obj["on"+evt] 就是handler
			//alert(obj["on"+evt]==handler);
			if (obj["on"+evt]!=handler)
				functions.push(obj["on"+evt]);
		}
		obj["on"+evt]=handler;
		return obj;
		
		
	}

	function delEvent(obj,evtype,fn) {
		if (obj.removeEventListener && !Browser.isOpera) {
			obj.removeEventListener(evtype,fn,false);
			return obj;
		}
		var fns=obj.functions || {};
		fns=fns[evtype] || [];
		for (var i=0;i<fns.length;i++) {
			if (fns[i]==fn) {
				fns.splice(i,1);
				return obj;
			}
		}
	}

	function handler(evt) {//哪个事件发生了?
		//对event对象标准化
		var evt=fixEvt(evt || window.event,this);
		var evtype=evt.type;
		var functions=this.functions[evtype];
		for (var i=0;i<functions.length;i++) {
			if (functions[i]) functions[i].call(this,evt);
		}
	}

	function fixEvt(evt,o) {
		if (!evt.target) {//IE
			evt.target=evt.srcElement;
			if (evt.type=="mouseover")
				evt.relatedTarget=evt.fromElement;
			else if ("mouseout"==evt.type)
				evt.relatedTarget=evt.toElement;
			
			evt.pageX=evt.clientX+document.documentElement.scrollLeft;
			evt.pageY=evt.clientY+document.documentElement.scrollTop;
			evt.stopPropagation=function () {
				evt.cancelBubble=true;
			};
			evt.preventDefault=function () {
				evt.returnValue=false;
			};
		}
		
		//IE与Opera的offsetX,offsetY没有将边框算进之内
		if (o!=window && o.nodeType) {//要求是一个DOM对象
			//evt.layerX=evt.offsetX+o.clientLeft;
			//evt.layerY=evt.offsetY+o.clientTop;
			//如何获取一个DOM元素在页面中的坐标
			var offset=getOffset(o);
			evt.layerX=evt.pageX-offset.x;
			evt.layerY=evt.pageY-offset.y;
			
		}
		
		
		return evt;
	}
	function getRealStyle(o,name) {
		if (window.getComputedStyle) {
			var style=window.getComputedStyle(o,null);
			return style.getPropertyValue(name);
		} else {
			var style=o.currentStyle;
			name=camelize(name);
			if (name=="float")
				name="styleFloat";
			return style[name];
		}
		
	}

	function camelize(s) {//将CSS属性名转换成驼峰式
		return s.replace(/-[a-z]/gi,function (c) {
			return c.charAt(1).toUpperCase();
		});
	}

	function getOffset(o) {
		//o.offsetLeft
		//o.offsetTop
		//o.offsetParent
		
		var x=y=0,de=document.documentElement;
		
		if (o==de) {
			return {
				x:de.scrollLeft,
				y:de.scrollTop
			};
		}
		while (o) {
			x+=o.offsetLeft;
			y+=o.offsetTop;
			o=o.offsetParent;
			if (o && o!=de && !Browser.isOpera) {
				x+=o.clientLeft;
				y+=o.clientTop;
			}
		}
		return {
			x:x,
			y:y
		};
	}	

	
/** ---------------------------获取当前日期---------------------------------  */	
function currentDate() {
	var d = new Date();
	var dtStr = d.getFullYear()+"-"+Base.prefix((d.getMonth()+1),2)+"-"+Base.prefix(d.getDate(),2);
	return dtStr;
}

/***--------------------Cookie Related------------------------------------**/
var Cookies = {};

//设置Cookies
Cookies.set = function(name, value){
     var argv = arguments;
     var argc = arguments.length;
     var expires = (argc > 2) ? argv[2] : null;
     var path = (argc > 3) ? argv[3] : '/';
     var domain = (argc > 4) ? argv[4] : null;
     var secure = (argc > 5) ? argv[5] : false;
     document.cookie = name + "=" + escape (value) +
       ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
       ((path == null) ? "" : ("; path=" + path)) +
       ((domain == null) ? "" : ("; domain=" + domain)) +
       ((secure == true) ? "; secure" : "");
};

//读取Cookies
Cookies.get = function(name){
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    var j = 0;
    while(i < clen){
        j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return Cookies.getCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if(i == 0)
            break;
    }
    return null;
};

//清除Cookies
Cookies.clear = function(name) {
  if(Cookies.get(name)){
    var expdate = new Date(); 
    expdate.setTime(expdate.getTime() - (86400 * 1000 * 1)); 
    Cookies.set(name, "", expdate); 
  }
};

//获取 Cookie值
Cookies.getCookieVal = function(offset){
   var endstr = document.cookie.indexOf(";", offset);
   if(endstr == -1){
       endstr = document.cookie.length;
   }
   return unescape(document.cookie.substring(offset, endstr));
};

/*****-----------------------JS验证相关----------------------------------
 * Email
 * Number
 * 中文
 * 
 ******/

/**
 * 常用到的js情形：
 * 1.Checkbox 全选与反选
 * 
 */