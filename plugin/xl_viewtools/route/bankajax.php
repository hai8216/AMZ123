<?php
$action = param('action', 'city');
if ($action == 'city') {
    $type = param('type');
    if ($type == 0 || !$type) {
        $banklist = db_find("bank_provinces", array('type' => 0), array(), 1, 500);
        echo json_encode(array('data' => 200, 'list' => $banklist));
    } else {
        $code = param('cityCode');
        $banklist = db_find("bank_provinces", array('type' => 1, 'provinceId' => $code), array(), 1, 500);
        echo json_encode(array('data' => 200, 'list' => $banklist));
    }
} elseif ($action == 'cityend') {
    $map['parentBankCode'] = param('parent_bank_code');
    $map['cityId'] = param('city_id');
    $isSql = db_find("bank_code", $map, array(), 1, 50);

    echo json_encode(array('data' => 200, 'list' => $isSql));
} elseif ($action == 'bank1') {
    $map['parent_bank_code'] = param('parent_bank_code');
    $isSql = db_find("bank_city", $map, array('id' => '1'), 1, 150);

    echo json_encode(array('data' => 200, 'list' => $isSql));
} elseif ($action == 'bank3') {
    $map['parent_bank_code'] = param('parent_bank_code');
    $isSql = db_find("bank_city", $map, array('id' => '1'), 1, 150);

    echo json_encode(array('data' => 200, 'list' => $isSql));
}
// elseif ($action == 'bank1end') {
////    $map['parentBankCode'] = param('parent_bank_code');
////    $map['cityId'] = param('city_id');
////    $isSql = db_find("bank_code",$map,array(),1,50);
////    echo json_encode(array('data' => 200, 'list' => $isSql));
//    $client = new QF_HTTP_CLIENT();
//    $data = $client->get("https://brokers.fx678.com/bank/branchBankList?parent_bank_code=ABNACNSHXXX");
//    if ($data['data']) {
//        foreach ($data['data'] as $v) {
//            $insert['branchBankCode'] = $v['branchBankCode'];
//            $insert['city'] = $v['city'];
//            $insert['cityId'] = $v['cityId'];
//            $insert['name'] = $v['name'];
//            $insert['parentBankCode'] = $v['parentBankCode'];
//            db_insert("bank_code",$insert);
//        }
//    }
//}


class QF_HTTP_CLIENT
{
    private $hostname = 'hualongxiang'; // 站点hostname
    private $token = 'xxxxxxxxx'; // 开放平台token

    /**
     * @param string $method 请求方式
     * @param string $path 请求路径
     * @param array $requestBody 请求体参数
     * @return array
     * @throws Exception
     */
    private function _request($method, $url, $requestBody = [])
    {
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, $method);
        if (count($requestBody)) {
            curl_setopt($curlHandle, CURLOPT_POST, 1);
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, http_build_query($requestBody));
        }
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 30); // 连接超时
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30); // 响应超时

        $response = curl_exec($curlHandle);
        $error = curl_error($curlHandle);
        $errno = curl_errno($curlHandle);
        // $status = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
        curl_close($curlHandle);

        if ($error) {
            throw new Exception("cURL Error #{$errno}. $error", 1);
        }

        $data = json_decode($response, true);
        if (json_last_error() != JSON_ERROR_NONE) {
            $error = json_last_error_msg();
            $errno = json_last_error();
            throw new Exception("Json Parsing Error #{$errno}. $error", 1);
        }
        return $data;
    }

    public function get($path)
    {
        return $this->_request('GET', $path, []);
    }

    public function put($path, $data)
    {
        return $this->_request('PUT', $path, $data);
    }

    public function post($path, $data)
    {
        return $this->_request('POST', $path, $data);
    }

    public function delete($path, $data)
    {
        return $this->_request('DELETE', $path, $data);
    }
}

?>