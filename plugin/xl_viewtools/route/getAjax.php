<?php
$host = param(1);
if($host=='hdkw'){
    $header['Host'] = "www.thdws.com";
    $data = curlhttpGet("https://www.thdws.com/TA2/search?term=".param('keywords'),$header);
    $data = json_decode($data,true);
    $rdata['keywords'] = $data['r'];
    echo json_encode($rdata);
}elseif($host=='wayfair'){

    $header=['User-Agent:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36'];
    $b = curlhttpGet("https://www.wayfair.com/",$header);
    $data = curlhttpGet("https://www.wayfair.com/a/search/predictive?query=".param('keywords'),$header);
    print_r($b);exit;
    $data = json_decode($data,true);
    foreach($data['suggestions'] as $k=> $v){
        $datalist[$k]['t'] = $v['value'];
    }
    $rdata['keywords'] = $datalist;
    echo json_encode($rdata);
}elseif($host=='lokw'){
    $header['Host'] = "www.lowes.com";
    $data = curlhttpGet("https://www.lowes.com/LowesSearchServices/resources/autocomplete/v2_0?searchTerm=".param('keywords')."&maxTerms=10",$header);
    $data = json_decode($data,true);
    foreach($data['terms'] as $k=> $v){
        $datalist[$k]['t'] = $v['name'];
    }
    $rdata['keywords'] = $datalist;
    echo json_encode($rdata);
}elseif($host=='bbbkw'){
    $header['Host'] = "www.bbbyproperties.com";
    $data = curlhttpGet("https://api-bbby.bbbyproperties.com/api/apps/bedbath_typeahead/query/t1?web3feo=abc&wt=json&q=".param('keywords')."&site=BedBathUS&isGroupby=true&isBrowser=true",$header);
    $data = json_decode($data,true);
    foreach($data['result']['searchTerms'] as $k=> $v){
        $datalist[$k]['t'] = $v['value'];
    }
    $rdata['keywords'] = $datalist;
    echo json_encode($rdata);
}


function curlhttpGet($url,$hader)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_HTTPHEADER, $hader);
    curl_setopt($curl, CURLOPT_HEADER, 0);//返回response头部信息
    curl_setopt($curl, CURLOPT_USERAGENT,'Mozilla/5.0 (Linux; U; Android 2.3.7; zh-cn; c8650 Build/GWK74) AppleWebKit/533.1 (KHTML, like Gecko)Version/4.0 MQQBrowser/4.5 Mobile Safari/533.1s');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 50);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_URL, $url);
    $res = curl_exec($curl);
    curl_close($curl);
    return $res;
}
