<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/9
 * Time: 11:48
 */
require_once APP_PATH . '/vendor/autoload.php';

use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Tmt\V20180321\TmtClient;
use TencentCloud\Tmt\V20180321\Models\TextTranslateRequest;

$viewname = param(1);
$viewname = $viewname ?: exit;
//$css['guanjianci'] = "<link rel=\"stylesheet\" href=\"".$conf['view_url']."../plugin/xl_viewtools/lib/guanjianci.css\">";
//$js['guanjianci'] = "<script src=\"".$conf['view_url']."../plugin/xl_viewtools/lib/guanjianci.js\"></script><script src=\"".$conf['view_url']."../plugin/xl_viewtools/lib/iconfont.js\"></script><script src=\"".$conf['view_url']."../plugin/xl_viewtools/lib/tool-keyword.js\"></script>";
$apppath = $conf['view_url'] . "../plugin/xl_viewtools/lib/";
$gjx = 1;
if ($viewname == 'jingli') {
    $header['title'] = '亚马逊招商经理联系方式 - AMZ123';
    $header['keywords'] = '亚马逊招商经理,招商经理,招商经理联系方式，AMZ123';
    $header['description'] = '最全的亚马逊招商经理联系方式。亚马逊卖家运营必备工具汇总!最专业的亚马逊卖家导航网站AMZ123.com,为Amazon卖家提供专业的常用工具。亚马逊卖家之路，从AMZ123开始。';
} elseif ($viewname == 'fbadepot') {
    $header['title'] = '亚马逊FBA仓库地址，收藏！';
    $header['keywords'] = '亚马逊仓库地址,FBA仓库地址,亚马逊FBA仓库地址,亚马逊美国仓库地址,美国FBA仓库地址,英国FBA仓库地址,日本FBA仓库地址';
    $header['description'] = '亚马逊FBA仓库地址大全，美国FBA仓库地址，加拿大FBA仓库地址，英国FBA仓库地址，德国FBA仓库地址，法国FBA仓库地址,日本FBA仓库地址查询，收藏起来，以备不时之需。';
} elseif ($viewname == 'bianjiqi') {
    $gjx = 1;
    $header['title'] = '亚马逊可视化编辑器 - AMZ123';
    $header['keywords'] = '可视化编辑器,亚马逊编辑器';
    $header['description'] = '亚马逊可视化编辑器可以帮助亚马逊卖家直观地编辑Listing详情，无需代码基础，编辑完成后直接复制到亚马逊后台即可。';
} elseif ($viewname == 'paypal') {
    $gjx = 1;
    $header['title'] = 'PayPal手续费计算器 - AMZ123贝宝费用计算工具';
    $header['keywords'] = 'PayPal手续费,PayPal计算器,PayPal费用,PayPal汇款,PayPal Fee Calculator';
    $header['description'] = '多个国家的即时PayPal费用计算器+反向计算器。 轻松计算出PayPal中国用户所需转账的实际金额，方便快捷！';
} elseif ($viewname == 'danwei') {
    $header['title'] = '万能在线单位转换器 - AMZ123';
    $header['keywords'] = '单位换算器,在线单位换算器';
    $header['description'] = '在线单位换算器：很方便的实现长度单位换算，重量转换换算等功能。';
} elseif ($viewname == 'guanjianci') {
    $header['title'] = '亚马逊长尾词挖掘工具 - AMZ123搜索框关键词拓展';
    $header['keywords'] = '亚马逊关键词工具,亚马逊长尾词工具,亚马逊关键词查询,亚马逊关键词挖掘';
    $header['description'] = '亚马逊长尾关键词工具，辅助卖家找出更加优质精准的关键词，可实时挖掘亚马逊搜索框拓展关键词。';
} elseif ($viewname == 'remove') {
    $header['title'] = '去除重复文本工具 - AMZ123';
    $header['keywords'] = '文本去重,关键词去重,关键字去重';
    $header['description'] = '去除重复文本工具可以帮助亚马逊卖家一键去除重复的关键词，找出更有的关键词。';
} elseif ($viewname == 'templetmail') {
    $header['title'] = '亚马逊邮件模板生成器 - AMZ123';
    $header['keywords'] = '亚马逊邮件模板,亚马逊索评邮件,亚马逊售后邮件';
    $header['description'] = '亚马逊邮件模版生成器可以帮助亚马逊卖家直接生成邮件模版，包括亚马逊客服售后邮件模板、亚马逊退货邮件模板和亚马逊索评邮件模板等。';
} elseif ($viewname == 'chengbenlirun') {
    $header['title'] = '亚马逊成本利润计算器 - AMZ123';
    $header['keywords'] = '亚马逊成本计算,亚马逊利润计算,亚马逊财务核算';
    $header['description'] = '成本利润计算器可以帮助卖家快速计算产品的收入和成本、利润、利润率，辅助卖家进行市场调研选品和运营。';
    $hv = db_find_one("exchange", array(), array('id' => '-1'));
} elseif ($viewname == 'guolvqi') {
    $header['title'] = '亚马逊关键词过滤器 - AMZ123';
    $header['keywords'] = '关键词过滤,关键词筛选';
    $header['description'] = '关键词过滤器可以对眼花缭乱的关键词进行筛选，防止卖家重复使用关键词导致浪费Listing资源。';
} elseif ($viewname == 'toggle') {
    $header['title'] = '英文字母大小写转换工具 - AMZ123';
    $header['keywords'] = '大小写转换,英文字母大小写';
    $header['description'] = '在线英文字母大小写转换工具,能很方便的将英文字母全部转换为大写或者小写,或者将英文句子转换成首字母大写。';
} elseif ($viewname == 'timestamp') {
    $header['title'] = 'Unix时间戳(Unix timestamp)转换工具 - AMZ123';
    $header['keywords'] = 'Unix时间戳转换,时间戳转换工具';
    $header['description'] = 'Unix时间戳转换可以把Unix时间转成北京时间。';
} elseif ($viewname == 'canonical') {
    $header['title'] = '亚马逊Canonical URL（权威链接）生成工具 - AMZ123';
    $header['keywords'] = 'Canonical URL,权威链接,权威链接生成';
    $header['description'] = '方便地生成搜索引擎友好的Canonical URL（权威链接）。';
} elseif ($viewname == 'buy') {
    $header['title'] = '亚马逊Add To Cart和Buy Together链接生成工具 - AMZ123';
    $header['keywords'] = 'Add To Cart,Buy Together';
    $header['description'] = '方便地生成Add To Cart和Buy Together链接。';
} elseif ($viewname == '2step') {
    $header['title'] = '亚马逊两步链接生成工具，二次链接生成 - AMZ123';
    $header['keywords'] = '两步链接,二次链接';
    $header['description'] = '两步链接，即引导客户顺着某个搜索关键词，用两个步骤，模拟搜索关键词，完成对目标listing详情页的访问。';
} elseif ($viewname == 'asinsearch') {
    $header['title'] = '多个ASIN搜索链接生成工具 - AMZ123';
    $header['keywords'] = 'ASIN搜索,多个ASIN';
    $header['description'] = '生成您指定的ASIN列表的Amazon搜索结果页面。';
} elseif ($viewname == '5star') {
    $header['title'] = '亚马逊五星好评链接生成工具 - AMZ123';
    $header['keywords'] = 'ASIN搜索,多个ASIN';
    $header['description'] = '生成一个已预设好的五星好评的“Create a Review”页面，方便您发送给客户填写评论使用。';
} elseif ($viewname == 'upc') {
    $header['title'] = '免费UPC码在线生成器 - AMZ123';
    $header['keywords'] = 'UPC码生成器,UPC,亚马逊UPC,UPC码在线生成,UPC码生成工具';
    $header['description'] = '免费UPC码在线生成器是一款专业生成UPC码的工具，所生成的条码有效符合亚马逊EAN/UPC编码要求和国际物品编码协会、美国统一委员会的规则。';
} elseif ($viewname == 'wordcounter') {
    $header['title'] = ' 单词词频统计工具 - AMZ123';
    $header['keywords'] = '字符统计, 单词统计, 单词出现次数统计, 高频词统计, 词频统计, word count, word counter';
    $header['description'] = '单词词频统计工具提供了字符统计和单词词频报告，帮助亚马逊卖家一键统计不同单词的出现次数，用于分析对手的listing和评价内容。';
} elseif ($viewname == 'hscode') {
    $header['title'] = ' 海关HS编码查询  - AMZ123';
    $header['keywords'] = 'Hs code查询,海关HS编码查询,海关商品编码查询,HS编码查询，HSCode海关编码查询';
    $header['description'] = '强大的商品编码、海关编码、hs编码查询数据库,可提供全面、准确的商品编码查询,海关编码查询,海关编码查询系统,hs code查询,hs code,海关hs商品编码查询等查询服务。';
} elseif ($viewname == 'piliang') {
    $header['title'] = ' 在线网址批量打开工具 - AMZ123';
    $header['keywords'] = '网址批量打开,网页批量打开,网站批量打开,批量打开链接';
    $header['description'] = '网址批量打开工具，可以在线快速批量打开指定的网站网页，一键批量打开大量链接，直接在浏览器里使用，无需安装，绿色安全！';
} elseif ($viewname == 'qinquan') {
    $header['title'] = ' 在线侵权检测工具 - AMZ123侵权词库系统';
    $header['keywords'] = '侵权检测工具,侵权检测,在线侵权检测,侵权词库,侵权检测系统';
    $header['description'] = '在线侵权检测工具，可以尽量避免标题描述等文字侵权商标侵权问题，无需安装，绿色安全！';
    $ks = db_find("keywords", array(), array(), 1, 100000);
    $kstext = '';
    foreach ($ks as $v) {
        $kstext .= $v['keywords'] . ";";
    }

} elseif ($viewname == 'dnyfanyi') {
    if (param('ajax')) {
        $cred = new Credential("AKIDud6LM47jUyLSKuqDX0BF4exyhfOq6Pec", "rTBeqa5LM6bQ1LSK1RZVeXBi04qKZy81");
        $httpProfile = new HttpProfile();
        $httpProfile->setEndpoint("tmt.tencentcloudapi.com");

        $clientProfile = new ClientProfile();
        $clientProfile->setHttpProfile($httpProfile);
        $client = new TmtClient($cred, "ap-guangzhou", $clientProfile);
        $req = new TextTranslateRequest();
        $countr = array(
            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210106/16098654603482.png/viewthumb',
                'name' => '中国',
                'tips' => '中文',
                'code' => 'zh'
            ),
            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210106/160987141343138.png/viewthumb',
                'name' => '马来西亚',
                'tips' => '马来语',
                'code' => 'ms',
                'shopee' => 'https://shopee.com.my/search?keyword=',
                'lazada' => 'https://www.lazada.com.my/catalog/?q=2'
            ),
            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210106/159782020645236.png',
                'name' => '印度尼西亚',
                'tips' => '印尼语',
                'code' => 'id',
                'shopee' => 'https://shopee.co.id/search?keyword=',
                'lazada' => 'https://www.lazada.co.id/catalog/?q='
            ),
            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210105/160985933231675.png/viewthumb',
                'name' => '泰国',
                'tips' => '泰语',
                'code' => 'th',
                'shopee' => 'https://shopee.co.th/search?keyword=',
                'lazada' => 'https://www.lazada.co.th/catalog/?q='
            ),
        );
        $countr2 = array(

            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210105/160896557615697.png/viewthumb',
                'name' => '菲律宾',
                'tips' => '菲律宾语',
                'code' => 'en', //
                'shopee' => 'https://shopee.ph/search?keyword=',
                'lazada' => 'https://www.lazada.com.ph/catalog/?q='
            ),
            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210105/160896565839194.png/viewthumb',
                'name' => '越南',
                'tips' => '越语',
                'code' => 'vi',
                'shopee' => 'https://shopee.vn/search?keyword=',
                'lazada' => 'https://www.lazada.vn/catalog/?q='
            ),
            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210105/159782033941138.png/viewthumb',
                'name' => '新加坡',
                'tips' => '英语',
                'code' => 'en',
                'shopee' => 'https://shopee.sg/search?keyword=',
                'lazada' => 'https://www.lazada.sg/catalog/?q='
            ),
            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210105/16098609904227.png/viewthumb',
                'name' => '巴西',
                'tips' => '葡萄牙语',
                'code' => 'pt',
                'shopee' => 'https://shopee.com.br/search?keyword=',
            )
        );
        $hb = array_merge($countr, $countr2);

        //转中文后再翻译,如果本来就是中文就告辞
        if (param('from') == 'zh') {
            $zhTS = param('q');
        } else {
            $params = array(
                "SourceText" => param('q'),
                "Source" => param('from'),
                "Target" => 'zh',
                "ProjectId" => 1211321
            );
            $zhTS = param('q');
            $req->fromJsonString(json_encode($params));
            $resp = $client->TextTranslate($req);
            $zhT = json_decode($resp->toJsonString(), true);
            $zhTS = $zhT['TargetText'];
        }
        //去掉自身翻译
        foreach ($countr as $k => $v) {
            if ($v['code'] == param('from')) {
                unset($countr[$k]);
            }
        }
        foreach ($countr2 as $k => $v) {
            if ($v['code'] == param('from')) {
                unset($countr2[$k]);
            }
        }
        foreach ($countr as $v) {
            $params = array(
                "SourceText" => $zhTS,
                "Source" => 'zh',
                "Target" => $v['code'],
                "ProjectId" => 1211321
            );
            $req->fromJsonString(json_encode($params));
            $resp = $client->TextTranslate($req);
            $data = json_decode($resp->toJsonString(), true);
            $fy[] = array('to' => $v['code'], 'text' => $data['TargetText'], 'urls' => getUrl($v['code'], $data['TargetText'], $hb));
        }
        sleep(1);
        foreach ($countr2 as $v) {
            $params = array(
                "SourceText" => $zhTS,
                "Source" => 'zh',
                "Target" => $v['code'],
                "ProjectId" => 1211321
            );
            $req->fromJsonString(json_encode($params));
            $resp = $client->TextTranslate($req);
            $data = json_decode($resp->toJsonString(), true);
            $fy[] = array('to' => $v['code'], 'text' => $data['TargetText'], 'urls' => getUrl($v['code'], $data['TargetText'], $hb));
        }

        //拼接一个繁体
        require APP_PATH . "/plugin/xl_viewtools/class/HanziConvert.php";
        $fttext = HanziConvert::convert($zhTS, true);
        $fy[] = array('to' => 'zh-TW', 'text' => $fttext, 'urls' => array('shopee' => 'https://xiapi.xiapibuy.com/search?keyword=' . $fttext, 'lazada' => ''));
        //拼接不参与计算的
        if (param('from') != 'zh-TW') {
            $fy[] = array('to' => param('from'), 'text' => param('q'), 'urls' => getUrl(param('from'), param('q'), $hb));
        }
        echo json_encode($fy);
        exit;
    } else {
        $val = db_find_one("viewtools_seo", array('viewname' => "dnyfanyi"));
        $header['title'] = $val['title'];
        $header['keywords'] = $val['keywords'];
        $header['description'] = $val['description'];
    }
} elseif ($viewname == 'lmfanyi') {
    if (param('ajax')) {
        $cred = new Credential("AKIDud6LM47jUyLSKuqDX0BF4exyhfOq6Pec", "rTBeqa5LM6bQ1LSK1RZVeXBi04qKZy81");
        $httpProfile = new HttpProfile();
        $httpProfile->setEndpoint("tmt.tencentcloudapi.com");

        $clientProfile = new ClientProfile();
        $clientProfile->setHttpProfile($httpProfile);
        $client = new TmtClient($cred, "ap-guangzhou", $clientProfile);
        $req = new TextTranslateRequest();
        $countr = array(
            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210106/16098654603482.png/viewthumb',
                'name' => '中国',
                'tips' => '中文',
                'code' => 'zh'
            ),
            array(
                'img' => 'https://www.amz123.com/upload/qrcode/20201226/160897160835905.png/viewthumb',
                'name' => '英国',
                'tips' => '英语',
                'code' => 'en'
            ),
            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210106/160986285547250.png/viewthumb',
                'name' => '墨西哥',
                'tips' => '西班牙语',
                'code' => 'es'
            ),
            array(
                'img' => 'https://img.amz123.com/upload/time_icon/20210105/16098609904227.png/viewthumb',
                'name' => '巴西',
                'tips' => '葡萄牙语',
                'code' => 'pt'
            )
        );
        //转中文后再翻译,如果本来就是中文就告辞
        if (param('from') == 'zh') {
            $zhTS = param('q');
        } else {
            $params = array(
                "SourceText" => param('q'),
                "Source" => param('from'),
                "Target" => 'zh',
                "ProjectId" => 1211321
            );
            $zhTS = param('q');
            $req->fromJsonString(json_encode($params));
            $resp = $client->TextTranslate($req);
            $zhT = json_decode($resp->toJsonString(), true);
            $zhTS = $zhT['TargetText'];
        }
        //去掉自身翻译
        foreach ($countr as $k => $v) {
            if ($v['code'] == param('from')) {
                unset($countr[$k]);
            }
        }
        foreach ($countr as $v) {
            $params = array(
                "SourceText" => $zhTS,
                "Source" => 'zh',
                "Target" => $v['code'],
                "ProjectId" => 1211321
            );
            $req->fromJsonString(json_encode($params));
            $resp = $client->TextTranslate($req);
            $data = json_decode($resp->toJsonString(), true);
            $fy[] = array('to' => $v['code'], 'text' => $data['TargetText'], 'urls' => getUrl($v['code'], $data['TargetText'], $hb));
        }
        echo json_encode($fy);
        exit;
    } else {
        $val = db_find_one("viewtools_seo", array('viewname' => "lmfanyi"));
        $header['title'] = $val['title'];
        $header['keywords'] = $val['keywords'];
        $header['description'] = $val['description'];
    }
} else {
    $val = db_find_one("viewtools_seo", array('viewname' => $viewname));
    $header['title'] = $val['title'];
    $header['keywords'] = $val['keywords'];
    $header['description'] = $val['description'];
}

function getUrl($code, $kw, $acode)
{
    foreach ($acode as $k => $v) {
        if ($v['code'] == $code) {
            if ($v['shopee']) {
                $list['shopee'] = $v['shopee'] . $kw;
            } else {
                $list['shopee'] = "";
            }
            if ($v['lazada']) {
                $list['lazada'] = $v['lazada'] . $kw;
            } else {
                $list['lazada'] = "";
            }
            return $list;
        }
    }

}

include _include(APP_PATH . 'plugin/xl_viewtools/htm/' . $viewname . '.htm');