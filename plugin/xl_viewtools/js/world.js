(function (t) {
    function a(a) {
        for (var i, s, o = a[0], l = a[1], c = a[2], u = 0, m = []; u < o.length; u++) s = o[u], Object.prototype.hasOwnProperty.call(n, s) && n[s] && m.push(n[s][0]), n[s] = 0;
        for (i in l) Object.prototype.hasOwnProperty.call(l, i) && (t[i] = l[i]);
        d && d(a);
        while (m.length) m.shift()();
        return r.push.apply(r, c || []), e()
    }

    function e() {
        for (var t, a = 0; a < r.length; a++) {
            for (var e = r[a], i = !0, o = 1; o < e.length; o++) {
                var l = e[o];
                0 !== n[l] && (i = !1)
            }
            i && (r.splice(a--, 1), t = s(s.s = e[0]))
        }
        return t
    }

    var i = {}, n = {world: 0}, r = [];

    function s(a) {
        if (i[a]) return i[a].exports;
        var e = i[a] = {i: a, l: !1, exports: {}};
        return t[a].call(e.exports, e, e.exports, s), e.l = !0, e.exports
    }

    s.m = t, s.c = i, s.d = function (t, a, e) {
        s.o(t, a) || Object.defineProperty(t, a, {enumerable: !0, get: e})
    }, s.r = function (t) {
        "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {value: "Module"}), Object.defineProperty(t, "__esModule", {value: !0})
    }, s.t = function (t, a) {
        if (1 & a && (t = s(t)), 8 & a) return t;
        if (4 & a && "object" === typeof t && t && t.__esModule) return t;
        var e = Object.create(null);
        if (s.r(e), Object.defineProperty(e, "default", {
                enumerable: !0,
                value: t
            }), 2 & a && "string" != typeof t) for (var i in t) s.d(e, i, function (a) {
            return t[a]
        }.bind(null, i));
        return e
    }, s.n = function (t) {
        var a = t && t.__esModule ? function () {
            return t["default"]
        } : function () {
            return t
        };
        return s.d(a, "a", a), a
    }, s.o = function (t, a) {
        return Object.prototype.hasOwnProperty.call(t, a)
    }, s.p = "";
    var o = window["webpackJsonp"] = window["webpackJsonp"] || [], l = o.push.bind(o);
    o.push = a, o = o.slice();
    for (var c = 0; c < o.length; c++) a(o[c]);
    var d = l;
    r.push([3, "chunk-vendors", "chunk-common"]), e()
})({
    "032a": function (t, a, e) {
    }, "2ac1": function (t, a, e) {
        "use strict";
        var i = e("8f28"), n = e.n(i);
        n.a
    }, 3: function (t, a, e) {
        t.exports = e("f1e8")
    }, "367b": function (t, a, e) {
        "use strict";
        var i = e("db15"), n = e.n(i);
        n.a
    }, "3efc": function (t, a, e) {
        "use strict";
        var i = e("abcb"), n = e.n(i);
        n.a
    }, "494d": function (t, a, e) {
    }, "7e35": function (t, a, e) {
        "use strict";
        var i = e("d789"), n = e.n(i);
        n.a
    }, "8f28": function (t, a, e) {
    }, abcb: function (t, a, e) {
    }, c8e7: function (t, a, e) {
        "use strict";
        var i = e("032a"), n = e.n(i);
        n.a
    }, d789: function (t, a, e) {
    }, db15: function (t, a, e) {
    }, f120: function (t, a, e) {
        "use strict";
        var i = e("494d"), n = e.n(i);
        n.a
    }, f1e8: function (t, a, e) {
        "use strict";
        e.r(a);
        e("cadf"), e("551c"), e("f751"), e("097d");
        var i = e("a026"), n = function () {
                var t = this, a = t.$createElement, e = t._self._c || a;
                return e("div", {attrs: {id: "app"}}, [e("router-view")], 1)
            }, r = [], s = (e("367b"), e("2877")), o = {}, l = Object(s["a"])(o, n, r, !1, null, null, null), c = l.exports,
            d = e("8c4f"), u = function () {
                var t = this, a = t.$createElement, e = t._self._c || a;
                return e("div", {staticClass: "home"}, [e("div", {
                    staticClass: "home-top",
                    attrs: {id: "yq_subscriber"}
                }, [e("span", {staticClass: "line"}), e("div", {staticClass: "link-inner"}, [e("span", {staticClass: "link-first"}, [e("a", {
                    attrs: {href: "javascript:void(0);"},
                    on: {
                        click: function (a) {
                            return window.open("https://www.amz123.com", "pc_epidemic_tab_overall")
                        }
                    }
                }, [t._v("AMZ123首页")])]), t._m(0)])]), e("div", {staticClass: "home-bottom"}, [e("WorldMiddle", {staticClass: "home-middle"})], 1)])
            }, m = [function () {
                var t = this, a = t.$createElement, e = t._self._c || a;
                return e("span", {staticClass: "link-other link-choose"}, [e("a", {attrs: {href: "javascript:void(0);"}}, [t._v("全球疫情")])])
            }], h = (e("a481"), e("3b2b"), function () {
                var t = this, a = t.$createElement, e = t._self._c || a;
                return e("div", {staticClass: "middle-inner"}, [e("MapWorld", {staticClass: "mapWorld"}), e("ListWorld", {staticClass: "listWorld"})], 1)
            }), f = [], p = function () {
                var t = this, a = t.$createElement, e = t._self._c || a;
                return e("div", {staticClass: "worldmap-box"}, [e("div", {staticClass: "worldmap-box-bg"}, [e("img", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.showbg,
                        expression: "showbg"
                    }], staticClass: "bg", attrs: {id: "worldmap-bg", src: "https://www.amz123.com/plugin/xl_viewtools/bg.png"}
                })]), e("div", {staticClass: "worldmap-box-inner"}, [e("img", {
                    staticClass: "title",
                    attrs: {src: "https://www.amz123.com/images/yiqingtitle.png"}
                }), e("span", {staticClass: "line1"}), e("span", {staticClass: "line2"}), e("div", {staticClass: "worldmap-inner"}, [e("div", {
                    staticClass: "worldmap",
                    attrs: {id: "worldmap"}
                }), e("div", {
                    directives: [{name: "show", rawName: "v-show", value: t.loadingShow, expression: "loadingShow"}],
                    staticClass: "spinner"
                }, [t._m(0), t._m(1), t._m(2)])])])])
            }, v = [function () {
                var t = this, a = t.$createElement, e = t._self._c || a;
                return e("div", {staticClass: "spinner-container container1"}, [e("div", {staticClass: "circle1"}), e("div", {staticClass: "circle2"}), e("div", {staticClass: "circle3"}), e("div", {staticClass: "circle4"})])
            }, function () {
                var t = this, a = t.$createElement, e = t._self._c || a;
                return e("div", {staticClass: "spinner-container container2"}, [e("div", {staticClass: "circle1"}), e("div", {staticClass: "circle2"}), e("div", {staticClass: "circle3"}), e("div", {staticClass: "circle4"})])
            }, function () {
                var t = this, a = t.$createElement, e = t._self._c || a;
                return e("div", {staticClass: "spinner-container container3"}, [e("div", {staticClass: "circle1"}), e("div", {staticClass: "circle2"}), e("div", {staticClass: "circle3"}), e("div", {staticClass: "circle4"})])
            }], b = (e("7f7f"), e("75fc")), g = {
                "Singapore Rep.": "新加坡",
                "Dominican Rep.": "多米尼加",
                Palestine: "巴勒斯坦",
                Bahamas: "巴哈马",
                "Timor-Leste": "东帝汶",
                Afghanistan: "阿富汗",
                "Guinea-Bissau": "几内亚比绍",
                "Côte d'Ivoire": "科特迪瓦",
                "Siachen Glacier": "锡亚琴冰川",
                "Br. Indian Ocean Ter.": "英属印度洋领土",
                Angola: "安哥拉",
                Albania: "阿尔巴尼亚",
                "United Arab Emirates": "阿联酋",
                Argentina: "阿根廷",
                Armenia: "亚美尼亚",
                "French Southern and Antarctic Lands": "法属南半球和南极领地",
                Australia: "澳大利亚",
                Austria: "奥地利",
                Azerbaijan: "阿塞拜疆",
                Burundi: "布隆迪",
                Belgium: "比利时",
                Benin: "贝宁",
                "Burkina Faso": "布基纳法索",
                Bangladesh: "孟加拉国",
                Bulgaria: "保加利亚",
                "The Bahamas": "巴哈马",
                "Bosnia and Herz.": "波斯尼亚和黑塞哥维那",
                Belarus: "白俄罗斯",
                Belize: "伯利兹",
                Bermuda: "百慕大",
                Bolivia: "玻利维亚",
                Brazil: "巴西",
                Brunei: "文莱",
                Bhutan: "不丹",
                Botswana: "博茨瓦纳",
                "Central African Rep.": "中非",
                Canada: "加拿大",
                Switzerland: "瑞士",
                Chile: "智利",
                China: "中国",
                "Ivory Coast": "象牙海岸",
                Cameroon: "喀麦隆",
                "Dem. Rep. Congo": "刚果民主共和国",
                Congo: "刚果",
                Colombia: "哥伦比亚",
                "Costa Rica": "哥斯达黎加",
                Cuba: "古巴",
                "N. Cyprus": "北塞浦路斯",
                Cyprus: "塞浦路斯",
                "Czech Rep.": "捷克",
                Germany: "德国",
                Djibouti: "吉布提",
                Denmark: "丹麦",
                Algeria: "阿尔及利亚",
                Ecuador: "厄瓜多尔",
                Egypt: "埃及",
                Eritrea: "厄立特里亚",
                Spain: "西班牙",
                Estonia: "爱沙尼亚",
                Ethiopia: "埃塞俄比亚",
                Finland: "芬兰",
                Fiji: "斐",
                "Falkland Islands": "福克兰群岛",
                France: "法国",
                Gabon: "加蓬",
                "United Kingdom": "英国",
                Georgia: "格鲁吉亚",
                Ghana: "加纳",
                Guinea: "几内亚",
                Gambia: "冈比亚",
                "Guinea Bissau": "几内亚比绍",
                "Eq. Guinea": "赤道几内亚",
                Greece: "希腊",
                Greenland: "格陵兰",
                Guatemala: "危地马拉",
                "French Guiana": "法属圭亚那",
                Guyana: "圭亚那",
                Honduras: "洪都拉斯",
                Croatia: "克罗地亚",
                Haiti: "海地",
                Hungary: "匈牙利",
                Indonesia: "印度尼西亚",
                India: "印度",
                Ireland: "爱尔兰",
                Iran: "伊朗",
                Iraq: "伊拉克",
                Iceland: "冰岛",
                Israel: "以色列",
                Italy: "意大利",
                Jamaica: "牙买加",
                Jordan: "约旦",
                Japan: "日本",
                Kazakhstan: "哈萨克斯坦",
                Kenya: "肯尼亚",
                Kyrgyzstan: "吉尔吉斯斯坦",
                Cambodia: "柬埔寨",
                Korea: "韩国",
                Kosovo: "科索沃",
                Kuwait: "科威特",
                "Lao PDR": "老挝",
                Lebanon: "黎巴嫩",
                Liberia: "利比里亚",
                Libya: "利比亚",
                "Sri Lanka": "斯里兰卡",
                Lesotho: "莱索托",
                Lithuania: "立陶宛",
                Luxembourg: "卢森堡",
                Latvia: "拉脱维亚",
                Morocco: "摩洛哥",
                Moldova: "摩尔多瓦",
                Madagascar: "马达加斯加",
                Mexico: "墨西哥",
                Macedonia: "马其顿",
                Mali: "马里",
                Myanmar: "缅甸",
                Montenegro: "黑山",
                Mongolia: "蒙古",
                Mozambique: "莫桑比克",
                Mauritania: "毛里塔尼亚",
                Malawi: "马拉维",
                Malaysia: "马来西亚",
                Namibia: "纳米比亚",
                "New Caledonia": "新喀里多尼亚",
                Niger: "尼日尔",
                Nigeria: "尼日利亚",
                Nicaragua: "尼加拉瓜",
                Netherlands: "荷兰",
                Norway: "挪威",
                Nepal: "尼泊尔",
                "New Zealand": "新西兰",
                Oman: "阿曼",
                Pakistan: "巴基斯坦",
                Panama: "巴拿马",
                Peru: "秘鲁",
                Philippines: "菲律宾",
                "Papua New Guinea": "巴布亚新几内亚",
                Poland: "波兰",
                "Puerto Rico": "波多黎各",
                "Dem. Rep. Korea": "朝鲜",
                Portugal: "葡萄牙",
                Paraguay: "巴拉圭",
                Qatar: "卡塔尔",
                Romania: "罗马尼亚",
                Russia: "俄罗斯",
                Rwanda: "卢旺达",
                "W. Sahara": "西撒哈拉",
                "Saudi Arabia": "沙特阿拉伯",
                Sudan: "苏丹",
                "S. Sudan": "南苏丹",
                Senegal: "塞内加尔",
                "Solomon Is.": "所罗门群岛",
                "Sierra Leone": "塞拉利昂",
                "El Salvador": "萨尔瓦多",
                Somaliland: "索马里兰",
                Somalia: "索马里",
                Serbia: "塞尔维亚",
                Suriname: "苏里南",
                Slovakia: "斯洛伐克",
                Slovenia: "斯洛文尼亚",
                Sweden: "瑞典",
                Swaziland: "斯威士兰",
                Syria: "叙利亚",
                Chad: "乍得",
                Togo: "多哥",
                Thailand: "泰国",
                Tajikistan: "塔吉克斯坦",
                Turkmenistan: "土库曼斯坦",
                "East Timor": "东帝汶",
                "Trinidad and Tobago": "特里尼达和多巴哥",
                Tunisia: "突尼斯",
                Turkey: "土耳其",
                Tanzania: "坦桑尼亚",
                Uganda: "乌干达",
                Ukraine: "乌克兰",
                Uruguay: "乌拉圭",
                "United States": "美国",
                Uzbekistan: "乌兹别克斯坦",
                Venezuela: "委内瑞拉",
                Vietnam: "越南",
                Vanuatu: "瓦努阿图",
                "West Bank": "西岸",
                Yemen: "也门",
                "South Africa": "南非",
                Zambia: "赞比亚",
                Zimbabwe: "津巴布韦",
                Comoros: "科摩罗"
            }, C = {
                name: "mapWorld", data: function () {
                    return {myChart: null, worldList: [], loadingShow: !0, showbg: !1}
                }, mounted: function () {
                    var t = this;
                    this.getData(), this.myChart = echarts.init(document.getElementById("worldmap")), window.addEventListener("resize", (function () {
                        t.myChart && setTimeout((function () {
                            t.myChart.resize({width: "auto"})
                        }))
                    })), document.getElementById("worldmap-bg").onload = function () {
                        t.showbg = !0
                    }
                }, beforeDestroy: function () {
                    this.disposeMap()
                }, methods: {
                    disposeMap: function () {
                        this.myChart && (this.myChart.dispose(), this.myChart = null)
                    }, handleWorldList: function (t) {
                        this.worldList = [];
                        for (var a = 0; a < t.length; a++) {
                            var e, i = t[a];
                            (e = this.worldList).push.apply(e, Object(b["a"])(i.city))
                        }
                    }, getData: function () {
                        var t = this;
                        this.$api.dataAPI.getMapData().then((function (a) {
                            console.log(a), 200 == a.code && (t.$store.commit("setMapData", Object.assign({}, a)), t.handleWorldList(a.data.foreign.list), t.initWorldChart(t.worldList), t.loadingShow = !1)
                        }))
                    }, initWorldChart: function (t) {
                        this.myChart = echarts.init(document.getElementById("worldmap")), this.mapFeatures = echarts.getMap("world").geoJson.features, this.myChart.setOption({
                            tooltip: {
                                extraCssText: "z-index: 5",
                                trigger: "item",
                                transitionDuration: 0,
                                alwaysShowContent: !1,
                                enterable: !0,
                                backgroundColor: "rgba(61,61,61,0.9)",
                                padding: [10, 15],
                                textStyle: {color: "rgba(255,255,255,1)"},
                                formatter: function (t) {
                                    var a = t.data, e = null;
                                    if (void 0 !== a) {
                                        var i, n = a.deathNum / a.value * 100;
                                        i = 0 === n ? n.toFixed(0) : n.toFixed(2), e = "\n                ".concat(a.name, "<br>\n                确诊：").concat(a.value, "<br>\n                死亡：").concat(a.deathNum, "<br>\n                死亡率：").concat(i, "%<br>\n                ").concat(a.city && a.city.length > 0 ? '<span id="showCity">查看详细数据</span>' : "", "\n              ")
                                    }
                                    return e
                                }
                            },
                            visualMap: {
                                show: !0,
                                type: "piecewise",
                                pieces: [{min: 1e3}, {min: 100, max: 999}, {min: 10, max: 99}, {min: 1, max: 9}],
                                showLabel: !0,
                                itemSymbol: "circle",
                                seriesIndex: [0],
                                left: "2%",
                                bottom: "2%",
                                hoverLink: !1,
                                inRange: {color: ["rgba(247,229,190,1)", "#F5B491", "#D56F45", "#701F11"]},
                                textStyle: {color: "rgba(144,143,143,1)"}
                            },
                            geo: {
                                show: !0,
                                map: "world",
                                roam: !1,
                                layoutCenter: ["50%", "50%"],
                                layoutSize: "180%",
                                nameMap: g,
                                label: {normal: {show: !1, textStyle: {color: "#000"}}},
                                itemStyle: {normal: {areaColor: "#1588e9", borderColor: "rgba(0, 0, 0, 0.3)"}}
                            },
                            series: [{
                                type: "map",
                                map: "world",
                                name: "确诊病例",
                                geoIndex: 0,
                                label: {normal: {show: !0}},
                                roam: !1,
                                itemStyle: {
                                    normal: {areaColor: "#031525", borderColor: "#3B5077"},
                                    emphasis: {areaColor: "#2B91B7"}
                                },
                                animation: !1,
                                data: t
                            }]
                        })
                    }
                }
            }, y = C, w = (e("2ac1"), Object(s["a"])(y, p, v, !1, null, "185aed07", null)), _ = w.exports, x = function () {
                var t = this, a = t.$createElement, e = t._self._c || a;
                return e("Card", [e("div", {staticClass: "listword-container"}, [e("div", {staticClass: "title title-size"}, [t._v("\n      世界疫情实时动态\n    ")]), e("div", {staticClass: "list-main"}, [e("div", {staticClass: "list"}, [e("RankTable", {
                    attrs: {
                        datas: t.listworld,
                        unfold: !1
                    }, on: {clickItem: t.clickItem}
                })], 1), e("div", {ref: "detail", staticClass: "list-detail"}, [e("RankTable", {
                    attrs: {
                        datas: t.listNation,
                        unfold: !1,
                        isNation: !0
                    }
                })], 1)])])])
            }, k = [], S = (e("55dd"), e("96cf"), e("3b8d")), I = e("d95a"), T = function () {
                var t = this, a = t.$createElement, e = t._self._c || a;
                return e("div", {staticClass: "ranktable-container"}, [e("div", {staticClass: "table-head"}, [e("div", {staticClass: "head-item"}, [t._v(t._s(t.isNation ? "" : "大洲"))]), e("div", {staticClass: "head-item"}, [t._v("确诊")]), e("div", {staticClass: "head-item"}, [t._v("新增确诊")]), e("div", {staticClass: "head-item"}, [t._v("治愈")]), e("div", {staticClass: "head-item"}, [t._v("死亡")])]), e("div", {
                    ref: "tableBody",
                    staticClass: "table-body",
                    class: {"list-nation": t.isNation}
                }, t._l(t.datas, (function (a, i) {
                    return e("div", {
                        key: i,
                        staticClass: "table-row",
                        class: {highlighted: t.hightIndex == i && !t.isNation}
                    }, [e("div", {
                        staticClass: "col-item first-col", on: {
                            click: function (e) {
                                return t.showSecond(i, a, e)
                            }
                        }
                    }, [t._v("\n        " + t._s(a.name) + "\n      ")]), e("div", {
                        staticClass: "col-item blue-text",
                        on: {
                            click: function (e) {
                                return t.showSecond(i, a, e)
                            }
                        }
                    }, [t._v(t._s(t._f("formatData")(a.value)))]), e("div", {
                        staticClass: "col-item blue-text",
                        on: {
                            click: function (e) {
                                return t.showSecond(i, a, e)
                            }
                        }
                    }, [t._v(t._s(t._f("formatData")(a.confirmedAdd)))]), e("div", {
                        staticClass: "col-item",
                        on: {
                            click: function (e) {
                                return t.showSecond(i, a, e)
                            }
                        }
                    }, [t._v(t._s(t._f("formatData")(a.cureNum)))]), e("div", {
                        staticClass: "col-item",
                        on: {
                            click: function (e) {
                                return t.showSecond(i, a, e)
                            }
                        }
                    }, [t._v(t._s(t._f("formatData")(a.deathNum)))]), t.showIndex == i ? e("div", {staticClass: "second-table"}, t._l(a.city, (function (a, n) {
                        return e("div", {key: n + i + "", staticClass: "table-row"}, [e("div", {
                            staticClass: "col-item",
                            attrs: {title: a.name}
                        }, [t._v("\n            " + t._s(t._f("splitText")(a.name)) + "\n          ")]), e("div", {staticClass: "col-item blue-text"}, [t._v(t._s(t._f("formatData")(a.value)))]), e("div", {staticClass: "col-item"}, [t._v(t._s(t._f("formatData")(a.cureNum)))]), e("div", {staticClass: "col-item"}, [t._v(t._s(t._f("formatData")(a.deathNum)))])])
                    })), 0) : t._e()])
                })), 0)])
            }, B = [], j = {
                name: "RankTable", props: {
                    datas: {
                        type: Array, default: function () {
                            return []
                        }
                    }, unfold: {type: Boolean, default: !0}, isNation: {type: Boolean, default: !1}
                }, data: function () {
                    return {showIndex: -1, hightIndex: 0}
                }, methods: {
                    showSecond: function (t, a, e) {
                        var i = this;
                        this.hightIndex = t, this.$emit("clickItem", t), this.unfold && a.city && 0 != a.city.length && (t == this.showIndex ? this.showIndex = -1 : this.showIndex = t, this.$nextTick((function () {
                            var t = i.$refs.tableBody;
                            t && e.target.parentNode.offsetTop - 72 < t.scrollTop && (t.scrollTop = e.target.parentNode.offsetTop - 73)
                        })))
                    }
                }, filters: {
                    formatData: function (t) {
                        return t || ""
                    }, splitText: function (t) {
                        return t.length <= 6 ? t : t.substring(0, 6) + "..."
                    }
                }
            }, N = j, M = (e("f120"), Object(s["a"])(N, T, B, !1, null, "b2725200", null)), D = M.exports, $ = {
                name: "listWorld", data: function () {
                    return {listworld: [], listIndex: 0, listNation: []}
                }, components: {Card: I["a"], RankTable: D}, computed: {
                    mapData: function () {
                        return this.$store.state.mapData
                    }
                }, watch: {
                    mapData: {
                        handler: function (t) {
                            this.dealData()
                        }, deep: !0
                    }
                }, methods: {
                    dealData: function () {
                        var t = Object(S["a"])(regeneratorRuntime.mark((function t() {
                            var a, e, i, n;
                            return regeneratorRuntime.wrap((function (t) {
                                while (1) switch (t.prev = t.next) {
                                    case 0:
                                        try {
                                            a = this.mapData, e = a.code, i = a.data, n = i.foreign, 200 == e && i && n && n.list && n.list.length && (this.listworld = Object(b["a"])(n.list).sort((function (t, a) {
                                                return a.value - t.value
                                            })), this.listNation = this.listworld[this.listIndex].city)
                                        } catch (r) {
                                        }
                                    case 1:
                                    case"end":
                                        return t.stop()
                                }
                            }), t, this)
                        })));

                        function a() {
                            return t.apply(this, arguments)
                        }

                        return a
                    }(), clickItem: function (t) {
                        var a = this;
                        this.listIndex = t, this.listNation = this.listworld[t].city, this.$nextTick((function () {
                            a.$refs.detail && a.$refs.detail.querySelector(".table-body") && (a.$refs.detail.querySelector(".table-body").scrollTop = 0)
                        }))
                    }
                }
            }, E = $, O = (e("3efc"), Object(s["a"])(E, x, k, !1, null, "45c90b15", null)), L = O.exports, R = {
                name: "worldMiddle", components: {MapWorld: _, ListWorld: L}, data: function () {
                    return {}
                }, methods: {}, mounted: function () {
                }
            }, A = R, P = (e("c8e7"), Object(s["a"])(A, h, f, !1, null, "0daafe18", null)), z = P.exports, G = {
                name: "world", components: {WorldMiddle: z}, mounted: function () {
                    TA.log({id: "pc_epidemic_world"})
                }, methods: {
                    openurl: function (t, a) {
                        TA.log({id: a});
                        var e = "";
                        this.getQuery("from") && (e = "?from=" + this.getQuery("from")), setTimeout((function () {
                            location.href = t + e
                        }), 300)
                    }, getQuery: function (t, a) {
                        a = a || window.location.href;
                        var e = RegExp("[?&#]" + t + "=([^&]*)").exec(a);
                        return e && decodeURIComponent(e[1].replace(/\+/g, " "))
                    }
                }
            }, W = G, q = (e("7e35"), Object(s["a"])(W, u, m, !1, null, "5fda24b0", null)), F = q.exports;
        i["a"].use(d["a"]);
        var K = new d["a"]({routes: [{path: "/", redirect: "/index"}, {path: "/index", component: F}]}), U = e("4360"),
            J = e("365c"), H = e("ca00"), Q = e("cafc"), V = e.n(Q), Z = e("1368"), Y = e.n(Z);
        e("2e94");
        Y.a.polyfill(), V()(i["a"]), i["a"].config.productionTip = !1, i["a"].prototype.$eventBus = new i["a"], i["a"].prototype.$api = J["a"], i["a"].prototype.$util = H["a"], i["a"].prototype.$acme = Acme, new i["a"]({
            router: K,
            store: U["a"],
            render: function (t) {
                return t(c)
            }
        }).$mount("#app")
    }
});
//# sourceMappingURL=world.js.map