/**
 * 自主封装一个时间插件
 * $dom.bind('click',function(event){timePacker($(this),event)});
 * @author shuaiwu Li
 */

function pinyin(dom, e, t) {
    var hours = null;//存储 "时"
    var minutes = null;//存储 "分"
    var senc = null;//秒
    var clientY = dom.offset().top + dom.height();//获取位置
    var clientX = dom.offset().left;
    var date = new Date();
    var nowHours = date.getHours();
    var nowMinutes = date.getMinutes();
    var nowS = date.getSeconds();
    var inputText = dom.is("input") ? dom.val() : dom.text();
    //插件容器布局
    var html = '';

    html += '<div class="timePacker">';
    html += '<div class="timePacker-hours" style="display: block;">';
    if (!t) {
        html += '<div class="timePacker-title"><span>其他</span></div>';
        html += '<div class="timePacker-content">';
        html += '<ul>';
        html += '<li class="hoursList kong" d="0">空</li>';
        html += '</ul>';
        html += '</div>';
    }
    html += '<div class="timePacker-title"><span>类型</span></div>';
    html += '<div class="timePacker-content">';
    html += '<ul>';
    html += '<li class="hoursList ym" d="1">声母</li>';
    html += '<li class="hoursList ym" d="2">韵母</li>';
    html += '</ul>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    if ($(".timePacker").length > 0) {
        $(".timePacker").remove();
    }
    $("body").append(html);
    $(".timePacker").css({
        position: "absolute",
        top: clientY,
        left: clientX
    });
    var _con = $(".timePacker"); // 设置目标区域,如果当前鼠标点击非此插件区域则移除插件
    $(document).mouseup(function (e) {
        if (!_con.is(e.target) && _con.has(e.target).length === 0) { // Mark 1
            _con.remove();
        }
    });


    $(".ym").bind('click', function () {
        $(this).addClass("timePackerSelect").siblings().removeClass("timePackerSelect");
        var senc = $(this).text();
        dom.is("input") ? dom.val(senc) : dom.text(senc);
        dom.attr('d', $(this).attr('d'))
        _con.remove();
    })

    $(".kong").bind('click', function () {
        $(this).addClass("timePackerSelect").siblings().removeClass("timePackerSelect");
        var senc = $(this).text();
        dom.is("input") ? dom.val(senc) : dom.text(senc);
        dom.attr('d', $(this).attr('d'))
        _con.remove();
    })
}

$('#generate').click(function () {
    var sm = new Array(), ym = new Array();
    sm = ['b', 'p',
        'm',
        'f',
        'd',
        't',
        'n',
        'l',
        'g',
        'k',
        'h',
        'j',
        'q',
        'x',
        'r',
        'z',
        'c',
        's',
        'y',
        'w'
    ]
    ym = ['a', 'o', 'e', 'i', 'u']
    var
        l1 = $('#l1').attr('d'),
        l2 = $('#l2').attr('d')
    l3 = $('#l3').attr('d')
    l4 = $('#l4').attr('d')
    l5 = $('#l5').attr('d')
    l6 = $('#l6').attr('d')
    l7 = $('#l7').attr('d')
    l8 = $('#l8').attr('d')
    $('#sc').html('生成中');
    var h = '<div id="table">';
    var dl1, dl2, dl3, dl4, dl5, dl6, dl7, dl8
    for (var i = 0; i < $('#length').val(); i++) {
        if (l1 == 1) {
            var _number = Math.floor(Math.random() * sm.length);
            dl1 = sm[_number];
        } else if (l1 == 2) {
            var _number = Math.floor(Math.random() * ym.length);
            dl1 = ym[_number];
        }


        if (l2 == 1) {
            var _number = Math.floor(Math.random() * sm.length);
            dl2 = sm[_number];
        } else if (l2 == 2) {
            var _number = Math.floor(Math.random() * ym.length);
            dl2 = ym[_number];
        }

        if (l3 == 1) {
            var _number = Math.floor(Math.random() * sm.length);
            dl3 = sm[_number];
        } else if (l3 == 2) {
            var _number = Math.floor(Math.random() * ym.length);
            dl3 = ym[_number];
        }


        if (l4 == 1) {
            var _number = Math.floor(Math.random() * sm.length);
            dl4 = sm[_number];
        } else if (l4 == 2) {
            var _number = Math.floor(Math.random() * ym.length);
            dl4 = ym[_number];
        }


        if (l5 == 1) {
            var _number = Math.floor(Math.random() * sm.length);
            dl5 = sm[_number];
        } else if (l5 == 2) {
            var _number = Math.floor(Math.random() * ym.length);
            dl5 = ym[_number];
        }
        if (l6 == 1) {
            var _number = Math.floor(Math.random() * sm.length);
            dl6 = sm[_number];
        } else if (l6 == 2) {
            var _number = Math.floor(Math.random() * ym.length);
            dl6 = ym[_number];
        }else if (l6 == 0) {
            var _number = Math.floor(Math.random() * ym.length);
            dl6 = '';
        }

        if (l7 == 1) {
            var _number = Math.floor(Math.random() * sm.length);
            dl7 = sm[_number];
        } else if (l7 == 2) {
            var _number = Math.floor(Math.random() * ym.length);
            dl7 = ym[_number];
        }else if (l7 == 0) {
            var _number = Math.floor(Math.random() * ym.length);
            dl7 = '';
        }

        if (l8 == 1) {
            var _number = Math.floor(Math.random() * sm.length);
            dl8 = sm[_number];
        } else if (l8 == 2) {
            var _number = Math.floor(Math.random() * ym.length);
            dl8 = ym[_number];
        }else if (l8 == 0) {
            var _number = Math.floor(Math.random() * ym.length);
            dl8 = '';
        }
        h += '<div>' + dl1 + dl2 + dl3 + dl4 + dl5 + dl6 + dl7 + dl8+'</div>'
    }
    h += '</div>'
    $('#sc').html(h);
    $('#generate').remove()
})
