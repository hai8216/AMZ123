function Review_Super() {
    var asin = document.getElementById("asin").value.trim();
    if (asin !== null && asin !== "") {
        if (asin.length > 10 || asin.length < 10) {
            document.getElementById("Review_Super").value = 'ASIN must be 10 characters long';
            document.getElementById("asin").focus();
            return !1
        }else{
            var m = document.getElementById("marketplace");
            switch (m.options[m.selectedIndex].value) {
                case "1":
                    var url = 'https://www.amazon.com';
                    ;
                    break;
                case "2":
                    var url = 'https://www.amazon.ca';
                    ;
                    break;
                case "3":
                    var url = 'https://www.amazon.fr';
                    ;
                    break;
                case "4":
                    var url = 'https://www.amazon.de';
                    ;
                    break;
                case "5":
                    var url = 'https://www.amazon.in';
                    ;
                    break;
                case "6":
                    var url = 'https://www.amazon.it';
                    ;
                    break;
                case "7":
                    var url = 'https://www.amazon.es';
                    ;
                    break;
                case "8":
                    var url = 'https://www.amazon.co.uk';
                    ;
                    break;
                case "9":
                    var url = 'https://www.amazon.com.au';
                    ;
                    break;
                default:
                    var url = 'https://www.amazon.com';
                    ;
                    break
            }
            var links = url+"/review/create-review?&asin="+asin+":5"
            document.getElementById("Review_Super").value  = links
        }
    }else {
        document.getElementById("Review_Super").value  = 'ASIN must be filled out';
        document.getElementById("asin").focus();
    }
}

function validate(type) {
    switch (type) {
        case "canonical":
            document.getElementById("canonical-key1").innerHTML = "";
            document.getElementById("canonical-key2").innerHTML = "";
            document.getElementById("canonical-key3").innerHTML = "";
            document.getElementById("canonical-key4").innerHTML = "";
            document.getElementById("canonical-key5").innerHTML = "";
            document.getElementById("canonical-asin").innerHTML = "";
            var c1 = document.getElementById("canonical-key1").value.trim();
            var c2 = document.getElementById("canonical-key2").value.trim();
            var c3 = document.getElementById("canonical-key3").value.trim();
            var c4 = document.getElementById("canonical-key4").value.trim();
            var c5 = document.getElementById("canonical-key5").value.trim();
            var c6 = document.getElementById("canonical-asin").value.trim();
            if (c1 == null || c1 == "") {
                document.getElementById("canonical-link").value = 'All fields are required';
                document.getElementById("canonical-key1").focus();
                return
            }
            if (c2 == null || c2 == "") {
                document.getElementById("canonical-link").value = 'All fields are required';
                document.getElementById("canonical-key2").focus();
                return
            }
            if (c3 == null || c3 == "") {
                document.getElementById("canonical-link").value = 'All fields are required';
                document.getElementById("canonical-key3").focus();
                return
            }
            if (c4 == null || c4 == "") {
                document.getElementById("canonical-link").value = 'All fields are required';
                document.getElementById("canonical-key4").focus();
                return
            }
            if (c5 == null || c5 == "") {
                document.getElementById("canonical-link").value = 'All fields are required';
                document.getElementById("canonical-key5").focus();
                return
            }
            if (c6 == null || c6 == "") {
                document.getElementById("canonical-link").value = 'All fields are required';
                document.getElementById("canonical-asin").focus();
                return
            }
            if (c6.length > 10 || c6.length < 10) {
                document.getElementById("canonical-link").value = 'ASIN must be 10 characters long';
                document.getElementById("canonical-asin").focus();
                return
            }
            setMarketplace(type);
            break;
        case "add-to-cart":
            document.getElementById("add-cart-asin").innerHTML = "";
            document.getElementById("add-cart-quantity").innerHTML = "";
            var asin = document.getElementById("add-cart-asin").value.trim();
            if (asin == null || asin == "") {
                document.getElementById("add-cart-link").value = 'ASIN must be filled out';
                document.getElementById("add-cart-asin").focus();
                return !1
            }
            var quantity = document.getElementById("add-cart-quantity").value.trim();
            if (quantity == null || quantity == "") {
                document.getElementById("add-cart-link").value = 'Quantity must be filled out';
                document.getElementById("add-cart-quantity").focus();
                return !1
            }
            if (isNaN(quantity)) {
                document.getElementById("add-cart-link").value = 'Quantity must be a number';
                document.getElementById("add-cart-quantity").focus();
                return !1
            }
            if (asin.length > 10 || asin.length < 10) {
                document.getElementById("add-cart-link").value = 'ASIN must be 10 characters long';
                document.getElementById("canonical-asin").focus();
                return !1
            }
            setMarketplace("add-to-cart");
            break;
        case "buy-together":
            document.getElementById("bt-asin1").innerHTML = "";
            document.getElementById("bt-asin2").innerHTML = "";
            document.getElementById("bt-asin3").innerHTML = "";
            document.getElementById("bt-asin4").innerHTML = "";
            document.getElementById("bt-quantity1").innerHTML = "";
            document.getElementById("bt-quantity2").innerHTML = "";
            document.getElementById("bt-quantity3").innerHTML = "";
            document.getElementById("bt-quantity4").innerHTML = "";
            var asin1 = document.getElementById("bt-asin1").value.trim();
            var quantity1 = document.getElementById("bt-quantity1").value.trim();
            if ((asin1 == null || asin1 == "") || (quantity1 == null || quantity1 == "")) {
                document.getElementById("bt-link").value = 'Invalid input parameters';
                document.getElementById("bt-asin1").focus();
                return !1
            }
            if (asin1.length > 10 || asin1.length < 10) {
                document.getElementById("bt-link").value = 'Invalid input parameters';
                document.getElementById("bt-asin1").focus();
                return !1
            }
            if (isNaN(quantity1)) {
                document.getElementById("bt-link").value = 'Invalid input parameters';
                document.getElementById("bt-quantity1").focus();
                return !1
            }
            var asin2 = document.getElementById("bt-asin2").value.trim();
            var quantity2 = document.getElementById("bt-quantity2").value.trim();
            if ((asin2 == null || asin2 == "") || (quantity2 == null || quantity2 == "")) {
                document.getElementById("bt-link").value = 'Invalid input parameters';
                document.getElementById("bt-asin2").focus();
                return !1
            }
            if (asin2.length > 10 || asin2.length < 10) {
                document.getElementById("bt-link").value = 'Invalid input parameters';
                document.getElementById("bt-asin2").focus();
                return !1
            }
            if (isNaN(quantity2)) {
                document.getElementById("bt-link").value = 'Invalid input parameters';
                document.getElementById("bt-quantity2").focus();
                return !1
            }
            var asin3 = document.getElementById("bt-asin3").value.trim();
            var quantity3 = document.getElementById("bt-quantity3").value.trim();
            if ((asin3 !== null && asin3 !== "") && (quantity3 !== null && quantity3 !== "")) {
                if (asin3.length > 10 || asin3.length < 10) {
                    document.getElementById("bt-link").value = 'Invalid input parameters';
                    document.getElementById("bt-asin3").focus();
                    return !1
                }
                if (isNaN(quantity3)) {
                    document.getElementById("bt-link").value = 'Invalid input parameters';
                    document.getElementById("bt-quantity3").focus();
                    return !1
                }
            }
            var asin4 = document.getElementById("bt-asin4").value.trim();
            var quantity4 = document.getElementById("bt-quantity4").value.trim();
            if ((asin4 !== null && asin4 !== "") && (quantity4 !== null && quantity4 !== "")) {
                if (asin4.length > 10 || asin4.length < 10) {
                    document.getElementById("bt-link").value = 'Invalid input parameters';
                    document.getElementById("bt-asin4").focus();
                    return !1
                }
                if (isNaN(quantity4)) {
                    document.getElementById("bt-link").value = 'Invalid input parameters';
                    document.getElementById("bt-quantity4").focus();
                    return !1
                }
            }
            setMarketplace("buy-together");
            break;
        case "2-step-via-storefront":
            break;
        case "2-step-via-brand":
            document.getElementById("b-brand").innerHTML = "";
            document.getElementById("b-keywords").innerHTML = "";
            var brand = document.getElementById("b-brand").value.trim();
            if (brand == null || brand == "") {
                document.getElementById("b-link").value = 'Invalid input parameters';
                document.getElementById("b-brand").focus();
                return !1
            }
            var keywords = document.getElementById("b-keywords").value.trim();
            if (keywords == null || keywords == "") {
                document.getElementById("b-link").value = 'KEYWORDS must be filled out';
                document.getElementById("b-keywords").focus();
                return !1
            }
            var asin = document.getElementById("b-asin").value.trim();
            if (asin !== null && asin !== "") {
                if (asin.length > 10 || asin.length < 10) {
                    document.getElementById("b-link").value = 'ASIN must be 10 characters long';
                    document.getElementById("b-asin").focus();
                    return !1
                }
            }
            var minimum = document.getElementById("b-min").value.trim();
            if (minimum !== null && minimum !== "") {
                if (isNaN(minimum)) {
                    document.getElementById("b-link").value = 'Invalid input parameters';
                    document.getElementById("b-min").focus();
                    return !1
                }
            }
            var maximum = document.getElementById("b-max").value.trim();
            if (maximum !== null && maximum !== "") {
                if (isNaN(maximum)) {
                    document.getElementById("b-link").value = 'Invalid input parameters';
                    document.getElementById("b-max").focus();
                    return !1
                }
            }
            setMarketplace("2-step-via-brand");
            break;
        case "2-step-via-hidden-keyword":
            document.getElementById("hidden-asin").innerHTML = "";
            document.getElementById("hidden-keywords").innerHTML = "";
            var keywords = document.getElementById("hidden-keywords").value.trim();
            if (keywords == null || keywords == "") {
                document.getElementById("hidden-link").value = 'KEYWORDS must be filled out';
                document.getElementById("hidden-keywords").focus();
                return !1
            }
            var asin = document.getElementById("hidden-asin").value.trim();
            if (asin !== null && asin !== "") {
                if (asin.length > 10 || asin.length < 10) {
                    document.getElementById("hidden-link").value = 'ASIN must be 10 characters long';
                    document.getElementById("hidden-asin").focus();
                    return !1
                }
            } else {
                document.getElementById("hidden-link").value = 'ASIN must be filled out';
                document.getElementById("hidden-asin").focus();
                return !1
            }
            setMarketplace("2-step-via-hidden-keyword");
            break;
        case "2-step-via-field-asin":
            document.getElementById("field-asin-asin").innerHTML = "";
            document.getElementById("field-asin-keywords").innerHTML = "";
            var keywords = document.getElementById("field-asin-keywords").value.trim();
            if (keywords == null || keywords == "") {
                document.getElementById("field-asin-link").value = 'KEYWORDS must be filled out';
                document.getElementById("field-asin-keywords").focus();
                return !1
            }
            var asin = document.getElementById("field-asin-asin").value.trim();
            if (asin !== null && asin !== "") {
                if (asin.length > 10 || asin.length < 10) {
                    document.getElementById("field-asin-link").value = 'ASIN must be 10 characters long';
                    document.getElementById("field-asin-asin").focus();
                    return !1
                }
            } else {
                document.getElementById("field-asin-link").value = 'ASIN must be filled out';
                document.getElementById("field-asin-asin").focus();
                return !1
            }
            setMarketplace("2-step-via-field-asin");
            break;
        case "targeted-asin-search-url":
            var lines = document.getElementById('field-targeted-asin').value.split('\n');
            if (lines.length > 50) {
                document.getElementById("targeted-asin-search-link").value = 'Insert no more than 50 ASINs';
                document.getElementById("field-targeted-asin").focus();
                document.getElementById("field-targeted-asin").value = "";
                return !1
            }
            setMarketplace("targeted-asin-search-url");
            break;
        case "walmart-2-step-via-brand":
            document.getElementById("walmart-brand-keywords").innerHTML = "";
            document.getElementById("walmart-brand").innerHTML = "";
            var keywords = document.getElementById("walmart-brand-keywords").value;
            if (keywords == null || keywords == "") {
                document.getElementById("walmart-2-step-via-brand-link").value = 'KEYWORDS must be filled out';
                document.getElementById("walmart-brand-keywords").focus();
                return !1
            }
            var brand = document.getElementById("walmart-brand").value;
            if (brand == null || brand == "") {
                document.getElementById("walmart-2-step-via-brand-link").value = 'BRAND must be filled out';
                document.getElementById("walmart-brand").focus();
                return !1
            }
            setMarketplace("walmart-2-step-via-brand");
            break;
        case "walmart-2-step-via-storefront":
            document.getElementById("walmart-storefront-keywords").innerHTML = "";
            document.getElementById("walmart-storefront").innerHTML = "";
            var keywords = document.getElementById("walmart-storefront-keywords").value;
            if (keywords == null || keywords == "") {
                document.getElementById("walmart-2-step-via-storefront-link").value = 'KEYWORDS must be filled out';
                document.getElementById("walmart-storefront-keywords").focus();
                return !1
            }
            var storefront = document.getElementById("walmart-storefront").value;
            if (storefront == null || storefront == "") {
                document.getElementById("walmart-2-step-via-storefront-link").value = 'STORE NAME must be filled out';
                document.getElementById("walmart-storefront").focus();
                return !1
            }
            setMarketplace("walmart-2-step-via-storefront");
            break;
        case "storefront":
            document.getElementById("walmart-storefront-keywords").innerHTML = "";
            document.getElementById("walmart-storefront").innerHTML = "";
            var keywords = document.getElementById("walmart-storefront-keywords").value;
            if (keywords == null || keywords == "") {
                document.getElementById("walmart-2-step-via-storefront-link").value = 'KEYWORDS must be filled out';
                document.getElementById("walmart-storefront-keywords").focus();
                return !1
            }
            var storefront = document.getElementById("walmart-storefront").value;
            if (storefront == null || storefront == "") {
                document.getElementById("walmart-2-step-via-storefront-link").value = 'STORE NAME must be filled out';
                document.getElementById("walmart-storefront").focus();
                return !1
            }
            setMarketplace("walmart-2-step-via-storefront");
            break
    }
};

function generateStorefront() {
    var id = document.getElementById("sf-id").value.toUpperCase();
    var keywords = document.getElementById("sf-keywords").value;

    //keywords = keywords.replace(/\s\s+/g, '+');
    keywords = keywords.replace(/\s/g, '+');
    var m = document.getElementById("marketplace");
    switch (m.options[m.selectedIndex].value) {
        case "1":
            var url = 'https://www.amazon.com';
            ;
            break;
        case "2":
            var url = 'https://www.amazon.ca';
            ;
            break;
        case "3":
            var url = 'https://www.amazon.fr';
            ;
            break;
        case "4":
            var url = 'https://www.amazon.de';
            ;
            break;
        case "5":
            var url = 'https://www.amazon.in';
            ;
            break;
        case "6":
            var url = 'https://www.amazon.it';
            ;
            break;
        case "7":
            var url = 'https://www.amazon.es';
            ;
            break;
        case "8":
            var url = 'https://www.amazon.co.uk';
            ;
            break;
        case "9":
            var url = 'https://www.amazon.com.au';
            ;
            break;
        default:
            var url = 'https://www.amazon.com';
            ;
            break
    }
    var sflink = url + '/s/ref=nb_sb_noss?url=me%3D' + id + '&field-keywords=' + keywords;

    var asin = document.getElementById("sf-asin").value;
    if ((asin != null && asin !== "")) {
        asin = asin.toUpperCase();
        // append asin
        sflink = sflink + '&field-asin=' + asin;
    }

    var min = document.getElementById("sf-min").value;
    if ((min != null && min !== "")) {

        // append minimum
        sflink = sflink + '&low-price=' + min;
    }

    var max = document.getElementById("sf-max").value;
    if ((max != null && max !== "")) {

        // append maximum
        sflink = sflink + '&high-price=' + max;
    }

    document.getElementById("sf-link").value = sflink;
}

function setMarketplace(type) {
    var m = document.getElementById("marketplace");
    switch (m.options[m.selectedIndex].value) {
        case "1":
            generate(type, 'https://www.amazon.com');
            break;
        case "2":
            generate(type, 'https://www.amazon.ca');
            break;
        case "3":
            generate(type, 'https://www.amazon.fr');
            break;
        case "4":
            generate(type, 'https://www.amazon.de');
            break;
        case "5":
            generate(type, 'https://www.amazon.in');
            break;
        case "6":
            generate(type, 'https://www.amazon.it');
            break;
        case "7":
            generate(type, 'https://www.amazon.es');
            break;
        case "8":
            generate(type, 'https://www.amazon.co.uk');
            break;
        case "9":
            generate(type, 'https://www.amazon.com.au');
            break;
        default:
            generate(type, 'https://www.amazon.com');
            break
    }
}

function generate(type, marketplace) {
    switch (type) {
        case "canonical":
            var c1 = document.getElementById("canonical-key1").value.trim().toUpperCase();
            var c2 = document.getElementById("canonical-key2").value.trim().toUpperCase();
            var c3 = document.getElementById("canonical-key3").value.trim().toUpperCase();
            var c4 = document.getElementById("canonical-key4").value.trim().toUpperCase();
            var c5 = document.getElementById("canonical-key5").value.trim().toUpperCase();
            var c6 = document.getElementById("canonical-asin").value.trim().toUpperCase();
            var keywords = c1.concat('-').concat(c2).concat('-').concat(c3).concat('-').concat(c4).concat('-').concat(c5);
            var canonicalLink = marketplace + '/' + keywords + '/dp/' + c6;
            document.getElementById("canonical-link").value = canonicalLink;
            break;
        case "add-to-cart":
            var asin = document.getElementById("add-cart-asin").value.trim().toUpperCase();
            var quantity = document.getElementById("add-cart-quantity").value.trim();
            var link = marketplace + '/gp/aws/cart/add.html?ASIN.1=' + asin + '&Quantity.1=' + quantity;
            document.getElementById("add-cart-link").value = link;
            break;
        case "buy-together":
            var asin1 = document.getElementById("bt-asin1").value.trim().toUpperCase();
            var quantity1 = document.getElementById("bt-quantity1").value.trim();
            var asin2 = document.getElementById("bt-asin2").value.trim().toUpperCase();
            var quantity2 = document.getElementById("bt-quantity2").value.trim();
            var btlink = marketplace + '/gp/aws/cart/add.html?ASIN.1=' + asin1 + '&Quantity.1=' + quantity1 + '&ASIN.2=' + asin2 + '&Quantity.2=' + quantity2;
            var asin3 = document.getElementById("bt-asin3").value.trim();
            var quantity3 = document.getElementById("bt-quantity3").value.trim();
            if ((asin3 != null && asin3 !== "") && (quantity3 !== null && quantity3 !== "")) {
                btlink = btlink + '&ASIN.3=' + asin3 + '&Quantity.3=' + quantity3
            }
            var asin4 = document.getElementById("bt-asin4").value.trim();
            var quantity4 = document.getElementById("bt-quantity4").value.trim();
            if ((asin4 != null && asin4 !== "") && (quantity4 !== null && quantity4 !== "")) {
                btlink = btlink + '&ASIN.4=' + asin4 + '&Quantity.4=' + quantity4
            }
            document.getElementById("bt-link").value = btlink;
            break;
        case "2-step-via-storefront":
            break;
        case "2-step-via-brand":
            var brand = document.getElementById("b-brand").value.trim().toUpperCase();
            brand = encodeURIComponent(brand);
            var keywords = document.getElementById("b-keywords").value.trim();
            keywords = keywords.replace(/\s/g, '+');
            var blink = marketplace + '/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=' + keywords + '&field-brand=' + brand;
            var asin = document.getElementById("b-asin").value.trim();
            if ((asin != null && asin !== "")) {
                asin = asin.toUpperCase();
                blink = blink + '&field-asin=' + asin
            }
            var min = document.getElementById("b-min").value.trim();
            if ((min != null && min !== "")) {
                blink = blink + '&low-price=' + min
            }
            var max = document.getElementById("b-max").value.trim();
            if ((max != null && max !== "")) {
                blink = blink + '&high-price=' + max
            }
            document.getElementById("b-link").value = blink;
            break;
        case "2-step-via-hidden-keyword":
            var keywords = document.getElementById("hidden-keywords").value.trim();
            keywords = keywords.replace(/\s/g, '+');
            var hiddenLink = marketplace + '/s/ref=nb_sb_noss_1?url=search-alias%3Daps&field-keywords=' + keywords;
            var asin = document.getElementById("hidden-asin").value.trim();
            if ((asin != null && asin !== "")) {
                asin = asin.toUpperCase();
                hiddenLink = hiddenLink + '&hidden-keywords=' + asin
            }
            document.getElementById("hidden-link").value = hiddenLink;
            break;
        case "2-step-via-field-asin":
            var keywords = document.getElementById("field-asin-keywords").value.trim();
            keywords = keywords.replace(/\s/g, '+');
            var fieldAsinLink = marketplace + '/s/?keywords=' + keywords + "&ie=UTF8";
            var asin = document.getElementById("field-asin-asin").value.trim();
            if ((asin != null && asin !== "")) {
                asin = asin.toUpperCase();
                fieldAsinLink = fieldAsinLink + '&field-asin=' + asin + "&rh=i:aps,ssx:relevance"
            }
            document.getElementById("field-asin-link").value = fieldAsinLink;
            break;
        case "targeted-asin-search-url":
            var targetedAsinSearchUrlLink = marketplace + '/s/?k=';
            var lines = document.getElementById('field-targeted-asin').value.split('\n');
            for (var i = 0; i < lines.length; i++) {
                if (i + 1 == lines.length)
                    targetedAsinSearchUrlLink = targetedAsinSearchUrlLink + lines[i]; else targetedAsinSearchUrlLink = targetedAsinSearchUrlLink + lines[i] + '%7C+'
            }
            targetedAsinSearchUrlLink = targetedAsinSearchUrlLink + '&ref=nb_sb_noss';
            document.getElementById("targeted-asin-search-link").value = targetedAsinSearchUrlLink;
            break;
        case "walmart-2-step-via-brand":
            var keywords = document.getElementById("walmart-brand-keywords").value;
            keywords = keywords.replace(/\s/g, '+');
            var brand = document.getElementById("walmart-brand").value;
            var walmartBrandLink = 'https://www.walmart.com/search/?cat_id=0&facet=brand%3A' + brand + '&query=' + keywords;
            document.getElementById("walmart-2-step-via-brand-link").value = walmartBrandLink;
            break;
        case "storefront":
            var storefrontid = document.getElementById("sf-id").value;
            keywords = keywords.replace(/\s/g, '+');
            var brand = document.getElementById("walmart-brand").value;
            var walmartBrandLink = 'https://www.walmart.com/search/?cat_id=0&facet=brand%3A' + brand + '&query=' + keywords;
            document.getElementById("walmart-2-step-via-brand-link").value = walmartBrandLink;
            break;
        case "walmart-2-step-via-storefront":
            var keywords = document.getElementById("walmart-storefront-keywords").value;
            keywords = keywords.replace(/\s/g, '+');
            var storefront = document.getElementById("walmart-storefront").value;
            storefront = storefront.replace(/\s/g, '+');
            var walmartStorefrontLink = 'https://www.walmart.com/search/?cat_id=0&facet=retailer%3A' + storefront + '&query=' + keywords;
            document.getElementById("walmart-2-step-via-storefront-link").value = walmartStorefrontLink;
            break
    }
}

function copyToClipboard(id) {
    var copyText = document.getElementById(id);
    copyText.select();
    document.execCommand("Copy")
}

function openLink(id) {
    var url = document.getElementById(id).value;
    if (url !== null && url !== "") {
        var win = window.open(url, '_blank');
        win.focus()
    }
};