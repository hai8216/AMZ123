<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_footer_ad';
$action = param(3) ?: "list";
cache_delete('footer_ad_cache');
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("footer_ad", array('is_deleted' => 0), array("order_id"=>'-1'), $page, $pagesize);
    $c = db_count("footer_ad", ['is_deleted' => 0]);
    $pagination = pagination(url("plugin-setting-xl_footer_ad-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_footer_ad/view/list.html');
} elseif($action=='delete'){
    db_update("footer_ad",array('id'=>param('toid')), ['is_deleted' => 1]);
    message(0,jump('操作成功',jump('plugin-setting-xl_footer_ad')));
}elseif($action =='setting') {
    if ($method == 'GET') {
        $data = kv_get("footer_ad_setting");
        $input['title1'] = form_text('title1', $data['title1']);
        $input['title2'] = form_text('title2', $data['title2']);
        include _include(APP_PATH . 'plugin/xl_footer_ad/view/setting.html');
    }else {
        $save['title1'] = param('title1');
        $save['title2'] = param('title2');
        kv_set('footer_ad_setting',$save);
        message(0, jump('保存成功', url('plugin-setting-xl_footer_ad-setting')));
    }
} elseif ($action == 'create') {

    if ($method == 'GET') {
        if (param('toid')) {
            $data = db_find_one("footer_ad", array('id' => param('toid'), 'is_deleted' => 0));
        } else {
            $data = array('id'=>0,'star_time'=>time(),'end_time'=>time());
        }
        $input['title'] = form_text('title', $data['title']);
        $input['desc'] = form_text('desc', $data['desc']);
        $input['links'] = form_text('links', $data['links']);
        $input['dateline'] = form_text('dateline', $data['dateline']);
        $input['style'] = form_select('style',[0=>"样式1",1=>"样式2",2=>"样式3"],$data['style']);
        $input['star_time'] = form_text('star_time', $data['star_time']);
        $input['end_time'] = form_text('end_time', $data['end_time']);
        $input['zb_time'] = form_text('zb_time', $data['zb_time']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        include _include(APP_PATH . 'plugin/xl_footer_ad/view/create.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $res)) {
//            print_r($result);exit;
            $type = $res[2];
            $path = "/upload/index_icon/" . date('Ymd') . "/";
            $ext = '.'.$type;
            $name = uniqid() . $ext;
            if(saveTencentBase64(base64_decode(str_replace($res[1],'', param('favicon'))), $path . $name)){
                $data['thumb'] = I($path . $name,'16590');
            }
        }
        $data['title'] = param('title');
        $data['order_id'] = param('order_id');
        $data['desc'] = param('desc');
        $data['links'] = param('links');
        $data['style'] = param('style');
        $data['dateline'] = time();
        $data['star_time'] = strtotime(param('star_time'));
        $data['zb_time'] = strtotime(param('zb_time'));
        $data['end_time'] = strtotime(param('end_time'));
        if (param('toid')) {
            cache_set('footer_ad_cache', '', 180);
            db_update("footer_ad", array('id' => param('toid')), $data);
        } else {
            db_insert("footer_ad", $data);
        }

        message(0, jump('保存成功', url('plugin-setting-xl_footer_ad')));
    }
}
function getTstatus($s, $e)
{
    if (time() < $e && time() > $s) {
        return "<span style='color:green'>生效中</span>";
    } else if (time() < $e && time() < $s) {
        return "<span style='color:blue'>未开始</span>";
    } else if (time() > $e) {
        return "<span style='color:grey'>已结束</span>";
    }
}

?>
