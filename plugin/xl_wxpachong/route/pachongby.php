<?php

function generateRegularExpression($words)
{
    $regular = implode('|', array_map('preg_quote', $words));
    return "/$regular/i";
}

function generateRegularExpressionString($string)
{
    $str_arr[0] = $string;
    $str_new_arr = array_map('preg_quote', $str_arr);
    return $str_new_arr[0];
}

function check_words($banned, $string)
{
    $match_banned = array();
    //循环查出所有敏感词

    $new_banned = strtolower($banned);
    $i = 0;
    do {
        $matches = null;
        if (!empty($new_banned) && preg_match($new_banned, $string, $matches)) {
            $isempyt = empty($matches[0]);
            if (!$isempyt) {
                $match_banned = array_merge($match_banned, $matches);
                $matches_str = strtolower(generateRegularExpressionString($matches[0]));
                $new_banned = str_replace("|" . $matches_str . "|", "|", $new_banned);
                $new_banned = str_replace("/" . $matches_str . "|", "/", $new_banned);
                $new_banned = str_replace("|" . $matches_str . "/", "/", $new_banned);
            }
        }
        $i++;
        if ($i > 20) {
            $isempyt = true;
            break;
        }
    } while (count($matches) > 0 && !$isempyt);

    //查出敏感词
    if ($match_banned) {
        return $match_banned;
    }
    //没有查出敏感词
    return array();
}

$password = "123123/as";
//if (empty($_POST['__sign']) || md5("{$password}shenjianshou.cn") != trim($_POST['__sign'])) {
//    echo json_encode([
//        "result" => 2,
//        "reason" => "发布失败, 错误原因: 发布密码验证失败!"
//    ]);
//    exit;
//}

if (!$_POST['title']) {
    exit;
}
////测试爬虫
//$data['biz'] = $_POST['biz'];
//$data['title'] = $_POST['article_title'];
//$data['dateline'] = time();
//db_insert('pachong',$data);
//echo json_encode(["result" => 1, "data" => "发布成功"]);
//exit;

//重复贴的处理
//$isHava = db_find_one("wxpachong_log", array('biz' => md5($_POST['weixin_tmp_url'])));
//if ($isHava['id']) {
//    echo json_encode([
//        "result" => 2,
//        "reason" => "文章重复"
//    ]);
//    exit;
//}
//爬虫记录
//$pclog['wx_id'] = $_POST['weixin_name'];
//$pclog['wx_name'] = $_POST['weixin_nickname'];
//$pclog['tid'] = 0;
//$pclog['url'] = $_POST['weixin_tmp_url'];
//$pclog['post_status'] = 0;
//$pclog['dateline'] = time();
//$pclog['biz'] = md5($_POST['weixin_tmp_url']);
//db_insert("wxpachong_log", $pclog);

require_once(APP_PATH . "/plugin/xl_aliopensearch/Config.inc.php");

use OpenSearch\Client\DocumentClient;

//自动发布

$wx_id = $_POST['weixin_name'];
//看本地绑定情况
//$Wx_bind = array();
//$Wx_bind = db_find_one("wechat_bind_pachong", array('wx_id' => $wx_id));
//if ($Wx_bind['web_uid']) {
//    $uid = $Wx_bind['web_uid'];
//} else {
//    $uid = 10;
//}
//if($Wx_bind['fid']){
//    $fid = $Wx_bind['fid'];//默认板块
//    $forum = forum_read($fid);
//}else{
//    $fid = 1;//默认板块
//    $forum = forum_read($fid);
//}
$uid = 4383;
$fid = 11;
$forum = forum_read($fid);
$sid = '';
$subject = $_POST['title'];
$message = $_POST['content']."<br /><div class='form'>（来源：".$_POST['user']."）</div>";
$time = strtotime($_POST['time']) - rand(10000,100000);

//
////特殊策略
//if ($Wx_bind['matchdo']) {
//    @include APP_PATH . "/plugin/xl_wxpachong/matchdo/" . $Wx_bind['wx_id'] . ".php";
//}
$thread = array(
    'fid' => $fid,
    'uid' => $uid,
    'sid' => $sid,
    'subject' => $subject,
    'message' => $message,
    'time' => $time,
    'longip' => $longip,
    'doctype' => 0,
);
$remote_img = db_find_one('setting', array('name' => 'remote_img'));
if ($remote_img['value']) {
    preg_match_all("/<img[^>]*src\s?=\s?[\'|\"]([^\'|\"]*)[\'|\"]/is", $message, $img);
    $imgArr = $img[1];
    if ($imgArr) {
        foreach ($imgArr as $k => $v) {
            if (strstr($v, 'http')) {
                $path = "./upload/remote/" . date('Y-m-d');
                if (!file_exists($path)) mkdir($path, 0755, true);
                $extname = substr($v, strrpos($v, '.'));
                if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
                    $ext = $extname;
                } else {
                    $ext = '.jpg';
                }
                $name = uniqid() . $ext;
                saveImg($v, $path . "/" . $name);
                //file_put_contents($path."/".$name, file_get_contents(isset($_get[url])?$_get[url]:$v));
                $message = str_replace($v, $path . "/" . $name, $message);
                $thread['message'] = $message;
            }
        }
    }
}

$tid = thread_create($thread, $pid);
$pid === FALSE AND message(-1, lang('create_post_failed'));
$tid === FALSE AND message(-1, lang('create_thread_failed'));
if ($tid) {
    //有封面就保存封面
    if ($_POST['article_thumbnail']) {
        $extname = substr($_POST['article_thumbnail'], strrpos($_POST['article_thumbnail'], '.'));
        if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
            $ext = $extname;
        } else {
            $ext = '.jpg';
        }
        $name = uniqid() . $ext;
        $path = "./upload/remote/" . date('Y-m-d');
        saveImg($_POST['article_thumbnail'], $path . "/" . $name);
    }
    db_update("thread", array('tid' => $tid), array('thumb' => $path . "/" . $name));

}

    //判断标题包含的也要审核
    $haya_post_examine_config = kv_get('haya_post_examine');
    $mgkeywords = explode("\r\n", $haya_post_examine_config['check_words']);
    $banned = generateRegularExpression($mgkeywords);
//    //检查违禁词
    $res_banned = array();
    $res_banned = check_words($banned, $subject);
    if (isset($res_banned[0])) {
        $haya_post_examine_check_subject_status = 1;
    } else {
        $haya_post_examine_check_subject_status = 0;
    }
$haya_post_examine_check_subject_status = 0;
    if ($haya_post_examine_check_subject_status) {
        $haya_post_examine_thread = thread__read($tid);
        $haya_post_examine_new_thread = array(
            'fid' => $haya_post_examine_thread['fid'],
            'tid' => $haya_post_examine_thread['tid'],
            'top' => $haya_post_examine_thread['top'],
            'uid' => $haya_post_examine_thread['uid'],
            'userip' => $haya_post_examine_thread['userip'],
            'subject' => $haya_post_examine_thread['subject'],
            'create_date' => $haya_post_examine_thread['create_date'],
            'last_date' => $haya_post_examine_thread['last_date'],
            'views' => $haya_post_examine_thread['views'],
            'posts' => $haya_post_examine_thread['posts'],
            'images' => $haya_post_examine_thread['images'],
            'files' => $haya_post_examine_thread['files'],
            'mods' => $haya_post_examine_thread['mods'],
            'closed' => $haya_post_examine_thread['closed'],
            'firstpid' => $haya_post_examine_thread['firstpid'],
            'lastuid' => $haya_post_examine_thread['lastuid'],
            'lastpid' => $haya_post_examine_thread['lastpid'],
        );
        if ($_POST['article_thumbnail']) {
            $extname = substr($_POST['article_thumbnail'], strrpos($_POST['article_thumbnail'], '.'));
            if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
                $ext = $extname;
            } else {
                $ext = '.jpg';
            }
            $name = uniqid() . $ext;
            $path = "./upload/remote/" . date('Y-m-d');
            saveImg($_POST['article_thumbnail'], $path . "/" . $name);
            $haya_post_examine_new_thread['thumb'] = $path . "/" . $name;
        }
        haya_post_examine_thread__create($haya_post_examine_new_thread);

        xn_log("调用'pachongby:thread_delete'删除帖子{$tid}", "del_thread");
        thread__delete($tid);

        // 更新原始统计
        user__update($uid, array('threads-' => 1));
        forum__update($fid, array(
            'threads-' => 1,
            'todaythreads-' => 1
        ));
        runtime_set('threads-', 1);
        runtime_set('todaythreads-', 1);
    }else{
        //直接出来的才有阿里
        $user = user_read($uid);
        $tableName = 'bbs_post';
        $smsg = strip_tags($message);
        $documentClient = new DocumentClient($client);
        $docs_to_upload = array();
        $item['cmd'] = 'ADD';
        $item["fields"] = array(
            'pid' => $pid,
            'tid' => $tid,
            'subject' => $subject,
            'isfirst' => 1,
            'message' => str_replace("&nbsp;", " ", $smsg),
            'author' => $user['username'],
            'authorid' => $uid,
            'fid' => $fid,
            'fname' => $forum['name'],
            'back_message' => '',
            'posttime' => $time,
        );
        $docs_to_upload[] = $item;
        $json = json_encode($docs_to_upload);
        $ret = $documentClient->push($json, $appName, $tableName);
    }
//增加文字数
//c0ontent_count
//db_update('wechat_bind_pachong', array('wx_id' => $wx_id), array('content_count+' => 1));
//db_update('wxpachong_log', array('biz' => $_POST['biz']), array('tid' => $tid, 'post_status' => 1));
echo json_encode(["result" => 1, "data" => "发布成功"]);