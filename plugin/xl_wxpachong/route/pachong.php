<?php

function generateRegularExpression($words)
{
    $regular = implode('|', array_map('preg_quote', $words));
    return "/$regular/i";
}

function generateRegularExpressionString($string)
{
    $str_arr[0] = $string;
    $str_new_arr = array_map('preg_quote', $str_arr);
    return $str_new_arr[0];
}

function check_words($banned, $string)
{
    $match_banned = array();
    //循环查出所有敏感词

    $new_banned = strtolower($banned);
    $i = 0;
    do {
        $matches = null;
        if (!empty($new_banned) && preg_match($new_banned, $string, $matches)) {
            $isempyt = empty($matches[0]);
            if (!$isempyt) {
                $match_banned = array_merge($match_banned, $matches);
                $matches_str = strtolower(generateRegularExpressionString($matches[0]));
                $new_banned = str_replace("|" . $matches_str . "|", "|", $new_banned);
                $new_banned = str_replace("/" . $matches_str . "|", "/", $new_banned);
                $new_banned = str_replace("|" . $matches_str . "/", "/", $new_banned);
            }
        }
        $i++;
        if ($i > 20) {
            $isempyt = true;
            break;
        }
    } while (count($matches) > 0 && !$isempyt);

    //查出敏感词
    if ($match_banned) {
        return $match_banned;
    }
    //没有查出敏感词
    return array();
}

$password = "123123/as";
if (empty($_POST['__sign']) || md5("{$password}shenjianshou.cn") != trim($_POST['__sign'])) {
    echo json_encode([
        "result" => 2,
        "reason" => "发布失败, 错误原因: 发布密码验证失败!"
    ]);
    exit;
}

if (!$_POST['biz']) {
    exit;
}
////测试爬虫
//$data['biz'] = $_POST['biz'];
//$data['title'] = $_POST['article_title'];
//$data['dateline'] = time();
//db_insert('pachong',$data);
//echo json_encode(["result" => 1, "data" => "发布成功"]);
//exit;

//重复贴的处理
$isHava = db_find_one("wxpachong_log", array('biz' => md5($_POST['weixin_tmp_url'])));

if ($isHava['id']) {
    echo json_encode([
        "result" => 2,
        "reason" => $isHava['id']
    ]);
    exit;
}


require_once(APP_PATH . "/plugin/xl_aliopensearch/Config.inc.php");

use OpenSearch\Client\DocumentClient;

//自动发布

$wx_id = $_POST['weixin_name'];
//看本地绑定情况
$Wx_bind = array();
$Wx_bind = db_find_one("wechat_bind_pachong", array('wx_id' => $wx_id));
if ($Wx_bind['web_uid']) {
    $uid = $Wx_bind['web_uid'];
} else {
    $uid = 10;
}
if ($Wx_bind['fid']) {
    $fid = $Wx_bind['fid'];//默认板块
    $forum = forum_read($fid);
} else {
    $fid = 1;//默认板块
    $forum = forum_read($fid);
}

$sid = '';
$subject = $_POST['article_title'];
$message = $_POST['article_content'];
$time = time();

//特殊策略
if ($Wx_bind['matchdo']) {
    @include APP_PATH . "/plugin/xl_wxpachong/matchdo/" . $Wx_bind['wx_id'] . ".php";
}
$thread = array(
    'fid' => $fid,
    'uid' => $uid,
    'sid' => $sid,
    'subject' => $subject,
    'message' => $message,
    'time' => $time,
    'longip' => $longip,
    'doctype' => 0,
);
$_POST['article_images'] = json_decode($_POST['article_images'],true);
if ($_POST['article_images'][0]) {
    foreach ($_POST['article_images'] as $k => $v) {
             $path = "upload/thread_wx_img/" . date('Ymd');
             $ext = '.jpg';
             $name = uniqid() . $ext;
             saveImg_pachong($v, $path . "/" . $name, '0');
             //file_put_contents($path."/".$name, file_get_contents(isset($_get[url])?$_get[url]:$v));
             $message = str_replace($v, $path . "/" . $name, $message);
             $thread['message'] = $message;
    }
}

$tid = thread_create($thread, $pid);
$pid === FALSE AND message(-1, lang('create_post_failed'));
$tid === FALSE AND message(-1, lang('create_thread_failed'));
if ($tid) {

    //有封面就保存封面
    if ($_POST['article_thumbnail']) {
        $ext = '.jpg';
        $path = "upload/thread_wx_thumb/" . date('Ymd');
        $name = uniqid() . $ext;
        saveImg_pachong($_POST['article_thumbnail'], $path . "/" . $name, 0);
        db_update("thread", array('tid' => $tid), array('thumb' => $path . "/" . $name));
    } else {
        //没有封面随机图
        $temp = mt_rand(1, 100);
        db_update("thread", array('tid' => $tid), array('thumb_id' => $temp));
    }

}
if ($Wx_bind['censor_status'] == '1') {
    //直接审核
    $haya_post_examine_thread = thread__read($tid);
    $haya_post_examine_new_thread = array(
        'fid' => $haya_post_examine_thread['fid'],
        'tid' => $haya_post_examine_thread['tid'],
        'top' => $haya_post_examine_thread['top'],
        'uid' => $haya_post_examine_thread['uid'],
        'userip' => $haya_post_examine_thread['userip'],
        'subject' => $haya_post_examine_thread['subject'],
        'create_date' => $haya_post_examine_thread['create_date'],
        'last_date' => $haya_post_examine_thread['last_date'],
        'views' => $haya_post_examine_thread['views'],
        'posts' => $haya_post_examine_thread['posts'],
        'images' => $haya_post_examine_thread['images'],
        'files' => $haya_post_examine_thread['files'],
        'mods' => $haya_post_examine_thread['mods'],
        'closed' => $haya_post_examine_thread['closed'],
        'firstpid' => $haya_post_examine_thread['firstpid'],
        'lastuid' => $haya_post_examine_thread['lastuid'],
        'lastpid' => $haya_post_examine_thread['lastpid'],
        'thumb'=>$haya_post_examine_thread['thumb'],
        'thumb_id'=>$haya_post_examine_thread['thumb_id'],
    );
    haya_post_examine_thread__create($haya_post_examine_new_thread);

    xn_log("调用'pachong:thread_delete'删除帖子{$tid}", "del_thread");
    thread__delete($tid);


    // 更新原始统计
    user__update($uid, array('threads-' => 1));
    forum__update($fid, array(
        'threads-' => 1,
        'todaythreads-' => 1
    ));
    runtime_set('threads-', 1);
    runtime_set('todaythreads-', 1);
} else {
    //判断标题包含的也要审核
    $haya_post_examine_config = kv_get('haya_post_examine');
    $mgkeywords = explode("\r\n", $haya_post_examine_config['check_words']);
    $banned = generateRegularExpression($mgkeywords);
//    //检查违禁词
    $res_banned = array();
    $res_banned = check_words($banned, $subject);
    if (isset($res_banned[0])) {
        $haya_post_examine_check_subject_status = 1;
    } else {
        $haya_post_examine_check_subject_status = 0;
    }
    if ($haya_post_examine_check_subject_status) {
        $haya_post_examine_thread = thread__read($tid);
        $haya_post_examine_new_thread = array(
            'fid' => $haya_post_examine_thread['fid'],
            'tid' => $haya_post_examine_thread['tid'],
            'top' => $haya_post_examine_thread['top'],
            'uid' => $haya_post_examine_thread['uid'],
            'userip' => $haya_post_examine_thread['userip'],
            'subject' => $haya_post_examine_thread['subject'],
            'create_date' => $haya_post_examine_thread['create_date'],
            'last_date' => $haya_post_examine_thread['last_date'],
            'views' => $haya_post_examine_thread['views'],
            'posts' => $haya_post_examine_thread['posts'],
            'images' => $haya_post_examine_thread['images'],
            'files' => $haya_post_examine_thread['files'],
            'mods' => $haya_post_examine_thread['mods'],
            'closed' => $haya_post_examine_thread['closed'],
            'firstpid' => $haya_post_examine_thread['firstpid'],
            'lastuid' => $haya_post_examine_thread['lastuid'],
            'lastpid' => $haya_post_examine_thread['lastpid'],
            'thumb'=>$haya_post_examine_thread['thumb'],
            'thumb_id'=>$haya_post_examine_thread['thumb_id'],
        );
        haya_post_examine_thread__create($haya_post_examine_new_thread);
        xn_log("调用'pachong1:thread_delete'删除帖子{$tid}", "del_thread");
        thread__delete($tid);
        user__update($uid, array('threads-' => 1));
        forum__update($fid, array(
            'threads-' => 1,
            'todaythreads-' => 1
        ));
        runtime_set('threads-', 1);
        runtime_set('todaythreads-', 1);
    } else {
        //直接出来的才有阿里
        $user = user_read($uid);
        $tableName = 'bbs_post';
        $smsg = strip_tags($message);
        $documentClient = new DocumentClient($client);
        $docs_to_upload = array();
        $item['cmd'] = 'ADD';
        $item["fields"] = array(
            'pid' => $pid,
            'tid' => $tid,
            'subject' => $subject,
            'isfirst' => 1,
            'message' => str_replace("&nbsp;", " ", $smsg),
            'author' => $user['username'],
            'authorid' => $uid,
            'fid' => $fid,
            'fname' => $forum['name'],
            'back_message' => '',
            'posttime' => $time,
        );
        $docs_to_upload[] = $item;
        $json = json_encode($docs_to_upload);
        $ret = $documentClient->push($json, $appName, $tableName);
    }

}
//增加文字数
//c0ontent_count
//爬虫记录
$pclog['wx_id'] = $_POST['weixin_name'];
$pclog['wx_name'] = $_POST['weixin_nickname'];
$pclog['tid'] = $tid;
$pclog['url'] = $_POST['weixin_tmp_url'];
$pclog['post_status'] = 0;
$pclog['dateline'] = time();
$pclog['biz'] = md5($_POST['weixin_tmp_url']);
db_insert("wxpachong_log", $pclog);
db_update('wechat_bind_pachong', array('wx_id' => $wx_id), array('content_count+' => 1));
db_update('wxpachong_log', array('biz' => md5($_POST['weixin_tmp_url'])), array('post_status' => 1));
$end = memory_get_usage() - $ms;
$url = 'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
if(db_find_one("memory_copy",array('md5'=>md5($tmpfile)))){
    db_update("memory_copy",array('md5'=>md5($tmpfile)),['memory'=>$end,'dateline'=>time()]);
}else{
    db_insert("memory_copy",['memory'=>$end,'md5'=>md5($tmpfile),'tpl'=>$tmpfile,'url'=>$url,'dateline'=>time()]);
}
echo json_encode(["result" => 1, "data" => "发布成功"]);