<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') AND exit('Access Denied.');
if (param('do')==1 && $_POST) {
    $data['wx_id'] = param('wx_id');
    $data['web_uid'] = param('web_uid');
    $data['fid'] = param('postfid');
    $data['censor_status'] = param('censor_status');
    $webname = db_find_one("user",array('uid'=>$data['web_uid']));
    $data['web_name'] = $webname['username'];

    //保持一致性
    $iswx = db_find_one("wechat_bind_pachong",array('wx_id'=>$data['wx_id']));
    if($iswx){
        message(0,'该微信已被绑定');
    }

    db_insert("wechat_bind_pachong",$data);
    message(0, jump('绑定成功', url('plugin-setting-xl_wxpachong')));

} elseif(param('do')==2){
    if($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('id', 0);
    $r = db_delete('wechat_bind_pachong', array('id'=>$_codeid));
    $r === FALSE AND message(-1, lang('delete_failed'));
}elseif(param('do')==3){
    if($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('id', 0);
    $r = db_update('wechat_bind_pachong', array('id'=>$_codeid),array('censor_status'=>1));
}elseif(param('do')==4){
    if($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('id', 0);
    $r = db_update('wechat_bind_pachong', array('id'=>$_codeid),array('censor_status'=>0));
} else{
    $page = param(3, 1);
    $srchtype = param(4, '');
    $keys = trim(xn_urldecode(param(5)));
    $map = [];
    if($srchtype){
        if($srchtype!='locluser'){
            $map['wx_id'] = array("LIKE"=>$keys);
        }else{
            $map['web_name'] = array("LIKE"=>$keys);
        }

    }
    $pagesize = 40;
    $n = db_count('wechat_bind_pachong', $map);
    $pagination = pagination(url("plugin-setting-xl_wxpachong-{page}"), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_wxpachong-{page}"), $n, $page, $pagesize);
    $codelist = db_find('wechat_bind_pachong', $map, array(),$page,$pagesize);
    include _include(APP_PATH . "/plugin/xl_wxpachong/view/admin.htm");
}