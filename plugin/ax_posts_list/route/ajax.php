<?php
$tid = param(2);
if ($user['gid'] != 1) {
    message(0, '权限错误');
    exit;
}
if ($method == 'GET') {
    $pimg = I(post_img($tid), 'pcthumb');
    $jianjie = db_find_one('thread', array('tid' => $tid));
    include _include(APP_PATH . 'plugin/ax_posts_list/view/edthumb.htm');
} else {
    $tid = param('tid');
    $jianjie = param('jianjie', '');
    db_update("thread", array('tid' => param('tid')), array('jianjie' => $jianjie));
    thread_cache_clean($tid);
    cache_delete('thread_postthumbmessage_' . $tid);
    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
        $type = $result[2];
        $time = time();
        $wwwpath = "/upload/thread_thumb/" . date('Ymd', $time) . "/";
        $new_file = time() . rand(100, 50000) . "." . $type;
        $icon = $wwwpath . $new_file;
        if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $icon)) {
            db_update("thread", array('tid' => param('tid')), array('thumb' => getImgCloud() . $wwwpath . $new_file . "/pcthumb"));
            $tid = param('tid');
            thread_cache_clean($tid);
            cache_delete('thread_postthumbmessage_' . $tid);
            message(0, 'ok');
        }
    } else {
        message(0, '没有发生变化');
    }


}