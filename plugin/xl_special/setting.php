<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') AND exit('Access Denied.');
$cate = array('主流平台','新兴平台');
if (param('do') == 'add') {
    $sp_id = param('sp_id');
    if ($sp_id) {
        $spdata = db_find_one("special", array('sp_id' => $sp_id));
        $input['tag'] = form_text("tag", $spdata['tag']);
        $input['zdtid'] = form_text("zdtid", $spdata['zdtid']);
        $input['tuid'] = form_text("tuid", $spdata['tuid']);
        include _include(APP_PATH . "/plugin/xl_special/view/eadd.htm");
    } else {
        $input['tag'] = form_text("tag", "");
        $input['tuid'] = form_text("tuid", "");
        $input['zdtid'] = form_text("zdtid","");
        include _include(APP_PATH . "/plugin/xl_special/view/add.htm");
    }

}elseif(param('do')=='top') {
    $page = param(3, 1);
    $pagesize = 20;
    $n = db_count('special_shop_top', array());
    $pagination = pagination(url("plugin-setting-xl_special-{page}", array('do' => 'top')), $n, $page, $pagesize);
    $speciallist = db_find('special_shop_top', array(), array('order_id' => '-1'), $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_special/view/toplist.htm");
}elseif(param('do')=='edtop'){
    if ($method == 'GET') {
        $id = param('id');
        $val = [];
        if($id){
            $val = db_find_one("special_shop_top",["id"=>$id]);
        }
        include _include(APP_PATH . 'plugin/xl_special/view/addtop.htm');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('thumb'), $res)) {
            $type = $res[2];
            $path = "./upload/index_icon/" . date('Ymd') . "/";
            $ext = '.'.$type;
            $name = uniqid() . $ext;
            if(saveTencentBase64(base64_decode(str_replace($res[1],'', param('thumb'))), $path . $name)){
                $data['thumb'] = $path . $name;
            }
        }
        $data['links'] = param('links');
        $data['order_id'] = param('order_id');
        if(param('id')){
            db_update("special_shop_top",["id"=>param("id")],$data);
        }else{
            db_insert("special_shop_top",$data);
        }
        message(0, jump('ok', url('plugin-setting-xl_special', array('do' => 'top'))));

    }
}elseif(param('do')=='deletetop'){
    if ($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('tid', 0);
    $r = db_delete('special_shop_top', array('id' => $_codeid));
    $r === FALSE AND message(-1, lang('delete_failed'));
}elseif(param('do') == 'adslider'){
    if ($method == 'GET') {
        include _include(APP_PATH . 'plugin/xl_special/view/adsliderhtml.htm');
    } else {
        $data['album'] = param('thumb');
        $datas['links'] = param('links');

        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $data['album'], $result)) {
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/banner/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/banner/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', $data['album'])))) {
                $datas['img'] = $wwwpath . $new_file;
                $datas['order_id'] = param('order_id');
                $datas['route'] = "platform";
                db_insert("platform_slider",$datas);
                message(0, jump('ok', url('plugin-setting-xl_special', array('do' => 'slider'))));
            }else{
                message(1, jump('图片上传失败，请重试', url('plugin-setting-xl_special', array('do' => 'slider'))));
            }

//            message(0, jump('ok', url('plugin-setting-xl_special', array('do' => 'slider'))));
        } else{
            message(0, jump('请上传图片', url('plugin-setting-xl_special', array('do' => 'slider'))));
        }
    }
}elseif(param('do') == 'slider'){
    $slist = db_find("platform_slider",array('route'=>'platform'),array('order_id'=>'-1'));
    include _include(APP_PATH . "/plugin/xl_special/view/slideradm.htm");
}elseif (param('do') == 'show') {
    $theme_id = param('theme_id');
    db_update("special_shop_new",array('theme_id' =>$theme_id), array('show'=>0));
    message(0, jump('操作成功', url('plugin-setting-xl_special.htm', array('do' => 'shop'))));
    exit;

} elseif (param('do') == 'hide') {
    $theme_id = param('theme_id');
    db_update("special_shop_new",array('theme_id' =>$theme_id), array('show'=>1));
    message(0, jump('操作成功', url('plugin-setting-xl_special.htm', array('do' => 'shop'))));
    exit;

} elseif (param('do') == 'create' && $_POST) {
    $data = array();
    $data['album'] = param('banner');
    $data['album_list'] = param('album_list');
    $data['title'] = param('title');
    $data['title'] = param('title');
    $data['tag'] = param("tag");
    $data['tuid'] = param("tuid");
    $data['zdtid'] = param("zdtid");
    $data['desc'] = param('desc');
    $data['seo_title'] = param('seo_title') ?: $data['title'];
    $data['seo_desc'] = param('seo_desc') ?: $data['desc'];
    $data['seo_keywords'] = param('seo_keywords') ?: $data['title'];
    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $data['album'], $result)) {
        $type = $result[2];
        $time = time();
        $path = "./upload/banner/" . date('Ymd') . "/";
        $ext = '.'.$type;
        $name = uniqid() . $ext;
        if(saveTencentBase64(base64_decode(str_replace($result[1],'', $data['album'])), $path . $name)){
            $data['album'] = I($path . $name);
        }
    }


//    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $data['album_list'], $result2)) {
//        $type = $result2[2];
//        $time = time();
//        $filepath = APP_PATH . "upload/banner/" . date('Ymd', $time) . "/";
//        $wwwpath = "upload/banner/" . date('Ymd', $time) . "/";
//        if (!file_exists($filepath)) {
//            //检查是否有该文件夹，如果没有就创建，并给予最高权限
//            mkdir($filepath, 0700);
//        }
//        $new_file = time() . rand(100, 50000) . "." . $type;
//        if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result2[1], '', $data['album_list'])))) {
//            $data['album_list'] = $wwwpath . $new_file;
//        } else {
//            $data['album_list'] = $wwwpath . "default.png";
//        }
//    } else {
//        $data['album_list'] = param('banner');
//    }

    db_insert("special", $data);
    if (!$data['title']) {
        message(0, jump('请输入标题', url('plugin-setting-xl_special', array('do' => 'add'))));
        exit;
    }
    if (!$data['desc']) {
        message(0, jump('请输入介绍', url('plugin-setting-xl_special', array('do' => 'add'))));
        exit;
    }
    message(0, jump('添加成功', url('plugin-setting-xl_special')));
} elseif (param('do') == 'ecreate' && $_POST) {
    $data = array();
    $spid = param('spid');
    $data['album'] = param('banner');
    $data['album_list'] = param('album_list');
    $data['title'] = param('title');
    $data['desc'] = param('desc');
    $data['seo_title'] = param('seo_title') ?: $data['title'];
    $data['seo_desc'] = param('seo_desc') ?: $data['desc'];
    $data['seo_keywords'] = param('seo_keywords') ?: $data['title'];
    $data['tag'] = param("tag");
    $data['tuid'] = param("tuid");
    $data['zdtid'] = param("zdtid");
    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $data['album'], $result)) {
        $type = $result[2];
        $time = time();
        $path = "./upload/banner/" . date('Ymd') . "/";
        $ext = '.'.$type;
        $name = uniqid() . $ext;
        if(saveTencentBase64(base64_decode(str_replace($result[1],'', $data['album'])), $path . $name)){
            $data['album'] = I($path . $name);
        }
    }



    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $data['album_list'], $result2)) {
        $type = $result2[2];
        $time = time();
        $filepath = APP_PATH . "upload/banner/" . date('Ymd', $time) . "/";
        $wwwpath = "upload/banner/" . date('Ymd', $time) . "/";
        if (!file_exists($filepath)) {
            //检查是否有该文件夹，如果没有就创建，并给予最高权限
            mkdir($filepath, 0700);
        }
        $new_file = time() . rand(100, 50000) . "." . $type;
        if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result2[1], '', $data['album_list'])))) {
            $data['album_list'] = $wwwpath . $new_file;
        } else {
            $data['album_list'] = $wwwpath . "default.png";
        }
    } else {
        $data['album_list'] = param('banner');
    }


    db_update("special", array('sp_id' => $spid), $data);
    message(0, jump('修改成功', url('plugin-setting-xl_special')));
} elseif (param('do') == 'addthread') {
    if ($method == 'GET') {
        $id = param('id', 0);
        $spid = param('sp_id');
        $val = array();
        $val = db_find_one('special_thread', array('id' => $id));
        $val = is_array($val) ? $val : array('id' => '', 'tid' => '', 'order_id' => '');
        include _include(APP_PATH . 'plugin/xl_special/view/addthread.htm');
    } else {

    }
} elseif (param('do') == 2) {
    if ($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('id', 0);
    $r = db_delete('special', array('sp_id' => $_codeid));
    $r === FALSE AND message(-1, lang('delete_failed'));
} elseif (param('do') == 'slider_del') {
    $r = db_delete('platform_slider', array('id' => param('id')));
    message(0, jump('ok', url('plugin-setting-xl_special', array('do' => 'slider'))));
} elseif (param('do') == 'ad_thread') {
    $spid = param('sp_id');
    $page = param('page', 1);
    $total = db_count("special_thraed", array('sp_id' => $spid));
    $pagesize = 20;
    $pagination = pagination(url('plugin-setting-xl_special.htm&do=ad_thread&sp_id=' . $spid), $total, $page, $pagesize);
    $speciallist = db_find("special_thread", array('sp_id' => $spid), array('order_id' => '-1'), $page, 20);
    $speciallist = is_array($speciallist) ? $speciallist : array();
    if (is_array($speciallist) && isset($speciallist[0]['tid'])) {
        foreach ($speciallist as $v) {
            $tids[] = $v['tid'];
        }
        foreach (db_find("thread", array('tid' => $tids)) as $t) {
            $theadinfo[$t['tid']] = $t;
        }
    }
    include _include(APP_PATH . "/plugin/xl_special/view/thread_list.htm");
} elseif (param('do') == 'checkaddthread') {
    $data['tid'] = param('tid');
    $data['sp_id'] = param('spid');
    $data['dateline'] = time();
    $data['order_id'] = param('order_id');
    db_insert('special_thread', $data);
    message(0, 'success');
} elseif (param('do') == 'check') {
    $tid = param('tid');
    $thread = db_find_one("thread", array('tid' => $tid));
    $data['code'] = 300;
    $data['thread'] = array();
    if ($thread['tid']) {
        $data['code'] = 200;
        $data['thread'] = $thread;
    }
    echo json_encode($data);
} elseif (param('do') == 'deletethread') {
    db_delete("special_thread", array('id' => param('tid')));;
    message(0, 'success');
} elseif (param('do') == 'deletetheme') {
    db_delete("special_juhe", array('theme_id' => param('tid')));;
    message(0, 'success');
} elseif (param('do') == 'ed_thread_order') {


    if ($method == 'GET') {
        $spid = param('tid', 0);
        $datas = db_find_one("special_thread", array('id' => $spid));
        include _include(APP_PATH . 'plugin/xl_special/view/ed_thread_order.htm');
    } else {
        db_update("special_thread",array('id'=>param('tid')),array('order_id'=>param('order_id')));
        message(0, 'success');
    }




} elseif (param('do') == 'juhe') {
    $page = param(3, 1);
    $pagesize = 20;
    $n = db_count('special_juhe', array());
    $pagination = pagination(url("plugin-setting-xl_special-{page}", array('do' => 'juhe')), $n, $page, $pagesize);
    $speciallist = db_find('special_juhe', array(), array('order_id' => '-1'), $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_special/view/juhe_list.htm");
} elseif(param('do')=='dagang'){
    $page = param(3, 1);
    $pagesize = 20;
    $n = db_count('special_dagang', array());
    $pagination = pagination(url("plugin-setting-xl_special-{page}", array('do' => 'dagang')), $n, $page, $pagesize);
    $speciallist = db_find('special_dagang', array(), [], $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_special/view/dagang_list.htm");
} elseif (param('do') == 'shop') {
    $page = param(3, 1);
    $pagesize = 20;
    $n = db_count('special_shop_new', array());
    $pagination = pagination(url("plugin-setting-xl_special-{page}", array('do' => 'shop')), $n, $page, $pagesize);
    $speciallist = db_find('special_shop_new', array(), array('hide'=>1,'order_id' => '-1'), $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_special/view/shop_list.htm");
}elseif (param('do') == 'gushi') {
    $page = param(3, 1);
    $pagesize = 20;
    $n = db_count('special_gushi', array());
    $pagination = pagination(url("plugin-setting-xl_special-{page}", array('do' => 'gushi')), $n, $page, $pagesize);
    $speciallist = db_find('special_gushi', array(), [], $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_special/view/gushi_list.htm");
}elseif (param('do') == 'addjuhe') {
    $navid = param('theme_id');
    if ($method == 'GET') {
        $val = array();
        $val = db_find_one('special_juhe', array('theme_id' => $navid));
        include _include(APP_PATH . "/plugin/xl_special/view/addjuhe.htm");
    } else {
        $data['name'] = param('name');
        $data['desc'] = param('desc');
        $data['thumb'] = param('thumb');
        $data['cate'] = param('cate');
        $data['links'] = param('links');

        $data['order_id'] = param('order_id');

        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $data['thumb'], $result2)) {
            $type = $result2[2];
            $time = time();
            $filepath = APP_PATH . "upload/banner/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/banner/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0700);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result2[1], '', $data['thumb'])))) {
                $data['thumb'] = $wwwpath . $new_file;
            } else {
                $data['thumb'] = $wwwpath . "default.png";
            }
        } else {
            $data['thumb'] = param('thumb');
        }

        if (param('theme_id')) {
            db_update('special_juhe', array('theme_id' => param('theme_id')), $data);
        } else {
            db_insert('special_juhe', $data);
        }
        message(0, 'success');
    }
} elseif (param('do') == 'adgushi') {
    $navid = param('gid');
    if ($method == 'GET') {
        $val = array();
        $val = db_find_one('special_gushi', array('gid' => $navid));
        include _include(APP_PATH . "/plugin/xl_special/view/adgushi.htm");
    } else {
        $data['username'] = param('username');
        $data['desc'] = param('desc');
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('avatar'), $res)) {
            $type = $res[2];
            $path = "./upload/index_icon/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('avatar'))), $path . $name)) {
                $data['avatar'] = I($path . $name);
            }
        }

        if (param('gid')) {
            db_update('special_gushi', array('gid' => param('gid')), $data);
        } else {
            db_insert('special_gushi', $data);
        }
        message(0, 'success');
    }
}  elseif (param('do') == 'addshop') {
    $navid = param('theme_id');
    if ($method == 'GET') {
        $val = array();
        $val = db_find_one('special_shop_new', array('theme_id' => $navid));
        $btn = unserialize($val['file_data']);
        $input['hide'] = form_select("hide",["显示","隐藏"],$val['hide']);
        include _include(APP_PATH . "/plugin/xl_special/view/addshop.htm");
    } else {
        $data['name'] = param('name');
        $data['title2'] = param('title2');
        $data['desc'] = param('desc');
        $data['thumb'] = param('thumb');
        $data['cate'] = param('cate');
        $data['links'] = param('links');
        $data['tops'] = param('tops',0);
        $data['hide'] = param('hide',0);
        $data['order_id'] = param('order_id');
        $data['btntextarea'] = param('btntextarea');
//        $file['btn_links1'] = param('btn_links1');
//        $file['btn_txt2'] = param('btn_txt2');
//        $file['btn_links2'] = param('btn_links2');
//        $file['btn_txt3'] = param('btn_txt3');
//        $file['btn_links3'] = param('btn_links3');
//        $data['file_data'] = serialize($file);
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $data['thumb'], $result2)) {
            $type = $result2[2];
            $time = time();
            $filepath = APP_PATH . "upload/banner/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/banner/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0700);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result2[1], '', $data['thumb'])))) {
                $data['thumb'] = $wwwpath . $new_file;
            } else {
                $data['thumb'] = $wwwpath . "default.png";
            }
        } else {
            $data['thumb'] = param('thumb');
        }

        if (param('theme_id')) {
            db_update('special_shop_new', array('theme_id' => param('theme_id')), $data);
        } else {
            db_insert('special_shop_new', $data);
        }
        message(0, 'success');
    }
}elseif(param('do')=='addagang'){
    if ($method == 'GET') {
        $data = [];
        if(param('dgid')){
            $data = db_find_one("special_dagang",["id"=>param('dgid')]);
        }
        $input['name'] = form_text('name', $data['name']);
        $input['title'] = form_text('title', $data['title']);
        $input['desc'] = form_textarea('desc', $data['desc']);
        $input['btn'] = form_text('btn', $data['btn']);
        $input['links'] = form_text('links', $data['links']);
        include _include(APP_PATH . "/plugin/xl_special/view/addagang.htm");
    }else{
        $input['name'] = param('name');
        $input['title'] = param('title');
        $input['desc'] = param('desc');
        $input['btn'] = param('btn');
        $input['links'] = param('links');
        if (param('dgid')) {
            db_update('special_dagang', array('id' => param('dgid')), $input);
        } else {
            db_insert('special_dagang', $input);
        }
        message(0, jump('ok', url('plugin-setting-xl_special', array('do' => 'dagang'))));
    }
}elseif(param('do')=='deleteshop'){
    db_delete('special_shop_new', ["theme_id"=>param('tid')]);
    message(0, 'success');

}else {
    $page = param( 3, 1);
    $pagesize = 40;
    $n = db_count('special', array());
    $pagination = pagination(url("plugin-setting-xl_special-{page}"), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_special-{page}"), $n, $page, $pagesize);
    $speciallist = db_find('special', array(), array(), $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_special/view/list.htm");
}