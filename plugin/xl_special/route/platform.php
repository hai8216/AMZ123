<?php

$spdata['seo_title'] = $header['title'] = "找平台_跨境电商平台开店入驻通道 - AMZ123跨境导航";
$spdata['keywords'] = $header['keywords'] = "跨境电商,跨境电商平台招商,国外电商平台开店,跨境电商平台入驻,账号注册,平台入驻";
$spdata['description'] = $header['description'] = "全球跨境电商平台开店入驻通道全面开启，AMZ123跨境导航为您整理亚马逊、Wish、eBay、Shopee、Shopify、沃尔玛等国外电商平台最新最全招商信息，助力中国卖家扬帆出海！";

$ajax = param('ajax');

$conf = include(APP_PATH . 'conf/conf.php');
$cate = array('主流平台', '新兴平台');
$topdata = db_find("special_shop_top", [], array('order_id' => '-1'), 1, 4);
if (!$ajax) {
    $dagangs = db_find("special_dagang", [], array(), 1, 4);
    $dagang = array_chunk($dagangs, 2);
    $gushi = db_find("special_gushi", [], array(), 1, 6);
    $map['cate'] = 0;
    $map['hide'] = 0;
    $splist = db_find("special_shop_new", $map, array('order_id' => '-1'), 1, 20);
    $splist2 = db_find("special_shop_new", ['cate' => 1,'hide'=>0], array('order_id' => '-1'), 1, 50);
//    print_r($dagang);
    if (check_wap()) {
        include _include(APP_PATH . "/plugin/xl_special/view/special/default/shop_4g.htm");
    } else {
        include _include(APP_PATH . "/plugin/xl_special/view/special/default/shop.htm");
    }
} else {
    $map = [];
    $cateid = param('cate');
    if ($cateid != '99') {
        $map['cate'] = $cateid - 1;
    }
    $page = param('page', 1);
    $pagesize = 10;
    $map['hide'] = 0;
    $splist = db_find("special_shop_new", $map, array('order_id' => '-1'), $page, $pagesize);
    $totalnum = db_count("special_shop_new", $map);
    $totalpage = ceil($totalnum / $pagesize);
    if ($totalpage > 10) {
        $totalpage = 10;
        $min = 10;
    }
    $end = ceil($totalnum / $pagesize);
    include _include(APP_PATH . "/plugin/xl_special/view/special/default/shop_list.htm");
}

function getEmpty($ary)
{
    foreach ($ary as $k => $v) {
        if (!$v) {
            unset($ary[$k]);
        }
    }
    return $ary;
}

function datagroup($ary)
{
    $a[] = array('t' => $ary['btn_txt1'], "l" => $ary['btn_links1']);
    $a[] = array('t' => $ary['btn_txt2'], "l" => $ary['btn_links2']);
    $a[] = array('t' => $ary['btn_txt3'], "l" => $ary['btn_links3']);
    return $a;
}