<?php
if (param('action') == 'userAjax') {
    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
        $type = $res[2];
        $path = "./upload/index_icon/" . date('Ymd') . "/";
        $ext = '.' . $type;
        $name = uniqid() . $ext;
        if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
            db_update("user", ["uid" => $user['uid']], ['banner' => $path . $name]);
        }
    }

} elseif (param('action') == 'top') {
    $tid = param('tid');
    if (!$tid) {
        message(0, '错误参数');
    } else {
        $topdata = db_find_one("thread_usertop", ['tid' => $tid], []);
        if ($topdata['id']) {
            message(0, '已置顶，请在置顶中删除');
        } else {
            $tinfo = thread__read($tid);
            if ($tinfo['uid'] != $user['uid']) {
                message(0, '错误数据');
            } else {
                $udata['tid'] = $tid;
                $udata['uid'] = $user['uid'];
                $udata['subject'] = $tinfo['subject'];
                $udata['dateline'] = time();
                db_insert("thread_usertop", $udata);
                message(200, 'ok');
            }
        }


    }

} elseif (param('action') == 'deltop') {
    $tid = param('tid');
    if (!$tid) {
        message(0, '错误参数');
    } else {
        $topdata = db_find_one("thread_usertop", ['tid' => $tid], []);
        $tinfo = thread__read($tid);
        if ($tinfo['uid'] == $user['uid'] && $topdata['id']) {
            db_delete("thread_usertop", ["tid"=>$tid]);
            message(200, 'ok');
        }
    }
} else {
    $uid = param('uid', $uid);
//$uid = 4;
    $page = param(1, 1);
    $pagesize = 20;
    $skeys =  param('skeys');
    $map['uid'] = $uid;
    $map['is_deleted'] = 0;
    if($skeys){
        $map['subject'] = ["LIKE"=>$skeys] ;
    }
    $threadlist = db_find("thread", $map, array('create_date' => '-1'), $page, $pagesize);
    foreach ($threadlist as $k => $v) {
        $threadlist[$k]['thumb'] = post_thumb_message($v['tid'])['thumb'];
        $threadlist[$k]['message'] = post_thumb_message($v['tid'])['message'];
    }
    if (check_wap()) {
        $totalnum = db_count("thread", array('uid' => $uid, 'is_deleted' => 0));
        $totalpage = ceil($totalnum / $pagesize);
        if ($totalpage > 10) {
            $totalpage = 10;
            $min = 10;
            $end = ceil($totalnum / $pagesize);
        }
        include _include(APP_PATH . "/plugin/xl_special/view/newSpacelist_4g.htm");
    } else {
        include _include(APP_PATH . "/plugin/xl_special/view/newSpacelist.htm");
    }
}
