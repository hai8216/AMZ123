<?php
$spid = param(1);
$spdata = db_find_one("special",array('sp_id'=>$spid));
$spdata['album'] = $spdata['album']?:"https://img.amz123.com/static/images/banner/156518387911128.png";
//获取下属文章信息
$page = param('page',1);
$pagesize = 20;

if (!$spdata['tuid']) {
    $threadlistdata = byTags($spdata['tag'], $page);
} else {
    $threadlistdata = byTagsoruids($spdata['tag'], $spdata['tuid'], $page);
}
//置顶TID
$sql = "SELECT * from bbs_thread where tid in(" . $spdata['zdtid'] . ")   order by create_date desc limit 20";

//    echo $sql;exit;
$threadTop = db_sql_find($sql);
if(!param('loadajax')){
    include _include(APP_PATH . "/plugin/xl_special/view/special/default/index.htm");
}else{
    ob_start();
    include _include(APP_PATH . "/plugin/xl_special/view/special/default/index_ajax.htm");
    $html = ob_get_clean();
    $ajaxdata['page'] = $page;
    $ajaxdata['code'] = 200;
    if (!empty($threadlistdata)) {
        $ajaxdata['c'] = 1;
        $ajaxdata['threadlist'] = $html;
    } else {
        $ajaxdata['c'] = 0;
        $ajaxdata['threadlist'] = "";
    }
    echo json_encode($ajaxdata);
    exit;

}
