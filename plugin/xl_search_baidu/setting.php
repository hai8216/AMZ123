<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') AND exit('Access Denied.');

$page = param(3, 1);
$excel = param('excel');
if ($excel == '1') {
    $sid = param('sid');
    require_once APP_PATH . '/plugin/xl_search_baidu/Classes/PHPExcel.php';
//实例化
    $phpexcel = new PHPExcel();
//设置表头
    $phpexcel->getActiveSheet()->setCellValue('A1', '搜索词')
        ->setCellValue('B1', '搜索时间');

    if ($sid) {
        $sid = db_find_one("baidu_searchlog", array('id' => $sid));

        $beginToday = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
//    echo $beginToday;exit;
        $Re = db_find("baidu_searchlog_more", array('skeys' => $sid['skeys'], 'dateline' => array('>' => $beginToday)), array('dateline' => '-1'), 1, 100000);
        foreach ($Re as $k => $v) {
            $k = $k + 2;
            $phpexcel->getActiveSheet()->setCellValue('A' . $k, $sid['skeys'])
                ->setCellValue('B' . $k, date('Y-m-d H:i', $v['dateline']));
        }
    } else {
        $beginToday = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
//    echo $beginToday;exit;
        $Re = db_find("baidu_searchlog_more", array('dateline' => array('>' => $beginToday)), array('dateline' => '-1'), 1, 100000);
        foreach ($Re as $k => $v) {
            $k = $k + 2;
            $phpexcel->getActiveSheet()->setCellValue('A' . $k, $v['skeys'])
                ->setCellValue('B' . $k, date('Y-m-d H:i', $v['dateline']));
        }
    }


    $obj_Writer = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
    $filename = $sid['skeys'] . "【" . date('Y年m月d日') . "】" . ".xls";//文件名

//设置header

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/download");
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header("Content-Transfer-Encoding: binary");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
    $obj_Writer->save('php://output');//输出
    die();//种植执行
} else if ($excel == '2') {
    $sid = param('sid');
    require_once APP_PATH . '/plugin/xl_search_baidu/Classes/PHPExcel.php';
//实例化
    $phpexcel = new PHPExcel();
//设置表头
    $phpexcel->getActiveSheet()->setCellValue('A1', '搜索词')
        ->setCellValue('B1', '搜索时间');

    if ($sid) {
        $sid = db_find_one("baidu_searchlog", array('id' => $sid));

        $beginToday = strtotime(date("Y-m-01"));
//    echo $beginToday;exit;
        $Re = db_find("baidu_searchlog_more", array('skeys' => $sid['skeys'], 'dateline' => array('>' => $beginToday)), array('dateline' => '-1'), 1, 100000);
        foreach ($Re as $k => $v) {
            $k = $k + 2;
            $phpexcel->getActiveSheet()->setCellValue('A' . $k, $sid['skeys'])
                ->setCellValue('B' . $k, date('Y-m-d H:i', $v['dateline']));
        }
    } else {
        $beginToday = strtotime(date("Y-m-01"));
//    echo $beginToday;exit;
        $Re = db_find("baidu_searchlog_more", array('dateline' => array('>' => $beginToday)), array('dateline' => '-1'), 1, 100000);
        foreach ($Re as $k => $v) {
            $k = $k + 2;
            $phpexcel->getActiveSheet()->setCellValue('A' . $k, $v['skeys'])
                ->setCellValue('B' . $k, date('Y-m-d H:i', $v['dateline']));
        }
    }


    $obj_Writer = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
    $filename = $sid['skeys'] . "【" . date('Y年m月') . "】" . ".xls";//文件名

//设置header

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/download");
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header("Content-Transfer-Encoding: binary");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
    $obj_Writer->save('php://output');//输出
    die();//种植执行
} else if ($excel == '3') {
    $sid = param('sid');
    require_once APP_PATH . '/plugin/xl_search_baidu/Classes/PHPExcel.php';
//实例化
    $phpexcel = new PHPExcel();
//设置表头
    $phpexcel->getActiveSheet()->setCellValue('A1', '搜索词')
        ->setCellValue('B1', '搜索时间');

    if ($sid) {
        $sid = db_find_one("baidu_searchlog", array('id' => $sid));

        $beginToday = strtotime(date("Y-01-01"));
//    echo $beginToday;exit;
        $Re = db_find("baidu_searchlog_more", array('skeys' => $sid['skeys'], 'dateline' => array('>' => $beginToday)), array('dateline' => '-1'), 1, 100000);
        foreach ($Re as $k => $v) {
            $k = $k + 2;
            $phpexcel->getActiveSheet()->setCellValue('A' . $k, $sid['skeys'])
                ->setCellValue('B' . $k, date('Y-m-d H:i', $v['dateline']));
        }
    } else {
        $beginToday = strtotime(date("Y-01-01"));
//    echo $beginToday;exit;
        $Re = db_find("baidu_searchlog_more", array('dateline' => array('>' => $beginToday)), array('dateline' => '-1'), 1, 100000);
        foreach ($Re as $k => $v) {
            $k = $k + 2;
            $phpexcel->getActiveSheet()->setCellValue('A' . $k, $v['skeys'])
                ->setCellValue('B' . $k, date('Y-m-d H:i', $v['dateline']));
        }
    }


    $obj_Writer = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
    $filename = $sid['skeys'] . "【" . date('Y年') . "】" . ".xls";//文件名

//设置header

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/download");
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header("Content-Transfer-Encoding: binary");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
    $obj_Writer->save('php://output');//输出
    die();//种植执行
} else {
    $pagesize = 40;
    $n = db_count('baidu_searchlog', array());
    $pagination = pagination(url("plugin-setting-xl_search_baidu-{page}", array('order' => param('order'))), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_search_baidu-{page}", array('order' => param('order'))), $n, $page, $pagesize);
    if (param('order')) {
        $speciallist = db_find('baidu_searchlog', array(), array('dateline' => '-1'), $page, $pagesize);
    } else {
        $speciallist = db_find('baidu_searchlog', array(), array('count' => '-1'), $page, $pagesize);
        foreach ($speciallist as $k => $v) {
            $speciallist[$k]['dateline'] = db_find_one("baidu_searchlog_more", array('skeys' => $v['skeys']), array('id' => '-1'))['dateline'];
        }
    }

    include _include(APP_PATH . "/plugin/xl_search_baidu/list.htm");
}
