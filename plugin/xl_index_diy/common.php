<?php
/*** Created by xuxiaolong.* weibo:@Sinamfyoyo* User: 许小龙* Date: 2020-10-20* Time: 17:52* Developer:商业定制联系QQ95327294 */

function get_siteurl_curlinfo($url = 'https://mimvp.com', $timeout = 5, $conntimeout = 3)
{
    $ch = curl_init();
    if (preg_match('/^http(s)?:\\/\\/.+/', $url)) {
        $url = $url;
    } else {
        $url = "https://" . $url;
    }

    $url_host = explode("/", $url)[2];
    $header = array();

    array_push($header, 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36');

    array_push($header, 'Referer:' . $url);

    array_push($header, 'host:' . $url_host);

    array_push($header, 'accept:  text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');

    array_push($header, 'upgrade-insecure-requests:1');

    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    curl_setopt($ch, CURLOPT_AUTOREFERER, 1);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
// HTTP 头中的 "Location: "重定向

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// 字符串返回

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
// https请求 不验证证书和hosts

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

    curl_setopt($ch, CURLOPT_HEADER, 1);
// 0表示不输出Header，1表示输出

    curl_setopt($ch, CURLOPT_NOBODY, 0);
// 0表示不输出Body，1表示输出

    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $conntimeout);
// 尝试连接时等待的秒数。设置为0，则无限等待

    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout + 5);
// 允许 cURL 函数执行的最长秒数

    curl_setopt($ch, CURLOPT_URL, $url);

    $output = curl_exec($ch);

    $curl_info = curl_getinfo($ch);

    curl_close($ch);

    $curl_info   ['page_info'] = get_page_info($output, $curl_info);

    return $curl_info;


}

function get_page_info($output)
{

    $page_info = array();

    $page_info   ['site_title'] = '';

    $page_info   ['site_description'] = '';

    $page_info   ['site_keywords'] = '';

    $page_info   ['friend_link_status'] = 0;

    $page_info   ['site_claim_status'] = 0;

    $page_info   ['site_home_size'] = 0;

    if (empty   ($output)) return $page_info;


# Keywords

    preg_match('/<META\s+name=["\']keywords[\'"]\s+content=["\']?([\w\W]*?)["\']/si', $output, $matches);

    if (empty   ($matches   [1])) {

        preg_match('/<META\s+content=["\']([\w\W]*?)["\']\s+name=["\']keywords["\']/si', $output, $matches);

    }

    if (empty   ($matches   [1])) {

        preg_match('/<META\s+http-equiv=["\']keywords["\']\s+content=["\']([\w\W]*?)["\']/si', $output, $matches);

    }

    if (empty   ($matches   [1])) {

        preg_match('/<META\s+content=["\']keywords["\']\s+http-equiv=["\']([\w\W]*?)["\']/si', $output, $matches);

    }

    if (!empty   ($matches   [1])) {

        $page_info['site_keywords'] = $matches    [1];

    }


# Description

    preg_match('/<META\s+name=["\']description["\']\s+content=["\']([\w\W]*?)["\']/si', $output, $matches);

    if (empty   ($matches   [1])) {

        preg_match('/<META\s+content=["\']([\w\W]*?)["\']\s+name=["\']description["\']/si', $output, $matches);

    }

    if (empty   ($matches   [1])) {

        preg_match('/<META\s+http-equiv=["\']description["\']\s+content=["\']([\w\W]*?)["\']/si', $output, $matches);

    }

    if (empty   ($matches   [1])) {

        preg_match('/<META\s+content=["\']description["\']\s+http-equiv=["\']([\w\W]*?)["\']/si', $output, $matches);

    }

    if (!empty   ($matches   [1])) {

        $page_info['site_description'] = $matches    [1];

    }


# Title

    preg_match('/<TITLE>([\w\W]*?)<\/TITLE>/si', $output, $matches);

    if (!empty   ($matches   [1])) {

        $page_info['site_title'] = $matches    [1];

    }


# mimvp-site-verification

    preg_match('/<META\s+name=["\']mimvp-site-verification["\']\s+content=["\']([\w\W]*?)["\']/si', $output, $matches);

    preg_match('/<META\s+name="mimvp-site-verification"\s+content="([\w\W]*?)"/si', $output, $matches);

    if (empty   ($matches   [1])) {

        preg_match('/<META\s+content=["\']([\w\W]*?)["\']\s+name=["\']mimvp-site-verification["\']/si', $output, $matches);

    }

    if (!empty   ($matches   [1])) {

        $page_info['site_claim_status'] = $matches    [1];

    }


    $page_info['site_home_size'] = strlen($output);
    foreach ($page_info as $k => $v) {
        //转码
        $page_info[$k] = characet($v);
    }
    return $page_info;


}

function characet($data)
{
    if (!empty($data)) {
        $fileType = mb_detect_encoding($data, array('UTF-8', 'GBK', 'LATIN1', 'BIG5'));
        if ($fileType != 'UTF-8') {
            $data = mb_convert_encoding($data, 'utf-8', $fileType);
        }
    }
    return $data;
}

function get_url_ico($url)
{

    $url_arr = parse_url($url);
    if (!isset($url_arr['scheme'])) {
        $url = "http://" . $url;
    } else {
        $url = $url_arr['scheme'] . "://" . $url_arr['host'];
    }

    if (url_exists($url)) {
        $api_url = "https://ico.di8du.com/get.php?url=" . $url;
        $ico = $url . "/favicon.ico";
        if (remote_file_exists($api_url)) {
            //保存到本地
            $path = "./upload/ico/" . date('Y-m-d');
            if (!file_exists(XIUNOPHP_PATH . "../" . $path)) mkdir(XIUNOPHP_PATH . "../" . $path, 0755, true);
            $ext = '.png';
            $name = uniqid() . $ext;
            if (saveIco($api_url, XIUNOPHP_PATH . "../" . $path . "/" . $name)) {
                return $path . "/" . $name;
            } else {
                return "plugin/xl_index_diy/empty-sour-icon.svg";
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function url_exists($url)
{
    $head = @get_headers($url);
    return is_array($head) ? true : false;
}

function remote_file_exists($url)
{
    $executeTime = ini_get('max_execution_time');
    ini_set('max_execution_time', 0);
    $headers = @get_headers($url);
    ini_set('max_execution_time', $executeTime);
    if ($headers) {
        $head = explode(' ', $headers[0]);
        if (!empty($head[1]) && intval($head[1]) < 400) return true;
    }
    return false;
}

function saveIco($url, $name)
{
    $r = null;
    if (function_exists("curl_init") && function_exists('curl_exec')) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if (ini_get("safe_mode") == false && ini_get("open_basedir") == false) {
            curl_setopt($ch, CURLOPT_MAXREDIRS, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        }
        if (extension_loaded('zlib')) {
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        $r = curl_exec($ch);
        curl_close($ch);
    } elseif (ini_get("allow_url_fopen")) {
        if (function_exists('ini_set')) ini_set('default_socket_timeout', 300);
        $r = file_get_contents((extension_loaded('zlib') ? 'compress.zlib://' : '') . $url);
    }

    if (strlen($r)>10) {
        $fp = @fopen($name, "w");

        fwrite($fp, $r);

        fclose($fp);

        return true;
    } else {
        return false;
    }
}