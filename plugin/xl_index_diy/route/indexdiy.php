<?php
if (!$user['uid']) {
    exit('not login');
}
$action = param(1);
include XIUNOPHP_PATH . "../plugin/xl_index_diy/common.php";

if ($action == 'getWeb') {
    $url = param('weburl');
    //查询网址库
    $isDBdata = db_find_one("index_diy_url", array('weburl' => $url));
    if ($isDBdata['id']) {
        $dbdata['page_info']['site_title'] = $isDBdata['webname'];
        $dbdata['ico'] = $isDBdata['webicon'];
        $data['data'] = $dbdata;
        $data['ico'] = $isDBdata['webicon'];
    } else {
        $data['data'] = get_siteurl_curlinfo($url);
        $data['ico'] = get_url_ico($url);
    }

    $data['code'] = 200;
    echo json_encode($data);
    exit;
} elseif ($action == 'postData') {
    //查询是否私有化
    $selfCount = db_count("index_diy_cate", array('uid' => $user['uid']));
    if (!$selfCount) {
        $publicCate = db_find("index_diy_cate", array('sub' => 1, 'uid' => 0), array('orderid' => '-1'), 1, 10);
        foreach ($publicCate as $v) {
            //私有化导航
            $fuid = db_insert("index_diy_cate", array('uid' => $user['uid'], 'cateName' => $v['cateName'], 'sub' => 0, 'orderid' => $v['orderid']));
            //私有化数据
            $defNavdata = db_find("index_diy_cate_data", array('fup_cateid' => $v['id'],'is_deleted' => 0), [], 1, 200);
            foreach ($defNavdata as $va) {
                db_insert("index_diy_cate_data", array(
                    'weburl' => $va['weburl'],
                    'fup_cateid' => $fuid,
                    'user_id' => $user['uid'],
                    'webname' => $va['webname'],
                    'webico' => $va['webico'],
                    'order_id' => $va['order_id'],
                    'sub' => 0,
                    'top' => $va['id'], //复制来源
                ));
            }
        }
    }
    $data['webname'] = param('webname');
    $data['weburl'] = param('weburl');
    $data['user_id'] = $user['uid'];
    if (!$data['webname'] || !$data['weburl'] || !$user['uid']) {
        $data['code'] = 100;
        $data['msg'] = "内容不足";
        echo json_encode($data);
        exit;
    }
    $data['order_id'] = substr(time(), 1, 10);
    if (preg_match('/^(data:\s*image\/(\S+);base64,)/', param('webicon'), $result)) {
        $type = $result[2];
        $time = time();
        $filepath = APP_PATH . "upload/ico/" . date('Ymd', $time) . "/";
        $wwwpath = "upload/ico/" . date('Ymd', $time) . "/";
        if (!file_exists($filepath)) {
            //检查是否有该文件夹，如果没有就创建，并给予最高权限
            mkdir($filepath, 0777, true);
        }
        $new_file = time() . rand(100, 50000) . "." . $type;
        if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('webicon'))))) {
            $data['webico'] = $wwwpath . $new_file;
        }
    } else {
        $data['webico'] = param('webicon');
    }


    $data['fup_cateid'] = param('fup_cateid');
    if (param('editid')) {

        //判断是否有权限
        $dataPro = db_find_one("index_diy_cate_data", array('id' => param('editid')));
        if ($dataPro['sub'] == 1) {
            //编辑公共化导航，若私有化所有
            $dataPro = db_find_one("index_diy_cate_data", array('webname' => $dataPro['webname'], 'user_id' => $user['uid']));
            $data['fup_cateid'] = $dataPro['fup_cateid'];
        }

        //私有化后重新执行查询

        if ($dataPro['user_id'] == $user['uid']) {
            unset($data['order_id']);
            //只要存在编辑就私有化
            db_update("index_diy_cate_data", array('id' => $dataPro['id']), $data);
            $data['code'] = 200;
            echo json_encode($data);
            exit;

        } else {
            $data['code'] = 100;
            $data['msg'] = "权限不足";
            echo json_encode($data);
            exit;
        }
    } else {
//        if (!$selfCount) {
//
//            $fupid2 = db_find_one("index_diy_cate",['cateName' => $fupid['cateName'],'uid'=>$user['uid']]);
//            $count = db_count("index_diy_cate_data", array('user_id' => $user['uid'], 'fup_cateid' =>$fupid2['id']));
//            $data['fup_cateid'] = $fupid2['id'];
//        }else{
        $fupid = db_find_one("index_diy_cate", ['id' => param('fup_cateid')]);
        if ($fupid['sub'] == 1) {
            //若是公共的
            $fupid2 = db_find_one("index_diy_cate", ['cateName' => $fupid['cateName'], 'uid' => $user['uid']]);
            $fupid2 = db_find_one("index_diy_cate", ['cateName' => $fupid['cateName'], 'uid' => $user['uid']]);
            $data['fup_cateid'] = $fupid2['id'];
        } else {
            $count = db_count("index_diy_cate_data", array('user_id' => $user['uid'], 'fup_cateid' => param('fup_cateid')));
        }

//        }

        if ($count >= 50) {
            $data['code'] = 100;
            $data['msg'] = "每个分类最多支持50个导航";
            echo json_encode($data);
            exit;
        }
        $data['id'] = db_insert("index_diy_cate_data", $data);
        if ($data['id']) {
            //保存到网址库
            //网址库如果存在不保存
            $isDBdata = db_find_one("index_diy_url", array('weburl' => $data['weburl']));
            if (!$isDBdata['id']) {
                db_insert("index_diy_url", array('webname' => $data['webname'], 'webicon' => $data['webico'], 'weburl' => $data['weburl']));
            }
            $data['code'] = 200;
            echo json_encode($data);
            exit;
        }
    }

} elseif
($action == 'updateOrder') {
    //查询是否私有化
    $selfCount = db_count("index_diy_cate", array('uid' => $user['uid']));
    if (!$selfCount) {
        $publicCate = db_find("index_diy_cate", array('sub' => 1, 'uid' => 0), array('orderid' => '-1'), 1, 10);
        foreach ($publicCate as $v) {
            //私有化导航
            $fuid = db_insert("index_diy_cate", array('uid' => $user['uid'], 'cateName' => $v['cateName'], 'sub' => 0, 'orderid' => $v['orderid']));
            //私有化数据
            $defNavdata = db_find("index_diy_cate_data", array('fup_cateid' => $v['id'],'is_deleted' => 0), [], 1, 200);
            foreach ($defNavdata as $va) {
                db_insert("index_diy_cate_data", array(
                    'weburl' => $va['weburl'],
                    'fup_cateid' => $fuid,
                    'user_id' => $user['uid'],
                    'webname' => $va['webname'],
                    'webico' => $va['webico'],
                    'order_id' => $va['order_id'],
                    'sub' => 0,
                    'top' => $va['id'], //复制来源
                ));
            }
        }
    }


    foreach ($_POST['orderArray'] as $k => $v) {
        if($k<70000){
            $w1 = db_find_one("index_diy_cate_data", array('id' => $k));
            $w2 = db_find_one("index_diy_cate_data", array('weburl' => $w1['weburl'],'webname'=>$w1['webname'],'user_id'=>$user['uid']));
            $k = $w2['id'];
        }
        $map['id'] = $k;
        $data['order_id'] = $v;
        db_update("index_diy_cate_data", $map, $data);
    }
} elseif ($action == 'del') {
    //查询是否私有化
    $selfCount = db_count("index_diy_cate", array('uid' => $user['uid']));
    if (!$selfCount) {
        $publicCate = db_find("index_diy_cate", array('sub' => 1, 'uid' => 0), array('orderid' => '-1'), 1, 10);
        foreach ($publicCate as $v) {
            //私有化导航
            $fuid = db_insert("index_diy_cate", array('uid' => $user['uid'], 'sub' => 0, 'orderid' => $v['orderid']));
            //私有化数据
            $defNavdata = db_find("index_diy_cate_data", array('fup_cateid' => $v['id'],'is_deleted' => 0), [], 1, 200);
            foreach ($defNavdata as $va) {
                db_insert("index_diy_cate_data", array(
                    'weburl' => $va['weburl'],
                    'fup_cateid' => $fuid,
                    'user_id' => $user['uid'],
                    'webname' => $va['webname'],
                    'webico' => $va['webico'],
                    'order_id' => $va['order_id'],
                    'sub' => 0,
                    'top' => $va['id'], //复制来源
                ));
            }
        }
    }
    if (db_delete("index_diy_cate_data", array('id' => param('dataid'), 'user_id' => $user['uid']))) {
        $data['code'] = 200;
        echo json_encode($data);
        exit;
    }
}
