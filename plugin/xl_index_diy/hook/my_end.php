<?php
exit;
else
if

($action == 'diysetting') {
    $selfCount = db_count("index_diy_cate", array('uid' => $user['uid']));
    if (!$selfCount) {
        $publicCate = db_find("index_diy_cate", array('sub' => 1, 'uid' => 0), array('orderid' => '-1'), 1, 10);
        foreach ($publicCate as $v) {
            //私有化导航
            $fuid = db_insert("index_diy_cate", array('uid' => $user['uid'], 'cateName' => $v['cateName'], 'sub' => 0, 'orderid' => $v['orderid']));
            //私有化数据
            $defNavdata = db_find("index_diy_cate_data", array('fup_cateid' => $v['id'],'is_deleted' => 0), [], 1, 200);
            foreach ($defNavdata as $va) {
                db_insert("index_diy_cate_data", array(
                    'weburl' => $va['weburl'],
                    'fup_cateid' => $fuid,
                    'user_id' => $user['uid'],
                    'webname' => $va['webname'],
                    'webico' => $va['webico'],
                    'order_id' => $va['order_id'],
                    'sub' => 0,
                    'top' => $va['id'], //复制来源
                ));
            }
        }
    }
    $page = param(2, 0);
    $diy_nav_cate_def = getDiyNav($user['uid']);
    $cateid = param('cateid', $diy_nav_cate_def[0]['id']);
    $page = !$page ? 1 : $page;
    $pagesize = 24;
    $listnum = db_count('index_diy_cate_data', array('user_id' => $user['uid'], 'fup_cateid' => $cateid));
    $pagination = pagination(url("my-diysetting-{page}"), $listnum, $page, $pagesize);
    $list = db_find("index_diy_cate_data", array('user_id' => $user['uid'], 'fup_cateid' => $cateid,'is_deleted' => 0), array('order_id' => '1'), $page, $pagesize);
    $isOpen = db_find_one('user', array('uid' => $user['uid']))['index_diy_radio'];
    include _include(APP_PATH . "plugin/xl_index_diy/view/htm/diylist.htm");
} elseif ($action == 'cate') {
    $selfCount = db_count("index_diy_cate", array('uid' => $user['uid']));
    if (!$selfCount) {
        $publicCate = db_find("index_diy_cate", array('sub' => 1, 'uid' => 0), array('orderid' => '-1'), 1, 10);
        foreach ($publicCate as $v) {
            //私有化导航
            $fuid = db_insert("index_diy_cate", array('uid' => $user['uid'], 'cateName' => $v['cateName'], 'sub' => 0, 'orderid' => $v['orderid']));
            //私有化数据
            $defNavdata = db_find("index_diy_cate_data", array('fup_cateid' => $v['id'],'is_deleted' => 0), [], 1, 200);
            foreach ($defNavdata as $va) {
                db_insert("index_diy_cate_data", array(
                    'weburl' => $va['weburl'],
                    'fup_cateid' => $fuid,
                    'user_id' => $user['uid'],
                    'webname' => $va['webname'],
                    'webico' => $va['webico'],
                    'order_id' => $va['order_id'],
                    'sub' => 0,
                    'top' => $va['id'], //复制来源
                ));
            }
        }
    }
    $diy_nav_cate_def = getDiyNav($user['uid']);
    include _include(APP_PATH . "plugin/xl_index_diy/view/htm/cate.htm");
} elseif($action=='addnavata'){
    $data['webname'] = param('webname');
    $data['weburl'] = param('weburl');
    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('webico'), $result)) {
        $type = $result[2];
        $time = time();
        $filepath = APP_PATH . "upload/ico/" . date('Ymd', $time) . "/";
        $wwwpath = "upload/ico/" . date('Ymd', $time) . "/";
        if (!file_exists($filepath)) {
            //检查是否有该文件夹，如果没有就创建，并给予最高权限
            mkdir($filepath, 0777, true);
        }
        $new_file = time() . rand(100, 50000) . "." . $type;
        if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('webico'))))) {
            $data['webico'] = $wwwpath . $new_file;
        }
    }
    if (param('id')) {
        db_update('index_diy_cate_data', array('id' => param('id'),'user_id'=>$user['uid']), $data);
    } else {
        $data['user_id'] = $user['uid'];
        $data['order_id'] = "99999999";
        $data['fup_cateid'] = param('fup_cateid');
        db_insert('index_diy_cate_data', $data);
    }
    message(0, 'success');
}elseif
($action == 'diysaveorder') {
    $order = array_filter(explode(',', param('order')));
    $toid = array_filter(explode(',', param('newordertoid')));
    foreach ($toid as $k => $v) {
        db_update("index_diy_cate", array('id' => $v), array('orderid' => $order[$k]));
    }
} elseif
($action == 'diysavedata') {

    $order = array_filter(explode(',', param('order')));
    $toid = array_filter(explode(',', param('newordertoid')));

    foreach ($toid as $k => $v) {
        db_update("index_diy_cate_data", array('id' => $v), array('order_id' => $order[$k]));
    }
} elseif
($action == 'diysaveradio') {
    $radio = param('radio');
    db_update("user", array('uid' => $user['uid']), array('index_diy_radio' => $radio));
} elseif
($action == 'addajax') {
    $id = param('toid');
    $val = db_find_one('index_diy', array('id' => $id));
    include _include(APP_PATH . "plugin/xl_index_diy/view/htm/addajax.htm");
} elseif ($action == 'delcate') {
    db_delete("index_diy_cate", array('id' => param('id'), 'uid' => $user['uid']));
    //同时删除下面的数据
    db_delete("index_diy_cate_data", array('fup_cateid' => param('id'), 'user_id' => $user['uid']));
    message(0, jump('删除成功', url('my-diysetting')));
} elseif($action=='deldata') {
    db_delete("index_diy_cate_data", array('id' => param('id'), 'user_id' => $user['uid']));
    message(0, jump('删除成功', url('my-diysetting')));
}elseif
($action == 'addajaxs') {
    $id = param('toid');
    $fup_cateid = param('fup_cateid');
    $val = db_find_one('index_diy_cate_data', array('id' => $id));
    include _include(APP_PATH . "plugin/xl_index_diy/view/htm/addajaxs.htm");
} elseif ($action == 'addcates') {
    if ($method == 'GET') {
        $val = array();
        if (param('cateid')) {
            $val = db_find_one('index_diy_cate', array('id' => param('cateid')));
        } else {
            $count = db_count('index_diy_cate', array('uid' => $user['uid']));
            if($count>=6){
                message(0,'你最多只能创建6个分类');
            }
            $val = [];
        }
        include _include(APP_PATH . 'plugin/xl_index_diy/view/htm/addcate.html');
    } else {
        $data['cateName'] = param('cateName');
        if (param('id')) {
            db_update('index_diy_cate', array('id' => param('id')), $data);
        } else {
            $data['uid'] = $user['uid'];
            $data['orderid'] = "0";
            db_insert('index_diy_cate', $data);
        }
        message(0, 'success');
    }
} elseif
($action == 'delete') {
    db_delete("index_diy", array('uid' => $user['uid'], 'id' => param('toid')));
    message(0, jump('删除成功', url('my-diysetting')));
}  elseif
($action == 'close') {
    $selfCount = db_count("index_diy_cate", array('uid' => $user['uid']));
    if (!$selfCount) {
        $publicCate = db_find("index_diy_cate", array('sub' => 1, 'uid' => 0), array('orderid' => '-1'), 1, 10);
        foreach ($publicCate as $v) {
            //私有化导航
            $fuid = db_insert("index_diy_cate", array('uid' => $user['uid'], 'cateName' => $v['cateName'], 'sub' => 0, 'orderid' => $v['orderid']));
            //私有化数据
            $defNavdata = db_find("index_diy_cate_data", array('fup_cateid' => $v['id'],'is_deleted' => 0), [], 1, 200);
            foreach ($defNavdata as $va) {
                db_insert("index_diy_cate_data", array(
                    'weburl' => $va['weburl'],
                    'fup_cateid' => $fuid,
                    'user_id' => $user['uid'],
                    'webname' => $va['webname'],
                    'webico' => $va['webico'],
                    'order_id' => $va['order_id'],
                    'sub' => 0,
                    'top' => $va['id'], //复制来源
                ));
            }
        }
    }
    include _include(APP_PATH . "plugin/xl_index_diy/view/htm/close.htm");
} elseif ($action == 'diydoadd') {
    $ary['links'] = param('links');
    if (strpos($ary['links'], 'http') !== false) {
        $ary['links'] = param('links');
    } else {
        $ary['links'] = "http://" . param('links');
    }
    $ary['title'] = param('title');
    $ary['uid'] = $user['uid'];
    $host = parse_url(urldecode($ary['links']))['host'];
    if (param('id')) {
        $path = "./upload/remote/favicon/" . date('Y-m-d') . "/";
        if (!file_exists($path)) mkdir($path, 0755, true);
        $name = param('id') . ".jpg";
        saveImg("http://api.byi.pw/favicon/?url=" . $ary['links'], $path . "/" . $name);
        $img = $path . "/" . $name;
        db_update("index_diy", array('id' => param('id')), array('title' => $ary['title'], 'links' => $ary['links'], 'favicon' => $img, 'host' => $host));

    } else {
        $nid = db_insert('index_diy', $ary);
        //http://www.baidu.com
        $path = "./upload/remote/favicon/" . date('Y-m-d') . "/";
        if (!file_exists($path)) mkdir($path, 0755, true);
        $name = $nid . ".jpg";
        saveImg("http://api.byi.pw/favicon/?url=" . $ary['links'], $path . "/" . $name);
        $img = $path . "/" . $name;
        db_update("index_diy", array('id' => $nid), array('order_id' => $nid, 'favicon' => $img, 'host' => $host));
    }


    message(0, 'success');
}


?>
