<?php
!defined('DEBUG') AND exit('Access Denied.');
include XIUNOPHP_PATH . "../plugin/xl_index_diy/common.php";
$pluginname = 'xl_index_diy';
$action = param(3) ?: "cate";
if ($action == 'cate') {
    $type = param(4, 'def');
    $page = param(5, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    if ($type == 'def') {
        $map['sub'] = 1;
        $map['uid'] = 0;
    } else {
//        $map['sub'] = 1;
        $map['uid'] = array('>' => 0);
        if (param('uid')) {
            $map['uid'] = param('uid');
        }
    }

    $arrlist = db_find("index_diy_cate", $map, array('orderid' => '-1'), $page, 30);
    $c = db_count("index_diy_cate", $map);
    if (param('uid')) {
        $pagination = pagination(url("plugin-setting-xl_index_diy-cate-other-{page}", array('uid' => param('uid'))), $c, $page, $pagesize);
    } else {
        $pagination = pagination(url("plugin-setting-xl_index_diy-cate-other-{page}"), $c, $page, $pagesize);
    }

    include _include(APP_PATH . 'plugin/xl_index_diy/admincp/cate.html');
} elseif ($action == 'addcate') {
    if ($method == 'GET') {
        $val = array();
        $val = db_find_one('index_diy_cate', array('id' => param('cateid')));
        include _include(APP_PATH . 'plugin/' . $pluginname . '/admincp/addcate.html');
    } else {
        $data['cateName'] = param('cateName');
        $data['sub'] = 1;
        $data['orderid'] = param('orderid');
        if (param('cateid')) {
            db_update('index_diy_cate', array('id' => param('cateid')), $data);
        } else {
            db_insert('index_diy_cate', $data);
        }
        message(0, 'success');
    }

} elseif ($action == 'cateadmincp') {
    $map['fup_cateid'] = param('cateid');
    $map['user_id'] = 0;
    $map['is_deleted'] = 0;
    $page = param(4, 1);
    $devalue = db_find_one('index_diy_cate', array('id' => param('cateid')));
    $arrlist = db_find("index_diy_cate_data", $map, array('order_id' => '-1'), $page, 20);
    $c = db_count("index_diy_cate_data", $map);
    $pagination = pagination(url("plugin-setting-xl_index_diy-cateadmincp-{page}", array('cateid' => $map['fup_cateid'])), $c, $page, 20);
    include _include(APP_PATH . 'plugin/xl_index_diy/admincp/cateadmincp.html');
} elseif ($action == 'weburl') {
    $page = param(4, 1);

    $srchtype = param('srchtype');
    $keyword = param('keyword');
    if ($keyword) {
        if ($srchtype == 'url') {
            $map['weburl'] = array('LIKE' => $keyword);
        } else {
            $map['webname'] = array('LIKE' => $keyword);
        }
        $ul = url("plugin-setting-xl_index_diy-weburl-{page}", array('srchtype' => $srchtype, 'keyword' => $keyword));
    } else {
        $ul = url("plugin-setting-xl_index_diy-weburl-{page}");
    }
    $arrlist = db_find("index_diy_url", $map, array('id' => '-1'), $page, 20);
    $c = db_count("index_diy_url", $map);
    $pagination = pagination($ul, $c, $page, 20);
    include _include(APP_PATH . 'plugin/xl_index_diy/admincp/weburl.html');
} elseif ($action == 'adddata') {
    if ($method == 'GET') {
        $fup_cateid = param('cateid');
        $val = array();
        $val = db_find_one('index_diy_cate_data', array('id' => param('data_id')));
        include _include(APP_PATH . 'plugin/' . $pluginname . '/admincp/adddata.html');
    } else {
        $data['fup_cateid'] = param('fup_cateid');
        $data['webname'] = param('webname');
        $data['weburl'] = param('weburl');
        $data['order_id'] = param('order_id');
        if (preg_match('/^(data:\s*image\/(\S+);base64,)/', param('webico'), $result)) {
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/ico/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/ico/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('webico'))))) {
                $data['webico'] = $wwwpath . $new_file;
            }
        } else {
            $data['webico'] = param('webico');
        }
        $data['top'] = param('1');
        if (param('id')) {
            db_update('index_diy_cate_data', array('id' => param('id')), $data);
        } else {
            db_insert('index_diy_cate_data', $data);
        }
        message(0, url('plugin-setting-xl_index_diy-cateadmincp', array('cateid' => $data['fup_cateid'])));
    }
} elseif ($action == 'deldata') {
    db_delete('index_diy_cate_data', array('id' => param('id')));
    message(0, 'success');
} elseif ($action == 'ajaxcheck') {
    $url = param('weburl');
    $data['data'] = get_siteurl_curlinfo($url);
    $data['ico'] = get_url_ico($url);
    $data['code'] = 200;
    echo json_encode($data);
    exit;
} elseif ($action == 'delcate') {
    db_delete('index_diy_cate', array('id' => param('id')));
    db_delete("index_diy_cate_data", array('fup_cateid' => param('id')));
    message(0, 'success');
} elseif ($action == 'adweburl') {
    if ($method == 'GET') {
        $id = param('cateid');
        $val = array();
        $val = db_find_one('index_diy_url', array('id' => $id));
        include _include(APP_PATH . 'plugin/' . $pluginname . '/admincp/adweburl.html');
    } else {
        $data['webname'] = param('webname');
        $data['weburl'] = param('weburl');
        if (preg_match('/^(data:\s*image\/(\S+);base64,)/', param('webicon'), $result)) {
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/ico/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/ico/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('webicon'))))) {
                $data['webicon'] = $wwwpath . $new_file;
            }
        } else {
            $data['webicon'] = param('webicon');
        }
        if (param('id')) {
            db_update('index_diy_url', array('id' => param('id')), $data);
        } else {
            db_insert('index_diy_url', $data);
        }
        message(0, 'success');
    }
} elseif ($action == 'delweburl') {
    db_delete('index_diy_url', array('id' => param('id')));
    message(0, 'success');
} else {
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    if (param('uid')) {
        $map['user_id'] = param('uid');
    } else {
        $map['user_id'] = array('>' => 0);
    }

    $map['is_deleted'] = 0;
    $arrlist = db_find("index_diy_cate_data", $map, array('order_id' => '-1'), $page, 30);
    $c = db_count("index_diy_cate_data", $map);
    $pagination = pagination(url("plugin-setting-xl_index_diy-default-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_index_diy/admincp.html');
}


?>
