<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>跳转中……</title>
</head>
<?php
error_reporting(0);
require_once dirname(__FILE__).'/service/AlipayTradeService.php';
require_once dirname(__FILE__).'/buildermodel/AlipayTradePagePayContentBuilder.php';
$conf = include '../../../../conf/conf.php';
	$out_trade_no = trim($_GET['orderid']);
	$subject = trim(urldecode($_GET['ordername']));
	$total_amount = abs($_GET['money']);
	$para = trim(urldecode($_GET['msg']));
	include '../../../../xiunophp/xiunophp.php';
	$info = explode("|",$para);
	$type = $info[0];
	$uid = $info[1];
	$user = $info[2];
	$rmbs = $total_amount * (int)$conf['pay'];
	$iscreate = db_count('pay_list',array('orderid'=>$out_trade_no));
	if($iscreate){exit("<script>alert('订单已存在，请勿重复提交。');window.location.href='/my-pay.htm';</script>");}
	$oid = db_create('pay_list',array('uid'=>$uid,'username'=>$user,'money'=>$total_amount,'rmbs'=>$rmbs,'orderid'=>$out_trade_no,'pay_from'=>'网页支付宝'));
	if(!$oid){exit("<script>alert('订单创建失败，请重试。');window.location.href='/my-pay.htm';</script>");}
	$host = (is_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/";
	$config = array (
	'app_id' => $conf['alipay_app_id'],
	'merchant_private_key' => $conf['alipay_private_key'],
	'notify_url' => $host . "plugin/xy_pay/alipay/notify_url.php",
	'return_url' => $host . ($type == 1 ? 'my-payinfo.htm' : 'reward.htm'),
	'charset' => "UTF-8",
	'sign_type'=>"RSA2",
	'gatewayUrl' => "https://openapi.alipay.com/gateway.do",
	'alipay_public_key' => $conf['alipay_public_key'],
	);
	$body = '';
	$payRequestBuilder = new AlipayTradePagePayContentBuilder();
	$payRequestBuilder->setBody($body);
	$payRequestBuilder->setSubject($subject);
	$payRequestBuilder->setTotalAmount($total_amount);
	$payRequestBuilder->setOutTradeNo($out_trade_no);
	$payRequestBuilder->setpassback_params($para);
	$aop = new AlipayTradeService($config);
	$response = $aop->pagePay($payRequestBuilder,$config['return_url'],$config['notify_url']);
	function is_https()
{
	if (defined('HTTPS') && HTTPS) return true;
	if (!isset($_SERVER)) return FALSE;
	if (!isset($_SERVER['HTTPS'])) return FALSE;
	if ($_SERVER['HTTPS'] === 1) {  //Apache
		return TRUE;
	} elseif ($_SERVER['HTTPS'] === 'on') { //IIS
			return TRUE;
	} elseif ($_SERVER['SERVER_PORT'] == 443) { //其他
		return TRUE;
	}
	return FALSE;
}
?>
</body>
</html>