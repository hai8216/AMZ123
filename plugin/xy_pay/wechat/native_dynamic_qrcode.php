<?php
include 'WxPayPubHelper.php';
	$out_trade_no = trim($_GET['orderid']);
	$subject = trim(urldecode($_GET['ordername']));
	$total_fee = abs($_GET['money']);
	$para = trim(urldecode($_GET['msg']));
	include '../../../xiunophp/xiunophp.php';
	$info = explode("|",$para);
	$type = $info[0];
	$uid = $info[1];
	$user = $info[2];
	$rmbs = $total_fee * (int)$conf['pay'];
	$iscreate = db_count('pay_list',array('orderid'=>$out_trade_no));
	if($iscreate){exit("<script>alert('订单已存在，请勿重复提交。');window.location.href='/my-pay.htm';</script>");}
	$oid = db_create('pay_list',array('uid'=>$uid,'username'=>$user,'money'=>$total_fee,'rmbs'=>$rmbs,'orderid'=>$out_trade_no,'pay_from'=>'网页微信'));
	if(!$oid){exit("<script>alert('订单创建失败，请重试。');window.location.href='/my-pay.htm';</script>");}
	$host = (is_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/";
	$urlstr = "?orderid=".$out_trade_no."&ordername=".urlencode($subject)."&money=".$total_fee."&msg=".$para;
	$notify = $host . "plugin/xy_pay/wechat/notify_url.php";
	$return = $host . "my-payinfo.htm";
	$return1 = $host . "reward.htm";
	$payurl = $host . "plugin/xy_pay/wechat/native_dynamic_qrcode.php".$urlstr;
	$checkurl = $host . "my-checkpaystate.htm";
	$unifiedOrder = new UnifiedOrder_pub();
	$unifiedOrder->setParameter("body",$subject);
	$unifiedOrder->setParameter("out_trade_no",$out_trade_no);
	$unifiedOrder->setParameter("total_fee",$total_fee*100);
	$unifiedOrder->setParameter("notify_url",$notify);
	$unifiedOrder->setParameter("trade_type",'NATIVE');
	$unifiedOrder->setParameter("attach",$para);//附加数据 
	$unifiedOrderResult = $unifiedOrder->getResult();
	if ($unifiedOrderResult["return_code"] == "FAIL") 
	{
		echo "通信出错：".$unifiedOrderResult['return_msg']."<br>";
	}
	elseif($unifiedOrderResult["result_code"] == "FAIL")
	{
		echo "错误代码：".$unifiedOrderResult['err_code']."<br>";
		echo "错误代码描述：".$unifiedOrderResult['err_code_des']."<br>";
	}
	elseif($unifiedOrderResult["code_url"] != NULL)
	{
		$code_url = $unifiedOrderResult["code_url"];
	}
		function is_https()
	{
		if (defined('HTTPS') && HTTPS) return true;
		if (!isset($_SERVER)) return FALSE;
		if (!isset($_SERVER['HTTPS'])) return FALSE;
		if ($_SERVER['HTTPS'] === 1) {  //Apache
			return TRUE;
		} elseif ($_SERVER['HTTPS'] === 'on') { //IIS
				return TRUE;
		} elseif ($_SERVER['SERVER_PORT'] == 443) { //其他
			return TRUE;
		}
		return FALSE;
	}
?>
<div class="col-lg main">
	<div align="center" id="qrcode"></div>
</div>
<div style="text-align:center;color:#C6303E">付款金额：<?php echo $total_fee;?>元</div>
<div style="text-align:center;color:#868e96 !important">请使用微信扫码付款</div>
<script src="qrcode.js"></script>
<script src="jquery-3.3.1.js?1.0"></script>
<script>
	var url = "<?php echo $code_url;?>";
	//参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
	var qr = qrcode(10, 'H');
	qr.addData(url);
	qr.make();
	var wording=document.createElement('p');
	wording.innerHTML = "";
	var code=document.createElement('DIV');
	code.innerHTML = qr.createImgTag();
	var element=document.getElementById("qrcode");
	element.appendChild(wording);
	element.appendChild(code);
	$(document).ready(function(){
    setInterval(get_zf_zt,2000);//自动执行get_zf_zt方法
	});
	function get_zf_zt(){
	$.ajax({
	type: "POST",
	url: '<?php echo $checkurl;?>' ,
	data: "orderid="+<?php echo $out_trade_no?>,
	success: function(data){
		if (data==1){
			window.parent.location.href='<?php echo $type==1 ?$return : $return1;?>'; 
		}
	}
	});
	} 
</script>