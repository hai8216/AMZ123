<?php 
error_reporting(0);
function is_https()
{
	if (defined('HTTPS') && HTTPS) return true;
	if (!isset($_SERVER)) return FALSE;
	if (!isset($_SERVER['HTTPS'])) return FALSE;
	if ($_SERVER['HTTPS'] === 1) {  //Apache
		return TRUE;
	} elseif ($_SERVER['HTTPS'] === 'on') { //IIS
			return TRUE;
	} elseif ($_SERVER['SERVER_PORT'] == 443) { //其他
		return TRUE;
	}
	return FALSE;
}
	require_once 'WxPayPubHelper.php';
	$out_trade_no = trim($_GET['orderid']);
	$subject = trim(urldecode($_GET['ordername']));
	$total_fee = abs($_GET['money']);
	$para = trim(urldecode($_GET['msg']));
	$host = (is_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/";
	$urlstr = urlencode("?orderid=".$out_trade_no."&ordername=".urlencode($subject)."&money=".$total_fee."&msg=".$para);
	$notify = $host . "plugin/xy_pay/wechat/notify_url.php";
	$return = $host . "my-payinfo.htm";
	$payurl = $host . "plugin/xy_pay/wechat/js_api_call.php".$urlstr;
	$jsApi = new JsApi_pub();
	if (!isset($_GET['code'])){
	$url = $jsApi->createOauthUrlForCode("$payurl");
	$golds = urlencode($total_fee);
	$url = str_replace("STATE", $golds, $url);
	include '../../../xiunophp/xiunophp.php';
		$info = explode("|",$para);
		$type = $info[0];
		$uid = $info[1];
		$user = $info[2];
		$rmbs = $total_fee * (int)$conf['pay'];
		$iscreate = db_count('pay_list',array('orderid'=>$out_trade_no));
		if($iscreate){exit("<script>alert('订单已存在，请勿重复提交。');window.location.href='/my-pay.htm';</script>");}
		$oid = db_create('pay_list',array('uid'=>$uid,'username'=>$user,'money'=>$total_fee,'rmbs'=>$rmbs,'orderid'=>$out_trade_no,'pay_from'=>'移动微信'));
		if(!$oid){exit("<script>alert('订单创建失败，请重试。');window.location.href='/my-pay.htm';</script>");}
		Header("Location: $url"); 
	}else{
		$code = $_GET['code'];
		$jsApi->setCode($code);
		$openid = $jsApi->getOpenId();
	}
	$unifiedOrder = new UnifiedOrder_pub();
	$unifiedOrder->setParameter("openid","$openid");
	$unifiedOrder->setParameter("body",$subject);
	$timeStamp = time();
	$unifiedOrder->setParameter("out_trade_no",$out_trade_no);
	$unifiedOrder->setParameter("total_fee",$total_fee*100);
	$unifiedOrder->setParameter("notify_url",$notify);
	$unifiedOrder->setParameter("trade_type","JSAPI");
	$unifiedOrder->setParameter("attach",$para);
	$prepay_id = $unifiedOrder->getPrepayId();
	$jsApi->setPrepayId($prepay_id);
	$jsApiParameters = $jsApi->getParameters();
?>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<title>微信安全支付</title>
<script type="text/javascript">
	//调用微信JS api 支付
	function jsApiCall(){
		WeixinJSBridge.invoke(
			'getBrandWCPayRequest',
			<?php echo $jsApiParameters; ?>,
			function(res){
				//alert(res.err_code+res.err_desc+res.err_msg);
				if(res.err_msg == "get_brand_wcpay_request:ok" ) 
				{
					window.location.href='<?php echo $return;?>';
				}
				else
				{
					alert('付款失败');	
				}
			}
		);
	}
	function callpay(){
		if (typeof WeixinJSBridge == "undefined"){
			if( document.addEventListener )
			{
					document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
				}else if (document.attachEvent){
					document.attachEvent('WeixinJSBridgeReady', jsApiCall); 
					document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
				}
		}else{
			jsApiCall();
		}
	}
</script>
</head>
<body onLoad="callpay()">	
</body>
</html>