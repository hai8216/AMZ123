<?php
function form_checkbox_pay($name, $checked = 0, $txt = '',$value = '1') {
	$add = $checked ? ' checked="checked"' : '';
	$s = "<td><label class=\"custom-input custom-checkbox mr-4\"><input type=\"checkbox\" name=\"$name\" value=\"$value\" $add /> $txt</label></td>";
	return $s;
}

function is_wap() {
	if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) { 
		return true; 
	}else{
		$touch_arr = array("mobile","android","wap","uc","wml","vnd","adr","mqqbrowser","nokiaBrowser","playbook", "blackberry","bb10","ipad","iphone");
		$useragent = $_SERVER["HTTP_USER_AGENT"];
		$th = str_ireplace($touch_arr,'',$useragent);
		if($useragent == $th){
			return false;
		}else{
			return true;
		}
	}
return false; 
}



function is_wechat() {  
  if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {  
    return true;  
  } else { 
    return false;  
  } 
}

function is_https()
{
    if (defined('HTTPS') && HTTPS) return true;
    if (!isset($_SERVER)) return FALSE;
    if (!isset($_SERVER['HTTPS'])) return FALSE;
    if ($_SERVER['HTTPS'] === 1) {  //Apache
        return TRUE;
    } elseif ($_SERVER['HTTPS'] === 'on') { //IIS
        return TRUE;
    } elseif ($_SERVER['SERVER_PORT'] == 443) { //其他
        return TRUE;
    }
    return FALSE;
}

function create_link($params, $codepay_key, $host = "")
{
    ksort($params); //重新排序$data数组
    reset($params); //内部指针指向数组中的第一个元素
    $sign = '';
    $urls = '';
    foreach ($params AS $key => $val) {
        if ($val == '') continue;
        if ($key != 'sign') {
            if ($sign != '') {
                $sign .= "&";
                $urls .= "&";
            }
            $sign .= "$key=$val"; //拼接为url参数形式
            $urls .= "$key=" . urlencode($val); //拼接为url参数形式
        }
    }

    $key = md5($sign . $codepay_key);//开始加密
    $query = $urls . '&sign=' . $key; //创建订单所需的参数
    $apiHost = ($host ? $host : "http://api2.fateqq.com:52888/creat_order/?"); //网关
    $url = $apiHost . $query; //生成的地址
    return array("url" => $url, "query" => $query, "sign" => $sign, "param" => $urls);
}

	function createLinkstring($data){
			$sign='';
			foreach ($data AS $key => $val) {
					if ($sign) $sign .= '&'; //第一个字符串签名不加& 其他加&连接起来参数
					$sign .= "$key=$val"; //拼接为url参数形式
			}
	}
	
	function daddslashes($string, $force = 0) {
	!defined('MAGIC_QUOTES_GPC') && define('MAGIC_QUOTES_GPC', get_magic_quotes_gpc());
	if(!MAGIC_QUOTES_GPC || $force) {
		if(is_array($string)) {
			foreach($string as $key => $val) {
				$string[$key] = daddslashes($val, $force);
			}
		} else {
			$string = trim(addslashes($string));
		}
	}
	return $string;
}

	function update_user_money($data,$num,$uid,$username,$conf = array(),$ip=''){
	
	
$para = urldecode($data['param']);//参数 type|uid|username|message
$info = explode("|",$para);
$type = $info[0];
$uid = $info[1];
$user = $info[2];
if($type == 2){
$msg = $info[3];
}
$msg = empty($msg)?null:$msg;
			$orderid = date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
			$money = (float)$data['money']; 
			$price = (float)$data['price']; 
			$type1 = (int)$data['type']; 
			if($type1 == 1){$pay_from ='支付宝';}elseif($type1 == 2){$pay_from ='QQ钱包';}elseif($type1 == 3){$pay_from ='微信支付';}
			$pay_no = $data['pay_no']; 
			$pay_time = (int)$data['pay_time']; 
			$status = 2; 
			if ($money <= 0 || $pay_time <= 0 || empty($pay_no)) {
					return 'error'; 
			}	
			if(!empty($uid)){
				$payinfo = db_find_one('pay_list',array('uid'=>$uid,'orderid_pay'=>$pay_no));
				if (empty($payinfo['orderid'])) {
					$num = $conf['pay'];
					$moneyadd = (int)$num * (int)$money ;
					db_create('pay_list',array('uid'=>$uid,'username'=>$user,'money'=>$money,'rmbs'=>$moneyadd,'orderid'=>$orderid,'orderid_pay'=>$pay_no,'state'=>$type,'pay_from'=>$pay_from));
					if($type == 2){
						db_update('pay_list',array('uid'=>$uid,'state'=>2),array('message'=>$msg));
					}
					elseif($type == 1){
						db_update('user',array('uid'=>$uid),array('rmbs+'=>$moneyadd));
						$msgtype = 15;
						$smg = $conf['rmb_name'] . " +". $money .$conf['rmb_unit'];
						// hook xy_score_add_score_info.php
					}
						return "success";
				}
			}
			return 'success';
	}
	
	
?>