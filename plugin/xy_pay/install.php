<?php

/*
	Xiuno BBS 4.0 拓展设置
*/

!defined('DEBUG') AND exit('Forbidden');
$tablepre = $db->tablepre;

$sql = "CREATE TABLE IF NOT EXISTS {$tablepre}pay_list (
	id int(11) unsigned NOT NULL auto_increment, 
	uid int(11) unsigned NOT NULL default '0',
	username char(20) NOT NULL default '0',
	money decimal(11,0) NOT NULL default '0',
	rmbs int(11) unsigned NOT NULL default '0',
	orderid char(50) NOT NULL default '0',
	orderid_pay char(50) default null,
	paytime timestamp NULL default CURRENT_TIMESTAMP,
	state int(11) unsigned NOT NULL default '0',
	pay_from char(20) NOT NULL default '0',
	message varchar(50) default null,
	PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;";
db_exec($sql);
$sql = "CREATE TABLE IF NOT EXISTS {$tablepre}payout_list (
	id int(11) unsigned NOT NULL auto_increment, 
	uid int(11) unsigned NOT NULL default '0',
	username char(20) NOT NULL default '0',
	money decimal(11,0) NOT NULL default '0',
	payout_num char(30) default null,
	payout_name char(20)  default null,
	paytime timestamp NULL default CURRENT_TIMESTAMP,
	admin_paytime timestamp NULL default null,
	state int(11) unsigned NOT NULL default '0',
	PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;";
db_exec($sql);
$sql = "CREATE TABLE IF NOT EXISTS {$tablepre}exchange_list (
	id int(11) unsigned NOT NULL auto_increment, 
	uid int(11) unsigned NOT NULL default '0',
	username char(20) NOT NULL default '0',
	fromnum char(50) NOT NULL default '0',
	tonum char(50) NOT NULL default '0',
	exctime timestamp NULL default CURRENT_TIMESTAMP,
	PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;";
db_exec($sql);

$sql = "ALTER TABLE ".$tablepre."user ADD payout_num char(30) default null;";
db_exec($sql);
$sql = "ALTER TABLE ".$tablepre."user ADD payout_name char(20) default null;";
db_exec($sql);
?>