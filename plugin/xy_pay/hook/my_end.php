<?php exit;
elseif($action == 'pay') {
	if($method == 'GET') {
		$header['title'] = lang('pay_user_pay').'-'.$conf['sitename'];
		$header['mobile_title'] = lang('pay_user_pay').'-'.$conf['sitename'];
		include _include(APP_PATH.'plugin/xy_pay/view/htm/my_pay.htm');
	}
}elseif($action == 'exchange') {
	if($method == 'GET') {
		$header['title'] = lang('pay_user_exchange').'-'.$conf['sitename'];
		$header['mobile_title'] = lang('pay_user_exchange').'-'.$conf['sitename'];
		include _include(APP_PATH.'plugin/xy_pay/view/htm/my_exchange.htm');
	}else{
		$ctype = param(2);
		if($ctype == 'r2c'){
		if(empty($conf['exchange'])){message(0, lang('pay_user_exchange_error4'));}
		$gnum= (int)param('rc', '', FALSE);
		$rnum = (int)$conf['exchange_bl'];
		if($gnum< $rnum ){message(0, lang('pay_user_exchange_error'));}
		if($gnum% $rnum ){message(0, lang('pay_user_exchange_error1'));}
		$addgold = $gnum;
		$subrmb = $gnum/ $rnum ;
		if($subrmb > $user['rmbs']){
			$msg = str_replace ('%%',!empty($conf['rmb_name']) ?$conf['rmb_name'] :$conf['pay_rmb'],lang('pay_user_exchange_error2'));
			message(0, $msg);
		}
		db_update('user',array('uid'=>$uid),array('rmbs-'=>$subrmb,'golds+'=>$addgold));
		$msg_from = ($conf['rmb_name'] ? $conf['rmb_name'] : lang('pay_rmb')) . " -". $subrmb . ($conf['rmb_unit'] ? $conf['rmb_unit'] : "");
		$msg_to = ($conf['gold_name'] ? $conf['gold_name'] : lang('pay_gold')) . " +". $addgold . ($conf['gold_unit'] ? $conf['gold_unit'] : "");
		$logmsg = array('uid'=>$uid,'username'=>$user['username'],'fromnum'=>$msg_from,'tonum'=>$msg_to);
		db_create('exchange_list',$logmsg);
		$msgtype = 13;
		$smg = $conf['rmb_name'] . " -". $subrmb .$conf['rmb_unit'].",".$conf['gold_name'] ." +".$addgold . $conf['gold_unit'];
		// hook xy_score_add_score_info.php
		message(0, lang('pay_user_exchange_success'));
		}
		elseif($ctype == 'c2r'){
		if(empty($conf['disexchange'])){message(0, lang('pay_user_exchange_error4'));}
		$rnum = (int)param('rc', '', FALSE);
		if($rnum <1){message(0, lang('pay_user_exchange_error'));}
		$gnum = $rnum * (int)$conf['exchange_bl'];
		if($gnum > $user['golds']){
			$msg = str_replace ('%%',!empty($conf['gold_name']) ?$conf['gold_name'] :$conf['pay_gold'],lang('pay_user_exchange_error2'));
			message(0, $msg);
		}
		db_update('user',array('uid'=>$uid),array('rmbs+'=>$rnum,'golds-'=>$gnum));
		$msg_from = ($conf['gold_name'] ? $conf['gold_name'] : lang('pay_gold')) . " -". $gnum . ($conf['gold_unit'] ? $conf['gold_unit'] : "");
		$msg_to = ($conf['rmb_name'] ? $conf['rmb_name'] : lang('pay_rmb')) . " +". $rnum . ($conf['rmb_unit'] ? $conf['rmb_unit'] : "");
		$logmsg = array('uid'=>$uid,'username'=>$user['username'],'fromnum'=>$msg_from,'tonum'=>$msg_to);
		db_create('exchange_list',$logmsg);
		$msgtype = 13;
		$smg = $conf['gold_name'] ." -".$gnum . $conf['gold_unit'].",". $conf['rmb_name'] . " +". $rnum .$conf['rmb_unit'];
		// hook xy_score_add_score_info.php
		message(0, lang('pay_user_exchange_success'));
		}
	}
}elseif($action == 'payout') {
	if($method == 'GET'){
		$header['title'] = lang('pay_user_payout').'-'.$conf['sitename'];
		$header['mobile_title'] = lang('pay_user_payout').'-'.$conf['sitename'];
		include _include(APP_PATH.'plugin/xy_pay/view/htm/my_payout.htm');
		}else{
			if(empty($user['payout_num']) || empty($user['payout_name'])){message(0, lang('pay_user_payout_msg12'));}
			$money = (int)param('money', '', FALSE);
			if($money < $conf['payout_min']){message(0, lang('pay_user_payout_msg8'));}
			$pinfo = db_count('payout_list',array('uid'=>$uid,'state'=>0));
			if(!empty($pinfo)){message(0, lang('pay_user_payout_msg10'));}
			if($money > $user['rmbs']){message(0, lang('pay_user_payout_msg9'));}
				db_update('user',array('uid'=>$uid),array('rmbs-'=>$money));
				db_create('payout_list',array('uid'=>$uid,'username'=>$user['username'],'money'=>$money,'payout_num'=>$user['payout_num'],'payout_name'=>$user['payout_name']));
				$msgtype = 14;
				$smg = $conf['rmb_name'] . " -". $money .$conf['rmb_unit'];
				// hook xy_score_add_score_info.php
			 message(0, lang('pay_user_payout_msg14'));
		}
}

elseif($action == 'payinfo') {
		$page = param(2, 0);
		$page = !$page ?1:$page;
		$pagesize = 10;
		$listnum = db_count('pay_list',array('uid'=>$uid,'state'=>1));
		$pagination = pagination(url("my-payinfo-{page}"), $listnum, $page, $pagesize);
		$infolist = db_find('pay_list',array('uid'=>$uid,'state'=>1), array('id'=>-1),$page, $pagesize);
		$header['title'] = lang('pay_user_payinfo').'-'.$conf['sitename'];
		$header['mobile_title'] = lang('pay_user_payinfo').'-'.$conf['sitename'];
		include _include(APP_PATH.'plugin/xy_pay/view/htm/my_payinfo.htm');
}

elseif($action == 'exchangeinfo') {
		$page = param(2, 0);
		$page = !$page ?1:$page;
		$pagesize = 10;
		$listnum = db_count('exchange_list',array('uid'=>$uid));
		$pagination = pagination(url("my-exchangeinfo-{page}"), $listnum, $page, $pagesize);
		$infolist = db_find('exchange_list',array('uid'=>$uid), array('id'=>-1),$page, $pagesize);
		$header['title'] = lang('pay_user_exchangeinfo').'-'.$conf['sitename'];
		$header['mobile_title'] = lang('pay_user_exchangeinfo').'-'.$conf['sitename'];
		include _include(APP_PATH.'plugin/xy_pay/view/htm/my_exchangeinfo.htm');
}

elseif($action == 'outinfo') {
		$page = param(2, 0);
		$page = !$page ?1:$page;
		$pagesize = 10;
		$listnum = db_count('payout_list',array('uid'=>$uid));
		$pagination = pagination(url("my-outinfo-{page}"), $listnum, $page, $pagesize);
		$infolist = db_find('payout_list',array('uid'=>$uid), array('id'=>-1),$page, $pagesize);
		$header['title'] = lang('pay_user_outinfo').'-'.$conf['sitename'];
		$header['mobile_title'] = lang('pay_user_outinfo').'-'.$conf['sitename'];
		include _include(APP_PATH.'plugin/xy_pay/view/htm/my_outinfo.htm');
}elseif($action == 'payoutnum'){//�������˺�
	if($method =='GET'){
		include _include(APP_PATH.'plugin/xy_pay/view/htm/my_payoutnum.htm');
	}else{
		$payoutnum = param('payoutnum', '', FALSE);
		$payoutname = param('payoutname', '', FALSE);
		if(empty($payoutnum) || empty($payoutname)){message(0, lang('pay_user_payout_num_fail1'));}
		$r = user_update($uid, array('payout_num'=>$payoutnum,'payout_name'=>$payoutname));
		$r === FALSE AND message(0, lang('pay_user_payout_num_fail'));
		message(0, lang('pay_user_payout_num_success'));
	}
}elseif($action == 'checkpaystate'){//���֧��״̬΢��PCר��
	$checkstate = db_count('pay_list',array('orderid'=>param('orderid'),'state'=>array('>'=>0)));
	echo($checkstate);
}
?>