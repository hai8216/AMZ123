<?php exit;
$pay_menu = array(
		'pay' => array(
		'url'=>url('pay-list'), 
		'text'=>lang('pay'), 
		'icon'=>'icon-cny', 
		'tab'=> array (
			'inlist'=>array('url'=>url('pay-list'), 'text'=>lang('pay_admin_pay_list')),
			'outlist'=>array('url'=>url('pay-out'), 'text'=>lang('pay_admin_out_list')),
			'exchangelist'=>array('url'=>url('pay-exchange'), 'text'=>lang('pay_admin_exchange_list')),
			'rewardlist'=>array('url'=>url('pay-rewardlist'), 'text'=>lang('pay_admin_reward_list')),
			'setting'=>array('url'=>url('pay-setting'), 'text'=>lang('pay_admin_setting')),
			'reward'=>array('url'=>url('pay-reward'), 'text'=>lang('pay_admin_reward')),
		)
	));
$menu += $pay_menu;
?>