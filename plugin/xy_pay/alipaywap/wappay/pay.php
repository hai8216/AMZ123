<?php
error_reporting(0);
header("Content-type: text/html; charset=utf-8");
require_once dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'service/AlipayTradeService.php';
require_once dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'buildermodel/AlipayTradeWapPayContentBuilder.php';
$conf = include '../../../../conf/conf.php';
	$out_trade_no = trim($_GET['orderid']);
	$subject = trim(urldecode($_GET['ordername']));
	$total_amount = abs($_GET['money']);
	$para = trim(urldecode($_GET['msg']));
	include '../../../../xiunophp/xiunophp.php';
	$info = explode("|",$para);
	$type = $info[0];
	$uid = $info[1];
	$user = $info[2];
	$rmbs = $total_amount * (int)$conf['pay'];
	$iscreate = db_count('pay_list',array('orderid'=>$out_trade_no));
	if($iscreate){exit("<script>alert('订单已存在，请勿重复提交。');window.location.href='/my-pay.htm';</script>");}
	$oid = db_create('pay_list',array('uid'=>$uid,'username'=>$user,'money'=>$total_amount,'rmbs'=>$rmbs,'orderid'=>$out_trade_no,'pay_from'=>'移动支付宝'));
	if(!$oid){exit("<script>alert('订单创建失败，请重试。');window.location.href='/my-pay.htm';</script>");}
	$host = (is_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/";
	$config = array (
	'app_id' => $conf['alipay_app_id'],
	'merchant_private_key' => $conf['alipay_private_key'],
	'notify_url' => $host . "plugin/xy_pay/alipaywap/notify_url.php",
	'return_url' => $host . ($type == 1 ? 'my-payinfo.htm' : 'reward.htm'),
	'charset' => "UTF-8",
	'sign_type'=>"RSA2",
	'gatewayUrl' => "https://openapi.alipay.com/gateway.do",
	'alipay_public_key' => $conf['alipay_public_key'],
	);
	$body = '';
	$timeout_express ="1m";
	$payRequestBuilder = new AlipayTradeWapPayContentBuilder();
	$payRequestBuilder->setBody($body);
	$payRequestBuilder->setSubject($subject);
	$payRequestBuilder->setOutTradeNo($out_trade_no);
	$payRequestBuilder->setTotalAmount($total_amount);
	$payRequestBuilder->setTimeExpress($timeout_express);
	$payRequestBuilder->setpassback_params($para);
	$payResponse = new AlipayTradeService($config);
	$result=$payResponse->wapPay($payRequestBuilder,$config['return_url'],$config['notify_url']);
	return ;
?>
