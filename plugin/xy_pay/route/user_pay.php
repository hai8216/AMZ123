<?php

!defined('DEBUG') AND exit('Access Denied.');
$action = param(1);
if($action == 'paynow'){
	if($conf['pay_type']){
	// 1=alipay 3=wechar;
		$param = param('paystate', '', FALSE);
		$type = param('paytype', '', FALSE);
		$price = param('money', '', FALSE);
		$messagebox = urldecode(param('messagebox', '', FALSE));
		
		$type = empty($type) ? 1 : $type;
		$out_trade_no = date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
		$subject = $param == 'pay_for_user' ? "账户充值" : "赞助本站";
		$paytype = $param == 'pay_for_user' ? 1 : 2;
		$para = urlencode($paytype.'|'.$user['uid']."|".$user['username']."|".$messagebox);
		$urlstr = "?orderid=".$out_trade_no."&ordername=".urlencode($subject)."&money=".$price."&msg=".$para ."&_t=".time();

			if($type == 1){
				//alipay
				if(is_wap()){
					header('Location:'.(is_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/plugin/xy_pay/alipaywap/wappay/pay.php$urlstr");
				}else{
					header('Location:'.(is_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/plugin/xy_pay/alipay/pagepay/pagepay.php$urlstr");
				}
			}elseif($type == 2){
			//QQpay
			}
			elseif($type == 3){
			//wechar
				if(is_wap()){
					if(is_wechat()){
						header('Location:'.(is_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/plugin/xy_pay/wechat/js_api_call.php$urlstr");
					}else{
						header('Location:'.(is_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/plugin/xy_pay/wechat/js_h5_call.php$urlstr");
					}
				}else{
					header('Location:'.(is_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/plugin/xy_pay/wechat/native_dynamic_qrcode.php$urlstr");
				}
			}
	
	}else{
						//个人支付接口
						
						$param = param('paystate', '', FALSE);
						$type = param('paytype', '', FALSE);
						$price = param('money', '', FALSE);
						$messagebox = urldecode(param('messagebox', '', FALSE));
						
						$type = empty($type) ? 1 : $type;
						$out_trade_no = date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
						$subject = $param == 'pay_for_user' ? "账户充值" : "赞助本站";
						$paytype = $param == 'pay_for_user' ? 1 : 2;
						$para = urlencode($paytype.'|'.$user['uid']."|".$user['username']."|".$messagebox);
						
						$config['key'] = $conf['mpaykey'];
						$config['gateway'] = '';  //设置支付网关
						$config['host'] = (is_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
						$config['notify_url'] = $config['host'] . '/' . url('pay-success');

						$parameter = array(
							"id" => (int)$conf['mpayid'],
							"type" => (int)$type,//支付方式
							"price" => (float)$price,//原价
							"pay_id" => $out_trade_no, //可以是用户ID,站内商户订单号,用户名
							"param" => $para,//自定义参数
							"act" => 0,//此参数即将弃用
							"outTime" => 180,//二维码超时设置
							"page" => 4,//订单创建返回JS 或者JSON
							"return_url" => $config["notify_url"],//付款后附带加密参数跳转到该页面
							"notify_url" => $config["notify_url"],//付款后通知该页面处理业务
							"style" => 1,//付款页面风格
							"pay_type" => 1,//支付宝使用官方接口
							"user_ip" => $ip,//付款人IP
							"chart" => 'utf-8'//字符编码方式
						);
						$back = create_link($parameter, $codepay_config['key']);
						switch ((int)$type) {
							case 1:
								$typeName = $conf['pay_alipay'];
									break;
							case 2:
								$typeName = $conf['pay_qq'];
									break;
							default:
								$typeName = $conf['pay_wechar'];
						}
					//准备传给前端输出的JSON
						$user_data = array(
						"return_url" => $parameter["return_url"],
						"type" => $parameter['type'],
						"outTime" => $parameter["outTime"],
						"codePay_id" => $parameter["id"],
						"logoShowTime" => 2
						);
					$user_data["logoShowTime"] = 2;
						$parameter['page'] = "4"; //设置返回JSON
						$back = create_link($parameter, $config['key'],$config['gateway']); //生成支付URL
						if (function_exists('file_get_contents')) { //如果开启了获取远程HTML函数 file_get_contents
								$json = file_get_contents($back['url']); //获取远程HTML
						} else if (function_exists('curl_init')) {
							$ch = curl_init(); //使用curl请求
							$timeout = 5;
							curl_setopt($ch, CURLOPT_URL, $back['url']);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
							$json = curl_exec($ch);
							curl_close($ch);
						}

						if (empty($json)) { //如果没有获取到远程HTML 则走JS创建订单
							$parameter['call'] = "callback";
							$parameter['page'] = "3";
							$back = create_link($parameter, $config['key'],'https://codepay.fateqq.com/creat_order/?');
							$html = '<script src="' . $back['url'] . '"></script>'; //JS数据
						} else { //获取到了JSON
							$data = json_decode($json);
							$qr = $data ? $data->qrcode : '';
							$html = "<script>callback({$json})</script>"; //JSON数据
						}
							$title_pay = lang('pay_user_pay_title')."-".$conf['sitename'];
						include _include(APP_PATH."plugin/xy_pay/view/htm/user_pay.htm");

	}

}
elseif($action == 'success'){

	$codepay_key = $conf['mpaykey'];
	$isPost = true; //默认为POST传入

	if (empty($_POST)) { //如果GET访问
		$_POST = $_GET;  //POST访问 为服务器或软件异步通知  不需要返回HTML
		$isPost = false; //标记为GET访问  需要返回HTML给用户
	}
	ksort($_POST); //排序post参数
	reset($_POST); //内部指针指向数组中的第一个元素
	$sign = ''; //加密字符串初始化
	foreach ($_POST AS $key => $val) {
		if ($val == '' || $key == 'sign') continue; //跳过这些不签名
		if ($sign) $sign .= '&'; //第一个字符串签名不加& 其他加&连接起来参数
		$sign .= "$key=$val"; //拼接为url参数形式
	}
	$pay_id = $_POST['pay_id']; //需要充值的ID 或订单号 或用户名
	$money = (float)$_POST['money']; //实际付款金额
	$price = (float)$_POST['price']; //订单的原价
	$param = urldecode($_POST['param']); //自定义参数

$info = explode("|",$param);

$paytype = $info[0];

	$type = (int)$_POST['type']; //支付方式
	$pay_no = $_POST['pay_no'];//流水号
	if (!$_POST['pay_no'] || md5($sign . $codepay_key) != $_POST['sign']) { //不合法的数据
		if ($isPost) exit('fail');  //返回失败 继续补单
		$result = $pay_id = $pay_no = lang('pay_user_fail');
		if ($type < 1) $type = 1;
	} else { //合法的数据
		//业务处理
		$result = update_user_money($_POST,$conf['pay'],$user['uid'],$user['username'],$conf,$ip); //调用示例业务代码 处理业务获得返回值
		if ($result == 'success') { //返回的是业务处理完成
			if ($isPost) exit($result); //服务器访问 业务处理完成 下面不执行了
			$result = lang('pay_user_success');
		} else {
			$error_msg = defined('DEBUG') && DEBUG ? $result : 'no'; //调试模式显示 否则输出no
			if ($isPost) exit($error_msg);  //服务器访问 返回给服务器
			$result = lang('pay_user_fail');
		}
	}
	$title_pay = $result ."-". $conf['sitename'];
	include _include(APP_PATH."plugin/xy_pay/view/htm/user_pay_success.htm");
}

?>