<?php

!defined('DEBUG') AND exit('Access Denied.');


$action = param(1);

if($action == 'list') {
	$kwd = xn_urldecode($_GET['kwd']);
	$header['title'] = lang('pay_admin_pay_list');
	$header['mobile_title'] = lang('pay_admin_pay_list');
	$page = param(2, 0);
	$page = !$page ?1:$page;
	$pagesize = 10;
	if(empty($kwd)) {
		$listnum = db_count('pay_list',array('state'=>1));
		$pagination = pagination(url("pay-list-{page}"), $listnum, $page, $pagesize);
		$infolist = db_find('pay_list',array('state'=>1), array('id'=>-1),$page, $pagesize);
	} else {
		$listnum = db_count('pay_list',array('username'=>$kwd,'state'=>1));
			if($listnum){
				$pagination = pagination(url("pay-list-{page}")."?kwd=".xn_urlencode($kwd), $listnum, $page, $pagesize);
				$infolist = db_find('pay_list',array('state'=>1,'username'=>$kwd), array('id'=>-1),$page, $pagesize);
			}else{
				$listnum = db_count('pay_list',array('orderid'=>$kwd,'state'=>1));
					if($listnum){
						$pagination = pagination(url("pay-list-{page}")."?kwd=".xn_urlencode($kwd), $listnum, $page, $pagesize);
						$infolist = db_find('pay_list',array('state'=>1,'orderid'=>$kwd), array('id'=>-1),$page, $pagesize);
					}else{
						$listnum = db_count('pay_list',array('orderid_pay'=>$kwd,'state'=>1));
						$pagination = pagination(url("pay-list-{page}")."?kwd=".xn_urlencode($kwd), $listnum, $page, $pagesize);
						$infolist = db_find('pay_list',array('state'=>1,'orderid_pay'=>$kwd), array('id'=>-1),$page, $pagesize);
					}
				
			}
	}
	include _include(APP_PATH."plugin/xy_pay/view/htm/admin_pay_list.htm");
}
elseif($action == 'out'){
	$kwd = xn_urldecode($_GET['kwd']);
	$header['title'] = lang('pay_admin_out_list');
	$header['mobile_title'] =lang('pay_admin_out_list');
	$page = param(2, 0);
	$page = !$page ?1:$page;
	$pagesize = 10;
	if(empty($kwd)) {
		$listnum = db_count('payout_list');
		$pagination = pagination(url("pay-list-{page}"), $listnum, $page, $pagesize);
		$infolist = db_find('payout_list',array(), array('id'=>-1),$page, $pagesize);
	} else {
		$listnum = db_count('payout_list',array('username'=>$kwd));
			if($listnum){
				$pagination = pagination(url("pay-list-{page}")."?kwd=".xn_urlencode($kwd), $listnum, $page, $pagesize);
				$infolist = db_find('payout_list',array('username'=>$kwd), array('id'=>-1),$page, $pagesize);
			}else{
				$listnum = db_count('payout_list',array('payout_num'=>$kwd));
				$pagination = pagination(url("pay-list-{page}")."?kwd=".xn_urlencode($kwd), $listnum, $page, $pagesize);
				$infolist = db_find('payout_list',array('payout_num'=>$kwd), array('id'=>-1),$page, $pagesize);
			}
	}
	include _include(APP_PATH."plugin/xy_pay/view/htm/admin_out_list.htm");
}
elseif($action == 'settle'){
		$payid = param('id');
			db_update('payout_list',array('id'=>$payid),array('state'=>1,'admin_paytime'=>date('Y-m-d H:i:s',$time)));
			header("Location:".$_SERVER["HTTP_REFERER"]);
}
elseif($action == 'exchange'){
	$kwd = xn_urldecode($_GET['kwd']);
	$header['title'] = lang('pay_admin_exchange_list');
	$header['mobile_title'] =lang('pay_admin_exchange_list');

	$page = param(2, 0);
	$page = !$page ?1:$page;
	$pagesize = 10;
	if(empty($kwd)) {
		$listnum = db_count('exchange_list');
		$pagination = pagination(url("pay-exchange-{page}"), $listnum, $page, $pagesize);
		$infolist = db_find('exchange_list',array(), array('id'=>-1),$page, $pagesize);
	} else {
			$listnum = db_count('exchange_list',array('username'=>$kwd));
			$pagination = pagination(url("pay-exchange-{page}")."?kwd=".xn_urlencode($kwd), $listnum, $page, $pagesize);
			$infolist = db_find('exchange_list',array('username'=>$kwd), array('id'=>-1),$page, $pagesize);
	}
	include _include(APP_PATH."plugin/xy_pay/view/htm/admin_exchange_list.htm");

}
elseif($action == 'rewardlist'){
	$kwd = xn_urldecode($_GET['kwd']);
	$header['title'] = lang('pay_admin_reward_list');
	$header['mobile_title'] =lang('pay_admin_reward_list');

	$page = param(2, 0);
	$page = !$page ?1:$page;
	$pagesize = 20;
	if(empty($kwd)) {

		$temp = db_sql_find("select uid from bbs_pay_list where state=2 group by uid");//db_count('pay_list',array('state'=>2));
		$listnum = count($temp);
		$pagination = pagination(url("pay-rewardlist-{page}"), $listnum, $page, $pagesize);
		$offset = ($page - 1) * $pagesize;
		$infolist = db_sql_find("select *,sum(money) as num from bbs_pay_list where state=2 group by uid order by num desc,message desc limit $offset,$pagesize");//db_find('pay_list',array('state'=>2), array('id'=>-1),$page, $pagesize);//

	} else {
		$temp = db_sql_find("select uid from bbs_pay_list where state=2 and username='$kwd' group by uid");//db_count('pay_list',array('state'=>2));
		$listnum = count($temp);
		$pagination = pagination(url("pay-rewardlist-{page}"), $listnum, $page, $pagesize);
		$offset = ($page - 1) * $pagesize;
		$infolist = db_sql_find("select *,sum(money) as num from bbs_pay_list where state=2 and username='$kwd' group by uid order by num desc,message desc limit $offset,$pagesize");//db_find('pay_list',array('state'=>2), array('id'=>-1),$page, $pagesize);//

	}
	include _include(APP_PATH."plugin/xy_pay/view/htm/admin_reward_list.htm");

}
elseif($action == 'setting'){
	if($method == 'GET'){
		$input = array();
		
		$input['pay_type'] = form_radio_yes_no('pay_type', $conf['pay_type']);
		$input['alipay_app_id'] = form_text('alipay_app_id', $conf['alipay_app_id']);
		$input['alipay_private_key'] = form_textarea('alipay_private_key', $conf['alipay_private_key'], '100%', 100);
		$input['alipay_public_key'] = form_textarea('alipay_public_key', $conf['alipay_public_key'], '100%', 100);
		
		$input['wechat_app_id'] = form_text('wechat_app_id', $conf['wechat_app_id']);
		$input['wechat_app_secret'] = form_text('wechat_app_secret', $conf['wechat_app_secret']);
		$input['wechat_business_id'] = form_text('wechat_business_id', $conf['wechat_business_id']);
		$input['wechat_business_secret'] = form_text('wechat_business_secret', $conf['wechat_business_secret']);
		
		$input['mpayid'] = form_text('mpayid', $conf['mpayid']);
		$input['mpaykey'] = form_text('mpaykey', $conf['mpaykey']);
		$input['pay'] = form_text('pay', $conf['pay']);
		$input['exchange'] = form_radio_yes_no('exchange', $conf['exchange']);
		$input['disexchange'] = form_radio_yes_no('disexchange', $conf['disexchange']);
		$input['exchange_bl'] = form_text('exchange_bl', $conf['exchange_bl']);
		$input['payout'] = form_radio_yes_no('payout', $conf['payout']);
		$input['alipay'] = form_checkbox_pay('alipay',$conf['alipay'],lang('pay_alipay'),'1');
		$input['qqpay'] = form_checkbox_pay('qqpay',$conf['qqpay'],lang('pay_qq'),'1');
		$input['wechar'] = form_checkbox_pay('wechar',$conf['wechar'],lang('pay_wechar'),'1');
		$input['payout_min'] = form_text('payout_min', $conf['payout_min']);
		$input['payout_service'] = form_text('payout_service', $conf['payout_service']);
		$input['pay_out_info'] = form_text('pay_out_info', $conf['pay_out_info']);
		$header['title'] = lang('pay_admin_setting');
		$header['mobile_title'] =lang('pay_admin_setting');
	
		include _include(APP_PATH."plugin/xy_pay/view/htm/admin_setting.htm");
	}else{
		$replace = array();
		$replace['pay_type'] = param('pay_type', 0);
		$replace['alipay_app_id'] = param('alipay_app_id', '', FALSE);
		$replace['alipay_private_key'] = param('alipay_private_key', '', FALSE);
		$replace['alipay_public_key'] = param('alipay_public_key', '', FALSE);
		
		$replace['wechat_app_id'] = param('wechat_app_id', '', FALSE);
		$replace['wechat_app_secret'] = param('wechat_app_secret', '', FALSE);
		$replace['wechat_business_id'] = param('wechat_business_id', '', FALSE);
		$replace['wechat_business_secret'] = param('wechat_business_secret', '', FALSE);

		$replace['mpayid'] = param('mpayid', '', FALSE);
		$replace['mpaykey'] = param('mpaykey', '', FALSE);
		$replace['pay'] = param('pay', '', FALSE);
		$replace['exchange'] = param('exchange', 0);
		$replace['disexchange'] = param('disexchange', 0);
		$replace['exchange_bl'] = param('exchange_bl', '', FALSE);
		$replace['pay_out_info'] = param('pay_out_info', '', FALSE);
		$replace['payout'] = param('payout', 0);
		$replace['alipay'] = param('alipay', 0);
		$replace['qqpay'] = param('qqpay', 0);
		$replace['wechar'] = param('wechar', 0);
		$replace['payout_min'] = param('payout_min', '', FALSE);
		$replace['payout_service'] = param('payout_service', '', FALSE);
		file_replace_var(APP_PATH.'conf/conf.php', $replace);
		message(0, lang('modify_successfully'));
	}
}elseif($action == 'reward'){
	if($method == 'GET'){
		$input['reward'] = form_radio_yes_no('reward', $conf['reward']);
		$input['reward_title'] = form_text('reward_title', $conf['reward_title']);
		$input['reward_button_title'] = form_text('reward_button_title', $conf['reward_button_title']);
		$input['reward_from_title'] = form_text('reward_from_title', $conf['reward_from_title']);
		$input['reward_info'] = form_textarea('reward_info', $conf['reward_info'], '100%', 100);
		include _include(APP_PATH."plugin/xy_pay/view/htm/admin_reward.htm");
	}else{
		$replace = array();
		$replace['reward'] = param('reward', 0);
		$replace['reward_title'] = param('reward_title', '', FALSE);
		$replace['reward_button_title'] = param('reward_button_title', '', FALSE);
		$replace['reward_from_title'] = param('reward_from_title', '', FALSE);
		$replace['reward_info'] = param('reward_info', '', FALSE);
		file_replace_var(APP_PATH.'conf/conf.php', $replace);
		message(0, lang('modify_successfully'));
	}
}

/** 
 * Sort array by filed and type, common utility method. 
 * @param array $array 
 * @param string $filed1 
 * @param string $type1 SORT_ASC or SORT_DESC 
 * @param string $filed2 
 * @param string $type2 SORT_ASC or SORT_DESC 
 */
/** 
 * Sort array by filed and type, common utility method. 
 * @param array $data 
 * @param string $sort_filed 
 * @param string $sort_type SORT_ASC or SORT_DESC 
 */
function sortByOneField($data, $filed, $type) 
{ 
  if (count($data) <= 0) { 
    return $data; 
  } 
  foreach ($data as $key => $value) { 
    $temp[$key] = $value[$filed]; 
  } 
  array_multisort($temp, $type, $data); 
  return $data; 
}
?>