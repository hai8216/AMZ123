<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') and exit('Access Denied.');
$pluginname = 'xl_yqlinks';
$action = param(3) ?: "list";
if ($action == 'list') {

    $page = param(4);
    $pagesize = 20;
    $map = [];
    if (param('cateid')) {
        $map['cate'] = param('cateid');
    }
    $list = db_find('yqlinks', $map, array('order_id' => -1), $page, $pagesize);
    $clist = db_find('yqlinks_cate', [], [], 1, 100);
    $listnum = db_count('yqlinks',$map);
     if (param('cateid')) {
         $pagination = pagination(url("plugin-setting-xl_yqlinks-list-{page}",["cateid"=>param('cateid')]), $listnum, $page, $pagesize);
     } else {
         $pagination = pagination(url("plugin-setting-xl_yqlinks-list-{page}"), $listnum, $page, $pagesize);
     }
    $map['cate'] = 0;
    $clists = db_find('yqlinks',$map, array('order_id' => '-1'), 1, 500);
    cache_set("links_0", serialize($clists));
    include _include(APP_PATH . "/plugin/xl_yqlinks/list.htm");

} elseif ($action == 'cate') {
    $page = param(4);
    $pagesize = 20;
    $list = db_find('yqlinks_cate', array(), [], $page, $pagesize);
    $listnum = db_count('yqlinks_cate');
    $pagination = pagination(url("plugin-setting-xl_yqlinks-cate-{page}"), $listnum, $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_yqlinks/cate.htm");

} elseif ($action == 'adcate') {
    if ($method == 'GET') {
        $val = array();
        $val = db_find_one('yqlinks_cate', array('id' => param(4)));
        $input['name'] = form_text("name", $val['name']);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/adcate.htm');
    } else {
        $data['name'] = param('name');
        if (param('id')) {
            db_update('yqlinks_cate', array('id' => param('id')), $data);
        } else {
            db_insert('yqlinks_cate', $data);
        }
        message(0, 'success');
    }

} elseif ($action == 'delcate') {
    $val = db_delete('yqlinks_cate', array('id' => param(4)));
    message(0, jump('删除成功', url('plugin-setting-xl_yqlinks-cate')));
} elseif ($action == 'clear') {
    $yqlinks = db_find('yqlinks', array(), array('order_id' => '-1'), 1, 100);
    kv_set('yqlinks', serialize($yqlinks));
    message(0, 'success');
} elseif ($action == 'add') {

    if ($method == 'GET') {
        $val = array();
        $val = db_find_one('yqlinks', array('id' => param(4)));
        $catelist = db_find("yqlinks_cate");
        $catelists[0] = "友情链接";
        foreach ($catelist as $v) {
            $catelists[$v['id']] = $v['name'];
        }
        $select = form_select('cate', $catelists, $val['cate']);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/add.htm');
    } else {
        $data['name'] = param('name');
        $data['links'] = param('links');
        $data['cate'] = param('cate');
        $data['order_id'] = param('order_id');
        if (param('id')) {
            db_update('yqlinks', array('id' => param('id')), $data);
        } else {
            db_insert('yqlinks', $data);
        }
        $yqlinks = db_find('yqlinks', array(), array('order_id' => '-1'), 1, 500);
        kv_set('yqlinks', serialize($yqlinks));
        $clist = db_find('yqlinks', array('cate' => $data['cate']), array('order_id' => '-1'), 1, 500);
        cache_set("links_" . $data['cate'], serialize($clist));
        message(0, 'success');
    }

} elseif ($action == 'del') {
    $val = array();
    $val = db_delete('yqlinks', array('id' => param(4)));
    $yqlinks = db_find('yqlinks', array(), array('order_id' => '-1'), 1, 100);
    kv_set('yqlinks', serialize($yqlinks));
    message(0, jump('删除成功', url('plugin-setting-xl_yqlinks')));
}
?>