<?php exit;
$fid = param('fid', 0);
if ($fid == '8') {
    $ax_vest = kv_get('ax_vest');
    $bind_state = db_find_one("wechat_bind", array('uid' => $uid));
    $parray = array_merge(
        explode(",", $ax_vest['user2']['0']),
        explode(",", $ax_vest['user3']['0']),
        explode(",", $ax_vest['user4']['0']),
        explode(",", $ax_vest['user5']['0']),
        explode(",", $ax_vest['user6']['0']),
        explode(",", $ax_vest['user7']['0']),
        explode(",", $ax_vest['user8']['0']),
        explode(",", $ax_vest['user9']['0']),
        explode(",", $ax_vest['user10']['0']),
        explode(",", $ax_vest['user2']['1']),
        explode(",", $ax_vest['user3']['1']),
        explode(",", $ax_vest['user4']['1']),
        explode(",", $ax_vest['user5']['1']),
        explode(",", $ax_vest['user6']['1']),
        explode(",", $ax_vest['user7']['1']),
        explode(",", $ax_vest['user8']['1']),
        explode(",", $ax_vest['user9']['1']),
        explode(",", $ax_vest['user10']['1'])
    );
    if (!$bind_state['uid'] && !in_array($uid, $parray) && !in_array($user['gid'], [1, 4, 106])) {
        message('-1', jump('你需要绑定微信才可以发帖,<a href="' . url('wechat_union-bind') . '" style="color:#369">去绑定</a>'));
    }
}