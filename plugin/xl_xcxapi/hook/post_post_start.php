<?php exit;
$ax_vest = kv_get('ax_vest');
$bind_state = db_find_one("wechat_bind", array('uid' => $uid));
$parray = array_merge(
    explode(",", $ax_vest['user2']['2']),
    explode(",", $ax_vest['user3']['2']),
    explode(",", $ax_vest['user4']['2']),
    explode(",", $ax_vest['user5']['2']),
    explode(",", $ax_vest['user6']['2']),
    explode(",", $ax_vest['user7']['2']),
    explode(",", $ax_vest['user8']['2']),
    explode(",", $ax_vest['user9']['2']),
    explode(",", $ax_vest['user10']['2']),
    explode(",", $ax_vest['user2']['1']),
    explode(",", $ax_vest['user3']['1']),
    explode(",", $ax_vest['user4']['1']),
    explode(",", $ax_vest['user5']['1']),
    explode(",", $ax_vest['user6']['1']),
    explode(",", $ax_vest['user7']['1']),
    explode(",", $ax_vest['user8']['1']),
    explode(",", $ax_vest['user9']['1']),
    explode(",", $ax_vest['user10']['1'])
);
if (!$bind_state['uid'] && !in_array($uid, $parray)) {
    message('-1', jump('你需要绑定微信才可以回复,<a href="' . url('wechat_union-bind') . '" style="color:#369">去绑定</a>'));
}