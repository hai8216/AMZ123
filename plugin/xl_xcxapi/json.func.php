<?php

function C_success($data,$code='200'){
    //验证秘钥情况
//    checkSign();
    header('Content-type: application/json');
    echo json_encode(array(
        'errCode' => $code,
        'errMessage'=>'success',
        'data' => $data
    ));
    exit;
}
function dataNbsp($smg){
    return str_replace("&nbsp;", " ", $smg);
}
function ax_mg($tid){
    $arr = db_find_one('post', array('tid'=>$tid));
    return $arr;
}
function ax_pg($tid){
    $arr = db_find_one('post', array('isfirst'=>0,'tid'=>$tid),array('create_date'=>-1));
    return $arr;
}
function checkSign(){
    global $_POST;
    $secretKey = kv_get('xcx_setting')['xcx_json_Secret'];
    $par = strtoupper(md5(strtoupper(md5($secretKey)) . $_POST['timestamp']));
    if($par!=$_POST['secret']){
        C_error('sign error!');
    }
}

function getConfig($jsonfileds){
    return kv_get('xcx_setting')[$jsonfileds];
}
function C_error($errMessage,$errCode = '500')
{
    header('Content-type: application/json');
    echo json_encode(array(
        'errCode' => $errCode,
        'errMessage' => $errMessage
    ));
    exit;
}
//对二维数组进行键值过滤
function getvalues($variables, $keys, $subkeys = array()) {
    $return = array();
    foreach($variables as $key => $value) {
        foreach($keys as $k) {
            if($k{0} == '/' && preg_match($k, $key) || $key == $k) {
                if($subkeys) {
                    $return[$key] =getvalues($value, $subkeys);
                } else {
                    if(!empty($value) || !empty($_GET['debug']) || (is_numeric($value) && intval($value) === 0 )) {
                        $return[$key] = is_array($value) ? arraystring($value) : (string)$value;
                    }
                }
            }
        }
    }
    return $return;
}
function  arraystring($array) {
    foreach($array as $k => $v) {
        $array[$k] = is_array($v) ? arraystring($v) : (string)$v;
    }
    return $array;
}