<?php
if(!isset($_FILES['file']['tmp_name'])){
    $data = ["code"=>0,"imgUrl"=>[]];
    echo json_encode($data);
    exit;
}
$path = "upload/form-img/" . date('Ymd') . "/";
$ext = '.png';
$name = uniqid() . $ext;
saveTencent_input(fopen($_FILES['file']['tmp_name'],'rb'), $path . $name);
$data = ["code"=>200,"imgUrl"=>[["url"=>"https://img.amz123.com/".$path . $name,"name"=>$name]]];
echo json_encode($data);
exit;