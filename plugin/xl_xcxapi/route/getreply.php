<?php
//评论
$ucount = db_count('user', array('ask_type' => 2));
$pageall = rand(0, intval(ceil($ucount / 5)) - 1);
$postuid = db_find('user', array('ask_type' => 2), array(), $pageall, 5);
$tmp_id = param('tmp_id');
$fid = 8;
$thread = db_find_one("thread", array('tmp_id' => $tmp_id));
$posttmp = db_find_one("thread_post_tmp", array('tmpid' => $tmp_id, 'status' => 0,'dateline'=>array('>'=>0)));
if ($posttmp['id']) {
    $post = array(
        'tid' => $thread['tid'],
        'uid' => $postuid[rand(0,4)]['uid'],
        'create_date' => time(),
        'userip' => $longip,
        'isfirst' => 0,
        'doctype' => 0,
        'quotepid' => 0,
        'message' => $posttmp['message'],
    );
    $pid = post_create($post, $fid, 101);
    empty($pid) AND message(-1, lang('create_post_failed'));
    if ($pid) {
        db_update("thread_post_tmp", array('id' => $posttmp['id']), array('status' => 1));
        message(0, jump('操作成功',url('thread-'.$thread['tid'])));
    }
}else{
    message(0, jump('没有评论或没有更多',url('thread-'.$thread['tid'])));
}