<?php
if ($_FILES['file'] && $user) {
    $uplad_tmp_name = $_FILES['file']['tmp_name'];
    $uplad_name = $_FILES['file']['name'];
    $time = time();
    $uptype = explode(".", $uplad_name);
    $type = "jpg";
    $filepath = APP_PATH . "upload/attach/" . date('Ymd', $time) . "/";
    $wwwpath = "upload/attach/" . date('Ymd', $time) . "/";
    $new_file = time() . rand(100, 50000) . "." . $type;
    if (!file_exists($wwwpath . $new_file)) {
        mkdir($filepath, 0777,true);
        $file = $filepath . $new_file;
        move_uploaded_file($uplad_tmp_name, $file);
        chmod($file, 0644);
        $img_url1 = $wwwpath . $new_file;
        $data['url'] = $img_url1;
        $data['code'] = 200;
    }else{
        $data['code'] = 0;
    }
    echo json_encode($data);exit;
}