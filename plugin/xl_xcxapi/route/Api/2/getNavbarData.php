<?php

$navBarData = ax_nav_sel_cache(0);
$navBarData = CLFORUM(getvalues($navBarData, array('/^\d+$/'), array('linkid','url', 'name', 'isApp', 'note')));
foreach ($navBarData as $k => $v) {
    if (!$v['isApp']) {
        unset($navBarData[$k]);
    } else {
        $alldata[] = $v;
        $navBarData[$k]['child'] = CLFORUM(getvalues(ax_nav_sel_cache($v['linkid']), array('/^\d+$/'), array('linkid','url', 'name', 'isApp', 'note')));
    }
}
function CLFORUM($ary){
    foreach($ary as &$v){
        if(explode("forum-",$v['url'])>1){
            $v['url'] = str_replace("forum-","",$v['url']);
            $v['url'] = str_replace(".htm","",$v['url']);
        }
    }
    return $ary;
}
$data['allData'] = CLFORUM(getvalues(db_find('ax_nav_sel', array('isApp' => 1), array('rank' => -1)), array('/^\d+$/'), array('linkid','url', 'name', 'isApp', 'note')));
$data['cateData'] = $navBarData;
C_success($data);