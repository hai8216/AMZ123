<?php
require_once(APP_PATH . "/plugin/xl_aliopensearch/Config.inc.php");
use OpenSearch\Client\DocumentClient;
$tid = param('1');
$postinfos =  db_find_one('post',array('tid' => $tid,'isfirst'=>1));
$postinfo =  db_find_one('thread',array('tid' => $tid));
$user =  db_find_one('user',array('uid' => $postinfo['uid']));
$forum =  db_find_one('forum',array('fid' => $postinfo['fid']));
$tableName = 'bbs_post';
$documentClient = new DocumentClient($client);
$docs_to_upload = array();
$item['cmd'] = 'add';
$smsg = strip_tags($postinfos['message']);
$item["fields"] = array(
    'pid'=>$postinfos['pid'],
    'tid' => $postinfos['tid'],
    'subject' => $postinfo['subject'],
    'isfirst' => 1,
    'message' => str_replace("&nbsp;", " ", $smsg),
    'author' => $user['username'],
    'authorid' => $user['uid'],
    'fid' => $postinfo['fid'],
    'fname' => $forum['name'],
    'back_message' => '',
    'posttime'=>$postinfo['create_date'],
);

if ($postinfo['fid'] == '3') {
    $item["fields"]['ax_name'] = $postinfo['ax_name'];
    $item["fields"]['ax_email'] = $postinfo['ax_email'];
    $item["fields"]['ax_weixin'] = $postinfo['ax_weixin'];
    $item["fields"]['ax_ymx'] = $postinfo['ax_ymx'];
    $item["fields"]['ax_facebook'] = $postinfo['ax_facebook'];
    $item["fields"]['ax_country'] = $postinfo['ax_country'];
    $item["fields"]['subject'] = $postinfo['ax_name']."&emsp;".$postinfo['ax_country']."&emsp;".$postinfo['ax_email']."&emsp;";
}
if ($postinfo['fid'] == '7' || $postinfo['fid']=='6') {

    $item["fields"]['ax_qq'] = $postinfo['ax_qq'];
    $item["fields"]['ax_qqname'] = $postinfo['ax_qqname'];
    $item["fields"]['ax_wx'] = $postinfo['ax_wx'];
    $item["fields"]['ax_axname'] = $postinfo['ax_axname'];
    $item["fields"]['ax_wxname'] = $postinfo['ax_wxname'];
    $item["fields"]['ax_alpay'] = $postinfo['ax_alpay'];
    $item["fields"]['ax_wab'] = $postinfo['ax_wab'];
    $item["fields"]['ax_dianpu'] = $postinfo['ax_dianpu'];
    $item["fields"]['ax_ydpurl'] = $postinfo['ax_ydpurl'];
    if($postinfo['fid']=='6'){
        $item["fields"]['subject'] = $postinfo['ax_axname']."&emsp;".$postinfo['ax_alpay']."&emsp;".$postinfo['ax_wx']."&emsp;".$postinfo['ax_qq'];
    }else{
        $item["fields"]['subject'] = $postinfo['ax_wab']."&emsp;".$postinfo['ax_dianpu'];
    }

}
$docs_to_upload[] = $item;
$json = json_encode($docs_to_upload);

$ret = $documentClient->push($json, $appName, $tableName);
message(1, jump('操作成功', url('thread-'.$tid)));

