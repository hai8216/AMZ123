<?php
if(rand(0,5)<4){
    echo "error";
    exit;
}
$type = param('type', 1);
$fid = 8;
$ucount = db_count('user',  array('ask_type'=>1));
$pageall = rand(0,intval(ceil($ucount/5))-1);
$postuid = db_find('user',  array('ask_type'=>1), array(), $pageall, 5);
$sid = '';
if ($type == '1') {
    //随机5个帖子
    $list = db_find("thread_tmp", array('status' => 0), array(), 1, 5);
    $i=0;
    foreach($list as $k=> $v){
        $thread = array(
            'fid' => $fid,
            'uid' => $postuid[$k]['uid'],
            'sid' => $sid,
            'subject' => $v['subject'],
            'message' => $v['message'],
            'time' => time(),
            'longip' => $longip,
            'doctype' => 0,
        );
        $remote_img = db_find_one('setting', array('name' => 'remote_img'));
        if ($remote_img['value']) {
            preg_match_all("/<img[^>]*src\s?=\s?[\'|\"]([^\'|\"]*)[\'|\"]/is", $v['message'], $img);
            $imgArr = $img[1];
            if ($imgArr) {
                foreach ($imgArr as $k => $v) {
                    if (strstr($v, 'http')) {
                        $path = "./upload/remote/" . date('Y-m-d');
                        if (!file_exists($path)) mkdir($path, 0777, true);
                        $extname = substr($v, strrpos($v, '.'));
                        if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
                            $ext = $extname;
                        } else {
                            $ext = '.jpg';
                        }
                        $name = uniqid() . $ext;
                        saveImg($v, $path . "/" . $name);
                        //file_put_contents($path."/".$name, file_get_contents(isset($_get[url])?$_get[url]:$v));
                        $message = str_replace($v, $path . "/" . $name, $message);
                        $thread['message'] = $message;
                    }
                }
            }
        }

        $tid = thread_create($thread, $pid);
        $pid === FALSE AND message(-1, lang('create_post_failed'));
        $tid === FALSE AND message(-1, lang('create_thread_failed'));
        if ($tid) {
            db_update("thread", array('tid' => $tid), array('tmp_id' => $v['id']));
             db_update("thread_tmp", array('id' => $v['id']), array('status' => 1));
            //有封面就保存封面
            if ($_POST['article_thumbnail']) {
                $extname = substr($_POST['article_thumbnail'], strrpos($_POST['article_thumbnail'], '.'));
                if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
                    $ext = $extname;
                } else {
                    $ext = '.jpg';
                }
                $name = uniqid() . $ext;
                $path = "./upload/remote/" . date('Y-m-d');
                saveImg($_POST['article_thumbnail'], $path . "/" . $name);
            }
            db_update("thread", array('tid' => $tid), array('thumb' => $path . "/" . $name));

        }
    }
    $i++;
}
echo "5条发布成功";