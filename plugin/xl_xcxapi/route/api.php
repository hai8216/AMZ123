<?php
//header('Access-Control-Allow-Origin:*');
$action = param(1);
include APP_PATH . "plugin/xl_xcxapi/json.func.php";
include APP_PATH . "plugin/xl_xcxapi/Api.func.php";
$ApiVersion = param('apiV', 1);
if ($ApiVersion == '2') {
    include APP_PATH . "plugin/xl_xcxapi/route/Api/".$ApiVersion."/".$action.".php";
    exit;
} else {

    $zdw = array(
        'CNY' => '人民币',
        'USD' => '美元',
        'JPY' => '日元',
        'HKD' => '港元',
        'EUR' => '欧元',
        'CAD' => '加拿大元',
        'AUD' => '澳元',
        'GBP' => '英镑'
    );
    if ($action == 'huilv') {
        $hl = db_find_one("exchange", array(), array('dateline' => -1));
        $r['CNY'] = 1;

        $r['USD'] = $hl['m'];

        $r['JPY'] = $hl['r'];

        $r['HKD'] = $hl['hk'];

        $r['EUR'] = $hl['o'];

        $r['CAD'] = $hl['j'];

        $r['AUD'] = $hl['od'];


        $r['GBP'] = $hl['y'];

        $a = 0;
        foreach ($r as $k => $v) {
            $rl[$a] = array(
                'e' => $k,
                'n' => $v,
                'f' => getF($k),
                'zdw' => $zdw[$k]
            );
            $a++;
        }
        //默认4个，JS不太好处理key键值的数据
        $mr = array(
            $rl[0], $rl[1], $rl[7], $rl[4], $rl[2]
        );
        if (param('host')) {
            if (param('host') == 'amazon.com') {
                C_success(array('hl' => $hl['m']));
                exit;
            } elseif (param('host') == 'amazon.ca') {
                C_success(array('hl' => $hl['j']));
                exit;
            } elseif (param('host') == 'amazon.com.au') {
                C_success(array('hl' => $hl['od']));
                exit;
            } elseif (param('host') == 'amazon.co.uk') {
                C_success(array('hl' => $hl['y']));
                exit;
            } elseif (param('host') == 'amazon.de' || param('host') == 'amazon.fr' || param('host') == 'amazon.it' || param('host') == 'amazon.es') {
                C_success(array('hl' => $hl['o']));
                exit;
            }
        }
        C_success(array('hl' => $rl, 'mr' => $mr, 'dateline' => date('Y/m/d H:i', $hl['dateline'])));
    } elseif ($action == 'accesstoken') {
        echo kv_get('wechat_union_access_token');
        exit;
    } elseif ($action == 'wxlogin') {
        $code = param('wxcode');
        $fromappid = param('fromappid', 0);
        if ($fromappid) {
            include APP_PATH . "plugin/xl_wechat_union/class/wechat_1.class.php";
        } else {
            include APP_PATH . "plugin/xl_wechat_union/class/wechat.class.php";
        }

        $wxApi = new Wechat_amz123();
        $wxApidata = $wxApi->getSession_key($code);
        C_error(array('session_key' => $wxApidata->session_key));
//    $unid = $wxApidata->unionid;
//    if (!$unid) {
//        //没有关注过公众号之类的需要去解密
//        C_error(array('session_key' => $wxApidata->session_key));
//    } else {
//        $bind_state = db_find_one("wechat_bind", array('unid' => $unid));
//        if ($bind_state) {
//            //分配一个userToken
//            $user = user_read($bind_state['uid']);
//            $data['userToken'] = $wxApi->setUsertoken($bind_state['uid'], $unid);
//            $data['userinfo'] = getvalues(array($user), array('/^\d+$/'), array('uid', 'username', 'avatar_url'))[0];
//            C_success($data);
//        }
//    }
    } elseif ($action == 'otherread') {
        $page = param('page', 1);
        $tid = param('tid', 1);
        $fid = param('fid', 1);
        $pagesize = 10;
        $extra = '';
        $post_list = thread_find(array('fid' => $fid, 'tid' => array('!=' => $tid)), array('create_date' => -1), $page, $pagesize);
        foreach ($post_list as $v) {
            $olist[] = $v;
        }
        $theeadJSon['otherlist'] = $olist;
        C_success($theeadJSon);
    } elseif ($action == 'wxdecode') {
        include_once APP_PATH . "plugin/xl_xcxapi/errorCode.php";
        include_once APP_PATH . "plugin/xl_xcxapi/wxBizDataCrypt.php";
        include APP_PATH . "plugin/xl_wechat_union/class/wechat.class.php";
        $wxApi = new Wechat_amz123();
        $encryptedData = param('encryptedData');
        $iv = param('iv');
        $sessionKey = param('session_key');
        $fromappid = param('fromappid', 0);
        if ($fromappid) {
            $appid = 'wx8ffec6dc364f3f12';
        } else {
            $appid = 'wx792bf24bce390228';
        }

        $pc = new WXBizDataCrypt($appid, $sessionKey);
        $errCode = $pc->decryptData($encryptedData, $iv, $data);

        if ($errCode == 0) {
            $unid = json_decode($data, true)['unionId'];
            $bind_state = db_find_one("wechat_bind", array('unid' => $unid));
            if ($bind_state) {
                //分配一个userToken
                $user = user_read($bind_state['uid']);
                $datas['userToken'] = user_token_gen($bind_state['uid']);
                $datas['userinfo'] = getvalues(array($user), array('/^\d+$/'), array('uid', 'username', 'avatar_url', 'mobile'))[0];
                C_success($datas);
            } else {
                C_error(array('wechat_user' => json_decode($data, true)), '498');
            }
        } else {
            C_error('用户信息获取失败，请重试', '499');
        }
    } elseif ($action == 'getmy') {
        $mod = param('mod', 'fav');
        if ($mod == 'fav') {
            $page = param('page', 1);
            $threadlist = haya_favorite_find(array('uid' => $uid), array('create_date' => -1), $page, 20);
            $count = db_count("haya_favorite", array('uid' => $uid));
            foreach ($threadlist as &$thread) {
                if ($thread['fid'] == '3') {
                    if ($thread['posts'] > 0) {
                        $thread['message'] = dataNbsp(ax_pg($thread['tid'])['message']);
                    } else {
                        $thread['message'] = strip_tags(ax_mg($thread['tid'])['message']);
                    }
                    $thread['subject'] = $thread['ax_name'] . " " . $thread['ax_country'] . " " . $thread['ax_email'];
                } elseif ($thread['fid'] == '6') {
                    if ($thread['posts'] > 0) {
                        $thread['message'] = dataNbsp(ax_pg($thread['tid'])['message']);
                    } else {
                        $thread['message'] = dataNbsp(strip_tags(ax_mg($thread['tid'])['message']));
                    }
                    $thread['subject'] = $thread['ax_axname'] . " " . $thread['ax_alpay'] . " " . $thread['ax_wx'] . " " . $thread['ax_qq'];
                }

                $thread = thread_safe_info($thread);
            }
            //降
            foreach ($threadlist as $v) {
                $threadlists[] = $v;
            }
            C_success(array('threadlist' => $threadlists, 'count' => $count));
        } elseif ($mod == 'post') {
            $page = param('page', 1);
            $threadlist = mythread_find_by_uid($uid, $page, 20);
            $count = db_count("mythread", array('uid' => $uid));
            foreach ($threadlist as &$thread) {
                if ($thread['fid'] == '3') {
                    if ($thread['posts'] > 0) {
                        $thread['message'] = dataNbsp(ax_pg($thread['tid'])['message']);
                    } else {
                        $thread['message'] = strip_tags(ax_mg($thread['tid'])['message']);
                    }
                    $thread['subject'] = $thread['ax_name'] . " " . $thread['ax_country'] . " " . $thread['ax_email'];
                } elseif ($thread['fid'] == '6') {
                    if ($thread['posts'] > 0) {
                        $thread['message'] = dataNbsp(ax_pg($thread['tid'])['message']);
                    } else {
                        $thread['message'] = dataNbsp(strip_tags(ax_mg($thread['tid'])['message']));
                    }
                    $thread['subject'] = $thread['ax_axname'] . " " . $thread['ax_alpay'] . " " . $thread['ax_wx'] . " " . $thread['ax_qq'];
                }

                $thread = thread_safe_info($thread);
            }
            //降
            foreach ($threadlist as $v) {
                $threadlists[] = $v;
            }
            C_success(array('threadlist' => $threadlists, 'count' => $count));

        }
//    } elseif ($action == 'pushslider') {
//        $tid = param(2);
//        $t = db_find_one("thread", array('tid' => $tid));
//        if ($gid == 1 && $uid) {
//            $pusd = db_find_one("ax_ads", array('url' => $tid));
//            if ($pusd['linkid']) {
//                echo json_encode(array('code' => 0, 'msg' => '该帖已被推送过了'));
//                exit;
//            }
//            $insert = array(
//                'type' => 9,
//                'icon' => $t['thumb'],
//                'name' => $t['subject'],
//                'url' => $tid,
//            );
//            db_insert("ax_ads", $insert);
//            echo json_encode(array('code' => 1));
//        } else {
//            echo json_encode(array('code' => 0, '推送失败'));
//        }
    } elseif ($action == 'bindwechat') {
        $kv_mobile = kv_get('mobile_setting');
        $login_type = $kv_mobile['login_type'];
        $mobile = param('mobile');            // 邮箱或者手机号 / email or mobile
        empty($mobile) AND message('mobile', lang('please_input') . lang('login_type_' . $login_type));
        if (is_mobile($mobile, $err)) {
            $_user = user_read_by_mobile($mobile);
            empty($_user) AND message('mobile', lang('mobile_not_exists'));
        } elseif (is_email($mobile, $err)) {
            ($login_type != 1 && $login_type != 2) AND message('mobile', lang('not_allow_login_by_email'));
            $_user = user_read_by_email($mobile);
            empty($_user) AND message('mobile', lang('email_not_exists'));
        } else {
            ($login_type != 1 && $login_type != 3) AND message('mobile', lang('not_allow_login_by_username'));
            $_user = user_read_by_username($mobile);
            empty($_user) AND message('mobile', lang('username_not_exists'));
        }

        $password = param('password');
        !is_password($password, $err) AND message('password', $err);
        md5($password . $_user['salt']) != $_user['password'] AND message('password', lang('password_incorrect'));

        user_update($_user['uid'], array('login_ip' => $longip, 'login_date' => $time, 'logins+' => 1));

        $uid = $_user['uid'];
        $bind_statue = db_find_one("wechat_bind", array('openid' => param('openId')));
        if ($bind_statue['uid']) {
            message('mobile', '该微信绑定到了其他账号');
        } else {
            $user = getvalues(array($_user), array('/^\d+$/'), array('uid', 'username', 'avatar_url', 'mobile'))[0];
            //执行绑定操作
            $binddata = array('uid' => $uid, 'xcx_openid' => param('openId'), 'unid' => param('unionId'), 'dateline' => time(), 'username' => param('username'), 'bindtype' => 1);
            db_insert("wechat_bind", $binddata);
            message(0, array('message' => lang('user_login_successfully'), 'userToken' => user_token_gen($_user['uid']), 'userinfo' => $user));
        }
    }

    function getF($v)
    {
        if ($v == 'USD' || $v == 'AUD' || $v == 'CAD' || $v == 'HKD') {
            return "$";
        } elseif ($v == 'JPY' || $v == 'CNY') {
            return "¥";
        } elseif ($v == 'EUR') {
            return "€";
        } elseif ($v == 'GBP') {
            return "£";
        }
    }
}