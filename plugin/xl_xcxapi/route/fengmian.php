<?php

$tid = param('tid', '');
if (!$tid) {
    exit;
} else {

    header("Content-Type:text/html; charset=utf-8");


    function getCurl($url)
    {
        $ch = curl_init();
        if (stripos($url, 'https://') !== false) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);
        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }

    function textalign($card, $str, $width, $x, $y, $fontsize, $fontfile, $color, $rowheight, $maxrow)
    {
        $tempstr = "";
        $row = 0;
        for ($i = 0; $i < mb_strlen($str, 'utf8'); $i++) {
            if ($row >= $maxrow) {
                break;
            }
            //第一 获取下一个拼接好的宽度 如果下一个拼接好的已经大于width了，就在当前的换行 如果不大于width 就继续拼接
            $tempstr = $tempstr . mb_substr($str, $i, 1, 'utf-8');//当前的文字
            $nextstr = $tempstr . mb_substr($str, $i + 1, 1, 'utf-8');//下一个字符串
            $nexttemp = imagettfbbox($fontsize, 0, $fontfile, $nextstr);//用来测量每个字的大小
            $nextsize = ($nexttemp[2] - $nexttemp[0]);
            if ($nextsize > $width - 10) {//大于整体宽度限制 直接换行 这一行写入画布
                $row = $row + 1;
                imagettftext($card, $fontsize, 0, $x, $y, $color, $fontfile, $tempstr);
                $y = $y + $fontsize + $rowheight;
                $tempstr = "";
            } else if ($i + 1 == mb_strlen($str, 'utf8') && $nextsize < $width - 10) {
                imagettftext($card, $fontsize, 0, $x, $y, $color, $fontfile, $tempstr);
            }
        }
        return $row;
    }

    function dcutstr($string, $length, $dot = ' ...')
    {
        if (strlen($string) <= $length) {
            return $string;
        }

        $pre = chr(1);
        $end = chr(1);
        $string = str_replace(array('&amp;', '&quot;', '&lt;', '&gt;'),
            array($pre . '&' . $end, $pre . '"' . $end, $pre . '<' . $end, $pre . '>' . $end), $string);

        $strcut = '';
        $n = $tn = $noc = 0;
        while ($n < strlen($string)) {

            $t = ord($string[$n]);
            if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
                $tn = 1;
                $n++;
                $noc++;
            } elseif (194 <= $t && $t <= 223) {
                $tn = 2;
                $n += 2;
                $noc += 2;
            } elseif (224 <= $t && $t <= 239) {
                $tn = 3;
                $n += 3;
                $noc += 2;
            } elseif (240 <= $t && $t <= 247) {
                $tn = 4;
                $n += 4;
                $noc += 2;
            } elseif (248 <= $t && $t <= 251) {
                $tn = 5;
                $n += 5;
                $noc += 2;
            } elseif ($t == 252 || $t == 253) {
                $tn = 6;
                $n += 6;
                $noc += 2;
            } else {
                $n++;
            }

            if ($noc >= $length) {
                break;
            }

        }
        if ($noc > $length) {
            $n -= $tn;
        }

        $strcut = substr($string, 0, $n);


        $strcut = str_replace(array($pre . '&' . $end, $pre . '"' . $end, $pre . '<' . $end, $pre . '>' . $end),
            array('&amp;', '&quot;', '&lt;', '&gt;'), $strcut);

        $pos = strrpos($strcut, chr(1));
        if ($pos !== false) {
            $strcut = substr($strcut, 0, $pos);
        }
        return $strcut . $dot;
    }
    $pt = cache_get('thread_fengmian_' . $tid);
    if ($pt && !param('reload')) {
        $data['img']  = "https://img.amz123.com/thread_fengmian/" . $pt . ".jpg";
        $data['code'] = 200;
        echo json_encode($data);
    } else {
        $font = APP_PATH . '/view/font/zts.ttf';
        $font2 = APP_PATH . '/view/font/zts.ttf';
        $logo = APP_PATH . '/view/img/logo.png';
        $line = APP_PATH . '/view/img/line.png';
        $tfujia = post_thumb_message($tid);
        $title = thread__read($tid)['subject'];
        $cover = I($tfujia['thumb'], 'fengmian4');
        $message = $tfujia['message'];
// 灰色背景的尺寸
        $boardWidth = 800;
        $boardHeight = 1000;
        $boardSource = imagecreatetruecolor($boardWidth, $boardHeight);
        $white = imagecolorallocate($boardSource, 255, 255, 255);
        imagefill($boardSource, 0, 0, $white);
        $imgContent = getCurl($cover);
        $imageSource = imagecreatefromstring($imgContent);
        list($imageWidth, $imageHeight) = getimagesizefromstring($imgContent);
        imagecopyresampled($boardSource, $imageSource, 0, 0, 0, 0, 800, 420, $imageWidth, $imageHeight);
        $imgBxheight = 420;
        $imgLogo = imagecreatefrompng($logo);
        imagecopyresampled($boardSource, $imgLogo, 20, 20, 0, 0, 150, 36, 300, 72);
//载入标题
        $black = imagecolorallocate($boardSource, 0, 0, 0);
        $row = textalign($boardSource, $title, 720, 60, $imgBxheight + 70, '29', $font, $black, 20, 2);
//载入内容
        if ($row > 0) {
            //文字多行，限定2行
            $mline = 200;
        } else {
            $mline = 160;
        }
        $textcolor = imagecolorallocate($boardSource, 102, 102, 102);
        textalign($boardSource, dcutstr($message, 190), 720, 60, $imgBxheight + $mline, '20', $font2, $textcolor, 40, 3);
//虚线
        $imgLogo = imagecreatefrompng($line);
        imagecopyresampled($boardSource, $imgLogo, 0, 800, 0, 0, 800, 36, 800, 36);


//媒体
        $h2color = imagecolorallocate($boardSource, 40, 40, 40);
        $h3color = imagecolorallocate($boardSource, 150, 150, 150);
        imagettftext($boardSource, 20, 0, 60, 890, $h2color, $font, "AMZ123跨境导航 ");
        imagettftext($boardSource, 17, 0, 60, 940, $h3color, $font, "跨境电商出海门户；做跨境电商，就上AMZ123 ");

//qrcode
        require_once APP_PATH . "/model/phpqrcode.php";
        $errorCorrectionLevel = '1';//容错级别
        $matrixPointSize = 6;//生成图片大小
        $thread = "https://www.amz123.com/" . url('thread-'.$tid);
        $imageSource = imagecreatefromstring(base64_decode(QRcode::PNG($thread, false)));
        imagecopyresampled($boardSource, $imageSource, 620, 850, 0, 0, 116, 116, 116, 116);


        ob_start();

        imagepng($boardSource);
        $image_data = ob_get_contents();
        ob_end_clean();
        $image_data_base64 = $image_data;
        $time = $tid.time();
        saveTencent_input($image_data_base64, "thread_fengmian/" . $time . ".jpg");
        imagedestroy($boardSource);
        imagedestroy($imageSource);
        imagedestroy($imgLogo);
        cache_set("thread_fengmian_" . $tid, $time, 60 * 60 * 24 * 30);
        $data['img']  = "https://img.amz123.com/thread_fengmian/" . $time . ".jpg";
        $data['code'] = 200;
        echo json_encode($data);
    }
}
?>