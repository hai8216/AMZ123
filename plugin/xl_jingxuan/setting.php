<?php

!defined('DEBUG') and exit('Access Denied.');
$tpllist = ["标准版", "图片版", "服务版"];
$action = param(3) ?: "list";
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $stid = param('stid');
    if ($stid) {
        $map['tid'] = $stid;
    } else {
        $map = [];
    }
    $arrlist = db_find("thread_jingxuan", $map, array(), $page, $pagesize);
    $c = db_count("thread_jingxuan", $map);
    $pagination = pagination(url("plugin-setting-xl_jingxuan-{page}", array('stid' => $stid)), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_jingxuan/admin/list.html');
} elseif ($action == 'create') {
    if ($method == 'GET') {
        if (param('id')) {
            $id = param('id');
            $val = db_find_one("thread_jingxuan", ["id" => $id]);
        }
        $input['tagid'] = form_text("tagid", $val['tagid'] ?: 0);
        $input['title'] = form_text("title", $val['title']);
        $input['tagname'] = form_text("tagname", $val['tagname']);
        $input['tpl'] = form_select("tpl", $tpllist, $val['tpl']);
        $input['title2'] = form_text("title2", $val['title2']);
        $input['links'] = form_text("links", $val['links']);
        $input['desc'] = form_text("desc", $val['desc']);
        $input['orderid'] = form_text("orderid", $val['orderid']);
        $input['btntext'] = form_text("btntext", $val['btntext']);
        $input['btnlinks'] = form_text("btnlinks", $val['btnlinks']);
        include _include(APP_PATH . 'plugin/xl_jingxuan/admin/create.html');
    } else {
        $input['tagid'] = param("tagid");
        $input['title'] = param("title");
        $input['tagname'] = param("tagname");
        $input['tpl'] = param("tpl");
        $input['title2'] = param("title2");
        $input['links'] = param("links");
        $input['desc'] = param("desc");
        $input['orderid'] = param("orderid");
        $input['btntext'] = param("btntext");
        $input['btnlinks'] = param("btnlinks");
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['thumb'] = $path . $name;
            }
        }
        if (param('id')) {
            db_update("thread_jingxuan", ["id" => param('id')], $input);
            $endid = param('id');
        } else {
            $endid = db_insert("thread_jingxuan", $input);
        }
        if ($input['tagid']) {
            $arrayTag = explode(",", $input['tagid']);
            if ($arrayTag[0]) {
                db_delete("thread_jingxuan_tag", ["jid" => $endid]);
                foreach ($arrayTag as $v) {
                    db_insert("thread_jingxuan_tag", ["jid" => $endid, "tagid" => $v]);
                }
            }

        }
        message(0, 'ok');
    }
}elseif ($action == 'del') {
    db_delete("thread_jingxuan", ["id" => param('id')]);
    message(0, jump('ok'));

}