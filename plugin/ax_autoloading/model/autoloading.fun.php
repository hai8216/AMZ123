<?php 
function pager_all($url, $totalnum, $page, $pagesize = 20) {
	$totalpage = ceil($totalnum / $pagesize);
	if($totalpage < 2) return '';
	$page = min($totalpage, $page);
	$s = '';
	$page > 1 AND $s .= '<li class="previous"><a href="'.str_replace('{page}', $page-1, $url).'"></a></li>';
	if($page > 1){
		$s .= "<span class='now-page'>"." $page / $totalpage "."</span>";
	}
		$totalnum >= $pagesize AND $page != $totalpage AND $s .= '<span class="next-page"  id="npage"><a href="'.str_replace('{page}', $page+1, $url).'"></a></span>';
		return $s;
}
?>