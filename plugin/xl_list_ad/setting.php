<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') and exit('Access Denied.');
$action = param('action', 'adadmincp');

if ($action == 'adpos') {
    if ($method == 'GET') {
        $adpos = db_find("list_ad", [], ["pos" => "-1"], 1, 40);
        include _include(APP_PATH . "/plugin/xl_list_ad/setting.htm");
    } else {
        $ind['adtitle'] = param('adtitle');
        $ind['addesc'] = param('addesc');
        $ind['pos'] = param('pos');
        if ($ind['pos'] > 40) {
            message(-1, '广告位最大40');
        } else {
            db_insert("list_ad", $ind);
            message(0, jump('添加成功', url('plugin-setting-xl_list_ad')));
        }
    }
} elseif ($action == 'adadmincp') {
    $adlist = db_find("list_ad_data");
    $fup = param('fup');
    include _include(APP_PATH . "/plugin/xl_list_ad/adadmincp.htm");
} elseif ($action == 'edit') {
    if ($method == 'GET') {
        $fup = intval(param('fup'));
        $id = intval(param('id'));
        if ($id) {
            $val = db_find_one("list_ad_data", ["id" => $id]);
            $sval = unserialize($val['adtext']);
        } else {
            $val = ["id" => 0, "ad_tag_text" => "", "ad_type" => 0];
        }
        include _include(APP_PATH . "/plugin/xl_list_ad/edit.htm");
    } else {
        $data['ad_tag_text'] = param('ad_tag_text', "广告");
        $data['ad_type'] = param('ad_type');
        $data['adtitle'] = param('adtitle');
        $data['fup'] = param('fup');
        $data['pos'] = param('pos');
        if ($data['ad_type'] == '1') {
            $serdata['type1_list_desc'] = param('type1_list_desc');

            $serdata['type1_list_avatar'] = param('type1_list_avatar');

            $serdata['type1_list_uname'] = param('type1_list_uname');

            $serdata['type1_list_title'] = param('type1_list_title');
            $serdata['type1_list_link'] = param('type1_list_link');
            if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('type1_hidden_img_tpl'), $res)) {
                $type = $res[2];
                $path = "./upload/index_icon/" . date('Ymd') . "/";
                $ext = '.' . $type;
                $name = uniqid() . $ext;
                if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('type1_hidden_img_tpl'))), $path . $name)) {
                    $serdata['icon'] = $path . $name;
                }
            } else {
                if (param('id')) {
                    $val = db_find_one("list_ad_data", ["id" => param('id')]);
                    $sval = unserialize($val['adtext']);
                    $serdata['icon'] = $sval['icon'];
                }

            }

        } else if ($data['ad_type'] == '3') {
            $serdata['type3_list_link'] = param('type3_list_link');
            if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('type3_hidden_img_tpl'), $res)) {
                $type = $res[2];
                $path = "./upload/index_icon/" . date('Ymd') . "/";
                $ext = '.' . $type;
                $name = uniqid() . $ext;
                if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('type3_hidden_img_tpl'))), $path . $name)) {
                    $serdata['icon'] = $path . $name;
                }
            } else {
                if (param('id')) {
                    $val = db_find_one("list_ad_data", ["id" => param('id')]);
                    $sval = unserialize($val['adtext']);
                    $serdata['icon'] = $sval['icon'];
                }
            }
        } else if ($data['ad_type'] == '2') {
            $serdata['type2_right_text'] = param('type2_right_text');
            $serdata['type2_right_link'] = param('type2_right_link');


            $serdata['type2_data_1_title'] = param('type2_data_1_title');
            $serdata['type2_data_1_link'] = param('type2_data_1_link');
            $serdata['type2_data_1_desc'] = param('type2_data_1_desc');

            $serdata['type2_data_2_title'] = param('type2_data_2_title');
            $serdata['type2_data_2_link'] = param('type2_data_2_link');
            $serdata['type2_data_2_desc'] = param('type2_data_2_desc');

            $serdata['type2_data_3_title'] = param('type2_data_3_title');
            $serdata['type2_data_3_link'] = param('type2_data_3_link');
            $serdata['type2_data_3_desc'] = param('type2_data_3_desc');


            if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('type41_hidden_img_tpl'), $res)) {
                $type = $res[2];
                $path = "./upload/index_icon/" . date('Ymd') . "/";
                $ext = '.' . $type;
                $name = uniqid() . $ext;
                if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('type41_hidden_img_tpl'))), $path . $name)) {
                    $serdata['model_img_1'] = $path . $name;
                }
            } else {
                if (param('id')) {
                    $val = db_find_one("list_ad_data", ["id" => param('id')]);
                    $sval = unserialize($val['adtext']);
                    $serdata['model_img_1'] = $sval['model_img_1'];
                }
            }


            if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('type42_hidden_img_tpl'), $res)) {
                $type = $res[2];
                $path = "./upload/index_icon/" . date('Ymd') . "/";
                $ext = '.' . $type;
                $name = uniqid() . $ext;
                if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('type42_hidden_img_tpl'))), $path . $name)) {
                    $serdata['model_img_2'] = $path . $name;
                }
            } else {
                if (param('id')) {
                    $val = db_find_one("list_ad_data", ["id" => param('id')]);
                    $sval = unserialize($val['adtext']);
                    $serdata['model_img_2'] = $sval['model_img_2'];
                }
            }


            if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('type43_hidden_img_tpl'), $res)) {
                $type = $res[2];
                $path = "./upload/index_icon/" . date('Ymd') . "/";
                $ext = '.' . $type;
                $name = uniqid() . $ext;
                if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('type43_hidden_img_tpl'))), $path . $name)) {
                    $serdata['model_img_3'] = $path . $name;
                }
            } else {
                if (param('id')) {
                    $val = db_find_one("list_ad_data", ["id" => param('id')]);
                    $sval = unserialize($val['adtext']);
                    $serdata['model_img_3'] = $sval['model_img_3'];
                }
            }


            if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('type4_hidden_img_tpl'), $res)) {
                $type = $res[2];
                $path = "./upload/index_icon/" . date('Ymd') . "/";
                $ext = '.' . $type;
                $name = uniqid() . $ext;
                if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('type4_hidden_img_tpl'))), $path . $name)) {
                    $serdata['model_logo'] = $path . $name;
                }
            } else {
                if (param('id')) {
                    $val = db_find_one("list_ad_data", ["id" => param('id')]);
                    $sval = unserialize($val['adtext']);
                    $serdata['model_logo'] = $sval['model_logo'];
                }
            }
        }
        $data['adtext'] = serialize($serdata);
        if (param('id')) {
            db_update("list_ad_data", ["id" => param('id')], $data);
        } else {
            db_insert("list_ad_data", $data);
        }

        message(0, jump('添加成功1', url('plugin-setting-xl_list_ad')));
    }
} elseif ($action == 'set') {
    if ($method == 'GET') {
        $kvs = kv_get("list_ad_set");
        $input['re_author'] = form_textarea('re_author', $kvs['re_author']);
        $input['re_zlzz'] = form_textarea('re_zlzz', $kvs['re_zlzz']);
        include _include(APP_PATH . "/plugin/xl_list_ad/set.htm");
    }else{
        $kvs['re_author'] = param('re_author');
        $kvs['re_zlzz'] = param('re_zlzz');
        kv_set("list_ad_set",$kvs);
        message(0, jump('操作成功', url('plugin-setting-xl_list_ad',array('action'=>'set'))));
    }
}elseif ($action == 'delete') {
    if(param('id')) {
        db_delete("list_ad_data", ['id' => param('id')]);
        message(0, 'ok');
    }else{
        message(1, 'no');
    }
}