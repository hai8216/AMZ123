<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');

if($method == 'GET') {

    $kv = kv_get('hd_vcode');


    $input = array();
    $input['hd_vcode_appId'] = form_text('hd_vcode_appId', $kv['hd_vcode_appId']);
    $input['hd_vcode_appSecret'] = form_text('hd_vcode_appSecret', $kv['hd_vcode_appSecret']);

    $input['hd_vcode_user_login_on'] = form_radio_yes_no('hd_vcode_user_login_on', $kv['hd_vcode_user_login_on']);
    $input['hd_vcode_user_create_on'] = form_radio_yes_no('hd_vcode_user_create_on', $kv['hd_vcode_user_create_on']);
    $input['hd_vcode_user_findpw_on'] = form_radio_yes_no('hd_vcode_user_findpw_on', $kv['hd_vcode_user_findpw_on']);
    $input['hd_vcode_thread_create_on'] = form_radio_yes_no('hd_vcode_thread_create_on', $kv['hd_vcode_thread_create_on']);
    $input['hd_vcode_post_create_on'] = form_radio_yes_no('hd_vcode_post_create_on', $kv['hd_vcode_post_create_on']);

    // hook plugin_vcode_setting_get_end.htm

    include _include(APP_PATH.'plugin/xn_captcha/setting.htm');

} else {

    $kv = array();
    $kv['hd_vcode_appId'] = param('hd_vcode_appId');
    $kv['hd_vcode_appSecret'] = param('hd_vcode_appSecret');

    $kv['hd_vcode_user_login_on'] = param('hd_vcode_user_login_on');
    $kv['hd_vcode_user_create_on'] = param('hd_vcode_user_create_on');
    $kv['hd_vcode_user_findpw_on'] = param('hd_vcode_user_findpw_on');
    $kv['hd_vcode_thread_create_on'] = param('hd_vcode_thread_create_on');
    $kv['hd_vcode_post_create_on'] = param('hd_vcode_post_create_on');

    // hook plugin_vcode_setting_kv_set_before.htm
    kv_set('hd_vcode', $kv);

    // hook plugin_vcode_setting_post_end.htm
    message(0, '修改成功');
}

?>