$kv_vcode = kv_get('hd_vcode');
if (!empty($kv_vcode['hd_vcode_thread_create_on'])) {
    if (!$_POST['vToken']) {
        message('500', '请滑动验证码');
    }
    include APP_PATH . "/plugin/xn_captcha/veri/CaptchaClient.php";
    $appId = $kv_vcode['hd_vcode_appId'];
    $appSecret = $kv_vcode['hd_vcode_appSecret'];
    $client_hd = new CaptchaClient($appId, $appSecret);
$client_hd->setTimeOut(2);
    $response = $client_hd->verifyToken($_POST['vToken']);
    if ($response->result == false) {
        message('500', '验证码错误');
    }
}
