<?php
$action = param('1');
include _include(APP_PATH . 'plugin/xl_video/function.php');
$action_ary = ["upload","doUpload","getSign","mylist","callback","ajax","tags"];
$topcate = db_find("video_cate", ["fupid" => 0]);
if (!$action) {
    $videolist = db_find("video",["back_status"=>1,"status"=>1],["create_time"=>'-1'],1,40);
    $bannlist =  db_find("video_banner",[],[],1,5);
    include _include(APP_PATH . 'plugin/xl_video/template/index.htm');
}elseif(in_array($action,$action_ary)){
    include_once APP_PATH . 'plugin/xl_video/action/'.$action.'.php';
}else{
    echo '403';
    exit;
}

