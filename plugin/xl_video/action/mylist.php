<?php
if(!in_array($user['gid'],[1,106])){
    echo '403';
    exit;
}
$myaction = param(2, 'mylist');
if ($myaction == 'mylist') {
    $page = param(4, 1);
    $pagesize = 10;
    $videolist = db_find("video", ["uid" => $user['uid']], ["create_time" => '-1'], $page, $pagesize);
    $c = db_count("video",["uid" => $user['uid']]);
    $pagination = pagination(url("video-mylist-mylist-{page}"), $c, $page, $pagesize);

    $videolist = db_find("video", [],[],1,500);
    foreach($videolist as $v){
        db_update("video",["video_id"=>$v['video_id']],["video_id_num"=>getVideoNum()]);
    }
}
include _include(APP_PATH . 'plugin/xl_video/template/mylist.htm');
?>