<?php
if(!in_array($user['gid'],[1,106])){
    echo '403';
    exit;
}
$insert['uid'] = $user['uid'];
$insert['fileid'] = param('fileId');
$insert['video_title'] = param('title');
$insert['video_desc'] = param('desc');
$insert['video_cate'] = param('cate');
$insert['video_url'] = param('videourl');
$insert['tags'] = param('tags');
$insert['create_time'] = time();
$insert['video_id_num'] = getVideoNum();
$vinfo = db_find_one("video", ["fileid" => $insert['fileid']]);
if ($vinfo['video_id']) {
    unset($insert['fileid']);
    unset($insert['video_id_num']);
    db_update("video", ["video_id" => $vinfo['video_id']], $insert);
} else {
    if (param('vid')) {
        unset($insert['fileid']);
        unset($insert['video_id_num']);
        $video_id = param('vid');
        db_update("video", ["video_id" => $video_id], $insert);
        db_delete("video_tag", ["video_id" => $video_id]);
    } else {
        $video_id = db_insert("video", $insert);
    }
}

$tag = param('tags');
$tagsary = explode(",", $tag);
foreach ($tagsary as $v) {
    if ($v) {
        db_insert("video_tag", ["tagname" => $v, "video_id" => $video_id]);
    }
}
$data['code'] = 200;
echo json_encode($data);
exit;