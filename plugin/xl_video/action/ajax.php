<?php
$modeaction = param(2);
if ($modeaction == 'postReply') {
    $message = param('message', '', FALSE);
    $vid  = param('vid');
    empty($message) and message('message', lang('please_input_message'));
    empty($vid) and message('message', '找不到评论对象');
    xn_strlen($message) > 2028000 and message('message', lang('message_too_long'));
    $post = array(
        'vid' => $vid,
        'uid' => $uid,
        'author'=>$user['username'],
        'create_date' => $time,
        'userip' => $longip,
        'message' => $message,
    );
    $vpid = db_insert("video_reply",$post);
    $return_html = param('html',1);
    empty($vpid) and message(-1, lang('create_post_failed'));
    $rcount = db_count("video_reply",["vid"=>$vid]);
    db_update("video",["video_id"=>$vid],["reply_count"=>$rcount]);
    if ($return_html) {
        $vinfo  = db_find_one("video",["video_id"=>$vid]);
        $vpost = db_find_one("video_reply",["vpid"=>$vpid]);
        $vpost['user_avatar_url'] = user_read_cache($vpost['uid'])['avatar_url'];
        $vpost['username'] = $vpost['author'];
        $vpost['create_date_fmt'] =  date('Y-n-j', $vpost['create_date']);
        $postlist = array($vpost);
        $filelist = array();
        ob_start();
        include _include(APP_PATH . 'plugin/xl_video/template/reply_list.htm');
        $s = ob_get_clean();
        message(0, $s);
    } else {
        message(0, lang('create_post_sucessfully'));
    }
}elseif($modeaction=='postStatus'){
    if(!in_array($user['gid'],[1,106])){
        echo '403';
        exit;
    }
    $vid  = param('vid');
    $vinfo = db_find_one("video",["video_id"=>$vid]);
    if(!$vinfo['back_status']){
        message(0,'视频处理中');
    }else{
        db_update("video",["video_id"=>$vid],["status"=>1]);
        message(200,'ok');
    }
}elseif($modeaction=='postLike'){
    $vid  = param('vid');
    $video = db_find_one("video",["video_id_num"=>$vid]);
    $tid = $video['video_id'];
    $digg_log = db_find_one("digg", ["uid" => $uid, "type_value" => $tid, "type" => 1]);
    if (isset($digg_log) && $digg_log['id']) {
        db_delete("digg", ["uid" => $uid, "type_value" => $tid, "type" => 1]);
        $digg_status = 0;
    } else {
        db_insert("digg", ["uid" => $uid, "type_value" => $tid, "dateline" => time(), "type" => 1]);
        $digg_status = 1;
    }
    $thread_digg_count = db_count("digg", ["type_value" => $tid, "type" => 1]);
    db_update("video", ["video_id" => $tid], ["like_count" => $thread_digg_count]);
    $thread_digg_info['count'] = $thread_digg_count;
    if ($thread_digg_count > 0) {
        $user_list = db_find("digg", ["type_value" => $tid, "type" => 1],[], 1, 1000);
        foreach ($user_list as $v) {
            $user_list_value[] = $v['uid'];
        }
    }else{
        $user_list_value = [];
    }
    $thread_digg_info['user_list'] = $user_list_value;
    cache_set("video_digg_info_" . $tid, $thread_digg_info, 3600);
    message(0, ["count"=>$thread_digg_count,'status'=>$digg_status]);

}