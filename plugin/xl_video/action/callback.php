<?php

$input = file_get_contents("php://input");
if ($input && $data = json_decode($input, true)) {
    if ($data['EventType'] == 'ProcedureStateChanged') {
        $fid = $data['ProcedureStateChangeEvent']['FileId'];
        $video['back_json'] = json_encode($data['ProcedureStateChangeEvent']['MetaData']);
        $video['back_status'] = 1;
        $video['cover'] = getAry($data['ProcedureStateChangeEvent']['MediaProcessResultSet'], 'CoverBySnapshot')['CoverBySnapshotTask']['Output']['CoverUrl'];
        $video['gif_cover'] = getAry($data['ProcedureStateChangeEvent']['MediaProcessResultSet'], 'AnimatedGraphics')['AnimatedGraphicTask']['Output']['Url'];
        $video['video_time'] = number_format($data['ProcedureStateChangeEvent']['MetaData']['VideoDuration'], 2);
        $vinfo = db_find_one("video", ["fileid" => $fid]);
        if($vinfo['video_id']) {
            db_update("video", ["fileid" => $fid], $video);
        }else{
            $video['fileid'] = $fid;
            db_insert("video", $video);
        }
    }
}

function getAry($ary, $k)
{
    foreach ($ary as $kd => $v) {
        if ($v['Type'] == $k) {
            return $ary[$kd];
        }
    }
}