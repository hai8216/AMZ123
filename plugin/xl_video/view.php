<?php
include _include(APP_PATH . 'plugin/xl_video/function.php');
$vid = param(1);
if (!$vid) {
    message(0, jump('视频不存在', url('video')));
} else {
    $vinfo = db_find_one("video", ["video_id_num" => $vid]);
    $vid = $vinfo['video_id'];
    if (!$vinfo['video_id']) {
        message(0, jump('视频不存在', url('video')));
    }
    if ($vinfo['status'] != 1 && $vinfo['uid'] != $user['uid']) {
        message(0, jump('视频不存在', url('video')));
    }
    $userinfo = user_read_cache($vinfo['uid']);
//    print_r($userinfo);exit;
    $tags = db_find("video_tag", ["video_id" => $vinfo['video_id']]);
    $postlist = db_find("video_reply", ["vid" => $vid]);
    foreach ($postlist as &$vpost) {
        $vpost['user_avatar_url'] = user_read_cache($vpost['uid'])['avatar_url'];
        $vpost['username'] = $vpost['author'];
        $vpost['create_date_fmt'] = date('Y-n-j', $vpost['create_date']);
    }
    //相关视频，TAG匹配+热度
    foreach ($tags as $tv) {
        $tagnamelist[] = $tv['tagname'];
    }
    $aboutVideotag = db_find("video_tag", ["tagname" => $tagnamelist]);
    foreach ($aboutVideotag as $tva) {
        $video_ids[] = $tva['video_id'];
    }
    $aboutVideo1 = db_find("video", ["video_id" => $video_ids,"status"=>1,"back_status"=>1], ["player_count" => '-1'], 1, 15);
    $acount = 15 - count($aboutVideo1);
    if ($acount > 0) {
        $notin = implode(",",$video_ids);
        $aboutVideo2 = db_sql_find("SELECT * FROM bbs_video where video_id not in ('$notin') and status=1 and back_status=1 ORDER BY RAND() LIMIT {$acount}");
        $aboutVideoList = array_merge($aboutVideo1,$aboutVideo2);
    }else{
        $aboutVideoList = $aboutVideo1;
    }
    $isLike = db_find_one("digg",["type_value"=>$vid,'uid'=>$user['uid'],'type'=>1]);
    include _include(APP_PATH . 'plugin/xl_video/template/view.htm');
}