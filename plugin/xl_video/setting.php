<?php

!defined('DEBUG') and exit('Access Denied.');
$action = param(3) ?: "list";
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("video", array(), array(), $page, $pagesize);
    $c = db_count("video");
    $keywords = param('keywords');
    $pagination = pagination(url("plugin-setting-xl_video-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_video/admin/list.html');
} elseif ($action == 'cate') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("video_cate", array('fupid'=>0), array(), $page, $pagesize);
    $c = db_count("video_cate");
    $pagination = pagination(url("plugin-setting-xl_video-cate-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_video/admin/cate.html');
}elseif ($action == 'tag') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("video_taglist",[], array(), $page, $pagesize);
    $c = db_count("video_taglist");
    $pagination = pagination(url("plugin-setting-xl_video-cate-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_video/admin/tag.html');
}elseif($action=='adcate'){
    if ($method == 'GET') {
        if (param('cate_id')) {
            $cid = param('cate_id');
            $val = db_find_one("video_cate", ["cate_id" => $cid]);
        }
        $select = db_find("video_cate",["fupid"=>0]);
        $selectary[0]="一级分类";
        foreach ($select as $v){
            $selectary[$v['cate_id']]=$v['name'];
        }
        unset($selectary[$val['cate_id']]);
        $input['name'] = form_text("name", $val['name']);
        $input['fupid'] = form_select("fupid", $selectary,$val['fupid']);
        include _include(APP_PATH . 'plugin/xl_video/admin/adcate.html');
    } else {
        $input['name'] = param("name");
        $input['fupid'] = param("fupid");
        if (param('cateid')) {
            db_update("video_cate", ["cate_id" => param('cateid')], $input);
        } else {
            db_insert("video_cate", $input);
        }
        message(0, 'ok');
    }
}elseif($action=='adtag'){
    if ($method == 'GET') {
        if (param('id')) {
            $id = param('id');
            $val = db_find_one("video_taglist", ["id" => $id]);
        }
        $input['name'] = form_text("name", $val['name']);
        include _include(APP_PATH . 'plugin/xl_video/admin/adtag.html');
    } else {
        $input['name'] = param("name");
        if (param('id')) {
            db_update("video_taglist", ["id" => param('id')], $input);
        } else {
            db_insert("video_taglist", $input);
        }
        message(0, 'ok');
    }
}elseif($action=='banner'){
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("video_banner", array(), array(), $page, $pagesize);
    $c = db_count("video_banner");
    $pagination = pagination(url("plugin-setting-xl_video-banner-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_video/admin/banner.html');
}elseif($action=='adbanner'){
    if ($method == 'GET') {
        if (param('bid')) {
            $bid = param('bid');
            $val = db_find_one("video_banner", ["bid" => $bid]);
        }
        $input['links'] = form_text("links", $val['links']);
        $input['orderid'] = form_text("orderid", $val['orderid']);
        include _include(APP_PATH . 'plugin/xl_video/admin/adbanner.html');
    } else {
        $input['links'] = param("links");
        $input['orderid'] = param("orderid");
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/video-banner/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['thumb'] = $path . $name;
            }
        }
        if (param('bid')) {
            db_update("video_banner", ["bid" => param('bid')], $input);
        } else {
            db_insert("video_banner", $input);
        }
        message(0, 'ok');
    }
}elseif($action=='delbanner'){
    db_delete("video_banner", ["bid" => param('bid')]);
    message(0, 'ok');
}elseif($action=='delcate'){
    db_delete("video_cate", ["cate_id" => param('cate_id')]);
    message(0, 'ok');
}elseif($action=='bannercheck'){
    if(cache_get('video_banner')=='1'){
        cache_set('video_banner',0);
    }else{
        cache_set('video_banner',1);
    }
    message(0, 'ok');
}