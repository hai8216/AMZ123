<?php

function randomNums($length = 10, $type = 5)
{
    // 取字符集数组
    $number = range(0, 9);
    $lowerLetter = range('a', 'z');
    $upperLetter = range('A', 'Z');
    // 根据type合并字符集
    if ($type == 1) {
        $charset = $number;
    } elseif ($type == 2) {
        $charset = $lowerLetter;
    } elseif ($type == 3) {
        $charset = $upperLetter;
    } elseif ($type == 4) {
        $charset = array_merge($number, $lowerLetter);
    } elseif ($type == 5) {
        $charset = array_merge($number, $upperLetter);
    } elseif ($type == 6) {
        $charset = array_merge($lowerLetter, $upperLetter);
    } elseif ($type == 7) {
        $charset = array_merge($number, $lowerLetter, $upperLetter);
    } else {
        $charset = $number;
    }
    $str = '';
    // 生成字符串
    for ($i = 0; $i < $length; $i++) {
        $str .= $charset[mt_rand(0, count($charset) - 1)];
        // 验证规则
        if ($type == 4 && strlen($str) >= 2) {
            if (!preg_match('/\d+/', $str) || !preg_match('/[a-z]+/', $str)) {
                $str = substr($str, 0, -1);
                $i = $i - 1;
            }
        }
        if ($type == 5 && strlen($str) >= 2) {
            if (!preg_match('/\d+/', $str) || !preg_match('/[A-Z]+/', $str)) {
                $str = substr($str, 0, -1);
                $i = $i - 1;
            }
        }
        if ($type == 6 && strlen($str) >= 2) {
            if (!preg_match('/[a-z]+/', $str) || !preg_match('/[A-Z]+/', $str)) {
                $str = substr($str, 0, -1);
                $i = $i - 1;
            }
        }
        if ($type == 7 && strlen($str) >= 3) {
            if (!preg_match('/\d+/', $str) || !preg_match('/[a-z]+/', $str) || !preg_match('/[A-Z]+/', $str)) {
                $str = substr($str, 0, -2);
                $i = $i - 2;
            }
        }
    }
    return $str;
}
function getVideoNum(){
    $randNum = randomNums();
    $isData = db_find_one("video",["video_id_num"=>$randNum]);
    if($isData['video_id']){
        getVideoNum();
    }else{
        return $randNum;
    }
}
function Sec2Time($allSec){
    // 总秒数
    $remainSec = (int)$allSec;
    // 可读时间
    $humanTime = '';

    // 最大的时间长度是天
    if($remainSec > 86400) {
        $days = (int)($remainSec / 86400);
        $remainSec = $remainSec % 86400;
        $humanTime .= $days.'天';
    }
    // 判断小时
    if($remainSec > 3600) {
        $hours = (int)($remainSec / 3600);
        $remainSec = $remainSec % 3600;
        $humanTime .= $hours.'时';
    }
    // 判断分钟
    if($remainSec > 60) {
        $minutes = (int)($remainSec / 60);
        $remainSec = $remainSec % 60;
        $humanTime .= $minutes.'分';
    }
    // 判断秒钟
    if($remainSec > 0) {
        $seconds = $remainSec;
        $humanTime .= $seconds.'秒';
    }
    return $humanTime ?? '[瞬间]';
}
?>