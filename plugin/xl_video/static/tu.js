var file = $('#file'), uploadbox = $('#uploader')
file.on('change', function () {
    uploadbox.find('.queueList').hide()
    uploadbox.find('.statusBar').css('display', 'flex')
    curFile = this.files[0];
    const getSignature = function () {
        let url = base_url
        return $.get(url, function (res) {
            return res;
        })
    };
    const tcVod = new TcVod.default({
        getSignature: getSignature
    })
    const uploader = tcVod.upload({
        mediaFile: curFile,
    })
    uploader.on('media_progress', function (info) {
        $('.uploadinfo .text').html((info.percent * 100).toFixed() + '%')
        $('.progress .percentage').css('width', info.percent * 100 + '%')
    })
    uploader.on('media_upload', function (info) {
        $('.uploadinfo .title').html('完成<svg t="1620734276108" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="6130" width="16" height="16"><path d="M512 512m-448 0a448 448 0 1 0 896 0 448 448 0 1 0-896 0Z" fill="#4CAF50" p-id="6131"></path><path d="M738.133333 311.466667L448 601.6l-119.466667-119.466667-59.733333 59.733334 179.2 179.2 349.866667-349.866667z" fill="#CCFF90" p-id="6132"></path></svg>')
        $('.uploadinfo').addClass('green')
        $('.postVideobtn').attr('disabled', false)
    })
    uploader.done().then(function (doneResult) {
        console.log(doneResult);
        $('#fileid').val(doneResult.fileId)
        $('#videourl').val(doneResult.video.url)
    }).catch(function (err) {

    })

})

$('.postVideobtn').click(function () {
    var fileId = $('#fileid').val(),
        tags = $('#tags').val(),
        title = $('#title').val(),
        cate = $('#cate').val(),
        desc = $('#desc').val(),
        vid = $('#videoid').val(),
        videourl = $('#videourl').val()
    if(!fileId || fileId==0){
        $.alert('请上传视频或视频完成后再发布')
        return false;
    }
    if(!title || title=='0'){
        $.alert('请输入标题')
        return false;
    }
    let postdata = {
        fileId:fileId,
        tags:tags,
        title:title,
        cate:cate,
        desc:desc,
        vid:vid,
        videourl:videourl
    }
    $('.postVideobtn').attr('disabled', true).html('发布中,请稍后...')
    $.post(xn.url('video-doUpload'),postdata,function(res){
        if(res.code==200){
            window.location.href=xn.url('video-mylist');
        }
    },'json')
})