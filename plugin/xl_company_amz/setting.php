<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$action = param('3') ?: 'list';
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 45;
    $start = ($page - 1) * $pagesize;
    $map = [];
    if(param('k')){
        $map['companyname'] = array('LIKE'=>trim(xn_urldecode(param('k'))));
    }
    $arrlist = db_find("company", $map, array(), $page, $pagesize);
    $c = db_count("company",$map);
    if(param('k')){
        $pagination = pagination(url("plugin-setting-xl_company_amz-list-{page}",array('k'=>param('k'))), $c, $page, $pagesize);
    }else{
        $pagination = pagination(url("plugin-setting-xl_company_amz-list-{page}"), $c, $page, $pagesize);
    }

    include _include(APP_PATH . 'plugin/xl_company_amz/view/list.html');
} elseif ($action == 'del') {
    $id = param('id');
    db_delete("company", array('id' => $id));
    message(0, 'success');

} elseif ($action == 'ex') {
    $map = [];
    if(param('k')){
        $map['companyname'] = array('LIKE'=>trim(xn_urldecode(param('k'))));
    }
    $arrlist = db_find("company", $map, array(), 1, 100000);
    require_once APP_PATH . '/plugin/xl_search_baidu/Classes/PHPExcel.php';
    $phpexcel = new PHPExcel();
    $phpexcel->getActiveSheet()->setCellValue('A1', '公司名');
    foreach ($arrlist as $k => $v) {
        $k = $k + 2;
        $phpexcel->getActiveSheet()->setCellValue('A' . $k, $v['companyname']);
    }

    $obj_Writer = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
    $filename = "导出公司.xls";//文件名
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/download");
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header("Content-Transfer-Encoding: binary");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
    $obj_Writer->save('php://output');//输出
    die();//种植执行

} elseif ($action == 'ad') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("company", array('id' => param('id')));
            $gsimg = json_decode($data['save_imglist']);
        }
        $ids = param('id');
        $arrlist = db_find("company", array(), array(), 1, 200);
        $input['companyname'] = form_text('companyname', $data['companyname']);
        $data['desc'] = preg_replace('/<br\\s*?\/??>/i', '', $data['desc']);
        $input['desc'] = form_textarea('desc', $data['desc'], '', '500');
        include _include(APP_PATH . 'plugin/xl_company_amz/view/ad.html');
    } else {
        $insert['companyname'] = param('companyname');
        $insert['desc'] = param('desc');
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('hidden_img_tpl'))))) {
                $insert['save_logo'] = $wwwpath . $new_file;
            }
        } else {
            $insert['save_logo'] = '/upload/company/2020-11-20/5fb7997948320.png';
        }
        //图片1
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl_1'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('hidden_img_tpl_1'))))) {
                $imglist[0] = $wwwpath . $new_file;
            }
        }

        //图片2
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl_2'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('hidden_img_tpl_2'))))) {
                $imglist[1] = $wwwpath . $new_file;
            }
        }
        //图片3
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl_3'), $result)) {
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('hidden_img_tpl_3'))))) {
                $imglist[2] = $wwwpath . $new_file;
            }
        }


        //图片4
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl_4'), $result)) {
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('hidden_img_tpl_4'))))) {
                $imglist[3] = $wwwpath . $new_file;
            }
        }


        //图片5
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl_5'), $result)) {
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('hidden_img_tpl_5'))))) {
                $imglist[4] = $wwwpath . $new_file;
            }
        }

        if (param('ids')) {
            $dta = db_find_one("company", array('id' => param('ids')));
            $imglist_tmp = json_decode($dta['save_imglist'], true);
            if ($imglist[0]) {
                $imglist_tmp[0] = $imglist[0];
            }
            if ($imglist[1]) {
                $imglist_tmp[1] = $imglist[1];
            }
            if ($imglist[2]) {
                $imglist_tmp[2] = $imglist[2];
            }
            if ($imglist[3]) {
                $imglist_tmp[3] = $imglist[3];
            }
            if ($imglist[4]) {
                $imglist_tmp[4] = $imglist[4];
            }
            $insert['save_imglist'] = json_encode($imglist_tmp);
            db_update("company", array('id' => param('ids')), $insert);
        } else {
            $insert['viewid'] = setRandViewid();
            $insert['save_imglist'] = json_encode($imglist);
            db_insert("company", $insert);
        }
        message(0, jump('操作成功', url('plugin-setting-xl_company_amz')));
    }
} elseif ($action == 'del') {
    $data = db_find_one("category_page", array('id' => param('id')));
    db_delete("category_page", array('id' => param('id')));
    message(0, jump('操作成功', url('plugin-setting-xl_company_amz-view', array('cid' => $data['cate']))));
}

function setRandViewid()
{
    $viewsid = rand(1000000, 9999999);
    $view = db_find_one("company", array('viewid' => $viewsid));
    if ($view['viewid']) {
        setRandViewid();
    } else {
        return $viewsid;
    }
}
?>