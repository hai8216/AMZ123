<?php
if (param(1)) {
    if (param(1) == 'search') {
        $m['companyname'] = array('LIKE' => trim(xn_urldecode(param('keywords'))));
        $listHot = db_find("company", $m, array(), 1, 10);
        include _include(APP_PATH . 'plugin/xl_company_amz/tpl/search.html');
    } else {
        $ids = param(1);
        $viewdata = db_find_one("company", array('viewid' => $ids));
        if (!$viewdata['viewid']) {
            http_location(url('company'));
        }
        $imglist = json_decode($viewdata['save_imglist']);
        $header['title'] = $viewdata['companyname'] . '工资福利待遇怎么样？-AMZ123跨境导航';
        $header['keywords'] = $viewdata['companyname'] . '公司介绍,' . $viewdata['companyname'] . '薪资待遇、' . $viewdata['companyname'] . '.办公环境';
        $header['description'] = $viewdata['companyname'] . '招聘面试怎么样？了解更多跨境电商公司福利待遇，最新招聘，企业介绍和工作环境，就上AMZ123跨境导航';
        include _include(APP_PATH . 'plugin/xl_company_amz/tpl/view.html');
    }
} else {
//function saveImgicons($url, $type = '')
//{
//    if ($url == 'http://staticcss4.jobui.com/template_1/images/rank/blankLogo.png') {
//        return "/upload/company/2020-11-20/5fb7997948320.png";
//    } else {
//        if($type=='big'){
//            $url = $url."!b";
//        }
//        $path = "./upload/company/" . date('Y-m-d');
//        if (!file_exists($path)) mkdir($path, 0755, true);
//        $extname = substr($url, strrpos($url, '.'));
//        if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
//            $ext = $extname;
//        } else {
//            $ext = '.jpg';
//        }
//        $name = $path . "/" . uniqid() . $ext;
//        $r = null;
//        if (function_exists("curl_init") && function_exists('curl_exec')) {
//            $ch = curl_init($url);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//            if (ini_get("safe_mode") == false && ini_get("open_basedir") == false) {
//                curl_setopt($ch, CURLOPT_MAXREDIRS, 1);
//                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//            }
//            if (extension_loaded('zlib')) {
//                curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
//            }
//            curl_setopt($ch, CURLOPT_TIMEOUT, 300);
//            $r = curl_exec($ch);
//            curl_close($ch);
//        } elseif (ini_get("allow_url_fopen")) {
//            if (function_exists('ini_set')) ini_set('default_socket_timeout', 300);
//            $r = file_get_contents((extension_loaded('zlib') ? 'compress.zlib://' : '') . $url);
//        }
//
//        $fp = @fopen($name, "w");
//
//        fwrite($fp, $r);
//
//        fclose($fp);
//
//        return $name;
//    }
//}
    $Elist = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    $e = param('e', 'A');
    $map['es'] = 1;
    if($e){
        $map['E'] = $e;
    }else{
        $map['E'] = $Elist[0];
    }
    $page = param('page', 1);
    $pagesize = 200;
    $start = ($page - 1) * $pagesize;
    $listHot = db_find("company", $map, array(), $page, $pagesize);
    $c = db_count("company", $map);
//    include "plugin/xn_search/model/__pinyin.func.php";
//    foreach($listHot as &$v){
//        $E = strtoupper(substr(pinyin($v['companyname']),0,1));
//        db_update("company",["id"=>$v['id']],["E"=>$E,"es"=>1]);
//    }
//foreach($listHot as $v){
//    $logo = saveImgicons($v['logo'],'logo');
//    db_update("company",array('id'=>$v['id']),array('save_logo'=>$logo));
//}


    $pagination = pagination(url("company")."?page={page}&e=".$e, $c, $page, $pagesize);

    $header['title'] = '跨境电商公司库-AMZ123跨境导航';
    $header['keywords'] ="跨境公司库";
    $header['description'] = "AMZ123跨境导航为您提供最全的跨境电商相关公司大全，帮您更方便了解跨境电商公司。";
    include _include(APP_PATH . 'plugin/xl_company_amz/tpl/company.html');
}