	'ax_sign1'=>'积分',
	'ax_sign2'=>'金币',	
	'ax_sign3'=>'人民币',
	'ax_sign4'=>'请登录后再签到!',
	'ax_sign5'=>'今天已经签过啦！',
	'ax_sign6'=>'连续签到3天额外奖励',
	'ax_sign7'=>'连续签到7天额外奖励',
	'ax_sign8'=>'连续签到15天额外奖励',
	'ax_sign9'=>'连续签到超过15天额外奖励',
	'ax_sign10'=>'成功签到！今日排名{number}，{message}总奖励{Credit}！',
	'ax_sign11'=>'首次签到，今日排名{number}，额外奖励{Credit}！',
	'ax_sign12'=>'每日签到',
	'ax_sign13'=>'今日排名',
	'ax_sign14'=>'签到列表',
	'ax_sign15'=>'总奖励',
	'ax_sign16'=>'签到时间',
	'ax_sign17'=>'签到天数',
	'ax_sign18'=>'排名',
	'ax_sign19'=>'今日奖励',
	'ax_sign20'=>'连签天数',
	'ax_sign21'=>'天',
	'ax_sign22'=>'签到',
	'ax_sign23'=>'1、每日签到奖励 {sign1}<br>2、签到3天奖励 {sign2}<br>3、签到7天奖励 {sign3}<br>4、签到15天奖励 {sign4}<br>5、签到15天以上奖励 {sign7} 至 {sign8}<br>6、每日签到第一名奖励 {sign5}<br>7、每日签到第二至五名奖励 {sign6}<br>',
	'ax_sign24'=>'每日签到奖励',
	'ax_sign25'=>'签到人数',
	'ax_sign26'=>'今日签到',
	'ax_sign27'=>'今日第一',
	'ax_sign28'=>'人',
	'ax_sign29'=>'已签',
	'ax_sign30'=>'签到',
	'ax_sign31'=>'蓝色',
	'ax_sign32'=>'红色',
	'ax_sign33'=>'绿色',
	'ax_sign34'=>'橙色',
	'ax_sign35'=>'连续签到',
	'ax_sign36'=>'无人签到，快来抢第一名',