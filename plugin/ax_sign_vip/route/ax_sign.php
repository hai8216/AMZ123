<?php
/*
	Xiuno BBS 4.0 每日签到
	插件由查鸽信息网制作网址：http://cha.sgahz.net/
*/
!defined('DEBUG') AND exit('Access Denied.');
include _include(APP_PATH . 'plugin/ax_sign_vip/model/ax_sign.func.php');
$active = param(1);
$active == 'list' ? $active = 'list' : $active = 'default';
$kv = kv_get('ax_sign');
switch ($kv['sign9']) {
    case 'credits':
        $Unit = lang('ax_sign1');
        break;
    case 'golds':
        $Unit = lang('ax_sign2');
        break;
    case 'rmbs':
        $Unit = lang('ax_sign3');
        break;
}
$ax_sign_set = ax_sign_read('ax_sign_set', 'id', 1);
$userinfo = ax_sign_read('ax_sign', 'uid', $uid);
$today = strtotime('today');
$yesterday = strtotime('yesterday');
$t = $time - $ax_sign_set['time'];
if ($t > 86400) {
    ax_sign_update('ax_sign_set', 'id', 1, array('ax_sign_top' => '', 'ax_sign_one' => '', 'ax_sign' => 0, 'time' => $today));
    $ax_sign_set['ax_sign_top'] = '';
    $ax_sign_set['ax_sign_one'] = '';
    $ax_sign_set['ax_sign'] = 0;
}

if ($method == 'GET') {
    if ($active == 'list') {
        $kv['sign11'] != 1 AND message(0, lang('ax_sign14') . lang('close'));
        $pagesize = 10;
        $page = param(2, 1);
        $pagination = pagination(url("ax_sign-list-{page}"), $ax_sign_set['ax_signnum'], $page, $pagesize);
        $ax_signlist = ax_sign_find('ax_sign', 'uid', array(), array('stime' => -1), $page, $pagesize);
        $ax_signlist_first = ax_sign_find('ax_sign', 'uid', array('stime'=>array(">"=>strtotime(date('Y-m-d',time())))), array('stime' =>"1"), 1, 10);
        foreach($ax_signlist_first as $v){
            $sign_order[] = $v;
        }
        $ax_sign_first = $sign_order[0]['user'];
    } else {
        $uidarr = explode(',', $ax_sign_set['ax_sign_top']);
        $ax_signlist = ax_sign_find('ax_sign', 'uid', array('stime'=>array(">"=>strtotime(date('Y-m-d',time())))), array('stime' =>"1"), 1, 10);
        foreach($ax_signlist as $v){
            $sign_order[] = $v;
        }
        $ax_sign_first = $sign_order[0]['user'];
    }

    $ax_sign_count = db_count("ax_sign",array('stime'=>array(">"=>strtotime(date('Y-m-d',time())))));
    include _include(APP_PATH . 'plugin/ax_sign_vip/htm/ax_sign.htm');
} elseif ($method == 'POST') {
    empty($uid) AND message(0, lang('ax_sign4'));
    $kv = kv_get('ax_sign');
    $Credit = $kv['sign1'];
    $number = $ax_sign_set['ax_sign'];
    $number += 1;
    $username = $ax_sign_set['ax_sign_one'];
    $ax_sign_top = $ax_sign_set['ax_sign_top'];
    $ax_signnum = $ax_sign_set['ax_signnum'];
    $times = $ax_sign_set['time'];
    if ($number == 1) {
        $Credit += $kv['sign5'];
    } else if ($number >= 2 && $number <= 5) {
        $Credit += $kv['sign6'];
    }
    if ($userinfo) {
        if ($userinfo['stime'] > $today) {
            message(-1, lang('ax_sign5'));
        } else {
            if ($userinfo['stime'] > $yesterday && $userinfo['stime'] < $today) {
                if ($userinfo['keepdays'] == 2) {
                    $Credit += $kv['sign2'];
                    $message = lang('ax_sign6') . $kv['sign2'] . $Unit . '，';
                } else if ($userinfo['keepdays'] == 6) {
                    $Credit += $kv['sign3'];
                    $message = lang('ax_sign7') . $kv['sign3'] . $Unit . '，';
                } else if ($userinfo['keepdays'] == 14) {
                    $Credit += $kv['sign4'];
                    $message = lang('ax_sign8') . $kv['sign4'] . $Unit . '，';
                } else if ($userinfo['keepdays'] >= 14) {
                    $Credit += rand($kv['sign7'], $kv['sign8']);
                    $message = lang('ax_sign9') . rand($kv['sign7'], $kv['sign8']) . $Unit . '，';
                } else {
                    $message = '';
                }
                $keepdays = $userinfo['keepdays'] + 1;
            } else {
                $message = '';
                $keepdays = 1;
            }
            ax_sign_update('ax_sign', 'uid', $uid, array('id' => $number, 'stime' => time(), 'counts+' => 1, 'credits+' => $Credit, 'todaycredits' => $Credit, 'keepdays' => $keepdays));
            $message = lang('ax_sign10', array('number' => $number, 'message' => $message, 'Credit' => $Credit . $Unit));
        }
    } else {
        ax_sign_create('ax_sign', array('id' => $number, 'uid' => $uid, 'stime' => time(), 'counts' => 1, 'credits' => $Credit, 'todaycredits' => $Credit, 'keepdays' => 1, 'user' => $user['username']));
        $ax_signnum = $ax_sign_set['ax_signnum'] + 1;
        $message = lang('ax_sign10', array('number' => $number, 'message' => '', 'Credit' => $Credit . $Unit));
    }
    if ($number == 1) {
        $username = $user['username'];
        $ax_sign_top = $uid;
        $times = $today;
    } else if ($number >= 2 && $number <= 10) {
        $ax_sign_top = $ax_sign_set['ax_sign_top'] . ',' . $uid;
    }
    $setarr = array('ax_sign' => $ax_sign_set['ax_sign'] + 1, 'ax_signnum' => $ax_signnum, 'ax_sign_one' => $username, 'ax_sign_top' => $ax_sign_top, 'time' => $times);
    ax_sign_update('ax_sign_set', 'id', 1, $setarr);
    ax_sign_update('user', 'uid', $uid, array($kv['sign9'] . '+' => $Credit));
    message(0, $message);
}
?>