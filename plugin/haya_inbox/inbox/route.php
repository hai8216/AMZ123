<?php

/**
 * 站内信，基于修罗论坛
 *
 * 用于商业用途请获取授权
 * 
 * @create 2017-12-23
 * @author deatil
 * 
 * @license GPL2 
 */
 
defined('DEBUG') OR exit('Forbidden');

define('HAYA_ROOT', APP_PATH.'plugin/haya_inbox/inbox/');

// 动作
$action = param(1);
empty($action) and $action = 'index';

$actions = array(
	'index',
	'system',
	'delete',
	'msg',
	'create',
	'send',
);

$tab = $action;

// 引入核心
include _include(APP_PATH.'plugin/haya_inbox/system/plugin/init.php');

include _include(APP_PATH.'plugin/haya_inbox/system/plugin/run.php');

