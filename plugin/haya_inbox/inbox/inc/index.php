<?php

!defined('DEBUG') and exit('Access Denied.');

$header['title'] = '私信 - 站内信';

$pagesize = isset($haya_inbox_config['msg_list_pagesize']) ? intval($haya_inbox_config['msg_list_pagesize']) : 10;
$page     = param(2, 1);

$haya_inbox_where = array(
	'sender_id' => $uid,
	'is_deleted' => 0,
);
$n = haya_inbox_log__count($haya_inbox_where);
$haya_inbox_msg_list = haya_inbox_log_find(
	$haya_inbox_where,
	array('last_time' => -1),
	$page, 
	$pagesize
);
if (!empty($haya_inbox_msg_list)) {
	foreach ($haya_inbox_msg_list as & $haya_inbox_msg) {
		$haya_inbox_msg['msg'] = haya_inbox_msg__read($haya_inbox_msg['last_mid']);
	}	
}
$pagination = pagination(url("inbox-index-{page}"), $n, $page, $pagesize);

include _include(HAYA_HTML.'index.htm');
