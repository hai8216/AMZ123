<?php

!defined('DEBUG') and exit('Access Denied.');

$haya_msg = param(2);
if ($haya_msg == 'read') {
	
	$header['title'] = '查看系统信息 - 站内信';
	
	$id = param(3);
	if (empty($id)) {
		message(0, jump('私信ID不能为空', 'back'));
	}
	
	$haya_inbox_msg = haya_inbox_msg__read($id);
	if (empty($haya_inbox_msg)) {
		message(0, jump('系统信息不存在', 'back'));
	}	
	
	if ($haya_inbox_msg['type'] != 2) {
		message(0, jump('系统信息不存在', 'back'));
	}
	
	if ($haya_inbox_msg['end_time'] < time()) {
		message(0, jump('系统信息已过期', 'back'));
	}
	
	include _include(HAYA_HTML.'system-msg.htm');
} else {
	$header['title'] = '系统 - 站内信';

	$pagesize = isset($haya_inbox_config['msg_list_pagesize']) ? intval($haya_inbox_config['msg_list_pagesize']) : 10;
	$page     = param(2, 1);

	$haya_inbox_where = array(
		'type' => 2,
		'end_time' => array('>=' => time()),
	);
	$n = haya_inbox_msg__count($haya_inbox_where);
	$haya_inbox_msg_list = haya_inbox_msg_find(
		$haya_inbox_where,
		array('create_time' => -1),
		$page, 
		$pagesize
	);
	$pagination = pagination(url("inbox-system-{page}"), $n, $page, $pagesize);

	include _include(HAYA_HTML.'system.htm');
}

