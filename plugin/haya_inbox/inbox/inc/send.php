<?php

!defined('DEBUG') and exit('Access Denied.');

$header['title'] = '发送私信 - 站内信';

if ($method == 'GET') {
	include _include(HAYA_HTML.'send.htm');
} else {

	haya_inbox_functions_check_my_send_msg($haya_inbox_config, $group, $grouplist);

	$receiver_id = param('id', '');
	if (empty($receiver_id)) {
		message(1, jump('用户不能为空', 'back'));
	}

	$content = param('content', '');
	if (empty($content)) {
		message(1, jump('发送信息不能为空', 'back'));
	}

	$receiver = user_read($receiver_id);
	if (empty($receiver)) {
		message(1, jump('用户不存在，请确认后重试', 'back'));
	}

	haya_inbox_functions_check_user_send_msg($haya_inbox_config, $receiver['gid']);

	$data = array(
		'sender_uid' => $user['uid'],
		'receiver_uid' => $receiver['uid'],
		'type' => 1,
		'content' => $content,
		'attaches' => '',
		'create_time' => time(),
		'create_ip' => $longip,
	);
	$msg_id = haya_inbox_msg__create($data);
	if ($msg_id == false) {
		message(1, jump('发送私信失败1', 'back'));
	}

	$sender_data = array(
		'mid' => $msg_id,
		'uid' => $user['uid'],
		'friend' => $receiver['uid'],
		'send_type' => 1,
		'is_send' => 1,
		'is_deleted' => 0,
		'status' => 1,
		'create_time' => time(),
		'create_ip' => $longip,
	);
	$sender_status = haya_inbox_send__create($sender_data);
	if ($sender_status == false) {
		message(1, jump('发送私信失败2', 'back'));
	}

	$receiver_data = array(
		'mid' => $msg_id,
		'uid' => $receiver['uid'],
		'friend' => $user['uid'],
		'send_type' => 2,
		'is_send' => 1,
		'is_deleted' => 0,
		'status' => 1,
		'create_time' => time(),
		'create_ip' => $longip,
	);
	$receiver_status = haya_inbox_send__create($receiver_data);	
	if ($receiver_status == false) {
		message(1, jump('发送私信失败3', 'back'));
	}

	$sender_log = haya_inbox_log_read_by_sender_id_and_receiver_id($user['uid'], $receiver['uid']);
	if (empty($sender_log)) {
		haya_inbox_log__create(array(
			'sender_id' => $user['uid'],
			'receiver_id' => $receiver['uid'],
			'last_type' => 1,
			'last_mid' => $msg_id,
			'last_time' => time(),
			'last_ip' => $longip,
			'comments' => 1,
			'unreads' => 0,
			'is_deleted' => 0,
			'create_time' => time(),
			'create_ip' => $longip,
		));
		haya_inbox_log__create(array(
			'sender_id' => $receiver['uid'],
			'receiver_id' => $user['uid'],
			'last_type' => 2,
			'last_mid' => $msg_id,
			'last_time' => time(),
			'last_ip' => $longip,
			'comments' => 1,
			'unreads' => 1,
			'is_deleted' => 0,
			'create_time' => time(),
			'create_ip' => $longip,
		));
	} else {
		haya_inbox_log_update_by_sender_id_and_receiver_id($user['uid'], $receiver['uid'], array(
			'last_type' => 1,
			'last_mid' => $msg_id,
			'last_time' => time(),
			'last_ip' => $longip,
			'comments+' => 1,
		));
		haya_inbox_log_update_by_sender_id_and_receiver_id($receiver['uid'], $user['uid'], array(
			'last_type' => 2,
			'last_mid' => $msg_id,
			'last_time' => time(),
			'last_ip' => $longip,
			'comments+' => 1,
			'unreads+' => 1,
		));
	}	

	user__update($receiver['uid'], array('inbox_unreads+' => 1));

	message(0, jump('发送私信成功', url('inbox-msg-'.$receiver_id)));	

}
