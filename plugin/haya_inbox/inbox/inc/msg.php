<?php

!defined('DEBUG') and exit('Access Denied.');

$header['title'] = '查看私信 - 站内信';

haya_inbox_functions_check_my_send_msg($haya_inbox_config, $group, $grouplist);

$receiver_id = param(2);
if (empty($receiver_id)) {
	message(1, jump('访问错误', url('inbox-index')));
}

$receiver_user = user_read($receiver_id);
if (empty($receiver_user)) {
	message(1, jump('用户不存在', url('inbox-index')));
}

$pagesize = isset($haya_inbox_config['msg_talk_list_pagesize']) ? intval($haya_inbox_config['msg_talk_list_pagesize']) : 10;
$page     = param(3, 1);

$haya_inbox_msg_where = array(
	'uid' => $uid,
	'friend' => $receiver_id,
	'is_send' => 1,
	'is_deleted' => 0,
	'status' => 1,
);
$n = haya_inbox_send__count($haya_inbox_msg_where);
$haya_inbox_msg_list = haya_inbox_send_find(
	$haya_inbox_msg_where,
	array('create_time' => 1),
	$page, 
	$pagesize
);
$pagination = pagination(url("inbox-msg-$receiver_id-{page}"), $n, $page, $pagesize);

$haya_inbox_log_count = haya_inbox_log__find(array('sender_id' => $uid, 'receiver_id' => $receiver_id));
if ($haya_inbox_log_count[0]['unreads'] > $user['inbox_unreads']) {
	$haya_inbox_log_count[0]['unreads'] = $user['inbox_unreads'];
}
user__update($user['uid'], array('inbox_unreads-' => $haya_inbox_log_count[0]['unreads']));

if (!empty($haya_inbox_msg_list)) {
	foreach ($haya_inbox_msg_list as & $haya_inbox_msg) {
		$haya_inbox_send = haya_inbox_send__read($haya_inbox_msg['sid']);
		if ($haya_inbox_send['read_time'] <= 0) {
			haya_inbox_send__update($haya_inbox_msg['sid'], array(
				'read_time' => time(),
				'read_ip' => $longip,
			));
		}
		
		$haya_inbox_msg['msg'] = haya_inbox_msg__read($haya_inbox_msg['mid']);
	}
}
	
$haya_inbox_log = haya_inbox_log_read_by_sender_id_and_receiver_id($uid, $receiver_id);
if ($haya_inbox_log['unreads'] > 0) {
	haya_inbox_log_update_by_sender_id_and_receiver_id($uid, $receiver_id, array(
		'unreads' => 0
	));
}


include _include(HAYA_HTML.'msg.htm');
