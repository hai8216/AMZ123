<?php

!defined('DEBUG') and exit('Access Denied.');

$header['title'] = '删除私信 - 站内信';

if ($method != 'POST') {
	message(1, jump('删除私信失败', 'back'));
}

haya_inbox_functions_check_my_send_msg($haya_inbox_config, $group, $grouplist);

$type = param(2, 'msg');

$id = param(3);
if (empty($id)) {
	message(1, jump('私信ID不能为空', 'back'));
}

$type = strtolower($type);
if ($type == 'user') {
	if (isset($haya_inbox_config['delete_msg_type']) 
		&& $haya_inbox_config['delete_msg_type'] == 2
	) {
		haya_inbox_send_delete_by_uid_and_friend($uid, $id);
		haya_inbox_log_delete_by_sender_id_and_receiver_id($uid, $id);
	
		$haya_inbox_sender_log = haya_inbox_log_read_by_sender_id_and_receiver_id($id, $uid);
		if (empty($haya_inbox_sender_log)) {
			haya_inbox_msg_delete_by_sender_uid_and_receiver_uid($uid, $id);
			haya_inbox_msg_delete_by_sender_uid_and_receiver_uid($id, $uid);
		}
	} else {
		haya_inbox_send_update_by_uid_and_friend($uid, $id, array('is_deleted' => 1));
		haya_inbox_log_update_by_sender_id_and_receiver_id($uid, $id, array('is_deleted' => 1, 'comments' => 0, 'unreads' => 0));
	}
} elseif ($type == 'cancel') {
	$haya_inbox_msg = haya_inbox_msg__read($id);
	if (empty($haya_inbox_msg)) {
		message(1, jump('私信不存在', 'back'));
	}
	
	$haya_inbox_send = haya_inbox_send_read_by_mid_and_uid($id, $uid);
	if (empty($haya_inbox_send)) {
		message(1, jump('你没有发送过该条私信', 'back'));
	}
	
	if ($haya_inbox_send['send_type'] != 1) {
		message(1, jump('你不能撤销该条私信', 'back'));
	}
	
	if ($haya_inbox_send['create_time'] + $haya_inbox_config['cancel_time'] < time()) {
		message(1, jump('超过时间限制，你不能再撤销该条私信', 'back'));
	}
	
	if (isset($haya_inbox_config['delete_msg_type']) 
		&& $haya_inbox_config['delete_msg_type'] == 2
	) {
		$status = haya_inbox_send_delete_by_sid_and_uid($id, $haya_inbox_msg['receiver_uid']);
	} else {
		$status = haya_inbox_send_update_by_mid_and_uid($id, $haya_inbox_msg['receiver_uid'], array('is_send' => 0, 'type' => 1));
	}
	
	if ($status === false) {
		message(1, jump('撤销私信失败', 'back'));
	}
	haya_inbox_send_update_by_mid_and_uid($id, $uid, array('type' => 1));

	haya_inbox_log_update_by_sender_id_and_receiver_id($id, $uid, array('comments-' => 1));
} else {
	$haya_inbox_msg = haya_inbox_msg__read($id);
	if (empty($haya_inbox_msg)) {
		message(1, jump('私信不存在', 'back'));
	}
	
	$haya_inbox_send = haya_inbox_send_read_by_mid_and_uid($id, $uid);
	if (empty($haya_inbox_send)) {
		message(1, jump('你没有发送过该条私信', 'back'));
	}
	
	if (isset($haya_inbox_config['delete_msg_type']) 
		&& $haya_inbox_config['delete_msg_type'] == 2
	) {
		$status = haya_inbox_send_delete_by_mid_and_uid($id, $uid);
	
		if ($status !== false) {
			$haya_inbox_friend_send = haya_inbox_send_find(array('mid' => $id));
			if (empty($haya_inbox_friend_send)) {
				haya_inbox_msg_delete_by_mid($id);
			}	
		}
	} else {
		$status = haya_inbox_send_update_by_mid_and_uid($id, $uid, array('is_deleted' => 1));
	}
	
	if ($status === false) {
		message(1, jump('删除私信失败', 'back'));
	}
	
	haya_inbox_log_update_by_sender_id_and_receiver_id($uid, $id, array('comments-' => 1));
}

message(0, jump('删除私信成功', 'back'));
