<?php

defined('DEBUG') OR exit('Forbidden');

$tablepre = $db->tablepre;

$sql = "
CREATE TABLE IF NOT EXISTS {$tablepre}inbox_msg (
	`mid` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`sender_uid` int(11) NOT NULL DEFAULT '0' COMMENT '私信发送者',
	`receiver_uid` int(11) NOT NULL DEFAULT '0' COMMENT '私信接收者',
	`type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '私信类型（1普通消息、2系统消息）',
	`content` text NOT NULL DEFAULT '' COMMENT '私信内容',
	`attaches` longtext NULL DEFAULT '' COMMENT '附件',
	`end_time` int(10) NULL DEFAULT '0' COMMENT '系统消息结束时间',
	`create_time` int(10) NULL DEFAULT '0' COMMENT '添加时间',
	`create_ip` varchar(25) NULL DEFAULT '' COMMENT '添加IP',
	PRIMARY KEY (`mid`),
	KEY `type` (`type`),
	KEY `sender_uid_receiver_uid` (`sender_uid`, `receiver_uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
db_exec($sql);

$sql = "
CREATE TABLE IF NOT EXISTS {$tablepre}inbox_send (
	`sid` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`mid` int(11) NOT NULL DEFAULT '0' COMMENT '私信ID',
	`uid` int(11) NOT NULL DEFAULT '0' COMMENT '信息所属用户',
	`friend` int(11) NOT NULL DEFAULT '0' COMMENT '信息对应用户',
	`send_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '发送类型（1发送、2接收）',
	`read_time` int(10) NULL DEFAULT '0' COMMENT '阅读时间',
	`read_ip` varchar(25) NULL DEFAULT '' COMMENT '阅读IP',
	`is_send` tinyint(1) NULL DEFAULT '1' COMMENT '是否已发送，0-撤回',
	`is_deleted` tinyint(1) NULL DEFAULT '0' COMMENT '1-已删除',
	`type` tinyint(1) NULL DEFAULT '0' COMMENT '1-已撤回',
	`status` tinyint(1) NULL DEFAULT '1' COMMENT '是否开启, 0-已删除',
	`create_time` int(10) NULL DEFAULT '0' COMMENT '添加时间',
	`create_ip` varchar(25) NULL DEFAULT '' COMMENT '添加IP',
	PRIMARY KEY (`sid`),
	KEY `sid` (`sid`),
	KEY `is_send` (`is_send`),
	KEY `is_deleted` (`is_deleted`),
	KEY `type` (`type`),
	KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
db_exec($sql);

$sql = "
CREATE TABLE IF NOT EXISTS {$tablepre}inbox_log (
	`lid` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`sender_id` int(11) NOT NULL DEFAULT '0' COMMENT '私信发送者',
	`receiver_id` int(11) NOT NULL DEFAULT '0' COMMENT '私信接收者',
	`last_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '最后站内信类型（1发送、2接收）',
	`last_mid` int(10) NULL DEFAULT '0' COMMENT '最后私信ID',
	`last_time` int(10) NULL DEFAULT '0' COMMENT '最后更新时间',
	`last_ip` varchar(25) NULL DEFAULT '' COMMENT '最后更新IP',
	`comments` int(11) NULL DEFAULT '1' COMMENT '私信数量',
	`unreads` int(11) NULL DEFAULT '0' COMMENT '未读数量',
	`is_deleted` tinyint(1) NULL DEFAULT '0' COMMENT '1-已删除',
	`create_time` int(10) NULL DEFAULT '0' COMMENT '添加时间',
	`create_ip` varchar(25) NULL DEFAULT '' COMMENT '添加IP',
	PRIMARY KEY (`lid`),
	KEY `lid` (`lid`),
	KEY `is_deleted` (`is_deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
db_exec($sql);

db_exec("ALTER TABLE {$tablepre}user ADD COLUMN inbox_unreads int(11) NULL DEFAULT '0' COMMENT '站内信未读';");
db_exec("ALTER TABLE {$tablepre}user ADD COLUMN inbox_system_unreads int(11) NULL DEFAULT '0' COMMENT '站内信系统信息未读';");

// 添加插件配置 
$haya_inbox_config = array(
	'is_admin_nav' => 1,
	'cancel_time' => 100,
	'tip' => '',

	'msg_list_pagesize' => 10,
	'msg_talk_list_pagesize' => 10,
	
	'delete_msg_type' => 1, // 1-软删除，2-完全删除
	
	'open_group' => 0,
	'user_group' => '',
	'user_no_group_tip' => '允许发送私信用户组【{msg_group}】，你当前所属用户组【{user_group}】不能发送私信！',
);
kv_set('haya_inbox', $haya_inbox_config); 

