<?php

return array(
	'setting' => array(
		'url' => url('inbox-setting'),
		'text' => '插件设置',
	),
	//'msg' => array(
	//	'url' => url('inbox-msg'),
	//	'text' => '站内信',
	//),
	'system' => array(
		'url' => url('inbox-system'),
		'text' => '系统信息',
	),
	'quanxian' => array(
		'url' => url('inbox-quanxian'),
		'text' => '权限设置',
	),
);

