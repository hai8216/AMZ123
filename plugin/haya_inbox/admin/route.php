<?php

/**
 * 站内信，基于修罗论坛
 *
 * 用于商业用途请获取授权
 * 
 * @create 2017-12-23
 * @author deatil
 * 
 * @license GPL2 
 */
defined('DEBUG') OR exit('Forbidden');

define('HAYA_ROOT', APP_PATH.'plugin/haya_inbox/admin/');

$haya_inbox_config = kv_get('haya_inbox');

// 动作
$action = param(1);
empty($action) and $action = 'setting';

$actions = array(
	'setting',
	'msg',
	'read',
	'talk',
	'delete',
	'system',
	'quanxian',
);

// 引入核心
include _include(APP_PATH.'plugin/haya_inbox/system/plugin/init.php');

// 定义导航
if (empty($hayaConf)) {
	$hayaConf = HAYA_CONF.'menu.php';
}
if (!file_exists($hayaConf)) {
	message(1, jump('插件配置文件不存在', url('inbox')));
}
$menuTab = include _include($hayaConf);

include _include(APP_PATH.'plugin/haya_inbox/system/plugin/run.php');

