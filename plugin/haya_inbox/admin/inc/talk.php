<?php

!defined('DEBUG') and exit('Access Denied.');

$header['title'] = '查看对话 - 站内信';

$haya_inbox_config = kv_get('haya_inbox');

$sender_id = param(2);
if (empty($sender_id)) {
	message(1, jump('发送方ID不能为空', 'back'));
}
$sender_user = user_read($sender_id);

$receiver_id = param(3);
if (empty($receiver_id)) {
	message(1, jump('接收方ID不能为空', 'back'));
}
$receiver_user = user_read($receiver_id);

$pagesize = 20;
$page     = param(4, 1);

$haya_inbox_msg_where = array(
	'uid' => $sender_id,
	'friend' => $receiver_id,
);
$n = haya_inbox_send__count($haya_inbox_msg_where);
$haya_inbox_msg_list = haya_inbox_send_find(
	$haya_inbox_msg_where,
	array('create_time' => 1),
	$page, 
	$pagesize
);
$pagination = pagination(url("inbox-talk-$sender_id-$receiver_id-{page}"), $n, $page, $pagesize);

if (!empty($haya_inbox_msg_list)) {
	foreach ($haya_inbox_msg_list as & $_haya_inbox_msg) {
		$_haya_inbox_msg['msg'] = haya_inbox_msg__read($_haya_inbox_msg['mid']);
	}
}
$action = 'msg';

include _include(HAYA_HTML.'talk.htm');
