<?php

!defined('DEBUG') and exit('Access Denied.');

$header['title'] = '删除私信 - 站内信';

if ($method != 'POST') {
	message(1, jump('删除私信失败', 'back'));
}

$id = param('id');
if (empty($id)) {
	message(1, jump('私信ID不能为空', 'back'));
}

$msg = haya_inbox_msg__read($id);
if (empty($msg)) {
	message(1, jump('私信不存在', 'back'));
}

$status = haya_inbox_msg_delete_by_mid($id);
if ($status === false) {
	message(1, jump('删除私信失败', 'back'));
}

haya_inbox_send_delete_by_mid($id);

message(0, jump('删除私信成功', 'back'));
