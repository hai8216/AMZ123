<?php

!defined('DEBUG') and exit('Access Denied.');

$header['title'] = '站内信设置';

if ($method == 'GET') {	
	$config = kv_get('haya_inbox');
	$user_group = isset($config['user_group']) ? explode(',', $config['user_group']) : array();
	
	include _include(HAYA_HTML.'setting.htm');
} else {
	
	$config = array();
	
	$config['is_admin_nav'] = param('is_admin_nav', '1');
	$config['cancel_time'] = param('cancel_time', '100');
	$config['tip'] = param('tip', '');
	
	$config['msg_list_pagesize'] = param('msg_list_pagesize', 10);
	$config['msg_talk_list_pagesize'] = param('msg_talk_list_pagesize', 10);

	$config['delete_msg_type'] = param('delete_msg_type', 1);
	
	$config['open_group'] = param('open_group', 0);
	$user_group = param('user_group', array());
	$config['user_group'] = implode(',', $user_group);
	$config['user_no_group_tip'] = param('user_no_group_tip', '');
	
	kv_set('haya_inbox', $config); 
	
	message(0, jump('配置修改成功', url('inbox-setting')));
}
