<?php

!defined('DEBUG') and exit('Access Denied.');

$header['title'] = '添加系统信息 - 站内信';

if ($method == 'POST') {
	$content = param('content', '');
	
	
	if (empty($content)) {
		message(1, jump('发送信息不能为空', 'back'));
	}
	
	$r = db_find('user', array('uid'=>array('!='=>1)));

	//$message = "<div class='comment-info'>发来了系统消息</div><div class='single-comment'>".$content."</div>";

	$message = "<div class='single-comment'>".$content."</div>";

	foreach($r as $v){
		notice_send($uid, $v['uid'], $message, 3); 	
	}

	message(0, jump('发送系统信息成功', url('inbox-system')));	


} else {
	include _include(HAYA_HTML.'system.htm');
}
