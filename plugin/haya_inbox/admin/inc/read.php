<?php

!defined('DEBUG') and exit('Access Denied.');

$header['title'] = '查看私信 - 站内信';

$action = 'msg';
	
$config = kv_get('haya_inbox');

$id = param(2);
if (empty($id)) {
	message(1, jump('私信ID不能为空', 'back'));
}

$haya_inbox_msg = haya_inbox_msg_read($id);
if (empty($haya_inbox_msg)) {
	message(1, jump('私信不存在', 'back'));
}

$sender_user = $haya_inbox_msg['sender_user'];
$receiver_user = $haya_inbox_msg['receiver_user'];

include _include(HAYA_HTML.'read.htm');
