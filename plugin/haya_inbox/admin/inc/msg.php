<?php

!defined('DEBUG') and exit('Access Denied.');

$haya_inbox_config = kv_get('haya_inbox');

$header['title'] = '站内信 - 站内信';

$pagesize = 20;
$type 	  = param(2);
$srchtype = param(3);
$keyword  = trim(xn_urldecode(param(4)));
$page     = param(5, 1);

$haya_inbox_where = array();
if ($type == 'msg') {
	$haya_inbox_where['type'] = 1;
} elseif ($type == 'system') {
	$haya_inbox_where['type'] = 2;
} else {
	$type = 'all';
}
if ($keyword) {
	if (!in_array($srchtype, array(
		'mid', 
		'content', 
		'sender_uid',
		'receiver_uid',
		'sender',
		'receiver',
	))) {
		$srchtype = 'mid';
	}
	
	if (in_array($srchtype, array('sender', 'receiver'))) {
		$haya_inbox_user = user_read_by_username($keyword);
		if (!empty($haya_inbox_user)) {
			if ($srchtype == 'sender') {
				$haya_inbox_where['sender_uid'] = array('LIKE' => $haya_inbox_user['uid']); 
			} else {
				$haya_inbox_where['receiver_uid'] = array('LIKE' => $haya_inbox_user['uid']); 
			}
		} else {
			$haya_inbox_where['sender_uid'] = '-1';
			$haya_inbox_where['receiver_uid'] = '-1';
		}
	} else {
		$haya_inbox_where[$srchtype] = array('LIKE' => $keyword); 
	}	
}

$haya_inbox_count = haya_inbox_msg__count($haya_inbox_where);
$haya_inbox_msg_list = haya_inbox_msg_find(
	$haya_inbox_where,
	array('create_time' => -1),
	$page, 
	$pagesize
);
$pagination = pagination(url("inbox-msg-$type-$srchtype-$keyword-{page}"), $haya_inbox_count, $page, $pagesize);

$haya_inbox_msg_count = haya_inbox_msg__count(array('type' => 1));
$haya_inbox_system_count = haya_inbox_msg__count(array('type' => 2));

include _include(HAYA_HTML.'msg.htm');








