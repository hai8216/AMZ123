<?php
 
defined('DEBUG') OR exit('Forbidden');

$tablepre = $db->tablepre;

db_exec("DROP TABLE IF EXISTS {$tablepre}inbox_msg;");
db_exec("DROP TABLE IF EXISTS {$tablepre}inbox_send;");
db_exec("DROP TABLE IF EXISTS {$tablepre}inbox_log;");

db_exec("ALTER TABLE {$tablepre}user DROP COLUMN inbox_unreads;");
db_exec("ALTER TABLE {$tablepre}user DROP COLUMN inbox_system_unreads;");

// 删除插件配置
kv_delete('haya_inbox'); 
