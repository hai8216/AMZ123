<?php

!defined('DEBUG') AND exit('Access Denied.');

// 引入动作
if (!in_array($action, $actions)) {
	message(1, jump('访问错误', $locationUrl));
}

// 判断文件
$hayaFile = HAYA_INC.$action.'.php';
if (!file_exists($hayaFile)) {
	message(1, jump($action . '动作不存在', $locationUrl));
}

user_login_check();

// 具体文件
include _include($hayaFile);	
