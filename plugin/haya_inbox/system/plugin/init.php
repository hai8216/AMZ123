<?php

!defined('DEBUG') AND exit('Access Denied.');

// 插件名称
defined('HAYA_PLUGIN_NAME') or define('HAYA_PLUGIN_NAME', 'haya_inbox');

// 定义插件根目录
defined('HAYA_ROOT') or define('HAYA_ROOT', APP_PATH.'plugin/'.HAYA_PLUGIN_NAME.'/');

// 定义插件动作根目录
defined('HAYA_INC') or define('HAYA_INC', HAYA_ROOT.'inc/');

// 定义插件模型根目录
defined('HAYA_MODEL') or define('HAYA_MODEL', HAYA_ROOT.'model/');

// 定义插件视图根目录
defined('HAYA_VIEW') or define('HAYA_VIEW', HAYA_ROOT.'view/');

// 定义插件页面根目录
defined('HAYA_HTML') or define('HAYA_HTML', HAYA_ROOT.'view/html/');

// 定义插件配置根目录
defined('HAYA_CONF') or define('HAYA_CONF', HAYA_ROOT.'conf/');

// 定义插件公共模型根目录
defined('HAYA_SYSTEM_MODEL') or define('HAYA_SYSTEM_MODEL', APP_PATH.'plugin/'.HAYA_PLUGIN_NAME.'/system/model/');

// 动作
if (empty($action)) {
	$action = 'setting';
}

if (empty($actions)) {
	$actions = array(
		'setting',
	);
}

if (empty($locationUrl)) {
	$locationUrl = url('inbox');
}

// 统一请求
$method = strtoupper($method);

