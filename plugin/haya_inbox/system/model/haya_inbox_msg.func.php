<?php

function haya_inbox_msg__create($arr) {
	$r = db_create('inbox_msg', $arr);
	return $r;
}

function haya_inbox_msg__update($mid, $arr) {
	$r = db_update('inbox_msg', array(
		'mid' => $mid
	), $arr);
	return $r;
}

function haya_inbox_msg__read($mid) {
	$r = db_find_one('inbox_msg', array(
		'mid' => $mid
	));
	return $r;
}

function haya_inbox_msg__find(
	$cond = array(), 
	$orderby = array(), 
	$page = 1, 
	$pagesize = 20
) {
	$r = db_find('inbox_msg', $cond, $orderby, $page, $pagesize);
	
	return $r;
}

function haya_inbox_msg__count($cond = array()) {
	$r = db_count('inbox_msg', $cond);
	return $r;
}

function haya_inbox_msg__maxmid() {
	return db_maxid('inbox_msg', 'mid');
}

function haya_inbox_msg_update_by_mid_and_sender_uid($mid, $sender_uid, $arr) {
	$r = db_update('inbox_msg', array(
		'mid' => $mid,
		'sender_uid' => $sender_uid
	), $arr);
	return $r;
}

function haya_inbox_msg_update_by_mid_and_receiver_uid($mid, $receiver_uid, $arr) {
	$r = db_update('inbox_msg', array(
		'mid' => $mid,
		'receiver_uid' => $receiver_uid
	), $arr);
	return $r;
}

function haya_inbox_msg_delete_by_mid($mid) {
	$r = db_delete('inbox_msg', array('mid' => $mid));
	return $r;
}

function haya_inbox_msg_delete_by_sender_uid($sender_uid) {
	$r = db_delete('inbox_msg', array('sender_uid' => $sender_uid));
	return $r;
}

function haya_inbox_msg_delete_by_receiver_uid($receiver_uid) {
	$r = db_delete('inbox_msg', array('receiver_uid' => $receiver_uid));
	return $r;
}

function haya_inbox_msg_delete_by_sender_uid_and_receiver_uid($sender_uid, $receiver_uid) {
	$r = db_delete('inbox_msg', array('sender_uid' => $sender_uid, 'receiver_uid' => $receiver_uid));
	return $r;
}

function haya_inbox_msg_read($mid) {
	$r = db_find_one('inbox_msg', array(
		'mid' => $mid
	));
	
	$r['sender_user'] = user_read_cache($r['sender_uid']);
	$r['receiver_user'] = user_read_cache($r['receiver_uid']);

	return $r;
}

function haya_inbox_msg_find(
	$cond = array(), 
	$orderby = array(), 
	$page = 1, 
	$pagesize = 20
) {
	$haya_inbox_msg_list = db_find('inbox_msg', $cond, $orderby, $page, $pagesize);
	
	if (!empty($haya_inbox_msg_list)) {
		foreach ($haya_inbox_msg_list as & $msg) {
			$msg['sender_user'] = user_read_cache($msg['sender_uid']);
			$msg['receiver_user'] = user_read_cache($msg['receiver_uid']);
		}
	}	
	
	return $haya_inbox_msg_list;
}

function haya_inbox_msg_read_by_type($type) {
	$haya_inbox = db_find_one('inbox_msg', array(
		'type' => $type
	));
	return $haya_inbox;
}

function haya_inbox_msg_find_type(
	$cond = array(), 
	$orderby = array(), 
	$page = 1, 
	$pagesize = 20
) {
	$haya_inbox_msg_list = haya_inbox_msg_find($cond, $orderby, $page, $pagesize);
	
	$haya_inbox_msg_key_list = array();
	if (!empty($haya_inbox_msg_list)) {
		foreach ($haya_inbox_msg_list as $haya_inbox_msg_list_key => $haya_inbox_msg_list_value) {
			$haya_inbox_msg_key_list[$haya_inbox_msg_list_value['type']] = $haya_inbox_msg_list_value;
		}
	}
	return $haya_inbox_msg_key_list;
}

?>