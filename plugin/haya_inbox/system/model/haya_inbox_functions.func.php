<?php

function haya_inbox_functions_check_my_send_msg(
	$haya_inbox_config, 
	$group,
	$grouplist
) {
	
	if (isset($haya_inbox_config['open_group']) 
		&& $haya_inbox_config['open_group'] == 1
	) {
		$haya_inbox_config_user_group = isset($haya_inbox_config['user_group']) ? explode(',', $haya_inbox_config['user_group']) : array();
		if (!in_array($group['gid'], $haya_inbox_config_user_group)) {
			$haya_inbox_config_user_no_group_tip = trim($haya_inbox_config['user_no_group_tip']);
		
			$haya_inbox_user_group = array();
			if (!empty($grouplist)) {
				foreach ($grouplist as $_gid => $_group) {
					if (in_array($_gid, $haya_inbox_config_user_group)) {
						$haya_inbox_user_group[] = $_group['name'];
					}
				}
			}

			$haya_inbox_config_user_no_group_tip = str_replace(
				array('{user_group}', '{msg_group}'), 
				array($group['name'], implode('，', $haya_inbox_user_group)), 
				$haya_inbox_config_user_no_group_tip
			);	
			
			message(-1, $haya_inbox_config_user_no_group_tip);
		}
	}	
	
	return true;
}

function haya_inbox_functions_check_user_send_msg($haya_inbox_config, $gid) {
	if (isset($haya_inbox_config['open_group']) 
		&& $haya_inbox_config['open_group'] == 1
	) {
		$haya_inbox_config_user_group = isset($haya_inbox_config['user_group']) ? explode(',', $haya_inbox_config['user_group']) : array();
		if (!in_array($gid, $haya_inbox_config_user_group)) {
			message(-1, "对方权限不足，不能够接收你发的私信");
		}
	}	
	
	return true;
}

function haya_inbox_functions_humandate($timestamp, $lan = array()) {
	$time = $_SERVER['time'];
	$lang = $_SERVER['lang'];
	
	static $custom_humandate = NULL;
	if ($custom_humandate === NULL) {
		$custom_humandate = function_exists('custom_humandate');
	}
	if ($custom_humandate) {
		return custom_humandate($timestamp, $lan);
	}
	
	$seconds = $time - $timestamp;
	
	if (empty($lan)) {
		$lan = $lang;
	}
	$haya_lan = array(
		'yesterday' => '昨天',
		'today' => '今天',
		'hour_ago' => '小时前',
		'minute_ago' => '分钟前',
		'second_ago' => '秒前',
	);
	$lan = array_merge($haya_lan, $lan);
	
	if ($seconds > 43200) {
		if (date('Y-m-d', $timestamp) == date('Y-m-d')) {
			return $lan['today'].date('H:i:s', $timestamp);
		} elseif (date('Y-m-d', $timestamp) == date('Y-m-d', strtotime("-1 day"))) {
			return $lan['yesterday'].date('H:i:s', $timestamp);
		} elseif (date('Y', $timestamp) == date('Y')) {
			return date('m-d H:i:s', $timestamp);
		} else {
			return date('Y-m-d H:i:s', $timestamp);
		}
	} elseif($seconds > 3600) {
		return floor($seconds / 3600).$lan['hour_ago'];
	} elseif($seconds > 60) {
		return floor($seconds / 60).$lan['minute_ago'];
	} else {
		return $seconds.$lan['second_ago'];
	}
}


?>