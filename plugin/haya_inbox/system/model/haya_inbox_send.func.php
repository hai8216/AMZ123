<?php

function haya_inbox_send__create($arr) {
	$r = db_create('inbox_send', $arr);
	return $r;
}

function haya_inbox_send__update($sid, $arr) {
	$r = db_update('inbox_send', array(
		'sid' => $sid
	), $arr);
	return $r;
}

function haya_inbox_send__read($sid) {
	$r = db_find_one('inbox_send', array(
		'sid' => $sid
	));
	return $r;
}

function haya_inbox_send__find(
	$cond = array(), 
	$orderby = array(), 
	$page = 1, 
	$pagesize = 20
) {
	$r = db_find('inbox_send', $cond, $orderby, $page, $pagesize);
	
	return $r;
}

function haya_inbox_send__count($cond = array()) {
	$r = db_count('inbox_send', $cond);
	return $r;
}

function haya_inbox_send__maxsid() {
	return db_maxid('inbox_send', 'sid');
}

function haya_inbox_send_update_by_mid_and_uid($mid, $uid, $arr) {
	$r = db_update('inbox_send', array(
		'mid' => $mid,
		'uid' => $uid
	), $arr);
	return $r;
}

function haya_inbox_send_update_by_sid_and_uid($sid, $uid, $arr) {
	$r = db_update('inbox_send', array(
		'sid' => $sid,
		'uid' => $uid
	), $arr);
	return $r;
}

function haya_inbox_send_update_by_sid_and_friend($sid, $friend, $arr) {
	$r = db_update('inbox_send', array(
		'sid' => $sid,
		'friend' => $friend
	), $arr);
	return $r;
}

function haya_inbox_send_update_by_uid_and_friend($uid, $friend, $arr) {
	$r = db_update('inbox_send', array(
		'uid' => $uid,
		'friend' => $friend,
	), $arr);
	return $r;
}

function haya_inbox_send_update_by_sid_and_uid_and_friend($sid, $uid, $friend, $arr) {
	$r = db_update('inbox_send', array(
		'sid' => $sid,
		'uid' => $uid,
		'friend' => $friend,
	), $arr);
	return $r;
}

function haya_inbox_send_delete_by_sid($sid) {
	$r = db_delete('inbox_send', array('sid' => $sid));
	return $r;
}

function haya_inbox_send_delete_by_mid($mid) {
	$r = db_delete('inbox_send', array('mid' => $mid));
	return $r;
}

function haya_inbox_send_delete_by_uid($uid) {
	$r = db_delete('inbox_send', array('uid' => $uid));
	return $r;
}

function haya_inbox_send_delete_by_friend($friend) {
	$r = db_delete('inbox_send', array('friend' => $friend));
	return $r;
}

function haya_inbox_send_delete_by_sid_and_uid($sid, $uid) {
	$r = db_delete('inbox_send', array(
		'sid' => $sid, 
		'uid' => $uid
	));
	return $r;
}

function haya_inbox_send_delete_by_mid_and_uid($mid, $uid) {
	$r = db_delete('inbox_send', array(
		'mid' => $mid, 
		'uid' => $uid
	));
	return $r;
}

function haya_inbox_send_delete_by_sid_and_friend($sid, $friend) {
	$r = db_delete('inbox_send', array(
		'sid' => $sid, 
		'friend' => $friend
	));
	return $r;
}

function haya_inbox_send_delete_by_uid_and_friend($uid, $friend) {
	$r = db_delete('inbox_send', array(
		'uid' => $uid, 
		'friend' => $friend
	));
	return $r;
}

function haya_inbox_send_find(
	$cond = array(), 
	$orderby = array(), 
	$page = 1, 
	$pagesize = 20
) {
	$haya_inbox_send_list = db_find('inbox_send', $cond, $orderby, $page, $pagesize);
	
	if (!empty($haya_inbox_send_list)) {
		$floor = ($page - 1)* $pagesize + 1;
		foreach ($haya_inbox_send_list as & $msg) {
			$msg['floor'] = $floor++;
			$msg['user'] = user_read_cache($msg['uid']);
			$msg['friend_user'] = user_read_cache($msg['friend']);
		}
	}	
	
	return $haya_inbox_send_list;
}

function haya_inbox_send_read_by_mid_and_uid($mid, $uid) {
	$r = db_find_one('inbox_send', array(
		'mid' => $mid,
		'uid' => $uid,
	));
	return $r;
}

function haya_inbox_send_read_by_sid_and_uid($sid, $uid) {
	$r = db_find_one('inbox_send', array(
		'sid' => $sid,
		'uid' => $uid,
	));
	return $r;
}


?>