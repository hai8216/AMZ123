<?php

function haya_inbox_log__create($arr) {
	$r = db_create('inbox_log', $arr);
	return $r;
}

function haya_inbox_log__update($lid, $arr) {
	$r = db_update('inbox_log', array(
		'lid' => $lid
	), $arr);
	return $r;
}

function haya_inbox_log__read($lid) {
	$r = db_find_one('inbox_log', array(
		'lid' => $lid
	));
	return $r;
}

function haya_inbox_log__find(
	$cond = array(), 
	$orderby = array(), 
	$page = 1, 
	$pagesize = 20
) {
	$list = db_find('inbox_log', $cond, $orderby, $page, $pagesize);
	
	return $list;
}

function haya_inbox_log__count($cond = array()) {
	$n = db_count('inbox_log', $cond);
	return $n;
}

function haya_inbox_log__maxlid() {
	return db_maxid('inbox_log', 'lid');
}

function haya_inbox_log_read_by_sender_id_and_receiver_id($sender_id, $receiver_id) {
	$r = db_find_one('inbox_log', array(
		'sender_id' => $sender_id, 
		'receiver_id' => $receiver_id
	));
	return $r;
}

function haya_inbox_log_update_by_sender_id_and_receiver_id($sender_id, $receiver_id, $arr) {
	$r = db_update('inbox_log', array(
		'sender_id' => $sender_id, 
		'receiver_id' => $receiver_id
	), $arr);
	return $r;
}

function haya_inbox_log_delete_by_lid($lid) {
	$r = db_delete('inbox_log', array('lid' => $lid));
	return $r;
}

function haya_inbox_log_delete_by_sender_id($sender_id) {
	$r = db_delete('inbox_log', array('sender_id' => $sender_id));
	return $r;
}

function haya_inbox_log_delete_by_receiver_id($receiver_id) {
	$r = db_delete('inbox_log', array('receiver_id' => $receiver_id));
	return $r;
}

function haya_inbox_log_delete_by_sender_id_and_receiver_id($sender_id, $receiver_id) {
	$r = db_delete('inbox_log', array(
		'sender_id' => $sender_id, 
		'receiver_id' => $receiver_id
	));
	return $r;
}

function haya_inbox_log_find(
	$cond = array(), 
	$orderby = array(), 
	$page = 1, 
	$pagesize = 20
) {
	$r = db_find('inbox_log', $cond, $orderby, $page, $pagesize);
	
	if (!empty($r)) {
		foreach ($r as & $log) {
			$log['sender_user'] = user_read_cache($log['sender_id']);
			$log['receiver_user'] = user_read_cache($log['receiver_id']);
		}
	}	
	
	return $r;
}


?>
