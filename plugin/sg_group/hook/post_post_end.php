<?php exit;
//若超上限，则该钩子全部不运行

$return_html = param('return_html', 0);
$forum = forum_read($fid);
$credits = $forum['post_credits'];
$golds = $forum['post_golds'];
$sg_group = setting_get('sg_group');
$todaytime = strtotime(date('Y-m-d', time()));
$todaycredit = db_find_one("credit_log", array('uid' => $uid, 'type' => 1, 'dateline' => array(">" => $todaytime)));
$todaygolds = db_find_one("credit_log", array('uid' => $uid, 'type' => 2, 'dateline' => array(">" => $todaytime)));
if ($credits) {
    if ($todaycredit['value'] >= $sg_group['credits_max']) {
        $credits = 0;
    } else {
        //存个记录
        if ($todaycredit['uid']) {
            db_update("credit_log", array('uid' => $uid, 'type' => 1, 'dateline' => array('>' => $todaytime)), array('value+' => $credits));
        } else {
            db_insert("credit_log", array('uid' => $uid, 'value' => $credits, 'type' => '1', 'dateline' => time()));
        }
    }
}
if ($golds) {
    if ($todaygolds['value'] >= $sg_group['gold_max']) {
        $golds = 0;
    } else {
        if ($todaygolds['uid']) {
            db_update("credit_log", array('uid' => $uid, 'type' => 2, 'dateline' => array('>' => $todaytime)), array('value+' => $golds));
        } else {
            db_insert("credit_log", array('uid' => $uid, 'value' => $golds, 'type' => '2', 'dateline' => time()));
        }

    }
}

$uid AND user_update($uid, array('credits+' => $credits, 'golds+' => $golds));
user_update_group($uid);


$message = '';
!empty($credits) AND $message = lang('sg_creditsplus', array('credits' => $credits));
!empty($golds) AND $message = lang('sg_goldsplus', array('golds' => $golds));
!empty($credits) && !empty($golds) AND $message = lang('sg_creditsplus', array('credits' => $credits)) . '、' . lang('sg_goldsplus', array('golds' => $golds));
if ($return_html) {
    $filelist = array();
    ob_start();
    include _include(APP_PATH . 'view/htm/post_list.inc.htm');
    $s = ob_get_clean();
    message(0, $s);
} else {
    $message = $message ? $message : lang('create_post_sucessfully');
    if ($ajax && $_GET['xcxapi']) {
        include APP_PATH . "/plugin/xl_xcxapi/json.func.php";
        $ajaxdata = db_find_one("post", array('pid' => $pid));
        $ajaxdata['posts'] = db_find_one("thread", array('tid' => $tid))['posts'];
        $ajaxdata['user'] = user_read($ajaxdata['uid']);
        $ajaxdata['user'] = getvalues(array($ajaxdata['user']), array('/^\d+$/'), array('username', 'avatar_url'))[0];
        $ajaxdata['create_date'] = humandate($ajaxdata['create_date']);
        message(0, array('message' => $message, 'mydata' => $ajaxdata));
    } else {
        message(0, $message);
    }
}
?>








