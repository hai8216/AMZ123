<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_blackplus';
$action = param(3) ?: "list";
$kid = param(4);
$ko = trim(xn_urldecode(param(5)));
if ($action == 'list') {

    $page = param(4, 1);
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;

    $list = db_find('black_comment', array(), array('id' => '-1'), $page, $pagesize);
    $c = db_count('black_comment', array());
    $pagination = pagination(url("plugin-setting-xl_blackplus-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/list.htm');

} elseif ($action == 'add') {
    if ($method == 'GET') {

        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/add.htm');
    } else {

        $pl_keywords = param('pl_keywords');
        $plkeywrods = explode("\n", $pl_keywords);
        foreach ($plkeywrods as $v) {
            db_insert('black_comment', array('comment' => $v));
        }
        message(0, 'success');
    }
} elseif ($action == 'delete') {
    db_delete('black_comment', array('id' => param('id')));
    message(0, 'success');
}
?>