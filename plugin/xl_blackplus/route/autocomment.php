<?php

//先获取随机帖子
$time = time();
$h = date('H', $time);
if (intval($h) >= 0 && intval($h) < 9) {
    exit;
}
$y = strtotime('-1year');
$threadcount = db_count("thread",array('fid'=>3,'is_deleted' => 0, 'create_date'=>array('>'=>$y)));
$thread_one = db_find("thread",array('fid'=>3,'is_deleted'=> 0,'create_date'=>array('>'=>$y)),array(),rand(1,$threadcount),1)[0];
//随机用户
$ucount = db_count('user', array('ask_type' => 1));
$pageall = rand(0, $ucount);
$postuid = db_find('user', array('ask_type' => 1), array(), $pageall, 1)[0];
//随机话
$bcount = db_count("black_comment");
$cone = db_find("black_comment",array(),array(),rand(1,$bcount),1)[0];
$post = array(
    'tid'=>$thread_one['tid'],
    'uid'=>$postuid['uid'],
    'create_date'=>time(),
    'userip'=>"",
    'isfirst'=>0,
    'doctype'=>"",
    'quotepid'=>'',
    'message'=>$cone['comment'],
);
$pid = post_create($post, 3, '101');

message(0, jump(lang('create_post_sucessfully'),url('thread-'.$thread_one['tid'])));