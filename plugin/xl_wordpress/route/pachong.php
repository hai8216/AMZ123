<?php
$password = "123123/as";
if (empty($_POST['__sign']) || md5("{$password}shenjianshou.cn") != trim($_POST['__sign'])) {
    echo json_encode([
        "result" => 2,
        "reason" => "发布失败, 错误原因: 发布密码验证失败!"
    ]);
    exit;
}

////测试爬虫
//$data['biz'] = $_POST['biz'];
//$data['title'] = $_POST['article_title'];
//$data['dateline'] = time();
//db_insert('pachong',$data);
//echo json_encode(["result" => 1, "data" => "发布成功"]);
//exit;

//重复贴的处理
$isHava = db_find_one("wpspachong_log", array('biz' => md5($_POST['title'])));
if ($isHava['id']) {
    echo json_encode([
        "result" => 2,
        "reason" => "文章重复"
    ]);
    exit;
}
//爬虫记录
$pclog['wps_name'] = $_POST['username'];
$pclog['wps_title'] = $_POST['title'];
$pclog['biz'] = md5($_POST['title']);
db_insert("wpspachong_log", $pclog);

require_once(APP_PATH . "/plugin/xl_aliopensearch/Config.inc.php");

use OpenSearch\Client\DocumentClient;

//自动发布
$fid = 1;//默认板块
$forum = forum_read($fid);
$wps_name = $_POST['username'];
//看本地绑定情况
$wps_bind = array();
$wps_bind = db_find_one("wordpress_bind", array('wps_name' => $wps_name));
if ($wps_bind['id']) {
    $uid = $wps_bind['local_uid'];
} else {
    $uid = 10;
}
$sid = '';
$subject = $_POST['title'];
$message = $_POST['content'];
$time = $_POST['dateline'] ? strtotime($_POST['dateline']) : time();

$thread = array(
    'fid' => $fid,
    'uid' => $uid,
    'sid' => $sid,
    'subject' => $subject,
    'message' => $message,
    'time' => $time,
    'longip' => $longip,
    'doctype' => 0,
);
$remote_img = db_find_one('setting', array('name' => 'remote_img'));
if ($remote_img['value']) {
    preg_match_all("/<img[^>]*src\s?=\s?[\'|\"]([^\'|\"]*)[\'|\"]/is", $message, $img);
    $imgArr = $img[1];
    if ($imgArr) {
        foreach ($imgArr as $k => $v) {
            if (strstr($v, 'http')) {
                $path = "./upload/remote/" . date('Y-m-d');
                if (!file_exists($path)) mkdir($path, 0755, true);
                $extname = substr($v, strrpos($v, '.'));
                if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
                    $ext = $extname;
                } else {
                    $ext = '.jpg';
                }
                $name = uniqid() . $ext;
                saveImg($v, $path . "/" . $name);
                //file_put_contents($path."/".$name, file_get_contents(isset($_get[url])?$_get[url]:$v));
                $message = str_replace($v, $path . "/" . $name, $message);
                $thread['message'] = $message;
            }
        }
    }
}

$tid = thread_create($thread, $pid);
$pid === FALSE AND message(-1, lang('create_post_failed'));
$tid === FALSE AND message(-1, lang('create_thread_failed'));
$user = user_read($uid);
if ($pid) {
    $tableName = 'bbs_post';
    $smsg = strip_tags($message);
    $documentClient = new DocumentClient($client);
    $docs_to_upload = array();
    $item['cmd'] = 'ADD';
    $item["fields"] = array(
        'pid' => $pid,
        'tid' => $tid,
        'subject' => $subject,
        'isfirst' => 1,
        'message' => str_replace("&nbsp;", " ", $smsg),
        'author' => $user['username'],
        'authorid' => $uid,
        'fid' => $fid,
        'fname' => $forum['name'],
        'back_message' => '',
        'posttime' => $time,
    );
    $docs_to_upload[] = $item;
    $json = json_encode($docs_to_upload);
    $ret = $documentClient->push($json, $appName, $tableName);
}
//增加文字数
//c0ontent_count
db_update('wpspachong_log', array('wps_name' => $wps_name), array('content_count+' => 1));
echo json_encode(["result" => 1, "data" => "发布成功"]);