<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') AND exit('Access Denied.');
if (param('do')==1 && $_POST) {
    $data['wps_name'] = param('wps_name');
    $data['local_uid'] = param('local_uid');
    $webname = db_find_one("user",array('uid'=>$data['local_uid']));
    $data['local_name'] = $webname['username'];

    //保持一致性
    $iswx = db_find_one("wordpress_bind",array('wps_name'=>$data['wps_name']));
    if($iswx){
        message(0,'该账号已被绑定');
    }
    db_insert("wordpress_bind",$data);
    message(0, jump('绑定成功', url('plugin-setting-xl_wordpress')));

} elseif(param('do')==2){
    if($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('id', 0);
    $r = db_delete('wordpress_bind', array('id'=>$_codeid));
    $r === FALSE AND message(-1, lang('delete_failed'));
} else{
    $page = param(3, 1);
    $pagesize = 40;
    $n = db_count('wordpress_bind', array());
    $pagination = pagination(url("plugin-setting-xl_wordpress-{page}"), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_wordpress-{page}"), $n, $page, $pagesize);
    $codelist = db_find('wordpress_bind', array(), array(),$page,$pagesize);
    include _include(APP_PATH . "/plugin/xl_wordpress/view/admin.htm");
}