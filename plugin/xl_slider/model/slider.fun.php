<?php
function getSlideData()
{
    $slider = $lists = array();

    //查询3个热门的放入缓存去
    $threadlist_Slider = cache_get('24hotThread_new');
    if (!$threadlist_Slider) {
        $time = strtotime('-3 day');
        $list = db_find("thread", array('fid' => 1, 'create_date' => array('>' => $time), 'is_deleted' => 0), array('views' => '-1'), 1, 5);
        cache_set('24hotThread_new', $list, 3600);
        $threadlist_Slider = $list;
    }
    foreach ($threadlist_Slider as $k => $v) {
        $lists[$k]['title'] = $threadlist_Slider[$k]['subject'];
        $lists[$k]['img'] = getimg($threadlist_Slider[$k]['tid']);
        $lists[$k]['time'] = humandate($threadlist_Slider[$k]['create_date']);
        $lists[$k]['links'] = url('thread-' . $threadlist_Slider[$k]['tid']);
        $lists[$k]['id'] = $threadlist_Slider[$k]['tid'];
    }
    $slider = db_find("xl_ads");

    foreach ($slider as $k => $v) {
        $t[] = $v;
    }
    array_splice($lists, ($v['pos']-1), 0, $t);
    return $lists;
}


function getimg($tid)
{
    return getPost_img($tid);

}