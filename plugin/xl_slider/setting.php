<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') AND exit('Access Denied.');

if (param('do') == 1 && $_POST) {
    $data['img'] = param('img');
    $data['title'] = param('title');
    $data['links'] = param('links');
    $data['pos'] = param('pos');
    $data['click'] = 0;
    db_insert('xl_ads', $data);
    message(0, jump('添加成功', url('plugin-setting-xl_slider')));

}
if (param('do') == 2 && $_POST) {
    if ($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('id', 0);
    $r = db_delete('xl_ads', array('id' => $_codeid));
    $r === FALSE AND message(-1, lang('delete_failed'));

}
if (param('do') == 3 && $_POST) {
    if ($method != 'POST') message(-1, 'Method Error.');
    $radio = param('radio');
    kv_set('index_slider_radio', $radio);
} else {
    $slider = array();
    $slider = db_find("xl_ads", array('pos' => array('<' => 5)));
    include _include(APP_PATH . "/plugin/xl_slider/setting.htm");
}

function postext($a)
{
    $pa = array('1' => '1', '2' => '2', '3' => '3');
    return $pa[$a];
}