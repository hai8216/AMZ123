<?php


!defined('DEBUG') and exit('Access Denied.');
$setting = setting_get('ax_ads');
$action = param(3);

$adsAry = [
    "99" => "不显示",
    "0" => "右侧全局轮播",
    "1" => "右侧固定1",
    "2" => "右侧固定2",
    "3" => "弹窗广告",
    "4" => "首页文字广告",
    "5" => "板块主页广告",
    "6" => "详细页面广告",
    "7" => "板块单广告",
    "8" => "侧栏广告(无限)",
//    "9" => "小程序幻灯片",
    "10" => "首页漂浮",
    "11" => "首页底部banner",
    "17" => "首页底部漂浮",
    "12" => "监控插件广告",
    "13" => "标签左侧广告",
    "14" => "标签右侧广告",
    "15" => "问答左侧广告",
    "16" => "问答右侧广告",
    "17" => "左下角漂浮",
    "18" => "弹窗广告2",
    "19"=>"登录幻灯片",
    "20"=>"红包漂浮",
    "21"=>"帖内顶部通栏",
    "22"=>"帖子顶部两图",
    "23"=>"视频详情页广告",
    "24"=>"单位换算右侧广告"
];
$type = param('type', 'def');
if ($type != 'def') {
    $linklist = db_find('ax_ads', array('type' => $type), array('rank' => -1), 1, 1000, 'linkid');
} else {
    $linklist = db_find('ax_ads', array(), array('rank' => -1), 1, 1000, 'linkid');
}

// 自定义链接


$maxid = db_maxid('ax_ads', 'linkid');

if ($action == '') {
    include _include(APP_PATH . 'plugin/ax_ads/setting.htm');

} elseif ($action == 'create') {
    if ($method == 'GET') {
        $linkid = param('linkid');
        if($linkid){
            $val = db_find_one("ax_ads",array('linkid'=>$linkid));
        }else{
            $val = ["linkid"=>0,"name" => "", "type" => 0, "rank" => "0", "name" => "", "note" => "", "url" => "", "target" => 0];
        }
        include _include(APP_PATH . 'plugin/ax_ads/add.htm');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/index_icon/" . date('Ymd') . "/";
            $ext = '.'.$type;
            $name = uniqid() . $ext;
            if(saveTencentBase64(base64_decode(str_replace($res[1],'', param('hidden_img_tpl'))), $path . $name)){
                $data['icon'] = $path . $name;
            }
        }
        $data['type'] = param('type');
        $data['rank'] = param('rank');
        $data['name'] = param('name');
        $data['name2'] = param('name2');
        $data['note'] = param('note');
        $data['url'] = param('url');
        $data['target'] = param('target');
        $data['create_date'] = time();
        if(param('linkid')){
            db_update("ax_ads",["linkid"=>param("linkid")],$data);
        }else{
            db_insert("ax_ads",$data);
        }
        message(0, array('a' => '成功'));

    }
} elseif($action=='delete'){
    db_delete("ax_ads",["linkid"=>param("linkid")]);
    message(0, array('a' => '成功'));
}elseif ($action == 'io') {

    $setting['icon_io'] = param('icon_io', 0);
    $setting['link_index_headernav'] = param('link_index_headernav', 0);
    $setting['link_index_friendlink'] = param('link_index_friendlink', 0);

    setting_set('ax_ads', $setting);
    message(0, array('a' => '修改成功', 'b' => '修改成功', 'c' => '失败'));

} elseif ($action == 'link') {

    $rowidarr = param('rowid', array(0));
    $iconarr = param('icon', array(''));
    $namearr = param('name', array(''));
    $notearr = param('note', array(''));
    $urlarr = param('url', array(''));
    $rankarr = param('rank', array(0));
    $typearr = param('type', array(0));
    $targetarr = param('target', array(0));

    unset($rowidarr[0]);
    unset($iconarr[0]);
    unset($namearr[0]);
    unset($notearr[0]);
    unset($urlarr[0]);
    unset($rankarr[0]);
    unset($typearr[0]);
    unset($targetarr[0]);

    $arrlist = array();
    foreach ($rowidarr as $k => $v) {
        $arr = array(
            'linkid' => $k,
            'name' => $namearr[$k],
            'note' => $notearr[$k],
            'url' => $urlarr[$k],
            'target' => $targetarr[$k],
            'rank' => $rankarr[$k],
            'type' => $typearr[$k],
        );
        // icon


        if (!isset($linklist[$k])) {
            db_create('ax_ads', $arr);
        } else {
            db_update('ax_ads', array('linkid' => $k), $arr);
        }

        if (!empty($iconarr[$k])) {
            //上云
            $s = array_value($iconarr, $k);
            if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $s, $res)) {
                $type = $res[2];
                $path = "/upload/ax_ads/" . date('Ymd') . "/";
                $ext = '.' . $type;
                $name = uniqid() . $ext;
                if (saveTencentBase64(base64_decode(str_replace($res[1], '', $s)), $path . $name)) {
                    $iconfile = $path . $name;
                }
            }
            db_update('ax_ads', array('linkid' => $k), array('icon' => getImgCloud() . $iconfile));

        }
    }
    // 删除
    $deletearr = array_diff_key($linklist, $rowidarr);
    foreach ($deletearr as $k => $v) {
        if ($v['icon']) {
            @unlink($v['icon']);
        }
        db_delete('ax_ads', array('linkid' => $k));
    }

    message(0, '保存成功');
}

?>
