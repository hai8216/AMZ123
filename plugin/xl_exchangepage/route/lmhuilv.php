<?php


$clist = array(
    array(
        'i' => 'CNY',
        'name' => '中国',
        'fh' => '¥'
    ),
    array(
        'i' => 'USD',
        'name' => '美元',
        'fh' => '¥'
    ),
    array(
        'i' => 'MXN',
        'name' => '墨西哥比索',
        'fh' => '$'
    ),
    array(
        'i' => 'BRL',
        'name' => '巴西雷阿尔',
        'fh' => '£'
    ),
    array(
        'i' => 'CLP',
        'name' => '智利比索',
        'fh' => '€'
    ),
    array(
        'i' => 'ARS',
        'name' => '阿根廷比索',
        'fh' => '¥'
    ),

    array(
        'i' => 'PEN',
        'name' => '秘鲁索尔',
        'fh' => '$'
    ),
    array(
        'i' => 'COP',
        'name' => '哥伦比亚比索',
        'fh' => '$'
    ),
    array(
        'i' => 'UYU',
        'name' => '乌拉圭比索',
        'fh' => '¥'
    ),
//    array(
//        'i' => 'PLN',
//        'name' => '波兰兹罗提',
//        'fh' => '¥'
//    ),
//    array(
//        'i' => 'RON',
//        'name' => '罗马尼亚新列伊',
//        'fh' => '¥'
//    ),
//    array(
//        'i' => 'SEK',
//        'name' => '瑞典克朗',
//        'fh' => '¥'
//    ),
//    array(
//        'i' => 'EUR',
//        'name' => '欧元',
//        'fh' => '¥'
//    ),
//    array(
//        'i' => 'GBP',
//        'name' => '英镑',
//        'fh' => '¥'
//    ),
);

$hdata = db_find_one("exchange_other", array('MXN' => array('>' => 0)), array('dateline' => '-1'));

foreach ($clist as $v) {
    $list[$v['i']] = $hdata[$v['i']];
}
$list['CNY'] = 1;
$Hjson = json_encode($list);
$header['title'] = "在线拉美汇率换算 - " . $header['title'];
include _include(APP_PATH . 'plugin/xl_exchangepage/lmhuilv.htm');