<?php

$clist = array(
    array(
        'i' => 'CNY',
        'name' => '中国',
        'fh' => '¥'
    ),
    array(
        'i' => 'USD',
        'name' => '美元',
        'fh' => '$'
    ),
    array(
        'i' => 'GBP',
        'name' => '英镑',
        'fh' => '£'
    ),
    array(
        'i' => 'EUR',
        'name' => '欧元',
        'fh' => '€'
    ),
    array(
        'i' => 'JPY',
        'name' => '日元',
        'fh' => '¥'
    ),

    array(
        'i' => 'AUD',
        'name' => '澳元',
        'fh' => '$'
    ),
    array(
        'i' => 'CAD',
        'name' => '加元',
        'fh' => '$'
    ),


    array(
        'i' => 'HKD',
        'name' => '港元',
        'fh' => '¥'
    ),


);
$hdata = db_find_one("exchange", array('m'=>array('>'=>0)), array('dateline' => '-1'));

$hlist['USD'] = $hdata['m'];
$hlist['GBP'] = $hdata['y'];
$hlist['EUR'] = $hdata['o'];
$hlist['JPY'] = $hdata['r'];
$hlist['CAD'] = $hdata['j'];
$hlist['CNY'] = 1;
$hlist['HKD'] =  $hdata['hk'];
$hlist['AUD'] = $hdata['od'];
$Hjson = json_encode($hlist);
//echo $Hjson;
//exit;
$header['title'] = "在线汇率换算 - ".$header['title'];
include _include(APP_PATH . 'plugin/xl_exchangepage/exchangepage.htm');