<?php


$clist = array(
    array(
        'i' => 'CNY',
        'name' => '中国',
        'fh' => '¥'
    ),
    array(
        'i' => 'USD',
        'name' => '美元',
        'fh' => '¥'
    ),
    array(
        'i' => 'PLN',
        'name' => '波兰兹罗提',
        'fh' => '¥'
    ),
    array(
        'i' => 'RON',
        'name' => '罗马尼亚新列伊',
        'fh' => '¥'
    ),
    array(
        'i' => 'SEK',
        'name' => '瑞典克朗',
        'fh' => '¥'
    ),
    array(
        'i' => 'EUR',
        'name' => '欧元',
        'fh' => '¥'
    ),
    array(
        'i' => 'GBP',
        'name' => '英镑',
        'fh' => '¥'
    ),
);

$hdata = db_find_one("exchange_other", array('MXN' => array('>' => 0)), array('dateline' => '-1'));

foreach ($clist as $v) {
    $list[$v['i']] = $hdata[$v['i']];
}
$list['CNY'] = 1;
$Hjson = json_encode($list);
$header['title'] = "在线欧洲汇率换算 - " . $header['title'];
include _include(APP_PATH . 'plugin/xl_exchangepage/euhuilv.htm');