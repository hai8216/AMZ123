<?php
!defined('DEBUG') AND exit('Access Denied.');
$action = param('3', 'time');
$pluginPath = APP_PATH . "plugin/xl_timeconsole";
if ($action == 'time') {
    $list = db_find("timeconsole", array('time_type' => 0), array('order' => '-1'), 1, 50);
    include _include($pluginPath . "/admin/time.htm");
} elseif ($action == 'page') {
    $list = db_find("timeconsole", array('time_type' => 2), array('order' => '-1'), 1, 50);
    include _include($pluginPath . "/admin/timepage.htm");
} elseif ($action == 'addtimePage') {
    if ($method == 'GET') {
        $edid = param('edid');
        if ($edid) {
            $val = db_find_one("timeconsole", array('id' => $edid));
        } else {
            $val = ['id' => '0', 'console_name' => '', 'time_offset' => '', 'time_mis' => '', 'order' => 0, 'type' => 0];
        }
        include _include($pluginPath . "/admin/addtimePage.htm");
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/time_icon/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if(saveTencentBase64(base64_decode(str_replace($result[1],'',  param('favicon'))), $wwwpath . $new_file)){
                $updata['icon'] = $wwwpath . $new_file;
            }
        }
        $updata['time_type'] = 2;
        $updata['console_name'] = param('console_name', '未知');
        $updata['time_offset'] = param('time_offset', '0');
        $updata['time_mis'] = param('time_mis', '');
        $updata['order'] = param('order', '0');
        $updata['type'] = param('type', '0');
        $edid = param('edid', 0);
        if ($edid) {
            db_update("timeconsole", array('id' => $edid), $updata);
        } else {
            db_insert("timeconsole", $updata);
        }
        message(0, 'success');
    }
} elseif ($action == 'addtime') {
    if ($method == 'GET') {
        $edid = param('edid');
        if ($edid) {
            $val = db_find_one("timeconsole", array('id' => $edid));
        } else {
            $val = ['id' => '0', 'console_name' => '', 'time_offset' => '', 'time_mis' => '', 'order' => 0, 'type' => 0];
        }
        include _include($pluginPath . "/admin/addtime.htm");
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }

            $new_file = time() . rand(100, 50000) . "." . $type;

            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('favicon'))))) {
                $updata['icon'] = $wwwpath . $new_file;
            }
        }
        $updata['console_name'] = param('console_name', '未知');
        $updata['time_offset'] = param('time_offset', '0');
        $updata['time_mis'] = param('time_mis', '');
        $updata['order'] = param('order', '0');
        $updata['type'] = param('type', '0');
        $edid = param('edid', 0);
        if ($edid) {
            db_update("timeconsole", array('id' => $edid), $updata);
        } else {
            db_insert("timeconsole", $updata);
        }
        message(0, 'success');
    }
} elseif ($action == 'deletetime') {
    $edid = param('toid');
    db_delete("timeconsole", array('id' => $edid));
    message(0, 'success');
} elseif ($action == 'djs') {
    $list = db_find("timeconsole", array('time_type' => 1), array('order' => '-1'), 1, 50);
    include _include($pluginPath . "/admin/djs.htm");
} elseif ($action == 'adddjs') {
    if ($method == 'GET') {
        $edid = param('edid');
        if ($edid) {
            $val = db_find_one("timeconsole", array('id' => $edid, 'time_type' => 1));
        } else {
            $val = ['id' => '0', 'dateline' => '', 'type' => 0, 'order' => 0, 'console_name' => ''];
        }
        include _include($pluginPath . "/admin/adddjs.htm");
    } else {
        $updata['console_name'] = param('console_name', '未知');
        $updata['time_type'] = 1;
        $updata['order'] = param('order');
        $updata['links'] = param('links', '');
        $updata['dateline'] = strtotime(param('dateline'));
        $edid = param('edid', 0);
        if ($edid) {
            db_update("timeconsole", array('id' => $edid), $updata);
        } else {
            db_insert("timeconsole", $updata);
        }
        message(0, 'success');
    }
}
function type($t)
{
    if ($t == 0) {
        return "+";
    } else {
        return "-";
    }
}

function nowTime($val)
{
    $sc = $val['time_offset'];
    $t = $val['type'];
    $nowTime = time();
    if ($val['time_mis'] > 0) {
        $sc = $val['time_mis'];
    } else {
        $sc = $sc * 3600;
    }

    if ($t) {
        $newTime = $nowTime - $sc;
    } else {
        $newTime = $nowTime + $sc;
    }
    return date('Y-m-d H:i:s', $newTime);
}

?>