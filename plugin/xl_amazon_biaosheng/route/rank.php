<?php
$header['title'] = '美亚飙升榜选品工具 - AMZ123跨境导航';
$cateList = db_find("ama_biaosheng_cate", array('fup' => 0), array(), 1, 5000);

$fup = param(1, 0);


$price = param(2, 0);


$star = param(3, 0);

$replycount = param(4, 0);

$biaosheng = param(5, 0);

$xiaoshou = param(6, 0);

$keywords = urldecode(param(7, ''));

$order = param("order", '1');
//获取最后一条时间
$lasttime = date('Y-m-d H:i', db_find_one('amazon_biaosheng_data', array(), array('id' => '-1'))['dateline']);
if ($fup) {
    $fup1info = db_find_one("ama_biaosheng_cate", array('id' => $fup));
    $map['fup'] = $fup;
}

if ($price) {
    $f_price = db_find_one("ama_biaosheng_filter", array('id' => $price));
    $map['allprice'] = array('>' => $f_price['end'], '<' => $f_price['star']);
}


if ($star) {
    $f_star = db_find_one("ama_biaosheng_filter", array('id' => $star));
    $map['star'] = array('>' => $f_star['end'], '<' => $f_star['star']);
}


if ($replycount) {
    $f_replycount = db_find_one("ama_biaosheng_filter", array('id' => $replycount));
    $map['reply_count'] = array('>' => $f_replycount['end'], '<' => $f_replycount['star']);
}

if ($biaosheng) {
    $f_biaosheng = db_find_one("ama_biaosheng_filter", array('id' => $biaosheng));
    $map['bs'] = array('>' => $f_biaosheng['end'], '<' => $f_biaosheng['star']);
}

if ($xiaoshou) {
    $f_xiaoshou = db_find_one("ama_biaosheng_filter", array('id' => $xiaoshou));
    $map['xs'] = array('>' => $f_xiaoshou['end'], '<' => $f_xiaoshou['star']);
}
if ($keywords) {
    $map['title'] = array('LIKE' => $keywords);
}
$page = param(8, 1);
$pagesize = 100;
$c = db_count('amazon_biaosheng_data', $map);

//排序
switch ($order) {
    case 1:
        //1默认
        $orderby = array();
        break;
    case 2:
        //价格倒叙
        $orderby = array('allprice' => '-1');
        break;
    case 3:
        //价格升叙
        $orderby = array('allprice' => '1');
        break;
    case 4:
        //评分倒叙
        $orderby = array('star' => '-1');
        break;
    case 5:
        //评分升叙
        $orderby = array('star' => '1');
        break;
    case 6:
        //评论数倒叙
        $orderby = array('reply_count' => '-1');
        break;
    case 7:
        //评论数倒叙
        $orderby = array('reply_count' => '1');
        break;
    case 8:
        //评论数倒叙
        $orderby = array('bs' => '-1');
        break;
    case 9:
        //评论数倒叙
        $orderby = array('bs' => '1');
        break;
    case 10:
        //评论数倒叙
        $orderby = array('xs' => '-1');
        break;
    case 11:
        //评论数倒叙
        $orderby = array('xs' => '1');
        break;

}
$orderby['id'] = '-1';
$map['rank'] = array('<=' => 100);
$bigdata = db_find('amazon_biaosheng_data', $map, $orderby, $page, $pagesize);

$count = count($bigdatas);

$pagination = pagination(url("moversshakers-" . $fup . "-" . $price . "-" . $star . "-" . $replycount . "-".$biaosheng."-".$xiaoshou."-" . $keywords . "-{page}",array('order'=>$order)), $c, $page, $pagesize);
//其他
$fiter1 = db_find("ama_biaosheng_filter", array('type' => 1));
$fiter2 = db_find("ama_biaosheng_filter", array('type' => 2));
$fiter3 = db_find("ama_biaosheng_filter", array('type' => 3));
$fiter4 = db_find("ama_biaosheng_filter", array('type' => 4));
$fiter5 = db_find("ama_biaosheng_filter", array('type' => 5));

if (check_wap()) {
    include _include(APP_PATH . 'plugin/xl_amazon_biaosheng/view/list_5g.html');
} else {
    include _include(APP_PATH . 'plugin/xl_amazon_biaosheng/view/list.html');
}
