<?php

$password = "123123/as";
if (empty($_POST['__sign']) || md5("{$password}shenjianshou.cn") != trim($_POST['__sign'])) {
    echo json_encode([
        "result" => 2,
        "reason" => "发布失败, 错误原因: 发布密码验证失败!"
    ]);
    exit;
}
$now_cate = param('now_cate');
$now_cate = str_replace("amp;", "", $now_cate);
$datas = json_decode($_POST['list'], true);
//创建分类
//先创建1级分类
$cate1_info = db_find_one("ama_biaosheng_cate", array('cate_name' => $now_cate));
if (!$cate1_info['id']) {
    $cate1_info['id'] = db_insert("ama_biaosheng_cate", array('fup' => 0, 'cate_name' => $now_cate));
}

//分解数据
//去重

foreach ($datas as $k => $v) {
    if (!$v['asin'] || !isset($v['asin'])) {
        unset($datas[$k]);
    } else {
        $as = explode("/dp/", $v['asin']);
//    print_r($v['asin']?$v['asin']:$v['title']);
//    print_r("\r\n");
        $isid = db_find_one('amazon_biaosheng_data', array('asin' => explode("/ref", $as[1])[0]));
        if (!$isid['id']) {
            $ary['title'] = $v['title'];
            $ary['reply_count'] = $v['reply_count'];
            $ary['allprice'] = str_replace("$", "", $v['allprice']);
            $ary['star'] = str_replace(" out of 5 stars", "", $v['star']);
            $ary['img'] = $v['img'];
            $as = explode("/dp/", $v['asin']);
            $ary['asin'] = explode("/ref", $as[1])[0];
            $ary['dateline'] = time();
            $ary['fup'] = $cate1_info['id'];

            $ary['bs'] = intval(str_replace(",","",str_replace("%", "", $v['biaosheng'])));
            //Sales rank: 75 (was 109)
            $str = $v['xiaoshou'];
            if ($str) {
                $str = explode("Sales rank: ", $str);
                $str = explode(" (was ", $str[1]);
            }
            $xs = isset($str[0]) ? $str[0] : 0;
            if (isset($str[1])) {
                $ls = str_replace(")", "", $str[1]);
            }
            $ary['xs'] = intval($xs);
            $ary['ls'] = intval(isset($ls) ? str_replace(",","",$ls) : 0);
            $ary['rank'] = intval(str_replace("#", "", $v['rank']));
            //查询分类顶替数据
            $isHave = db_find_one('amazon_biaosheng_data', array('fup' => $cate1_info['id'], 'rank' => $ary['rank']));
            if ($isHave['id']) {
                db_update("amazon_biaosheng_data", array('id'=>$isHave['id']), $ary);
            } else {
                db_insert('amazon_biaosheng_data', $ary);
            }
        } else {
            $ary['title'] = $v['title'];
            $ary['reply_count'] = $v['reply_count'];
            $ary['allprice'] = str_replace("$", "", $v['allprice']);
            $ary['star'] = str_replace(" out of 5 stars", "", $v['star']);
            $ary['img'] = $v['img'];
            $as = explode("/dp/", $v['asin']);
            $ary['asin'] = explode("/ref", $as[1])[0];
            $ary['dateline'] = time();
            $ary['fup'] = $cate1_info['id'];

            $ary['bs'] = intval(str_replace(",","",str_replace("%", "", $v['biaosheng'])));
            //Sales rank: 75 (was 109)
            $str = $v['xiaoshou'];
            if ($str) {
                $str = explode("Sales rank: ", $str);
                $str = explode(" (was ", $str[1]);
            }
            $xs = isset($str[0]) ? $str[0] : 0;
            if (isset($str[1])) {
                $ls = str_replace(")", "", $str[1]);
            }
            $ary['xs'] = intval($xs);
            $ary['ls'] = intval(isset($ls) ? str_replace(",","",$ls) : 0);
            $ary['rank'] = intval(str_replace("#", "", $v['rank']));
            db_update("amazon_biaosheng_data", array('asin' => $isid['asin']), $ary);
        }
    }
}
echo json_encode([
    "result" => 1,
    "reason" => "发布成功"
]);
exit;