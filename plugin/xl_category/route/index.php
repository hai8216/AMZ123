<?php
$cid = param('1');
if($cid){
    $data = db_find_one("category",array('cid'=>$cid));
    $header['title'] = $data['seo_title']?$data['seo_title']:$data['cname'];
    $header['keywords'] = $data['seo_keywords'];
    $header['description'] = $data['seo_desc'];
    $page = param(2, 1);
    $pagesize = 45;
    $start = ($page - 1) * $pagesize;
    $c = db_count("category_page",array('cate'=>$cid));
    $pagination = pagination(url("category-".$cid."-{page}"), $c, $page, $pagesize);
    $datalist = db_find("category_page",array('cate'=>$cid),array(),$page,$pagesize);
    include _include(APP_PATH . 'plugin/xl_category/view/index.html');
}else{
    include _include(APP_PATH . 'plugin/xl_category/view/search.html');
}
