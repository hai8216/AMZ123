<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$action = param('3') ?: 'list';
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 45;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("category", array(), array(), $page, $pagesize);
    $c = db_count("category");
    $pagination = pagination(url("plugin-setting-xl_category-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_category/view/list.html');
} elseif ($action == 'products') {
    $page = param(4, 1);
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("category_page", array(), array(), $page, $pagesize);
    $c = db_count("category_page");
    $pagination = pagination(url("plugin-setting-xl_category-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_category/view/plist.html');
} elseif ($action == 'ad') {
    if ($method == 'GET') {
        if(param('cid')){
            $data = db_find_one("category",array('cid'=>param('cid')));
        }
        $arrlist = db_find("category", array(), array(), 1, 200);
        foreach($arrlist as $v){
            $sary[$v['cid']]=$v['cname'];
        }
        $input['cate'] = form_select('cate',$sary,$data['cid']);
        $input['code'] = form_text('code','');
        $input['name'] = form_text('name','');
        $input['ename'] = form_text('ename','');
        $input['bz'] = form_text('bz','');
        include _include(APP_PATH . 'plugin/xl_category/view/ad.html');
    } else {
        $insert['cate'] = param('cate');
        $insert['code'] = param('code');
        $insert['name'] = param('name');
        $insert['ename'] = param('ename');
        $insert['bz'] = param('bz');
        db_insert("category_page",$insert);
        message(0,jump('操作成功',url('plugin-setting-xl_category-view',array('cid'=>param('cid')))));
    }
}elseif($action=='view'){
    $page = param(4, 1);
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("category_page", array('cate'=>param('cid')), array(), $page, $pagesize);
    $c = db_count("category_page");
    $pagination = pagination(url("plugin-setting-xl_category-view-{page}",array('cate'=>param('cid'))), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_category/view/vlist.html');
}elseif($action=='ed'){
    if ($method == 'GET') {
        $data = db_find_one("category",array('cid'=>param('cid')));
        $input['cname'] = form_text('cname',$data['cname']);
        $input['cdesc'] = form_textarea('cdesc',$data['cdesc']);

        $input['simple_desc'] = form_text('simple_desc',$data['simple_desc']);
        $input['seo_title'] = form_text('seo_title',$data['seo_title']);
        $input['seo_keywords'] = form_text('seo_keywords',$data['seo_keywords']);
        $input['seo_desc'] = form_textarea('seo_desc',$data['seo_desc']);
        include _include(APP_PATH . 'plugin/xl_category/view/eds.html');
    } else {
        $insert['cname'] = param('cname');
        $insert['cdesc'] = param('cdesc');

        $insert['simple_desc'] = param('simple_desc');
        $insert['seo_title'] = param('seo_title');
        $insert['seo_keywords'] = param('seo_keywords');
        $insert['seo_desc'] = param('seo_desc');

        db_update("category",array('cid'=>param('cid')),$insert);
        message(0,jump('操作成功',url('plugin-setting-xl_category')));
    }
}elseif($action=='eddata'){
    if ($method == 'GET') {
        if(param('id')){
            $data = db_find_one("category_page",array('id'=>param('id')));
        }else{
            message(-1,'1');
        }
        $arrlist = db_find("category", array(), array(), 1, 200);
        foreach($arrlist as $v){
            $sary[$v['cid']]=$v['cname'];
        }
        $input['cate'] = form_select('cate',$sary,$data['cate']);
        $input['code'] = form_text('code',$data['code']);
        $input['name'] = form_text('name',$data['name']);
        $input['ename'] = form_text('ename',$data['ename']);
        $input['bz'] = form_text('bz',$data['bz']);
        include _include(APP_PATH . 'plugin/xl_category/view/ed.html');
    } else {
        $insert['cate'] = param('cate');
        $insert['code'] = param('code');
        $insert['name'] = param('name');
        $insert['ename'] = param('ename');
        $insert['bz'] = param('bz');
        db_update("category_page",array('id'=>param('id')),$insert);
        message(0,jump('操作成功',url('plugin-setting-xl_category-view',array('cid'=>param('cate')))));
    }
}elseif($action=='del'){
    $data = db_find_one("category_page",array('id'=>param('id')));
    db_delete("category_page",array('id'=>param('id')));
    message(0,jump('操作成功',url('plugin-setting-xl_category-view',array('cid'=>$data['cate']))));
}
?>