?><?php
$ss_user_oauth = kv_get('ss_user_oauth');
$ss_oauth_list = array();
if(!empty($ss_user_oauth['api'])) {
	$apis = explode(',', $ss_user_oauth['api']);
	foreach($apis as $_api) {
		$ss_oauth_list[$_api] = $ss_user_oauth['setting'][$_api];
	}
};
if(isset($ss_oauth_list['wxlogin']) && $route != 'oauth' && $route != 'user' && $ss_oauth_list['wxlogin']['status'] && strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false && empty($user)) {
	$referer = array_value($_SERVER, 'REQUEST_URI', '');
	$referer = str_replace(array('\"', '"', '<', '>', ' ', '*', "\t", "\r", "\n"), '', $referer); // 干掉特殊字符 strip special chars
	if(
		strpos($referer, 'user-login.htm') !== FALSE 
		|| strpos($referer, 'user-logout.htm') !== FALSE 
		|| strpos($referer, 'user-create.htm') !== FALSE 
		|| strpos($referer, 'user-setpw.htm') !== FALSE 
		|| strpos($referer, 'user-resetpw_complete.htm') !== FALSE
	) {
		$referer = '';
	}
	//入口referer保存
    $_SESSION['wx_referer'] = $referer;
	http_location(url('oauth-wxlogin-login-key-wapautologin',array('referer'=>$referer)));
}