<?php

/*
	Xiuno BBS 4 插件：OAuth协议集成器
	BY:倚楼观天象(沈俊阳)
*/

!defined('DEBUG') AND exit('Forbidden');

$tablepre = $db->tablepre;
$sql = "CREATE TABLE IF NOT EXISTS {$tablepre}ss_user_oauth (
	id int(11) unsigned NOT NULL auto_increment COMMENT '主键',
	uid int(11) unsigned NOT NULL COMMENT '用户编号',
	platid CHAR( 20 ) NOT NULL DEFAULT '' COMMENT '平台标识',
	openid char(40) NOT NULL DEFAULT '' COMMENT '第三方唯一身份标识',
	PRIMARY KEY (id),
	KEY openid_platid (platid,openid)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";

$r = db_exec($sql);
$r === FALSE AND message(-1, '创建表结构失败'); // 中断，安装失败。

// 初始化
$kv = kv_get('ss_user_oauth');
if(!$kv) {
	$kv = array('meta'=>'', 'bind'=>'1', 'api'=>'qq,wechat,taobao,baidu,sina,netease,wxlogin', 'setting'=>'');
	$apis = explode(',', $kv['api']);
	foreach($apis as $api) {
		(!isset($kv['setting'][$api]) || !is_array($kv['setting'][$api])) && $kv['setting'][$api] = array('appid'=>'', 'appkey'=>'', 'status'=>0);
	}
	kv_set('ss_user_oauth', $kv);
}

?>