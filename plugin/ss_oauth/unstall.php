<?php

/*
	Xiuno BBS 4.0 插件：OAuth协议集成器
	BY:倚楼观天象(沈俊阳)
*/

!defined('DEBUG') AND exit('Forbidden');

$tablepre = $db->tablepre;
$sql = "DROP TABLE IF EXISTS {$tablepre}ss_user_oauth;";

$r = db_exec($sql);

kv_delete('ss_user_oauth');
$r === FALSE AND message(-1, '卸载表失败');

?>