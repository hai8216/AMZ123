<?php
/*
	Xiuno BBS 4.0 插件：OAuth协议集成器
	BY:倚楼观天象(沈俊阳)
*/
!defined('DEBUG') AND exit('Access Denied.');
// 写入记录
ss_oauth_token_set($username, $avatar, $api, $openid);
// 跳转新页面处理
http_location(url('oauth-bind'));