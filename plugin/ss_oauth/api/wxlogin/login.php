<?php
!defined('DEBUG') AND exit('Access Denied.');
require 'init.php';

$func = param(3,'index');

if($func == 'pc') {
	$cache_name = 'ss_oauth_'.$uniqid;
	$token = kv_get($cache_name);
	$code = 0;
	$message = url('oauth-bind');
	if(!empty($token)) {
		$token = json_decode(base64_decode($token),true);
		
		if($token['bind'] == 0) {
			ss_oauth_token_set($token['username'], $token['avatar'], $token['api'], $token['openid']);
		} else {
			$uid = $token['uid'];
			$_SESSION['uid'] = $uid;
			user_token_set($uid);
			$message = '/';
		}
		kv_delete($cache_name);
		$_SESSION['ss_oauth_key'] = '';
		message($code, $message);
	} else {
		message(1, url('my'));
	}
} else if($func == 'key') {
	$uniqid = param(4);
    empty($uniqid) AND message(-1,'错误的请求');
	
	$ss_oauth_getk = array();
	$ss_oauth_getk['key'] = $uniqid;
	
	// 微信登录
	$referer = param('referer');
	if(!empty($referer)) {
		$ss_oauth_getk['referer'] = $referer;
	}
	$back_url = http_url_path().url('oauth-'.$api.'-back',$ss_oauth_getk);
	
	http_location(WX_CONNECT_URL.'?appid=' . WX_ID . '&redirect_uri=' . urlencode($back_url) . '&response_type=code&scope=snsapi_userinfo&state=wxlogin#wechat_redirect');
}

if($func != 'index') exit();

if(empty($uniqid)) {
	$uniqid = ss_get_uniqid();
	$_SESSION['ss_oauth_key'] = $uniqid;
}

$keyurl = $siteurl.url('oauth-wxlogin-login-key-'.$uniqid);
?>
<?php include _include(APP_PATH.'view/htm/header.inc.htm');?>

<div class="row">
	<div class="col-lg-6 mx-auto">
		<div class="card">
			<div class="card-header">
				微信扫一扫				
			</div>
			<div class="card-body ajax_modal_body">
				<div id="xcode" class="text-center"></div>
			</div>
		</div>
		
	</div>
</div>

<?php include _include(APP_PATH.'view/htm/footer.inc.htm');?>

<script src='./plugin/ss_oauth/api/wxlogin/jquery.qrcode.min.js'></script>
<script>
    $('#xcode').qrcode('<?php echo $keyurl;?>');
    setInterval(function () {
		$.xget('<?php echo url('oauth-wxlogin-login-pc')?>',function(code, message){
			if(code==0) {
				window.location.href = message;
			} else if(code==1){
//                window.location.href = message;
			} else {
				$('#xcode').html("");
				$('#xcode').qrcode(message);
			}
		});
    },1500);
</script>