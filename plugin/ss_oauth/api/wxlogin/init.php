<?php
!defined('DEBUG') AND exit('Access Denied.');

define('WX_ID', $_api['appid']);
define('WX_SECRET', $_api['appkey']);
define('WX_CONNECT_URL', 'https://open.weixin.qq.com/connect/oauth2/authorize');
define('WX_TOKEN_URL', 'https://api.weixin.qq.com/sns/oauth2/access_token');
define('WX_USERINFO_URL', 'https://api.weixin.qq.com/sns/userinfo');

$siteurl = http_url_path();

function ss_get_uniqid() {
	return uniqid(md5(microtime(true)),true);
}

$uniqid = _SESSION('ss_oauth_key');
?>