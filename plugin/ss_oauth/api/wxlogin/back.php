<?php
!defined('DEBUG') AND exit('Access Denied.');
require 'init.php';

$code = param('code');
!empty($code) or xn_message(-1, 'Error Request.(-1)');

$state = param('state');
if($state != 'wxlogin') {xn_message(-1, 'Error Request.(-2)');}

$uniqid = param('key');
empty($uniqid) AND message(-1,'错误的请求');

$par = '?grant_type=authorization_code'
	 . '&code='.$code
	 . '&appid='.WX_ID
	 . '&secret='.WX_SECRET;
$rec = https_get(WX_TOKEN_URL.$par);

if(strpos($rec, 'access_token') !== false) {
	$arr = json_decode($rec, true);
	$_token = $arr['access_token'];
	$openid = $arr['openid'];
	
	$_user = ss_oauth_read_user($api, $openid);
	$ss_token = array();
	if($_user) {
		// 已绑定账号
		$ss_token = array('bind'=>1, 'uid'=>$_user['uid']);
	} else {
		$par = "?access_token={$_token}&openid={$openid}";
		$rec = https_get(WX_USERINFO_URL.$par);
		if(strpos($rec, 'nickname') !== false) {
			$arr = json_decode($rec, true);
			$ss_token = array('bind'=>0, 'username'=>$arr['nickname'], 'avatar'=>$arr['headimgurl'], 'api'=>$api, 'openid'=>$openid);
		}
	}
	
	if($uniqid == 'wapautologin') {
		$referer = param('referer');
		if(!empty($referer)) {
			$_SESSION['ss_oauth_referer'] = $referer;
		}
		$message = url('oauth-bind');
		if($ss_token['bind'] == 0) {
			ss_oauth_token_set($ss_token['username'], $ss_token['avatar'], $ss_token['api'], $ss_token['openid']);
		} else {
			ss_oauth_login_user($ss_token['uid']);
		}
		http_location($message);
	} else {
		$token = base64_encode(json_encode($ss_token));
		kv_set('ss_oauth_'.$uniqid, $token);
		message(0, '登陆成功');
	}
	
} else {
	// 防止微信版按后退
	if($uniqid == 'wapautologin' && $user) {
		http_location('./');
	}
	xn_message(0, 'Error Request.(-3)');
}
?>