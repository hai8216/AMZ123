<?php
!defined('DEBUG') AND exit('Access Denied.');
require 'init.php';

$code = param('code');
!empty($code) or xn_message(-1, 'Error Request.(-1)');

$par = '?grant_type=authorization_code'
	 . '&client_id='.QQ_ID
	 . '&client_secret='.QQ_SECRET
	 . '&code='.$code
	 . '&redirect_uri='.urlencode($back_url);
$rec = https_get(QQ_TOKEN_URL.$par);

if(strpos($rec, 'access_token') !== false) {
	parse_str($rec, $arr);
	if($arr['access_token']) {
		$_token = $arr['access_token'];
		$par = '?access_token='.$_token;
		$rec = https_get(QQ_ME_URL.$par);
		if(strpos($rec, 'client_id') !== false) {
			$rec = str_replace('callback(', '', $rec);
			$rec = str_replace(');', '', $rec);
			$rec = trim($rec);
			$arr = json_decode($rec, true);
			$openid = $arr['openid'];
			
			$_user = ss_oauth_read_user($api, $openid);
			if($_user) {
				// 已绑定账号
				ss_oauth_login_user($_user['uid']);
			} else {
				$par = '?access_token='.$_token.'&oauth_consumer_key='.QQ_ID.'&openid='.$openid;
				$rec = https_get(QQ_USERINFO_URL.$par);
				if(strpos($rec, 'nickname') !== false) {
					$arr = json_decode($rec, true);
					$username = $arr['nickname'];
					$avatar = !empty($arr['figureurl_qq_2']) ? $arr['figureurl_qq_2'] : $arr['figureurl_qq_1'];
					include APP_PATH.'plugin/ss_oauth/bind.php';
				}
			}
		}
	}
} else {
	xn_message(0, 'Error Request.(-3)');
}