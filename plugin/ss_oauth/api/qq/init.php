<?php
!defined('DEBUG') AND exit('Access Denied.');

define('QQ_ID', $_api['appid']);
define('QQ_SECRET', $_api['appkey']);
define('QQ_CONNECT_URL', 'https://graph.qq.com/oauth2.0/authorize');
define('QQ_TOKEN_URL', 'https://graph.qq.com/oauth2.0/token');
define('QQ_ME_URL', 'https://graph.qq.com/oauth2.0/me');
define('QQ_USERINFO_URL', 'https://graph.qq.com/user/get_user_info');
?>