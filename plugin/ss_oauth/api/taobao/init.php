<?php
!defined('DEBUG') AND exit('Access Denied.');

define('TB_ID', $_api['appid']);
define('TB_SECRET', $_api['appkey']);
define('TB_CONNECT_URL', 'https://oauth.taobao.com/authorize');
define('TB_TOKEN_URL', 'https://oauth.taobao.com/token');
?>