<?php
!defined('DEBUG') AND exit('Access Denied.');
require 'init.php';

$code = param('code');
!empty($code) or xn_message(-1, 'Error Request.(-1)');

$par = '?grant_type=authorization_code'
	 . '&code='.$code
	 . '&client_id='.TB_ID
	 . '&client_secret='.TB_SECRET
	 . '&redirect_uri='.urlencode($back_url);
$rec = https_get(TB_TOKEN_URL.$par);
if(strpos($rec, 'access_token') !== false) {
	$arr = json_decode($rec, true);
	$_token = $arr['access_token'];
	$openid = $arr['taobao_user_id'];
	
	$_user = ss_oauth_read_user($api, $openid);
	if($_user) {
		// 已绑定账号
		ss_oauth_login_user($_user['uid']);
	} else {
		$username = $arr['taobao_user_nick'];
		$avatar = '';
		include APP_PATH.'plugin/ss_oauth/bind.php';
	}
} else {
	xn_message(0, 'Error Request.(-3)');
}
?>