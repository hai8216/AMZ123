<?php
!defined('DEBUG') AND exit('Access Denied.');
require 'init.php';

$code = param('code');
!empty($code) or xn_message(-1, 'Error Request.(-1)');

$o = new SaeTOAuthV2(WB_AKEY, WB_SKEY);
$token = '';
$keys = array();
$keys['code'] = $code;
$keys['redirect_uri'] = $back_url;
try {
	$token = $o->getAccessToken('code', $keys);
} catch (OAuthException $e) {
	
}

if($token) {
	$c = new SaeTClientV2(WB_AKEY, WB_SKEY, $token['access_token']);
	$me = $c->show_user_by_id($token['uid']);
	if(isset($me['error'])) message(-1, 'API Error:'.$me['error']);
	if($me && isset($me['screen_name'])) {
		$openid = $me['id'];
		
		$_user = ss_oauth_read_user($api, $openid);
		if($_user) {
			// 已绑定账号
			ss_oauth_login_user($_user['uid']);
		} else {
			$username = $me['screen_name'];
			$avatar = $me['profile_image_url'];
			include APP_PATH.'plugin/ss_oauth/bind.php';
		}
	}
} else {
	xn_message(0, 'Error Request.(-3)');
}
?>