<?php
!defined('DEBUG') AND exit('Access Denied.');
require 'init.php';

$code = param('code');
!empty($code) or xn_message(-1, 'Error Request.(-1)');

$par = '?grant_type=authorization_code'
	 . '&code='.$code
	 . '&appid='.WX_ID
	 . '&secret='.WX_SECRET;
$rec = https_get(WX_TOKEN_URL.$par);
if(strpos($rec, 'access_token') !== false) {
	$arr = json_decode($rec, true);
	$_token = $arr['access_token'];
	$openid = $arr['openid'];
	
	$_user = ss_oauth_read_user($api, $openid);
	if($_user) {
		// 已绑定账号
		ss_oauth_login_user($_user['uid']);
	} else {
		$par = "?access_token={$_token}&openid={$openid}";
		$rec = https_get(WX_USERINFO_URL.$par);
		if(strpos($rec, 'nickname') !== false) {
			$arr = json_decode($rec, true);
			$username = $arr['nickname'];
			$avatar = $arr['headimgurl'];
			include APP_PATH.'plugin/ss_oauth/bind.php';
		}
	}
} else {
	xn_message(0, 'Error Request.(-3)');
}
?>