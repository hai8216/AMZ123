<?php
!defined('DEBUG') AND exit('Access Denied.');
require 'init.php';
?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" /> 
    <title>微信登录</title>
	<style>
	* {word-break:break-all;font-family:"Segoe UI","Lucida Grande",Helvetica,Arial,Verdana,"Microsoft YaHei";}
	body {margin:0;font-size:14px;color:#333333;background:#EFEFF4;-webkit-user-select:none;}
	</style>
</head>
<body>
	<div style="width:100%;text-align:center;padding-top:30px;">
		<div id="weixin_qrcode"></div>
		<div style="padding:16px;font-size:16px;color:#999999;">
		<a href="<?php echo http_referer();?>" style="color:#2E7DC6;text-decoration:none;">取消并返回</a>
		</div>
	</div>
	<script src="https://res.wx.qq.com/connect/zh_CN/htmledition/js/wxLogin.js"></script>
	<script type="text/javascript">
	var obj = new WxLogin({
		id:"weixin_qrcode", 
		appid: "<?php echo WX_ID;?>", 
		scope: "snsapi_login", 
		redirect_uri: "<?php echo urlencode($back_url);?>",
		state: "",
		style: "",
		href: ""
	});
	</script>
</body>
</html>