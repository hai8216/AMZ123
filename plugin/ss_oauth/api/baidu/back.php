<?php
!defined('DEBUG') AND exit('Access Denied.');
require 'init.php';

$code = param('code');
!empty($code) or xn_message(-1, 'Error Request.(-1)');

$par = '?grant_type=authorization_code'
	 . '&code='.$_REQUEST['code']
	 . '&client_id='.BD_ID
	 . '&client_secret='.BD_SECRET
	 . '&redirect_uri='.urlencode($back_url);
$rec = https_get(BD_TOKEN_URL.$par);
if(strpos($rec, 'access_token') !== false) {
	$arr = json_decode($rec, true);
	$_token = $arr['access_token'];
	$par = '?access_token='.$_token;
	$rec = https_get(BD_USERINFO_URL.$par);
	if(strpos($rec, 'uname') !== false) {
		$arr = json_decode($rec, true);
		$openid = $arr['uid'];
		
		$_user = ss_oauth_read_user($api, $openid);
		if($_user){
			// 已绑定账号
			ss_oauth_login_user($_user['uid']);
		}else{
			$username = $arr['uname'];
			$avatar = '';
			include APP_PATH.'plugin/ss_oauth/bind.php';
		}
	}
} else {
	xn_message(0, 'Error Request.(-3)');
}
?>