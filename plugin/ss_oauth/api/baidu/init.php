<?php
!defined('DEBUG') AND exit('Access Denied.');

define('BD_ID', $_api['appid']);
define('BD_SECRET', $_api['appkey']);
define('BD_CONNECT_URL', 'https://openapi.baidu.com/oauth/2.0/authorize');
define('BD_TOKEN_URL', 'https://openapi.baidu.com/oauth/2.0/token');
define('BD_USERINFO_URL', 'https://openapi.baidu.com/rest/2.0/passport/users/getLoggedInUser');
?>