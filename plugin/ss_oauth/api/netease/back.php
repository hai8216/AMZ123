<?php
!defined('DEBUG') AND exit('Access Denied.');
require 'init.php';

$code = param('code');
!empty($code) or xn_message(-1, 'Error Request.(-1)');

$par = '?grant_type=authorization_code'
	 . '&code='.$_REQUEST['code']
	 . '&client_id='.NE_ID
	 . '&client_secret='.NE_SECRET
	 . '&redirect_uri='.urlencode($back_url);
$rec = https_get(NE_TOKEN_URL.$par);
if(strpos($rec, 'access_token') !== false) {
	$arr = json_decode($rec, true);
	$_token = $arr['access_token'];
	$url = NE_USERINFO_URL.'?access_token='.$_token;
	$rec = https_get($url);
	if(strpos($rec, 'userId') !== false) {
		$arr = json_decode($rec, true);
		$openid = $arr['userId'];
		
		$_user = ss_oauth_read_user($api, $openid);
		if($_user) {
			// 已绑定账号
			ss_oauth_login_user($_user['uid']);
		} else {
			$backname = isset($arr['username']) ? $arr['username'] : $openid;
			$avatar = '';
			include APP_PATH.'plugin/ss_oauth/bind.php';
		}
	}
} else {
	xn_message(0, 'Error Request.(-3)');
}
?>