<?php
!defined('DEBUG') AND exit('Access Denied.');

define('NE_ID', $_api['appid']);
define('NE_SECRET', $_api['appkey']);
define('NE_CONNECT_URL', 'http://reg.163.com/open/oauth2/authorize.do');
define('NE_TOKEN_URL', 'http://reg.163.com/open/oauth2/token.do');
define('NE_USERINFO_URL', 'https://reg.163.com/open/oauth2/getUserInfo.do');
?>