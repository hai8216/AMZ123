<?php
function ss_oauth_read_user($platid, $openid) {
	$sql = array('platid' => $platid, 'openid' => $openid);
	$arr = db_find_one('ss_user_oauth', $sql);
	if($arr) {
		$arr2 = user_read($arr['uid']);
		if($arr2) {
			$arr = array_merge($arr, $arr2);
		} else {
			db_delete('ss_user_oauth', $sql);
			return FALSE;
		}
	}
	return $arr;
}

function ss_oauth_filter_username($str) {
    $str = preg_replace_callback(
        '/./u',
        function (array $match) {
            return strlen($match[0]) >= 4 ? '' : $match[0];
        },
        $str);
    return $str;
}

function ss_oauth_create_user($username, $avatar, $platid, $openid) {
	global $conf, $time, $longip;
	
	$arr = ss_oauth_read_user($platid, $openid);
	if($arr) return message(-2, '已经注册');
	
	$username = ss_oauth_filter_username($username);
	
	// 自动产生一个用户名
	$r = user_read_by_username($username);
	if($r) {
		// 特殊字符过滤
		$username = xn_substr($username.'_'.$time, 0, 31);
		$r = user_read_by_username($username);
		if($r) return message(-1, '用户名被占用。');
	}
	// 自动产生一个 Email
	$email = "qq_$time@qq.com";
	$r = user_read_by_email($email);
	if($r) return message(-1, 'Email 被占用');
	// 随机密码
	$password = md5(rand(1000000000, 9999999999).$time);
	$user = array(
		'username'=>$username,
		'email'=>$email,
		'password'=>$password,
		'gid'=>101,
		'salt'=>rand(100000, 999999),
		'create_date'=>$time,
		'create_ip'=>$longip,
		'avatar'=>0,
		'logins' => 1,
		'login_date' => $time,
		'login_ip' => $longip,
	);
	$uid = user_create($user);
	if(empty($uid)) return message(-1, '注册失败');
	
	$user = user_read($uid);
	
	ss_oauth_bind_user($uid, $platid, $openid);
	
	runtime_set('users+', '1');
	runtime_set('todayusers+', '1');
	
	// 头像不重要，忽略错误。
	if($avatar) {
		$filename = "$uid.png";
		$dir = substr(sprintf("%09d", $uid), 0, 3).'/';
		$path = $conf['upload_path'].'avatar/'.$dir;
		!is_dir($path) AND mkdir($path, 0777, TRUE);
		
		//$data = http_get($avatar);
		$data = http_multi_get(array($avatar));
		if($data[0]) {
			file_put_contents($path.$filename, $data[0]);
			user_update($uid, array('avatar' => $time));
		}
	}
	return $user;
	
}

function ss_oauth_bind_user($uid, $platid, $openid){
	$r = db_insert('ss_user_oauth', array('uid' => $uid, 'platid' => $platid, 'openid' => $openid));
	if(!$r) return xn_error(-1, '绑定失败');
}

function ss_oauth_login_user($uid, $url='/'){
	$_SESSION['uid'] = $uid;
	user_token_set($uid);
	if(isset($_SESSION['ss_oauth_referer']) && !empty($_SESSION['ss_oauth_referer']) && $url == '/') {
		$url = $_SESSION['ss_oauth_referer'];
	}
	message(0, jump('登陆成功', $url, 2));
}

function ss_oauth_token_set($username, $avatar, $platid, $openid){
	global $time, $useragent, $conf;
	$tokenkey = md5($conf['auth_key']);
	$token = xn_encrypt("$username@_@$avatar@_@$platid@_@$openid", $tokenkey);
	setcookie('bbs_ss_oauth_token', $token, $time + 8640000, '');
}

function ss_oauth_token_get(){
	global $time, $ip, $useragent, $conf;
	$token = param('bbs_ss_oauth_token');
	if(empty($token)) return FALSE;
	$tokenkey = md5($conf['auth_key']);
	$s = xn_decrypt($token, $tokenkey);
	if(empty($s)) return FALSE;
	$arr = explode("@_@", $s);
	if(count($arr) != 4) return FALSE;
	return $arr;
}

function ss_oauth_token_clear() {
	global $time;
	setcookie('bbs_ss_oauth_token', '', $time - 8640000, '');
}
?>