<?php

/*
	Xiuno BBS 4.0 插件：OAuth协议集成器
	BY:倚楼观天象(沈俊阳)
*/

!defined('DEBUG') AND exit('Access Denied.');
include APP_PATH . 'plugin/ss_oauth/model/oauth.func.php';

$api = param(1);
if ($api == 'bind') {
    $arr = ss_oauth_token_get();
    $arr or exit('TOKEN 获取失败。');
    list($username, $avatar, $api, $openid) = $arr;
    if ($ss_user_oauth['bind'] == 0) {
        $method = 'POST';
        $_REQUEST['mode'] = 5;
    }
    if ($method == 'POST') {
        $mode = param('mode', 1);
        if ($mode == 1) {
            if ($uid) {
                ss_oauth_bind_user($uid, $api, $openid);
                if ($api == 'wxlogin') {
                    $us = db_count('ss_user_oauth', array('uid' => $uid, 'platid' => $api, 'openid' => $openid));
                    if ($us == 1) {
                        if ($user['xy_regfrom']) {
                            $fuid = base64_decode($user['xy_regfrom']);
                            $invite_setting = kv_get('invite');
                            db_update('user', array('uid' => $fuid), array('credits+' => $invite_setting['invite_givenum']));
                            $smg = $conf['gold_name'] . " +" . $invite_setting['invite_givenum'] . $conf['gold_unit'];
                            $arr = array('uid' => $fuid, 'createtime' => $time, 'type' => 30, 'message' => $smg, 'uip' => $ip);
                            db_create('score_info', $arr);
                        }
                    }
                }

                message(0, lang('save_successfully'));
            } else {
                message(-1, lang('please_login'));
            }
        } elseif ($mode == 4) {
            ss_oauth_token_clear();
            message(0, lang('save_successfully'));
        } elseif ($mode == 5) {
            $user = ss_oauth_create_user($username, $avatar, $api, $openid);
            $uid = $user['uid'];
            ss_oauth_login_user($uid);
        }

    } else {
        $header['title'] = lang('bind');

        include APP_PATH . 'plugin/ss_oauth/htm/bind.htm';
    }

} elseif ($api == 'delwx') {
    $old = db_find_one('wechat_bind', array('uid' => $uid));
    $users = db_find_one("user", array('uid' => $uid));
    $r = db_delete('wechat_bind', array('uid' => $uid));
    if ($r) {
        if ($old['openid']) {
            //发送一个模板消息
            include APP_PATH . "plugin/xl_wechat_union/class/wechat.class.php";
            $wechat = new Wechat_amz123();
            $postdata = array(
                'touser' => $old['openid'],
                'template_id' => 'h3_hppnv4mlhJfAPXX7iRZtqkXRFd8_lGolM8F4Bj0w',
                'url' => 'http://www.amz123.com',
                'data' => array(
                    'first' => array(
                        'value' => '解除绑定成功',
                        'color' => "#173177"
                    ),
                    'keyword1' => array(
                        'value' => $users['username'],
                        'color' => "#173177"
                    ),
                    'keyword2' => array(
                        'value' => "于" . date('Y-m-d H:i') . "解除绑定",
                        'color' => "#173177"
                    ),
                    'remark' => array(
                        'value' => '[  亚马逊卖家之路，从AMZ123开始  ]',
                        'color' => "#173177"
                    )

                ),
            );
            $wechat->sendTpl(json_encode($postdata));
        }
        message(0, jump('解绑成功', url('my')));

    } else {
        message(0, jump('你还未绑定', url('my')));
    }
} elseif ($api == 'delqq') {
    $r = db_delete('ss_user_oauth', array('uid' => $uid, 'platid' => 'qq'));
    if ($r) {
        message(0, jump('解绑成功', url('my')));
    } else {
        message(0, jump('你还未绑定', url('my')));
    }
} else {

    $action = param(2, 'login');
    $action = !in_array($action, array('login', 'back')) ? 'login' : $action;
    //先记录下当前的URL
    //$ss_user_oauth = kv_get('ss_user_oauth');
    $ref = $_GET['referer'];
//    setcookie('wx_ref', $ref,  8640000);
    if (!empty($ss_user_oauth['api'])) {
        $apis = explode(',', $ss_user_oauth['api']);
        if (in_array($api, $apis)) {
            $back_url = http_url_path() . url('oauth-' . $api . '-back');
            $_api = $ss_user_oauth['setting'][$api];
            if ($_api['status'] != 1) {
                message(0, '该接口暂未开放');
            } else {
                include APP_PATH . 'plugin/ss_oauth/api/' . $api . '/' . $action . '.php';
            }
        }
    }

}