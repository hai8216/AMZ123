<?php

/*
	Xiuno BBS 4.0 插件：OAuth协议集成器
	BY:倚楼观天象(沈俊阳)
*/

!defined('DEBUG') AND exit('Access Denied.');

if($method == 'GET') {
	
	$kv = kv_get('ss_user_oauth');
	
	$input = array();
	$input['meta'] = form_text('meta', $kv['meta']);
	$input['api'] = form_text('api', $kv['api']);
	$input['bind'] = form_select('bind', array(0=>'禁用',1=>'启用'), $kv['bind']);
	
	$apis = explode(',', $kv['api']);
	$list = '';
	$back = '';
	$path = http_url_path();
	$siteurl = substr($path, 0, strrpos(substr($path, 0, -1), '/')).'/';
	foreach($apis as $api) {
		(!isset($kv['setting'][$api]) || !is_array($kv['setting'][$api])) && $kv['setting'][$api] = array('appid'=>'', 'appkey'=>'', 'status'=>0);
		$_api = $kv['setting'][$api];
		$list .= '<tr><td><p class="form-control-static">'.lang($api).'</p></td><td>'.form_text('setting['.$api.'][appid]', $_api['appid']).'</td><td>'.form_text('setting['.$api.'][appkey]', $_api['appkey']).'</td><td><div class="input-group">'.str_replace('custom-select','form-control',form_select('setting['.$api.'][status]', array(0=>'禁用',1=>'启用'), $_api['status'])).'</div></td></tr>';
		$back .= "<p class=\"m-a-1 small\">".lang($api)." 回调地址：".$siteurl.url('oauth-'.$api.'-back')."</p>";
	}
	
	include _include(APP_PATH.'plugin/ss_oauth/setting.htm');
	
} else {

	$kv = array();
	$kv['meta'] = param('meta');
	$kv['setting'] = $_POST['setting'];
	$kv['api'] = param('api');
	$kv['bind'] = param('bind');
	
	kv_set('ss_user_oauth', $kv);
	
	message(0, '修改成功');

}

?>