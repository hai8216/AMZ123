<?php
$action = param(3, 'list');
function getTagname($v){
    return db_find_one("thread_tag",array('tag_id'=>$v))['tag_name'];
}
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 20;
    $arrlist = db_find("thread_ad", array(), array('id' => '-1'), $page, $pagesize);
    $c = db_count('thread_ad');
    $pagination = pagination(url("plugin-setting-xl_thread_ad-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_thread_ad/admin/list.htm');
} elseif ($action == 'edittag') {
    if ($method == 'GET') {
        $aid = param('id');
        $taglists = db_find("thread_ad_tag", array('thread_ad_id' => $aid),array(),1,500);
        include _include(APP_PATH . 'plugin/xl_thread_ad/admin/edtag.htm');
    } else {
        db_delete("thread_ad_tag", array('thread_ad_id' => param('aid')));
        foreach ($_POST['taglist'] as $v) {
            db_insert("thread_ad_tag", array('thread_ad_id' => param('aid'), 'tag_id' => $v,'tag_name'=>getTagname($v)));
        }
        message(0, 'success');
    }
} elseif($action=='status'){
    $status = db_find_one("thread_ad",array('id'=>param('aid')));
    if($status['status']=='1'){
        db_update("thread_ad",array('id'=>param('aid')),array('status'=>0));
    }else{
        db_update("thread_ad",array('id'=>param('aid')),array('status'=>1));
    }
    message(0,jump('切换成功',url('plugin-setting-xl_thread_ad')));
}elseif($action=='status2'){
    $status = db_find_one("thread_ad",array('id'=>param('aid')));
    if($status['status']=='2'){
        db_update("thread_ad",array('id'=>param('aid')),array('status'=>0));
    }else{
        db_update("thread_ad",array('id'=>param('aid')),array('status'=>2));
    }
    message(0,jump('切换成功',url('plugin-setting-xl_thread_ad')));
}elseif ($action == 'del') {
    $aid = param('aid');
    db_delete("thread_ad_tag", array('thread_ad_id' => $aid));
    db_delete("thread_ad", array('id' => $aid));
    message(0,jump('成功删除',url('plugin-setting-xl_thread_ad')));
}elseif ($action == 'ajaxtag') {
    $k = param('tag');
    $kdata = db_find('thread_tag', array('tag_name' => array('LIKE' => $k)));
    echo json_encode(array('code' => 0, 'data' => $kdata));
} elseif ($action == 'addad') {
    if ($method == 'GET') {
        if (param('aid')) {
            $val = db_find_one("thread_ad", array('id' => param('aid')));
        } else {
            $val = array('ad_thumb' => '', 'ad_title' => '', 'ad_desc' => '', 'ad_links' => '');
        }
        include _include(APP_PATH . 'plugin/xl_thread_ad/admin/add.htm');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/thread_ad/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/thread_ad/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }

            $new_file = time() . rand(100, 50000) . "." . $type;

            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('hidden_img_tpl'))))) {
                $data['ad_thumb'] = $wwwpath . $new_file;
            }
        }
        $data['ad_title'] = param('ad_title');
        $data['ad_desc'] = param('ad_desc');
        $data['ad_links'] = param('ad_links');
        $data['order'] = param('order');
        if (param('aid')) {
            db_update("thread_ad", array('id' => param('aid')), $data);
        } else {
            db_insert("thread_ad", $data);
        }
        message(0, 'success');

    }
}
?>