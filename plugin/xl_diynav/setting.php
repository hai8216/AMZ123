<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_diynav';
$action = param(3) ?: "list";
if ($action == 'list') {
    //找服务
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('diynav', array(),array(), $page, $pagesize);
    $c = db_count("diynav");
    $pagination = pagination(url("plugin-setting-xl_diynav-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_diynav/view/list.html');
} elseif ($action == 'data') {
    //找服务
    $page = param(4, 1);
    $fupid = param('fupid');
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('diynav_data', array('fupid'=>$fupid),array(), $page, $pagesize);
    $c = db_count("diynav_data",array('fupid'=>$fupid));
    $fupdata = db_find_one("diynav", array('id' => $fupid));
    $pagination = pagination(url("plugin-setting-xl_diynav-data-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_diynav/view/data.html');
}elseif($action=='deldata'){
    $fupdata = db_find_one("diynav_data", array('id' =>  param('id')));
    db_delete('diynav_data', array('id' => param('id')));
    message(0, jump('操作成功', url('plugin-setting-xl_diynav-data',array('fupid'=>$fupdata['fupid']))));
}elseif($action=='delnav'){
    db_delete('diynav', array('id' => param('id')));
    message(0, jump('操作成功', url('plugin-setting-xl_diynav')));
}elseif ($action == 'adnav') {
    $type = param(4, 0);
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("diynav", array('id' => param('id')));
        } else {
            $data = array('id' => 0);
        }

        $input['navname'] = form_text('navname', $data['navname']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        $input['status'] = form_select('status', array('1' => '启用', 0 => '暂不启用'), $data['status']);
        $input['jt'] = form_select('jt', array('1' => '有', 0 => '无'), $data['jt']);
        include _include(APP_PATH . 'plugin/xl_diynav/view/adnav.html');
    } else {
        $data['navname'] = param('navname');
        $data['status'] = param('status');
        $data['jt'] = param('jt');
        if (param('id')) {
            db_update("diynav", array('id' => param('id')), $data);
        } else {
            db_insert("diynav", $data);
        }
        message(0, jump('保存成功', url('plugin-setting-xl_diynav')));
    }
}elseif ($action == 'adnavdata') {
    $type = param(4, 0);
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("diynav_data", array('id' => param('id')));
        } else {
            $data = array('id'=>0);
        }
        $fupid = param('fupid')?param('fupid'):$data['fupid'];

        $input['name'] = form_text('name', $data['name']);
        $input['desc'] = form_text('desc', $data['desc']);
        $input['links'] = form_text('links', $data['links']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        include _include(APP_PATH . 'plugin/xl_diynav/view/adnavdata.html');
    } else {

        $s = param('favicon');
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/',$s,$res)) {
            $type = $res[2];
            $path = "/upload/index_icon/" . date('Ymd') . "/";
            $ext = '.'.$type;
            $name = uniqid() . $ext;
            if(saveTencentBase64(base64_decode(str_replace($res[1],'', $s)), $path . $name)){
                $data['favicon'] = I($path . $name,'icon52');
            }
        }
        $data['name'] = param('name');
        $data['desc'] = param('desc');
        $data['links'] = param('links');
        $data['fupid'] = param('fupid');
        $data['order_id'] = param('order_id');;
        if (param('id')) {
            db_update("diynav_data", array('id' => param('id')), $data);
        } else {
            db_insert("diynav_data", $data);
        }
        message(0, jump('保存成功', url('plugin-setting-xl_diynav-data-'.$data['fupid'])));
    }
} elseif ($action == 'adwuliu') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("find_fuwu", array('id' => param('id')));
        } else {
            $data = array('id' => 0, 'cate1' => 0, 'cate2' => 0, 'cate3' => 0, 'cate4' => 0);
        }
        $catelist = db_find("find_fuwu_cate", array('type' => 2, 'fup' => 0));
        $input['name'] = form_text('name', $data['name']);
        $input['desc'] = form_textarea('desc', $data['desc']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        include _include(APP_PATH . 'plugin/xl_diynav/view/adwuliu.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/remote/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/remote/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('favicon'))))) {
                $data['icon'] = $wwwpath . $new_file;
            }
        }
        $data['cate1'] = param('cate1');
        $data['cate2'] = param('cate2');
        $data['cate3'] = param('cate3');
        $data['cate4'] = param('cate4');
        $data['name'] = param('name');
        $data['desc'] = param('desc');
        $data['order_id'] = param('order_id');
        if (param('id')) {
            db_update("find_fuwu", array('id' => param('id')), $data);
        } else {
            $data['model_type'] = 1;
            db_insert("find_fuwu", $data);
        }
        message(0, jump('保存成功', url('plugin-setting-xl_diynav-wuliu')));
    }
}
?>