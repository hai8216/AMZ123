<?php

!defined('DEBUG') and exit('Access Denied.');
$action = param(3) ?: "list";
$tpl = [
    "0" => '四图标模块一',
    "1" => '四图标模块二',
    "3" => '多图图片模块',
    "4" => '单图图片模块',
    "5" => '推荐服务模块',
    "6" => '客服顾问模块',
];
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;
    $keywords = search_keyword_safe(xn_urldecode(param(5)));
    if ($keywords) {
        $map['name'] = array("LIKE"=>$keywords);
    } else {
        $map = [];
    }
    $arrlist = db_find("pingtai_domain", $map, array(), $page, $pagesize);
    $c = db_count("pingtai_domain",$map);

    $pagination = pagination(url("plugin-setting-xl_pingtai-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_pingtai/admin/list.html');
} elseif ($action == 'dellist') {
    $id = param('delid');
    if (!$id) {
        message(0, '错误的参数');
        exit;
    }
    db_delete("pingtai_domain", ["id" => $id]);
    message(0, 'ok');
} elseif ($action == 'create') {
    if ($method == 'GET') {
        if (param('pid')) {
            $pid = param('pid');
            $val = db_find_one("pingtai_domain", ["id" => $pid]);
        }
        $input['name'] = form_text("name", $val['name']);
        $input['desc'] = form_text("desc", $val['desc']);
        $input['domain'] = form_text("domain", $val['domain']);
        $input['seo_title'] = form_text("seo_title", $val['seo_title']);
        $input['seo_desc'] = form_text("seo_desc", $val['seo_desc']);
        $input['seo_keywords'] = form_text("seo_keywords", $val['seo_keywords']);
        $input['tag'] = form_text("tag", $val['tag']);
        $input['tuid'] = form_text("tuid", $val['tuid']);
        $input['tabtext'] = form_textarea("tabtext", $val['tabtext'], '', 150);

        include _include(APP_PATH . 'plugin/xl_pingtai/admin/create.html');
    } else {
        $input['name'] = param("name");
        $input['desc'] = param("desc");
        $input['domain'] = param("domain");
        $input['seo_title'] = param("seo_title");
        $input['seo_desc'] = param("seo_desc");
        $input['tag'] = param("tag");
        $input['tuid'] = param("tuid");
        $input['tabtext'] = param("tabtext");
        $input['seo_keywords'] = param("seo_keywords");
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['avatar'] = $path . $name;
            }
        }
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl_1'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl_1'))), $path . $name)) {
                $input['banner'] = $path . $name;
            }
        }
        if (param('id')) {
            db_update("pingtai_domain", ["id" => param('id')], $input);
        } else {
            db_insert("pingtai_domain", $input);
        }

        message(0, 'ok');
    }
} elseif ($action == 'noref') {
    $pid = param('pid');
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("pingtai_domain_noref", ["pid" => $pid], array(), $page, $pagesize);
    $c = db_count("pingtai_domain_noref", ["pid" => $pid]);
    $pagination = pagination(url("plugin-setting-xl_pingtai-noref-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_pingtai/admin/noreflist.html');

} elseif ($action == 'rightmodel') {
    $pid = param('pid');
    if (!$pid) {
        message(0, '错误的参数');
        exit;
    }
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $modelList = db_find("pingtai_model", ["pid" => $pid], ["orderid" => '-1'], 1, 20);
    $c = db_count("pingtai_model", ["pid" => $pid]);
    $pagination = pagination(url("plugin-setting-xl_pingtai-rightmodel", ["pid" => $pid]), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_pingtai/admin/model-list.html');
} elseif ($action == 'delrightmodel') {
    $id = param('delid');
    if (!$id) {
        message(0, '错误的参数');
        exit;
    }
    db_delete("pingtai_model", ["id" => $id]);
    message(0, 'ok');
} elseif ($action == 'norefcreate') {
    $pid = param('pid');
    if ($method == 'GET') {
        $val = [];
        if ($id = param('id')) {
            $val = db_find_one("pingtai_domain_noref", ["id" => $id]);
            $pid = $pid ?: $val['pid'];
        }
        $input['tabName'] = form_text("tabName", $val['tabName']);
        $input['dateHtml'] = form_textarea("dateHtml", $val['dateHtml'], '', '300');
        $input['tabDomain'] = form_text("tabDomain", $val['tabDomain']);
        $input['dateType'] = form_select("dateType", ["0" => "帖文", "1" => "文本"], $val['dateType']);
        $input['tag'] = form_text("tag", $val['tags']);
        $input['seo_title'] = form_text("seo_title", $val['seo_title']);
        $input['seo_desc'] = form_text("seo_desc", $val['seo_desc']);
        $input['seo_keywords'] = form_text("seo_keywords", $val['seo_keywords']);
        $input['uids'] = form_text("uids", $val['uids']);
        include _include(APP_PATH . 'plugin/xl_pingtai/admin/model-norefcreate.html');
    } else {
        $input['seo_title'] = param("seo_title");
        $input['seo_desc'] = param("seo_desc");
        $input['seo_keywords'] = param("seo_keywords");
        $input['tabName'] = param("tabName");
        $input['dateHtml'] = param("dateHtml", '', false);
        $input['dateType'] = param("dateType");
        $input['tabDomain'] = param("tabDomain");
        $input['tags'] = param("tag");
        $input['uids'] = param("uids");
        $input['pid'] = param("pid");
        if (param('id')) {
            db_update("pingtai_domain_noref", ["id" => param('id')], $input);
        } else {
            db_insert("pingtai_domain_noref", $input);
        }
        message(0, 'ok');
    }
} elseif ($action == 'delnoref') {
    db_delete("pingtai_domain_noref", ["id" => param('delid')]);
    message(0, 'ok');
} elseif ($action == 'createModel') {
    $pid = param('pid');
    if ($method == 'GET') {
        $val = [];
        if ($id = param('id')) {
            $val = db_find_one("pingtai_model", ["id" => $id]);
        }
        $input['name'] = form_text("name", $val['name']);
        $input['desc'] = form_text("desc", $val['desc']);
        $input['orderid'] = form_text("orderid", $val['orderid']);
        $input['tplid'] = form_select("tplid", $tpl, $val['tplid']);
        $input['morelinks'] = form_text("tplid", $val['morelinks']);
        include _include(APP_PATH . 'plugin/xl_pingtai/admin/model-create.html');
    } else {
        $input['name'] = param("name");
        $input['orderid'] = param("orderid");
        $input['tplid'] = param("tplid");
        $input['pid'] = param("pid");
        $input['morelinks'] = param("morelinks");
        $input['desc'] = param("desc");
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['icon'] = $path . $name;
            }
        }
        if (param('id')) {
            db_update("pingtai_model", ["id" => param('id')], $input);
        } else {
            db_insert("pingtai_model", $input);
        }
        message(0, 'ok');
    }
} elseif ($action == 'modelData') {
    $pid = param('pid');
    $id = param('id');
    if (!$pid || !$id) {
        message(0, '错误的参数');
        exit;
    }
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $data = db_find("pingtai_model_data", ["mid" => $id], ["orderid" => '-1'], 1, 20);
    $c = db_count("pingtai_model_data", ["mid" => $id]);
    $pagination = pagination(url("plugin-setting-xl_pingtai-modelData", ["id" => $id, "pid" => $pid]), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_pingtai/admin/modelData-list.html');
} elseif ($action == 'top') {
    $pid = param('pid');
    if (!$pid) {
        message(0, '错误的参数');
        exit;
    }
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $data = db_find("pingtai_top", ["pid" => $pid], ["orderid" => '-1'], 1, 20);
    $c = db_count("pingtai_top");
    $pagination = pagination(url("plugin-setting-xl_pingtai-top", ["pid" => $pid]), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_pingtai/admin/top-list.html');
} elseif ($action == 'addmodelData') {
    $pid = param('pid');
    $mid = param('mid');
    if ($method == 'GET') {
        $val = [];
        if ($id = param('id')) {
            $val = db_find_one("pingtai_model_data", ["mid" => $mid]);
        }
        $input['name'] = form_text("name", $val['name']);
        $input['links'] = form_text("links", $val['links']);
        $input['orderid'] = form_text("orderid", $val['orderid']);
        $input['text'] = form_text("text", $val['text']);
        $input['text2'] = form_text("text2", $val['text2']);
        include _include(APP_PATH . 'plugin/xl_pingtai/admin/model-create-data.html');
    } else {
        $input['name'] = param("name");
        $input['orderid'] = param("orderid");
        $input['links'] = param("links");
        $input['mid'] = param("mid");
        $input['pid'] = param("pid");
        $input['text'] = param("text");
        $input['text2'] = param("text2");
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['thumb'] = $path . $name;
            }
        }
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl_1'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl_1'))), $path . $name)) {
                $input['thumb2'] = $path . $name;
            }
        }
        if (param('dataid')) {
            db_update("pingtai_model_data", ["dataid" => param('dataid')], $input);
        } else {
            db_insert("pingtai_model_data", $input);
        }
        message(0, 'ok');
    }
} elseif ($action == 'delmodelData') {
    $id = param('delid');
    if (!$id) {
        message(0, '错误的参数');
        exit;
    }
    db_delete("pingtai_model_data", ["dataid" => $id]);
    message(0, 'ok');
} elseif ($action == 'createTop') {
    $pid = param('pid');
    if ($method == 'GET') {
        $val = [];
        if ($id = param('id')) {
            $val = db_find_one("pingtai_top", ["id" => $id]);
        }
        $input['text'] = form_text("text", $val['text']);
        $input['links'] = form_text("links", $val['links']);
        $input['orderid'] = form_text("orderid", $val['orderid']);
        include _include(APP_PATH . 'plugin/xl_pingtai/admin/top-create.html');
    } else {
        $input['text'] = param("text");
        $input['orderid'] = param("orderid");
        $input['links'] = param("links");
        $input['pid'] = param("pid");
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['thumb'] = $path . $name;
            }
        }
        if (param('id')) {
            db_update("pingtai_top", ["id" => param('id')], $input);
        } else {
            db_insert("pingtai_top", $input);
        }
        message(0, 'ok');
    }
} elseif ($action == 'tagajax') {
    if ($method == 'GET') {
        $input['key'] = form_text("key", '');
        include _include(APP_PATH . 'plugin/xl_pingtai/admin/tagajax.html');
    } else {
        $key = param('key');
        $map['tag_name'] = array('LIKE' => $key);
        $search = db_find("thread_tag", $map, [], 1, 10);
        echo json_encode($search);
        exit;
    }
}