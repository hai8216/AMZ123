<?php
require_once APP_PATH.'/vendor/autoload.php';
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Captcha\V20190722\CaptchaClient;
use TencentCloud\Captcha\V20190722\Models\DescribeCaptchaResultRequest;
!defined('DEBUG') AND exit('Access Denied.');

$action = param(1);
// hook thread_start.php
$fid = param('fid', 0);
db_insert("test_pc",['ip'=>ip(),'ua'=>$_SERVER['HTTP_USER_AGENT']]);
if ($fid == '3') {
    $action = param(1);
// 发表主题帖 | create new thread
    if ($action == 'create') {

        // hook thread_create_get_post.php

        user_login_check();

        if ($method == 'GET') {

            // hook thread_create_get_start.php

            $fid = param(2, 0);
            $forum = $fid ? forum_read($fid) : array();

            $forumlist_allowthread = forum_list_access_filter($forumlist, $gid, 'allowthread');
            $forumarr = xn_json_encode(arrlist_key_values($forumlist_allowthread, 'fid', 'name'));
            if (empty($forumlist_allowthread)) {
                message(-1, lang('user_group_insufficient_privilege'));
            }

            $header['title'] = lang('create_thread');
            $header['mobile_title'] = $fid ? $forum['name'] : '';
            $header['mobile_linke'] = url("forum-$fid");

            // hook thread_create_get_end.php

            include _include(APP_PATH . 'view/htm/post.htm');

        } else {

            // hook thread_create_thread_start.php
            $ax_act2 = kv_get('ax_act');

            $fid = param('fid', 0);
            $forum = forum_read($fid);
            empty($forum) AND message('fid', lang('forum_not_exists'));

            $r = forum_access_user($fid, $gid, 'allowthread');
            !$r AND message(-1, lang('user_group_insufficient_privilege'));

            $subject = param('subject');
            //kv 以及下方判断后接入  特定板块不输入标题
            if ($fid != $ax_act2['black']) {
                empty($subject) AND message('subject', lang('please_input_subject'));
            }

            xn_strlen($subject) > 128 AND message('subject', lang('subject_length_over_limit', array('maxlength' => 128)));

            $message = param('message', '', FALSE);
            empty($message) AND message('message', lang('please_input_message'));
            $doctype = param('doctype', 0);
            $doctype > 10 AND message(-1, lang('doc_type_not_supported'));
            xn_strlen($message) > 2028000 AND message('message', lang('message_too_long'));

            $thread = array(
                'fid' => $fid,
                'uid' => $uid,
                'sid' => $sid,
                'subject' => $subject,
                'message' => $message,
                'time' => $time,
                'longip' => $longip,
                'doctype' => $doctype,
            );

            // hook thread_create_thread_before.php

            $tid = thread_create($thread, $pid);
            $pid === FALSE AND message(-1, lang('create_post_failed'));
            $tid === FALSE AND message(-1, lang('create_thread_failed'));

            // hook thread_create_thread_end.php
            // hook thread_create_thread_end_hook_end.php

            message(0, lang('create_thread_sucessfully'));
        }

// 帖子详情 | post detail
    } else {

        // thread-{tid}-{page}-{keyword}.htm
        $tid = param(1, 0);
        $page = param(2, 1);
        $keyword = param(3);
        $pagesize = $conf['postlist_pagesize'];
        //$pagesize = 10;
        //$page == 1 AND $pagesize++;

        // hook thread_info_start.php

        $thread = thread_read($tid);
        empty($thread) AND message(-1, lang('thread_not_exists'));

        $fid = $thread['fid'];
        $forum = forum_read($fid);
        empty($forum) AND message(3, lang('forum_not_exists'));

        $postlist = post_find_by_tid($tid, $page, $pagesize);
        empty($postlist) AND message(4, lang('post_not_exists'));

        if ($page == 1) {
            empty($postlist[$thread['firstpid']]) AND message(-1, lang('data_malformation'));
            $first = $postlist[$thread['firstpid']];
            unset($postlist[$thread['firstpid']]);
            $attachlist = $imagelist = $filelist = array();

            // 如果是大站，可以用单独的点击服务，减少 db 压力
            // if request is huge, separate it from mysql server
            thread_inc_views($tid);
        } else {
            $first = post_read($thread['firstpid']);
        }

        $keywordurl = '';
        if ($keyword) {
            $thread['subject'] = post_highlight_keyword($thread['subject'], $keyword);
            //$first['message'] = post_highlight_keyword($first['subject']);
            $keywordurl = "-$keyword";
        }
        $allowpost = forum_access_user($fid, $gid, 'allowpost') ? 1 : 0;
        $allowupdate = forum_access_mod($fid, $gid, 'allowupdate') ? 1 : 0;
        $allowdelete = forum_access_mod($fid, $gid, 'allowdelete') ? 1 : 0;

        forum_access_user($fid, $gid, 'allowread') OR message(-1, lang('user_group_insufficient_privilege'));

        $pagination = pagination(url("thread-$tid-{page}$keywordurl"), $thread['posts'] + 1, $page, $pagesize);

        $header['title'] = $thread['subject'] . '-' . $forum['name'] . '-' . $conf['sitename'];
        //$header['mobile_title'] = lang('thread_detail');
        $header['mobile_title'] = $forum['name'];;
        $header['mobile_link'] = url("forum-$fid");
        $header['keywords'] = '';
        $header['description'] = $thread['subject'];
        $_SESSION['fid'] = $fid;


        // hook thread_info_end.php

        include _include(APP_PATH . 'view/htm/thread.htm');
    }

// hook thread_end.php
} else {


    $action = param(1);


// 发表主题帖 | create new thread
    if ($action == 'create') {

        // hook thread_create_get_post.php

        user_login_check();

        if ($method == 'GET') {

            // hook thread_create_get_start.php

            $fid = param(2, 0);
            $forum = $fid ? forum_read($fid) : array();

            $forumlist_allowthread = forum_list_access_filter($forumlist, $gid, 'allowthread');
            $forumarr = xn_json_encode(arrlist_key_values($forumlist_allowthread, 'fid', 'name'));
            if (empty($forumlist_allowthread)) {
                message(-1, lang('user_group_insufficient_privilege'));
            }

            $header['title'] = lang('create_thread');
            $header['mobile_title'] = $fid ? $forum['name'] : '';
            $header['mobile_linke'] = url("forum-$fid");

            // hook thread_create_get_end.php

            include _include(APP_PATH . 'view/htm/post.htm');

        } else {

            // hook thread_create_thread_start.php
            $ax_bg = kv_get('ax_bg');

            $fid = param('fid', 0);
            $forum = forum_read($fid);
            empty($forum) AND message('fid', lang('forum_not_exists'));

            $r = forum_access_user($fid, $gid, 'allowthread');
            !$r AND message(-1, lang('user_group_insufficient_privilege'));

            $subject = param('subject');
            //kv 以及下方判断后接入  特定板块不输入标题
            if ($fid != $ax_bg['black'] and $fid != $ax_bg['tw']) {
                empty($subject) AND message('subject', lang('please_input_subject'));
            }


            xn_strlen($subject) > 128 AND message('subject', lang('subject_length_over_limit', array('maxlength' => 128)));

            $message = param('message', '', FALSE);
            empty($message) AND message('message', lang('please_input_message'));
            $doctype = param('doctype', 0);
            $doctype > 10 AND message(-1, lang('doc_type_not_supported'));
            xn_strlen($message) > 2028000 AND message('message', lang('message_too_long'));

            $thread = array(
                'fid' => $fid,
                'uid' => $uid,
                'sid' => $sid,
                'subject' => $subject,
                'message' => $message,
                'time' => $time,
                'longip' => $longip,
                'doctype' => $doctype,
            );

            // hook thread_create_thread_before.php

            $tid = thread_create($thread, $pid);
            $pid === FALSE AND message(-1, lang('create_post_failed'));
            $tid === FALSE AND message(-1, lang('create_thread_failed'));

            // hook thread_create_thread_end.php
            // hook thread_create_thread_end_hook_end.php
            message(0, lang('create_thread_sucessfully'));
        }

// 帖子详情 | post detail
    } else {

        // thread-{tid}-{page}-{keyword}.htm
        $tid = param(1, 0);
        $page = param(2, 1);
        $keyword = param(3);
        $pagesize = $conf['postlist_pagesize'];
        //$pagesize = 10;
        //$page == 1 AND $pagesize++;

        // hook thread_info_start.php

        $thread = thread_read($tid);
        empty($thread) AND http_404();

        $fid = $thread['fid'];
        $forum = forum_read($fid);
        empty($forum) AND http_404();;

        $postlist = post_find_by_tid($tid, $page, $pagesize);
        empty($postlist) AND message(4, lang('post_not_exists'));

        if ($page == 1) {
            empty($postlist[$thread['firstpid']]) AND message(-1, lang('data_malformation'));
            $first = $postlist[$thread['firstpid']];
            unset($postlist[$thread['firstpid']]);
            $attachlist = $imagelist = $filelist = array();

            // 如果是大站，可以用单独的点击服务，减少 db 压力
            // if request is huge, separate it from mysql server
            thread_inc_views($tid);
        } else {
            $first = post_read($thread['firstpid']);
        }

        $keywordurl = '';
        if ($keyword) {
            $thread['subject'] = post_highlight_keyword($thread['subject'], $keyword);
            //$first['message'] = post_highlight_keyword($first['subject']);
            $keywordurl = "-$keyword";
        }
        $allowpost = forum_access_user($fid, $gid, 'allowpost') ? 1 : 0;
        $allowupdate = forum_access_mod($fid, $gid, 'allowupdate') ? 1 : 0;
        $allowdelete = forum_access_mod($fid, $gid, 'allowdelete') ? 1 : 0;

        forum_access_user($fid, $gid, 'allowread') OR message(-1, lang('user_group_insufficient_privilege'));

        $pagination = pagination(url("thread-$tid-{page}$keywordurl"), $thread['posts'] + 1, $page, $pagesize);

        $ax_bg = kv_get('ax_bg');

        if ($fid == $ax_bg['black']) {
            $header['title'] = $thread['ax_wab'] . "_" . $thread['ax_dianpu'] . "_" . $conf['sitename'];
        } elseif ($fid == $ax_bg['tw']) {
            $header['title'] = $thread['ax_axname'] . "_" . $thread['ax_alpay'] . "_" . $thread['ax_wx'] . "_" . $thread['ax_qq'] . "_" . $conf['sitename'];
        } else {
            $header['title'] = $thread['subject'] . '-' . $forum['name'] . '-' . $conf['sitename'];
        }


        //$header['mobile_title'] = lang('thread_detail');
        $header['mobile_title'] = $forum['name'];;
        $header['mobile_link'] = url("forum-$fid");
        $header['keywords'] = '';
        $header['description'] = $thread['subject'];
        $_SESSION['fid'] = $fid;


        // hook thread_info_end.php
//        if(param('debug')){
//            IHTML($first['message']);exit;
//        }
        if (param('debug')) {
            $first['message_fmt'] = IHTML($first['message_fmt']);
        }
        include _include(APP_PATH . 'view/htm/thread.htm');
    }
    // hook thread_end.php
}


?>