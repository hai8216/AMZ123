<?php
$header['title'] = '美亚畅销榜选品工具 - AMZ123跨境导航';
$cateList = db_find("ama_cate", array('fup' => 0,'hide'=>0), array(), 1, 5000);
$sql['sqlname'] = "amazon_bigdata";
$fup = param(1, 0);
$fup2 = param(2, 0);
$price = param(3, 0);
$star = param(4, 0);
$replycount = param(5, 0);
$keywords = urldecode(param(6, ''));

$diys1 = param("diys1");
$diys2 = param("diys2");

$diyp1 = param("diyp1");
$diyp2 = param("diyp2");

$diyr1 = param("diyr1");
$diyr2 = param("diyr2");
$order = param("order",'1');
//获取最后一条时间
$lasttime = date('Y-m-d H:i',db_find_one($sql['sqlname'],array(),array('id'=>'-1'))['dateline']);
if ($fup) {
    $map['fup'] = $fup;
    $cateList2 = db_find("ama_cate", array('fup' => $fup,'hide'=>0), array(), 1, 5000);
    foreach($cateList2 as $v){
        $ary_fup2[]=$v['id'];
    }
    $map['fup2'] = $ary_fup2;
    $fup1info = db_find_one("ama_cate", array('id' => $fup));
    if ($fup2) {
        $map['fup2'] = $fup2;
        $fup2info = db_find_one("ama_cate", array('id' => $fup2));
    }
}else{
    foreach($cateList as $v){
        $ary_fup[]=$v['id'];
    }
    $map['fup'] = array();
}

if ($diyp1 || $diyp2) {
    $map['allprice'] = array('>' => $diyp1, '<' => $diyp2);
    $diyprice = 999999999;
    $f_price['title'] = $diyp1 . " - " . $diyp2;
} else {
    if ($price) {
        $f_price = db_find_one("ama_filter", array('id' => $price));
        $map['allprice'] = array('>' => $f_price['end'], '<' => $f_price['star']);
    }
}


if ($diys1 || $diys2) {
    $map['star'] = array('>' => $diys1, '<' => $diys2);
    $f_star['title'] = $diys1 . " - " . $diys2;
} else {
    if ($star) {
        $f_star = db_find_one("ama_filter", array('id' => $star));
        $map['star'] = array('>' => $f_star['end'], '<' => $f_star['star']);
    }
}

if ($diyr1 || $diyr2) {
    $map['reply_count'] = array('>' => $diyr1, '<' => $diyr2);
    $f_replycount['title'] = $diyr1 . " - " . $diyr2;
} else {

    if ($replycount) {
        $f_replycount = db_find_one("ama_filter", array('id' => $replycount));
        $map['reply_count'] = array('>' => $f_replycount['end'], '<' => $f_replycount['star']);
    }
}
if($keywords){
    $map['title'] = array('LIKE' => $keywords);
}
$page = param(7, 1);
$pagesize = 200;
$c = db_count($sql['sqlname'], $map);

//排序
switch ($order){
    case 1:
        //1默认
        $orderby = array();
        break;
    case 2:
        //价格倒叙
        $orderby = array('allprice'=>'-1');
        break;
    case 3:
        //价格升叙
        $orderby = array('allprice'=>'1');
        break;
    case 4:
        //评分倒叙
        $orderby = array('star'=>'-1');
        break;
    case 5:
        //评分升叙
        $orderby = array('star'=>'1');
        break;
    case 6:
        //评论数倒叙
        $orderby = array('reply_count'=>'-1');
        break;
    case 7:
        //评论数倒叙
        $orderby = array('reply_count'=>'1');
        break;

}

$bigdatas = db_find($sql['sqlname'], $map, $orderby, $page, $pagesize);
$count = count($bigdatas);
$bigdata[0] = array_slice($bigdatas, 0, ceil($count / 2));
$bigdata[1] = array_slice($bigdatas, $count - ceil($count / 2), intval($count / 2));
$pagination = pagination(url("bestseller-" . $fup . "-" . $fup2 . "-" . $price . "-" . $star . "-" . $replycount . "-" . $keywords . "-{page}"), $c, $page, $pagesize);
//其他
$fiter1 = db_find("ama_filter", array('type' => 1));
$fiter2 = db_find("ama_filter", array('type' => 2));
$fiter3 = db_find("ama_filter", array('type' => 3));

if (check_wap()) {
    include _include(APP_PATH . 'plugin/xl_amazon_rankdata/view/list_5g.html');
}else{
    include _include(APP_PATH . 'plugin/xl_amazon_rankdata/view/list.html');
}
