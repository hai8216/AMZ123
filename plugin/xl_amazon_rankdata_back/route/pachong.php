<?php
$password = "123123/as";
if (empty($_POST['__sign']) || md5("{$password}shenjianshou.cn") != trim($_POST['__sign'])) {
    echo json_encode([
        "result" => 2,
        "reason" => "发布失败, 错误原因: 发布密码验证失败!"
    ]);
    exit;
}

$cate1 = str_replace("amp;", "", param('cate1'));
$cate2 = param('now_cate');
$datas = json_decode($_POST['data'], true);
//创建分类
//先创建1级分类
$cate1_info = db_find_one("ama_cate", array('cate_name' => $cate1));
if (!$cate1_info['id']) {
    $cate1_info['id'] = db_insert("ama_cate", array('fup' => 0, 'cate_name' => $cate1));
    $cate2_info['id'] = db_insert("ama_cate", array('fup' => $cate1_info['id'], 'cate_name' => $cate2));
} else {
    //对2级去重
    $cate2_info['id'] = db_find_one("ama_cate", array('cate_name' => $cate2))['id'];
    if(!$cate2_info['id']){
        $cate2_info['id'] = db_insert("ama_cate", array('fup' => $cate1_info['id'], 'cate_name' => $cate2));
    }
}
//分解数据
//去重
foreach ($datas as $v) {
    if ($v['asin']) {
        $as = explode("/dp/", $v['asin']);
        $asins = explode("/ref", $as[1])[0];
        $isid = db_find_one('amazon_bigdata', array('asin' => $asins));


        $ary['title'] = $v['title'];
        $ary['reply_count'] = $v['reply_count'];
        $ary['allprice'] = str_replace("$", "", $v['allprice']);
        $ary['star'] = str_replace(" out of 5 stars", "", $v['star']);
        $ary['img'] = $v['img'];
        $ary['asin'] = $asins;
        $ary['dateline'] = time();
        $ary['fup'] = $cate1_info['id'];
        $ary['fup2'] = $cate2_info['id'];
        $ary['rank'] = intval(str_replace("#", "", $v['rank']));

        if (!$isid['id']) {
            //判断排名，直接用排名替换数据
            $isHave = db_find_one('amazon_bigdata', array('fup' => $cate1_info['id'],'fup2'=>$cate2_info['id'], 'rank' => $ary['rank']));
            if ($isHave['id']) {
                db_update("amazon_bigdata", array('id'=>$isHave['id']), $ary);
            } else {
                db_insert('amazon_bigdata', $ary);
            }
        } else {
            db_update('amazon_bigdata', array('asin' => $asins), $ary);
        }
    }
}
echo json_encode(["result" => 1, "data" => "发布成功"]);