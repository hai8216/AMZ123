<?php

!defined('DEBUG') AND exit('Access Denied.');

if ($method == 'GET') {

    $kv = kv_get('nocopy');

    $input = array();

    $input['nopcy_endtxt'] = form_text('nopcy_endtxt', $kv['nopcy_endtxt']);

    foreach ($forumlist_show as $v) {
        $nopcy_forum[] = form_checkbox('nopcy_forum_' . $v['fid'], $kv['nocopy_fid'][$v['fid']], $v['name']);
    }
    include _include(APP_PATH . 'plugin/xl_nocopy/setting.htm');

} elseif ($method == 'POST') {

    $kv = array();
    $kv = kv_get('nocopy');
    $kv['nopcy_endtxt'] = param('nopcy_endtxt');
    foreach ($forumlist_show as $v) {
        $kv['nocopy_fid'][$v['fid']] = param('nopcy_forum_'.$v['fid']);
    }
    kv_set('nocopy', $kv);
    message(0, '修改成功');
}
