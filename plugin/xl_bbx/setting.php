<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') AND exit('Access Denied.');
cache_delete('bbx_cache');
if (param('do')==1 && $_POST) {
    $data['title'] = param('title');
    $data['href'] = param('href');
    $data['order'] = param('order');
    $iswx = db_find_one("bbx",array('title'=>$data['title']));
    if($iswx){
        message(0,'该工具箱已被添加');
    }

    db_insert("bbx",$data);
    cache_delete('bbx_cache');
    message(0, jump('添加成功', url('plugin-setting-xl_bbx')));

} if (param('do')==2 && $_POST) {
    if($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('id', 0);
    $r = db_delete('bbx', array('id'=>$_codeid));
    $r === FALSE AND message(-1, lang('delete_failed'));

}  else{
    cache_delete('bbx_cache');
    $page = param(3, 1);
    $pagesize = 40;
    $n = db_count('bbx', array());
    $pagination = pagination(url("plugin-setting-xl_bbx-{page}"), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_bbx-{page}"), $n, $page, $pagesize);
    $codelist = array();
    $codelist = db_find('bbx', array(), array("order"=>-1),$page,$pagesize);
    include _include(APP_PATH . "/plugin/xl_bbx/setting.htm");
}