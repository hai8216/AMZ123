<?php exit;
$groupData = db_find_one("group", ["gid" => $user['gid']]);
if ($groupData['closePost'] == 1) {
    //开始限制
    if ((date('w', time()) == 6) || (date('w', time()) == 0)) {
        //如果是周末就直接限制
        message(0, jump('暂无法发帖', url('index')));
        exit;
    } else {
        //如果不是周末就控制时间
        if (date('H', time()) >= 18 || date('H', time()) <= 9) {
            //如果是周末就直接限制
            message(0, jump('暂无法发帖', url('index')));
            exit;
        }
    }

}
