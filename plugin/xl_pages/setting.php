<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_pages';
$action = param(3) ?: "list";
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("pages", array(), array(), $page, $pagesize);
    $c = db_count("pages");
    $pagination = pagination(url("plugin-setting-xl_pages-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_pages/admin/list.html');
} if ($action == 'upload') {
    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('data'), $result)) {
        $type = $result[2];
        $time = time();
        $filepath = APP_PATH . "upload/banner/" . date('Ymd', $time) . "/";
        $wwwpath = "https://www.amz123.com/upload/banner/" . date('Ymd', $time) . "/";
        if (!file_exists($filepath)) {
            //检查是否有该文件夹，如果没有就创建，并给予最高权限
            mkdir($filepath, 0700);
        }
        $new_file = time() . rand(100, 50000) . "." . $type;
        if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('data'))))) {
            $data['album'] = $wwwpath . $new_file;
        } else {
            $data['album'] = $wwwpath . "default.png";
        }
    } else {
        $data['album'] = $wwwpath . "default.png";
    }
    message(0,array('url'=>$data['album']));
} elseif ($action == 'create') {
    if ($method == 'GET') {
        if (param('toid')) {
            $data = db_find_one("pages",array('id'=>param('toid')));
        } else {
            $data = array();
        }
        $input['title'] = form_text('title', $data['title']);
        $input['keywords'] = form_text('keywords', $data['keywords']);
        $input['description'] = form_text('description', $data['description']);
        $input['domain'] = form_text('domain', $data['domain']);
        $input['status'] = form_radio_yes_no('status', $data['status']);
        $input['message'] = form_textarea('message', $data['message']);
        include _include(APP_PATH . 'plugin/xl_pages/admin/create.html');
    } else {
        $data['title'] = param('title');
        $data['keywords'] = param('keywords');
        $data['description'] = param('description');
        $data['domain'] = param('domain');
        $data['status'] = param('status');
        $data['message'] =param('message', '', FALSE);
        foreach ($data as $k => $v) {
            if (!$v && $k != 'status') {
                message(401, $v);
            }
        }
        if (param('toid')) {
            db_update("pages",array('id'=>param('toid')),$data);
        } else {
            db_insert("pages",$data);
        }

        message(0, '保存成功');
    }
}
?>