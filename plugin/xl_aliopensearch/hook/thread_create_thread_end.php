<?php exit;
$tableName = 'bbs_post';
$smsg = strip_tags($message);
$documentClient = new DocumentClient($client);
$docs_to_upload = array();
$item['cmd'] = 'ADD';
$item["fields"] = array(
    'pid'=>$pid,
    'tid' => $tid,
    'subject' => $subject,
    'isfirst' => 1,
    'message' => str_replace("&nbsp;", " ", $smsg),
    'author' => $user['username'],
    'authorid' => $uid,
    'fid' => $fid,
    'fname' => $forum['name'],
    'back_message' => '',
    'posttime'=>$time,
);
if ($fid == '3') {
    $ax_country = param('ax_country');
    $ax_name = param('ax_name');
    $ax_email = param('ax_email');
    $ax_weixin = param('ax_weixin');
    $ax_ymx = param('ax_ymx');
    $ax_facebook = param('ax_facebook');
    $item["fields"]['ax_name'] = $ax_name;
    $item["fields"]['ax_email'] = $ax_email;
    $item["fields"]['ax_weixin'] = $ax_weixin;
    $item["fields"]['ax_ymx'] = $ax_ymx;
    $item["fields"]['ax_facebook'] = $ax_facebook;
    $item["fields"]['ax_country'] = $ax_country;
    $item["fields"]['subject'] = $ax_name."&emsp;".$ax_country."&emsp;".$ax_email."&emsp;";
}
if ($fid == '6') {
    $ax_qq = param('ax_qq');
    $ax_qqname = param('ax_qqname');
    $ax_wx = param('ax_wx');
    $ax_wxname = param('ax_wxname');
    $ax_alpay = param('ax_alpay');
    $ax_axname = param('ax_axname');
    $ax_wab = param('ax_wab');
    $ax_dianpu = param('ax_dianpu');
    $ax_ydpurl = param('ax_ydpurl');


    $item["fields"]['ax_qq'] = $ax_qq;
    $item["fields"]['ax_qqname'] = $ax_qqname;
    $item["fields"]['ax_wx'] = $ax_wx;
    $item["fields"]['ax_axname'] = $ax_axname;
    $item["fields"]['ax_wxname'] = $ax_wxname;
    $item["fields"]['ax_alpay'] = $ax_alpay;
    $item["fields"]['ax_wab'] = $ax_wab;
    $item["fields"]['ax_dianpu'] = $ax_dianpu;
    $item["fields"]['ax_ydpurl'] = $ax_ydpurl;
    $item["fields"]['subject'] = $ax_axname."&emsp;".$ax_alpay."&emsp;".$ax_wx."&emsp;".$ax_qq;

}



if ($fid == '7' || $fid=='6') {
    $ax_qq = param('ax_qq');
    $ax_qqname = param('ax_qqname');
    $ax_wx = param('ax_wx');
    $ax_wxname = param('ax_wxname');
    $ax_alpay = param('ax_alpay');
    $ax_axname = param('ax_axname');
    $ax_wab = param('ax_wab');
    $ax_dianpu = param('ax_dianpu');
    $ax_ydpurl = param('ax_ydpurl');


    $item["fields"]['ax_qq'] = $ax_qq;
    $item["fields"]['ax_qqname'] = $ax_qqname;
    $item["fields"]['ax_wx'] = $ax_wx;
    $item["fields"]['ax_axname'] = $ax_axname;
    $item["fields"]['ax_wxname'] = $ax_wxname;
    $item["fields"]['ax_alpay'] = $ax_alpay;
    $item["fields"]['ax_wab'] = $ax_wab;
    $item["fields"]['ax_dianpu'] = $ax_dianpu;
    $item["fields"]['ax_ydpurl'] = $ax_ydpurl;
    if($fid=='6'){
        $item["fields"]['subject'] = $ax_axname."&emsp;".$ax_alpay."&emsp;".$ax_wx."&emsp;".$ax_qq;
    }else{
        $item["fields"]['subject'] = $ax_wab."&emsp;".$ax_dianpu;
    }


}

$docs_to_upload[] = $item;
$json = json_encode($docs_to_upload);
$ret = $documentClient->push($json, $appName, $tableName);


