<?php

!defined('DEBUG') and exit('Access Denied.');

$action = param(3);

$user1 = array(param('u1'), param('uf1'), param('uh1'), param('ub1'));
$user2 = array(param('u2'), param('uf2'), param('uh2'), param('ub2'));;
$user3 = array(param('u3'), param('uf3'), param('uh3'), param('ub3'));;
$user4 = array(param('u4'), param('uf4'), param('uh4'), param('ub4'));;
$user5 = array(param('u5'), param('uf5'), param('uh5'), param('ub5'));;
$user6 = array(param('u6'), param('uf6'), param('uh6'), param('ub6'));
$user7 = array(param('u7'), param('uf7'), param('uh7'), param('ub7'));
$user8 = array(param('u8'), param('uf8'), param('uh8'), param('ub8'));
$user9 = array(param('u9'), param('uf9'), param('uh9'), param('ub9'));
$user10 = array(param('u10'), param('uf10'), param('uh10'), param('ub10'));
$user11 = array(param('u11'), param('uf11'), param('uh11'), param('ub11'));
$user12 = array(param('u12'), param('uf12'), param('uh12'), param('ub12'));
$user13 = array(param('u13'), param('uf13'), param('uh13'), param('ub13'));
$user14 = array(param('u14'), param('uf14'), param('uh14'), param('ub14'));
$user15 = array(param('u15'), param('uf15'), param('uh15'), param('ub15'));
$user16 = array(param('u16'), param('uf16'), param('uh16'), param('ub16'));
$user17 = array(param('u17'), param('uf17'), param('uh17'), param('ub17'));
$user18 = array(param('u18'), param('uf18'), param('uh18'), param('ub18'));
$user19 = array(param('u19'), param('uf19'), param('uh19'), param('ub19'));
$user20 = array(param('u20'), param('uf20'), param('uh20'), param('ub20'));


if ($method == 'GET') {

    $ax_vest = kv_get('ax_vest');

    if (empty($ax_vest)) {

        $ax_vest = array(
            'user1' => $user1,
            'user2' => $user2,
            'user3' => $user3,
            'user4' => $user4,
            'user5' => $user5,
            'user6' => $user6,
            'user7' => $user7,
            'user8' => $user8,
            'user9' => $user9,
            'user10' => $user10,
            'user11' => $user11,
            'user12' => $user12,
            'user13' => $user13,
            'user14' => $user14,
            'user15' => $user15,
            'user16' => $user16,
            'user17' => $user17,
            'user18' => $user18,
            'user19' => $user19,
            'user20' => $user20,

        );
        kv_set('ax_vest', $ax_vest);
    }


    $input1 = array(
        form_text('u1', $ax_vest['user1'][0], '100%', '版主管理ID'),
        form_text('uf1', $ax_vest['user1'][1], '100%', '发帖马甲ID'),
        form_text('uh1', $ax_vest['user1'][2], '100%', '回帖马甲ID'),
        form_text('ub1', $ax_vest['user1'][3], '100%', '备注')
    );
    $input2 = array(
        form_text('u2', $ax_vest['user2'][0], '100%', '版主管理ID'),
        form_text('uf2', $ax_vest['user2'][1], '100%', '发帖马甲ID'),
        form_text('uh2', $ax_vest['user2'][2], '100%', '回帖马甲ID'),
        form_text('ub2', $ax_vest['user2'][3], '100%', '备注')
    );
    $input3 = array(
        form_text('u3', $ax_vest['user3'][0], '100%', '版主管理ID'),
        form_text('uf3', $ax_vest['user3'][1], '100%', '发帖马甲ID'),
        form_text('uh3', $ax_vest['user3'][2], '100%', '回帖马甲ID'),
        form_text('ub3', $ax_vest['user3'][3], '100%', '备注')
    );
    $input4 = array(
        form_text('u4', $ax_vest['user4'][0], '100%', '版主管理ID'),
        form_text('uf4', $ax_vest['user4'][1], '100%', '发帖马甲ID'),
        form_text('uh4', $ax_vest['user4'][2], '100%', '回帖马甲ID'),
        form_text('ub4', $ax_vest['user4'][3], '100%', '备注')
    );
    $input5 = array(
        form_text('u5', $ax_vest['user5'][0], '100%', '版主管理ID'),
        form_text('uf5', $ax_vest['user5'][1], '100%', '发帖马甲ID'),
        form_text('uh5', $ax_vest['user5'][2], '100%', '回帖马甲ID'),
        form_text('ub5', $ax_vest['user5'][3], '100%', '备注')
    );
    $input6 = array(
        form_text('u6', $ax_vest['user6'][0], '100%', '版主管理ID'),
        form_text('uf6', $ax_vest['user6'][1], '100%', '发帖马甲ID'),
        form_text('uh6', $ax_vest['user6'][2], '100%', '回帖马甲ID'),
        form_text('ub6', $ax_vest['user6'][3], '100%', '备注')
    );
    $input7 = array(
        form_text('u7', $ax_vest['user7'][0], '100%', '版主管理ID'),
        form_text('uf7', $ax_vest['user7'][1], '100%', '发帖马甲ID'),
        form_text('uh7', $ax_vest['user7'][2], '100%', '回帖马甲ID'),
        form_text('ub7', $ax_vest['user7'][3], '100%', '备注')
    );
    $input8 = array(
        form_text('u8', $ax_vest['user8'][0], '100%', '版主管理ID'),
        form_text('uf8', $ax_vest['user8'][1], '100%', '发帖马甲ID'),
        form_text('uh8', $ax_vest['user8'][2], '100%', '回帖马甲ID'),
        form_text('ub8', $ax_vest['user8'][3], '100%', '备注')
    );
    $input9 = array(
        form_text('u9', $ax_vest['user9'][0], '100%', '版主管理ID'),
        form_text('uf9', $ax_vest['user9'][1], '100%', '发帖马甲ID'),
        form_text('uh9', $ax_vest['user9'][2], '100%', '回帖马甲ID'),
        form_text('ub9', $ax_vest['user9'][3], '100%', '备注')
    );
    $input10 = array(
        form_text('u10', $ax_vest['user10'][0], '100%', '版主管理ID'),
        form_text('uf10', $ax_vest['user10'][1], '100%', '发帖马甲ID'),
        form_text('uh10', $ax_vest['user10'][2], '100%', '回帖马甲ID'),
        form_text('ub10', $ax_vest['user10'][3], '100%', '备注')
    );

    $input11 = array(
        form_text('u11', $ax_vest['user11'][0], '100%', '版主管理ID'),
        form_text('uf11', $ax_vest['user11'][1], '100%', '发帖马甲ID'),
        form_text('uh11', $ax_vest['user11'][2], '100%', '回帖马甲ID'),
        form_text('ub11', $ax_vest['user11'][3], '100%', '备注')
    );

    $input12 = array(
        form_text('u12', $ax_vest['user12'][0], '100%', '版主管理ID'),
        form_text('uf12', $ax_vest['user12'][1], '100%', '发帖马甲ID'),
        form_text('uh12', $ax_vest['user12'][2], '100%', '回帖马甲ID'),
        form_text('ub12', $ax_vest['user12'][3], '100%', '备注')
    );
    $input13 = array(
        form_text('u13', $ax_vest['user13'][0], '100%', '版主管理ID'),
        form_text('uf13', $ax_vest['user13'][1], '100%', '发帖马甲ID'),
        form_text('uh13', $ax_vest['user13'][2], '100%', '回帖马甲ID'),
        form_text('ub13', $ax_vest['user13'][3], '100%', '备注')
    );
    $input14 = array(
        form_text('u14', $ax_vest['user14'][0], '100%', '版主管理ID'),
        form_text('uf14', $ax_vest['user14'][1], '100%', '发帖马甲ID'),
        form_text('uh14', $ax_vest['user14'][2], '100%', '回帖马甲ID'),
        form_text('ub14', $ax_vest['user14'][3], '100%', '备注')
    );
    $input15 = array(
        form_text('u15', $ax_vest['user15'][0], '100%', '版主管理ID'),
        form_text('uf15', $ax_vest['user15'][1], '100%', '发帖马甲ID'),
        form_text('uh15', $ax_vest['user15'][2], '100%', '回帖马甲ID'),
        form_text('ub15', $ax_vest['user15'][3], '100%', '备注')
    );
    $input16 = array(
        form_text('u16', $ax_vest['user16'][0], '100%', '版主管理ID'),
        form_text('uf16', $ax_vest['user16'][1], '100%', '发帖马甲ID'),
        form_text('uh16', $ax_vest['user16'][2], '100%', '回帖马甲ID'),
        form_text('ub16', $ax_vest['user16'][3], '100%', '备注')
    );
    $input17 = array(
        form_text('u17', $ax_vest['user17'][0], '100%', '版主管理ID'),
        form_text('uf17', $ax_vest['user17'][1], '100%', '发帖马甲ID'),
        form_text('uh17', $ax_vest['user17'][2], '100%', '回帖马甲ID'),
        form_text('ub17', $ax_vest['user17'][3], '100%', '备注')
    );

    $input18 = array(
        form_text('u18', $ax_vest['user18'][0], '100%', '版主管理ID'),
        form_text('uf18', $ax_vest['user18'][1], '100%', '发帖马甲ID'),
        form_text('uh18', $ax_vest['user18'][2], '100%', '回帖马甲ID'),
        form_text('ub18', $ax_vest['user18'][3], '100%', '备注')
    );


    $input19 = array(
        form_text('u19', $ax_vest['user19'][0], '100%', '版主管理ID'),
        form_text('uf19', $ax_vest['user19'][1], '100%', '发帖马甲ID'),
        form_text('uh19', $ax_vest['user19'][2], '100%', '回帖马甲ID'),
        form_text('ub19', $ax_vest['user19'][3], '100%', '备注')
    );

    $input20 = array(
        form_text('u20', $ax_vest['user20'][0], '100%', '版主管理ID'),
        form_text('uf20', $ax_vest['user20'][1], '100%', '发帖马甲ID'),
        form_text('uh20', $ax_vest['user20'][2], '100%', '回帖马甲ID'),
        form_text('ub20', $ax_vest['user20'][3], '100%', '备注')
    );
    include _include(APP_PATH . 'plugin/ax_vest/setting.htm');


} else {
    $ax_vest['user1'] = $user1;
    $ax_vest['user2'] = $user2;
    $ax_vest['user3'] = $user3;
    $ax_vest['user4'] = $user4;
    $ax_vest['user5'] = $user5;
    $ax_vest['user6'] = $user6;
    $ax_vest['user7'] = $user7;
    $ax_vest['user8'] = $user8;
    $ax_vest['user9'] = $user9;
    $ax_vest['user10'] = $user10;
    $ax_vest['user11'] = $user11;
    $ax_vest['user12'] = $user12;
    $ax_vest['user13'] = $user13;
    $ax_vest['user14'] = $user14;
    $ax_vest['user15'] = $user15;
    $ax_vest['user16'] = $user16;
    $ax_vest['user17'] = $user17;
    $ax_vest['user18'] = $user18;
    $ax_vest['user19'] = $user19;
    $ax_vest['user20'] = $user20;
    kv_set('ax_vest', $ax_vest);
    message(0, '修改成功');
}


?>
