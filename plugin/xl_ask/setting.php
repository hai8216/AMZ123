<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') AND exit('Access Denied.');
if (param('do') == 'add') {

} elseif (param('do') == 'randadd') {
    $regData = file_get_contents('http://discuz.csdn123.net/avatar/get_user.php');
    $randUser = unserialize($regData);
    if(param('type')){
        $ask_type = 2;
    }else{
        $ask_type = 1;
    }
//    print_r($randUser);exit;
    //创建
    foreach ($randUser as $v) {
//        $longip = "127.0.0.1";
        $time = time();
        $email = $v['m'];
        $username = $v['u'];
        $password = 'randaskTiwen';
        $salt = xn_rand(16);
        $pwd = md5($password . $salt);
        $gid = 101;
        $_user = array(
            'username' => $username,
            'email' => $email,
            'password' => $pwd,
            'salt' => $salt,
            'gid' => $gid,
            'create_ip' => $longip,
            'create_date' => $time,
            'logins' => 1,
            'login_date' => $time,
            'login_ip' => $longip,
            'ask_type'=>$ask_type,
        );
        $uid = user_create($_user);
        $uid === FALSE AND message('email', lang('user_create_failed'));
        $user = user_read($uid);
        //哈希头像
        require_once(APP_PATH."/plugin/jan_identicon/Identicon/Identicon.php");

        $identicon = new Identicon();
        $string = $_user['username'];
        $size = 50;
        $data = $identicon->getImageData($string, $size);
        $filename = "$uid.png";
        $dir = substr(sprintf("%09d", $uid), 0, 3).'/';
        $path = $conf['upload_path'].'avatar/'.$dir;
        $url = $conf['upload_url'].'avatar/'.$dir.$filename;
        !is_dir($path) AND (mkdir($path, 0777, TRUE) OR message(-2, lang('directory_create_failed')));
        file_put_contents($path.$filename, $data) OR message(-1, lang('write_to_file_failed'));
        user_update($uid, array('avatar'=>$time));
    }

    message(0, jump('添加成功', url('plugin-setting-xl_ask')));
} elseif (param('do') == 2) {
    if ($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('id', 0);
    $r = db_update('user', array('uid' => $_codeid),array('ask_type'=>0));
    $r === FALSE AND message(-1, lang('delete_failed'));
} elseif (param('do') == 'delcj') {
    if ($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('id', 0);
    $r = db_delete('thread_tmp', array('id' => $_codeid));
    $r === FALSE AND message(-1, lang('delete_failed'));
}elseif (param('do') == 'delcjpl') {
    if ($method != 'POST') message(-1, 'Method Error.');
    $_codeid = param('id', 0);
    $r = db_delete('thread_post_tmp', array('id' => $_codeid));
    $r === FALSE AND message(-1, lang('delete_failed'));
}elseif (param('do') == 'huida') {
    $page = param(3, 1);
    $pagesize = 40;
    $n = db_count('user', array('ask_type'=>2));
    $pagination = pagination(url("plugin-setting-xl_ask-{page}"), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_ask-{page}"), $n, $page, $pagesize);
    $speciallist = db_find('user',  array('ask_type'=>2), array(), $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_ask/view/list.htm");
}elseif (param('do') == 'caiji') {
    $page = param(3, 1);
    $pagesize = 40;
    $n = db_count('thread_tmp', array());
    $pagination = pagination(url("plugin-setting-xl_ask-{page}",array('do'=>'caiji')), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_ask-{page}",array('do'=>'caiji')), $n, $page, $pagesize);
    $speciallist = db_find('thread_tmp',  array(), array(), $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_ask/view/caiji.htm");
}elseif (param('do') == 'caijipl') {
    $page = param(3, 1);
    $pagesize = 40;
    $n = db_count('thread_post_tmp', array('tmpid'=>param('tmpid')));
    $pagination = pagination(url("plugin-setting-xl_ask-{page}",array('do'=>'caijipl','tmpid'=>param('tmpid'))), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_ask-{page}",array('do'=>'caijipl','tmpid'=>param('tmpid'))), $n, $page, $pagesize);
    $speciallist = db_find('thread_post_tmp',  array('tmpid'=>param('tmpid')), array(), $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_ask/view/caijipl.htm");
}else {
    $page = param(3, 1);
    $pagesize = 40;
    $n = db_count('user', array('ask_type'=>1));
    $pagination = pagination(url("plugin-setting-xl_ask-{page}"), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_ask-{page}"), $n, $page, $pagesize);
    $speciallist = db_find('user',  array('ask_type'=>1), array(), $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_ask/view/list.htm");
}