<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') and exit('Access Denied.');
$pluginname = 'xl_findpage';
$action = param(3) ?: "list";
if ($action == 'model') {
    //找服务
    $page = param(4, 1);
    $pagesize = 30;
    $keywords = search_keyword_safe(xn_urldecode(param('keywords', '')));
    $srchtype = param('srchtype', '');
    if ($keywords) {
        if ($srchtype) {
            $map['id'] = $keywords;
        } else {
            $map['name'] = array("LIKE" => $keywords);
        }
    }
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('find_fuwu', $map, array('radio_1' => '-1', 'radio_2' => '-1', 'radio_3' => '-1', 'id' => '-1'), $page, $pagesize);
    $c = db_count("find_fuwu", $map);
    $pagination = pagination(url("plugin-setting-xl_findpage-model-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_findpage/view/model-list.html');
}elseif($action=='savetop'){
    $top = param('topk');
    kv_set("findwuliu_savetop",$top);
    message(0, jump('保存成功', url('plugin-setting-xl_findpage-wuliu')));
}elseif($action=='savetophaiwaicang'){
    $top = param('topk');
    kv_set("findhaiwaicang_savetop",$top);
    message(0, jump('保存成功', url('plugin-setting-xl_findpage-haiwaicang')));
} elseif($action=='video'){
    if ($method == 'GET') {
        $toid = param('id');
        $info = db_find_one('find_fuwu', ["id"=>$toid]);
        include _include(APP_PATH . 'plugin/xl_findpage/action/view/video.html');
    }else{
        $input['video'] = param('fileid');
        db_update("find_fuwu", array('id' => param('toid')), $input);
        message(0, jump('保存成功', url('plugin-setting-xl_findpage-video',["id"=>param('toid')])));
    }
}elseif($action=='delQrcode2') {
    db_update("find_fuwu", array('id' => param('toid')), ["qrcode2"=>'']);
    message(0, jump('保存成功', url('plugin-setting-xl_findpage-wsfuwu',["toid"=>param('toid')])));
}elseif($action=='delvideo'){
    $input['video'] = 0;
    db_update("find_fuwu", array('id' => param('toid')), $input);
    message(0, jump('保存成功', url('plugin-setting-xl_findpage-video',["id"=>param('toid')])));
}elseif ($action == 'setradio') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("find_fuwu", array('id' => param('id')));
        } else {
            $data = array();
        }
        $input['radio_1'] = form_radio('radio_1', array('0' => '关', 1 => "开"), $data['radio_1']);
        $input['color_1'] = form_radio('color_1', array('0' => '白色', 1 => "灰色"), $data['color_1']);
        $input['color_2'] = form_radio('color_2', array('0' => '白色', 1 => "灰色"), $data['color_2']);
        $input['color_3'] = form_radio('color_3', array('0' => '白色', 1 => "灰色"), $data['color_3']);
        $input['radio_2'] = form_radio('radio_2', array('0' => '关', 1 => "开"), $data['radio_2']);
        $input['radio_3'] = form_radio('radio_3', array('0' => '关', 1 => "开"), $data['radio_3']);
        $input['domain'] = form_text('domain', $data['domain']);
        include _include(APP_PATH . 'plugin/xl_findpage/view/radio.html');
    } else {
        $data['radio_1'] = param('radio_1');
        $data['radio_2'] = param('radio_2');
        $data['radio_3'] = param('radio_3');
        $data['color_1'] = param('color_1');
        $data['color_2'] = param('color_2');
        $data['color_3'] = param('color_3');
        $data['domain'] = param('domain');
        db_update("find_fuwu", array('id' => param('id')), $data);
        message(0, jump('保存成功', url('plugin-setting-xl_findpage-model')));
    }

} elseif ($action == 'list') {
    //找服务
    $page = param(4, 1);
    $pagesize = 30;
    $keywords = search_keyword_safe(xn_urldecode(param('keywords', '')));
    $srchtype = param('srchtype', '');
    if ($keywords) {
        if ($srchtype) {
            $map['desc'] = array("LIKE" => $keywords);
        } else {
            $map['name'] = array("LIKE" => $keywords);
        }

    }
    $map['model_type'] = 0;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('find_fuwu', $map, ['type' => 1, 'id' => '-1'], $page, $pagesize);
    $c = db_count("find_fuwu", $map);
    $search_right = kv_get("fuwu_search_right");
    $pagination = pagination(url("plugin-setting-xl_findpage-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_findpage/view/list.html');
} elseif ($action == 'saveright') {
    $search_right['search_right_text'] = param('search_right_text');
    $search_right['search_right_links'] = param('search_right_links');
    kv_set(param('savetype') . "_search_right", $search_right);
    message(0, '成功');
} elseif ($action == 'datalist') {
    $page = param(4, 1);
    $formid = param('toid');
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $data = db_find_one("find_fuwu", array('id' => $formid));
    $arrlist = db_find('find_fuwu_data', array('formid' => $formid), array(), $page, $pagesize);
    $c = db_count("find_fuwu_data", array('formid' => $formid));
    $pagination = pagination(url("plugin-setting-xl_findpage-datalist-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_findpage/view/datalist.html');
} elseif ($action == 'wuliu') {
    $page = param(4, 1);
    $pagesize = 30;
    $map['model_type'] = 1;
    $keywords = search_keyword_safe(xn_urldecode(param('keywords', '')));
    $srchtype = param('srchtype', '');
    if ($keywords) {
        if ($srchtype == '1') {
            $map['desc'] = array("LIKE" => $keywords);
        } elseif ($srchtype == '2') {
            $map['name2'] = array("LIKE" => $keywords);
        } else {
            $map['name'] = array("LIKE" => $keywords);
        }

    }
    $start = ($page - 1) * $pagesize;

    $datas = db_find("find_fuwu_cate", array('type' => 2), [], 1, 500);
    foreach ($datas as $v) {
        $s[$v['cid']] = $v['name'];
    }
    $search_right = kv_get("wuliu_search_right");
    $arrlist = db_find('find_fuwu', $map, array('hide' => 1, 'id' => '-1'), $page, $pagesize);
    $c = db_count("find_fuwu", $map);
    $pagination = pagination(url("plugin-setting-xl_findpage-wuliu-{page}"), $c, $page, $pagesize);
    $topk = kv_get("findwuliu_savetop");
    include _include(APP_PATH . 'plugin/xl_findpage/view/wuliulist.html');
} elseif ($action == 'haiwaicang'){
    $page = param(4, 1);
    $pagesize = 30;
    $map['model_type'] = 4;
    $keywords = search_keyword_safe(xn_urldecode(param('keywords', '')));
    $srchtype = param('srchtype', '');
    if ($keywords) {
        if ($srchtype == '1') {
            $map['desc'] = array("LIKE" => $keywords);
        } elseif ($srchtype == '2') {
            $map['name2'] = array("LIKE" => $keywords);
        } else {
            $map['name'] = array("LIKE" => $keywords);
        }

    }
    $start = ($page - 1) * $pagesize;

    $datas = db_find("find_fuwu_cate", array('type' => 4), [], 1, 500);
    foreach ($datas as $v) {
        $s[$v['cid']] = $v['name'];
    }
    $search_right = kv_get("haiwaicang_search_right");
    $arrlist = db_find('find_fuwu', $map, array('hide' => 1, 'id' => '-1'), $page, $pagesize);
    $c = db_count("find_fuwu", $map);
    $pagination = pagination(url("plugin-setting-xl_findpage-haiwaicang-{page}"), $c, $page, $pagesize);
    $topk = kv_get("findhaiwaicang_savetop");
    include _include(APP_PATH . 'plugin/xl_findpage/view/haiwaicanglist.html');
} elseif ($action == 'pro') {
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $datas = db_find("find_fuwu_cate", array('type' => 3), array(), 1, 500);
    foreach ($datas as $v) {
        $s[$v['cid']] = $v['name'];
    }
    $keywords = search_keyword_safe(xn_urldecode(param('keywords', '')));

    $srchtype = param('srchtype', '');
    if ($keywords) {
        if ($srchtype) {
            $map['desc'] = array("LIKE" => $keywords);
        } else {
            $map['name'] = array("LIKE" => $keywords);
        }

    }
    $map['model_type'] = 2;
    $search_right = kv_get("pro_search_right");
    $arrlist = db_find('find_fuwu', $map, array(), $page, $pagesize);
    $c = db_count("find_fuwu", $map);
    $pagination = pagination(url("plugin-setting-xl_findpage-pro-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_findpage/view/prolist.html');
} elseif ($action == 'adcate') {
    $type = param(4, 0);
    if ($method == 'GET') {
        if (param('cid')) {
            $data = db_find_one("find_fuwu_cate", array('cid' => param('cid')));
        } else {
            $data = array('cid' => 0);
        }

        $input['fup'] = form_select('fup', array('0' => '顶级'), $data['type']);
        $input['name'] = form_text('name', $data['name']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        $input['type'] = form_select('type', array('1' => '服务分类', 2 => '物流分类', 3 => '产品分类',4 => '海外仓分类'), $data['type']);
        include _include(APP_PATH . 'plugin/xl_findpage/view/adcate.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/fuwu_icon/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $wwwpath . $new_file)) {
                $data['icon'] = $wwwpath . $new_file;
            }
        }

        $data['name'] = param('name');
        $data['fup'] = param('fup');
        $data['order_id'] = param('order_id');
        $data['type'] = param('type');
        if (param('cid')) {
            db_update("find_fuwu_cate", array('cid' => param('cid')), $data);
        } else {
            db_insert("find_fuwu_cate", $data);
        }
        message(0, jump('保存成功', url('plugin-setting-xl_findpage-cate-' . $data['type'])));
    }
} elseif ($action == 'deledata') {
    $data = db_find_one("find_fuwu_data", array('id' => param('pid')));
    db_delete("find_fuwu_data", array('id' => param('pid')));
    message(0, jump('删除成功', url('plugin-setting-xl_findpage-datalist-' . $data['formid'])));
} elseif ($action == 'adddata') {
    $type = param(4, 0);
    if ($method == 'GET') {
        if (param('pid')) {
            $data = db_find_one("find_fuwu_data", array('id' => param('pid')));
        } else {
            $data = array('id' => 0);
        }
        $toid = param('toid');
        $input['name'] = form_text('name', $data['title']);
        $input['desc'] = form_text('desc', $data['desc']);
        $input['links'] = form_text('links', $data['links']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        include _include(APP_PATH . 'plugin/xl_findpage/view/adddata.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/fuwu_icon/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $wwwpath . $new_file)) {
                $data['icon'] = $wwwpath . $new_file;
            }
        }
        $data['title'] = param('name');
        $data['desc'] = param('desc');
        $data['links'] = param('links');
        $data['formid'] = param('formid');
        $data['order_id'] = param('order_id');;
        if (param('pid')) {
            db_update("find_fuwu_data", array('id' => param('pid')), $data);
        } else {
            db_insert("find_fuwu_data", $data);
        }
        message(0, jump('保存成功', url('plugin-setting-xl_findpage-datalist-' . $data['formid'])));
    }
} elseif ($action == 'adwuliu') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("find_fuwu", array('id' => param('id')));
        } else {
            $data = array('id' => 0, 'cate1' => 0, 'cate2' => 0, 'cate3' => 0, 'cate4' => 0);
        }
        $catelist = db_find("find_fuwu_cate", array('type' => 2, 'fup' => 0));
        $input['name'] = form_text('name', $data['name']);
        $input['desc'] = form_textarea('desc', $data['desc']);
        $input['hide'] = form_select('hide', array('0' => '显示', '1' => '隐藏'), $data['hide']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        $input['domain'] = form_text('domain', $data['domain']);

        include _include(APP_PATH . 'plugin/xl_findpage/view/adwuliu.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/fuwu_icon/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $wwwpath . $new_file)) {
                $data['icon'] = $wwwpath . $new_file;
            }
        }
        $data['cate1'] = implode(",", json_decode(param('cate1', '', false), true));
        $data['cate2'] = implode(",", json_decode(param('cate2', '', false), true));
        $data['cate3'] = implode(",", json_decode(param('cate3', '', false), true));
        $data['cate4'] = implode(",", json_decode(param('cate4', '', false), true));
        $data['name'] = param('name');
        $data['hide'] = param('hide');
        $data['desc'] = param('desc');
        $data['domain'] = param('domain');

        $data['order_id'] = param('order_id');
        $cateary = array_merge(json_decode(param('cate1', '', false)), json_decode(param('cate2', '', false)), json_decode(param('cate3', '', false)), json_decode(param('cate4', '', false)));
        if (param('id')) {
            db_update("find_fuwu", array('id' => param('id')), $data);
            db_delete("find_wuliu_pk", array('wuliu_id' => param('id')));
            foreach (json_decode(param('cate1', '', false)) as $k => $v) {
                db_insert("find_wuliu_pk", array('wuliu_id' => param('id'), 'type' => 1, 'cateid' => $v));
            }
            foreach (json_decode(param('cate2', '', false)) as $k => $v) {
                db_insert("find_wuliu_pk", array('wuliu_id' => param('id'), 'type' => 2, 'cateid' => $v));
            }
            foreach (json_decode(param('cate3', '', false)) as $k => $v) {
                db_insert("find_wuliu_pk", array('wuliu_id' => param('id'), 'type' => 3, 'cateid' => $v));
            }
            foreach (json_decode(param('cate4', '', false)) as $k => $v) {
                db_insert("find_wuliu_pk", array('wuliu_id' => param('id'), 'type' => 4, 'cateid' => $v));
            }
        } else {
            $data['model_type'] = 1;
            $wuliuid = db_insert("find_fuwu", $data);
            if ($wuliuid) {
                foreach (json_decode(param('cate1', '', false)) as $k => $v) {
                    db_insert("find_wuliu_pk", array('wuliu_id' => $wuliuid, 'type' => 1, 'cateid' => $v));
                }
                foreach (json_decode(param('cate2', '', false)) as $k => $v) {
                    db_insert("find_wuliu_pk", array('wuliu_id' => $wuliuid, 'type' => 2, 'cateid' => $v));
                }
                foreach (json_decode(param('cate3', '', false)) as $k => $v) {
                    db_insert("find_wuliu_pk", array('wuliu_id' => $wuliuid, 'type' => 3, 'cateid' => $v));
                }
                foreach (json_decode(param('cate4', '', false)) as $k => $v) {
                    db_insert("find_wuliu_pk", array('wuliu_id' => $wuliuid, 'type' => 4, 'cateid' => $v));
                }
            }
        }
        message(0, jump('保存成功', url('plugin-setting-xl_findpage-wuliu')));
    }
} elseif($action == 'adhaiwaicang'){
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("find_fuwu", array('id' => param('id')));
        } else {
            $data = array('id' => 0, 'cate1' => 0, 'cate2' => 0, 'cate3' => 0, 'cate4' => 0);
        }
        $catelist = db_find("find_fuwu_cate", array('type' => 4, 'fup' => 0));
        $input['name'] = form_text('name', $data['name']);
        $input['desc'] = form_textarea('desc', $data['desc']);
        $input['hide'] = form_select('hide', array('0' => '显示', '1' => '隐藏'), $data['hide']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        $input['domain'] = form_text('domain', $data['domain']);

        include _include(APP_PATH . 'plugin/xl_findpage/view/adhaiwaicang.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/haiwaicang_icon/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $wwwpath . $new_file)) {
                $data['icon'] = $wwwpath . $new_file;
            }
        }
        $data['cate1'] = implode(",", json_decode(param('cate1', '', false), true));
        $data['cate2'] = implode(",", json_decode(param('cate2', '', false), true));
        $data['cate3'] = implode(",", json_decode(param('cate3', '', false), true));
        $data['cate4'] = implode(",", json_decode(param('cate4', '', false), true));
        $data['name'] = param('name');
        $data['hide'] = param('hide');
        $data['desc'] = param('desc');
        $data['domain'] = param('domain');

        $data['order_id'] = param('order_id');
        $cateary = array_merge(json_decode(param('cate1', '', false)), json_decode(param('cate2', '', false)), json_decode(param('cate3', '', false)), json_decode(param('cate4', '', false)));
        if (param('id')) {
            db_update("find_fuwu", array('id' => param('id')), $data);
            db_delete("find_haiwaicang_pk", array('haiwaicang_id' => param('id')));
            foreach (json_decode(param('cate1', '', false)) as $k => $v) {
                db_insert("find_haiwaicang_pk", array('haiwaicang_id' => param('id'), 'type' => 1, 'cateid' => $v));
            }
            foreach (json_decode(param('cate2', '', false)) as $k => $v) {
                db_insert("find_haiwaicang_pk", array('haiwaicang_id' => param('id'), 'type' => 2, 'cateid' => $v));
            }
            foreach (json_decode(param('cate3', '', false)) as $k => $v) {
                db_insert("find_haiwaicang_pk", array('haiwaicang_id' => param('id'), 'type' => 3, 'cateid' => $v));
            }
            foreach (json_decode(param('cate4', '', false)) as $k => $v) {
                db_insert("find_haiwaicang_pk", array('haiwaicang_id' => param('id'), 'type' => 4, 'cateid' => $v));
            }
        } else {
            $data['model_type'] = 4;
            $wuliuid = db_insert("find_fuwu", $data);
            if ($wuliuid) {
                foreach (json_decode(param('cate1', '', false)) as $k => $v) {
                    db_insert("find_haiwaicang_pk", array('haiwaicang_id' => $wuliuid, 'type' => 1, 'cateid' => $v));
                }
                foreach (json_decode(param('cate2', '', false)) as $k => $v) {
                    db_insert("find_haiwaicang_pk", array('haiwaicang_id' => $wuliuid, 'type' => 2, 'cateid' => $v));
                }
                foreach (json_decode(param('cate3', '', false)) as $k => $v) {
                    db_insert("find_haiwaicang_pk", array('haiwaicang_id' => $wuliuid, 'type' => 3, 'cateid' => $v));
                }
                foreach (json_decode(param('cate4', '', false)) as $k => $v) {
                    db_insert("find_haiwaicang_pk", array('haiwaicang_id' => $wuliuid, 'type' => 4, 'cateid' => $v));
                }
            }
        }
        message(0, jump('保存成功', url('plugin-setting-xl_findpage-haiwaicang')));
    }
}elseif ($action == 'adpro') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("find_fuwu", array('id' => param('id')));
        } else {
            $data = array('id' => 0, 'cate' => 0);
        }
        $catelist = db_find("find_fuwu_cate", array('type' => 3, 'fup' => 0));
        foreach ($catelist as $v) {
            $s[$v['cid']] = $v['name'];
        }
        $input['name'] = form_text('name', $data['name']);
        $input['cate'] = form_select('cate', $s, $data['cate']);
        $input['desc'] = form_textarea('desc', $data['desc']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        $input['hide'] = form_select('hide', array('0' => '显示', '1' => '隐藏'), $data['hide']);
        include _include(APP_PATH . 'plugin/xl_findpage/view/adpro.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/fuwu_icon/" . date('Ymd', $time) . "/";
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $wwwpath . $new_file)) {
                $data['icon'] = $wwwpath . $new_file;
            }
        }
        $data['cate'] = param('cate');
        $data['name'] = param('name');
        $data['hide'] = param('hide');
        $data['desc'] = param('desc');
        $data['order_id'] = param('order_id');
        if (param('id')) {
            db_update("find_fuwu", array('id' => param('id')), $data);
        } else {
            $data['model_type'] = 2;
            db_insert("find_fuwu", $data);
        }
        message(0, jump('保存成功', url('plugin-setting-xl_findpage-wuliu')));
    }
} elseif
($action == 'addcate2') {
    if ($method == 'GET') {
        if (param('cid')) {
            $data = db_find_one("find_fuwu_cate", array('cid' => param('cid')));
        } else {
            $data = array('cid' => 0);
        }
        if (param('fup')) {
            $fupd = db_find_one("find_fuwu_cate", array('cid' => param('fup')));
            $data['type'] = $fupd['type'];
            $data['fup'] = $fupd['cid'];
        }
        $datas = db_find("find_fuwu_cate", array('type' => 2, 'fup' => 0));
        foreach ($datas as $v) {
            $s[$v['cid']] = $v['name'];
        }
        $types = array('1' => '服务分类', 2 => '物流分类', 3 => '产品分类',4 => '海外仓分类');
        $input['fup'] = form_select('fup', $s, $data['fup']);
        $input['name'] = form_text('name', $data['name']);
        $input['type'] = $types[$data['type']];
        include _include(APP_PATH . 'plugin/xl_findpage/view/adcate2.html');
    } else {
        $data['name'] = param('name');
        $data['fup'] = param('fup');
        $data['type'] = param('type');
        if (param('cid')) {
            db_update("find_fuwu_cate", array('cid' => param('cid')), $data);
        } else {
            db_insert("find_fuwu_cate", $data);
        }
        message(0, jump('保存成功'));
    }
} elseif
($action == 'addcate4') {
    if ($method == 'GET') {
        if (param('cid')) {
            $data = db_find_one("find_fuwu_cate", array('cid' => param('cid')));
        } else {
            $data = array('cid' => 0);
        }
        if (param('fup')) {
            $fupd = db_find_one("find_fuwu_cate", array('cid' => param('fup')));
            $data['type'] = $fupd['type'];
            $data['fup'] = $fupd['cid'];
        }
        $datas = db_find("find_fuwu_cate", array('type' => 4, 'fup' => 0));
        foreach ($datas as $v) {
            $s[$v['cid']] = $v['name'];
        }
        $types = array('1' => '服务分类', 2 => '物流分类', 3 => '产品分类',4 => '海外仓分类');
        $input['fup'] = form_select('fup', $s, $data['fup']);
        $input['name'] = form_text('name', $data['name']);
        $input['type'] = $types[$data['type']];
        include _include(APP_PATH . 'plugin/xl_findpage/view/adcate4.html');
    } else {
        $data['name'] = param('name');
        $data['fup'] = param('fup');
        $data['type'] = param('type');
        if (param('cid')) {
            db_update("find_fuwu_cate", array('cid' => param('cid')), $data);
        } else {
            db_insert("find_fuwu_cate", $data);
        }
        message(0, jump('保存成功'));
    }
}elseif ($action == 'cate') {
    $type = param(4, 0);
    if ($type == '2') {
        $t = '物流分类';
    } elseif ($type == '1') {
        $t = '服务分类';
    } elseif ($type == '3') {
        $t = '产品分类';
    }elseif ($type == '4'){
        $t = '海外仓分类';
    }
    if (!$type) {
        message(0, jump('请从功能页进入', url('plugin-setting-xl_findpage')));
    }
    $page = param(5, 1);
    $pagesize = 100;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('find_fuwu_cate', array('type' => $type, 'fup' => 0), array('order_id' => '-1'), $page, $pagesize);
    $c = db_count("find_fuwu_cate", array('type' => $type));
    $pagination = pagination(url("plugin-setting-xl_findpage-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_findpage/view/cate.html');
} elseif ($action == 'adfuwu') {

    if ($method == 'GET') {
        if (param('toid')) {
            $data = db_find_one("find_fuwu", array('id' => param('toid')));
        } else {
            $data = array('id' => 0);
        }
        $cate = db_find("find_fuwu_cate", array('type' => 1, 'fup' => 0), array(), 0, 30);
        foreach ($cate as $v) {
            $cates[$v['cid']] = $v['name'];
        }
        $input['name'] = form_text('name', $data['name']);
        $input['desc'] = form_text('desc', $data['desc']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        $input['cate'] = form_select('cate', $cates, $data['cate']);
        $input['domain'] = form_text('domain', $data['domain']);

        $input['type'] = form_select('type', array('1' => '推荐', 0 => '正常', '2' => '隐藏'), $data['type']);
        include _include(APP_PATH . 'plugin/xl_findpage/view/adfuwu.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/fuwu_icon/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $wwwpath . $new_file)) {
                $datas['icon'] = $wwwpath . $new_file;
            }
        }
        $datas['name'] = param('name');
        $datas['desc'] = param('desc');
        $datas['cate'] = param('cate');
        $datas['type'] = param('type');
        $datas['order_id'] = param('order_id');
        $datas['dateline'] = time();
        $datas['domain'] = param('domain');

        if (param('toid')) {
            db_update("find_fuwu", array('id' => param('toid')), $datas);
        } else {
            db_insert("find_fuwu", $datas);
        }
        $catelistabc = db_find("find_fuwu_cate", array('type' => 1, 'fup' => 0), array(), 0, 50);
        foreach ($catelistabc as $va) {
            $count = db_count("find_fuwu", array('cate' => $va['cid']));
            db_update("find_fuwu_cate", array('cid' => $va['cid']), array('count' => $count));
        }
//        message(0, jump('保存成功', url('plugin-setting-xl_findpage')));
    }
} elseif ($action == 'wsfuwu') {
    if ($method == 'GET') {
        $data = db_find_one("find_fuwu", array('id' => param('toid')));
        $input['seo_title'] = form_text('seo_title', $data['seo_title']);
        $input['tab'] = form_textarea('tab', $data['tab'], '', 150);
        $input['qq'] = form_text('qq', $data['qq']);
        $input['gwlinks'] = form_text('gwlinks', $data['gwlinks']);
        $input['domain2'] = form_text('domain2', $data['domain2']);
        $input['mobile'] = form_text('mobile', $data['mobile']);
        $input['seo_keyword'] = form_text('seo_keyword', $data['seo_keyword']);
        $input['seo_desc'] = form_text('seo_desc', $data['seo_desc']);
        $input['name'] = form_text('name', $data['name2']);
        $input['tips'] = form_text('tips', $data['tips'] ? $data['tips'] : "微信扫一扫");
        $input['desc'] = form_textarea('desc', $data['desc2']);
        $input['wztype'] = form_select('wztype', array(0 => '用户uid', 1 => '标签'), $data['wztype']);
        $input['wzvalue'] = form_text('wzvalue', $data['wzvalue']);
        include _include(APP_PATH . 'plugin/xl_findpage/view/wsfuwu.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/remote/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/remote/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $wwwpath . $new_file)) {
                $data['qrcode'] = $wwwpath . $new_file;
            }
        }
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon2'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/remote/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon2'))), $wwwpath . $new_file)) {
                $data['icon2'] = $wwwpath . $new_file;
            }
        }


        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon3'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/fuwu_icon/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon3'))), $wwwpath . $new_file)) {
                $data['banner'] = $wwwpath . $new_file;
            }
        }
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon4'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/fuwu_icon/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon4'))), $wwwpath . $new_file)) {
                $data['qrcode2'] = $wwwpath . $new_file;
            }
        }
        $data['name2'] = param('name', '', false);
        $data['desc2'] = param('desc', '', false);
        $data['tips'] = param('tips', '', false);
        $data['com_html'] = param('message', '', false);
        $data['pro_html'] = param('message2', '', false);
        $data['seo_title'] = param('seo_title', '', false);
        $data['wztype'] = param('wztype', '', false);
        $data['wzvalue'] = param('wzvalue', '', false);
        $data['seo_keyword'] = param('seo_keyword', '', false);
        $data['seo_desc'] = param('seo_desc', '', false);
        $data['qq'] = param('qq', '', false);
        $data['gwlinks'] = param('gwlinks', '', false);
        $data['domain2'] = param('domain2', '', false);
        $data['mobile'] = param('mobile', '', false);
        $data['tab'] = param('tab', '', false);
        if (param('toid')) {
            db_update("find_fuwu", array('id' => param('toid')), $data);
        }
        message(0, jump('保存成功', url('plugin-setting-xl_findpage-wsfuwu', array('toid' => param('toid')))));
    }
} elseif ($action == 'wshaiwaicang'){
    if ($method == 'GET') {
        $data = db_find_one("find_fuwu", array('id' => param('toid')));
        $input['seo_title'] = form_text('seo_title', $data['seo_title']);
        $input['tab'] = form_textarea('tab', $data['tab'], '', 150);
        $input['qq'] = form_text('qq', $data['qq']);
        $input['gwlinks'] = form_text('gwlinks', $data['gwlinks']);
        $input['domain2'] = form_text('domain2', $data['domain2']);
        $input['mobile'] = form_text('mobile', $data['mobile']);
        $input['seo_keyword'] = form_text('seo_keyword', $data['seo_keyword']);
        $input['seo_desc'] = form_text('seo_desc', $data['seo_desc']);
        $input['name'] = form_text('name', $data['name2']);
        $input['tips'] = form_text('tips', $data['tips'] ? $data['tips'] : "微信扫一扫");
        $input['desc'] = form_textarea('desc', $data['desc2']);
        $input['wztype'] = form_select('wztype', array(0 => '用户uid', 1 => '标签'), $data['wztype']);
        $input['wzvalue'] = form_text('wzvalue', $data['wzvalue']);
        include _include(APP_PATH . 'plugin/xl_findpage/view/wshaiwaicang.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/remote/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/remote/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $wwwpath . $new_file)) {
                $data['qrcode'] = $wwwpath . $new_file;
            }
        }
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon2'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/remote/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon2'))), $wwwpath . $new_file)) {
                $data['icon2'] = $wwwpath . $new_file;
            }
        }


        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon3'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/haiwaicang_icon/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon3'))), $wwwpath . $new_file)) {
                $data['banner'] = $wwwpath . $new_file;
            }
        }
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon4'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/haiwaicang_icon/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon4'))), $wwwpath . $new_file)) {
                $data['qrcode2'] = $wwwpath . $new_file;
            }
        }
        $data['name2'] = param('name', '', false);
        $data['desc2'] = param('desc', '', false);
        $data['tips'] = param('tips', '', false);
        $data['com_html'] = param('message', '', false);
        $data['pro_html'] = param('message2', '', false);
        $data['seo_title'] = param('seo_title', '', false);
        $data['wztype'] = param('wztype', '', false);
        $data['wzvalue'] = param('wzvalue', '', false);
        $data['seo_keyword'] = param('seo_keyword', '', false);
        $data['seo_desc'] = param('seo_desc', '', false);
        $data['qq'] = param('qq', '', false);
        $data['gwlinks'] = param('gwlinks', '', false);
        $data['domain2'] = param('domain2', '', false);
        $data['mobile'] = param('mobile', '', false);
        $data['tab'] = param('tab', '', false);
        if (param('toid')) {
            db_update("find_fuwu", array('id' => param('toid')), $data);
        }
        message(0, jump('保存成功', url('plugin-setting-xl_findpage-wshaiwaicang', array('toid' => param('toid')))));
    }
}elseif ($action == 'adbannerData') {
    if ($method == 'GET') {
        $data = [];
        $input['order'] = form_text('order', $data['order']);
        $input['link'] = form_text('link', $data['link']);
        include _include(APP_PATH . 'plugin/xl_findpage/view/addbanner.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/fuwu_banner/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $wwwpath . $new_file)) {
                $data['imgs'] = $wwwpath . $new_file;
            }
        }
        $data['link'] = param('link');
        $data['order'] = param('order');
        db_insert("find_fuwu_banner", $data);
        message(0, jump('新增成功', url('plugin-setting-xl_findpage')));
    }
} elseif ($action == 'adBanner') {
    $list = db_find("find_fuwu_banner", array(), array('order' => '-1'));
    include _include(APP_PATH . 'plugin/xl_findpage/view/banner.html');
} elseif ($action == 'deletewuliu') {
    $toid = param('toid');
    db_delete('find_fuwu', array('model_type' => 1, 'id' => $toid));
    message(0, jump('删除成功', url('plugin-setting-xl_findpage-wuliu')));
}
elseif ($action == 'deletehaiwaicang') {
    $toid = param('toid');
    db_delete('find_fuwu', array('model_type' => 4, 'id' => $toid));
    message(0, jump('删除成功', url('plugin-setting-xl_findpage-haiwaicang')));
} elseif ($action == 'delbanner') {
    $toid = param('toid');
    db_delete('find_fuwu_banner', array('id' => $toid));
    message(0, jump('删除成功', url('plugin-setting-xl_findpage-adBanner')));
} elseif ($action == 'deletelist') {
    $toid = param('toid');
    db_delete('find_fuwu', array('model_type' => 0, 'id' => $toid));
    message(0, jump('删除成功', url('plugin-setting-xl_findpage-list')));
} elseif ($action == 'deletpro') {
    $toid = param('toid');
    db_delete('find_fuwu', array('model_type' => 2, 'id' => $toid));
    message(0, jump('删除成功', url('plugin-setting-xl_findpage-pro')));
} elseif ($action == 'delcate') {
    $data['code'] = 0;
    $data['cid'] = param('cid');
    db_delete('find_fuwu_cate', array('cid' => param('cid')));
    echo json_encode($data);
    eixt;
} else {
    include _include(APP_PATH . 'plugin/xl_findpage/action/' . $action . '.php');
}

?>