<?php

//找服务
$ref = param('ref', 'list');
if ($ref == 'list') {
    $map['userid'] = param('id');
    $arrlist = db_find('find_fuwu_wenti', $map, ["order_id"=>"-1"], 1, 10);
    include _include(APP_PATH . 'plugin/xl_findpage/action/view/wenti.html');
} elseif ($ref == 'add') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("find_fuwu_wenti", array('id' => param('id')));
        } else {
            $data = [];
        }
        $toid = param('toid');
        $input['a'] = form_text('a', $data['a']);
        $input['q'] = form_text('q', $data['q']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        include _include(APP_PATH . 'plugin/xl_findpage/action/view/addwenti.html');
    } else {
        $data['q'] = param('q');
        $data['a'] = param('a');
        $data['userid'] = param('toid');
        $data['order_id'] = param('order_id');
        if (param('id')) {
            db_update("find_fuwu_wenti", array('id' => param('id')), $data);
        }else{
            db_insert("find_fuwu_wenti", $data);
        }

        message(0, jump('新增成功', url('plugin-setting-xl_findpage-setwenti',['id'=>$data['userid']])));
    }
}elseif($ref=='del'){
    $id = param('delid');
    if (!$id) {
        message(0, '错误的参数');
        exit;
    }
    db_delete("find_fuwu_wenti", ["id" => $id]);
    message(0, 'ok');
}