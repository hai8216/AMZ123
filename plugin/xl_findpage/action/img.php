<?php
//找服务
$ref = param('ref', 'list');
if ($ref == 'list') {
    $map['userid'] = param('id');
    $arrlist = db_find('find_fuwu_img', $map, ["order_id"=>"-1"]);
    include _include(APP_PATH . 'plugin/xl_findpage/action/view/img.html');
} elseif ($ref == 'add') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("find_fuwu_img", array('id' => param('id')));
        } else {
            $data = [];
        }
        $toid = param('toid');
        $input['type'] = form_select('type', ["0"=>'单图',1=>'轮播'],$data['type']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        $input['links'] = form_text('links', $data['links']);
        $input['name'] = form_text('name', $data['name']);
        $input['text'] = form_text('text', $data['text']);
        include _include(APP_PATH . 'plugin/xl_findpage/action/view/addimg.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/fuwu_banner/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('hidden_img_tpl'))), $wwwpath . $new_file)) {
                $data['thumb'] = $wwwpath . $new_file;
            }
        }
        $data['userid'] = param('toid');
        $data['type'] = param('type');
        $data['links'] = param('links');
        $data['name'] = param('name');
        $data['text'] = param('text');
        $data['order_id'] = param('order_id');
        if (param('id')) {
            db_update("find_fuwu_img", array('id' => param('id')), $data);
        }else{
            db_insert("find_fuwu_img", $data);
        }
        message(0, jump('新增成功', ''));
    }
}elseif($ref=='del'){
    $id = param('delid');
    if (!$id) {
        message(0, '错误的参数');
        exit;
    }
    db_delete("find_fuwu_img", ["id" => $id]);
    message(0, 'ok');
}