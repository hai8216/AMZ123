<?php
//找服务
$ref = param('ref', 'list');
if ($ref == 'list') {
    $map['userid'] = param('id');
    $arrlist = db_find('find_fuwu_haoping', $map, ["order_id"=>"-1"], 1, 6);
    include _include(APP_PATH . 'plugin/xl_findpage/action/view/haoping.html');
} elseif ($ref == 'add') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("find_fuwu_haoping", array('id' => param('id')));
        } else {
            $data = [];
        }
        $toid = param('toid');
        $input['uname'] = form_text('uname', $data['uname']);
        $input['desc'] = form_text('desc', $data['desc']);
        $input['message'] = form_text('message', $data['message']);
        $input['order_id'] = form_text('order_id', $data['order_id']);
        include _include(APP_PATH . 'plugin/xl_findpage/action/view/addhaoping.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $wwwpath = "upload/fuwu_banner/" . date('Ymd', $time) . "/";
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (saveTencentBase64(base64_decode(str_replace($result[1], '', param('favicon'))), $wwwpath . $new_file)) {
                $data['thumb'] = $wwwpath . $new_file;
            }
        }
        $data['uname'] = param('uname');
        $data['desc'] = param('desc');
        $data['userid'] = param('toid');
        $data['message'] = param('message');
        $data['order_id'] = param('order_id');
        if (param('id')) {
            db_update("find_fuwu_haoping", array('id' => param('id')), $data);
        }else{
            db_insert("find_fuwu_haoping", $data);
        }

        message(0, jump('新增成功', url('plugin-setting-xl_findpage-sethaoping',['id'=>$data['userid']])));
    }
}elseif($ref=='del'){
    $id = param('delid');
    if (!$id) {
        message(0, '错误的参数');
        exit;
    }
    db_delete("find_fuwu_haoping", ["id" => $id]);
    message(0, 'ok');
}