<?php

//找服务
$ref = param('ref', 'list');
if ($ref == 'list') {
    $map['userid'] = param('id');
    $arrlist = db_find('find_fuwu_tab', $map, [], 1, 10);
    include _include(APP_PATH . 'plugin/xl_findpage/action/view/tab.html');
} elseif ($ref == 'add') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("find_fuwu_tab", array('id' => param('id')));
        } else {
            $data = [];
        }
        $toid = param('toid');
        $input['tabName'] = form_text('tabName', $data['tabName']);
        $input['tabDomain'] = form_text('tabDomain', $data['tabDomain']);
        $input['seo_title'] = form_text("seo_title", $data['seo_title']);
        $input['seo_desc'] = form_text("seo_desc", $data['seo_desc']);
        $input['seo_keywords'] = form_text("seo_keywords", $data['seo_keywords']);
        $input['tag'] = form_text("tag", $data['tags']);
        $input['tag_and'] = form_text("tag_and", $data['tag_and']);
        $input['uids'] = form_text("uids", $data['uids']);
        include _include(APP_PATH . 'plugin/xl_findpage/action/view/addtab.html');
    } else {
        $input['seo_title'] = param("seo_title");
        $input['seo_desc'] = param("seo_desc");
        $input['seo_keywords'] = param("seo_keywords");
        $input['tabName'] = param("tabName");
        $input['tabDomain'] = param("tabDomain");
        $input['tag_and'] = param("tag_and");
        $input['tags'] = param("tag");
        $input['uids'] = param("uids");
        $input['userid'] = param('toid');

        if (param('id')) {
            db_update("find_fuwu_tab", array('id' => param('id')), $input);
        }else{
            db_insert("find_fuwu_tab", $input);
        }
        message(0, jump('新增成功', url('plugin-setting-xl_findpage-tab',['id'=>$input['userid']])));
    }
}elseif($ref=='del'){
    $id = param('delid');
    if (!$id) {
        message(0, '错误的参数');
        exit;
    }
    db_delete("find_fuwu_tab", ["id" => $id]);
    message(0, 'ok');
}