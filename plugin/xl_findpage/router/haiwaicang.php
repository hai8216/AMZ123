<?php
//TODO type=4 表示海外仓
$catelist = db_find("find_fuwu_cate", array('type' => 4, 'fup' => 0), array(), 0, 4);
foreach ($catelist as $v) {
    $catename[$v['cid']] = $v['name'];
}
$keyword = search_keyword_safe(xn_urldecode(param('keyword', '')));
$cate[1] = param(1, 0);
$cate[2] = param(2, 0);
$cate[3] = param(3, 0);
$cate[4] = param(4, 0);
function getUrl($k, $v)
{
    global $cate;
    if ($k == 0) {
        echo url('haiwaicang-' . $v['cid'] . '-' . $cate[2] . '-' . $cate[3] . '-' . $cate[4] . '');
    } elseif ($k == 1) {
        echo url('haiwaicang-' . $cate[1] . '-' . $v['cid'] . '-' . $cate[3] . '-' . $cate[4] . '');
    } elseif ($k == 2) {
        echo url('haiwaicang-' . $cate[1] . '-' . $cate[2] . '-' . $v['cid'] . '-' . $cate[4] . '');
    } elseif ($k == 3) {
        echo url('haiwaicang-' . $cate[1] . '-' . $cate[2] . '-' . $cate[3] . '-' . $v['cid']);
    }
}

function getUrl2($k)
{
    global $cate;
    if ($k == 0) {
        echo url('haiwaicang-0-' . $cate[2] . '-' . $cate[3] . '-' . $cate[4] . '');
    } elseif ($k == 1) {
        echo url('haiwaicang-' . $cate[1] . '-0-' . $cate[3] . '-' . $cate[4] . '');
    } elseif ($k == 2) {
        echo url('haiwaicang-' . $cate[1] . '-' . $cate[2] . '-0-' . $cate[4] . '');
    } elseif ($k == 3) {
        echo url('haiwaicang-' . $cate[1] . '-' . $cate[2] . '-' . $cate[3] . '-0');
    }

}

$sql = "";

if ($keyword) {
    $map['name'] = array('LIKE' => $keyword);
} else {
    if ($cate[1] > 0) {
        $map['cate1'] = array('LIKE' => $cate[1]);
    }
    if ($cate[2] > 0) {
        $map['cate2'] = array('LIKE' => $cate[2]);
        $sql .= " AND cateid=$cate[2] AND type=2";
    }
    if ($cate[3] > 0) {
        $map['cate3'] = array('LIKE' => $cate[3]);
        $sql .= " AND cateid=$cate[3] AND type=3";
    }
    if ($cate[4] > 0) {
        $map['cate4'] = array('LIKE' => $cate[4]);
        $sql .= " AND cateid=$cate[4] AND type=4";
    }
}
//
//echo "select * from bbs_find_haiwaicang_pk where id>0 ".$sql;
//$pk_ary = db_sql_find("select * from bbs_find_haiwaicang_pk where id>0 ".$sql."");
//foreach($pk_ary as $v){
//    $pkid[]=$v['haiwaicang_id'];
//}
if ($cate[3] == '57') {
    unset($map['cate4']);
    unset($map['cate2']);
}
$topk = kv_get("findhaiwaicang_savetop");

if ($topk && !$keyword && !$cate[1] &&  !$cate[2]  && !$cate[3]  && !$cate[4] ) {
    $sql .= " AND id not in(" . $topk . ")";
    $mapin['id'] = explode(",", $topk);
    $toplist = db_find("find_fuwu", $mapin, [], 1, 50);
    foreach($mapin['id'] as $k=>$v){
        $toplistPk[$v]=$k;
    }
    foreach( $toplist as $k=>$v){
        $toplist[$k]['k'] = $toplistPk[$v['id']];
    }

    $column = array_column($toplist,'k');

    array_multisort($column,SORT_ASC,$toplist);

}
$map['model_type'] = 4;
$map['hide'] = 0;

$page = param('page', 1);
$list = db_find("find_fuwu", $map, array(), $page, 50);
if ($topk) {
    foreach ($list as $k => $v) {
        if (in_array($v['id'], $mapin['id'])) {
            unset($list[$k]);
        }
    }
}
if ($cate[1] || $cate[3]) {
    $cateData = db_find_one("find_fuwu_cate", array('cid' => $cate[1]));
    $cateData2 = db_find_one("find_fuwu_cate", array('cid' => $cate[3]));
    $header['title'] = $cateData['name'] . $cateData2['name'] . "亚马逊FBA物流运费_头程费用-AMZ123跨境导航";
    $header['keywords'] = $cateData['name'] . $cateData2['name'] . ',' . $cateData['name'] . 'FBA,' . $cateData['name'] . '亚马逊物流,跨境物流';
    $header['description'] = $cateData['name'] . $cateData2['name'] . '价格查询，AMZ123为跨境卖家提供更加专业的物流公司推荐，包含FBA海运、FBA空运、FBA快递、专线小包、中欧铁路货运及海外仓等服务，助您一站式解决跨境物流问题。';
} else {
    $header['title'] = "找海外仓 - 找亚马逊FBA物流服务商 - AMZ123跨境导航";
    $header['keywords'] = '找海外仓,FBA物流,FBA物流服务商,亚马逊物流服务商,国际物流专线,跨境物流专线';
    $header['description'] = '找亚马逊海陆空运物流货代、专线小包服务商上AMZ123跨境导航！AMZ123为您提供丰富的跨境电商物流公司，助您找到合适的物流服务商！';
}
function shuffle_assoc($list)
{
    if (!is_array($list))
        return $list;

    $keys = array_keys($list);
    shuffle($keys);
    $random = array();
    foreach ($keys as $key)
        $random[$key] = shuffle_assoc($list[$key]);
    return $random;

}

$list = shuffle_assoc($list);
if (param('ajax')) {
    $json['code'] = 200;
    $json['c'] = count($list);
    $json['data'] = $list;
    echo json_encode($json);
    exit;
} else {
    include _include(APP_PATH . 'plugin/xl_findpage/htm/haiwaicang.html');
}
