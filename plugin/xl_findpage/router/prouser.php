<?php
//$Dpages = null;

if ($Dpages) {
    $udata = $Dpages;
} else {
    $ids = param(1);
    $udata = db_find_one("find_fuwu", array('id' => $ids));
    if($udata['domain']){
        header("Location: https://www.amz123.com/".$udata['domain']);
    }
}

if (!$udata['id']) {
    message(0, '数据不存在，请核对后在访问');
}
if (check_wap()) {
    $wap = "_4g";
} else {
    $wap = "";
}
$udatatag = explode(PHP_EOL, $udata['tab']);;
$header['title'] = $udata['seo_title'] ? $udata['seo_title'] : $header['title'];
$header['keywords'] = $udata['seo_keyword'] ? $udata['seo_keyword'] : $header['keywords'];
$header['description'] = $udata['seo_desc'] ? $udata['seo_desc'] : $header['description'];
if ($Dpages) {
    $otherDomain = param(1, 'index');
} else {
    $otherDomain = "index";
}
if ($otherDomain == 'index') {
    include _include(APP_PATH . 'plugin/xl_findpage/htm/prouser' . $wap . '.html');
} else {
    $domainInfo = db_find_one("find_fuwu_tab", array('userid' => $udata['id'], 'tabDomain' => $otherDomain));
    $header['title'] = $domainInfo['seo_title'];
    $header['keywords'] = $domainInfo['seo_keywords'];
    $header['description'] = $domainInfo['seo_description'];
    $page = param('page',1);
    if($domainInfo['tag_and']){
        if (!$domainInfo['uids']) {
            $threadlistdata = byTags_2($domainInfo['tags'], $domainInfo['tag_and'], $page);
        } else {
            $threadlistdata = byTagsoruids_2($domainInfo['tags'], $domainInfo['uids'], $domainInfo['tag_and'], $page);
        }
    }else{
        if (!$domainInfo['uids']) {
            $threadlistdata = byTags($domainInfo['tags'], 1);
        } else {
            $threadlistdata = byTagsoruids($domainInfo['tags'], $domainInfo['uids'], $page);
        }
    }
    if(param('loadajax')){
        ob_start();
        require_once(APP_PATH . 'plugin/xl_findpage/htm/prouser_news' . $wap . '_ajax.html');
        $html = ob_get_clean();
        $ajaxdata['page'] = $page;
        $ajaxdata['code'] = 200;
        if (!empty($threadlistdata)) {
            $ajaxdata['c'] = 1;
            $ajaxdata['threadlist'] = $html;
        } else {
            $ajaxdata['c'] = 0;
            $ajaxdata['threadlist'] = "";
        }
        echo json_encode($ajaxdata);
        exit;
    }else{
        include _include(APP_PATH . 'plugin/xl_findpage/htm/prouser_news' . $wap . '.html');
    }

}
