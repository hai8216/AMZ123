<?php
$header['title']  = '找产品 - 跨境电商选品平台 - AMZ123跨境导航';
$header['keywords']  = '找产品,选品,跨境电商货源,外贸货源';
$header['description']  = '找亚马逊/eBay/Shopee/独立站外贸热卖爆款货源上AMZ123跨境导航！AMZ123为跨境电商卖家提供海量优质外贸货源，帮卖家和工厂更好对接，助您找到合适的优质工厂！';
if (param('ajax')) {
    $map['model_type'] = 2;
    if(param('cate')){
        $map['cate'] = param('cate');
    }
    $map['hide'] = 0;
    $list = db_find("find_fuwu",$map,array(),1,50);
    include _include(APP_PATH . 'plugin/xl_findpage/htm/ajax.html');
} else {
    $cate = db_find("find_fuwu_cate", array('type' => 3, 'fup' => 0), array(), 0, 30);
    foreach ($cate as $v) {
        $catename[$v['cid']] = $v['name'];
    }

    include _include(APP_PATH . 'plugin/xl_findpage/htm/chanping.html');
}