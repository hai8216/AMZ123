<?php
$catelist = db_find("find_fuwu_cate", array('type' => 2, 'fup' => 0), array(), 0, 4);
foreach ($catelist as $v) {
    $catename[$v['cid']] = $v['name'];
}
$keyword = search_keyword_safe(xn_urldecode(param('keyword', '')));
$cate[1] = param(1, 0);
$cate[2] = param(2, 0);
$cate[3] = param(3, 0);
$cate[4] = param(4, 0);
$cate[5] = param(5, 0);//地区分类


$areaMap = [
    1 => [
        'name' => '北美',
        'cids' => [20,32] //美国，加拿大
    ],
    2 => [
        'name' => '欧洲',
        'cids' => [25,26,27,28,29,30,31],
    ],
    3 => [
        'name' => '拉美',
        'cids' => [33,40]
    ],
    4 => [
        'name' => '日韩',
        'cids' => [24,35],
    ],
    5 => [
        'name' => '东南亚',
        'cids' => [34,38,36],
    ],
    6 => [
        'name' => '中东',
        'cids' => [39],
    ],
    7 => [
        'name' => '非洲',
        'cids' => [41],
    ],
    8 => [
        'name' => '澳洲',
        'cids' => [37],
    ],
];


//兼容以前的url，如果没传地区ID，根据cate1(国家ID)匹配
$cids = [];
if (array_key_exists($cate[5],$areaMap)){
    $cids = $areaMap[$cate[5]]['cids'];
}


$catelist = db_find("find_fuwu_cate",array('type' => 2, 'fup' => 0) , array(), 0, 4);
foreach ($catelist as $v) {
    $catename[$v['cid']] = $v['name'];
}


function getUrl($k, $v)
{
    global $cate;
    if ($k == 0) {
        echo url('wuliu-' . $v['cid'] . '-' . $cate[2] . '-' . $cate[3] . '-' . $cate[4]. '-' . $cate[5]);
    } elseif ($k == 1) {
        echo url('wuliu-' . $cate[1] . '-' . $v['cid'] . '-' . $cate[3] . '-' . $cate[4]. '-' . $cate[5]);
    } elseif ($k == 2) {
        echo url('wuliu-' . $cate[1] . '-' . $cate[2] . '-' . $v['cid'] . '-' . $cate[4]. '-' . $cate[5]);
    } elseif ($k == 3) {
        echo url('wuliu-' . $cate[1] . '-' . $cate[2] . '-' . $cate[3] . '-' . $v['cid']. '-' . $cate[5]);
    }
}

function getUrl2($k)
{
    global $cate;
    if ($k == 0) {
        echo url('wuliu-0-' . $cate[2] . '-' . $cate[3] . '-' . $cate[4] . ''. '-' . $cate[5]);
    } elseif ($k == 1) {
        echo url('wuliu-' . $cate[1] . '-0-' . $cate[3] . '-' . $cate[4] . ''. '-' . $cate[5]);
    } elseif ($k == 2) {
        echo url('wuliu-' . $cate[1] . '-' . $cate[2] . '-0-' . $cate[4] . ''. '-' . $cate[5]);
    } elseif ($k == 3) {
        echo url('wuliu-' . $cate[1] . '-' . $cate[2] . '-' . $cate[3] . '-0'. '-' . $cate[5]);
    }

}

/**
 * 获取地区url
 */
function getAreaUrl($k){
    global $cate;
    echo url('wuliu-'.'0'.'-' . $cate[2] . '-' . $cate[3] . '-' . $cate[4].'-'.$k);
}


$sql = "";
$listSql = "select * from bbs_find_fuwu where 1=1 ";


if ($keyword) {
    $map['name'] = array('LIKE' => $keyword);
    $listSql .= "AND name LIKE '%{$keyword}%'";
} else {
    if ($cate[1] > 0) {
        $map['cate1'] = array('LIKE' => $cate[1]);
        $listSql .= " AND cate1 LIKE '%{$cate[1]}%' ";
    }
    if ($cate[2] > 0 && $cate[3] != '57') {
        $map['cate2'] = array('LIKE' => $cate[2]);
        $sql .= " AND cateid=$cate[2] AND type=2";
        $listSql .= "AND cate2 LIKE '%{$cate[2]}%'";
    }
    if ($cate[3] > 0) {
        $map['cate3'] = array('LIKE' => $cate[3]);
        $listSql .= " AND cate3 LIKE '%{$cate[3]}%'";
    }
    if ($cate[4] > 0 && $cate[3] != '57') {
        $map['cate4'] = array('LIKE' => $cate[4]);
        $listSql .= " AND cate4 LIKE '%{$cate[4]}%'";
    }
    if ($cate[5] > 0 && $cids){
        $listSql .= " AND ( ";
        foreach ($cids as $k => $v){
            if ($k == 0){
                $listSql .= "cate1 LIKE '%{$v}%' ";
            }else{
                $listSql .= "OR cate1 LIKE '%{$v}%' ";
            }
        }
        $listSql .= ")";
    }
}
//
//echo "select * from bbs_find_wuliu_pk where id>0 ".$sql;
//$pk_ary = db_sql_find("select * from bbs_find_wuliu_pk where id>0 ".$sql."");
//foreach($pk_ary as $v){
//    $pkid[]=$v['wuliu_id'];
//}
if ($cate[3] == '57') {
    unset($map['cate4']);
    unset($map['cate2']);
}
$total = 100;

if ($cate[5] > 0 && $cate[1] == 0){
    $top_key = "wuliu_top_cate5_{$cate[5]}";
}elseif ($cate[1] == 24){
    $top_key = "wuliu_top_cate1_{$cate[1]}";
}else{
    $top_key = "findwuliu_savetop";
}


$topk = kv_get($top_key);

if ($topk && (!$cate[1] || $cate[1] == 24) && !$cate[2] && !$cate[3] && !$cate[4]) {
    $sql .= " AND id not in(" . $topk . ")";
    $mapin['id'] = explode(",", $topk);
    $newSql = $listSql . " AND id in ({$topk}) order by order_id desc limit 0,100";
    $toplist = db_sql_find($newSql);
    foreach($mapin['id'] as $k=>$v){
        $toplistPk[$v]=$k;
    }
    foreach( $toplist as $k=>$v){
        $toplist[$k]['k'] = $toplistPk[$v['id']];
    }

    $column = array_column($toplist,'k');

    array_multisort($column,SORT_ASC,$toplist);

}
$map['model_type'] = 1;
$map['hide'] = 0;

$page = param('page', 1);
$pageSize = 100;
$offset = ($page - 1) * $pageSize;

$listSql .= " AND model_type = 1 and hide = 0 order by order_id desc limit {$offset},{$pageSize}";
$list = db_sql_find($listSql);

if ($topk && (!$cate[1] || $cate[1] == 24) && !$cate[2] && !$cate[3] && !$cate[4]) {
    foreach ($list as $k => $v) {
        if (in_array($v['id'], $mapin['id'])) {
            unset($list[$k]);
        }
    }
}
if ($cate[1] || $cate[3]) {
    $cateData = db_find_one("find_fuwu_cate", array('cid' => $cate[1]));
    $cateData2 = db_find_one("find_fuwu_cate", array('cid' => $cate[3]));
    $header['title'] = $cateData['name'] . $cateData2['name'] . "亚马逊FBA物流运费_头程费用-AMZ123跨境导航";
    $header['keywords'] = $cateData['name'] . $cateData2['name'] . ',' . $cateData['name'] . 'FBA,' . $cateData['name'] . '亚马逊物流,跨境物流';
    $header['description'] = $cateData['name'] . $cateData2['name'] . '价格查询，AMZ123为跨境卖家提供更加专业的物流公司推荐，包含FBA海运、FBA空运、FBA快递、专线小包、中欧铁路货运及海外仓等服务，助您一站式解决跨境物流问题。';
} else {
    $header['title'] = "找物流 - 找亚马逊FBA物流服务商 - AMZ123跨境导航";
    $header['keywords'] = '找物流,FBA物流,FBA物流服务商,亚马逊物流服务商,国际物流专线,跨境物流专线';
    $header['description'] = '找亚马逊海陆空运物流货代、专线小包服务商上AMZ123跨境导航！AMZ123为您提供丰富的跨境电商物流公司，助您找到合适的物流服务商！';
}
function shuffle_assoc($list)
{
    if (!is_array($list))
        return $list;

    $keys = array_keys($list);
    shuffle($keys);
    $random = array();
    foreach ($keys as $key)
        $random[$key] = shuffle_assoc($list[$key]);
    return $random;

}

//if (!$keyword && !$cate[1] &&  !$cate[2]  && !$cate[3]  && !$cate[4]) {
    $list = shuffle_assoc($list);
//}

if (param('ajax')) {
    $json['code'] = 200;
    $json['c'] = count($list);
    $json['data'] = $list;
    echo json_encode($json);
    exit;
} else {
    include _include(APP_PATH . 'plugin/xl_findpage/htm/wuliu.html');
}
