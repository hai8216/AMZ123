<?php

defined('DEBUG') OR exit('Forbidden');

$tablepre = $db->tablepre;

// 主题审核
$sql = "
CREATE TABLE {$tablepre}haya_examine_thread (
	fid smallint(6) NOT NULL default '0',			# 版块 id
	tid int(11) NOT NULL default '0',		# 主题id
	top tinyint(1) NOT NULL default '0',			# 置顶级别: 0: 普通主题, 1-3 置顶的顺序
	uid int(11) unsigned NOT NULL default '0',		# 用户id
	userip int(11) unsigned NOT NULL default '0',		# 发帖时用户ip ip2long()，主要用来清理
	subject char(128) NOT NULL default '',		# 主题
	create_date int(11) unsigned NOT NULL default '0',	# 发帖时间
	last_date int(11) unsigned NOT NULL default '0',	# 最后回复时间

	views int(11) unsigned NOT NULL default '0',		# 查看次数, 剥离出去，单独的服务，避免 cache 失效
	posts int(11) unsigned NOT NULL default '0',		# 回帖数
	images tinyint(6) NOT NULL default '0',		# 附件中包含的图片数
	files tinyint(6) NOT NULL default '0',		# 附件中包含的文件数
	mods tinyint(6) NOT NULL default '0',			# 预留：版主操作次数，如果 > 0, 则查询 modlog，显示斑竹的评分
	closed tinyint(1) unsigned NOT NULL default '0',	# 预留：是否关闭，关闭以后不能再回帖、编辑。
	firstpid int(11) unsigned NOT NULL default '0',	# 首贴 pid
	lastuid int(11) unsigned NOT NULL default '0',	# 最近参与的 uid
	lastpid int(11) unsigned NOT NULL default '0',	# 最后回复的 pid
	
	is_edit tinyint(1) unsigned NOT NULL default '0',	# 是否为编辑
	edit_content longtext NOT NULL,				# 编辑内容
	
	KEY (tid),					# 主键
	KEY (lastpid),					# 最后回复排序
	KEY (fid, tid),					# 发帖时间排序，正序。数据量大时可以考虑建立小表，对小表进行分区优化，只有数据量达到千万级以上时才需要。
	KEY (fid, lastpid),				# 顶贴时间排序，倒序
	KEY (is_edit)					# 是否编辑
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
$r = db_exec($sql);

// 回帖审核
$sql = "
CREATE TABLE {$tablepre}haya_examine_post (
	fid int(11) NOT NULL default '0',			# 版块 id
	tid int(11) unsigned NOT NULL default '0',		# 主题id
	pid int(11) unsigned NOT NULL default '0',		# 帖子id
	uid int(11) unsigned NOT NULL default '0',		# 用户id
	isfirst int(11) unsigned NOT NULL default '0',	# 是否为首帖，与 thread.firstpid 呼应
	create_date int(11) unsigned NOT NULL default '0',	# 发贴时间
	userip int(11) unsigned NOT NULL default '0',		# 发帖时用户ip ip2long()
	images smallint(6) NOT NULL default '0',		# 附件中包含的图片数
	files smallint(6) NOT NULL default '0',		# 附件中包含的文件数
	doctype tinyint(3) NOT NULL default '0',		# 类型，0: html, 1: txt; 2: markdown; 3: ubb
	quotepid int(11) NOT NULL default '0',		# 引用哪个 pid，可能不存在

	message longtext NOT NULL,				# 内容，用户提示的原始数据
	message_fmt longtext NOT NULL,			# 内容，存放的过滤后的html内容，可以定期清理，减肥。	
	
	is_edit tinyint(1) unsigned NOT NULL default '0',	# 是否为编辑
	edit_content longtext NOT NULL,				# 编辑内容
	
	KEY (pid),
	KEY (tid, pid),
	KEY (uid),						# 我的回帖，清理数据需要
	KEY (is_edit)					# 是否编辑
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
$r = db_exec($sql);

// 添加插件配置
$haya_post_examine_config = array(
	"examine_groups" => array(),
	"thread_examine_groups" => array(),
	"post_examine_groups" => array(),
	"examine_forums" => array(),
	"check_words" => '',
);
kv_set('haya_post_examine', $haya_post_examine_config); 

?>