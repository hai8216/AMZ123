<?php

defined('DEBUG') OR exit('Forbidden');

$tablepre = $db->tablepre;

$sql = "
DROP TABLE IF EXISTS {$tablepre}haya_examine_thread;
";
$r = db_exec($sql);

$sql = "
DROP TABLE IF EXISTS {$tablepre}haya_examine_post;
";
$r = db_exec($sql);

// 删除插件配置
kv_delete('haya_post_examine'); 

?>
