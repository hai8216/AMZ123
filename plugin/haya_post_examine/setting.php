<?php

defined('DEBUG') OR exit('Forbidden');

$header['title'] = '帖子审核设置';

if ($method == 'GET') {
	
	$config = kv_get('haya_post_examine');
	$check_word_examine_groups = [
		0 => '游客组',
		7 => '禁止用户组',
		101 => '普通用户组'
	];
	!isset($config['check_word_examine_group']) && $config['check_word_examine_group'] = [];

	$check_word_examine_forms = [
		1 => '跨境头条',
		3 => '测评黑名单',
		6 => '骗子曝光',
		7 => '跟卖曝光',
		8 => '跨境交流',
		12 => '跨境市场',
	];
	!isset($config['check_word_examine_form']) && $config['check_word_examine_form'] = [];
	
	include _include(APP_PATH . 'plugin/haya_post_examine/view/htm/setting.htm');
	
} else {
	
	$config = array();
	
	$config['examine_groups'] = param('examine_groups', array());
	$config['thread_examine_groups'] = param('thread_examine_groups', array());
	$config['post_examine_groups'] = param('post_examine_groups', array());
	$config['examine_forums'] = param('examine_forums', array());
	$config['check_words'] = param('check_words', '');
	
	$config['edit_examine'] = param('edit_examine', 0);
	$config['check_word_examine_group'] = param('check_word_examine_group', []);
	$config['check_word_examine_form'] = param('check_word_examine_form', []);
	
	kv_set('haya_post_examine', $config); 

	message(0, jump('设置保存成功', url('plugin-setting-haya_post_examine')));
	
}

?>
