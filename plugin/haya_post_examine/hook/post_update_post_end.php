<?php
exit;

$haya_post_examine_config = kv_get('haya_post_examine');
!isset($haya_post_examine_config['check_word_examine_group']) && $haya_post_examine_config['check_word_examine_group'] = [];
!isset($haya_post_examine_config['check_word_examine_form']) && $haya_post_examine_config['check_word_examine_form'] = [];
if (isset($haya_post_examine_config['edit_examine'])
	&& $haya_post_examine_config['edit_examine'] == 1
) {
	
	if (!empty($haya_post_examine_config['check_words'])) {
		$haya_post_examine_check_words = preg_split('/[\r\n|;|' . PHP_EOL . ']/', $haya_post_examine_config['check_words']);
		
		if ($isfirst) {
			$haya_post_examine_check_subject_status = preg_match('/(['.implode('|', $haya_post_examine_check_words).'])/is', $subject);
		}
		
		$haya_post_examine_check_message_status = preg_match('/(['.implode('|', $haya_post_examine_check_words).'])/is', $message);
	} else {
		if ($isfirst) {
			$haya_post_examine_check_subject_status = false;
		}
		
		$haya_post_examine_check_message_status = false;
	}
	
	if ($isfirst) {

		if ((in_array($fid, $haya_post_examine_config['examine_forums'])
			&& in_array($gid, $haya_post_examine_config['thread_examine_groups']))
			|| (($haya_post_examine_check_subject_status || $haya_post_examine_check_message_status)
            && (in_array($fid, $haya_post_examine_config['check_word_examine_form']) || in_array($gid, $haya_post_examine_config['check_word_examine_group'])))
		) {
			$haya_post_examine_thread = thread__read($tid);
			
			$haya_post_examine_new_thread = array(
				'fid' => $haya_post_examine_thread['fid'],
				'tid' => $haya_post_examine_thread['tid'],
				'top' => $haya_post_examine_thread['top'],
				'uid' => $haya_post_examine_thread['uid'],
				'userip' => $haya_post_examine_thread['userip'],
				'subject' => $haya_post_examine_thread['subject'],
				'create_date' => $haya_post_examine_thread['create_date'],
				'last_date' => $haya_post_examine_thread['last_date'],
				
				'views' => $haya_post_examine_thread['views'],
				'posts' => $haya_post_examine_thread['posts'],
				'images' => $haya_post_examine_thread['images'],
				'files' => $haya_post_examine_thread['files'],
				'mods' => $haya_post_examine_thread['mods'],
				'closed' => $haya_post_examine_thread['closed'],
				'firstpid' => $haya_post_examine_thread['firstpid'],
				'lastuid' => $haya_post_examine_thread['lastuid'],
				'lastpid' => $haya_post_examine_thread['lastpid'],
				
				'is_edit' => 1,
				'edit_content' => xn_json_encode($haya_post_examine_thread),
			);
			haya_post_examine_thread__create($haya_post_examine_new_thread);

            xn_log("调用'post_update_post_end:thread_delete'删除帖子{$tid}", "del_thread");
			thread__delete($tid);
	
			// 更新原始统计
			user__update($uid, array('threads-'=>1));
			forum__update($fid, array(
				'threads-'=>1, 
				'todaythreads-'=>1
			));
			runtime_set('threads-', 1);
			runtime_set('todaythreads-', 1);
			
			// 替换语言包
			$_SERVER['lang']['update_successfully'] = preg_replace("/[\，\。\,\.\;]/", '', $_SERVER['lang']['update_successfully']);
			$_SERVER['lang']['update_successfully'] = $_SERVER['lang']['update_successfully'].'，请等待管理员的审核';

		}		
		
	} 
	
}

?>