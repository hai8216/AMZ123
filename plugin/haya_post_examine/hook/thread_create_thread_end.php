<?php exit;

function generateRegularExpression($words)
{
    $regular = implode('|', array_map('preg_quote', $words));
    return "/$regular/i";
}
function generateRegularExpressionString($string){
    $str_arr[0]=$string;
    $str_new_arr=  array_map('preg_quote', $str_arr);
    return $str_new_arr[0];
}
function check_words($banned,$string)
{    $match_banned=array();
    //循环查出所有敏感词

    $new_banned=strtolower($banned);
    $i=0;
    do{
        $matches=null;
        if (!empty($new_banned) && preg_match($new_banned, $string, $matches)) {
            $isempyt=empty($matches[0]);
            if(!$isempyt){
                $match_banned = array_merge($match_banned, $matches);
                $matches_str=strtolower(generateRegularExpressionString($matches[0]));
                $new_banned=str_replace("|".$matches_str."|","|",$new_banned);
                $new_banned=str_replace("/".$matches_str."|","/",$new_banned);
                $new_banned=str_replace("|".$matches_str."/","/",$new_banned);
            }
        }
        $i++;
        if($i>20){
            $isempyt=true;
            break;
        }
    }while(count($matches)>0 && !$isempyt);

    //查出敏感词
    if($match_banned){
        return $match_banned;
    }
    //没有查出敏感词
    return array();
}

if (!param('autoTime')) {
    $haya_post_examine_config = kv_get('haya_post_examine');
    !isset($haya_post_examine_config['check_word_examine_group']) && $haya_post_examine_config['check_word_examine_group'] = [];
    !isset($haya_post_examine_config['check_word_examine_form']) && $haya_post_examine_config['check_word_examine_form'] = [];
    if (!empty($haya_post_examine_config['check_words'])) {
        $haya_post_examine_check_words = preg_split('/[\r\n|;|' . PHP_EOL . ']/', $haya_post_examine_config['check_words']);


        $mgkeywords = explode("\r\n", $haya_post_examine_config['check_words']);
        $banned = generateRegularExpression($mgkeywords);
//    //检查违禁词
        $res_banned = array();
        $res_banned = check_words($banned, $subject);
        if (isset($res_banned[0])) {
            $haya_post_examine_check_subject_status = 1;
        } else {
            $haya_post_examine_check_subject_status = 0;
        }
        $haya_post_examine_check_message_status = false;
    } else {
        $haya_post_examine_check_subject_status = false;
        $haya_post_examine_check_message_status = false;
    }

    if ((in_array($fid, $haya_post_examine_config['examine_forums'])
            && in_array($gid, $haya_post_examine_config['thread_examine_groups']))
        || ($haya_post_examine_check_subject_status && (in_array($fid, $haya_post_examine_config['check_word_examine_form']) || in_array($gid, $haya_post_examine_config['check_word_examine_group'])))
        || $haya_post_examine_check_message_status
    ) {
        $haya_post_examine_thread = thread__read($tid);

        $haya_post_examine_new_thread = array(
            'fid' => $haya_post_examine_thread['fid'],
            'tid' => $haya_post_examine_thread['tid'],
            'top' => $haya_post_examine_thread['top'],
            'uid' => $haya_post_examine_thread['uid'],
            'userip' => $haya_post_examine_thread['userip'],
            'subject' => $haya_post_examine_thread['subject'],
            'create_date' => $haya_post_examine_thread['create_date'],
            'last_date' => $haya_post_examine_thread['last_date'],

            'views' => $haya_post_examine_thread['views'],
            'posts' => $haya_post_examine_thread['posts'],
            'images' => $haya_post_examine_thread['images'],
            'files' => $haya_post_examine_thread['files'],
            'mods' => $haya_post_examine_thread['mods'],
            'closed' => $haya_post_examine_thread['closed'],
            'firstpid' => $haya_post_examine_thread['firstpid'],
            'lastuid' => $haya_post_examine_thread['lastuid'],
            'lastpid' => $haya_post_examine_thread['lastpid'],
        );
        haya_post_examine_thread__create($haya_post_examine_new_thread);

        xn_log("调用'thread_create_thread_end:thread_delete'删除帖子{$tid}", "del_thread");
        thread__delete($tid);

        // 更新原始统计
        user__update($uid, array('threads-' => 1));
        forum__update($fid, array(
            'threads-' => 1,
            'todaythreads-' => 1
        ));
        runtime_set('threads-', 1);
        runtime_set('todaythreads-', 1);

        // 替换语言包
        $_SERVER['lang']['create_thread_sucessfully'] = preg_replace("/[\，\。\,\.\;]/", '', $_SERVER['lang']['create_thread_sucessfully']);
        $_SERVER['lang']['create_thread_sucessfully'] = $_SERVER['lang']['create_thread_sucessfully'] . '，请等待管理员的审核';
    }
}
?>
