<?php

!defined('DEBUG') AND exit('Access Denied.');

if (empty($user)) {
	message(0, '请登录！');
}

if ($method != 'GET') {
	message(0, '提交错误！');
}

// 类型
$type = param(1, ''); 
if (empty($type)) {
	message(0, '访问错误！');
}

if ($type == 'thread') {

	$orderby = param('orderby');
	$extra = array(); 

	$active = 'haya_examine';
	!in_array($orderby, array('tid', 'lastpid')) AND $orderby = 'lastpid';

	// 分页
	$page = param(3, 1); 

	$cond = array('uid' => $uid);
	
	$pagesize = $conf['pagesize'];

	$threads = haya_post_examine_thread__count($cond);
	$threadlist = haya_post_examine_thread_find($cond, $page, $pagesize, array($orderby => -1));
	$pagination = pagination(url("haya_examine-thread-$fid-{page}"), $threads, $page, $pagesize);			

	$header['title'] = '我的未审核主题-'.$conf['sitename'];
	
	include _include(APP_PATH . 'plugin/haya_post_examine/view/htm/my/thread.htm');

} elseif ($type == 'post') {	

	$active = 'haya_examine';
	$orderby = 'lastpid';
	$extra = array(); 

	// 分页
	$page = param(3, 1); 

	$cond = array('uid' => $uid);
	
	$pagesize = $conf['pagesize'];

	$posts = haya_post_examine_post__count($cond);
	$postlist = haya_post_examine_post_find($cond, $page, $pagesize, array('pid' => -1));
	$pagination = pagination(url("haya_examine-post-$fid-{page}"), $posts, $page, $pagesize);			

	$header['title'] = '我的未审核回复-'.$conf['sitename'];

	include _include(APP_PATH . 'plugin/haya_post_examine/view/htm/my/post.htm');
	
}

