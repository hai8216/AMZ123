<?php

!defined('DEBUG') AND exit('Access Denied.');

if (empty($user)) {
	message(0, '请登录！');
}

if ($method != 'GET') {
	message(0, '提交错误！');
}

// 类型
$type = param(1, ''); 
if (empty($type)) {
	message(0, '访问错误！');
}

$user_can_view = true;
$haya_post_examine_config = kv_get('haya_post_examine');
if (!(in_array($gid, $haya_post_examine_config['examine_groups']) 
	|| $gid == 1
)) {
	$user_can_view = false;
}		

if ($type == 'thread') {
	$tid = param(2); 
	if (empty($tid)) {
		message(0, 'TID不能为空');
	}

	$thread = haya_post_examine_thread_read($tid);
	
	$forum = forum_read($thread['fid']);
	empty($forum) AND message(-1, '板块不存在');
	
	if (!($user_can_view 
		|| $thread['uid'] == $uid
		|| in_array($uid, explode(',', $forum['moduids']))
	)) {
		message(0, '你没有权限进行该操作！');
	}
	
	$first = post_read($thread['firstpid']);
	
	include _include(APP_PATH . 'plugin/haya_post_examine/view/htm/view/thread.htm');

} elseif ($type == 'post') {
	$pid = param(2); 
	if (empty($pid)) {
		message(0, 'ID不能为空');
	}

	$post = haya_post_examine_post_read($pid);
	if (empty($post)) {
		message(0, '待审核的回复不存在');
	}
	
	$thread = thread_read($post['tid']);
	
	$forum = forum_read($thread['fid']);
	empty($forum) AND message(-1, '板块不存在');
	
	if (!($user_can_view 
		|| $post['uid'] == $uid
		|| in_array($uid, explode(',', $forum['moduids']))
	)) {
		message(0, '你没有权限进行该操作！');
	}
	
	$first = post_read($thread['firstpid']);
	
	include _include(APP_PATH . 'plugin/haya_post_examine/view/htm/view/post.htm');
}

