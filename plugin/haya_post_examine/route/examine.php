<?php

!defined('DEBUG') AND exit('Access Denied.');

if (empty($user)) {
	message(0, '请登录！');
}

if ($method != 'GET') {
	message(0, '提交错误！');
}

// 类型
$type = param(1, ''); 
if (empty($type)) {
	message(0, '访问错误！');
}

$user_examine = true;
$haya_post_examine_config = kv_get('haya_post_examine');
if (!(in_array($gid, $haya_post_examine_config['examine_groups']) 
	|| $gid == 1
)) {
	$user_examine = false;
}
	
if ($type == 'thread') {
	$orderby = param('orderby');
	$extra = array(); 

	$active = 'haya_examine';
	!in_array($orderby, array('tid', 'lastpid')) AND $orderby = 'lastpid';

	// 板块
	$fid = param(2); 
	if (empty($fid)) {
		message(0, '访问错误！');
	}

	// 分页
	$page = param(3, 1); 

	if ($fid == 'index') {
		if (!$user_examine) {
			message(-1, '你没有权限进行该操作！');
		}		
		
		$cond = array();
		
		$pagesize = $conf['pagesize'];

		$threads = haya_post_examine_thread__count($cond);		
		$threadlist = haya_post_examine_thread_find($cond, $page, $pagesize, array($orderby => -1));
		$pagination = pagination(url("haya_examine-thread-index-{page}"), $threads, $page, $pagesize);			
		
		$header['title'] = '审核-'.$conf['sitename'];
		$header['keywords'] = '';
		$header['description'] = '';
		
		$fid = 0;
		
		include _include(APP_PATH.'view/htm/index.htm');
		
	} else {
		$fid = intval($fid);
		
		if (!in_array($fid, $haya_post_examine_config['examine_forums'])) {
			message(0, '当前板块不能够进行审核');
		}
		
		$cond = array('fid' => $fid);
		
		$forum = forum_read($fid);
		empty($forum) AND message(-1, '板块不存在');
		
		if (!($user_examine 
			|| in_array($uid, explode(',', $forum['moduids']))
		)) {
			message(-1, '你没有权限进行该操作！');
		}		
		
		$pagesize = $conf['pagesize'];

		$threads = haya_post_examine_thread__count($cond);
		$threadlist = haya_post_examine_thread_find($cond, $page, $pagesize, array($orderby => -1));
		$pagination = pagination(url("haya_examine-thread-$fid-{page}"), $threads, $page, $pagesize);			

		$header['title'] = $forum['seo_title'] ? $forum['seo_title'] : $forum['name'].'-'.$conf['sitename'];
		$header['mobile_title'] = $forum['name'];
		$header['mobile_link'] = url("haya_examine-thread-$fid");
		$header['keywords'] = '';
		$header['description'] = $forum['brief'];

		$_SESSION['fid'] = $fid;

		include _include(APP_PATH.'view/htm/forum.htm');
	}

} elseif ($type == 'post') {	

	$active = 'haya_examine';
	$orderby = 'lastpid';
	$extra = array(); 

	// 板块
	$fid = param(2); 
	if (empty($fid)) {
		message(0, '访问错误！');
	}

	// 分页
	$page = param(3, 1); 

	if ($fid == 'index') {
		$cond = array();
		
		$pagesize = $conf['pagesize'];

		$posts = haya_post_examine_post__count($cond);
		$postlist = haya_post_examine_post_find($cond, $page, $pagesize, array('pid' => -1));
		$pagination = pagination(url("haya_examine-post-index-{page}"), $posts, $page, $pagesize);			
		
		$header['title'] = '主题审核-'.$conf['sitename'];
		$header['keywords'] = '主题审核';
		$header['description'] = '主题审核';
		
		$fid = 0;
		$haya_post_examine_type = 'thread';

		include _include(APP_PATH.'view/htm/index.htm');
		
	} else {
		$fid = intval($fid);
		
		if (!in_array($fid, $haya_post_examine_config['examine_forums'])) {
			message(0, '当前板块不能够进行审核');
		}
		
		$cond = array('fid' => $fid);
		
		$forum = forum_read($fid);
		empty($forum) AND message(3, lang('forum_not_exists'));
		
		$pagesize = $conf['pagesize'];

		$posts = haya_post_examine_post__count($cond);
		$postlist = haya_post_examine_post_find($cond, $page, $pagesize, array('pid' => -1));
		$pagination = pagination(url("haya_examine-post-$fid-{page}"), $posts, $page, $pagesize);			

		$header['title'] = '回复审核-'.$forum['name'].'-'.$conf['sitename'];
		$header['mobile_title'] = $forum['name'];
		$header['mobile_link'] = url("haya_examine-post-$fid");
		$header['keywords'] = '';
		$header['description'] = $forum['brief'];

		$_SESSION['fid'] = $fid;
		
		$threadlist = array();
		$haya_post_examine_type = 'post';

		include _include(APP_PATH.'view/htm/forum.htm');
	}
	
}

