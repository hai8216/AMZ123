<?php

!defined('DEBUG') AND exit('Access Denied.');

if (empty($user)) {
	message(-1, '请登录！');
}

// 类型
$type = param(1, ''); 
if (empty($type)) {
	message(-1, '访问错误！');
}

$action = param(2, ''); 
if (empty($action)) {
	message(-1, '访问错误！');
}
require_once(APP_PATH . "/plugin/xl_aliopensearch/Config.inc.php");

use OpenSearch\Client\DocumentClient;
$user_examine = true;
$haya_post_examine_config = kv_get('haya_post_examine');
if (!(in_array($gid, $haya_post_examine_config['examine_groups']) 
	|| $gid == 1)
) {
	$user_examine = false;
}

if ($type == 'thread') {
	if ($action == 'delete') {
		
		if ($method == 'GET') {
			
			include _include(APP_PATH . 'plugin/haya_post_examine/view/htm/mod/thread/delete.htm');
			
		} else {
			
			$tidarr = param('tidarr', array(0));
			empty($tidarr) AND message(-1, '请选择要删除的主题');
			
			$threadlist = haya_post_examine_thread_find_by_tids($tidarr);
			foreach ($threadlist as &$thread) {
				$fid = $thread['fid'];
				$tid = $thread['tid'];
				
				$forum = forum__read($thread['fid']);

				if ($user_examine
					|| (!empty($forum) && in_array($uid, explode(',', $forum['moduids'])))
					|| $uid == $thread['uid']
				) {
					haya_post_examine_thread_delete($tid);
					
					haya_post_examine_notice_send($uid, $thread['uid'], "管理员删除了你的待审核主题《".$thread['subject']."》。");
				}
			}
			
			message(0, '主题删除成功');
		}

	} elseif($action == 'examine') {

		$fid = param(3); 
		$forum = forum__read($fid);

		if (!($user_examine
			|| (!empty($forum) && in_array($uid, explode(',', $forum['moduids'])))
		)) {
			message(-1, '你没有权限进行该操作！');
		}		
		
		if ($method == 'GET') {
			
			include _include(APP_PATH . 'plugin/haya_post_examine/view/htm/mod/thread/examine.htm');
			
		} else {
			
			$tidarr = param('tidarr', array(0));
			empty($tidarr) AND message(-1, lang('please_choose_thread'));
			
			$threadlist = haya_post_examine_thread_find_by_tids($tidarr);
			foreach ($threadlist as &$thread) {
				$fid = $thread['fid'];
				$tid = $thread['tid'];
				
				// 清除可能存在的主题
                xn_log("调用'mod:thread_delete'删除帖子{$thread['tid']}", "del_thread");
				thread__delete($thread['tid']);
				
				if ($thread['is_edit']) {
                    $new_thread = xn_json_decode($thread['edit_content']);
                    unset($new_thread['p']);
                    unset($new_thread['n']);
                    $new_thread['is_deleted'] = 0;
				} else {
					$new_thread = array (
                        'tid' => $tid,
						'fid' => $fid,
						'subject' => $thread['subject'],
						'uid' => $thread['uid'],
						'create_date' => $thread['create_date'],
						'last_date' => $thread['last_date'],
						'firstpid' => $thread['firstpid'],
						'lastpid' => $thread['lastpid'],
						'userip' => $thread['userip'],
                        'is_deleted' => 0
					);
					//将封面设置进去
                    if($thread['thumb']){
                        $new_thread['thumb'] = $thread['thumb'];
                    }
				}

                $tmp_thread = db_find_one('thread', array('tid' => $tid));
                if ($tmp_thread) {
                    $r = thread__update($tid, $new_thread);
                } else {
                    $r = thread__create($new_thread);
                }
				if ($r === FALSE) {
					message(-1, '主题审核失败！');
				}
				
				haya_post_examine_thread__delete($tid);
				
				// 更新原始统计
				user__update($thread['uid'], array('threads+'=>1));
				forum__update($fid, array(
					'threads+'=>1, 
					'todaythreads+'=>1
				));
				runtime_set('threads+', 1);
				runtime_set('todaythreads+', 1);

				//阿里搜索
                //查询POST的内容
                $postdata = db_find_one("post",array('tid' => $tid,'isfirst'=>1));
                $user = user_read($thread['uid']);
                $tableName = 'bbs_post';
                $smsg = strip_tags($postdata['message']);
                $documentClient = new DocumentClient($client);
                $docs_to_upload = array();
                $item['cmd'] = 'ADD';
                $item["fields"] = array(
                    'pid' => $postdata['pid'],
                    'tid' => $tid,
                    'subject' => $thread['subject'],
                    'isfirst' => 1,
                    'message' => str_replace("&nbsp;", " ", $smsg),
                    'author' => $user['username'],
                    'authorid' => $thread['uid'],
                    'fid' => $fid,
                    'fname' => $forum['name'],
                    'back_message' => '',
                    'posttime' => time(),
                );
                $docs_to_upload[] = $item;
                $json = json_encode($docs_to_upload);
                $ret = $documentClient->push($json, $appName, $tableName);
                //审核记录
                $sin['tid'] = $tid;
                $sin['actiontime'] = time();
                $sin['uid'] = $uid;
                $sin['subject'] = $thread['subject'];
                db_insert("shenhe_log",$sin);
				haya_post_examine_notice_send($uid, $thread['uid'], "管理员通过了你的待审核主题《<a href='".url("thread-".$thread['tid'])."' target='_blank'>".$thread['subject']."</a>》。");
			}
			
			message(0, "主题审核完成");
		}		
	}
} elseif ($type == 'post') {
	if ($action == 'delete') {
		
		if ($method == 'GET') {
			
			include _include(APP_PATH . 'plugin/haya_post_examine/view/htm/mod/post/delete.htm');
			
		} else {
			
			$pidarr = param('pidarr', array(0));
			empty($pidarr) AND message(-1, '请选择要删除的回复');
			
			$postlist = haya_post_examine_post_find_by_pids($pidarr);
			if (!empty($postlist)) {
				foreach ($postlist as &$post) {
					$pid = $post['pid'];
					
					$thread = thread__read($post['tid']);
					$forum = forum__read($thread['fid']);
					
					if ($user_examine
						|| (!empty($forum) && in_array($uid, explode(',', $forum['moduids'])))
						|| $uid == $post['uid']
					) {
						haya_post_examine_post_delete($pid);
						
						$post['message'] = htmlspecialchars(strip_tags($post['message']));
						haya_post_examine_notice_send($uid, $post['uid'], "管理员删除了你的待审核回复《".$post['message']."》。");
					}
				}
			}
			
			message(0, '回复删除成功');
		}

	} elseif($action == 'examine') {
		
		$fid = param(3); 
		$forum = forum__read($fid);

		if (!($user_examine
			|| (!empty($forum) && in_array($uid, explode(',', $forum['moduids'])))
		)) {
			message(-1, '你没有权限进行该操作！');
		}		
		
		if ($method == 'GET') {
			
			include _include(APP_PATH . 'plugin/haya_post_examine/view/htm/mod/post/examine.htm');
			
		} else {
			
			$pidarr = param('pidarr', array(0));
			empty($pidarr) AND message(-1, '请选择要审核的回复');
			
			$postlist = haya_post_examine_post_find_by_pids($pidarr);
			foreach ($postlist as &$post) {
				$fid = $post['fid'];
				$tid = $post['tid'];
				$pid = $post['pid'];
				
				post__delete($post['pid']);
				
				if ($post['is_edit']) {
					$create_pid = post__create(xn_json_decode($post['edit_content']), $gid);
				} else {
					$new_post = array (
						'tid' => $tid,
						'pid' => $pid,
						'uid' => $post['uid'],
						'isfirst' => $post['isfirst'],
						'create_date' => $post['create_date'],
						'userip' => $post['userip'],
						'images' => $post['images'],
						'files' => $post['files'],
						'doctype' => $post['doctype'],
						'quotepid' => $post['quotepid'],
						
						'message' => $post['message'],
						'message_fmt' => $post['message_fmt'],
					);
					$create_pid = post__create($new_post, $gid);
				}
				
				if ($create_pid === FALSE) {
					message(-1, '回复审核失败！');
				}
				
				haya_post_examine_post__delete($post['pid']);
				
				// 恢复原始统计
				thread__update($post['tid'], array('posts+'=>1));
				user__update($post['uid'], array('posts+'=>1));

				runtime_set('posts+', 1);
				runtime_set('todayposts+', 1);
				forum__update($fid, array('todayposts+'=>1));
				
				$post['message'] = htmlspecialchars(strip_tags($post['message']));
				haya_post_examine_notice_send($uid, $post['uid'], "管理员通过了你的待审核回复《<a href='".url("thread-".$post['tid'])."#".$post['pid']."' target='_blank'>".$post['message']."</a>》。");
			}
			
			message(0, "回复审核完成");
		}		
	}
	
}

?>