<?php
require_once(APP_PATH . "/plugin/xl_aliopensearch/Config.inc.php");

use OpenSearch\Client\DocumentClient;

if (empty($user)) {
    message(0, '请登录！');
}

$user_examine = true;
$haya_post_examine_config = kv_get('haya_post_examine');
if (!(in_array($gid, $haya_post_examine_config['examine_groups'])
    || $gid == 1
)) {
    $user_examine = false;
}
$tid = param('tid');
$tidinfo = db_find_one("post", array('tid' => $tid, 'isfirst' => 1));
if (!$user_examine) {
    message(-1, '你没有权限进行该操作！');
}
$pid = $tidinfo['pid'];
$post = post_read($pid);

$actiondo = isset($_GET['actiondo']) ? $_GET['actiondo'] : 'index';
if ($actiondo == 'index') {
    $forumlist = forum_list_cache();
    $forumlist_allowthread = forum_list_access_filter($forumlist, $gid, 'allowthread');
    $forumarr = xn_json_encode(arrlist_key_values($forumlist_allowthread, 'fid', 'name'));

// 如果为数据库减肥，则 message 可能会被设置为空。
// if lost weight for the database, set the message field empty.
    $post['message'] = htmlspecialchars($post['message'] ? $post['message'] : $post['message_fmt']);

    ($uid != $post['uid']) AND $post['message'] = xn_html_safe($post['message']);

    $attachlist = $imagelist = $filelist = array();
    if ($post['files']) {
        list($attachlist, $imagelist, $filelist) = attach_find_by_pid($pid);
    }
    $action = 'update';
    $thread = thread_read($tid);
    $isfirst = 1;
    $threadsh = db_find_one("haya_examine_thread", array('tid' => $tid));
    $thread['subject'] = $threadsh['subject'];
    include _include(APP_PATH . '/plugin/haya_post_examine/view/htm/post.htm');
} elseif ($actiondo == 'delete') {
    $threadlist = haya_post_examine_thread_find_by_tids(array($tid));
    foreach ($threadlist as &$thread) {
        $fid = $thread['fid'];
        $tid = $thread['tid'];

        $forum = forum__read($thread['fid']);

        if ($user_examine
            || (!empty($forum) && in_array($uid, explode(',', $forum['moduids'])))
            || $uid == $thread['uid']
        ) {
            haya_post_examine_thread_delete($tid);

            haya_post_examine_notice_send($uid, $thread['uid'], "管理员删除了你的待审核主题《" . $thread['subject'] . "》。");
        }
    }

    message(0, jump('主题删除成功', $_SERVER['HTTP_REFERER']));
} else if ($actiondo == 'delali') {
    if ($gid == 1) {
        $tableName = 'bbs_post';
        $documentClient = new DocumentClient($client);
        $docs_to_upload = array();
        $item['cmd'] = 'delete';
        $item["fields"] = array(
            'pid' => intval($_POST['pid']),
        );
        $docs_to_upload[] = $item;
        $json = json_encode($docs_to_upload);
        $ret = $documentClient->push($json, 'AMZ123_PRO', $tableName);
        echo 1;
    }
} else {
    //编辑后通过
    //更新标题和内容
    $threadsh = db_find_one("haya_examine_thread", array('tid' => $tid));
    $subject = htmlspecialchars(param('subject', '', FALSE));
    $message = param('message', '', FALSE);
    empty($message) AND message('message', lang('please_input_message'));
    mb_strlen($message, 'UTF-8') > 2048000 AND message('message', lang('message_too_long'));
    if ($subject != $threadsh['subject']) {
        mb_strlen($subject, 'UTF-8') > 80 AND message('subject', lang('subject_max_length', array('max' => 80)));
        db_update("haya_examine_thread", array('tid' => $tid), array('subject' => $subject));
    }
    $r = post_update($pid, array('doctype' => 0, 'message' => $message));
    $r === FALSE AND message(-1, lang('update_post_failed'));


    $threadlist = haya_post_examine_thread_find_by_tids(array($tid));
    foreach ($threadlist as &$thread) {
        $fid = $thread['fid'];
        $tid = $thread['tid'];

        // 清除可能存在的主题
        xn_log("调用'edit:thread_delete'删除帖子{$thread['tid']}", "del_thread");
        thread__delete($thread['tid']);

        if ($thread['is_edit']) {
            $new_thread = xn_json_decode($thread['edit_content']);
            unset($new_thread['n']);
            unset($new_thread['p']);
            $new_thread['is_deleted'] = 0;
        } else {
            $new_thread = array(
                'tid' => $tid,
                'fid' => $fid,
                'subject' => $thread['subject'],
                'uid' => $thread['uid'],
                'create_date' => $thread['create_date'],
                'last_date' => $thread['last_date'],
                'firstpid' => $thread['firstpid'],
                'lastpid' => $thread['lastpid'],
                'userip' => $thread['userip'],
                'is_deleted' => 0
            );
        }
        $tmp_thread = db_find_one('thread', array('tid' => $tid));
        if ($tmp_thread) {
            $r = thread__update($tid, $new_thread);
        } else {
            $r = thread__create($new_thread);
        }

        if ($thread['thumb']) {
            db_update("thread", array('tid' => $tid), array('thumb' => $thread['thumb']));
        }
        if ($r === FALSE) {
            message(-1, '主题审核失败！');
        }

        haya_post_examine_thread__delete($tid);

        // 更新原始统计
        user__update($uid, array('threads+' => 1));
        forum__update($fid, array(
            'threads+' => 1,
            'todaythreads+' => 1
        ));
        runtime_set('threads+', 1);
        runtime_set('todaythreads+', 1);

        $postdata = db_find_one("post",array('tid' => $tid,'isfirst'=>1));
        $user = user_read($thread['uid']);
        $tableName = 'bbs_post';
        $smsg = strip_tags($postdata['message']);
        $documentClient = new DocumentClient($client);
        $docs_to_upload = array();
        $item['cmd'] = 'ADD';
        $forum = forum__read($fid);
        $item["fields"] = array(
            'pid' => $postdata['pid'],
            'tid' => $tid,
            'subject' => $thread['subject'],
            'isfirst' => 1,
            'message' => str_replace("&nbsp;", " ", $smsg),
            'author' => $user['username'],
            'authorid' => $thread['uid'],
            'fid' => $fid,
            'fname' => $forum['name'],
            'back_message' => '',
            'posttime' => time(),
        );
        $docs_to_upload[] = $item;
        $json = json_encode($docs_to_upload);
        $ret = $documentClient->push($json, $appName, $tableName);
        //审核记录
        $sin['tid'] = $tid;
        $sin['actiontime'] = time();
        $sin['uid'] = $uid;
        $sin['subject'] = $thread['subject'];
        db_insert("shenhe_log",$sin);
        haya_post_examine_notice_send($uid, $thread['uid'], "管理员通过了你的待审核主题《<a href='" . url("thread-" . $thread['tid']) . "' target='_blank'>" . $thread['subject'] . "</a>》。");
    }


    message(0, lang('update_successfully'));
    //修改标题

}