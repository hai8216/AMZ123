<?php

function haya_post_examine_post__create($arr) {
	$r = db_create('haya_examine_post', $arr);
	return $r;
}

function haya_post_examine_post__count($cond = array()) {
	$n = db_count('haya_examine_post', $cond);
	return $n;
}

function haya_post_examine_post__delete($pid) {
	$r = db_delete('haya_examine_post', array('pid' => $pid));
	return $r;
}

function haya_post_examine_post__read($pid) {
	$r = db_find_one('haya_examine_post', array('pid' => $pid));
	return $r;
}

function haya_post_examine_post__find(
	$cond = array(), 
	$page = 1, 
	$pagesize = 20,
	$orderby = array()
) {
	$haya_examine_posts = db_find('haya_examine_post', $cond, $orderby, $page, $pagesize);	
	return $haya_examine_posts;
}

function haya_post_examine_post_read($pid) {
	$post = haya_post_examine_post__read($pid);
	post_format($post);
	return $post;
}

function haya_post_examine_post_find($cond = array(), $page = 1, $pagesize = 20, $orderby = array()) {
	$postlist = haya_post_examine_post__find($cond, $page, $pagesize, $orderby);
	$floor = 1;
	if ($postlist) foreach($postlist as &$post) {
		$post['floor'] = $floor++;
		post_format($post);
		
		$thread = thread_read_cache($post['tid']);
		$post['subject'] = $thread['subject'];
	}
	return $postlist;
}

function haya_post_examine_post_find_by_pids($pids, $order = array('pid'=>-1)) {
	if (!$pids) return array();
	$postlist = db_find('haya_examine_post', array('pid'=>$pids), $order, 1, 1000, 'pid');
	if ($postlist) foreach($postlist as &$post) post_format($post);
	return $postlist;
}

function haya_post_examine_post_create($arr) {
	$r = db_create('haya_examine_post', $arr);
	return $r;
}

function haya_post_examine_post_count($cond = array()) {
	$n = db_count('haya_examine_post', $cond);
	return $n;
}

function haya_post_examine_post_delete_by_tid($tid) {
	$r = db_delete('haya_examine_post', array('tid' => $tid));
	return $r;
}

function haya_post_examine_post_delete_by_pid($pid) {
	$r = db_delete('haya_examine_post', array('pid' => $pid));
	return $r;
}

function haya_post_examine_post_delete_by_uid($uid) {
	$r = db_delete('haya_examine_post', array('uid' => $uid));
	return $r;
}

// 删除回复
function haya_post_examine_post_delete($pid) {
	global $conf;
	
	$post = haya_post_examine_post__read($pid);
	
	if (empty($post)) return TRUE;

	($post['images'] || $post['files']) AND attach_delete_by_pid($pid);
	
	$r = haya_post_examine_post__delete($pid);
	if ($r === FALSE) return FALSE;
	
	return $r;
}


?>
