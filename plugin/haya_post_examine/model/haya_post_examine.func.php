<?php

// 截取
function haya_post_examine_substr($s, $len = 120, $htmlspe = TRUE){

   	if ($htmlspe == FALSE) {
   		$s = strip_tags($s);
		$s = htmlspecialchars($s);
   	}
	
   	$more = xn_strlen($s) > $len ? '...' : '';
	$s = xn_substr($s, 0, $len).$more;

	return $s;
} 

// 消息
function haya_post_examine_notice_send($fromuid, $recvuid, $message, $type = 3){
	if (!function_exists("notice_send")) {
		return false;
	}
	
	notice_send($fromuid, $recvuid, $message, $type);
	
	return true;
} 

?>
