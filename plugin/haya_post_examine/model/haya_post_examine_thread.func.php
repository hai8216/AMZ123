<?php

function haya_post_examine_thread__create($arr) {
	$r = db_create('haya_examine_thread', $arr);
	return $r;
}

function haya_post_examine_thread__read($tid) {
	$forum = db_find_one('haya_examine_thread', array('tid' => $tid));
	return $forum;
}

function haya_post_examine_thread__delete($tid) {
	$r = db_delete('haya_examine_thread', array('tid' => $tid));
	return $r;
}

function haya_post_examine_thread__count($cond = array()) {
	$n = db_count('haya_examine_thread', $cond);
	return $n;
}

function haya_post_examine_thread__find(
	$cond = array(), 
	$page = 1, 
	$pagesize = 20,
	$orderby = array()
) {
	$r = db_find('haya_examine_thread', $cond, $orderby, $page, $pagesize);	
	return $r;
}

function haya_post_examine_thread_read($tid) {
	$thread = haya_post_examine_thread__read($tid);
	thread_format($thread);
	return $thread;
}

function haya_post_examine_thread_find($cond = array(), $page = 1, $pagesize = 20, $orderby = array()) {
	$threadlist = haya_post_examine_thread__find($cond, $page, $pagesize, $orderby);
	if ($threadlist) {
		foreach ($threadlist as &$thread) {
			thread_format($thread);
		}
	}
	return $threadlist;
}

function haya_post_examine_thread_find_by_tids($tids, $order = array('lastpid'=>-1)) {
	if(!$tids) return array();
	$threadlist = db_find('haya_examine_thread', array('tid'=>$tids), $order, 1, 1000, 'tid');
	if($threadlist) foreach($threadlist as &$thread) thread_format($thread);
	return $threadlist;
}

function haya_post_examine_thread_create($arr) {
	$r = db_create('haya_examine_thread', $arr);
	return $r;
}

function haya_post_examine_thread_count($cond = array()) {
	$n = db_count('haya_examine_thread', $cond);
	return $n;
}

// 删除主题
function haya_post_examine_thread_delete($tid) {
	global $conf;
	
	$thread = haya_post_examine_thread__read($tid);
	
	if (empty($thread)) return TRUE;
	
	$fid = $thread['fid'];
	$uid = $thread['uid'];

	// 删除所有回帖，同时更新 posts 统计数
	$n = post_delete_by_tid($tid);
	
	// 删除我的主题
	$uid AND mythread_delete($uid, $tid);
	
	// 清除相关缓存
	forum_list_cache_delete();
	
	$r = haya_post_examine_thread__delete($tid);
	if ($r === FALSE) return FALSE;
	
	return $r;
}

function haya_post_examine_thread_delete_by_fid($fid) {
	$r = db_delete('haya_examine_thread', array('fid' => $fid));
	return $r;
}

function haya_post_examine_thread_delete_by_uid($uid) {
	$r = db_delete('haya_examine_thread', array('uid' => $uid));
	return $r;
}

?>
