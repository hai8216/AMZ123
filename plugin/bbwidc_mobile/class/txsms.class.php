<?php

/**
 *
 * 作    者：colin <ck_lin0@163.com>
 * 日    期：2018-05-07
 * 版    本：1.0.0
 * 功能说明：
 *
 **/
class txsms
{
    private $sdkappid = '1400123983';
    private $appkey = '4e582f7d3047bbd7b4f31be8eaad1008';
    private $requestUrl = 'https://yun.tim.qq.com/v5/tlssmssvr/sendsms';

    public function sendSms($phoneNumbers, $code)
    {

        $params = array($code);

        $random = $this->getNonceStr(10);
        $time = time();
        $sig = $this->getsig($random, $time, $phoneNumbers);
        $sign = "";
        $tel = array(
            'mobile' => $phoneNumbers,
            'nationcode' => 86
        );
        $tpl_id = 194243;
        $array = array(
            'params' => $params,
            'sig' => $sig,
            'sign' => $sign,
            'tel' => $tel,
            'tpl_id' => $tpl_id,
            'time' => $time
        );
        $url = $this->requestUrl . '?sdkappid=' . $this->sdkappid . '&random=' . $random;
        $data = json_encode($array);
        $result = json_decode($this->curlpost($url, $data), true);
        return $result;
    }

    private function getNonceStr($length = 10)
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    private function getsig($random, $time, $mobile)
    {
        return hash("sha256", "appkey=$this->appkey&random=$random&time=$time&mobile=$mobile");
    }

    private function curlpost($url, $data = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// 这个是主要参数
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

}