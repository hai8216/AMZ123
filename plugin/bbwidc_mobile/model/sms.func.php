<?php

include APP_PATH . 'plugin/bbwidc_mobile/model/sms_sms.func.php';


// 发送验证码接口
function sms_send_code($tomobile, $code)
{
    global $_POST, $ajax, $_GET;
    
    // 根据类型调用不同的短信发送 SDK
    if ($ajax && $_GET['xcxapi']) {
        $r = sms_sms_send_code_tentcent($tomobile, $code);
        if ($r['stat'] == '100') {
            message(0, '发送成功！');
        } else {
            message(-1, '验证码发送失败！:' . $r['message']);
        }

        return $r;
    } else {
        $r = sms_sms_send_code_tentcent($tomobile, $code);
        if ($r['stat'] == '100') {
            message(0, '发送成功！');
        } else {
            message(-1, '验证码发送失败！:' . $r['message']);
        }

        return $r;
    }


}

/*
function sms_sms_send_code($tomobile, $code, $d, $s)
{
	return false;
}
*/
// sms_send('15600900902', "您的初始密码为：123456");

/*

Array
(
    [result] => 0
    [errmsg] => OK
    [ext] => 
    [sid] => 8:xxxxxxxxxxxxxxxxxxxxxxx
    [fee] => 1
)

*/

?>