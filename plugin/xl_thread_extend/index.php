<?php
/* 200 ok
*  201 未登录
 * 202 存在数据
 * 203 单个数据不存在
*/
header("access-control-allow-headers: Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Mx-ReqToken,X-Requested-With");
header("access-control-allow-methods: GET, POST, PUT, DELETE, HEAD, OPTIONS");
header("access-control-allow-credentials: true");
header("access-control-allow-origin: *");
if (!$uid) {
    returnJson('201', "not login");
}
$action = param('action');
if ($action == 'postData') {
    if ($method == 'GET') {
        exit;
    } else {
        $data = json_decode(file_get_contents("php://input"), true);
        $isHave = db_find_one("moni_reg", ["uid" => $uid,"setp" => $data['setp']]);
        if($isHave){
            returnJson('202', "ok");
        }

        $insert['uid'] = $uid;
        $insert['setp'] = $data['setp'];
        $insert['data'] = serialize($data);
        db_insert("moni_reg", $insert);
        returnJson('200', "ok");
    }
} elseif ($action == 'getSetp') {
    $lastSetp = db_find_one("moni_reg", ["uid" => $uid], ["id" => -1]);
    if ($lastSetp['uid']) {
        returnJson('200', intval($lastSetp['setp'])+1);
    } else {
        returnJson('200', 1);
    }

}elseif ($action == 'getSetpData') {
    $data = json_decode(file_get_contents("php://input"), true);
    $oneData = db_find_one("moni_reg", ["uid" => $uid,"setp"=>$data['setp']]);
    if ($oneData['uid']) {
        if(isset($data['filed']) && $data['filed']){
            $oneData = unserialize($oneData['data'])[$data['filed']];
        }else{
            $oneData = unserialize($oneData['data']);
        }
        returnJson('200', $oneData);
    } else {
        returnJson('203', 1);
    }

}
function returnJson($code, $parms = [])
{
    $data['code'] = $code;
    $data['data'] = $parms;
    echo json_encode($data);
    exit;
}