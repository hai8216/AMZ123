<?php exit;
function getThreadExtend($tid)
{
    $tplid = db_find_one("thread_extend", ["tid" => $tid])['extend_id'];
    $extendData = cache_get("extend_thread_" . $tplid);
    if (empty($extendData)) {
        $extendlist = db_find("thread_extend", ["tid" => $tid, "status" => 1]);
        foreach ($extendlist as $v) {
            $extendData[$v['tid']] = db_find_one("thread_extend_data", ["id" => $v['extend_id']]);
        }
        cache_set("extend_thread_" . $tplid, $extendData, 6000);
    }
    return $extendData;
}