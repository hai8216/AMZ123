<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/
$tplAry = [
    "0" => "二维码名片"
];
!defined('DEBUG') and exit('Access Denied.');
$pluginname = 'xl_thread_extend';
$action = param(3) ?: "list";
if ($action == 'list') {
    //找服务
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('thread_extend', array(), array(), $page, $pagesize);
    $c = db_count("thread_extend");
    $pagination = pagination(url("plugin-setting-xl_thread_extend-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_thread_extend/view/list.html');
} elseif ($action == 'extend') {
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('thread_extend_data', array(), array(), $page, $pagesize);
    $c = db_count("thread_extend_data");
    $pagination = pagination(url("plugin-setting-xl_thread_extend-extend-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_thread_extend/view/extend.html');
} elseif ($action == 'createdata') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("thread_extend_data", array('id' => param('id')));
        } else {
            $data = array('id' => 0);
        }

        $input['tpl'] = form_select('tpl', $tplAry, $data['tpl']);
        $input['title'] = form_text('title', $data['title']);
        $input['btnText'] = form_text('btnText', $data['btnText']);
        include _include(APP_PATH . 'plugin/xl_thread_extend/view/createdata.html');
    } else {
        $data['title'] = param('title');
        $data['btnText'] = param('btnText');
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/',param('hidden_img_tpl_1'),$res)) {
            $type = $res[2];
            if (param('hidden_img_tpl_1')) {
                $path = "./upload/extend/" . date('Ymd') . "/";
                $ext = '.'.$type;
                $name = uniqid() . $ext;
                if(saveTencentBase64(base64_decode(str_replace($res[1],'', param('hidden_img_tpl_1'))), $path . $name)){
                    $data['thumb'] = I($path . $name);
                }
            }
        }


        $oter = [];
        if (param('tpl') == 0) {
            if (preg_match('/^(data:\s*image\/(\w+);base64,)/',param('hidden_img_tpl_2'),$res)) {
                $type = $res[2];
                if (param('hidden_img_tpl_2')) {
                    $path = "./upload/extend/" . date('Ymd') . "/";
                    $ext = '.'.$type;
                    $name = uniqid() . $ext;
                    if(saveTencentBase64(base64_decode(str_replace($res[1],'', param('hidden_img_tpl_2'))), $path . $name)){
                        $qrcodeImg = I($path . $name);
                    }
                }
            }
            $oter['qrcode'] = $qrcodeImg;
        }
        $data['otherField'] = serialize($oter);
        if (param('id')) {
            cache_set("extend_thread_" . param('id'),'0');
            db_update("thread_extend_data", array('id' => param('id')), $data);
        } else {
            db_insert("thread_extend_data", $data);
        }
        message(0, jump('保存成功', url('plugin-setting-xl_thread_extend-extend')));
    }
} elseif ($action == 'adextend') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("thread_extend", array('id' => param('id')));
        } else {
            $data = array('id' => 0);
        }

        $input['extend_id'] =  form_text('extend_id', $data['extend_id']);
        $input['tid'] = form_text('tid', $data['tid']);
        $input['status'] = form_select('status',[1=>"生效",0=>"不生效"],$data['status']);
        include _include(APP_PATH . 'plugin/xl_thread_extend/view/adextend.html');
    } else {
        $data['extend_id'] = param('extend_id');
        $data['tid'] = param('tid');
        $data['status'] = param('status');
        if (param('id')) {
            db_update("thread_extend", array('id' => param('id')), $data);
        } else {
            db_insert("thread_extend", $data);
        }
        message(0, jump('保存成功', url('plugin-setting-xl_thread_extend')));
    }
} elseif($action=='delData'){
    db_delete("thread_extend_data", ["id" => param('id')]);
    message(0, jump('ok'));
}elseif($action=='delextend'){
    db_delete("thread_extend", ["id" => param('id')]);
    message(0, jump('ok'));
}
?>