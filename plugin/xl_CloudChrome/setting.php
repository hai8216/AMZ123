<?php

!defined('DEBUG') AND exit('Access Denied.');

$ac = param(3);
if ($ac == 'edit') {
    if ($method == 'GET') {
        $id = param(4);
        $data = db_find_one("chrome_usertoken", array('id' => $id));
        include _include(APP_PATH . 'plugin/xl_CloudChrome/edit.html');
    } else {
        ;
        db_update("chrome_usertoken", array('id' => param('id')), array('token' => param('token'), 'type' => param('type')));
        message(0, jump('修改成功', url('plugin-setting-xl_CloudChrome'), 1));
    }
} else if ($ac == 'add') {
    if ($method == 'GET') {
        $id = param(4);
        $data = db_find_one("chrome_usertoken", array('id' => $id));
        include _include(APP_PATH . 'plugin/xl_CloudChrome/add.html');
    } else {
        ;
        db_insert("chrome_usertoken", array('token' => param('token'), 'type' => param('type')));
        message(0, jump('添加成功', url('plugin-setting-xl_CloudChrome'), 1));
    }
} else if ($ac == 'gonggao') {
    if ($method == 'GET') {
        $kv = kv_get('CloudChrome');
        $input['title'] = form_text('title', $kv['title']);
        $input['links'] = form_text('links', $kv['links']);
        include _include(APP_PATH . 'plugin/xl_CloudChrome/gonggao.html');
    } else {
        $kv['title'] = param('title');
        $kv['links'] = param('links');
        // hook plugin_vcode_setting_kv_set_before.htm
        kv_set('CloudChrome', $kv);
        message(0, jump('添加成功', url('plugin-setting-xl_CloudChrome-gonggao'), 1));
    }
} else if ($ac == 'delete') {
    if (db_delete("chrome_usertoken", array('id' => param('token_id')))) {
        message(0, 'delete');
    }
} else {
    $tokenList = db_find("chrome_usertoken");
    include _include(APP_PATH . 'plugin/xl_CloudChrome/setting.htm');
}

?>
