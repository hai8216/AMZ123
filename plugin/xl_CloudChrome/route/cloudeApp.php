<?php
header('Access-Control-Allow-Origin:*'); // *代表允许任何网址请求
$email = param('name');            // 邮箱或者手机号 / email or mobile
$password = md5(param('pwd'));
//empty($email) AND errorJSON(lang('email_is_empty'));
if (is_mobile($email, $err)) {
    $_user = user_read_by_mobile($email);
    empty($_user) AND errorJSON(lang('mobile_not_exists'));
} elseif (is_email($email, $err)) {
    $_user = user_read_by_email($email);
    empty($_user) AND errorJSON(lang('email_not_exists'));
} else {
    $_user = user_read_by_username($email);
    empty($_user) AND errorJSON(lang('username_not_exists'));
}

!is_password($password, $err) AND message('password', $err);
$check = (md5($password . $_user['salt']) == $_user['password']);
// hook user_login_post_password_check_after.php
!$check AND errorJSON(lang('password_incorrect'));

$uid = $_user['uid'];
if (param('appid') == 'XIAOLONGBGIMGHD') {
    $json_string = array('uid' => $uid, 'name' => $email);
    $pp = php_encrypt_js_code($json_string);
    $data['code'] = 200;
    $data['TokenCode'] = $pp;
    echo json_encode($data);
}elseif (param('appid') == 'ccadnhchhebeadhodmaighgpaeagnfha') {
    //检验是否通过了激活
    $isVerify = db_find_one("chormeTools_jiance_verify", array('uid' => $uid));
    if ($isVerify) {
        $json_string = array('uid' => $uid, 'name' => $email, 'use_code' => $isVerify['use_card']);
        $data['code'] = 200;
    } else {
        $json_string = array('uid' => $uid, 'name' => $email, 'use_code' => 0);
        $data['code'] = 201;
    }
    $pp = authcode(serialize($json_string),'ENCODE','amz123comxuxiaolong');
    $data['TokenCode'] = $pp;
    echo json_encode($data);
} else if (param('appid') == 'XIAOLONGJK') {
    $json_string = $email;
    $data['code'] = 200;
    $data['userToken'] = authcode(json_encode(array('uid' => $uid)), 'ENCODE', 'xiaolongamz123jk');
    $wechat = db_find_one("wechat_bind", array('uid' => $uid));
    $wechatOpenid = $wechat['openid'] ? $wechat['openid'] : 0;
    if ($wechatOpenid) {
        $data['wechat'] = authcode($wechatOpenid, 'ENCODE', 'xiaolongamz123jk');
    }
    $data['user'] = $json_string;
//    $list = db_find('chrome_plugin_monitor',array('uid'=>1));
//    $data['tracks']=$list;
    echo json_encode($data);
} else {
    if ($uid) {
        $kv = array();
        $kv = kv_get('CloudChrome');
        $need = array('keepa.com');
        $postUrl = parse_url(urldecode($_POST['url']))['host'];
        $data['code'] = 200;
        if (strpos($postUrl, 'keepa') !== false) {
            $type = 'keepa';
        } else {
            $type = 'merchantwords';
        }
        $Tokenlist = db_find("chrome_usertoken", array('type' => $type));
        $data['type'] = $type;
        $data['gonggao'] = $kv;
//    print_r($Tokenlist);exit;
        foreach ($Tokenlist as $v) {
            $data['token'][] = $v['token'];
        }

        echo json_encode($data);
    }
}
function errorJSON($msg)
{
    $data['code'] = 300;
    $data['msg'] = $msg;
    echo json_encode($data);
    exit;
}

function php_encrypt_js_code($data)
{

    //加密向量16位
    $iv = "ZZWBKJ_ZHIHUAWEI";
    //PHP加密秘钥16位
    $key = "ZZWBKJ_ZHIHUAWEI";
    $base64_str = json_encode($data);
    $encrypted = openssl_encrypt($base64_str,  "aes-128-cbc", $key, true, $iv);
    return base64_encode($encrypted);
}


function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0)
{
    $ckey_length = 4;
    $key = md5($key != '' ? $key : "amz123xiaolonglong");
    $keya = md5(substr($key, 0, 16));
    $keyb = md5(substr($key, 16, 16));
    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';

    $cryptkey = $keya . md5($keya . $keyc);
    $key_length = strlen($cryptkey);

    $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
    $string_length = strlen($string);

    $result = '';
    $box = range(0, 255);

    $rndkey = array();
    for ($i = 0; $i <= 255; $i++) {
        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
    }

    for ($j = $i = 0; $i < 256; $i++) {
        $j = ($j + $box[$i] + $rndkey[$i]) % 256;
        $tmp = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }

    for ($a = $j = $i = 0; $i < $string_length; $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $tmp = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
    }

    if ($operation == 'DECODE') {
        if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
            return substr($result, 26);
        } else {
            return '';
        }
    } else {
        return $keyc . str_replace('=', '', base64_encode($result));
    }

}

?>