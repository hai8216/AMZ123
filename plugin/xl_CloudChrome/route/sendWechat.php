<?php
$arr = file_get_contents("php://input");
$nodata = json_decode($arr,true);
include APP_PATH . "plugin/xl_wechat_union/class/wechat.class.php";
$wechat = new Wechat_amz123();
$bind_state = db_find_one("wechat_bind", array('uid' => 2));
$catename = ["无分类","跟卖监控","上新监控","差评监控",'其他监控'];
$postdata = array(
    'touser' => $bind_state['openid'],
    'template_id' => 'YtCpjAq6ZClqoXgpRdJ8WsHTzZa2kB-OuQxOXZsX3LY',
    'url' => 'http://www.amz123.com',
    'data' => array(
        'first' => array(
            'value' => $catename[$nodata['cate']],
            'color' => "#173177"
        ),
        'keyword1' => array(
            'value' =>'【'.$nodata['title'].'】',
            'color' => "#173177"
        ),
        'keyword2' => array(
            'value' => date('Y-m-d H:i'),
            'color' => "#173177"
        ),
        'remark' => array(
            'value' => '数据由['.$nodata['oldParsedText'].']变为['.$nodata['justParsedText'].']
[  亚马逊卖家之路，从AMZ123开始  ]',
            'color' => "#173177"
        )

    ),
);
$wechat->sendTpl(json_encode($postdata));