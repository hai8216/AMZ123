<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') and exit('Access Denied.');
$action = param(3, 'formlist');
include APP_PATH . 'model/form_model.func.php';
include APP_PATH . 'plugin/plugin_form/common.php';
$admintoken = _cookie('bbs_admin_token');
if ($action == 'formlist') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $keywords = param('keywords');
    $map = [];
    if ($keywords) {
        $map['title'] = array('LIKE' => $keywords);
        $arrlist = db_find("form_model", $map, array(), $page, $pagesize);
    } else {
        $arrlist = db_find("form_model", array(), array(), $page, $pagesize);
    }
    $c = db_count("formlist", $map);
    $pagination = pagination(url("plugin-setting-plugin_form-formlist-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/plugin_form/admin/formlist.html');
}elseif($action=='del'){
    db_delete("form_model",["id"=>param('delid')]);
    message(0,0);
}elseif ($action == 'create') {
    if ($method == 'GET') {
        if (param('id')) {
            $id = param('id');
            $input['id'] = form_text("id", $id);
            $val = db_find_one("form_model", ["id" => $id]);
        }else{
            $id = 0;
        }
        $channel = db_find("form_model",[],[],1,50);
        $input['title'] = form_text("title", $val['title']);
        $input['desc'] = form_textarea("desc", $val['desc']);
//        $input['banner'] = form_text("title", $val['title']);
//        $input['background'] = form_text("title", $val['title']);
//        $input['footer'] = form_text("title", $val['title']);
        $input['url'] = form_text("url", $val['url']);
        $input['islogin'] = form_radio_yes_no("islogin",$val['islogin']);
        $input[''] = form_text("starttime", $val['starttime']);
        $input['endtime'] = form_text("endtime", $val['endtime']?:time());
        $input['starttime'] = form_text("starttime", $val['starttime']?:time());

        include _include(APP_PATH . 'plugin/plugin_form/admin/create.html');
    } else {

        $input['title'] = param("title");
        $input['desc'] = param("desc");
        $input['starttime'] = param('starttime');
        $input['endtime'] = param('endtime');
        $input['islogin'] = param("islogin");
        $input['addtime'] = time();

        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('banner'), $res)) {
            $type = $res[2];
            $path = "./upload/form-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('banner'))), $path . $name)) {
                $input['background'] = $path . $name;
            }
        }
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('footer'), $res)) {
            $type = $res[2];
            $path = "./upload/form-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('footer'))), $path . $name)) {
                $input['footer'] = $path . $name;
            }
        }
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('banner3'), $res)) {
            $type = $res[2];
            $path = "./upload/form-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('banner3'))), $path . $name)) {
                $input['background_wap'] = $path . $name;
            }
        }
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('footer4'), $res)) {
            $type = $res[2];
            $path = "./upload/form-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('footer4'))), $path . $name)) {
                $input['footer_wap'] = $path . $name;
            }
        }
        if (param('id')) {
            $inserid = param('id');
            db_update("form_model", ["id" => param('id')], $input);
        } else {
            $input['fields'] = json_encode([]);
            $inserid = db_insert("form_model", $input);
            $inserid ? do_table($inserid) : '';
        }
        $encry = new Encrypt();
        model__update($inserid, ['key' => strtolower($encry->toString($inserid))]);
        message(0, 'ok');
    }
}