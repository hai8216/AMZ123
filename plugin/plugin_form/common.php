<?php
function returnJson($code = 200,$message = '',$data = [],$header = [])
{
    header('Content-Type:application/json; charset=utf-8');
//		http_response_code($code);    //设置返回头部
    $return['code'] = (int)$code;
    $return['message'] = $message;
    //$return['data'] = is_array($data) ? $data : ['info'=>$data];
    $return['data'] =  $data ;
    // 发送头部信息
    foreach ($header as $name => $val) {
        if (is_null($val)) {
            header($name);
        } else {
            header($name . ':' . $val);
        }
    }
    exit(json_encode($return,JSON_UNESCAPED_UNICODE));
}
function checkAdmin($admin_token){
    define('XN_ADMIN_BIND_IP', array_value($conf, 'admin_bind_ip'));
    global $longip, $time, $useragent, $conf;
    $useragent_md5 = md5($useragent);
    $key = md5((XN_ADMIN_BIND_IP ? $longip : '').$useragent_md5.xn_key());
    $s = xn_decrypt($admin_token, $key);

    list($_ip, $_time) = explode("\t", $s);

    // 后台超过 3600 自动退出。
    // Background / more than 3600 automatic withdrawal.
    //if($_ip != $longip || $time - $_time > 3600) {
    if((XN_ADMIN_BIND_IP && $_ip != $longip || !XN_ADMIN_BIND_IP) && $time - $_time > 3600) {
        returnJson(-1, lang('admin_token_expiry'));
        exit;
    }
    return true;
}
class Encrypt
{
    static private $keyMap = [
        'OKR2S64LCBIZAW7D1UHV8YNG35QXME9F',
        '3ZE2SQFY94HW5NKLUBC7XOD6V1ARMIG8',
        'VQ154BDWLIAY9RO2FMCSKE783ZU6XNHG',
        'UL9DN587FW43CVRAMSHIB1OXQ62KEZGY',
        '2RLMGVIUH1DWB94X5ZKAQNC7O83ES6FY',
        'DFEKXUY2RB8OHNM46VZ1CQ7SG3WL5I9A',
        'B2RV5CMW63OZUQEGS49Y7NHDXALFK1I8',
        'Z6KX9G5QWRB7F4EIMO3NAV1YD8LCUHS2',
        'OW69YFEB8HX57ARQN2DLMKZV31GIUSC4',
        'MC9D7Y8SIZAKQX5BG3R1NOLEV6FU4WH2',
        '6AID1YBQVW74SRN8EM9O3LK2GUXZFHC5',
        'XLE71IFQNSRW54DOV6H2AKZUY39C8BGM',
        'RUZXWNQD8F2917CHK4VBYIEALM3GS5O6',
        '96GAO8M345UI1VL27SERNZCHXDYKWBFQ',
        'D2AI5SVEX8OH3WKG4RUC7LYBQ16Z9NMF',
        '4RGI2ABDK5SH8FWQMUE1VXLZ69YN37CO',
    ];
    /**
     * 将数字编码为字符串
     */
     public function toString($num = 0)
    {

        // 对传入的数字，取hash的第一位
        $hash = self::getHash($num);

        // 根据Hash，选择不同的字典
        $keymap = self::getKeyMap($hash);

        // 数字转二进制
        $map = self::fixBin(decbin($num));

        // 如果不足10位，前面自动补全对应个数的0
        $len = strlen($map);
        if ($len < 10) {
            $map = substr('00000000000000000000', 0, (10 - $len)) . $map;
        }

        // 按5个一组，编码成数字，根据KeyMap加密
        $keys = '';
        $len = strlen($map);
        for ($index = 0; $index < strlen($map); $index += 5) {
            $keys .= substr($keymap, bindec(substr($map, $index, 5)), 1);
        }

        return $hash . $keys;
    }

    /**
     * 将字符串编码为数字
     */
     public function toInt($str = '')
    {

        //根据生成规则，最小长度为3
        if (strlen($str) < 3) {
            return false;
        }
        $hash = substr($str, 0, 1);
        $keys = substr($str, 1);

        // 根据Hash，选择不同的字典
        $keymap = self::getKeyMap($hash);

        $bin = '';
        // 根据字典，依次 index，并转换为二进制
        for ($i = 0; $i < strlen($keys); $i++) {
            for ($index = 0; $index < strlen($keymap); $index++) {
                if (strtoupper(substr($keys, $i, 1)) === substr($keymap, $index, 1)) {
                    $bin .= self::fixBin(decbin($index));
                }
            }
        }

        // 二进制转换为数字
        $num = bindec($bin);
        if (self::getHash($num) === $hash) {
            return $num;
        } else {
            return false;
        }

    }

    /**
     * 根据Hash取字典
     */
     private function getKeyMap($hash = 'A')
    {
        return self::$keyMap[hexdec($hash)];
    }

    /**
     * 不足5位的二进制，自动补全二进制位数
     */
     private function fixBin($bin = '110')
    {
        $len = strlen($bin);
        if ($len % 5 != 0) {
            $bin = substr('00000', 0, (5 - $len % 5)) . $bin;
        }
        return $bin;
    }

    /**
     * 对数字进行Hash
     */
     private function getHash($num = 0)
    {
        return strtoupper(substr(md5(self::getKeyMap(0) . $num), 1, 1));
    }
}