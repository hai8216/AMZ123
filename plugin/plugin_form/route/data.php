<?php
//user_login_check();

include APP_PATH . 'model/form_model.func.php';
include APP_PATH . 'plugin/plugin_form/common.php';
$action = param('a', 'index');
$modelid = param('mid', '');
if (!$modelid) {
    returnJson(100, '表单ID错误');
}
if ($action == 'index') {
    $page = param('page', 1);
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;
    $list = data__find($modelid, [], [], $page, $pagesize);


    //获取对应的KEY
    $forminfi = model__read($modelid);
    $formlist = json_decode($forminfi['fields'], true)['list'];
    foreach ($formlist as $v) {
        $modelName[$v['model']] = $v['name'];
    }
    foreach ($list as $k => $v) {
        $lists[] = $v;
    }
    $header = get_fields("bbs_form_model_data" . $modelid);
    $tabheader = [];
    foreach ($header as $ka => $va) {
        if (strpos($ka, 'text_') !== false || $ka=='ip'  || $ka=='id'  || $ka=='addtime') {
            unset($header[$ka]);
            continue;
        }
        if (!$modelName[$ka]) {
            $tabheader[] = ["name" => $ka, "model" => $ka];
        } else {
            $tabheader[] = ["name" => $modelName[$ka], "model" => $ka];
        }
    }
    foreach ($lists as $k => $v) {
        $lists[$k]['uid'] = $v['uid']?user__read($v['uid'])['username']:"游客";
    }
    returnJson(200, '数据获取成功', ["tabledata" => $lists, "header" => $tabheader,"tableinfo"=>$forminfi]);
} elseif ($action == 'del') {
    $id = intval(param('id'));
    data__delete($modelid, $id);
    returnJson(200, '删除成功');
} elseif ($action == 'show') {
    $id = intval(param('id'));
    if (!$id) {
        returnJson(100, 'ID错误');
    }
    if (!$data = data__read($modelid, $id)) {
        returnJson(100, '未找到');
    }
    //还原多选结构
//    $model = model__read($modelid);
//    $model_fields = json_decode($model['fields'], true);
//    if($model_fields && $model_fields['list']){
//    foreach ($model_fields['list'] as $v) {
//        if ($v['type'] == 'checkbox') {
//            if (isset($data[$v['model']]) && $data[$v['model']]) {
////                $temp = explode(',', $data[$v['model']]);
////                foreach ($temp as $value) {
////                    $data[$v['model']][] = ['value' => $value['value'], 'label' => $value['value']];
////                }
//                $data[$v['model']] = explode(',', $data[$v['model']]);
//            } else {
//                $data[$v['model']] = [];
//            }
//        }
//    }}
    //还原地址接结构
//    if(isset($data['address']) && $data['address']){
//        $data['address'] = implode(',',$data['address']);
//    }
    returnJson(200, '获取详情', $data);
} elseif ($action == 'head') {
    $model = model__read($modelid);
    $fields = $model['fields'] ? json_decode($model['fields'], true) : [];
    $list = isset($fields['list']) ? $fields['list'] : [];
    $head = [
        ['key' => 'id', 'title' => 'ID'],
        ['key' => 'uid', 'title' => '用户ID'],
        ['key' => 'addtime', 'title' => '添加时间']
    ];
    foreach ($list as $v) {
        $head[] = ['key' => $v['model'], 'title' => $v['name']];
    }
    returnJson(200, '获取字段', $head);
}