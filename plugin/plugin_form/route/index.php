<?php
include APP_PATH . 'model/form_model.func.php';
include APP_PATH . 'plugin/plugin_form/common.php';
$key = param('k', '');
$action = param('a', 'index');
$userToken =  param('userToken', '');
$encry = new Encrypt();
$modelid = $encry->toInt(strtoupper($key));
global $user;
if (!$modelid) {
    returnJson(100, '表单ID错误');
}
if (!$model = model__read($modelid)) {
    returnJson(100, '未找到表单');
}
if($model['islogin']==1){
    if(!$userToken){
        returnJson(100, '用户未登录');
    }else{
        $fuid = user_token_get_do($userToken);
        if(!$fuid){
            returnJson(100, '登录已失效');
        }
    }
}
if ($action == 'index') {
    if ($model['starttime'] < date('Y-m-d H:i:s')) {
        returnJson(100, '还未开始');
    }
    if ($model['endtime'] > date('Y-m-d H:i:s')) {
        returnJson(100, '已结束');
    }
    if ($model['islogin'] && empty($user)) {
        returnJson(100, '请登录');
    }
    $data = [];
    if ($user) {
        $data = data__read_one($modelid, ['uid' => $uid]);
        //还原多选结构
        if ($data) {
            $model_fields = json_decode($model['fields'], true);
            if ($model_fields && $model_fields['list']) {
                foreach ($model_fields['list'] as $v) {
                    if ($v['type'] == 'checkbox') {
                        if (isset($data[$v['model']]) && $data[$v['model']]) {
//                        $temp = explode(',', $data[$v['model']]);
//                        foreach ($temp as $value) {
//                            $data[$v['model']][] = ['value' => $value['value'], 'label' => $value['value']];
//                        }
                            $data[$v['model']] = explode(',', $data[$v['model']]);
                        } else {
                            $data[$v['model']] = [];
                        }
                    }
                }
            }
            //还原地址接结构
            $data['address'] = isset($data['address']) && $data['address'] ? explode(',', $data['address']) : [];
        }
    }
    $model['starttime'] = strtotime($model['starttime']);
    $model['endtime'] = strtotime($model['endtime']);
    returnJson(200, '表单详情', ['model' => $model, 'data' => $data]);
} elseif ($action == 'add') {
    $id = param('id', '');
    $data = param_json('fields', '');
//    print_r($fields);exit;
//    $data = json_decode($fields, true);
    if (!$data) {
        returnJson(100, '数据错误');
    }
    if (!$modelid) {
        returnJson(100, '表单ID错误');
    }
    //验证手机验证码
    if($data['mobile']){
        $mobile = $data['mobile'];
        $smscode = db_find_one("mobile_code", array('phone' => $mobile), array('dateline' => '-1'));
        if ($smscode['code'] != $data['vCode']) {
            returnJson(100, '手机验证码错误');
        }
    }
//    if (!$model = model__read($modelid)) {
//        returnJson(100, '未找到表单');
//    }
//    if ($model['starttime'] > date('Y-m-d H:i:s')) {
//        returnJson(100, '还未开始');
//    }
//    if ($model['endtime'] < date('Y-m-d H:i:s')) {
//        returnJson(100, '已结束');
//    }
    $data['uid'] = $uid ?: 0;
    $data['ip'] = md5($longip);
    if (!$uid) {
        $modelFormid = data__filter($modelid, $longip);
    } else {
        $modelFormid = data__filter_byuid($modelid, $uid);
    }
    if ($modelFormid) {
        returnJson(100, '你已填过该表单了');
    }

    //存储多选
    $model_fields = json_decode($model['fields'], true);
    if ($model_fields && $model_fields['list']) {
        foreach ($model_fields['list'] as $v) {
            if ($v['type'] == 'checkbox') {
                if (isset($data[$v['model']]) && $data[$v['model']]) {
//                $temp = [];
//                foreach ($data[$v['model']] as $value) {
//                    $temp[] = $value['value'];
//                }
//                $data[$v['model']] = implode(',', $temp);
                    $data[$v['model']] = implode(',', $data[$v['model']]);
                } else {
                    $data[$v['model']] = '';
                }
            }
        }
    }
    //存储地址
//    if (isset($data['address']) && $data['address']) {
//        $data['address'] = implode(',', $data['address']);
//    }
    unset($data['vCode']);
    if ($id && data__read($modelid, $id)) {
        data__update($modelid, $id, $data);
    } else {
        $data['addtime'] = date('Y-m-d H:i:s');
        $id = data__create($modelid, $data);
    }
    returnJson(200, '保存成功', ['id' => $id]);
}

