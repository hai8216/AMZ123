<?php
//user_login_check();

include APP_PATH . 'model/form_model.func.php';
include APP_PATH . 'plugin/plugin_form/common.php';
$action = param('a', 'index');
if ($action == 'index') {
    $page = param('page', 1);
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;
    $list = model__find([], [], $page, $pagesize);
    returnJson(200, '数据获取成功', $list);
} elseif ($action == 'add') {
    $data['title'] = param('title');
    $data['desc'] = param('desc', '');
    $data['banner'] = param('banner', '');
    $data['background'] = param('background', '');
    $data['footer'] = param('footer', '');
    $data['url'] = param('url', '');
    $data['islogin'] = param('islogin', 0);
    $data['starttime'] = param('starttime');
    $data['endtime'] = param('endtime');
    $data['addtime'] = date('Y-m-d H:i:s');
    if (!$data['title']) {
        returnJson(100, '必须填写标题');
    }
    if (!$data['starttime'] || !$data['endtime']) {
        returnJson(100, '必须选择开始时间和结束时间');
    }
    $encry = new Encrypt();
    $res = model__create($data);
    model__update($res, ['key' => strtolower($encry->toString($res))]);
    returnJson(200, '表单创建成功');
} elseif ($action == 'edit') {
    $id = intval(param('id'));
    if (!$id) {
        returnJson(100, '表单ID错误');
    }
    if (!model__read($id)) {
        returnJson(100, '未找到表单');
    }
    $data['title'] = param('title');
    $data['desc'] = param('desc', '');
    $data['banner'] = param('banner', '');
    $data['background'] = param('background', '');
    $data['footer'] = param('footer', '');
    $data['url'] = param('url', '');
    $data['islogin'] = param('islogin', 0);
    $data['starttime'] = param('starttime');
    $data['endtime'] = param('endtime');
    if (!$data['title']) {
        returnJson(100, '必须填写标题');
    }
    if (!$data['starttime'] || !$data['endtime']) {
        returnJson(100, '必须选择开始时间和结束时间');
    }
    model__update($id, $data);
    returnJson(200, '表单修改成功');
} elseif ($action == 'del') {
    $id = intval(param('id'));
    model__delete($id);
    returnJson(200, '表单删除成功');
} elseif ($action == 'show') {
    $id = intval(param('id'));
    $admin_token = param('admin_token');
    checkAdmin($admin_token);
    if (!$id) {
        returnJson(100, '表单ID错误');
    }
    if (!$data = model__read($id)) {
        returnJson(100, '未找到表单');
    }
    returnJson(200, '表单详情', $data);
} elseif ($action == 'usershow') {
    $k = param('k');
    if (!$k) {
        returnJson(100, '表单ID错误');
    }
    if (!$data = model__read_key($k)) {
        returnJson(100, '未找到表单1');
    }
    if (!$uid) {
        $modelFormid = data__filter($data['id'], $longip);
    } else {
        $modelFormid = data__filter_byuid($data['id'], $uid);
    }
    $data['formbyuser'] = $modelFormid ? 1 : 0;
    $data['background'] = $data['background']?"https://img.amz123.com/".$data['background']."/viewthumb":"";
    $data['footer'] = $data['footer']?"https://img.amz123.com/".$data['footer']."/viewthumb":"";
    returnJson(200, '表单详情', $data);
} elseif ($action == 'field') {
    $id = intval(param('id'));
    if (!$id) {
        returnJson(100, 'ID错误');
    }
    if (!model__read($id)) {
        returnJson(100, '未找到表单');
    }
    $fields = json_encode(param_json('fields'));
    $data['fields'] = $fields;
    model__update($id, $data);
    $res = do_field($id, $fields);
    if ($res) {
        returnJson(200, '表单保存成功');
    } else {
        returnJson(100, '表单保存失败');
    }
} elseif ($action == 'getUserinfo') {
    $t = "EecC9gxmt0YB2qtQJxtK6jv_2BQA2sY4p0EoQFnQ_3D_3D";
//    echo user_token_get_do($t);exit;
    if ($uid > 0) {
        returnJson(200, '获取成功', ['userToken' => user_token_gen($uid)]);
    }else{
        returnJson(0);
    }
}
