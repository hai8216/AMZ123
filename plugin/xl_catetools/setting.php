<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_catetools';
$action = param(3) ?: "list";
if ($action == 'list') {
    $list = db_find("catetools_category", array(), array('order_id' => '-1'), 1, 500);
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/list.htm');
}elseif($action=='deletelist'){
    db_delete('catetools_data', array('id' => param('toid')));
    message(0, 'success');
}
elseif ($action == 'datalist') {
    $list = db_find("catetools_data", array(), array('order_id' => '-1'), 1, 500);
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/datalist.htm');
}elseif ($action == 'deleteview') {
    db_delete('catetools_category', array('cid' => param('toid')));
    message(0, 'success');
} elseif ($action == 'addcate') {
    if ($method == 'GET') {
        $cate = array('cid'=>0,'name' => '', 'order_id' => '');
        if (param(4)) {
            $cate = db_find_one("catetools_category", array('cid' => param(4)));
        }
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addcate.htm');
    } else {
        if (param('cid')) {
            $data['name'] = param('name');
            $data['order_id'] = param('order_id');
             db_update('catetools_category', array('cid' => param('cid')), $data);
            message(0, 'success');
        } else {
            $insert['name'] = param('name');
            $insert['order_id'] = param('order_id');
            db_insert("catetools_category",$insert);
            message(0, 'success');
        }
    }
} elseif ($action == 'editcate') {
    if ($method == 'GET') {

        $val = db_find_one("catetools_data", array('id' => param(4)));
        $cate = db_find("catetools_category", array(), array('order_id' => '-1'), 1, 500);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/editdata.htm');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777,true);
            }

            $new_file = time() . rand(100, 50000) . "." . $type;

            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '',param('favicon'))))) {
                $icon = $wwwpath . $new_file;
            } else {
                $icon = param('favicon');
            }
        } else {
            $icon =  param('favicon');
        }

            $insert['title'] = param('title');
            $insert['desc'] = param('desc');
            $insert['favicon'] = $icon;
            $insert['cate_id'] = param('cate_id');
            $insert['links'] = param('links');
            $insert['order_id'] = param('order_id');
            db_update("catetools_data",array('id'=>param('id')),$insert);
            message(0, 'success');
        }

}elseif ($action == 'fadddata') {
    if ($method == 'GET') {
        $toid = param(4);
        $val = array('favicon'=>'','title'=>'','desc'=>'','links'=>'','order_id'=>'');
        $cate = db_find("catetools_category", array(), array('order_id' => '-1'), 1, 500);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/fadddata.htm');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777,true);
            }

            $new_file = time() . rand(100, 50000) . "." . $type;

            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '',param('favicon'))))) {
                $icon = $wwwpath . $new_file;
            } else {
                $icon = param('favicon');
            }
        } else {
            $icon =  param('favicon');
        }
        if (param('toid')) {
            $data['name'] = param('name');
            $data['order_id'] = param('order_id');
            db_update('catetools_category', array('cid' => param('cid')), $data);
            message(0, 'success');
        } else {
            $insert['title'] = param('title');
            $insert['desc'] = param('desc');
            $insert['favicon'] = $icon;
            $insert['cate_id'] = param('cate_id');
            $insert['links'] = param('links');
            $insert['order_id'] = param('order_id');
            db_insert("catetools_data",$insert);
            message(0, 'success');
        }
    }
}elseif ($action == 'adddata') {
    if ($method == 'GET') {
        $toid = param(4);
        $val = array('favicon'=>'','title'=>'','desc'=>'','links'=>'','order_id'=>'');
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/adddata.htm');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777,true);
            }

            $new_file = time() . rand(100, 50000) . "." . $type;

            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '',param('favicon'))))) {
                $icon = $wwwpath . $new_file;
            } else {
                $icon = param('favicon');
            }
        } else {
            $icon =  param('favicon');
        }
        if (param('toid')) {
            $data['name'] = param('name');
            $data['order_id'] = param('order_id');
            db_update('catetools_category', array('cid' => param('cid')), $data);
            message(0, 'success');
        } else {
            $insert['title'] = param('title');
            $insert['desc'] = param('desc');
            $insert['favicon'] = $icon;
            $insert['cate_id'] = param('cate_id');
            $insert['links'] = param('links');
            $insert['order_id'] = param('order_id');
            db_insert("catetools_data",$insert);
            message(0, 'success');
        }
    }
}
?>