<?php
checkVip();
$Pagename="美国站";
$routeName = "usseller";
$header['title'] = "亚马逊".$Pagename."Top卖家排名 - AMZ123跨境导航";
$header['keywords'] = ''.$Pagename.'top卖家,'.$Pagename.'大卖排行,'.$Pagename.'卖家排名';
$header['description'] ='美国亚马逊大卖排行榜单，排名基于最近30天内店铺收到的好评feedback数，了解竞争对手的第一步，先从学习TOP卖家开始。';
//省份统计
$table = "amazon_seller_us";
$simpleTable = "us";
$com="com";
$cmap[$simpleTable.'Count']= array('>' => 1);
$clist = db_find("amazon_seller_country",$cmap,[$simpleTable."Count"=>"-1"],1,200);
foreach($clist as $v){
    $cname[$v['code']] = $v['name'];
}
$page = param(3, 1);
$pagesize = 100;
$map = [];
$sf = param(2, 0);
$gj = param(1, '');
$f1 = param('f1');
$f2 = param('f2');
$f3 = param('f3');
$fall = param('fall');
$pnum = param('pnum');
$shengName = db_find("shengfen", [], [], 1, 50);
$allcount = cache_get('usseller_count');
if (!$allcount || $allcount < 1) {
    $allcount = db_count("amazon_seller_us");
    cache_set('usseller_count', $allcount, 60 * 60 * 24 * 10);
}

$shengfenCount = unserialize(kv_get("seller_count_sheng_".$table));
foreach($shengfenCount as $v){
    $shengfenCounts[$v['sheng_id']] = $v['COUNT']>0?$v['COUNT']:0;
}
foreach ($shengName as $k => $v) {
    $orderf[] = $shengfenCounts[$v['id']];
}
array_multisort($orderf, SORT_DESC, $shengName);
if ($sf) {
    $map['sheng_id'] = $sf;
}
if ($gj) {
    $map['countryCode'] = $gj;
}
$order['feedcount1'] = "-1";
if ($f1) {
    $f1data = explode("_", $f1);
    if (!is_numeric($f1data[0]) || !is_numeric($f1data[1])) {
        unset($f1);
    } else {
        $map['feedcount1'] = array('>=' => $f1data[0], '<=' => $f1data[1]);
    }
    $fparms['f1'] = $f1;
}
if ($f2) {
    $f2data = explode("_", $f2);
    if (!is_numeric($f2data[0]) || !is_numeric($f2data[1])) {
        unset($f2);
    } else {

        $map['feedcount2'] = array('>=' => $f2data[0], '<=' => $f2data[1]);
    }
    $fparms['f2'] = $f2;
}


if ($f3) {
    $f3data = explode("_", $f3);
    if (!is_numeric($f3data[0]) || !is_numeric($f3data[1])) {
        unset($f3);
    } else {
        $map['feedcount3'] = array('>=' => $f3data[0], '<=' => $f3data[1]);
    }
    $fparms['f3'] = $f3;
}


if ($fall) {
    $falldata = explode("_", $fall);
    if (!is_numeric($falldata[0]) || !is_numeric($falldata[1])) {
        unset($f3);
    } else {
        $map['feedcount4'] = array('>=' => $falldata[0], '<=' => $falldata[1]);
    }
    $fparms['fall'] = $fall;
}

if ($pnum) {
    $pnumdata = explode("_", $pnum);
    if (!is_numeric($pnumdata[0]) || !is_numeric($pnumdata[1])) {
        unset($f3);
    } else {
        $map['itemNums'] = array('>=' => $pnumdata[0], '<=' => $pnumdata[1]);
    }
    $fparms['pnum'] = $pnum;
}

if (is_array($fparms)) {
    $fpar = "?";
}
foreach ($fparms as $k => $v) {
    $fpar .= "" . $k . "=" . $v . "&";
}

//print_r($map);exit;
$listPk = db_find("amazon_seller_us", $map, $order, $page, $pagesize, '', ["id"]);
foreach ($listPk as $v) {
    $pkids[] = $v['id'];
}
$list = db_find("amazon_seller_us", ["id" => $pkids], $order, 1, $pagesize);
foreach ($shengName as $v) {
    $s[$v['id']] = $v['name'];
}
foreach ($list as $k => $v) {
    $list[$k]['brandList'] = IH(Dataa($v['brandList']));
}
$c = db_count("amazon_seller_us", $map);
$pagination = pagination(url("usseller-" . $gj . "-" . $sf . "-{page}",["f1"=>$f1,"f2"=>$f2,"fall"=>$fall,"pnum"=>$pnum]), $c, $page, $pagesize);

include _include(APP_PATH . 'plugin/xl_amazon_usseller/htm/common.html');

function Dataa($data)
{
//    $processString = preg_match_all('/\'(.*?)\'/',$data, $data);
//    print_r($data[1]);exit;
//    $allcount = count($data[1]);
//    $sf = $data[1][$allcount-3];
//    return $sf;
    $f = str_replace(array("['", "']", "', '", "','", "[\"", "\"]", "\",\""), "|", $data);
    $d = explode("|", $f);
    return $d;
}

function IH($ary)
{
    $s = '';
    if (is_array($ary)) {
        foreach ($ary as $v) {
            if ($v) {
                $v = str_replace(array("['", "']", "', '", "','", "\"", "',"), "", $v);
                $s .= "<p>" . $v . "</p>";
            }
        }
    }
    return $s;
}
