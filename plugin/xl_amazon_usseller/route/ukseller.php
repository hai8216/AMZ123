<?php
checkVip();
$Pagename="英国站";
$routeName = "ukseller";
$header['title'] = "亚马逊".$Pagename."Top卖家排名 - AMZ123跨境导航";
$header['keywords'] = ''.$Pagename.'top卖家,'.$Pagename.'大卖排行,'.$Pagename.'卖家排名';
$header['description'] ='英国亚马逊大卖排行榜单，排名基于最近30天内店铺收到的好评feedback数，了解竞争对手的第一步，先从学习TOP卖家开始。';
//省份统计
$com="co.uk";
$table = "amazon_seller_uk";
$simpleTable = "uk";
$cmap[$simpleTable.'Count']= array('>' => 1);
$clist = db_find("amazon_seller_country",$cmap,[$simpleTable."Count"=>"-1"],1,200);
foreach($clist as $v){
    $cname[$v['code']] = $v['name'];
}
$shengfenCount = unserialize(kv_get("seller_count_sheng_".$table));
foreach($shengfenCount as $v){
    $shengfenCounts[$v['sheng_id']] = $v['COUNT']>0?$v['COUNT']:0;
}

$page = param(3, 1);
$pagesize = 30;
$map = [];
$sf = param(2, 0);
$gj = param(1, '');
$f1 = param('f1');
$f2 = param('f2');
$f3 = param('f3');
$fall = param('fall');
$pnum = param('pnum');
$shengName = db_find("shengfen", [], [], 1, 50);
foreach ($shengName as $k => $v) {
    $orderf[] = $shengfenCounts[$v['id']];
}
array_multisort($orderf, SORT_DESC, $shengName);
$allcount = cache_get('ukseller_count');
if(!$allcount || $allcount<1){
    $allcount =  db_count("amazon_seller_uk");
    cache_set('ukseller_count',$allcount,60*60*24*10);
}

if ($sf) {
    $map['sheng_id'] = $sf;
}
if ($gj) {
    $map['countryCode'] = $gj;
}
$order['feedcount1'] = "-1";
if ($f1) {
    $f1data = explode("_", $f1);
    if (!is_numeric($f1data[0]) || !is_numeric($f1data[1])) {
        unset($f1);
    } else {
        $map['feedcount1'] = array('>=' => $f1data[0], '<=' => $f1data[1]);
    }
    $fparms['f1'] = $f1;
}
if ($f2) {
    $f2data = explode("_", $f2);
    if (!is_numeric($f2data[0]) || !is_numeric($f2data[1])) {
        unset($f2);
    } else {

        $map['feedcount2'] = array('>=' => $f2data[0], '<=' => $f2data[1]);
    }
    $fparms['f2'] = $f2;
}


if ($f3) {
    $f3data = explode("_", $f3);
    if (!is_numeric($f3data[0]) || !is_numeric($f3data[1])) {
        unset($f3);
    } else {
        $map['feedcount3'] = array('>=' => $f3data[0], '<=' => $f3data[1]);
    }
    $fparms['f3'] = $f3;
}


if ($fall) {
    $falldata = explode("_", $fall);
    if (!is_numeric($falldata[0]) || !is_numeric($falldata[1])) {
        unset($f3);
    } else {
        $map['feedcount4'] = array('>=' => $falldata[0], '<=' => $falldata[1]);
    }
    $fparms['fall'] = $fall;
}

if ($pnum) {
    $pnumdata = explode("_", $pnum);
    if (!is_numeric($pnumdata[0]) || !is_numeric($pnumdata[1])) {
        unset($f3);
    } else {
        $map['itemNums'] = array('>=' => $pnumdata[0], '<=' => $pnumdata[1]);
    }
    $fparms['pnum'] = $pnum;
}

if(is_array($fparms)){
    $fpar = "?";
}
foreach($fparms as $k =>$v){
    $fpar.="".$k."=".$v."&";
}


$listPk = db_find("amazon_seller_uk", $map, $order, $page, 50,'',["id"]);
foreach($listPk as $v){
    $pkids[]=$v['id'];
}
$list =  db_find("amazon_seller_uk", ["id"=>$pkids], $order, 1, 50);
foreach ($shengName as $v) {
    $s[$v['id']] = $v['name'];
}
foreach ($list as $k => $v) {
    $list[$k]['brandList'] = IH(Dataa($v['brandList']));
}
$c = db_count("amazon_seller_uk", $map);
$pagination = pagination(url("ukseller-" . $gj . "-" . $sf . "-{page}",["f1"=>$f1,"f2"=>$f2,"fall"=>$fall,"pnum"=>$pnum]), $c, $page, 50);

include _include(APP_PATH . 'plugin/xl_amazon_usseller/htm/common.html');

function Dataa($data)
{
//    $processString = preg_match_all('/\'(.*?)\'/',$data, $data);
//    print_r($data[1]);exit;
//    $allcount = count($data[1]);
//    $sf = $data[1][$allcount-3];
//    return $sf;
    $f = str_replace(array("['", "']", "', '", "','", "[\"", "\"]", "\",\""), "|", $data);
    $d = explode("|", $f);
    return $d;
}

function IH($ary)
{
    $s = '';
    if (is_array($ary)) {
        foreach ($ary as $v) {
            if ($v) {
                $v = str_replace(array("['", "']", "', '", "','", "\"", "',"), "", $v);
                $s .= "<p>" . $v . "</p>";
            }
        }
    }
    return $s;
}
