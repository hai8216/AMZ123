<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') and exit('Access Denied.');
$pluginname = 'xl_amazon_usseller';
$action = param(3) ?: "list";
$tableName = param('ci');
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('amazon_seller_'.$tableName, [], [], $page, $pagesize);
    $c = db_count("amazon_seller_".$tableName, []);
    $pagination = pagination(url("plugin-setting-xl_amazon_usseller-model-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_amazon_usseller/admin/list.html');
} elseif($action=='actionShengfen'){
    $table = "amazon_seller_".$tableName;
    $count = db_sql_find("SELECT COUNT(id) AS COUNT,sheng_id FROM bbs_{$table} WHERE sheng_id>0 and countryCode='CN' GROUP BY sheng_id");
    kv_set("seller_count_sheng_".$table,serialize($count));
    message(0,jump('整理完成',url('plugin-setting-xl_amazon_usseller',["ci"=>$tableName])));
}
elseif($action=='actionguojia'){
    $table = "amazon_seller_".$tableName;
    $count = db_sql_find("SELECT COUNT(id) AS COUNT,countryCode FROM bbs_{$table}  GROUP BY countryCode");
    foreach($count as $v){
        if($v['countryCode']){
             db_update("amazon_seller_country",["code"=>$v['countryCode']],[$tableName."Count"=>$v['COUNT']]);
        }
    }
//    print_r($count);exit;
    message(0,jump('整理完成',url('plugin-setting-xl_amazon_usseller',["ci"=>$tableName])));
}elseif($action=='setTime'){
    kv_set("seller_time_".$tableName,time());
    message(0,jump('整理完成',url('plugin-setting-xl_amazon_usseller',["ci"=>$tableName])));
}elseif($action=='clist'){
    $arrlist = db_find('amazon_seller_country', [], [], 1, 200);
    include _include(APP_PATH . 'plugin/xl_amazon_usseller/admin/clist.html');
}
?>