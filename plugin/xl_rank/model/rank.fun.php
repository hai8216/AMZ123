<?php
function TimeRank($type='1'){
    $TimeRank_15 = cache_get('TimeRank_15_'.$type);
    if (!$TimeRank_15) {
        if($type=='1'){
            $time = strtotime('-7 day');
        }elseif($type=='2'){
            $time = strtotime('-30 day');
        }elseif($type=='3'){
            $time = strtotime('-365 day');
        }elseif($type=='4'){
            $time = strtotime('-2 day');
        }
        $list = db_find("thread", array('fid' => 15, 'create_date' => array('>' => $time)), array('views' => '-1'), 1, 10);
        cache_set('TimeRank_15_'.$type, $list,60*30);
        return $list;
    } else {
        return $TimeRank_15;
    }
}
?>