<?php
//require APP_PATH . '/vendor/autoload.php';
//$ms = memory_get_usage();
//$filename = APP_PATH."/upload/1.xlsx";
//$extension = \PhpOffice\PhpSpreadsheet\IOFactory::identify($filename);
//$objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($extension);
//$objReader->setReadDataOnly(true);
//$objReader->setReadEmptyCells(false);
//$excel_array = $objReader->load($filename);
//unset($excel_array);
//unset($objReader);
//echo  memory_get_usage() - $ms;
//exit;

ini_set('max_execution_time', '0');
require APP_PATH . '/vendor/autoload.php';
require APP_PATH . '/model/es.class.php';
ini_set('memory_limit', '1024M');
$action = param(3, 'usa');
$page = param(4, 1);
$pagesize = 100;
if ($action == 'stars') {
    $id = param('id');
    $indexData = db_find_one("es_index", array('id' => $id));
    db_update("es_index", array('type' => $indexData['type']), array('status' => 0));
    db_update("es_index", array('id' => $id), array('status' => 1));
    db_update("es_status", ['type' => $indexData['type']], ['close' => 0, 'updateTime' => time()]);
    message(0, jump('启用完成', url('plugin-setting-xl_usatopkeywords-' . $indexData['type'])));
} elseif ($action == 'deleteES') {
    $id = param('id');
    $indexData = db_find_one("es_index", array('id' => $id));
    db_update("es_status", ['type' => $indexData['type']], ['close' => 1]);
    $esTable = $indexData['index_name'];
    $es = new es($esTable);
    $es->deleteAll();
    db_delete("es_index", array('id' => $id));
    message(200, '');
} elseif ($action == 'addIndex') {
    $type = param('indextype');
    $esTable = "keyword_" . $type . "_" . date('Ymd');
    $es = new es($esTable);
    if ($es->getIndex()) {
        message(0, jump('存在索引，请操作索引', url('plugin-setting-xl_usatopkeywords-' . $type)));
    }
    $es->addIndex();
    db_insert("es_index", array('index_name' => $esTable, 'dateline' => date('Y-m-d'), 'status' => 0, 'type' => $type));
    message(0, jump('创建完成，请按照步骤索引数据', url('plugin-setting-xl_usatopkeywords-' . $type)));
} elseif ($action == 'exportData') {
    $id = param('id');
    $indexData = db_find_one("es_index", array('id' => $id));
    if ($indexData['status'] == '2') {
        message(500, jump('有未完成的任务', url('plugin-setting-xl_usatopkeywords-' . $type)));
    }
    db_update("es_index", array('id' => $id), array('status' => 2));
    $esTable = $indexData['index_name'];
    $es = new es($esTable);
    $count = db_count("es_keywords_" . $indexData['type']);
    $limit = ceil($count / 3000);
    for ($i = 1; $i <= $limit; $i++) {
        $pc = db_find("es_keywords_" . $indexData['type'], array(), array('newRank' => '1'), $i, 3000);
        $es->moreDate($pc);
        unset($pc);
    }
    db_update("es_index", array('id' => $id), array('status' => 0));
    message(200, '');
} elseif ($action == 'delete') {

//} elseif ($action == 'usa') {
//    if ($method == 'GET') {
//        $navlist = db_find("es_index", array('type' => 'usa'), array('id' => '-1'), $page, $pagesize);
//        foreach ($navlist as $k => $v) {
//            $esTable = $v['index_name'];
//            $es = new es($esTable);
//            $navlist[$k]['count'] = $es->count()['count'];
//        }
//        include _include(APP_PATH . 'plugin/xl_usatopkeywords/ranklist.htm');
//    } else {
//        $starttime = time();
//        $filename = "usa_" . date('Ymd');
//        if (move_uploaded_file($_FILES["file"]["tmp_name"], APP_PATH . "upload/xsl/" . $filename . ".xlsx")) {
//            $filePath = APP_PATH . "upload/xsl/" . $filename . ".xlsx";
//            RankSql('usa', $filePath);
//        }
//        $endtime = time();
//        message(0, '请完成下一步');
//    }
//} elseif ($action == 'japan') {
//    if ($method == 'GET') {
//        $navlist = db_find("es_index", array('type' => $action), array('id' => '-1'), $page, $pagesize);
//        foreach ($navlist as $k => $v) {
//            $esTable = $v['index_name'];
//            $es = new es($esTable);
//            $navlist[$k]['count'] = $es->count()['count'];
//        }
//        include _include(APP_PATH . 'plugin/xl_usatopkeywords/ranklist.htm');
//    } else {
//        $starttime = time();
//        $filename = "japan_" . date('Ymd');
//        if (move_uploaded_file($_FILES["file"]["tmp_name"], APP_PATH . "upload/xsl/" . $filename . ".xlsx")) {
//            $filePath = APP_PATH . "upload/xsl/" . $filename . ".xlsx";
//            RankSql('usa', $filePath);
//        }
//        $endtime = time();
//        message(0, '请完成下一步');
//    }
//
} elseif (in_array($action, array('usa', 'japan', 'en', 'de', 'fr', 'it', 'esp','mx','ca'))) {
    $cname = $action;
    $isStatus = db_find_one("es_status", array('type' => $cname));
    if (!$isStatus['id']) {
        db_insert("es_status", ['type' => $cname, 'close' => 1]);
    }
    if ($method == 'GET') {
        $sqlcouhnt = db_count("es_keywords_" . $cname);
        $navlist = db_find("es_index", array('type' => $cname), array('id' => '-1'), $page, $pagesize);
        foreach ($navlist as $k => $v) {
            $esTable = $v['index_name'];
            $es = new es($esTable);
            $navlist[$k]['count'] = $es->count()['count'];
        }
        include _include(APP_PATH . 'plugin/xl_usatopkeywords/ranklist.htm');
    } else {
        db_update("es_status", ['type' => $cname], ['close' => 1]);
        $starttime = time();
        $filename = $cname . "_" . date('Ymd');
        if (move_uploaded_file($_FILES["file"]["tmp_name"], APP_PATH . "upload/xsl/" . $filename . ".xlsx")) {
            $filePath = APP_PATH . "upload/xsl/" . $filename . ".xlsx";
            RankSql($cname, $filePath);
        }
        $endtime = time();
        message(0, jump('导入完成,使用内存'.memory_get_usage().'',url('plugin-setting-xl_usatopkeywords-'.$cname)));
    }
}


function RankSql($tablename, $filename)
{
    $tablename="bbs_es_keywords_".$tablename;
    $extension = \PhpOffice\PhpSpreadsheet\IOFactory::identify($filename);
    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($extension);
    if ($extension == 'Csv') {
        //默认字符集
        $objReader->setInputEncoding('GBK');
        //默认分隔符
        $objReader->setDelimiter(',');
    }

    $objReader->setReadDataOnly(true);
    $objReader->setReadEmptyCells(false);
    $excel_array = $objReader->load($filename)->getActiveSheet()->toArray();
    db_exec("update  `$tablename` set oldRank = newRank,newRank=0");
    $insertXls = "insert into `$tablename` (`name`,`newRank`,`md5Name`) VALUES ";

    $insert_arr = [];
    for ($i = 1; $i < count($excel_array); $i++) {
        if ($excel_array[$i][1] == '' || !intval($excel_array[$i][2])) {
            continue;
        }
        $rank = intval($excel_array[$i][2]);
        $keyword = addslashes($excel_array[$i][1]);
        $md5Name = md5($keyword);
        $insert_arr[] = "('$keyword',$rank,'$md5Name')";
    }

    for ($offset = 0; $offset < count($insert_arr); $offset += 1000) {
        $sql = $insertXls . implode(',', array_slice($insert_arr, $offset, 1000)) . " ON DUPLICATE KEY UPDATE name=VALUES(name),newRank=VALUES(newRank)";
        db_exec($sql);
    }
//删除如今排名后
    $del = "delete from `$tablename` where newRank = 0 ";
    db_exec($del);
}
