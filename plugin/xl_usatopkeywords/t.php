<?php
require APP_PATH . 'vendor/autoload.php';

//use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$action = param(3, 'base');
if ($action == 'base') {
    if ($method == 'GET') {
        include _include(APP_PATH . 'plugin/xl_usatopkeywords/setting.htm');
    } else {
        $starttime = time();
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
//        // $file = param('file');
        $file = APP_PATH . 'plugin/xl_usatopkeywords/new1.xlsx';
        $extension = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
        $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($extension);
        if ($extension == 'Csv') {
            //默认字符集
            $objReader->setInputEncoding('GBK');
            //默认分隔符
            $objReader->setDelimiter(',');
        }
        $objReader->setReadDataOnly(true);
        $objReader->setReadEmptyCells(false);
//        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        $excel_array = $objReader->load($file)->getActiveSheet()->toArray();
        $insert_data = array();
        $sql3 = "INSERT INTO `bbs_keywords_list` (`name`, `keyword`, `class`, `rank2`,'old') VALUES ";
        foreach ($excel_array as $key => $value) {
            if ($key == 0) {
                continue;
            }
            if ($value[1] == '' || !intval($value[2])) {
                continue;
            }
            $name = md5($value[1]);
            $rank = intval($value[2]);
            $keyword = addslashes($value[1]);
            if ($key > 1) {
                $sql3 .= ",";
            }
            $sql3 .= "('$name','$keyword','$value[0]',$rank,2)";
        }
        $sql1 = "UPDATE `bbs_keywords_list` SET `rank1`=`rank2` ,`old`=1";
        $sql2 = "UPDATE `bbs_keywords_list` SET `rank2`=250001,'old'=1";
        $sql3 .= "on duplicate key update class = VALUES(class),rank2 = VALUES(rank2),old=2;";
        $sql4 = "UPDATE `bbs_keywords_list` SET `up`=`rank1`-`rank2`";
        db_exec("START TRANSACTION");
        $result1 = db_exec($sql1) === false ? false : true;
        if ($result1) {
            $result2 = db_exec($sql2) === false ? false : true;
            if ($result2) {
                $result3 = db_exec($sql3) === false ? false : true;
                if ($result3) {
                    $result4 = db_exec($sql4) === false ? false : true;
                    if ($result4) {
                        $endtime = time();
                        db_exec("COMMIT");
                        message(0, ''.$endtime-$starttime.'，请不要重复点击。');
                    }
                }
            }
        }
        db_exec("ROLLBACK");
        message(0, '导入失败');
    }
}elseif($action=='upload'){
//    echo APP_PATH."plugin/xl_usatopkeywords/xsl/usa.xlsx";exit;
//    print_r($_FILES["file"]["name"]);exit;
    $filename = rand(11111111111,91111111111).time();
    if(move_uploaded_file($_FILES["file"]["tmp_name"],APP_PATH."upload/xsl/".$filename.".xlsx")){
        $starttime = time();
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
//        // $file = param('file');
        $file = APP_PATH."upload/xsl/".$filename.".xlsx";
        $extension = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
        $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($extension);
        if ($extension == 'Csv') {
            //默认字符集
            $objReader->setInputEncoding('GBK');
            //默认分隔符
            $objReader->setDelimiter(',');
        }
        $objReader->setReadDataOnly(true);
        $objReader->setReadEmptyCells(false);
//        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        $excel_array = $objReader->load($file)->getActiveSheet()->toArray();
        $insert_data = array();
        $sql3 = "INSERT INTO `bbs_keywords_list` (`name`, `keyword`, `class`, `rank2`) VALUES ";
        foreach ($excel_array as $key => $value) {
            if ($key == 0) {
                continue;
            }
            if ($value[1] == '' || !intval($value[2])) {
                continue;
            }
            $name = md5($value[1]);
            $rank = intval($value[2]);
            $keyword = addslashes($value[1]);
            if ($key > 1) {
                $sql3 .= ",";
            }
            $sql3 .= "('$name','$keyword','$value[0]',$rank)";
        }
        $sql1 = "UPDATE `bbs_keywords_list` SET `rank1`=`rank2`";
        $sql2 = "UPDATE `bbs_keywords_list` SET `rank2`=250001";
        $sql3 .= "on duplicate key update class = VALUES(class),rank2 = VALUES(rank2);";
        $sql4 = "UPDATE `bbs_keywords_list` SET `up`=`rank1`-`rank2`";
        db_exec("START TRANSACTION");
        $result1 = db_exec($sql1) === false ? false : true;
        if ($result1) {
            $result2 = db_exec($sql2) === false ? false : true;
            if ($result2) {
                $result3 = db_exec($sql3) === false ? false : true;
                if ($result3) {
                    $result4 = db_exec($sql4) === false ? false : true;
                    if ($result4) {
                        $endtime = time();
                        db_exec("COMMIT");
                        message(0, '导入成功，耗时:'.$endtime-$starttime.'，请不要重复点击。');
                    }
                }
            }
        }
        db_exec("ROLLBACK");
        message(0, '导入失败');
    };
}elseif($action=='uploadyg'){
//    echo APP_PATH."plugin/xl_usatopkeywords/xsl/usa.xlsx";exit;
//    print_r($_FILES["file"]["name"]);exit;
    $filename = rand(11111111111,91111111111).time();
    if(move_uploaded_file($_FILES["file"]["tmp_name"],APP_PATH."upload/xsl/".$filename.".xlsx")){
        $starttime = time();
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
//        // $file = param('file');
        $file = APP_PATH."upload/xsl/".$filename.".xlsx";
        $extension = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
        $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($extension);
        if ($extension == 'Csv') {
            //默认字符集
            $objReader->setInputEncoding('GBK');
            //默认分隔符
            $objReader->setDelimiter(',');
        }
        $objReader->setReadDataOnly(true);
        $objReader->setReadEmptyCells(false);
//        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        $excel_array = $objReader->load($file)->getActiveSheet()->toArray();
        $insert_data = array();
        $sql3 = "INSERT INTO `bbs_keywords_list_yg` (`name`, `keyword`, `class`, `rank2`) VALUES ";
        foreach ($excel_array as $key => $value) {
            if ($key == 0) {
                continue;
            }
            if ($value[1] == '' || !intval($value[2])) {
                continue;
            }
            $name = md5($value[1]);
            $rank = intval($value[2]);
            $keyword = addslashes($value[1]);
            if ($key > 1) {
                $sql3 .= ",";
            }
            $sql3 .= "('$name','$keyword','$value[0]',$rank)";
        }
        $sql1 = "UPDATE `bbs_keywords_list_yg` SET `rank1`=`rank2`";
        $sql2 = "UPDATE `bbs_keywords_list_yg` SET `rank2`=250001";
        $sql3 .= "on duplicate key update class = VALUES(class),rank2 = VALUES(rank2);";
        $sql4 = "UPDATE `bbs_keywords_list_yg` SET `up`=`rank1`-`rank2`";
        db_exec("START TRANSACTION");
        $result1 = db_exec($sql1) === false ? false : true;
        if ($result1) {
            $result2 = db_exec($sql2) === false ? false : true;
            if ($result2) {
                $result3 = db_exec($sql3) === false ? false : true;
                if ($result3) {
                    $result4 = db_exec($sql4) === false ? false : true;
                    if ($result4) {
                        $endtime = time();
                        db_exec("COMMIT");
                        message(0, '导入成功，耗时:'.$endtime-$starttime.'，请不要重复点击。');
                    }
                }
            }
        }
        db_exec("ROLLBACK");
        message(0, '导入失败');
    };
}




elseif($action=='uploadde'){
//    echo APP_PATH."plugin/xl_usatopkeywords/xsl/usa.xlsx";exit;
//    print_r($_FILES["file"]["name"]);exit;
    $filename = rand(11111111111,91111111111).time();
    if(move_uploaded_file($_FILES["file"]["tmp_name"],APP_PATH."upload/xsl/".$filename.".xlsx")){
        $starttime = time();
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
//        // $file = param('file');
        $file = APP_PATH."upload/xsl/".$filename.".xlsx";
        $extension = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
        $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($extension);
        if ($extension == 'Csv') {
            //默认字符集
            $objReader->setInputEncoding('GBK');
            //默认分隔符
            $objReader->setDelimiter(',');
        }
        $objReader->setReadDataOnly(true);
        $objReader->setReadEmptyCells(false);
//        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        $excel_array = $objReader->load($file)->getActiveSheet()->toArray();
        $insert_data = array();
        $sql3 = "INSERT INTO `bbs_keywords_list_de` (`name`, `keyword`, `class`, `rank2`) VALUES ";
        foreach ($excel_array as $key => $value) {
            if ($key == 0) {
                continue;
            }
            if ($value[1] == '' || !intval($value[2])) {
                continue;
            }
            $name = md5($value[1]);
            $rank = intval($value[2]);
            $keyword = addslashes($value[1]);
            if ($key > 1) {
                $sql3 .= ",";
            }
            $sql3 .= "('$name','$keyword','$value[0]',$rank)";
        }
        $sql1 = "UPDATE `bbs_keywords_list_de` SET `rank1`=`rank2`";
        $sql2 = "UPDATE `bbs_keywords_list_de` SET `rank2`=250001";
        $sql3 .= "on duplicate key update class = VALUES(class),rank2 = VALUES(rank2);";
        $sql4 = "UPDATE `bbs_keywords_list_de` SET `up`=`rank1`-`rank2`";
        db_exec("START TRANSACTION");
        $result1 = db_exec($sql1) === false ? false : true;
        if ($result1) {
            $result2 = db_exec($sql2) === false ? false : true;
            if ($result2) {
                $result3 = db_exec($sql3) === false ? false : true;
                if ($result3) {
                    $result4 = db_exec($sql4) === false ? false : true;
                    if ($result4) {
                        $endtime = time();
                        db_exec("COMMIT");
                        message(0, '导入成功，耗时:'.$endtime-$starttime.'，请不要重复点击。');
                    }
                }
            }
        }
        db_exec("ROLLBACK");
        message(0, '导入失败');
    };
}

elseif($action=='uploadfg'){
//    echo APP_PATH."plugin/xl_usatopkeywords/xsl/usa.xlsx";exit;
//    print_r($_FILES["file"]["name"]);exit;
    $filename = rand(11111111111,91111111111).time();
    if(move_uploaded_file($_FILES["file"]["tmp_name"],APP_PATH."upload/xsl/".$filename.".xlsx")){
        $starttime = time();
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
//        // $file = param('file');
        $file = APP_PATH."upload/xsl/".$filename.".xlsx";
        $extension = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
        $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($extension);
        if ($extension == 'Csv') {
            //默认字符集
            $objReader->setInputEncoding('GBK');
            //默认分隔符
            $objReader->setDelimiter(',');
        }
        $objReader->setReadDataOnly(true);
        $objReader->setReadEmptyCells(false);
//        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        $excel_array = $objReader->load($file)->getActiveSheet()->toArray();
        $insert_data = array();
        $sql3 = "INSERT INTO `bbs_keywords_list_fg` (`name`, `keyword`, `class`, `rank2`) VALUES ";
        foreach ($excel_array as $key => $value) {
            if ($key == 0) {
                continue;
            }
            if ($value[1] == '' || !intval($value[2])) {
                continue;
            }
            $name = md5($value[1]);
            $rank = intval($value[2]);
            $keyword = addslashes($value[1]);
            if ($key > 1) {
                $sql3 .= ",";
            }
            $sql3 .= "('$name','$keyword','$value[0]',$rank)";
        }
        $sql1 = "UPDATE `bbs_keywords_list_fg` SET `rank1`=`rank2`";
        $sql2 = "UPDATE `bbs_keywords_list_fg` SET `rank2`=250001";
        $sql3 .= "on duplicate key update class = VALUES(class),rank2 = VALUES(rank2);";
        $sql4 = "UPDATE `bbs_keywords_list_fg` SET `up`=`rank1`-`rank2`";
        db_exec("START TRANSACTION");
        $result1 = db_exec($sql1) === false ? false : true;
        if ($result1) {
            $result2 = db_exec($sql2) === false ? false : true;
            if ($result2) {
                $result3 = db_exec($sql3) === false ? false : true;
                if ($result3) {
                    $result4 = db_exec($sql4) === false ? false : true;
                    if ($result4) {
                        $endtime = time();
                        db_exec("COMMIT");
                        message(0, '导入成功，耗时:'.$endtime-$starttime.'，请不要重复点击。');
                    }
                }
            }
        }
        db_exec("ROLLBACK");
        message(0, '导入失败');
    };
}

elseif($action=='uploadjp'){
//    echo APP_PATH."plugin/xl_usatopkeywords/xsl/usa.xlsx";exit;
//    print_r($_FILES["file"]["name"]);exit;
    $filename = rand(11111111111,91111111111).time();
    if(move_uploaded_file($_FILES["file"]["tmp_name"],APP_PATH."upload/xsl/".$filename.".xlsx")){
        $starttime = time();
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
//        // $file = param('file');
        $file = APP_PATH."upload/xsl/".$filename.".xlsx";
        $extension = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
        $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($extension);
        if ($extension == 'Csv') {
            //默认字符集
            $objReader->setInputEncoding('GBK');
            //默认分隔符
            $objReader->setDelimiter(',');
        }
        $objReader->setReadDataOnly(true);
        $objReader->setReadEmptyCells(false);
//        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        $excel_array = $objReader->load($file)->getActiveSheet()->toArray();
        $insert_data = array();
        $sql3 = "INSERT INTO `bbs_keywords_list_jp` (`name`, `keyword`, `class`, `rank2`) VALUES ";
        foreach ($excel_array as $key => $value) {
            if ($key == 0) {
                continue;
            }
            if ($value[1] == '' || !intval($value[2])) {
                continue;
            }
            $name = md5($value[1]);
            $rank = intval($value[2]);
            $keyword = addslashes($value[1]);
            if ($key > 1) {
                $sql3 .= ",";
            }
            $sql3 .= "('$name','$keyword','$value[0]',$rank)";
        }
        $sql1 = "UPDATE `bbs_keywords_list_jp` SET `rank1`=`rank2`";
        $sql2 = "UPDATE `bbs_keywords_list_jp` SET `rank2`=250001";
        $sql3 .= "on duplicate key update class = VALUES(class),rank2 = VALUES(rank2);";
        $sql4 = "UPDATE `bbs_keywords_list_jp` SET `up`=`rank1`-`rank2`";
        db_exec("START TRANSACTION");
        $result1 = db_exec($sql1) === false ? false : true;
        if ($result1) {
            $result2 = db_exec($sql2) === false ? false : true;
            if ($result2) {
                $result3 = db_exec($sql3) === false ? false : true;
                if ($result3) {
                    $result4 = db_exec($sql4) === false ? false : true;
                    if ($result4) {
                        $endtime = time();
                        db_exec("COMMIT");
                        message(0, '导入成功，耗时:'.$endtime-$starttime.'');
                    }
                }
            }
        }
        db_exec("ROLLBACK");
        message(0, '导入失败');
    };
}