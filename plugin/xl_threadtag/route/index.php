<?php
$route = param(1);
$tagdata = db_find_one("thread_tag", array('tag_name' => urldecode($route)));
if ($tagdata['tag_id']) {
    //查询这个标签下的帖子
    $page = param(2, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $tagthraedlist = db_find("thread_tag_keys", array('tags_id' => $tagdata['tag_id']), array('keys_id' => '-1'), 1, 8000);
    foreach ($tagthraedlist as $v) {
        $tids[] = $v['tid'];
    }
    $thraedlist = db_find("thread", array('tid' => $tids), array('create_date' => '-1'), $page, $pagesize);
    foreach ($thraedlist as $k => $v) {
        $thraedlist[$k]['user'] = user_read_cache($v['uid']);
        $thraedlist[$k]['create_date'] = humandate($v['create_date']);
        $thraedlist[$k]['img'] = getPost_img($v['tid']);
        $thraedlist[$k]['jianjie'] = $v['jianjie'] ? $v['jianjie'] : post_message($v['tid']);
    }
    $c = db_count("thread_tag_keys", array('tags_id' => $tagdata['tag_id']));
    $pagination = pagination(url("tag-" . urldecode($route) . "-{page}"), $c, $page, $pagesize);
    //热门标签
    $randTag = db_sql_find("SELECT * FROM bbs_thread_tag  ORDER BY 37*(UNIX_TIMESTAMP() ^ tag_id) & 0xffff LIMIT 30");
    $header['title'] = $tagdata['seo_title'] ? $tagdata['seo_title'] : $tagdata['tag_name'];
    $header['keywords'] = $tagdata['seo_keywords'];
    $header['description'] = $tagdata['seo_desc'];

    if (cache_get('tag_hotthreadlist_time' . $tagdata['tag_id']) <= time()) {
        //标签下点击最高的帖子
        $hotthread = db_find("thread", array('tid' => $tids), array('views' => '-1'), 1, 10);
        //缓存下
        cache_set('tag_hotthreadlist' . $tagdata['tag_id'], serialize($hotthread));
        cache_set('tag_hotthreadlist_time' . $tagdata['tag_id'], time() + 10 * 60);
    } else {
        $hotthread = unserialize(cache_get('tag_hotthreadlist' . $tagdata['tag_id']));
    }

    //增加点击量,5分钟一次缓存
    $cache_views_time = cache_get('tag_views_time' . $tagdata['tag_id']);
    $cache_views = cache_get('tag_views_' . $tagdata['tag_id']);
    if (!$cache_views) {
        cache_set('tag_views_' . $tagdata['tag_id'], $tagdata['views'] + 1);
        cache_set('tag_views_time' . $tagdata['tag_id'], time() + 5*60);
    } else {
        if ($cache_views_time <= time()) {
            db_exec("UPDATE LOW_PRIORITY bbs_thread_tag SET `views`='$cache_views'+1 WHERE tag_id='$tagdata[tag_id]'");
            //缓存下
            cache_set('tag_views_time' . $tagdata['tag_id'], time() + 5*60);
            cache_set('tag_views_' . $tagdata['tag_id'], $cache_views + 1);
        } else {
            cache_set('tag_views_' . $tagdata['tag_id'], $cache_views + 1);
        }
    }
    include _include(APP_PATH . 'plugin/xl_threadtag/tpl/index.html');
}