<?php
$ajax = param('ajax');
$page = param(1, 1);
$pagesize = 21;
$start = ($page - 1) * $pagesize;
$userlist = db_sql_find("SELECT * FROM bbs_user where gid=106 ORDER BY RAND() LIMIT 21");

foreach ($userlist as $k => $v) {
    $dir = substr(sprintf("%09d", $v['uid']), 0, 3);
    $userlist[$k]['avatar_url'] = $v['avatar'] ? $conf['upload_url'] . "avatar/" . $dir . "/" . $v['uid'] . ".png" : 'view/img/avatar.png';
    $userlist[$k]['threadlist'] = getUserThread($v['uid']);
    $userlist[$k]['follow'] = $haya_follow_check_follow_user = haya_follow_find_by_uid_and_follow_uid($v['uid'], $uid);
}
$header['title'] = '专栏作者 | AMZ123亚马逊卖家导航';
function getUserThread($uid)
{
    //还是缓存
    $cache_views_time = cache_get('user_thread_' . $uid);
    $cache_views = cache_get('user_thread_data' . $uid);
    if ($cache_views_time <= time()) {
        $udata = db_find("thread", array('uid' => $uid), array('create_date'=>'-1'), 1, 4);
        cache_set('user_thread_data' . $uid, serialize($udata));
        cache_set('user_thread_' . $uid, time()+12*60*60);
        return  $udata;
    } else {
        return  unserialize($cache_views);
    }
}
if($ajax){
    include _include(APP_PATH . 'plugin/xl_threadtag/tpl/author_ajax.html');
}else{
    include _include(APP_PATH . 'plugin/xl_threadtag/tpl/author.html');
}