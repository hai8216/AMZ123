<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_threadtag';
$action = param(3) ?: "list";

if ($action == 'list') {

    $page = param(4, 1);
    $pagesize = 50;
    $start = ($page - 1) * $pagesize;
    if(param('keyword')){
        $arrlist = db_find("thread_tag", array('tag_name'=>array('LIKE'=>param('keyword'))), array(), $page, $pagesize);
        $c = db_count("thread_tag", array('tag_name'=>array('LIKE'=>param('keyword'))));
        $pagination = pagination(url("plugin-setting-xl_threadtag-list-{page}",array('keyword'=>param('keyword'))), $c, $page, $pagesize);
    }elseif(param('eempty')){
        $arrlist = db_find("thread_tag", array('e'=>''), array(), $page, $pagesize);
        $c = db_count("thread_tag", array('e'=>''));
        $pagination = pagination(url("plugin-setting-xl_threadtag-list-{page}",array('eempty'=>1)), $c, $page, $pagesize);
    }else{
        $arrlist = db_find("thread_tag", array(), array(), $page, $pagesize);
        $c = db_count("thread_tag");
        $pagination = pagination(url("plugin-setting-xl_threadtag-list-{page}"), $c, $page, $pagesize);
    }
    include _include(APP_PATH . 'plugin/' . $pluginname . '/admin/list.html');
} elseif($action=='top'){
    if ($method == 'GET') {
        $input = array();
        $datas = db_find_one("thread_tag", array('tag_id' => param('tag_id')));
        $input['top1'] = form_text("top1", $datas['top1']);
        $input['top2'] = form_text("top2", $datas['top2']);
        $input['top3'] = form_text("top3", $datas['top3']);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/admin/top.html');
    } else {
        db_update("thread_tag", array('tag_id' => param('tag_id')),array('top1'=>param('top1'),'top2'=>param('top2'),'top3'=>param('top3')));
        message(0, jump('保存成功', url('plugin-setting-xl_threadtag-list-'.param(4,1))));
    }
}elseif ($action == 'export') {
    if ($method == 'GET') {
        include _include(APP_PATH . 'plugin/' . $pluginname . '/admin/export.html');
    } else {

        move_uploaded_file($_FILES["txt"]["tmp_name"], XIUNOPHP_PATH . '../plugin/xl_threadtag/tmp/tag.txt');
        $file_path = XIUNOPHP_PATH . '../plugin/xl_threadtag/tmp/tag.txt';
        if (file_exists($file_path)) {
            $fp = fopen($file_path, "r");
            $str = fread($fp, filesize($file_path));
            $str = explode("\r\n", $str);
            foreach ($str as $v) {
                db_insert("thread_tag", array('tag_name' => $v,'e'=>pinyin1($v)));
            }
            fclose($fp);
            message(0, jump('导入成功', url('plugin-setting-xl_threadtag')));
        }
    }
} elseif ($action == 'addtextarea') {
    if ($method == 'GET') {
        $textarea = form_textarea("textarea", '');
        include _include(APP_PATH . 'plugin/' . $pluginname . '/admin/textarea.html');
    } else {
        $str = $_POST['textarea'];
        $str = explode("\r\n", $str);
        foreach ($str as $v) {
            db_insert("thread_tag", array('tag_name' => $v,'e'=>pinyin1($v)));
        }
        fclose($fp);
        message(0, jump('导入成功', url('plugin-setting-xl_threadtag')));
    }
} elseif
($action == 'addtag') {
    if ($method == 'GET') {
        if (param('tag_id')) {
            $data = db_find_one("thread_tag", array('tag_id' => param('tag_id')));
        }
        $input['tag_name'] = form_text("tag_name", $data['tag_name']);
        $input['seo_title'] = form_text("seo_title", $data['seo_title']);
        $input['seo_keywords'] = form_text("seo_keywords", $data['seo_keywords']);
        $input['seo_desc'] = form_textarea("seo_desc", $data['seo_desc']);
        $input['desc'] = form_textarea("desc", $data['desc'],'',200);
        $input['sip_desc'] = form_textarea("sip_desc", $data['sip_desc']);
        $input['e'] = form_text("e", $data['e']);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/admin/add.html');
    } else {
        $dataAry = array(
            'tag_name' => param('tag_name'),
            'seo_title' => param('seo_title'),
            'seo_keywords' => param('seo_keywords'),
            'seo_desc' => param('seo_desc','',false),
            'desc' => param('desc','',false),
            'sip_desc' => param('sip_desc','',false),
            'e'=>param('e')?:pinyin1(param('tag_name'))
        );
        if (param('tag_id')) {
            $data = db_find_one("thread_tag", array('tag_id' => param('tag_id')));
            db_update("thread_tag", array('tag_id' => param('tag_id')), $dataAry);
        } else {
            db_insert("thread_tag", $dataAry);
        }
        message(0, jump('操作完成', url('plugin-setting-xl_threadtag')));
    }
}elseif ($action == 'del') {
    $did = param('delid');
    db_delete("thread_tag", array('tag_id' => $did));
//    db_delete("tools_model", array('pages_id' => $did));
    message(0, jump('删除成功', url('plugin-setting-xl_threadtag-list', array('toid' => param('toid')))));
}

function getfirstchar($s0){
    $fchar = ord($s0{0});

    if($fchar >= ord("A") and $fchar <= ord("z") )return strtoupper($s0{0});
    $s1 = iconv("UTF-8","gb2312", $s0);
    $s2 = iconv("gb2312","UTF-8", $s1);
    if($s2 == $s0){$s = $s1;}else{$s = $s0;}
    $asc = ord($s{0}) * 256 + ord($s{1}) - 65536;
    if($asc >= -20319 and $asc <= -20284) return "A";
    if($asc >= -20283 and $asc <= -19776) return "B";
    if($asc >= -19775 and $asc <= -19219) return "C";
    if($asc >= -19218 and $asc <= -18711) return "D";
    if($asc >= -18710 and $asc <= -18527) return "E";
    if($asc >= -18526 and $asc <= -18240) return "F";
    if($asc >= -18239 and $asc <= -17923) return "G";
    if($asc >= -17922 and $asc <= -17418) return "H";
    if($asc >= -17417 and $asc <= -16475) return "J";
    if($asc >= -16474 and $asc <= -16213) return "K";
    if($asc >= -16212 and $asc <= -15641) return "L";
    if($asc >= -15640 and $asc <= -15166) return "M";
    if($asc >= -15165 and $asc <= -14923) return "N";
    if($asc >= -14922 and $asc <= -14915) return "O";
    if($asc >= -14914 and $asc <= -14631) return "P";
    if($asc >= -14630 and $asc <= -14150) return "Q";
    if($asc >= -14149 and $asc <= -14091) return "R";
    if($asc >= -14090 and $asc <= -13319) return "S";
    if($asc >= -13318 and $asc <= -12839) return "T";
    if($asc >= -12838 and $asc <= -12557) return "W";
    if($asc >= -12556 and $asc <= -11848) return "X";
    if($asc >= -11847 and $asc <= -11056) return "Y";
    if($asc >= -11055 and $asc <= -10247) return "Z";
    return null;
}

function pinyin1($zh){
    if($zh=='汶川'){
        return "W";
    }else{
        return  getfirstchar($zh);
    }
}
?>