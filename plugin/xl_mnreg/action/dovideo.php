<?php
$ext = db_find_one("moni_ext",["uid"=>$uid]);
if(!$ext['id']){
    $new = [
        "uid"=>$uid,
        "video"=>$data['videoStatus']?:1
    ];
    db_insert("moni_ext",$new);
    returnJson('200', $new);
}else{
    db_update("moni_ext",["uid"=>$uid],["video"=>$data['videoStatus']?:1]);
    $ext = db_find_one("moni_ext",["uid"=>$uid]);
    returnJson('200', $ext);
}


