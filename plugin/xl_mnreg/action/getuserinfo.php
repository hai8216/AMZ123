<?php
if($uid){
    $token = user_token_gen($uid);
    $userInfo['token'] = $token;
    $userInfo['userinfo'] = getvalue(["avatar","username"],user__read($uid));
    returnJson('200', $userInfo);
}else{
    returnJson('100', []);
}