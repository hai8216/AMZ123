<?php
if ($method == 'GET') {
    exit;
} else {
    $data = json_decode(file_get_contents("php://input"), true);
    $isHave = db_find_one("moni_ext", ["uid" => $uid]);
    if ($isHave) {
        db_update("moni_ext",["uid" => $uid],["type"=>$data['type'],"setp"=>$data['setp']]);
    }else{
        $insert['uid'] = $uid;
        $insert['type'] = $data['type'];
        $insert['setp'] = $data['setp'];
        db_insert("moni_ext", $insert);
    }
    returnJson('200', "ok");
}