<?php

if ($method == 'GET') {
    exit;
} else {
    $data = json_decode(file_get_contents("php://input"), true);
    $isHave = db_find_one("moni_liangbu", ["uid" => $uid,"setp"=>$data['setp']]);
    if ($isHave) {
        returnJson('200', "ok");
    }else{
        $insert['uid'] = $uid;
        $insert['setp'] = $data['setp'];
        $insert['data'] = unserialize($data['data']);
        db_insert("moni_liangbu", $insert);
    }
    returnJson('200', "ok");
}