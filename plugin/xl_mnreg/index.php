<?php
/* 200 ok
*  201 未登录
 * 202 存在数据
 * 203 单个数据不存在
*/
header("access-control-allow-headers: Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Mx-ReqToken,X-Requested-With");
header("access-control-allow-methods: GET, POST, PUT, DELETE, HEAD, OPTIONS");
header("access-control-allow-credentials: true");
header("access-control-allow-origin: *");
$action = param('action');
if ($action != 'login') {
    if (!$uid) {
        returnJson('201', "not login");
    }
}
if ($action == 'login') {
    $data = json_decode(file_get_contents("php://input"), true);
    $email = $data['username'];
    $password = $data['password'];
    empty($email) and returnJson('500', lang('email_is_empty'));
    if (is_email($email, $err)) {
        $_user = user_read_by_email($email);
        empty($_user) and returnJson('500', lang('email_not_exists'));
    } else {
        $_user = user_read_by_username($email);
        empty($_user) and returnJson('500', lang('username_not_exists'));
    }
    md5(md5($password) . $_user['salt']) != $_user['password'] and returnJson('500', lang('password_incorrect'));


    user_update($_user['uid'], array('login_ip' => $longip, 'login_date' => $time, 'logins+' => 1));

    $token = user_token_gen($_user['uid']);
    $userInfo['token'] = $token;
    $userInfo['userinfo'] = getvalue(["avatar","username"],user__read($_user['uid']));
    returnJson('200', $userInfo);

} elseif ($action == 'postData') {
    if ($method == 'GET') {
        exit;
    } else {
        $data = json_decode(file_get_contents("php://input"), true);
        $isHave = db_find_one("moni_reg", ["uid" => $uid, "setp" => $data['setp']]);
        if ($isHave) {
            returnJson('202', "ok");
        }
        $insert['uid'] = $uid;
        $insert['setp'] = $data['setp'];
        $insert['data'] = serialize($data);
        db_insert("moni_reg", $insert);
        returnJson('200', "ok");
    }
} elseif ($action == 'getSetp') {
    $lastSetp = db_find_one("moni_reg", ["uid" => $uid], ["id" => -1]);
    if ($lastSetp['uid']) {
        returnJson('200', intval($lastSetp['setp']) + 1);
    } else {
        returnJson('200', 1);
    }

} elseif ($action == 'getSetpData') {
    $data = json_decode(file_get_contents("php://input"), true);
    $oneData = db_find_one("moni_reg", ["uid" => $uid, "setp" => $data['setp']]);
    if ($oneData['uid']) {
        if (isset($data['filed']) && $data['filed']) {
            $oneData = unserialize($oneData['data'])[$data['filed']];
        } else {
            $oneData = unserialize($oneData['data']);
        }
        returnJson('200', $oneData);
    } else {
        returnJson('203', 1);
    }

}else{
    $data = json_decode(file_get_contents("php://input"), true);
    include APP_PATH."/plugin/xl_mnreg/action/{$action}.php";

}
function getvalue($ary,$needary){
    global $conf;
    $dir = substr(sprintf("%09d", $needary['uid']), 0, 3);
    $needary['avatar'] = $needary['avatar'] ? "https://www.amz123.com/".$conf['upload_url']."avatar/$dir/$needary[uid].png?".$needary['avatar'] : 'https://www.amz123.com/view/img/avatar.png';
    $newData = [];
    foreach($needary as $k=>$v){
        if(in_array($k,$ary)){
            $newData[$k]=$v;
        }
    }
    return $newData;
}
function returnJson($code, $parms = [])
{
    $data['code'] = $code;
    $data['data'] = $parms;
    echo json_encode($data);
    exit;
}