<?php
function passcode_get_code($tid){
    $thread = thread_read($tid);
    $right_code = $thread['passcode'];
    return $right_code;
}
function passcode_insert_db($tid,$uid){
    db_insert('thread_code',array('tid'=>$tid,'uid'=>$uid,'time'=>time()));
}
function passcode_check_ip(){
    global $longip;
    $set_passcode = setting_get('tt_passcode');
    $max_ip = $set_passcode['max_ip'];
    if($max_ip=='0') return TRUE;
    $fail_count = db_count('thread_code_ip',array('ip'=>$longip));
    return $fail_count<$max_ip;
}
function passcode_insert_ip(){
    global $longip;
    db_insert('thread_code_ip',array('ip'=>$longip,'time'=>time()));
}
function passcode_daily_cron(){
    db_truncate('thread_code_ip');
}
function passcode_verify($tid,$code,$uid){
    $result = db_find_one('thread_code',array('tid'=>$tid,'uid'=>$uid));
    if($result) return 1;
    $check_ip_result = passcode_check_ip();
    if(!$check_ip_result) return -2;//max
    $right_code = passcode_get_code($tid);
    if($right_code==$code){
        passcode_insert_db($tid,$uid);
        return 1;
    } else {
        passcode_insert_ip();
        return -1;
    }
}
function passcode_need_input($tid){
    global $uid,$group;
    if($group['gid']==1) return FALSE;
    $thread = thread_read($tid);
    if($thread['passcode']=='0') return FALSE;
    if($uid==$thread['uid'])return FALSE;
    $result = db_find_one('thread_code',array('tid'=>$tid,'uid'=>$uid));
    if($result) return FALSE;
    else return TRUE;
}
?>