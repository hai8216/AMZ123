<?php
!defined('DEBUG') AND exit('Forbidden');
$tablepre = $db->tablepre;
$sql = "ALTER TABLE {$tablepre}group ADD COLUMN allowcode INT(8) DEFAULT '0'";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread ADD COLUMN passcode CHAR(20) DEFAULT '0'";
db_exec($sql);
$sql="CREATE TABLE IF NOT EXISTS `{$tablepre}thread_code` (
  `code_id` int(10) NOT NULL AUTO_INCREMENT,
  `tid` int(10) NOT NULL,
  `uid` int(10) DEFAULT '0',
  `time` int(20) DEFAULT '0',
  PRIMARY KEY (code_id),
	KEY (code_id),
	UNIQUE KEY (code_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
db_exec($sql);
$sql="CREATE TABLE IF NOT EXISTS `{$tablepre}thread_code_ip` (
  `try_id` int(10) NOT NULL AUTO_INCREMENT,
  `time` int(20) NOT NULL,
  `ip` int(20) NOT NULL,
  PRIMARY KEY (try_id),					# 
	KEY (try_id),						# 
	UNIQUE KEY (try_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
db_exec($sql);
group_list_cache_delete();
setting_set('tt_passcode',array('max_ip'=>5));
?>