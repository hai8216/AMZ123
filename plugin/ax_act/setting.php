<?php

!defined('DEBUG') AND exit('Access Denied.');

$action = param(3);
$_gid = param('post_gid', array());
$tw = param('post_tw');
$black = param('black');
$shop = param('shop');
$ewm = param('ewm');
$ewm_name = param('ewm_name');

if ($action == 'cate') {
    $list = db_find("thread_cate", array(), array('order_id'=>'-1'), 1, 200);
    include _include(APP_PATH . 'plugin/ax_act/cate.htm');
} elseif ($action == 'addcate') {
    include _include(APP_PATH . 'plugin/ax_act/addcate.htm');
}elseif($action=='edcate'){
    $val = db_find_one("thread_cate",array('cateid'=>param(4)));
    include _include(APP_PATH . 'plugin/ax_act/addcate.htm');
} elseif ($action == 'doaddnav') {
    if(param('id')){
        $data = array('catename' => param('nav_title'), 'order_id' => param('order_id', 0));
        db_update("thread_cate",array('cateid'=>param('id')),$data);
    }else{
        $data = array('catename' => param('nav_title'), 'order_id' => param('order_id', 0));
        db_insert("thread_cate",$data);
    }

    message(0,'ok');
} elseif($action=='delete'){
    db_delete('thread_cate',array('cateid'=>param('id')));
    message(0,'ok');
}else {


    if ($method == 'GET') {
//初始化设置	
        $ax_act = kv_get('ax_act');
        if (empty($ax_act)) {

            $ax_act = array(
                'gid' => $_gid,
                'tw' => $tw,
                'black' => $black,
                'shop' => $shop,
                'ewm' => $ewm,
                'ewm_name' => $ewm_name
            );
            kv_set('ax_act', $ax_act);
        }
//初始化结束
        $input = form_text('ewm', $ax_act['ewm'], '100%', '输入二维码地址');
        $input2 = form_text('ewm_name', $ax_act['ewm_name'], '100%', '二维码文字');

        include _include(APP_PATH . 'plugin/ax_act/setting.htm');
    } else {

        $ax_act['gid'] = $_gid;
        $ax_act['tw'] = $tw;
        $ax_act['black'] = $black;
        $ax_act['shop'] = $shop;
        $ax_act['ewm'] = $ewm;
        $ax_act['ewm_name'] = $ewm_name;
        kv_set('ax_act', $ax_act);
        message(0, '修改成功');
    }


}

?>
