<?php 

!defined('DEBUG') AND exit('Forbidden');
$tablepre = $db->tablepre;


$sql = "ALTER TABLE {$tablepre}thread drop column ax_pic";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_time_1";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_time_2";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_place";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_sp";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_type";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_link";
db_exec($sql);


$sql = "ALTER TABLE {$tablepre}thread drop column ax_country";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_name";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_email";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_weixin";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_ymx";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread drop column ax_facebook";
db_exec($sql);
?>