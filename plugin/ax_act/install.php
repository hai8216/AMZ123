<?php 
!defined('DEBUG') AND exit('Forbidden');
$tablepre = $db->tablepre;
//图片
$sql = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_pic text(0) DEFAULT ''";
db_exec($sql);
//时间，
$sql2 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_time_1  datetime";//开始时间
db_exec($sql2);

$sql3 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_time_2 datetime";//结束时间
db_exec($sql3);

//地点，
$sql4 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_place text(0) DEFAULT ''";
db_exec($sql4);
//发帖人（活动组织方）
$sql5 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_sp text(0) DEFAULT ''";
db_exec($sql5);
//是否收费
$sql6 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_type int(4) DEFAULT '0'";
db_exec($sql6);
//报名链接
$sql7 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_link text(0) DEFAULT ''";
db_exec($sql7);


//国家
$sql11 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_country text(0) DEFAULT ''";
db_exec($sql11);
//名字
$sql112 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_name  text(0)";
db_exec($sql112);
//邮箱
$sql114 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_email text(0) DEFAULT ''";
db_exec($sql114);
//微信号
$sql115 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_weixin text(0) DEFAULT ''";
db_exec($sql115);
//亚马逊主页
$sql116 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_ymx text(0) DEFAULT ''";
db_exec($sql116);
//Facebook主页
$sql117 = "ALTER TABLE {$tablepre}thread ADD COLUMN ax_facebook text(0) DEFAULT ''";
db_exec($sql117);

?>