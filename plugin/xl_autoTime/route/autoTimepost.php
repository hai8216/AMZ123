<?php



    $action = param(1);
    $taskid = param('taskid');

    if ($action == 'delete') {
        if ($user['gid'] == 1) {
            $tinfos = db_find_one("thread_autolog", ["id" => $taskid]);
            db_delete("thread_autolog", ["id" => $taskid]);
            db_delete("thread_autotime", ["tid" => $tinfos['tid']]);
            db_delete("post_autotime", ["tid" => $tinfos['tid']]);
            message(0, jump('操作成功', url('my-threadtask')));
        }
    } elseif ($action == 'edit') {

//        if ($method == 'GET') {
//            include _include(APP_PATH . 'plugin/xl_autoTime/view/htm/edit.htm');
//        } else {
//
//        }

    } elseif ($action == 'task') {

        //获取定时任务
        $map['status'] = 0;
        $map['time']=["<="=>time()];
        $task = db_find("thread_autolog", $map, [], 1, 100);

        if (!empty($task)) {
            foreach ($task as $v) {
                $_thread = db_find_one("thread", ["tid" => $v['tid']]);
                $_post = db_find_one("post", ["tid" => $v['tid']]);
                if (!$_thread['tid']) {
                    $autothread = db_find_one("thread_autotime", ["tid" => $v['tid']]);
                    $autothread['create_date'] = $v['time'];
                    $autothread['last_date'] = $v['time'];
                    $itid = db_insert("thread", $autothread);
                }
                if (!$_post['tid']) {
                    $autothread = db_find_one("post_autotime", ["tid" => $v['tid']]);
                    $autothread['create_date'] = $v['time'];
                    $ipid = db_insert("post", $autothread);
                }
                if($ipid){
                    db_update("thread_autolog",["id"=>$v['id']], ["status" => 1]);
                }
            }
        }else{
            echo 1;
        }

    }
