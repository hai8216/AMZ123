<?php exit;

if ($gid == 1) {
    //定时发帖，先删除
    if (param('autoTime')) {
        $allFiled = db_find_one("thread", ["tid" => $tid]);
        $allFiled_post = db_find_one("post", ["tid" => $tid]);
        if (db_insert("thread_autotime", $allFiled) && db_insert("post_autotime", $allFiled_post)) {
            db_delete("thread", ["tid" => $tid]);
            db_delete("post", ["tid" => $tid]);
            $logs = ["tid" => $tid, "time" => $checkTime, "status" => 0, "uid" => $user['uid']];
            db_insert("thread_autolog", $logs);
        }
    }
}
?>
