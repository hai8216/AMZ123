<?php

if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('favicon'), $result)) {
//            print_r($result);exit;
    $type = $result[2];
    $time = time();
    $filepath = APP_PATH . "upload/tools/" . date('Ymd', $time) . "/";
    $wwwpath = "upload/tools/" . date('Ymd', $time) . "/";
    if (!file_exists($filepath)) {
        //检查是否有该文件夹，如果没有就创建，并给予最高权限
        mkdir($filepath, 0777,true);
    }
    $new_file = time() . rand(100, 50000) . "." . $type;
    if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '',param('favicon'))))) {
        $icon = $wwwpath . $new_file;
    }
    $einsert['favicon'] = $icon;
}
//结构不同，只能将图片、标题单独存，其他的序列化
$einsert['title'] = param('title');

$einsert['mid'] = param('mid');
$einsert['order_id'] = param('order_id',0);
$insert['desc'] = param('desc');
$insert['links'] = param('links');
$insert['tags'] = param('tags');
$einsert['top'] = param('top');
$einsert['dateline'] = time();
$einsert['fileds'] = serialize($insert);
if(param('did')){
    db_update("tools_model_data",array('did'=>param('did')),$einsert);
}else{
    db_insert("tools_model_data",$einsert);
}

message(0, 'success');