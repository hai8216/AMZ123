<?php
//$mid = param('mid');
//$page = param('page');
//$value = db_find_one("tools_model", array('mid' => $mid));
//if ($value['mtype'] == 1) {
//    $vd = explode(',', $value['value']);
//    $threaadlist = db_find("thread", array('uid' => $vd), array('create_date' => '-1'), $page, 20);
//    $tcount = db_count("thread", array('uid' => $vd));
//} else {
//    $vd = explode(',', $value['value']);
//    $taglist = db_find("thread_tag", array('tag_name' => $vd), array(), 1, 1000);
//    foreach ($taglist as $v) {
//        $tagtids[] = $v['tag_id'];
//    }
//
//
//    $tidlist = db_find("thread_tag_keys", array('tags_id' => $tagtids), array('keys_id' => '-1'), 1, 1000);
//
//    foreach ($tidlist as $v) {
//        $tidsary[] = $v['tid'];
//    }
//    $threaadlist = db_find("thread", array('tid' => $tidsary), array('create_date' => '-1'), $page, 20);
//    $tcount = db_count("thread", array('tid' => $tidsary));
//}

header("Access-Control-Allow-Origin: *");//允许所有地址跨域请求
function saveImgicons($url, $type = '')
{
    if ($url == 'http://staticcss4.jobui.com/template_1/images/rank/blankLogo.png') {
        return "/upload/company/2020-11-20/5fb7997948320.png";
    } else {

        if (strpos($url, '51job') !== false) {
            if ($type == 'big') {
                $url = str_replace('_small', '', $url);
            }
        } else {
            if ($type == 'big') {
                $url = $url . "!b";
            }
        }
        $path = "./upload/company/" . date('Y-m-d');
        if (!file_exists($path)) mkdir($path, 0755, true);
        $extname = substr($url, strrpos($url, '.'));
        if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
            $ext = $extname;
        } else {
            $ext = '.jpg';
        }
        $name = $path . "/" . uniqid() . $ext;
        $r = null;
        if (function_exists("curl_init") && function_exists('curl_exec')) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            if (ini_get("safe_mode") == false && ini_get("open_basedir") == false) {
                curl_setopt($ch, CURLOPT_MAXREDIRS, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            }
            if (extension_loaded('zlib')) {
                curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
            }
            curl_setopt($ch, CURLOPT_TIMEOUT, 300);
            $r = curl_exec($ch);
            curl_close($ch);
        } elseif (ini_get("allow_url_fopen")) {
            if (function_exists('ini_set')) ini_set('default_socket_timeout', 300);
            $r = file_get_contents((extension_loaded('zlib') ? 'compress.zlib://' : '') . $url);
        }

        $fp = @fopen($name, "w");

        fwrite($fp, $r);

        fclose($fp);

        return $name;
    }
}

function saveImgicons_d($url, $type = '')
{
    $path = "./upload/company/" . date('Y-m-d');
    if (!file_exists($path)) mkdir($path, 0755, true);
    $extname = substr($url, strrpos($url, '.'));
    if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
        $ext = $extname;
    } else {
        $ext = '.jpg';
    }
    $name = $path . "/" . uniqid() . $ext;
    $r = null;
    if (function_exists("curl_init") && function_exists('curl_exec')) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if (ini_get("safe_mode") == false && ini_get("open_basedir") == false) {
            curl_setopt($ch, CURLOPT_MAXREDIRS, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        }
        if (extension_loaded('zlib')) {
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        $r = curl_exec($ch);
        curl_close($ch);
    } elseif (ini_get("allow_url_fopen")) {
        if (function_exists('ini_set')) ini_set('default_socket_timeout', 300);
        $r = file_get_contents((extension_loaded('zlib') ? 'compress.zlib://' : '') . $url);
    }

    $fp = @fopen($name, "w");

    fwrite($fp, $r);

    fclose($fp);

    return $name;

}

function setRandViewid()
{
    $viewsid = rand(1000000, 9999999);
    $view = db_find_one("company", array('viewid' => $viewsid));
    if ($view['viewid']) {
        setRandViewid();
    } else {
        return $viewsid;
    }
}

$action = param('act');
if ($action == 'ajaxMore') {
    $mid = param('mid');
    $page = param('page');
    $value = db_find_one("tools_model", array('mid' => $mid));
    if ($value['mtype'] == 1) {
        $vd = explode(',', $value['value']);
        $threaadlist = db_find("thread", array('uid' => $vd), array('create_date' => '-1'), $page, 20);
        $tcount = db_count("thread", array('uid' => $vd, 'is_deleted'=>0));
    } else {
        $vd = explode(',', $value['value']);
        $taglist = db_find("thread_tag", array('tag_name' => $vd), array(), 1, 1000);
        foreach ($taglist as $v) {
            $tagtids[] = $v['tag_id'];
        }


        $tidlist = db_find("thread_tag_keys", array('tags_id' => $tagtids), array('keys_id' => '-1'), 1, 1000);

        foreach ($tidlist as $v) {
            $tidsary[] = $v['tid'];
        }
        $threaadlist = db_find("thread", array('tid' => $tidsary), array('create_date' => '-1'), $page, 20);
        $tcount = db_count("thread", array('tid' => $tidsary, 'is_deleted' => 0));
    }
//    print_r($threaadlist);exit;
    include _include(APP_PATH . 'plugin/xl_tools_pages/view/ajaxnews.html');
} else {
    if ($_POST['companyname'] || $_POST['companyname2']) {
        //去重
        $companyname = $_POST['companyname'] ? $_POST['companyname'] : $_POST['companyname2'];
        $isHave = db_find_one("company", array('companyname' => $companyname));
        if ($isHave['id']) {
            echo json_encode(["result" => 1, "data" => "发布成功1"]);
            exit;
        }
        $pclog['companyname'] = $companyname;
        $pclog['desc'] = $_POST['desc'];
        $pclog['imglist'] = $_POST['imglist'];

        $pclog['logo'] = $_POST['logo'];

        if (strlen($_POST['logo']) > 10) {
            $pclog['save_logo'] = saveImgicons($_POST['logo'], 'logo');
        } else {
            $pclog['save_logo'] = "/upload/company/2020-11-20/5fb7997948320.png";
        }

        $pclog['views'] = 0;

        $pclog['viewid'] = setRandViewid();
        if (param('form') == 'chrome') {
            foreach ($_POST['imglist'] as $k => $v) {
                if ($k < 5) {
                    $newimg[] = saveImgicons_d($v, 'big');
                }
            }
        } else {
            $imglist = json_decode($_POST['imglist'], true);
            foreach ($imglist as $v) {
                $newimg[] = saveImgicons($v, 'big');
            }
        }

        $pclog['save_imglist'] = json_encode($newimg);
        include "plugin/xn_search/model/__pinyin.func.php";
        $pclog['E'] = strtoupper(substr(pinyin($companyname), 0, 1));
        $pclog['es'] = 1;
        $id = db_insert("company", $pclog);
        echo json_encode(["result" => 1, "data" => $id]);

    } else {
        echo json_encode(["result" => 0, "data" => "0"]);
    }
}
?>
