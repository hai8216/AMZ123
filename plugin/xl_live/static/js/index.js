
function getTab(tabid,dom){
    dom.html("<div class='loading'><div class='i'><i class='iconfont icon-Loading'></i></div></div>")
    $('.loading-btn').hide();
    $.get(tabApi,{tabid:tabid},function(res){
       if(res.code==200){
           livepage = res.cur_page
           dom.html(res.html)
           if(res.total_page>1 || res.cur_page<res.total_page){
               $('.loading-btn').show();
           }
           if(res.cur_page>=res.total_page){
               $('.loading-btn').hide();
           }
       }
    },'json')
}
function loadtabMore(tabid,page,dom,btndom){
    btndom.attr('disabled',true).html('加载中')
    $.get(tabApi,{tabid:tabid,page:page},function(res){
        if(res.code==200){
            livepage = res.cur_page;
            if(res.total_page>1 || res.cur_page<res.total_page){
                $('.loading-btn').show();
            }
            if(res.cur_page>=res.total_page){
                $('.loading-btn').hide();
            }
            dom.append(res.html)
            btndom.attr('disabled',false).html(' 加载更多 <i class="iconfont icon-youjiantou"></i>')
        }
    },'json')
}
