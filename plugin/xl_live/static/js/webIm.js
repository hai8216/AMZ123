var tim = TIM.create(options);
tim.setLogLevel(1);
let loginStatus = tim.login({userID: userid, userSig: userSig});
let onSdkReady = function (event) {
    //加入直播群
    let joinGroup = tim.joinGroup({groupID: liveroom, type: TIM.TYPES.GRP_AVCHATROOM});
    joinGroup.then(function (imResponse) {
        ready = 1;
    }).catch(function (imError) {
        //加入失败
        danMuError()
    });
};
tim.on(TIM.EVENT.SDK_READY, onSdkReady);
let onSdkNotReady = function (event) {
    tim.login({userID: userid, userSig: userSig});
};
tim.on(TIM.EVENT.KICKED_OUT, function(event) {
    kicked_out()
});
let onMessageReceived = function (event) {
    console.log(event.data[0])
    if(event.data[0].type=='TIMTextElem'){
        var html = '<div class="message talklist"  user-id="'+event.data[0].from+'" gorup-id="'+event.data[0].to+'"><span>'+event.data[0].nick+'：</span>'+event.data[0].payload.text+'</div>'
        $('.textarea').append(html)
        $('.imlist').scrollTop($('.imlist')[0].scrollHeight);
    }
};
tim.on(TIM.EVENT.MESSAGE_RECEIVED, onMessageReceived);

function danMuError() {
    return '';
}
$('#sendmsg').keyup(function(e) {
    var jthis = $(this);
    if((e.which == 13 || e.which == 10)) {

        let pushmessage = $('#sendmsg').val()
        pushmessage = pushmessage.replace(/\s*/g,"")
        if(pushmessage=='' || pushmessage.length<2){
            errorTips('发送内容不能为空或字数太少')
            return false;
        } else{
            $('#send').click()
        }

    }
});
$('#send').click(function () {
    if (uid>0) {
        let pushmessage = $('#sendmsg').val()
        pushmessage = pushmessage.replace(/\s*/g,"")
        if(pushmessage=='' || pushmessage.length<2){
            errorTips('发送内容不能为空或字数太少')
            return false;
        }
        let groupMessageoption = tim.createTextMessage({
            to: liveroom,
            conversationType: TIM.TYPES.CONV_GROUP,
            payload: {
                text: $('#sendmsg').val()
            },
        });
        $('#sendmsg').val('')
        let groupMessage = tim.sendMessage(groupMessageoption);
        groupMessage.then(function (imResponse) {
            console.log(imResponse)
            var html = '<div class="message talklist"  gorup-id="'+imResponse.data.message.to+'" user-id="'+imResponse.data.message.from+'"><span>'+imResponse.data.message.nick+'：</span>'+imResponse.data.message.payload.text+'</div>'
            $('.textarea').append(html)
            $('.imlist').scrollTop($('.imlist')[0].scrollHeight);
            deftips()
        }).catch(function (imError) {
           if(imError.code=='10017'){
               errorTips('你已经被禁言')
           }
        })
    }
})
function errorTips(msg){
    $('.sendbox .tips').html(msg).css({color:'red'})
}
function deftips(){
    $('.sendbox .tips').html('发表言论时请文明用语').css({color:'#999'})
}
function kicked_out(){
    $('#status1').remove()
    $('#status2').show()
}