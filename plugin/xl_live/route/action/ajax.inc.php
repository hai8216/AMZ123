<?php
$mod = param(2);

if ($mod == 'tab') {
    ob_start();
    $tabid = param('tabid', 0);
    $page = param('page', 1);
    if ($tabid) {
        $live_cate_list = db_find("live_room", ["live_channel_id" => $tabid], ["live_status" => '-1']);
        $map["live_channel_id"] =  $tabid;
    } else {
        $live_cate_list = db_find("live_room", [], ["live_status" => '-1']);
        $map = [];
    }
    include _include(APP_PATH . 'plugin/xl_live/template/action/ajax_livelist.htm');
    $html = ob_get_clean();
    $jsondata['code'] = 200;
    $jsondata['cur_page'] = $page;
    $jsondata['total_page'] = ceil(db_count("live_room",$map)/8);
    $jsondata['html'] = $html;
    echo json_encode($jsondata);
    exit;
} elseif ($mod == 'callback') {
//回调通知
    $type = param('push');
    $notice_key = '5f96a91ae46b659fe9422e18595fad49';
    $input = file_get_contents("php://input");
    if ($input && $data = json_decode($input, true)) {
        //完成回调鉴权
        if (md5($notice_key . $data['t']) == $data['sign']) {
            switch ($type) {

                case '1':
                    list($a, $b, $live_id) = explode("_", $data['channel_id']);
                    db_update("live_room", ["live_id" => $live_id], ["live_status" => 3]);
                    break;

                case '2':
                    list($a, $b, $live_id) = explode("_", $data['channel_id']);
                    db_update("live_room", ["live_id" => $live_id], ["live_status" => 1]);
                    break;

                case '3':
                    list($a, $b, $live_id) = explode("_", $data['channel_id']);
                    db_update("live_room", ["live_id" => $live_id], ["live_video_url" => $data['video_url']]);
                    break;
                default:
                    break;
            }
        }
    }
} elseif ($mod == 'ban') {
    include_once APP_PATH . "plugin/xl_live/common/tim.class.php";
    $tlgroup = new Tlgroup();
    if ($method == 'GET') {
        $gid = base64_decode(param('gid'));
        $liveuser = param('live_user_id');
        $user['bantime'] = $tlgroup->getBanlist($liveuser, $gid);
        list($a, $b, $juid) = explode("_", $liveuser);
        $user['name'] = user_read($juid)['username'];
        include _include(APP_PATH . 'plugin/xl_live/template/action/ajax_ban.htm');
    } else {
        $jj = param('jj');
        $gid = param('gid');
        $liveuser = param('userid');
        if (!$liveuser || $gid || $jj) {
            message(1, 'error');
        }
        $tlgroup->banuser($liveuser, $gid, $jj);
        message(0, 'success');
    }
} elseif ($mod == 'jubao') {
    if ($method == 'GET') {
        $live_id = param('live_id');
        include _include(APP_PATH . 'plugin/xl_live/template/action/ajax_jubao.htm');
    } else {
        if (!$user['uid']) {
            message(1, '请登录后再举报');
            exit;
        }
        $live_id = param('live_id');
        $desc = param('desc');
        $insert['desc'] = $desc;
        $insert['live_id'] = $live_id;
        $insert['uid'] = $user['uid'];
        $insert['time'] = time();
        if (!$desc) {
            message(1, '请输入举报理由');
            exit;
        }
        db_insert("live_jubao", $insert);
        message(0, 'success');
    }
}