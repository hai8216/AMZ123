<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') and exit('Access Denied.');
$action = param(3, 'livelist');
if ($action == 'livelist') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $keywords = param('keywords');
    $map = [];
    if ($keywords) {
        $map['name'] = array('LIKE' => $keywords);
        $arrlist = db_find("live_room", $map, array(), $page, $pagesize);
    } else {
        $arrlist = db_find("live_room", array(), array(), $page, $pagesize);
    }
    $c = db_count("live_room", $map);
    $pagination = pagination(url("plugin-setting-xl_live-livelist-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_live/admin/livelist.html');
} elseif ($action == 'channel') {
//    $f =  file_get_contents(APP_PATH . 'plugin/1.json');
//    $a = json_decode($f,true);
//    foreach($a as $k =>$v){
//        if($v['_T']=='LogPlayerKill'){
//            print_r($v['killer']['name']);
//            print_r("->".$v['victim']['name']);
//            print_r($v);
//            echo "<br />";
//        }
//    }
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $keywords = param('keywords');
    $map = [];
    if ($keywords) {
        $map['name'] = array('LIKE' => $keywords);
        $arrlist = db_find("live_channel", $map, array(), $page, $pagesize);
    } else {
        $arrlist = db_find("live_channel", array(), array(), $page, $pagesize);
    }
    $c = db_count("live_channel", $map);
    $pagination = pagination(url("plugin-setting-xl_live-channel-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_live/admin/channellist.html');
} elseif ($action == 'createlive') {
    if ($method == 'GET') {
        if (param('live_id')) {
            $live_id = param('live_id');
            $input['live_id'] = form_text("live_id", $live_id);
            $val = db_find_one("live_room", ["live_id" => $live_id]);
        }
        $input['live_name'] = form_text("live_name", $val['live_name']);
        $channel = db_find("live_channel",[],[],1,50);
        foreach($channel as $v){
            $channel_ary[$v['cid']]=$v['channel_name'];
        }
        $input['live_channel_id'] = form_select("live_channel_id",$channel_ary, $val['live_channel_id']);
        $input['live_startime'] = form_text("live_startime", $val['live_startime'] ? date('Y-m-d H:i', $val['live_startime']) : date('Y-m-d H:i', time()));
        $input['desc'] = form_textarea("desc", $val['desc'], '', '150');
        $input['seo_title'] = form_text("seo_title", $val['seo_title']);
        $input['seo_desc'] = form_text("seo_desc", $val['seo_desc']);
        $input['seo_keywords'] = form_text("seo_keywords", $val['seo_keywords']);
        include _include(APP_PATH . 'plugin/xl_live/admin/createlive.html');
    } else {
        $input['live_name'] = param("live_name");
        $input['live_startime'] = strtotime(param('live_startime'));
        $input['desc'] = param("desc");
        $input['live_channel_id'] = param("live_channel_id");
        $input['seo_title'] = param("seo_title");
        $input['seo_desc'] = param("seo_desc");
        $input['seo_keywords'] = param("seo_keywords");
        $input['create_time'] = param("create_time");
//        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
//            $type = $res[2];
//            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
//            $ext = '.' . $type;
//            $name = uniqid() . $ext;
//            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
//                $input['live_cover'] = $path . $name;
//            }
//        }
        if (param('live_id')) {
            db_update("live_room", ["live_id" => param('live_id')], $input);
        } else {
            db_insert("live_room", $input);
        }

        message(0, 'ok');
    }
} elseif ($action == 'createchannel') {
    if ($method == 'GET') {
        if (param('cid')) {
            $cid = param('cid');
            $val = db_find_one("live_channel", ["cid" => $cid]);
        }
        $input['channel_name'] = form_text("channel_name", $val['channel_name']);
        $input['channel_desc'] = form_text("channel_desc", $val['channel_desc']);
        $input['seo_title'] = form_text("seo_title", $val['seo_title']);
        $input['seo_desc'] = form_text("seo_desc", $val['seo_desc']);
        $input['seo_keywords'] = form_text("seo_keywords", $val['seo_keywords']);
        include _include(APP_PATH . 'plugin/xl_live/admin/createchannel.html');
    } else {
        $input['channel_name'] = param("channel_name");
        $input['channel_desc'] = param("channel_desc");
        $input['seo_title'] = param("seo_title");
        $input['seo_desc'] = param("seo_desc");
        $input['seo_keywords'] = param("seo_keywords");
//        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
//            $type = $res[2];
//            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
//            $ext = '.' . $type;
//            $name = uniqid() . $ext;
//            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
//                $input['live_cover'] = $path . $name;
//            }
//        }
        if (param('cid')) {
            db_update("live_channel", ["cid" => param('cid')], $input);
        } else {
            db_insert("live_channel", $input);
        }

        message(0, 'ok');
    }
} elseif ($action == 'getRmtp') {
    include_once APP_PATH . "/plugin/xl_live/common/live.func.php";
    $live_id = "amz_live_". param('live_id'); //推流标识符
    $rmtpary = getPushUrl('', $live_id);
    include _include(APP_PATH . 'plugin/xl_live/admin/rmtp.html');
}elseif($action=='jubao'){
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("live_jubao", array(), array(), $page, $pagesize);
    $c = db_count("live_jubao");
    $pagination = pagination(url("plugin-setting-xl_live-jubao-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_live/admin/jubao.html');
}elseif($action=='tag'){
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("live_tag", array(), array(), $page, $pagesize);
    $c = db_count("live_tag");
    $pagination = pagination(url("plugin-setting-xl_live-tag-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_live/admin/tag.html');
}elseif ($action == 'createtag') {
    if ($method == 'GET') {
        if (param('tagid')) {
            $tagid = param('tagid');
            $val = db_find_one("live_tag", ["tagid" => $tagid]);
        }
        $input['tag_name'] = form_text("tag_name", $val['tag_name']);
        $input['seo_title'] = form_text("seo_title", $val['seo_title']);
        $input['seo_desc'] = form_text("seo_desc", $val['seo_desc']);
        $input['seo_keywords'] = form_text("seo_keywords", $val['seo_keywords']);
        include _include(APP_PATH . 'plugin/xl_live/admin/createtag.html');
    } else {
        $input['tag_name'] = param("tag_name");
        $input['seo_title'] = param("seo_title");
        $input['seo_desc'] = param("seo_desc");
        $input['seo_keywords'] = param("seo_keywords");
        if (param('tagid')) {
            db_update("live_tag", ["tagid" => param('tagid')], $input);
        } else {
            db_insert("live_tag", $input);
        }

        message(0, 'ok');
    }
}