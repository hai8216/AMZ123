<?php

function getPushUrl($time = '', $streamName = '', $domain = 'livepush.amz123.com', $key = '3e397e7ba1d3192c8a630876f971f195')
{
    $streamName = $streamName ?: uniqid('amz');
    if (!$time) {
        $time = date("Y-m-d", strtotime("+1 day"));
    }
    if ($key && $time) {
        $txTime = strtoupper(base_convert(strtotime($time), 10, 16));
        $txSecret = md5($key . $streamName . $txTime);
        $ext_str = "?" . http_build_query(array(
                "txSecret" => $txSecret,
                "txTime" => $txTime
            ));
    }
    $url = $domain . "/live/";
    $param = $streamName . (isset($ext_str) ? $ext_str : "");
    $arr = [
        'rtmp' => "rtmp://" . $url . $param,
        'obs' => ['url' => "rtmp://" . $url, 'secret_key' => $param]
    ];
    return $arr;
}

function getPlayurl($live_id)
{
    $key = "3e397e7ba1d3192c8a630876f971f195";
    return ["flv"=>"https://live.amz123.com/live/amz_live_" . $live_id . ".flv","m3u8"=>"https://live.amz123.com/live/amz_live_" . $live_id . ".m3u8"];
}