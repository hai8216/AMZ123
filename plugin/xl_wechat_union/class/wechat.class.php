<?php

class Wechat_amz123
{
    private $appId;
    private $appSecret;

    public function __construct()
    {
        $this->appId = 'wx46da02edf61636f9';
        $this->appSecret = '971b645770a1f6e5ca69bba1608f1375';
    }

    public function createNonceStr($length = 16)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    public function setUsertoken($uid,$unid)
    {
        return $this->authcode($uid . "\t" . $unid . "\t" . time(),'ENCODE','amz123.com.mfyoyo.xcx.usertoken');

    }
    public function setMenu($myMenu) {
        $access_token = $this->getAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=$access_token";

        if (defined('JSON_UNESCAPED_UNICODE')) {
            $json = is_string($myMenu) ? $myMenu : json_encode($myMenu, JSON_UNESCAPED_UNICODE);
        } else {
            $json = is_string($myMenu) ? $myMenu : json_encode($myMenu);
        }

        $json = urldecode($json);
//        $res = self::post($url, $json);
        $res = $this->httpPost($url, $json);
        print_r($res);
    }
    public function getSession_key($code)
    {
        $url = "https://api.weixin.qq.com/sns/jscode2session?appid=wx792bf24bce390228&secret=38b4849b325b42110988b52c99fcae9a&js_code=" . $code . "&grant_type=authorization_code";
        $res = json_decode($this->httpGet($url));
        return $res;
    }

    public function random($length, $chars = '123456789')
    {
        $hash = '';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
        return $hash;
    }

    public
    function getTicket($postData)
    {
        $access_token = $this->getAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" . $access_token;
        $res = json_decode($this->httpPost($url, $postData));
        if($res->errcode=='40001'){
            $access_token = $this->getAccessToken_new();
            $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" . $access_token;
            $res = json_decode($this->httpPost($url, $postData));
        }
        return $res;
    }

    public function sendTpl($data)
    {
        $access_token = $this->getAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $access_token;
        $res = json_decode($this->httpPost($url, $data));
        return $res;
    }

    public
    function getUnid($openid, $user = '0')
    {
        $userinfo = $this->apigetUserinfo($openid);
        if ($user) {
            return $userinfo;
        } else {
            return $userinfo['unionid'];
        }

    }

    public function getToken(){
        return $this->getAccessToken();
    }
    public
    function sendMessage($openid, $message)
    {
        $msg = array(
            'ToUserName' => $openid,
            'FromUserName' => "amz123com",
            'CreateTime' => time(),
            'MsgType' => "text",
            'Content' => $message,
        );
        $xmldata = $this->xml_encode($msg);
        echo $xmldata;
        exit;
    }

    public
    function sendMessage_img($openid, $imgid)
    {
        $textTpl = "
                    <xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[image]]></MsgType>
                    <Image>
                    <MediaId><![CDATA[%s]]></MediaId>
                    </Image>
                    </xml>";
        $time = time();
        $resultStr = sprintf($textTpl, $openid,"amz123com", $time, $imgid);
        echo $resultStr;
        exit;
    }

    private
    static function xmlSafeStr($str)
    {
        return '<![CDATA[' . preg_replace("/[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f]/", '', $str) . ']]>';
    }

    private
    function data_to_xml($data)
    {
        $xml = '';
        foreach ($data as $key => $val) {
            is_numeric($key) && $key = "item id=\"$key\"";
            $xml .= "<$key>";
            $xml .= (is_array($val) || is_object($val)) ? self::data_to_xml($val) : self::xmlSafeStr($val);
            list($key,) = explode(' ', $key);
            $xml .= "</$key>";
        }
        return $xml;
    }

    private
    function xml_encode($data, $root = 'xml', $item = 'item', $attr = '', $id = 'id', $encoding = 'utf-8')
    {
        if (is_array($attr)) {
            $_attr = array();
            foreach ($attr as $key => $value) {
                $_attr[] = "{$key}=\"{$value}\"";
            }
            $attr = implode(' ', $_attr);
        }
        $attr = trim($attr);
        $attr = empty($attr) ? '' : " {$attr}";
        $xml = "<{$root}{$attr}>";
        $xml .= self::data_to_xml($data, $item, $id);
        $xml .= "</{$root}>";
        return $xml;
    }

    private
    function apigetUserinfo($openid)
    {
        $access_token = $this->getAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" . $access_token . "&openid=" . $openid . "&lang=zh_CN";
        $res = json_decode($this->httpGet($url), true);
        return $res;
    }

    public function getUser($openid)
    {
        return $this->apigetUserinfo($openid);
    }

    private
    function getAccessToken()
    {
        $access_token = cache_get('wechat_union_access_token');
        if (!$access_token || strlen($access_token)<10) {
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
            $res = json_decode($this->httpGet($url));
            if ($res->access_token) {
                $urlpath = $_SERVER['PHP_SELF'];
                db_insert("getac",["path"=>$urlpath,'time'=>date('Y-m-d H:i')]);
                $access_token = $res->access_token;
                cache_set('wechat_union_access_token', $access_token,3600);
            }
        }
        return $access_token;
    }

    private
    function getAccessToken_new()
    {
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
        $res = json_decode($this->httpGet($url));
        if ($res->access_token) {
            $urlpath = $_SERVER['PHP_SELF'];
            db_insert("getac",["path"=>$urlpath,'time'=>date('Y-m-d H:i')]);
            $access_token = $res->access_token;
            cache_set('wechat_union_access_token', $access_token,3600);
        }
        return $access_token;
    }

    private
    function httpGet($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 50);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        $res = curl_exec($curl);
        curl_close($curl);
        return $res;
    }

    private function httpPost($url, $postData)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        # curl_setopt( $ch, CURLOPT_HEADER, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $data = curl_exec($ch);
        if (!$data) {
            error_log(curl_error($ch));
        }
        curl_close($ch);
        return $data;
    }

    private
    function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0)
    {

        $ckey_length = 4;

        $key = md5($key ? $key : "amz123.com.mfyoyo");
        $keya = md5(substr($key, 0, 16));
        $keyb = md5(substr($key, 16, 16));
        $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';

        $cryptkey = $keya . md5($keya . $keyc);
        $key_length = strlen($cryptkey);

        $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
        $string_length = strlen($string);

        $result = '';
        $box = range(0, 255);

        $rndkey = array();
        for ($i = 0; $i <= 255; $i++) {
            $rndkey[$i] = ord($cryptkey[$i % $key_length]);
        }

        for ($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }

        for ($a = $j = $i = 0; $i < $string_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
        }

        if ($operation == 'DECODE') {
            if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
                return substr($result, 26);
            } else {
                return '';
            }
        } else {
            return $keyc . str_replace('=', '', base64_encode($result));
        }

    }
}

?>