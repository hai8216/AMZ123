<?php

$action = param('1');
include APP_PATH . "plugin/xl_wechat_union/class/wechat.class.php";
$wechat = new Wechat_amz123();
if ($action == 'login') {
    if ($uid) {
        message(0, jump('无须重复登录', url('my')));
    }
    include _include(APP_PATH . 'plugin/xl_wechat_union/htm/login.htm');
} elseif ($action == 'login_ajax') {
    //登录的需要临时的token
    $session_id = md5(time() . rand(11111, 9999) . rand(11111, 9999));
    $scene = array('action' => 'login', 'randToken' => $session_id, 'uid' => 0, 'dateline' => time());
    $postData = array('expire_seconds' => '400', 'action_name' => 'QR_STR_SCENE', 'action_info' => array('scene' => array('scene_str' => serialize($scene))));
    $wechatData = $wechat->getTicket(json_encode($postData));
//    print_r($wechatData);exit;
    $ticket = $wechatData->ticket;
    if ($ticket) {
        //存入操作记录中，用于轮询
        db_insert("wechat_tmp_polling", $scene);
    }
    echo json_encode(array('ticket' => $ticket, 'session_id' => $session_id));
    exit;
}elseif ($action == 'api_login_ajax') {
    //登录的需要临时的token
    $session_id = md5(time() . rand(11111, 9999) . rand(11111, 9999));
    $scene = array('action' => 'login', 'randToken' => $session_id, 'uid' => 0, 'dateline' => time());
    $postData = array('expire_seconds' => '400', 'action_name' => 'QR_STR_SCENE', 'action_info' => array('scene' => array('scene_str' => serialize($scene))));
    $wechatData = $wechat->getTicket(json_encode($postData));
    $ticket = $wechatData->ticket;
    if ($ticket) {
        cache_set("wechat_tmp_polling_".$session_id,$scene,300);
    }
    echo json_encode(array('ticket' => $ticket, 'session_id' => $session_id));
    exit;
} elseif ($action == 'bind') {
    if (!$uid) {
        message(0, jump('请先登录', url('user-login')));
    }
    include _include(APP_PATH . 'plugin/xl_wechat_union/htm/bind.htm');
} elseif ($action == 'bind_ajax') {
    $session_id = session_id();
    $scene = array('action' => 'bind', 'randToken' => $session_id, 'uid' => $uid);
    $postData = array('expire_seconds' => '3600', 'action_name' => 'QR_STR_SCENE', 'action_info' => array('scene' => array('scene_str' => serialize($scene))));
    $wechatData = $wechat->getTicket(json_encode($postData));
//    print_r($wechatData);exit;
    $ticket = $wechatData->ticket;
    if ($ticket) {
        //存入操作记录中，用于轮询
        db_insert("wechat_tmp_polling", $scene);
    }
    echo json_encode(array('ticket' => $ticket, 'session_id' => $session_id));
    exit;
} elseif ($action == 'bind_polling') {
    //绑定没必要查session_id
    $randToken = param('session_id');
    $bind_state = db_find_one("wechat_bind", array('uid' => $uid));
    if ($bind_state['unid']) {
        db_delete("wechat_tmp_polling", array('randToken' => $randToken));
        message(0, '绑定成功');
    } else {
        message(1, '未绑定');
    }

} elseif ($action == 'login_polling') {
    $randToken = param('randToken');
    $bind_state = db_find_one("wechat_tmp_polling", array('randToken' => $randToken));
    if ($bind_state['unid']) {
        $finduser = db_find_one("wechat_bind", array('unid' => $bind_state['unid']));
        if ($finduser['uid']) {
            db_delete("wechat_tmp_polling", array('randToken' => $randToken));
            $_user = db_find_one("user", array('uid' => $finduser['uid']));
            user_update($_user['uid'], array('login_ip' => $longip, 'login_date' => $time, 'logins+' => 1));
            $uid = $_user['uid'];
            $_SESSION['uid'] = $uid;
            user_token_set($_user['uid']);
            $bind_state = db_find_one("wechat_bind", array('uid' => $uid));
            $postdata = array(
                'touser' => $bind_state['openid'],
                'template_id' => 's85g88QZaZspid90KJ8mAOXWa-9LolT7dXUPGGLlwg4',
                'url' => 'http://www.amz123.com',
                'data' => array(
                    'first' => array(
                        'value' => '登录成功',
                        'color' => "#173177"
                    ),
                    'keyword1' => array(
                        'value' => $_user['username'],
                        'color' => "#173177"
                    ),
                    'keyword2' => array(
                        'value' => "于" . date('Y-m-d H:i') . "登录",
                        'color' => "#173177"
                    ),
                    'remark' => array(
                        'value' => '[  亚马逊卖家之路，从AMZ123开始  ]',
                        'color' => "#173177"
                    )

                ),
            );
            $wechat->sendTpl(json_encode($postdata));


            message(0, '登录成功');
        } else {
            message(2, $bind_state['randToken']);
        }
    } else {
        message(1, '未定义');
    }
} elseif ($action == 'setMenu') {
    $setMenu['button'] = array(
        array('name' => '跨境头条', 'type' => 'view', 'url' => 'http://www.amz123.com/forum-1.htm'),
        array('name' => '跨境导航', 'type' => 'view', 'url' => 'http://www.amz123.com/'),
        array('name' => '社群', 'type' => 'view', 'url' => 'http://www.amz123.com/thread-47749.htm'),
        array('name' => '测评黑名单', 'type' => 'view', 'url' => 'http://www.amz123.com/forum-3.htm')
    );
//    print_r($setMenu);exit;
    $wechat->setMenu($setMenu);
}elseif ($action == 'getToken') {
//
//   echo $wechat->sendMessage_img(1,'KpqdaArA6DFJPw8NYQFuq9td0EVe7goMdOhYdyrh2cE');
} elseif ($action == 'messageback') {
    $data = file_get_contents('php://input');
    $data = (array)simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);

    $openid = $data['FromUserName'];


    if ($data['Event'] == 'subscribe') {
        //发送消息
        $userinfo = $wechat->getUnid($openid,1);
        $unid = $userinfo['unionid'];
        $data['EventKey'] = str_replace("qrscene_", "", $data['EventKey']);
        $message = unserialize($data['EventKey']);
    } else {
        $userinfo = $wechat->getUnid($openid, 1);
        $unid = $userinfo['unionid'];
        $message = unserialize($data['EventKey']);
    }
//    if ($data['MsgType'] == 'text' && $data['Content'] == '社群') {
//        $wechat->sendMessage_img($openid, "KpqdaArA6DFJPw8NYQFuq9td0EVe7goMdOhYdyrh2cE");
//    }

    if ($message['action'] == 'bind') {
        //执行绑定
        $bind_state = db_find_one("wechat_bind", array('uid' => $message['uid']));
        //绑定到其他微信了
        $bind_statue = db_find_one("wechat_bind", array('openid' => $openid));
        if ($bind_statue['uid']) {
            $wechat->sendMessage($openid, '该微信绑定到了其他账号');
        }
        $binddata = array('uid' => $message['uid'], 'openid' => $openid, 'unid' => $unid, 'dateline' => time());

        if ($bind_state['id']) {
            $wechat->sendMessage($openid, '该账号已经绑定过了');
        } else {
            db_insert("wechat_bind", $binddata);
        }
        $users = db_find_one("user", array('uid' => $message['uid']));

        if ($users['xy_regfrom']) {
            $fuid = base64_decode($users['xy_regfrom']);
            $invite_setting = kv_get('invite');
            db_update('user', array('uid' => $fuid), array('credits+' => $invite_setting['invite_givenum']));
            $smg = $conf['gold_name'] . " +" . $invite_setting['invite_givenum'] . $conf['gold_unit'];
            $arr = array('uid' => $fuid, 'createtime' => $time, 'type' => 30, 'message' => $smg, 'uip' => $ip);
            db_create('score_info', $arr);
        }


        $postdata = array(
            'touser' => $openid,
            'template_id' => 'MyAISPGW9TP7gVwOnYeg--EBQ7jMfrIP3femNRoJouM',
            'url' => 'http://www.amz123.com',
            'data' => array(
                'first' => array(
                    'value' => '绑定微信成功',
                    'color' => "#173177"
                ),
                'keyword1' => array(
                    'value' => $users['username'],
                    'color' => "#173177"
                ),
                'keyword2' => array(
                    'value' => "于" . date('Y-m-d H:i') . "绑定成功",
                    'color' => "#173177"
                ),
                'remark' => array(
                    'value' => '[  亚马逊卖家之路，从AMZ123开始  ]',
                    'color' => "#173177"
                )

            ),
        );
        $wechat->sendTpl(json_encode($postdata));


    } elseif ($message['action'] == 'login') {
        $bind_state = db_find_one("wechat_bind", array('openid' => $openid));
        db_update('wechat_tmp_polling', array('randToken' => $message['randToken']), array('login_state' => $openid, 'unid' => $unid, 'userinfo' => serialize($userinfo)));
        if ($bind_state['uid']) {
            db_update('wechat_tmp_polling', array('randToken' => $message['randToken']), array('uid' => $bind_state['uid']));
        }else{
            $wechat->sendMessage($openid, ''.$userinfo['nickname'].',桔子等你好久了，快来看看如何玩转AMZ123。<a href="https:///www.amz123.com">点击这里。</a>');
        }
    }
//    $wechat->sendMessage_img($openid, "KpqdaArA6DFJPw8NYQFuq_qC95xkzjSpC8LBKfKpS_8");
//    $wechat->sendMessage($openid,$data['EventKey']);

} elseif ($action == 'register') {
    if ($uid) {
        message(0, jump('无须重复登录', url('my')));
    }
    $randToken = param(2);
    $bind_state = db_find_one("wechat_tmp_polling", array('randToken' => $randToken));
    if ($bind_state['login_state']) {
        $userinfo = unserialize($bind_state['userinfo']);
        $_SESSION['register_wechat_openid'] = $bind_state['login_state'];
        include _include(APP_PATH . 'plugin/xl_wechat_union/htm/register.htm');
    } else {
        message(0, '超时，请重试');
    }
}