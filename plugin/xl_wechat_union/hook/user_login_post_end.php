<?php exit;
$randToken = param('randToken');
if($randToken){
    $bind_state = db_find_one("wechat_tmp_polling", array('randToken' => $randToken));
    include APP_PATH . "plugin/xl_wechat_union/class/wechat.class.php";
    $wechat = new Wechat_amz123();
    $unid = $bind_state['unid'];
    $binddata = array('uid' => $uid, 'openid' => $bind_state['login_state'], 'unid' => $unid, 'dateline' => time());
    db_insert("wechat_bind", $binddata);
    $postdata = array(
        'touser' => $bind_state['login_state'],
        'template_id' => 'MyAISPGW9TP7gVwOnYeg--EBQ7jMfrIP3femNRoJouM',
        'url' => 'http://www.amz123.com',
        'data' => array(
            'first' => array(
                'value' => '绑定微信成功',
                'color' => "#173177"
            ),
            'keyword1' => array(
                'value' => $_user['username'],
                'color' => "#173177"
            ),
            'keyword2' => array(
                'value' => "于" . date('Y-m-d H:i') . "绑定成功",
                'color' => "#173177"
            ),
            'remark' => array(
                'value' => '【 亚马逊卖家之路，从AMZ123开始 】',
                'color' => "#173177"
            )

        ),
    );
    $wechat->sendTpl(json_encode($postdata));
    message(999, jump('登录并绑定成功',url('my')));
}