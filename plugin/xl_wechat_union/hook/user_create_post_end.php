<?php exit;
$randToken = param('randToken');
if($randToken){
    $bind_state = db_find_one("wechat_tmp_polling", array('randToken' => $randToken));
    include APP_PATH . "plugin/xl_wechat_union/class/wechat.class.php";
    $wechat = new Wechat_amz123();
    $unid = $bind_state['unid'];
    $binddata = array('uid' => $uid, 'openid' => $bind_state['login_state'], 'unid' => $unid, 'dateline' => time());
    db_insert("wechat_bind", $binddata);
    $juser = db_find_one("user", array('uid' => $uid));
    if ($juser['xy_regfrom']) {
        $fuid = base64_decode($juser['xy_regfrom']);
        $invite_setting = kv_get('invite');
        db_update('user', array('uid' => $fuid), array('credits+' => $invite_setting['invite_givenum']));
        $smg = $conf['gold_name'] . " +" . $invite_setting['invite_givenum'] . $conf['gold_unit'];
        $arr = array('uid' => $fuid, 'createtime' => $time, 'type' => 30, 'message' => $smg, 'uip' => $ip);
        db_create('score_info', $arr);
    }
    $postdata = array(
        'touser' => $bind_state['login_state'],
        'template_id' => 'MyAISPGW9TP7gVwOnYeg--EBQ7jMfrIP3femNRoJouM',
        'url' => 'http://www.amz123.com',
        'data' => array(
            'first' => array(
                'value' => '绑定微信成功',
                'color' => "#173177"
            ),
            'keyword1' => array(
                'value' => $_user['username'],
                'color' => "#173177"
            ),
            'keyword2' => array(
                'value' => "于" . date('Y-m-d H:i') . "绑定成功",
                'color' => "#173177"
            ),
            'remark' => array(
                'value' => '【 亚马逊卖家之路，从AMZ123开始 】',
                'color' => "#173177"
            )

        ),
    );
    $wechat->sendTpl(json_encode($postdata));
    message(9999, jump('注册并绑定成功',url('my')));
}