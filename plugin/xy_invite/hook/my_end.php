<?php exit;
elseif($action == 'invite'){
		$invite_setting = kv_get('invite');
		$input = array();
		$input['invite_on'] = form_radio_yes_no('invite_on', $invite_setting['invite_on']);
		$input['invite_givenum'] = form_text('invite_givenum', $invite_setting['invite_givenum']);
		$input['invite_info'] = form_textarea('invite_info', $invite_setting['invite_info'], '100%', 100);
		$header['title'] = lang('invite6').'-'.$conf['sitename'];
		$header['mobile_title'] = lang('invite6').'-'.$conf['sitename'];
		include _include(APP_PATH."plugin/xy_invite/view/htm/invate_myinvite.htm");
}elseif($action == 'invitelist'){
		$invite_setting = kv_get('invite');
		$header['title'] = lang('invite7').'-'.$conf['sitename'];
		$header['mobile_title'] = lang('invite7').'-'.$conf['sitename'];
		$page = param(2, 0);
		$page = !$page ?1:$page;
		$pagesize = 6;
		$listnum = db_count('user',array('xy_regfrom'=>base64_encode($uid)));
		$pagination = pagination(url("my-invitelist-{page}"), $listnum, $page, $pagesize);
		$infolist = db_find('user',array('xy_regfrom'=>base64_encode($uid)), array('uid'=>-1),$page, $pagesize);
		include _include(APP_PATH."plugin/xy_invite/view/htm/invate_list.htm");
		
}
?>