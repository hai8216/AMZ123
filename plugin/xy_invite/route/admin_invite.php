<?php
!defined('DEBUG') AND exit('Forbidden');
$action = param(1);
if($action == 'setting'){
	if($method == 'GET'){
		$invite_setting = kv_get('invite');
		$input = array();
		$input['invite_on'] = form_radio_yes_no('invite_on', $invite_setting['invite_on']);
		$input['invite_givenum'] = form_text('invite_givenum', $invite_setting['invite_givenum']);
		$input['invite_info'] = form_textarea('invite_info', urldecode($invite_setting['invite_info']), '100%', 100);
		$header['title'] = lang('invite2');
		$header['mobile_title'] =lang('invite2');

		include _include(APP_PATH."plugin/xy_invite/view/htm/admin_invite_setting.htm");
	}else{
		$invite_setting = array();
		$invite_setting['invite_on'] = param('invite_on', 0);
		$invite_setting['invite_givenum'] = param('invite_givenum', '', FALSE);
		$invite_setting['invite_info'] = urlencode(param('invite_info', '', FALSE));
		kv_set('invite',$invite_setting);
		message(0, lang('modify_successfully'));
	}
}
elseif($action == 'list'){
	$kwd = xn_urldecode($_GET['kwd']);
	$header['title'] = lang('invite1');
	$header['mobile_title'] =lang('invite1');

	$page = param(2, 0);
	$page = !$page ?1:$page;
	$pagesize = 20;
	if(empty($kwd)) {
		$listnum = db_count('user',array('xy_regfrom'=>array('!='=>'null')));
		$pagination = pagination(url("invite-list-{page}"), $listnum, $page, $pagesize);
		$infolist = db_find('user',array('xy_regfrom'=>array('!='=>'null')), array('uid'=>-1),$page, $pagesize);
	} else {
			$listnum = db_count('user',array('uid'=>$kwd));
			$pagination = pagination(url("invite-list-{page}")."?kwd=".xn_urlencode($kwd), $listnum, $page, $pagesize);
			$infolist = db_find('user',array('uid'=>$kwd,'xy_regfrom'=>array('!='=>'null')), array('uid'=>-1),$page, $pagesize);
	}
	include _include(APP_PATH."plugin/xy_invite/view/htm/admin_invitelist.htm");

}
?>