<?php 
// +----------------------------------------------------------------------
// | Author: 无双君 <1718905538@qq.com>
// +----------------------------------------------------------------------
// | QQ: 1718905538
// +----------------------------------------------------------------------

/*
	Xiuno BBS 4.0 插件实例：精华主题卸载
	admin/plugin-unstall-highlight.htm
*/

!defined('DEBUG') AND exit('Forbidden');

$tablepre = $db->tablepre;
$sql = "DROP TABLE {$tablepre}setting";
db_exec($sql);
?>