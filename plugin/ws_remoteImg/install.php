<?php 
// +----------------------------------------------------------------------
// | Author: 无双君 <1718905538@qq.com>
// +----------------------------------------------------------------------
// | QQ: 1718905538
// +----------------------------------------------------------------------


/*
	Xiuno BBS 4.0 插件实例：图片本地化
	admin/plugin-install-ws_remoteImg.htm
*/

!defined('DEBUG') AND exit('Forbidden');

# 开关表
$tablepre = $db->tablepre;
$sql = "CREATE TABLE IF NOT EXISTS {$tablepre}setting (
	`id`  int(10) NOT NULL AUTO_INCREMENT , 
	`name`  varchar(50) NOT NULL , 
	`value`  tinyint(2) NOT NULL DEFAULT 0, 
	PRIMARY KEY (`id`) 
)ENGINE=MyISAM DEFAULT CHARSET=utf8;
;";
db_exec($sql);
?>