<?php exit;

preg_match_all("/<img[^>]*src\s?=\s?[\'|\"]([^\'|\"]*)[\'|\"]/is", $arr['message'], $img);
$imgArr = $img[1];
if ($imgArr) {
    foreach ($imgArr as $k => $v) {
        if (strstr($v, 'http')) {
            if (strstr($v, 'img.amz123.com') === false) {
                $path = "./upload/thread_detail_img/" . date('Ymd');
                //本地保存
                if (!file_exists($path)) mkdir($path, 0755, true);
                $extname = substr($v, strrpos($v, '.'));
                if (in_array($extname, array('.jpg', '.png', '.jpeg', '.gif'))) {
                    $ext = $extname;
                } else {
                    $ext = '.jpg';
                }
                $name = uniqid() . $ext;
                saveTencent($v, $path . "/" . $name);
                //file_put_contents($path."/".$name, file_get_contents(isset($_get[url])?$_get[url]:$v));
                $message = str_replace($v, $path . "/" . $name, $arr['message']);
                $arr['message'] = $message;
            }
        }
    }
}