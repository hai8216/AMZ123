<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_index_admincp';
$action = param(3) ?: "navlist";
$navid = param(4);
$tpl = array(0 => '标准格子', 1 => '9列小图文', 2 => '12列小图文', 3 => '6列单图', 4 => '10列纯文');
if ($action == 'navlist') {

    if (!$navid) {
        $navlist = db_find('index_navlist',array(),array('order_id'=>'-1'),1,50);
//        print_r($navlist);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/navlist.htm');
    } else {
        $navdata = db_find_one('index_navlist', array('id' => $navid));
        $list = db_find('index_navlist_view', array('navid' => $navid),array('order_id'=>'-1'),1,500);

        $list = is_array($list) ? $list : array();
//        print_r($list);
        foreach($list as $k =>$v){
            if(strpos($v['favicon'],'tubiao') !==false){
                $list[$k]['favicon'] = "../".$v['favicon'];
            }
        }
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/navview.htm');
    }
} elseif ($action == 'addnav') {
    if ($method == 'GET') {
        $val = array();
        $val = db_find_one('index_navlist', array('id' => $navid));

        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addnav.htm');
    } else {
        $data['nav_title'] = param('nav_title');
        $data['nav_desc'] = param('nav_desc');
        $data['fup'] = param('fup');
        $data['nav_template'] = param('nav_template');
        $data['status'] =  param('status');
        $data['nav_desc_links'] = param('nav_desc_links');
        $data['left_radio'] = param('left_radio');
        $data['order_id'] = param('order_id');
        if (param('id')) {
            db_update('index_navlist', array('id' => param('id')), $data);
        } else {
            db_insert('index_navlist', $data);
        }
        $navlist = db_find('index_navlist',array('status'=>0),array('order_id'=>'-1'),1,50);
        kv_set('index_nav_cache', serialize($navlist));
        message(0, 'success');
    }
} elseif ($action == 'addnavview') {
    if ($method == 'GET') {
        $navid = param(4);
        $toid = param(5);
        $val = db_find_one('index_navlist_view', array('id' => $toid));
        $val = is_array($val) ? $val : array('title' => '', 'desc' => '', 'color' => '', 'tipso' => '', 'favicon' => '','links'=>'');
//        $navid = param(4)
        if(strpos($val['favicon'],'tubiao') !==false){

            $val['favicon'] = "../".$val['favicon'];

        }
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addnavview.htm');
    } else {
        $data['title'] = param('title');
        $data['desc'] = param('desc');
        $data['color'] = param('color')?:"444";
        $data['tipso'] = param('tipso');
        $data['favicon'] = param('favicon');
        $data['links'] = param('links');
        $data['smallicon'] = param('smallicon');

        $data['order_id'] = param('order_id');
        if (param('toid')) {
            db_update('index_navlist_view', array('id' => param('toid')), $data);
        } else {
            $data['navid'] = param('navid');
//            print_r($data);exit;
            db_insert('index_navlist_view', $data);
        }
        message(0, 'success');
    }
}elseif ($action == 'edfirst') {
    if ($method == 'GET') {
        $toid = param(4);
        $val = db_find_one('index_first', array('id' => $toid));
        $insert['first'] = unserialize($val['first']);
        $insert['sec'] = unserialize($val['sec']);
        $insert['thd'] = unserialize($val['thd']);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/edfirst.htm');
    }else{
        $data1['title'] = param('title');
        $data1['link'] = param('link');
        $data2['title2'] = param('title2');
        $data2['link2'] = param('link2');
        $data3['title3'] = param('title3');
        $data3['link3'] = param('link3');
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777,true);
            }

            $new_file = time() . rand(100, 50000) . "." . $type;

            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '',param('hidden_img_tpl'))))) {
                $icon = $wwwpath . $new_file;
            } else {
                $icon = param('hidden_img_tpl');
            }
        } else {
            $icon =  param('hidden_img_tpl');
        }



        $insert['first'] = serialize($data1);
        $insert['sec'] = serialize($data2);
        $insert['thd'] = serialize($data3);
        $insert['icon'] = $icon;
        $insert['order_id'] = param('orderid');
        if (param('toid')) {
            db_update('index_first', array('id' => param('toid')), $insert);
        } else {
            $data['id'] = param('id');
//            print_r($data);exit;
            db_insert('index_first', $insert);
        }
        message(0, 'success');
    }
}elseif($action == 'addfirst') {
    if ($method == 'GET') {
        $navid = param(4);
        $toid = param(5);
        $val = db_find_one('index_first', array('id' => $toid));
//        $val = is_array($val) ? $val : array('title' => '', 'desc' => '', 'color' => '', 'tipso' => '', 'favicon' => '','links'=>'');
//        $navid = param(4)
        if(strpos($val['favicon'],'tubiao') !==false){

            $val['favicon'] = "../".$val['favicon'];

        }
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addfirst.htm');
    } else {




        $data1['title'] = param('title');
        $data1['link'] = param('link');
        $data2['title2'] = param('title2');
        $data2['link2'] = param('link2');
        $data3['title3'] = param('title3');
        $data3['link3'] = param('link3');




        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/qrcode/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/qrcode/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777,true);
            }

            $new_file = time() . rand(100, 50000) . "." . $type;

            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '',param('hidden_img_tpl'))))) {
                $icon = $wwwpath . $new_file;
            } else {
                $icon = param('hidden_img_tpl');
            }
        } else {
            $icon =  param('hidden_img_tpl');
        }



        $insert['first'] = serialize($data1);
        $insert['sec'] = serialize($data2);
        $insert['thd'] = serialize($data3);
        $insert['icon'] = $icon;
        if (param('toid')) {
            db_update('index_first', array('id' => param('toid')), $insert);
        } else {
            $data['id'] = param('id');
//            print_r($data);exit;
            db_insert('index_first', $insert);
        }
        message(0, 'success');
    }
} elseif ($action == 'deleteview') {
    db_delete('index_navlist_view', array('id' => param('toid')));
    message(0, 'success');
}elseif($action=='navdelete'){
    db_delete('index_navlist', array('id' => param('id')));
    message(0, 'success');
}elseif($action=='cygj'){
//    $navdata = db_find_one('index_navlist', array('id' => 999));
    $navid=999;
    $list = db_find('index_navlist_view', array('navid' => 999),array('order_id'=>'-1'),1,500);
    $list = is_array($list) ? $list : array();
    foreach($list as $k =>$v){
        if(strpos($v['favicon'],'tubiao') !==false){
            $list[$k]['favicon'] = "../".$v['favicon'];
        }
    }
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/navview.htm');
}elseif($action=='cygjtext'){
   if ($method == 'GET') {
       $otext = kv_get('otext');
       $input['otext'] = form_text('otext',$otext['otext'] );
       $input['otextlinks'] = form_text('otextlinks', $otext['otextlinks'] );
       include _include(APP_PATH . 'plugin/' . $pluginname . '/view/navview_base.htm');
   }else{
       $otext['otext'] = param('otext');
       $otext['otextlinks'] = param('otextlinks');
       kv_set('otext',$otext);
       message('0',jump('ok',url('plugin-setting-xl_index_admincp-cygjtext')));
   }

}elseif($action=='first'){
//    $navdata = db_find_one('index_navlist', array('id' => 999));
    $list = db_find('index_first',array(),array('order_id'=>'-1'),1,500);
    $list = is_array($list) ? $list : array();
    foreach($list as $k=>$v){
        $list[$k]['data']['first'] = unserialize($v['first']);
        $list[$k]['data']['sec'] = unserialize($v['sec']);
        $list[$k]['data']['thd'] = unserialize($v['thd']);
    }

    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/first.htm');
}elseif($action=='zwpt'){
//    $navdata = db_find_one('index_navlist', array('id' => 999));
    $list = db_find('index_navlist_view', array('navid' => 998),array('order_id'=>'-1'),1,500);
    $list = is_array($list) ? $list : array();
    foreach($list as &$v){
        if(strpos($v['favicon'],'tubiao') !==false){
            $v['favicon'] = "../".$v['favicon'];
        }
    }
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/navview.htm');
}
?>