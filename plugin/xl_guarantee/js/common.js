$(function () {
    $('body').on('click', '.jieshoujiaoyi', function () {
        var index = layer.load(0, {shade: 0.2});
        $.get($(this).attr('data-url'), function (res) {
            layer.closeAll();
            layer.open({
                type: 1,
                skin: 'layui-layer-rim', //加上边框
                area: ['420px', '300px'], //宽高
                content: res
            });
        })
    })
    $('body').on('click', '.sub_button', function () {
    //$(".sub_button").click(function () {
        $.xpost($(this).attr('data-url'), function (code,message) {
                if (code == 0) {
                    console.log(message);
                    //layer.alert(message, {icon: 1});
                    layer.msg(message, {icon: 1});
                    window.location.reload();
                } else {
                    layer.alert(message, {icon: 1});
                    //layer.msg(res.message, {icon: 2});
                }
            }
        )
    })

    $("#touser").blur(function () {
        var name = $("#touser").val()
        $.get(xn.url("guaranteeajax-searchuser"), {name: name}, function (res) {
            if (res == '0') {
                $('#touser_info').html('<i class="iconfont icon-jinggao"></i> 【' + name + '】 不存在');
            } else {
                $('#touser_info').html('<i class="iconfont icon-ok" style="color:#07c160"></i>');
            }
        })
    })
    /* 发起交易检测 */
    $("#myphone").blur(function () {
        var myphone = $("#myphone").val()
        if (myphone) {
            $('#myphone_info').html('<i class="iconfont icon-ok" style="color:#07c160"></i>');
        } else {
            $('#myphone_info').html('<i class="iconfont icon-jinggao"> 必填</i>');
        }
    })

    $("#myqq").blur(function () {
        var myqq = $("#myqq").val()
        if (myqq) {
            $('.mywechatqq').html('<i class="iconfont icon-ok" style="color:#07c160"></i>');
        } else {
            var mywechat = $("#mywechat").val()
            var myqq = $("#myqq").val()

            if (!mywechat && !myqq) {
                $('.mywechatqq').html('<i class="iconfont icon-jinggao"> QQ和微信二者选其一</i>');
            }
        }
    })

    $("#mywechat").blur(function () {
        var mywechat = $("#mywechat").val()
        if (mywechat) {
            $('.mywechatqq').html('<i class="iconfont icon-ok" style="color:#07c160"></i>');
        } else {
            var mywechat = $("#mywechat").val()
            var myqq = $("#myqq").val()
            if (!mywechat && !myqq) {
                $('.mywechatqq').html('<i class="iconfont icon-jinggao"> QQ和微信二者选其一</i>');
            }
        }
    })
    $(".radio-box").click(function () {
        if ($("input[name='user_type']:checked").val()) {
            $('#user_type').html('<i class="iconfont icon-ok" style="color:#07c160"></i>');
        }
    })


    $("#order_price").blur(function () {
        var order_price = $("#order_price").val()
        if (!isNaN(order_price)) {
            $('#order_price_info').html('<i class="iconfont icon-ok" style="color:#07c160"></i>');
        } else {
            $('#order_price_info').html('<i class="iconfont icon-jinggao"> 必填或格式不正确，请输入正确的金额</i>');
        }
    })

})

function get_list(type, iddom) {
    var url = xn.url("guaranteeajax-list-" + type);
    $.get(url, function (res) {
        $(iddom).html(res)
    })
}