<?php

$action = param(3, 'base');
if ($action == 'base') {

    if ($method == 'GET') {
        $kvget = kv_get('guarantee');
        $input['feilv'] = form_text('feilv', $kvget['feilv'], '300px', '百分比，例如30%填入30即可');
        $input['days'] = form_text('days', $kvget['days'], '300px', '天，订单买家确认完成后多少天结算给卖家');
        include _include(APP_PATH . 'plugin/xl_guarantee/admin/base.htm');
    } else {
        $feilv = param('feilv');
        $days = param('days');
        kv_set('guarantee', array('feilv' => $feilv,'days'=>$days));
        message(0,'设置成功');
    }
}