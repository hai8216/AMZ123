<?php
user_login_check();

include _include(APP_PATH . 'plugin/xl_guarantee/common.php');
include _include(APP_PATH . 'plugin/xl_guarantee/core/core.php');
$action = param('1', 'list');
if ($action == 'list') {
    $type = param(2, 0);
    $page = param(3, 1);
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;
    $list['my'] = db_find("guarantee_list", array('uid' => $uid), array('dateline1' => '-1'), $start, $pagesize);
    $list['tomy'] = db_find("guarantee_list", array('touid' => $uid), array('dateline1' => '-1'), $start, $pagesize);
    $alllist = array_merge($list['my'], $list['tomy']);
    $count1 = db_count("guarantee_list", array('uid' => $uid));
    $count2 = db_count("guarantee_list", array('touid' => $uid));
    $pcount = $count1 + $count2;
    include _include(APP_PATH . 'plugin/xl_guarantee/view/ajax/list.html');
} elseif ($action == 'searchuser') {
    $user = db_count("user", array('username' => param('name')));
    if ($user > 0) {
        echo 1;
    } else {
        echo 0;
    }
} elseif ($action == 'jieshou') {
    $gid = param(2);
    if (!$gid) {
        echo '参数异常';
        exit;
    } else {
        if ($method == 'GET') {
            include _include(APP_PATH . 'plugin/xl_guarantee/view/ajax/jieshoujiaoyi.html');
        } else {
            $update['toqq'] = param('toqq');
            $update['tophone'] = param('tophone');
            $update['status'] = 2;
            db_update("guarantee_list",array('gid'=>param('2')),$update);
            writeLogs($gid,'接受交易 下一步: 等待买方付款。',$uid,$user['username']);
            message(0,'ok');
        }
    }
}elseif ($action == 'pay') {
    $gid = param('2');
    if (!$gid) {
        message(1,'必须选择待支付的交易');
        exit;
//        message(-1, jump('必须选择待支付的交易'));
//        exit;
    }
    $guarantee = db_find_one("guarantee_list", array('gid' => $gid, 'status' => 2));
    if ($guarantee['uid'] == $uid || $guarantee['touid'] == $uid) {
        $user = db_find_one("user", array('uid' => $uid));
        $paytotal = compare($guarantee['order_price_type'], $guarantee['order_price'], $user['rmbs'], $guarantee['feilv']);
        if (!$paytotal) {
            message(1,'余额不足，请充值');
            exit;
//            message(-1, jump('余额不足，请充值'));
//            exit;
        }
        $guarantee_update = array();
        $guarantee_update['status'] = 3;
        $guarantee_update['dateline2'] = time();
        $user_update = array();
        $user_update['rmbs-'] = $paytotal;
        //修改订单状态，完成用户余额变动
        db_update('guarantee_list', array('gid' => $gid), $guarantee_update);
        db_update('user', array('uid' => $uid), $user_update);
        db_update("guarantee_member",['uid'=>$uid],['all_price+'=>$paytotal]);
        writeLogs($gid,'支付成功 下一步: 等待买方确认完成交易。',$uid,$user['username']);
        //message(0, jump('支付成功'));
       message(0, '支付成功');
        exit;
    } else {
        message(1,'没有可支付的交易');
        exit;
//        message(-1, jump('没有可支付的交易'));
//        exit;
    }
}elseif ($action=='confirm'){
    $gid = param('2');
    if (!$gid) {
        message(1,'必须选择待支付的交易');
        exit;
    }
    $guarantee = db_find_one("guarantee_list", array('gid' => $gid, 'status' => 3));
    if (($guarantee['uid'] == $uid && $guarantee['user_type']==1) ||($guarantee['touid'] == $uid && $guarantee['user_type']==2)) {

        /*********确认交易成功后等待结算***********/
//        $guarantee_update = array();
//        $guarantee_update['status'] = 4;
//        db_update('guarantee_list', array('gid' => $gid), $guarantee_update);
        /*********确认交易成功后等待结算 end***********/

        /*********确认交易成功后直接结算***********/
        writeLogs($gid,'确认完成交易 下一步: 等待系统结算。',$uid,$user['username']);
        $guarantee_update = array();
        $guarantee_update['status'] = 5;
        //$guarantee_update['dateline2'] = time();
        db_update('guarantee_list', array('gid' => $gid), $guarantee_update);
        $mai= showRmb($guarantee,'mai');
        $user_update = array();
        $user_update['rmbs+'] = $mai;
        if($guarantee['user_type']==1){
            db_update('user', array('uid' => $guarantee['touid']), $user_update);
            db_update("guarantee_member",['uid'=>$guarantee['touid']],['all_price+'=>$mai]);
        }else{
            db_update('user', array('uid' => $guarantee['uid']), $user_update);
            db_update("guarantee_member",['uid'=> $guarantee['uid']],['all_price+'=>$mai]);
        }
        writeLogs($gid,'结算成功，交易全部完成。');
        /*********确认交易后直接结算 end***********/

       message(0, '确认交易成功');
        exit;
    }else {
        message(1,'没有可确认完成的交易');
        exit;
    }
}elseif($action=='settle'){
    $kvget = kv_get('guarantee');
    $days =$kvget['days']?:1;
    $dateline = strtotime(date('Y-m-d'))-$days*3600*24;
    $list= db_find('guarantee_list', $cond = array('status'=>4,array('dateline2'=>array('<='=>$dateline))), '', $page = 1, $pagesize = 100000);
    foreach ($list as $val){
        $mai= showRmb($val,'mai');
        $user_update = array();
        $user_update['rmbs+'] = $mai;
        if($val['user_type']==1){
            db_update('user', array('uid' => $val['touid']), $user_update);
            db_update("guarantee_member",['uid'=>$val['touid']],['all_price+'=>$mai]);
        }else{
            db_update('user', array('uid' => $val['uid']), $user_update);
            db_update("guarantee_member",['uid'=> $val['uid']],['all_price+'=>$mai]);
        }
        writeLogs($gid,'结算成功，交易全部完成。');
    }
}