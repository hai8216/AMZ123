<?php
user_login_check();
include _include(APP_PATH . 'plugin/xl_guarantee/common.php');
include _include(APP_PATH . 'plugin/xl_guarantee/core/core.php');
$base_setting = kv_get("guarantee");
$action = param(1, 'index');
if ($action == 'index') {
    include _include(APP_PATH . 'plugin/xl_guarantee/view/index.html');
} elseif ($action == 'detail') {
    $gid = param(2);
    if (!$gid) {
        message(0, jump('该交易不存在', url('guarantee')));
    }
    $order_info = db_find_one("guarantee_list", array('gid' => $gid));
    if (!$order_info['gid']) {
        message(0, jump('该交易不存在', url('guarantee')));
    } else {
        $logs =  db_find("guarantee_logs", array('gid' => $gid),array('id' => '1'));
        include _include(APP_PATH . 'plugin/xl_guarantee/view/detail.html');
    }
//    elseif(time() - $order_info['dateline1']>60*60*24){
//        message(0,jump('该交易已过期',url('guarantee')));
//    }

} elseif ($action == 'new') {
    if ($method == 'GET') {
        include _include(APP_PATH . 'plugin/xl_guarantee/view/create.html');
    } else {
        $insert['uid'] = $uid;
        //表中写入交易发起人用户名
        $insert['username'] = $user['username'];
        $insert['myphone'] = param('myphone');
        $insert['myqq'] = param('myqq');
        $insert['mywechat'] = param('mywechat');
        $insert['user_type'] = param('user_type');
        $insert['touser'] = param('touser');
        $saerchuser = db_find_one("user", array('username' => $insert['touser']));
        $insert['touid'] = $saerchuser['uid'];
        $insert['order_price'] = param('order_price');
        $insert['order_price_type'] = param('order_price_type');
        $agreen_guize = param('agreen_guize');
        $agreen_hetong = param('agreen_hetong');
        $insert['content'] = param('content');
        $insert['dateline1'] = time();
        $insert['status'] = 0;
        //获取当前设置的交易手续费比例，并写入当前交易订单。
        $kvget = kv_get('guarantee');
        $insert['feilv'] = $kvget['feilv']?:0;
        if (!$insert['myphone']) {
            message(-1, jump('请填入我的手机'));
            exit;
        }
        if (!$insert['myqq'] && !$insert['mywechat']) {
            message(-1, jump('请填入我的QQ或微信'));
            exit;
        }
        if (!$insert['user_type']) {
            message(-1, jump('请选择买卖关系'));
            exit;
        }
        if (!$insert['touser']) {
            message(-1, jump('请填入交易对方'));
            exit;
        }
        if (!$insert['order_price']) {
            message(-1, jump('请填入交易金额'));
            exit;
        }
        if (!$insert['order_price_type']) {
            message(-1, jump('请选择中介费收取方'));
            exit;
        }

        if (!$agreen_guize) {
            message(-1, jump('你必须同意交易规则'));
            exit;
        }
        if (!$insert['content']) {
            message(-1, jump('你必须输入交易内容'));
            exit;
        }
        db_update("guarantee_member",['uid'=>$insert['uid']],['ing+'=>1]);
        db_update("guarantee_member",['uid'=>$insert['touid']],['ing+'=>1]);
        $gid = db_insert("guarantee_list", $insert);
        $log = '发起交易 下一步: 联系<a href="?user-'.$insert['touid'].'.htm" style="color: red">'.$insert['touser'].'</a>接受交易';
        writeLogs($gid,$log,$uid,$user['username']);
        message(0, jump('发布成功，等待对方接受交易', url('guarantee')));
    }
}