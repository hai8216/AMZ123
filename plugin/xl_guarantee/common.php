<?php

function guarantee_init()
{
    global $user;
    $guarantee_member = db_find_one("guarantee_member", array('uid' => $user['uid']));
    if (!$guarantee_member) {
        db_insert("guarantee_member", array('uid' => $user['uid']));
    }
}

function guarantee_user_init()
{
    global $user;
    $guarantee_member = db_find_one("guarantee_member", array('uid' => $user['uid']));
//    $guarantee_member['ing'] = number_format($guarantee_member['all_price']);
//    $guarantee_member['end'] = number_format($guarantee_member['all_price']);
//    $guarantee_member['all_price'] = number_format($guarantee_member['all_price'], 2);
    return $guarantee_member;
}

function getStatus($s, $v)
{
    global $user;
    if ($user['uid'] == $v['uid']) {
        if ($v['user_type'] == 1) {
            switch ($s) {
                case 0:
                    return "等待对方接受交易";
                    break;
                case 1:
                    return "交易被关闭";
                    break;
                case 2:
                    return "<a  data-url='" . url('guaranteeajax-pay-' . $v['gid']) . "' class='smallbtn sub_button' href='javascript:;'>立即支付</a>";
                    break;
                case 3:
                    return "<a  data-url='" . url('guaranteeajax-confirm-' . $v['gid']) . "' class='smallbtn sub_button' href='javascript:;'>确认完成</a>";
                    break;
                case 4:
                    return "等待中介结算";
                    break;
                case 5:
                    return "交易完成";
                    break;
            }
        } else {
            switch ($s) {
                case 0:
                    return "<a  data-url='" . url('guaranteeajax-jieshou-' . $v['gid']) . "' class='smallbtn jieshoujiaoyi' href='javascript:;'>接受交易</a>";
                    break;
                case 1:
                    return "交易被关闭";
                    break;
                case 2:
                    return "等待对方付款";
                    break;
                case 3:
                    return "等待对方确认交易完成";
                    break;
                case 4:
                    return "等待中介结算";
                    break;
                case 5:
                    return "交易完成";
                    break;
            }
        }
    }else{
        if ($v['user_type'] == 1) {
            switch ($s) {
                case 0:
                    return "<a  data-url='" . url('guaranteeajax-jieshou-' . $v['gid']) . "' class='smallbtn jieshoujiaoyi' href='javascript:;'>接受交易</a>";
                    break;
                case 1:
                    return "交易被关闭";
                    break;
                case 2:
                    return "等待对方付款";
                    break;
                case 3:
                    return "等待对方确认交易完成";
                    break;
                case 4:
                    return "等待中介结算";
                    break;
                case 5:
                    return "交易完成";
                    break;
            }
        }else{
            switch ($s) {
                case 0:
                    return "等待对方接受交易";
                    break;
                case 1:
                    return "交易被关闭";
                    break;
                case 2:
                    return "<a  data-url='" . url('guaranteeajax-pay-' . $v['gid']) . "' class='smallbtn sub_button' href='javascript:;'>立即支付</a>";
                    break;
                case 3:
                    return "<a  data-url='" . url('guaranteeajax-confirm-' . $v['gid']) . "' class='smallbtn sub_button' href='javascript:;'>确认完成</a>";
                    break;
                case 4:
                    return "等待中介结算";
                    break;
                case 5:
                    return "交易完成";
                    break;
            }
        }

    }

   /* if ($v['touid'] != $user['uid']) {
        switch ($s) {
            case 0:
                return "等待对方接受交易";
                break;
            case 1:
                return "交易被关闭";
                break;
            case 2:
                return "交易完成";
                break;
            case 3:
                return "对方已接受交易，等待付款";
                break;
        }
    } else {
        switch ($s) {
            case 0:
                return "<a  data-url='" . url('guaranteeajax-jieshou-' . $v['gid']) . "' class='smallbtn jieshoujiaoyi' href='javascript:;'>接受交易</a>";
                break;
            case 1:
                return "交易被关闭";
                break;
            case 2:
                return "交易完成";
                break;
            case 3:
                return "<a  data-url='" . url('guaranteeajax-pay-' . $v['gid']) . "' class='smallbtn jieshoujiaoyi' href='javascript:;'>立即支付</a>";
                // return "等待付款";
                break;
        }
    }*/
}

function getbuyfang($v)
{
//    global $user;
    //发起人是买方
    if ($v['user_type'] == 1) {
        return '<a href="' . url('user-' . $v['uid']) . '" target="_blank">' . $v['username'] . '</a>';
    }
    //发起人是卖方
    if ($v['user_type'] == 2) {
        return '<a href="' . url('user-' . $v['touid']) . '" target="_blank">' . $v['touser'] . '</a>';
    }
}

function getmaifang($v)
{
//    global $user;
    //发起人是买方
    if ($v['user_type'] == 1) {
        //买方是自己
        return '<a href="' . url('user-' . $v['touid']) . '" target="_blank">' . $v['touser'] . '</a>';
    }
    //发起人是卖方
    if ($v['user_type'] == 2) {
        return '<a href="' . url('user-' . $v['uid']) . '" target="_blank">' . $v['username'] . '</a>';
    }
}



function getuser_type($order_info, $uid)
{
    if ($order_info['user_type']==1) {
        if ($uid == $order_info['uid']) {
            return "买家";
        } else {
            return "卖家";
        }
    } else {
        if ($uid == $order_info['uid']) {
            return "卖家";
        } else {
            return "买家";
        }
    }

}

function getorder_price_type($type)
{
    switch ($type) {
        case 1:
            return "买家支付";
            break;
        case 2:
            return "卖家支付";
            break;
        case 3:
            return "双方平摊";
            break;
    }
}

function getMaiUser($v)
{
    if ($v['user_type'] == '1') {
        $data['maijia']['username'] = user_read($v['uid'])['username'];
        $data['maijia']['uid'] = $v['uid'];
        $data['maijia']['qq'] = $v['myqq'];
        $data['maijia']['wechat'] = $v['mywechat'];
        $data['maijia']['phone'] = $v['myphone'];
        $data['maijia']['real_name'] = db_find_one("member_real", array('uid' => $v['uid']));
        $data['maijia2']['uid'] = $v['touid'];
        $data['maijia2']['username'] = $v['touser'];
        $data['maijia2']['qq'] = $v['toqq'];
        $data['maijia2']['phone'] = $v['tophone'];
        $data['maijia2']['wechat'] = $v['towechat'];
        $data['maijia2']['real_name'] = db_find_one("member_real", array('uid' => $v['touid']));
    } else {
        $data['maijia2']['username'] = user_read($v['uid'])['username'];
        $data['maijia2']['uid'] = $v['uid'];
        $data['maijia2']['qq'] = $v['myqq'];
        $data['maijia2']['wechat'] = $v['mywechat'];
        $data['maijia2']['phone'] = $v['myphone'];
        $data['maijia2']['real_name'] = db_find_one("member_real", array('uid' => $v['uid']));
        $data['maijia']['uid'] = $v['touid'];
        $data['maijia']['username'] = $v['touser'];
        $data['maijia']['wechat'] = $v['towechat'];
        $data['maijia']['qq'] = $v['toqq'];
        $data['maijia']['phone'] = $v['tophone'];
        $data['maijia']['real_name'] = db_find_one("member_real", array('uid' => $v['touid']));
    }
    return $data;
}

/**
 * 计算买方和卖方实际支付或收到的钱
 * @param $order
 * @param string $role
 * @return string
 */
function showRmb($order, $role = 'buy')
{
    $tax_price = number_format($order['order_price'] * $order['feilv'] / 100, 2);
    $buy = $mai = 0;
    switch ($order['order_price_type']) {
        case 1:
            $buy = $order['order_price'] + $tax_price;
            $mai = $order['order_price'];
            break;
        case 2:
            $buy = $order['order_price'];
            $mai = $order['order_price'] - $tax_price;
            break;
        case 3:
            $buy = $order['order_price'] + $tax_price / 2;
            $mai = $order['order_price'] - $tax_price / 2;
            break;
        default:
            break;
    }
    if ($role == 'buy') {
        return number_format($buy, 2);
    } else {
        return number_format($mai, 2);
    }
}

/**
 * 比较判断当前用户金额是否充足
 * @param $type 中介费支付类型,1买家支付 2卖家支付 3双方平摊
 * @param $order_price 交易金额
 * @param $user_price  用户当前余额
 * @param $feilv  当前税率
 * @return bool
 */
function compare($type, $order_price, $user_price, $feilv)
{
    if ($order_price > $user_price) {
        return false;
    }
    $tax_price = number_format($order_price * $feilv / 100, 2);
    $paytotal = 0;
    switch ($type) {
        case 1:
            $paytotal = $order_price + $tax_price;
            break;
        case 2:
            $paytotal = $order_price;
            break;
        case 3:
            $paytotal = $order_price + $tax_price / 2;
            break;
        default:
            break;
    }
    if ($paytotal > 0 && $user_price >= $paytotal) {
        return number_format($paytotal, 2);
    }
    return false;
}

function writeLogs($gid,$log,$uid=0,$username='系统'){
    $insert_data = array();
    $insert_data['gid']= $gid;
    $insert_data['uid']= $uid;
    $insert_data['log']=$log;
    $insert_data['username']=$username;
    $insert_data['dateline'] = date('Y-m-d H:i:s');
    db_insert('guarantee_logs',$insert_data);
}