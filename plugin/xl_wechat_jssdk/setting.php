<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');

if($method == 'GET') {
    $kv = kv_get('wechatsetting');
    $input = array();
    $input['wechat_jssdk_smsradio'] = form_radio_yes_no('wechat_jssdk_smsradio', $kv['wechat_jssdk_smsradio']);

    include _include(APP_PATH.'plugin/xl_wechat_jssdk/setting.htm');

} else {
    $kv = array();
    $kv['wechat_jssdk_smsradio'] = 1;
    kv_set('wechatsetting', $kv);
    message(0, '修改成功');
}

?>