<?php
require APP_PATH . '/vendor/autoload.php';
require APP_PATH . '/model/es.class.php';
$pk = "it";
$urlex = "it";
$indexData = db_find_one("es_index", ['type' => $pk, 'status' => 1], ['id' => '-1']);
$indexStatus = db_find_one("es_status", ['type' => $pk]);
$htitle = '意大利站TOP 25W搜索词排名';
$header['title'] = "意大利站TOP 25W搜索词排名 - AMZ123跨境导航";
if (!$indexData['id'] || $indexStatus['close'] == '1') {
    $close = 1;
} else {
    $area = $indexData['index_name'];
    $urlarae = $pk . "topkeywords";
    $es = new es($area);
    $page = param(1, 1);
    $key = param('3');
    $pagesize = 500;
    if ($page > 20) {
        $page = 1;
    }
    $rank = param('rank', '', FALSE);
    $uprank = param('uprank', '', FALSE);
    $order = param('2', 1);
    $start = ($page - 1) * $pagesize;


//$c = 250000;
    function getUp($up)
    {
        if ($up > 0) {
            return "↓" . $up;
        } elseif ($up == '0') {
            return "-";
        } else {
            return "↑" . $up;
        }
    }


    if ($order == 1) {
        $orderby = "newRank";
    } elseif ($order == 2) {
        $orderby = "oldRank";
    } elseif ($order == 3) {
        $orderby = "up";
    }

    if ($order == 1) {
        $orderby = "newRank";
    } elseif ($order == 2) {
        $orderby = "oldRank";
    } elseif ($order == 3) {
        $orderby = "up";
    }
    if($uprank){
        $orderby="up";
    }
    if ($key) {
        $searchList = $es->search($key, $page, $orderby, $rank, $uprank);
        $downList = $es->search($key, $page, $orderby, $rank, $uprank,2000);
        foreach ($searchList['data'] as $k => $v) {
//            print_r($searchList['data']);exit;
            $searchList['data'][$k]['name'] = str_replace(urldecode($key), "<em>$key</em>", $v['name']);
            $searchList['data'][$k]['exportName'] = $v['name'];
        }
    } else {
        $searchList = $es->search('', $page, $orderby, $rank, $uprank);
        $downList = $es->search('', $page, $orderby, $rank, $uprank,2000);
    }
//print_r( $searchList );exit;
    if (param('do') == 'down' && $key) {

        require_once APP_PATH . '/plugin/xl_search_baidu/Classes/PHPExcel.php';
        $phpexcel = new PHPExcel();
        $phpexcel->getActiveSheet()->setCellValue('A1', '搜索词');
        $phpexcel->getActiveSheet()->setCellValue('B1', '本周排名');
        $phpexcel->getActiveSheet()->setCellValue('C1', '上周排名');
        $phpexcel->getActiveSheet()->setCellValue('D1', '排名涨幅');
        foreach ($downList['data'] as $k => $v) {
            $k = $k + 2;
            $phpexcel->getActiveSheet()->setCellValue('A' . $k, $v['name']);
            $phpexcel->getActiveSheet()->setCellValue('B' . $k, $v['newRank']);
            $phpexcel->getActiveSheet()->setCellValue('C' . $k, $v['oldRank']);
            $phpexcel->getActiveSheet()->setCellValue('D' . $k, $v['up']);
        }


        $obj_Writer = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
        $filename = "导出微信参与数据.xls";//文件名

//设置header

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/download");
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header("Content-Transfer-Encoding: binary");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Pragma: no-cache");
        $obj_Writer->save('php://output');//输出
        die();//种植执行
    }
    $pagination = pagination(url($pk."topkeywords-{page}-" . $order . "-" . $key . "", array('rank' => $rank, 'uprank' => $uprank)), $searchList['total']>10000?8500:$searchList['total'], $page, $pagesize);
}
include _include(APP_PATH . 'plugin/xl_top/view/itkeywordslist.htm');

//if($_GET['debug']){
//    $arrlist =  db_find("sellers",array(),array(),1,20000);
//    foreach($arrlist as $v){
//        $marchant = explode('s?merchant=',$v['link1']);
//        db_update("sellers",array('id'=>$v['id']),array('link2'=>$marchant[0]."gp/aag/main?seller=".$marchant[1]));
//    }
//
//
//}
