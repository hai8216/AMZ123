elseif($action == 'jz'){
	
	$_uid = param(2, 0);
	
	$method != 'POST' AND message(-1, 'Method error');
	
	empty($group['allowdeleteuser']) AND message(-1, lang('insufficient_delete_user_privilege'));
	
	$u = user_read($_uid);
	empty($u) AND message(-1, lang('user_not_exists_or_deleted'));
	
	$u['gid'] < 6 AND message(-1, lang('cant_delete_admin_group'));
	
	$r = db_update('user', array('uid'=>$_uid), array('gid'=>7));
	$r === FALSE AND message(-1, lang('delete_failed'));
	
	message(0, lang('delete_successfully'));
}elseif($action == 'jf'){
	
	$_uid = param(2, 0);
	
	$method != 'POST' AND message(-1, 'Method error');
	
	empty($group['allowdeleteuser']) AND message(-1, lang('insufficient_delete_user_privilege'));
	
	$u = user_read($_uid);
	empty($u) AND message(-1, lang('user_not_exists_or_deleted'));
	
	$u['gid'] < 6 AND message(-1, lang('cant_delete_admin_group'));
	
	$r = db_update('user', array('uid'=>$_uid), array('gid'=>101));
	$r === FALSE AND message(-1, lang('delete_failed'));
	
	message(0, lang('delete_successfully'));
}
