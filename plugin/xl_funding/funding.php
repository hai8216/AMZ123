<?php
include APP_PATH."/plugin/xl_funding/func.php";
$attribute = ["", '卖家', '服务商', "平台"];
$local = getLocal();
$number = getTouzilun();
foreach($number as $k =>$v){
    $numbers[$v['id']] = $v;
}
$a = param('a');
$t = param('t');
$ll = param('local');
$n = param('n');
$map = [];

if ($a) {
    $map['attribute'] = $a;
}
if ($t) {
    if ($t > 2009) {
        $tarea = GetYearRange($t);
        $map['dateline'] = array(">" => $tarea['s'], "<" => $tarea['e']);
    } else {
        $tarea = GetYearRange(2009);
        $map['dateline'] = array("<" => $tarea['e']);
    }
}
if ($ll) {
    $map['local'] = $ll;
}
if ($n) {
    $map['number'] = $n;
}
if (param('keywords')) {
    $keyword = search_keyword_safe(xn_urldecode(param('keywords')));
    $map['name'] = ["LIKE" => $keyword];
}else{
    $keyword = "";
}
$page = param(1, 1);
$pagesize = 50;
$start = ($page - 1) * $pagesize;
$datalist = db_find("funding", $map, ["dateline" => -1], $page, $pagesize);
$c = db_count("funding", $map);
$pagination = pagination(url("funding-{page}"), $c, $page, $pagesize);

function touzifang($tz)
{
    $ary = explode("\n", $tz);
    if (count($ary) > 5) {
        return implode('，', array_splice($ary, 0, 5)) . '等';
    }
    return implode('，', $ary);
}


function checkurl($a, $t, $ll, $n)
{
    $urls = ["a" => $a, "t" => $t, "local" => $ll, "n" => $n];
    foreach ($urls as $k => $v) {
        if (!$v) {
            unset($urls[$k]);
        }
    }
    $url = url('funding', $urls);
    return $url;
}

function GetYearRange($selectTime)
{

    $timeF = strtotime($selectTime . "-1-1");
    $year = (int)$selectTime + 1;
    $timeL = strtotime($year . "-1-1");
    $t['s'] = $timeF;
    $t['e'] = $timeL;
    return $t;

}
$header['title'] = "跨境电商投融资数据库-AMZ123跨境导航";
$header['keywords'] = "跨境电商融资,跨境电商投资,跨境电商估值,跨境电商并购";
$header['description'] = "AMZ123投资数据库，收录了中国、全球的跨境电商风险投资、IPO上市、收购合并事件，为您提供跨境电商领域最新项目、投资收购新闻、行业调研等服务。";
if(check_wap()){
    include _include(APP_PATH . 'plugin/xl_funding/view/funding_4g.html');
}else{
    include _include(APP_PATH . 'plugin/xl_funding/funding.html');
}
