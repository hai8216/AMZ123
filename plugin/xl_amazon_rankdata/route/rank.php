<?php

$cateList = db_find("amazon_bestseller_cate", ["type" => 1], ["cateName" => "-1"], 1, 50);
foreach ($cateList as $v) {
    $domain[$v['domain']] = $v['id'];
    $cateName[$v['id']] = $v['cateName'];
}
$curdomain = param(1);
$fup = $domain[$curdomain];
$header['title'] = '亚马逊美国站' . $cateName[$fup] . '类目TOP排行榜-AMZ123跨境导航';
$header['keywords'] = '类目排名,类目排行榜,类目BSR';
$header['description'] = '亚马逊' . $cateName[$fup] . ' BSR类目排名，根据亚马逊销售排行榜为数据来源，帮卖家详细展示本类目产品的大类排名，为卖家选品提供依据。';
$title = $cateName[$fup] . '类目TOP排行榜';

$price = param(2, 0);
$star = param(3, 0);
$replycount = param(4, 0);
$keywords = urldecode(param(5, ''));
$trank = param("trank");
$diys1 = param("diys1");
$diys2 = param("diys2");

$diyp1 = param("diyp1");
$diyp2 = param("diyp2");

$diyr1 = param("diyr1");
$diyr2 = param("diyr2");
$order = param("order", '1');
//获取最后一条时间
if ($diyp1 || $diyp2) {
    $map['price'] = array('>' => $diyp1, '<' => $diyp2);
    $diyprice = 999999999;
    $f_price['title'] = $diyp1 . " - " . $diyp2;
} else {
    if ($price) {
        $f_price = db_find_one("ama_filter", array('id' => $price));
        $map['price'] = array('>' => $f_price['end'], '<' => $f_price['star']);
    }
}


if ($diys1 || $diys2) {
    $map['star'] = array('>' => $diys1, '<' => $diys2);
    $f_star['title'] = $diys1 . " - " . $diys2;
} else {
    if ($star) {
        $f_star = db_find_one("ama_filter", array('id' => $star));
        $map['star'] = array('>' => $f_star['end'], '<' => $f_star['star']);
    }
}

if ($diyr1 || $diyr2) {
    $map['reply_count'] = array('>' => $diyr1, '<' => $diyr2);
    $f_replycount['title'] = $diyr1 . " - " . $diyr2;
} else {

    if ($replycount) {
        $f_replycount = db_find_one("ama_filter", array('id' => $replycount));
        $map['reply_count'] = array('>' => $f_replycount['end'], '<' => $f_replycount['star']);
    }
}
$page = param(6, 1);
$pagesize = 50;
$map['cateId'] = $fup ? $fup : 1;
//$map['title'] = [">"=>0];


//排序
switch ($order) {
    case 1:
        //1默认
        $orderby = array('rank' => '1');
        break;
    case 2:
        //价格倒叙
        $orderby = array('price' => '-1');
        break;
    case 3:
        //价格升叙
        $orderby = array('price' => '1');
        break;
    case 4:
        //评分倒叙
        $orderby = array('star' => '-1');
        break;
    case 5:
        //评分升叙
        $orderby = array('star' => '1');
        break;
    case 6:
        //评论数倒叙
        $orderby = array('reply_count' => '-1');
        break;
    case 7:
        //评论数倒叙
        $orderby = array('reply_count' => '1');
        break;

}


if ($frank = param('rank')) {
    $fmap = $map;
    $fmap['rank'] = ["<=" => $frank, ">" => 0];
    $fcount = db_count("amazon_bestseller_1012", $fmap);
    $curpage = ceil($fcount / $pagesize);
    $page = $curpage;
}
$map['rank'] = [">" => 0];
if ($rankqj = param('rankqj')) {
    $ranks = explode("-", $rankqj);
    $map['rank'] = [">=" => $ranks[0], "<=" => $ranks[1]];
}

$c = db_count("amazon_bestseller_1012", $map);
$orderby = $orderby?$orderby:array('rank' => '1');
$bigdata = db_find("amazon_bestseller_1012", $map, $orderby, $page, $pagesize);

$pagination = pagination(url("bsr-" . $curdomain . "-" . $price . "-" . $star . "-" . $replycount . "-" . $keywords . "-{page}",array('rank'=>$frank,'rankqj'=>$rankqj)), $c, $page, $pagesize);
//其他
$fiter1 = db_find("ama_filter", array('type' => 1));
$fiter2 = db_find("ama_filter", array('type' => 2));
$fiter3 = db_find("ama_filter", array('type' => 3));
$fiter4 = ["101-500", "500-1000", "1001-2000"];
if (check_wap()) {
    include _include(APP_PATH . 'plugin/xl_amazon_rankdata/view/list_5g.html');
} else {
    include _include(APP_PATH . 'plugin/xl_amazon_rankdata/view/list.html');
}

function showTime($v)
{
    if ($v['time']) {
        return str_replace(" 00:00:00", "", $v['time']);
    } else if ($v['time2']) {
        return str_replace(" 00:00:00", "", $v['time2']);
    } else if ($v['time3']) {
        return str_replace(" 00:00:00", "", $v['time3']);
    } else if ($v['time4']) {
        return str_replace(" 00:00:00", "", $v['time4']);
    }
}
