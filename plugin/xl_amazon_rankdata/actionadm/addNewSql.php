<?php

if ($method == 'GET') {
    include _include(APP_PATH . 'plugin/xl_amazon_rankdata/admin/' . $ac . '.htm');
} else {
       //先新增表
    $table = param('sqlname');
    $sql = "CREATE TABLE IF NOT EXISTS bbs_{$table} (
	`id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `reply_count` int(10) DEFAULT NULL,
  `allprice` decimal(8,2) DEFAULT NULL,
  `star` decimal(4,2) DEFAULT NULL,
  `img` varchar(90) DEFAULT NULL,
  `asin` char(20) DEFAULT NULL,
  `dateline` char(20) DEFAULT NULL,
  `fup` int(6) DEFAULT NULL,
  `fup2` int(6) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `reply_count` (`reply_count`,`allprice`,`star`,`fup`,`fup2`),
  KEY `reply_count_2` (`reply_count`),
  KEY `allprice` (`allprice`),
  KEY `star` (`star`),
  KEY `fup` (`fup`,`fup2`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
  db_exec($sql);
  db_update("ama_sql",array('cj'=>1),array('cj'=>0));
        $insert = array(
            'dateline'=>time(),
            'status'=>0,
            'sqlname'=>param('sqlname'),
            'cj'=>1,
        );
        db_insert("ama_sql",$insert);
        message(0,'ok');
}