<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') and exit('Access Denied.');
$pluginname = 'xl_index_admincp';
$action = param(3) ?: "navlist";
$navid = param(4);
$tpl = array(0 => '标准格子', 1 => '9列小图文', 2 => '12列小图文', 3 => '6列单图', 4 => '10列纯文', 5 => '8列小图文', 6 => '8列标准', 7 => '微信名片', 8 => '1图2链接', 9 => '首页尾部', 10 => '纯图3', 11 => '纯图4', 12 => '纯图5', 13 => '纯图6',14=>"6列按钮快");
if ($action == 'navlist') {

    if (!$navid) {
        $navlist = db_find('index_navlist', array(), array('order_id' => '-1'), 1, 50);
//        print_r($navlist);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/navlist.htm');
    } else {
        $navdata = db_find_one('index_navlist', array('id' => $navid));
        $list = db_find('index_navlist_view', array('navid' => $navid), array('order_id' => '-1'), 1, 500);

        $list = is_array($list) ? $list : array();
//        print_r($list);
        foreach ($list as $k => $v) {
            if (strpos($v['favicon'], 'tubiao') !== false) {
                $list[$k]['favicon'] = "../" . $v['favicon'];
            }
        }
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/navview.htm');
    }
} elseif ($action == 'addsixbox') {
    if ($method == 'GET') {
        $toid = param(4);
        $val = db_find_one('index_sixbox', array('id' => $toid));
        $val = is_array($val) ? $val : array('title' => '', 'desc' => '',  'favicon' => '', 'links' => '');
        if (strpos($val['favicon'], 'tubiao') !== false) {
            $val['favicon'] = "../" . $val['favicon'];
        }
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addsixbox.htm');
    } else {
        $data['title'] = param('title');
        $data['desc'] = param('desc');
        $data['favicon'] = param('favicon');
        $data['links'] = param('links');
        $data['order_id'] = param('order_id');
        $data['btntext'] = param('btntext');


        //将图片云化，现在全是BASE64
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $data['favicon'], $res)) {
            $type = $res[2];
            if ($data['favicon']) {
                $path = "./upload/index_icon/" . date('Ymd') . "/";
                $ext = '.' . $type;
                $name = uniqid() . $ext;
                if (saveTencentBase64(base64_decode(str_replace($res[1], '', $data['favicon'])), $path . $name)) {
                    $data['favicon'] = I($path . $name, 'icon52');
                }
            }
        }
        if (param('toid')) {
            db_update('index_sixbox', array('id' => param('toid')), $data);
        } else {
            db_insert('index_sixbox', $data);
        }
        message(0, 'success');
    }

} elseif ($action == 'sixbox') {

    $list = db_find('index_sixbox', [], array('order_id' => '-1'), 1, 500);

    $list = is_array($list) ? $list : array();
//        print_r($list);
    foreach ($list as $k => $v) {
        if (strpos($v['favicon'], 'tubiao') !== false) {
            $list[$k]['favicon'] = "../" . $v['favicon'];
        }
    }
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/sixboxlist.htm');

} elseif ($action == 'addnav') {
    if ($method == 'GET') {
        $val = array();
        $val = db_find_one('index_navlist', array('id' => $navid));

        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addnav.htm');
    } else {
        $data['nav_title'] = param('nav_title');
        $data['nav_desc'] = param('nav_desc');
        $data['fup'] = param('fup');
        $data['nav_template'] = param('nav_template');
        $data['status'] = param('status');
        $data['nav_desc_links'] = param('nav_desc_links');
        $data['left_radio'] = param('left_radio');
        $data['order_id'] = param('order_id');
        $data['show_title'] = param('show_title');

        if (param('id')) {
            db_update('index_navlist', array('id' => param('id')), $data);
        } else {
            db_insert('index_navlist', $data);
        }
        $navlist = db_find('index_navlist', array('status' => 0), array('order_id' => '-1'), 1, 50);
        kv_set('index_nav_cache', serialize($navlist));
        message(0, 'success');
    }
} elseif ($action == 'addnavview') {
    if ($method == 'GET') {
        $navid = param(4);
        $toid = param(5);
        $navdata = db_find_one('index_navlist', array('id' => $navid), array('order_id' => '-1'));
        $val = db_find_one('index_navlist_view', array('id' => $toid));
        $val = is_array($val) ? $val : array('title' => '', 'desc' => '', 'color' => '', 'tipso' => '', 'favicon' => '', 'links' => '');
//        $navid = param(4)
        if (strpos($val['favicon'], 'tubiao') !== false) {

            $val['favicon'] = "../" . $val['favicon'];

        }
        if ($navdata['nav_template'] == '8') {
            include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addnavview8.htm');
        } else {
            include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addnavview.htm');
        }

    } else {
        $data['title'] = param('title');
        $data['desc'] = param('desc');
        $data['color'] = param('color') ?: "444";
        $data['tipso'] = param('tipso');
        $data['favicon'] = param('favicon');
        $data['favicon2'] = param('favicon2', '');
        $data['links'] = param('links');
        $data['smallicon'] = param('smallicon');
        $data['order_id'] = param('order_id');
        $data['title2'] = param('title2');
        $data['links2'] = param('links2');
        $data['title3'] = param('title3');
        $data['links3'] = param('links3');
        $data['btntext'] = param('btntext');
        //将图片云化，现在全是BASE64
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $data['favicon'], $res)) {
            $type = $res[2];
            if ($data['favicon']) {
                $path = "./upload/index_icon/" . date('Ymd') . "/";
                $ext = '.' . $type;
                $name = uniqid() . $ext;
                if (saveTencentBase64(base64_decode(str_replace($res[1], '', $data['favicon'])), $path . $name)) {
                    $data['favicon'] = I($path . $name, 'icon52');
                }
            }
        }
        if (param('toid')) {
            db_update('index_navlist_view', array('id' => param('toid')), $data);
        } else {
            $data['navid'] = param('navid');
//            print_r($data);exit;
            db_insert('index_navlist_view', $data);
        }
        message(0, 'success');
    }
} elseif ($action == 'edfirst') {
    if ($method == 'GET') {
        $toid = param(4);
        $val = db_find_one('index_first', array('id' => $toid));
        $insert['first'] = unserialize($val['first']);
        $insert['sec'] = unserialize($val['sec']);
        $insert['thd'] = unserialize($val['thd']);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/edfirst.htm');
    } else {
        $data1['title'] = param('title');
        $data1['link'] = param('link');
        $data2['title2'] = param('title2');
        $data2['link2'] = param('link2');
        $data3['title3'] = param('title3');
        $data3['link3'] = param('link3');
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/index_icon/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $icon = I($path . $name, 'icon96');
            }
        }
        $insert['first'] = serialize($data1);
        $insert['sec'] = serialize($data2);
        $insert['thd'] = serialize($data3);
        $insert['icon'] = $icon;
        $insert['order_id'] = param('orderid');
        if (param('toid')) {
            db_update('index_first', array('id' => param('toid')), $insert);
        } else {
            $data['id'] = param('id');
//            print_r($data);exit;
            db_insert('index_first', $insert);
        }
        message(0, 'success');
    }
} elseif ($action == 'addfirst') {
    if ($method == 'GET') {

        $navid = param(4);

        $toid = param(5);
        $val = db_find_one('index_first', array('id' => $toid));
//        $val = is_array($val) ? $val : array('title' => '', 'desc' => '', 'color' => '', 'tipso' => '', 'favicon' => '','links'=>'');
//        $navid = param(4)
        if (strpos($val['favicon'], 'tubiao') !== false) {

            $val['favicon'] = "../" . $val['favicon'];

        }
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addfirst.htm');
    } else {


        $data1['title'] = param('title');
        $data1['link'] = param('link');
        $data2['title2'] = param('title2');
        $data2['link2'] = param('link2');
        $data3['title3'] = param('title3');
        $data3['link3'] = param('link3');


        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
//           $type = $res[2];
            $path = "./upload/index_icon/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $icon = I($path . $name, 'icon96');
            }
        }

        $insert['first'] = serialize($data1);
        $insert['sec'] = serialize($data2);
        $insert['thd'] = serialize($data3);
        $insert['icon'] = $icon;
        if (param('toid')) {
            db_update('index_first', array('id' => param('toid')), $insert);
        } else {
            $data['id'] = param('id');
//            print_r($data);exit;
            db_insert('index_first', $insert);
        }
        message(0, 'success');
    }
} elseif ($action == 'addicon') {
    if ($method == 'GET') {

        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addicon.htm');
    } else {

        $data1['title'] = param('title');
        $data1['link'] = param('link');

        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
//          $path = "./upload/index_icon/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $insert['icon'] = I($path . $name, 'icon96');
            }
        }
        $insert['type'] = 1;
        $insert['line'] = param('line');
        $insert['order_id'] = param('order_id');
        $insert['first'] = serialize($data1);
        db_insert('index_first', $insert);
        message(0, 'success');
    }
} elseif ($action == 'edqtgj') {
    if ($method == 'GET') {
        $toid = param(4);
        $val = db_find_one("index_other", array('id' => $toid));
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/edqtgj.htm');
    } else {

        $insert['name'] = param('name');
        $insert['link'] = param('link');
        $insert['type'] = param('type');
        $insert['order_id'] = param('order_id');
        $insert['tips'] = param('tips', '');
        db_update('index_other', array('id' => param('toid')), $insert);
        message(0, 'success');
    }
} elseif ($action == 'addqtgj') {
    if ($method == 'GET') {

        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addqtgj.htm');
    } else {

        $insert['name'] = param('name');
        $insert['link'] = param('link');
        $insert['type'] = param('type');
        $insert['order_id'] = param('order_id');
        db_insert('index_other', $insert);
        message(0, 'success');
    }
} elseif ($action == 'edicon') {
    if ($method == 'GET') {
        $toid = param(4);
        $val = db_find_one("index_first", array('id' => $toid));
        $val['first'] = unserialize($val['first']);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/edicon.htm');
    } else {

        $data1['title'] = param('title');
        $data1['link'] = param('link');

        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "/upload/index_icon/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $insert['icon'] = I($path . $name, 'icon96');
            }
        }
        $insert['type'] = 1;
        $insert['line'] = param('line');
        $insert['order_id'] = param('order_id');
        $insert['first'] = serialize($data1);
        $toid = param('toid');
        db_update('index_first', array('id' => $toid), $insert);
        message(0, 'success');
    }
} elseif ($action == 'updateRedis') {
    $navid = param(4);
    $index_Nav_cache = db_find('index_navlist_view', array('navid' => $navid), array('order_id' => '1'), 1, 1000);
    cache_set('index_Nav_cache_navid_' . $navid, $index_Nav_cache, 60 * 60 * 240 * 10);
    message(0, 'success');
} elseif ($action == 'deleteview') {
    db_delete('index_navlist_view', array('id' => param('toid')));
    message(0, 'success');
} elseif ($action == 'deletesixbox') {
    db_delete('index_sixbox', array('id' => param('toid')));
    message(0, 'success');
} elseif ($action == 'deleteicon') {
    db_delete('index_first', array('id' => param('toid')));
    message(0, 'success');
} elseif ($action == 'navdelete') {
    db_delete('index_navlist', array('id' => param('id')));
    message(0, 'success');
} elseif ($action == 'deleteqtgj') {
    db_delete('index_other', array('id' => param('toid')));
    message(0, 'success');
} elseif ($action == 'cygj') {
//    $navdata = db_find_one('index_navlist', array('id' => 999));
    cache_delete('index_Nav_cache_navid_999');
    $navid = 999;
    $list = db_find('index_navlist_view', array('navid' => 999), array('order_id' => '-1'), 1, 500);
    $list = is_array($list) ? $list : array();
    foreach ($list as $k => $v) {
        if (strpos($v['favicon'], 'tubiao') !== false) {
            $list[$k]['favicon'] = "../" . $v['favicon'];
        }
    }
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/navview.htm');
} elseif ($action == 'cygjtext') {
    if ($method == 'GET') {
        $otext = kv_get('otext');
        $input['otext'] = form_text('otext', $otext['otext']);
        $input['otextlinks'] = form_text('otextlinks', $otext['otextlinks']);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/navview_base.htm');
    } else {
        $otext['otext'] = param('otext');
        $otext['otextlinks'] = param('otextlinks');
        kv_set('otext', $otext);
        message('0', jump('ok', url('plugin-setting-xl_index_admincp-cygjtext')));
    }

} elseif ($action == 'first') {
//    $navdata = db_find_one('index_navlist', array('id' => 999));
    $list = db_find('index_first', array('type' => 0), array('order_id' => '-1'), 1, 500);
    $list = is_array($list) ? $list : array();
    foreach ($list as $k => $v) {
        $list[$k]['data']['first'] = unserialize($v['first']);
        $list[$k]['data']['sec'] = unserialize($v['sec']);
        $list[$k]['data']['thd'] = unserialize($v['thd']);
    }

    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/first.htm');
} elseif ($action == 'icon') {
//    $navdata = db_find_one('index_navlist', array('id' => 999));
    $list = db_find('index_first', array('type' => 1), array('order_id' => '-1'), 1, 500);
    $list = is_array($list) ? $list : array();
    foreach ($list as $k => $v) {
        $list[$k]['data']['first'] = unserialize($v['first']);
    }
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/icon.htm');
} elseif ($action == 'qtgj') {
//    $navdata = db_find_one('index_navlist', array('id' => 999));
    $list = db_find('index_other', array(), array('order_id' => '-1'), 1, 500);

    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/qtgj.htm');
} elseif ($action == 'zwpt') {
//    $navdata = db_find_one('index_navlist', array('id' => 999));
    $list = db_find('index_navlist_view', array('navid' => 998), array('order_id' => '-1'), 1, 500);
    $list = is_array($list) ? $list : array();
    foreach ($list as &$v) {
        if (strpos($v['favicon'], 'tubiao') !== false) {
            $v['favicon'] = "../" . $v['favicon'];
        }
    }
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/navview.htm');
}
?>
