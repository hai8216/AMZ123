<?php
/**
 * Created by xuxiaolong.
 * weibo:@Sinamfyoyo
 * User: 许小龙
 * Date: 2018-08-01
 * Time: 16:16
 * Developer:商业定制联系QQ95327294
 */

$type = param('type') ?: 'thread';
$page = param('page') ?: 1;
$pagesize = 20;
$start = ($page - 1) * $pagesize;
$threadlist = array();
switch ($type) {
    case "thread":
        $threadlist = db_sql_find("SELECT * FROM bbs_thread ORDER BY views DESC LIMIT $start, $pagesize");
        if ($threadlist) foreach ($threadlist as &$thread) thread_format($thread);
        include _include(APP_PATH . "/plugin/xl_admin_log/template/admin_search_list.htm");
        break;
    case "lookuser":
        $tid = param('tid');
        $thread = thread_read($tid);
        $uselist = db_sql_find("SELECT * FROM bbs_admincp_thread_log where tid='$tid' ORDER BY count DESC LIMIT $start, $pagesize");
        foreach ($uselist as $k => $v) $uselist[$k]['user'] = user_read_cache($v['uid']);
        include _include(APP_PATH . "/plugin/xl_admin_log/template/admin_lookuser_list.htm");
        break;
    case "log":
        $tablepre = $db->tablepre;
        $search_loglist = db_sql_find("SELECT * FROM {$tablepre}admincp_search_log  ORDER BY count DESC LIMIT $start, $pagesize");
        include _include(APP_PATH . "/plugin/xl_admin_log/template/admin_search_log.htm");
        break;
    case "userkeys":
        $uids = $avatar = $userinfo = array();
        $tablepre = $db->tablepre;
        $keywords = param('keywords');
        $loglist = db_sql_find("SELECT * FROM {$tablepre}admincp_search_userlog  where keywords='$keywords' LIMIT $start, $pagesize");
        foreach ($loglist as $v) {
            $uids[] = $v['uid'];
        }
        $userinfo = db_sql_find("SELECT * FROM {$tablepre}user  where uid in(" . implode(',', $uids) . ")");
        if ($userinfo) {
            foreach ($userinfo as $k => $v) {
                $avatar[$v['uid']] = $v['avatar'];
            }
        }
        foreach ($loglist as $k => $v) {
            $dir = substr(sprintf("%09d", $v['uid']), 0, 3);
            $loglist[$k]['avatar_url'] = $avatar[$v['uid']] ? $conf['upload_url'] . "avatar/" . $dir . "/" . $v['uid'] . ".png" : 'view/img/avatar.png';
        }
        include _include(APP_PATH . "/plugin/xl_admin_log/template/admin_search_userkey.htm");
        break;

    case "userlog":
        if (isset($_GET['uid']) && $_GET['uid'] > 0) {
            $tablepre = $db->tablepre;
            $uid = intval($_GET['uid']);
            $loglist = db_sql_find("SELECT * FROM {$tablepre}admincp_search_userlog where uid='$uid' order by id  LIMIT $start, $pagesize");
            include _include(APP_PATH . "/plugin/xl_admin_log/template/admin_search_userkey_uid.htm");
        } elseif (isset($_GET['username']) && $_GET['username']) {
            $tablepre = $db->tablepre;
            $username = param('username');
            $user = db_sql_find_one("SELECT * FROM {$tablepre}user where username='$username'");
            $uid = intval($user['uid']);
            $loglist = db_sql_find("SELECT * FROM {$tablepre}admincp_search_userlog where uid='$uid' order by id  LIMIT $start, $pagesize");
            include _include(APP_PATH . "/plugin/xl_admin_log/template/admin_search_userkey_uid.htm");
        } else {
            $tablepre = $db->tablepre;
            $loglist = db_sql_find("SELECT * FROM {$tablepre}admincp_search_userlog group by uid order by id  LIMIT $start, $pagesize");
            include _include(APP_PATH . "/plugin/xl_admin_log/template/admin_search_userlog.htm");
        }
        break;
    default:
        include _include(APP_PATH . "/plugin/xl_admin_log/template/admin_search_list.htm");
        break;
}


