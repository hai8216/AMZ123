<?php
/**
 * Created by xuxiaolong.
 * weibo:@Sinamfyoyo
 * User: 许小龙
 * Date: 2018-08-01
 * Time: 10:18
 * Developer:商业定制联系QQ95327294
 */
!defined('DEBUG') AND exit('Access Denied.');
$mod =  param('mod')?param('mod'):'search';
$mod_array = ['user','search','log','userlog'];
if(in_array($mod,$mod_array)){
    include APP_PATH.'plugin/xl_admin_log/func/common.php';
    include APP_PATH.'plugin/xl_admin_log/action/'.$mod.'.inc.php';
}