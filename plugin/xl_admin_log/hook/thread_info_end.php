
if ($uid) {
    //记录过的就+1就行了,不清楚redis环境，若存在Redis环境，放入redis更好，这个以后再说
    $isLog = db_find_one("admincp_thread_log", array('tid' => $tid, 'uid' => $uid));
    if ($isLog['id']) {
        $sqladd = !in_array($conf['cache']['type'], array('mysql', 'pdo_mysql')) ? '' : ' LOW_PRIORITY';
        $tablepre = $db->tablepre;
        $time = time();
        $r = db_exec("UPDATE$sqladd `{$tablepre}admincp_thread_log` SET count=count+1,dateline='$time' WHERE id='$isLog[id]'");
    } else {
        $Array = array(
            "uid" => $uid,
            "tid" => $tid,
            "username" => $user['username'],
            "dateline" => time(),
            "count" => 1,
        );
        $orderId = db_insert('admincp_thread_log', $Array);
    }
}