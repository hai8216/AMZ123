<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_realv';
$action = param(3) ?: "list";
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("member_real", array(), array(), $page, $pagesize);
    $c = db_count("member_real");
    $pagination = pagination(url("plugin-setting-xl_realv-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_realv/admin/list.html');
}elseif ($action == 'del') {
  $id = param('ids');
  $ruser = db_find_one("member_real",array('id'=>$id));
  db_delete("member_real",array('id'=>$id));
  db_update("user",array('uid'=>$ruser['uid']),array('real_status'=>0));
  message(0,jump('删除成功',url('plugin-setting-xl_realv-list')));
}
?>