<?php

!defined('DEBUG') and exit('Access Denied.');
$action = param(3) ?: "list";
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("qinquan", array(), array(), $page, $pagesize);
    $c = db_count("qinquan");
    $keywords = param('keywords');
    $pagination = pagination(url("plugin-setting-xl_qinquan-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_qinquan/admin/list.html');
} elseif ($action == 'lvshi') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("qinquan_lvshi", array(), array(), $page, $pagesize);
    $c = db_count("qinquan_lvshi");
    $keywords = param('keywords');
    $pagination = pagination(url("plugin-setting-xl_qinquan-lvshi-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_qinquan/admin/lvshi.html');
} elseif ($action == 'create') {
    if ($method == 'GET') {
        if (param('id')) {
            $pid = param('id');
            $val = db_find_one("qinquan", ["id" => $pid]);
        }
        $input['pinpai'] = form_text("pinpai", $val['pinpai']);
        $input['daili'] = form_text("daili", $val['daili']);
        $input['yuangao'] = form_text("yuangao", $val['yuangao']);
        $input['anjiancode'] = form_text("anjiancode", $val['anjiancode']);
        $input['links'] = form_text("links", $val['links']);
        $input['news_links'] = form_text("news_links", $val['news_links']);
        include _include(APP_PATH . 'plugin/xl_qinquan/admin/create.html');
    } else {
        $input['pinpai'] = param("pinpai");
        $input['daili'] = param("daili");
        $input['yuangao'] = param("yuangao");
        $input['anjiancode'] = param("anjiancode");
        $input['links'] = param("links");
        $input['news_links'] = param("news_links");
        $input['dateline'] = time();
        if (param('id')) {
            db_update("qinquan", ["id" => param('id')], $input);
        } else {
            db_insert("qinquan", $input);
        }

        message(0, 'ok');
    }
} elseif ($action == 'lscreate') {
    if ($method == 'GET') {
        if (param('id')) {
            $pid = param('id');
            $val = db_find_one("qinquan_lvshi", ["id" => $pid]);
        }
        $input['name'] = form_text("name", $val['name']);
        $input['desc'] = form_textarea("desc", $val['desc'], '', 150);
        $input['btntext'] = form_text("btntext", $val['btntext'] ?: "立即了解");
        $input['btnlinks'] = form_text("btnlinks", $val['btnlinks']);
        include _include(APP_PATH . 'plugin/xl_qinquan/admin/lscreate.html');
    } else {
        $input['name'] = param("name");
        $input['desc'] = param("desc");
        $input['btntext'] = param("btntext");
        $input['btnlinks'] = param("btnlinks");
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['thumb'] = $path . $name;
            }
        }
        if (param('id')) {
            db_update("qinquan_lvshi", ["id" => param('id')], $input);
        } else {
            db_insert("qinquan_lvshi", $input);
        }

        message(0, 'ok');
    }
} elseif ($action == 'set') {
    if ($method == 'GET') {
        $setting = kv__get("qinquan_setting");
        $input['tag'] = form_text("tag", $setting['tag']);
        include _include(APP_PATH . 'plugin/xl_qinquan/admin/setting.html');
    } else {
        $setting = kv__get("qinquan_setting");
        $setting['tag'] = param("tag");
        kv_set('qinquan_setting', $setting);
        message(0, 'ok');
    }
} elseif ($action == 'del') {
    db_delete("qinquan", ["id" => param('id')]);
    message(0, jump('ok'));
} elseif ($action == 'lsdel') {
    db_delete("qinquan_lvshi", ["id" => param('id')]);
    message(0, jump('ok'));
}