<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') and exit('Access Denied.');
$pluginname = 'xl_tools_pages';
$action = param(3) ?: "list";
$select = array(
    '0' => '暂不显示',
    '1' => '图标导航',
    '2' => '工具框3栏',
    '3' => '工具框4栏',
    '4' => '工具框5栏',
    '7' => '图片样式2（1张）',
    '8' => '图片样式2（2张）',
    '9' => '图片样式2（3张）',
    '10' => '图片样式2（4张）',
    '11' => '图片样式2（5张）',
    '12' => '图片样式3（3栏）',
    '13' => '图片样式3（4栏）',
    '100' => '图片样式6（6张）',
    '15' => '链接无图6栏',
    '16' => '纯文模块',
    '17' => '链接有图4个',
    '14' => '链接有图5个',
    '18' => '链接有图6个',
    '181' => '链接有图7个',
    '182' => '链接有图8个',
    '19' => '一行4个不要图',
    '191' => '一行5个不要图',
    '192' => '一行6个不要图',
    '193' => '一行7个不要图',
    '194' => '一行8个不要图',
    '20' => '文本编辑框',
    '21' => '一行5个图文',
    '22' => '品牌介绍',
    '23' => 'banner模块',
    '24' => '临时活动',
    '25' => '随机展示',
    '26' => '联系方式',
    '27' => '分类图标',
    '28' => '商标模块4',
    '29' => '商标模块5',
    '30' => '搜索框',
    '999' => '新闻模块',

);
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $keywords = search_keyword_safe(xn_urldecode(param(6, '')));
//    echo $keywords;exit;
    if ($keywords) {
        $srchtype = param(5);
        if ($srchtype) {
            $srchtypes = 'domain';
        } else {
            $srchtypes = 'title';
        }
//        echo $keywords;exit;
        $arrlist = db_find("tools_pages", array($srchtypes => array('LIKE' => $keywords)), array(), $page, $pagesize);
        $c = db_count("tools_pages", array($srchtypes => array('LIKE' => $keywords)));
    } else {
        $arrlist = db_find("tools_pages", array(), array(), $page, $pagesize);
        $c = db_count("tools_pages");
    }

    $pagination = pagination(url("plugin-setting-xl_tools_pages-list-{page}-" . $srchtype . "-" . param(6, '')), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_tools_pages/admin/list.html');
} elseif ($action == 'newscreate') {
    if ($method == 'GET') {
        if (param('mid')) {
            $data = db_find_one("tools_model", array('mid' => param('mid')));
            $toid = $data['pages_id'];
        } else {
            $data = array('mid' => '0');
            $toid = param('toid');
        }

        include _include(APP_PATH . 'plugin/xl_tools_pages/admin/model/news.html');
    } else {
        $data['name'] = param('name');
        $data['mtype'] = param('type');
        $data['ui'] = param('ui');
        $data['order'] = param('order_id');
        $data['more_links'] = param('more_links');
        $data['pages_id'] = param('toid');
        $data['tpl'] = 999;
        $data['htitle'] = 0;
        $data['value'] = param('value');

        if (param('mid')) {
            db_update("tools_model", array('mid' => param('mid')), $data);
        } else {
//            print_r($data);exit;
            db_insert("tools_model", $data);
        }

        message(0, jump('保存成功', url('plugin-setting-xl_tools_pages-model', array('toid' => param('toid')))));

    }

} elseif ($action == 'create') {
    if ($method == 'GET') {
        if (param('toid')) {
            $data = db_find_one("tools_pages", array('id' => param('toid')));
        } else {
            $data = array();
        }

        $input['title'] = form_text('title', $data['title']);
        $input['marginTop'] = form_text('marginTop', $data['marginTop']);
        $input['title_ui'] = form_text('title_ui', $data['title_ui']);
        $input['domain'] = form_text('domain', $data['domain']);
        $input['keywords'] = form_text('keywords', $data['keywords']);
        $input['description'] = form_text('description', $data['description']);
        $input['rh'] = form_text('rh', $data['rh']);
        $input['bgcolor'] = form_text('bgcolor', $data['bgcolor']);
        $input['stitle'] = form_text('stitle', $data['stitle']);
        $input['status'] = form_radio('status', array('0' => '否', '1' => '是', '2' => '融合顶部导航模式'), $data['status']);
        $input['width'] = form_radio('width', array('0' => '自动默认', '1' => '超宽', '2' => '全屏'), $data['width']);
        $input['mtpl'] = form_radio('mtpl', array('0' => '关闭', '1' => '文字锚点', '2' => '图文锚点'), $data['mtpl']);
        $input['order_id'] = form_text('order_id', $data['order_id']);

        include _include(APP_PATH . 'plugin/xl_tools_pages/admin/create.html');
    } else {
        $data['bgcolor'] = param('bgcolor');
        $data['title'] = param('title');
        $data['marginTop'] = param('marginTop');
        $data['title_ui'] = param('title_ui');
        $data['stitle'] = param('stitle');
        $data['domain'] = param('domain');
        $data['status'] = param('status');
        $data['keywords'] = param('keywords');
        $data['description'] = param('description');
        $data['mtpl'] = param('mtpl');
        $data['order_id'] = param('order_id');
        $data['width'] = param('width');
        $data['rh'] = param('rh');
//        foreach ($data as $k => $v) {
//            if (!$v && (!in_array($k, array('status', 'mtpl','order_id')))) {
//                message(401, $v);
//            }
//        }
        if (param('toid')) {
            db_update("tools_pages", array('id' => param('toid')), $data);
        } else {
            db_insert("tools_pages", $data);
        }
        $endpage = db_count("tools_pages");
        $endpage = ceil($endpage / 30);
        message(0, jump($endpage, url('plugin-setting-xl_tools_pages-list-' . $endpage)));
    }
} elseif ($action == 'model') {
    $toid = param('toid');
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("tools_model", array('pages_id' => $toid), array('order' => '-1'), $page, $pagesize);
    $c = db_count("tools_model", array('pages_id' => $toid));
    $pagination = pagination(url("plugin-setting-xl_tools_pages-model-{page}", array('toid' => $toid)), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_tools_pages/admin/model-list.html');

} elseif ($action == 'modelcreate') {
    if ($method == 'GET') {
        $toid = param('toid');
        if (param('mid')) {
            $data = db_find_one("tools_model", array('mid' => param('mid')));
        } else {
            $data = array();
        }
        $select2 = array('显示', '不显示');
        $input['name'] = form_text('name', $data['name']);
        $input['contentSize'] = form_text('contentSize', $data['contentSize']);
        $input['tpl'] = form_select('tpl', $select, $data['tpl']);
        $input['order'] = form_text('order', $data['order']);
        $input['htitle'] = form_select('htitle', $select2, $data['htitle']);
        $input['maodian'] = form_text('maodian', $data['maodian']);
        $input['desc'] = form_text('desc', $data['desc']);
        $input['more_links'] = form_text('more_links', $data['more_links']);
        $input['fontSize'] = form_text('fontSize', $data['fontSize']);
        $input['marginTop'] = form_text('marginTop', $data['marginTop']);
        include _include(APP_PATH . 'plugin/xl_tools_pages/admin/model-create.html');
    } else {
        $data['fontSize'] = param('fontSize');
        $data['contentSize'] = param('contentSize');

        $data['name'] = param('name');
        $data['marginTop'] = param('marginTop');
        $data['tpl'] = param('tpl');
        $data['order'] = param('order');
        $data['pages_id'] = param('toid');
        $data['htitle'] = param('htitle');
        $data['maodian'] = param('maodian');
        $data['more_links'] = param('more_links');
        $data['desc'] = param('desc');
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('fileicon'), $result)) {
//            print_r($result);exit;
            $type = $result[2];
            $time = time();
            $filepath = APP_PATH . "upload/tools/" . date('Ymd', $time) . "/";
            $wwwpath = "upload/tools/" . date('Ymd', $time) . "/";
            if (!file_exists($filepath)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($filepath, 0777, true);
            }
            $new_file = time() . rand(100, 50000) . "." . $type;
            if (file_put_contents($filepath . $new_file, base64_decode(str_replace($result[1], '', param('fileicon'))))) {
                $icon = $wwwpath . $new_file;
            } else {
                $icon = param('fileicon');
            }
            $data['icon'] = $icon;
        }

//        $data['icon'] = $icon;

        if (param('mid')) {
            db_update("tools_model", array('mid' => param('mid')), $data);
        } else {
            db_insert("tools_model", $data);
        }
        if (param('route')) {
            message(0, jump('保存成功', "https://www.amz123.com/" . param('route')));
        } else {
            message(0, jump('保存成功', url('plugin-setting-xl_tools_pages-model', array('toid' => param('toid')))));

        }
    }
} elseif ($action == 'removeModel') {
    db_delete("tools_model", array('mid' => param('mid')));
    message(0, jump('操作成功', url('plugin-setting-xl_tools_pages-model', array('toid' => param('toid')))));
} elseif ($action == 'addmodeldata') {
    $mid = param('mid');
    $modelTpldata = db_find_one("tools_model", array('mid' => $mid));
//    $select = array(
//        '0'=>'暂不显示',
//        '1' => '图标导航',
//        '2' => '工具框3栏',
//        '3' => '工具框4栏',
//        '4' => '工具框5栏',
//        '5' => '图片样式1（3栏）',
//        '6' => '图片样式1（4栏）',
//        '7' => '图片样式2（1张）',
//        '8' => '图片样式2（2张）',
//        '9' => '图片样式2（3张）',
//        '10' => '图片样式2（4张）',
//        '11' => '图片样式2（5张）',
//        '12' => '图片样式3（3栏）',
//        '13' => '图片样式3（4栏）',
//        '14' => '链接有图5栏',
//        '15' => '链接无图6栏',
//        '16' => '纯文模块',
//    );
    if (in_array($modelTpldata['tpl'], array(2, 3, 4))) {
        $tplid = 2;
    } else if (in_array($modelTpldata['tpl'], array(5, 6, 7, 8, 9, 10, 11, 12, 13))) {
        $tplid = 3;
    } else if (in_array($modelTpldata['tpl'], array(14))) {
        $tplid = 4;
    } else if (in_array($modelTpldata['tpl'], array(15))) {
        $tplid = 5;
    } else if (in_array($modelTpldata['tpl'], array(16))) {
        $tplid = 6;
    } else if (in_array($modelTpldata['tpl'], array(1, 17, 18, 181, 182, 19, 191, 192, 193, 194, 21))) {
        $tplid = 1;
    } else if (in_array($modelTpldata['tpl'], array(20))) {
        $datas = db_find_one("tools_model_data", array('mid' => $mid));
        $data = unserialize($datas['fileds']);
        $tplid = 20;
    } else if (in_array($modelTpldata['tpl'], array(22))) {
        //品牌介绍
        $tplid = 22;
    } else if (in_array($modelTpldata['tpl'], array(23))) {
        //品牌介绍
        $tplid = 23;
    } else if (in_array($modelTpldata['tpl'], array(28, 29))) {
        //品牌介绍
        $tplid = 28;
    } else {
        $tplid = $modelTpldata['tpl'];
    }
    include _include(APP_PATH . 'plugin/xl_tools_pages/admin/model/create_' . $tplid . '.html');
} elseif ($action == 'eddatacreate') {
    $mid = param('mid');
    $did = param('did');
    $toid = param('toid');
    $modelTpldata = db_find_one("tools_model", array('mid' => $mid));
//    $select = array(
//        '0'=>'暂不显示',
//        '1' => '图标导航',
//        '2' => '工具框3栏',
//        '3' => '工具框4栏',
//        '4' => '工具框5栏',
//        '5' => '图片样式1（3栏）',
//        '6' => '图片样式1（4栏）',
//        '7' => '图片样式2（1张）',
//        '8' => '图片样式2（2张）',
//        '9' => '图片样式2（3张）',
//        '10' => '图片样式2（4张）',
//        '11' => '图片样式2（5张）',
//        '12' => '图片样式3（3栏）',
//        '13' => '图片样式3（4栏）',
//        '14' => '链接有图5栏',
//        '15' => '链接无图6栏',
//        '16' => '纯文模块',
//    );
    $data = db_find_one("tools_model_data", array('did' => $did));
    if (in_array($modelTpldata['tpl'], array(2, 3, 4))) {
        $tplid = 2;
    } else if (in_array($modelTpldata['tpl'], array(5, 6, 7, 8, 9, 10, 11, 12, 13))) {
        $tplid = 3;
    } else if (in_array($modelTpldata['tpl'], array(14))) {
        $tplid = 4;
    } else if (in_array($modelTpldata['tpl'], array(15))) {
        $tplid = 5;
    } else if (in_array($modelTpldata['tpl'], array(16))) {
        $tplid = 6;
    } else if (in_array($modelTpldata['tpl'], array(1, 17, 18, 181, 182, 19, 191, 192, 193, 194, 21))) {
        $tplid = 1;
    } else if (in_array($modelTpldata['tpl'], array(20))) {
        $tplid = 20;
    } else if (in_array($modelTpldata['tpl'], array(22))) {
        $tplid = 22;
    } else if (in_array($modelTpldata['tpl'], array(23))) {
        //品牌介绍
        $tplid = 23;
    } else if (in_array($modelTpldata['tpl'], array(28, 29))) {
        //品牌介绍
        $tplid = 28;
    } else {
        $tplid = $modelTpldata['tpl'];
    }

    include _include(APP_PATH . 'plugin/xl_tools_pages/admin/model/edcreate_' . $tplid . '.html');
} elseif ($action == 'doModel') {
    $mtype = param('type');
    include APP_PATH . "plugin/xl_tools_pages/model/doModel_" . $mtype . ".php";
} elseif ($action == 'modeldata') {
    $mid = param('mid');
    $toid = param('toid');
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("tools_model_data", array('mid' => $mid), array('order_id' => '-1'), $page, $pagesize);
    $c = db_count("tools_model_data", array('mid' => $mid));
    $pagination = pagination(url("plugin-setting-xl_tools_pages-modeldata-{page}", array('mid' => $mid, 'toid' => $toid)), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_tools_pages/admin/modeldata.html');
} elseif ($action == 'deltedata') {
    $did = param('did');
    db_delete("tools_model_data", array('did' => $did));
    message(0, jump('删除成功', url('plugin-setting-xl_tools_pages-modeldata', array('mid' => param('mid'), 'toid' => param('toid')))));
} elseif ($action == 'del') {
    $did = param('delid');
    db_delete("tools_pages", array('id' => $did));
//    db_delete("tools_model", array('pages_id' => $did));
    message(0, jump('删除成功', url('plugin-setting-xl_tools_pages-list', array('toid' => param('toid')))));
}
?>
