<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') AND exit('Access Denied.');
$action = param(3);
if ($action == 'newadd') {
    $getTask = param('task', '', FALSE);
    $tasklist = explode("\n", $getTask);
    if (!$tasklist[0]) {
        message(0, jump('任务不正确', url('plugin-setting-xl_adcredits')));
    } else {
        $tempcode = time() . rand(1111, 9999) . rand(11, 99);
        $success = 0;
        foreach ($tasklist as $v) {
            //分解积分

            $taskline = explode("->", $v);
            $actionType = substr($taskline[1], 0, 1);
            $uid = db_find_one("user", array('username' => $taskline[0]))['uid'];
            if ($uid) {
                $actionType = $actionType?$actionType:"+";
                $need = explode("" . $actionType . "", $taskline[1]);
                $update_array['credits' . $actionType] = $need[1];
                $uid AND $update_array AND user_update($uid, $update_array);
                db_insert("credits_log_list", array('logid' => 0, 'name' => $taskline[0],'action'=>$actionType, 'credit' => $need[1], 'tempcode' => $tempcode, 'status' => 0));
                $success++;
            } else {
                db_insert("credits_log_list", array('logid' => 0, 'name' => $taskline[0], 'action'=>$actionType,'credit' => $need[1], 'tempcode' => $tempcode, 'status' => 1, 'm' => $taskline[0] . "不存在"));
            }
            unset($taskline);
            unset($actionType);
            unset($uid);
            unset($need);
            unset($update_array);
        }
        $name = explode("->", $tasklist[0]);
        $insert = array('name' => $name[0] . "等人的积分记录", 'dateline' => time(), 'success_count' => $success);
        $logid = db_insert("credits_log", $insert);
        db_update("credits_log_list", array('tempcode' => $tempcode), array('logid' => $logid));
//        print_r($insert);
        message(0, jump('任务执行完成', url('plugin-setting-xl_adcredits-list')));

    }

} elseif ($action == 'list') {
    $page = param(4);
    $pagesize = 20;
    $task = db_find('credits_log', array(), array('dateline' => -1), $page, $pagesize);
    $listnum = db_count('credits_log');
    $pagination = pagination(url("plugin-setting-xl_adcredits-list-{page}"), $listnum, $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_adcredits/list.htm");
} elseif ($action == 'view') {
    $page = param(4);
    $pagesize = 20;
    $logid = param(5);
    $task = db_find('credits_log_list', array('logid'=>$logid), array('id' => -1), $page, $pagesize);
    $listnum = db_count('credits_log_list',array('logid'=>$logid));
    $pagination = pagination(url("plugin-setting-xl_adcredits-view-{page}-".$logid), $listnum, $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_adcredits/view.htm");
}else {
    $input['task'] = form_textarea('task', '', '100%', 500);
    include _include(APP_PATH . "/plugin/xl_adcredits/setting.htm");
}