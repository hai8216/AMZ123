<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/
$gj = array(
    1=>'美国',
    2=>'日本',
    3=>'欧洲',
    4=>'中东',
    5=>'印度',
    6=>'澳洲',
    7=>'东南亚');
!defined('DEBUG') AND exit('Access Denied.');
$action = param('3')?:'list';
if ($action == 'list') {
    $page = param(4, 1);
    $keyword = trim(xn_urldecode(param(5)));
    $map = [];
    if($keyword){
        $map['title'] = array('LIKE'=>$keyword);
    }
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("calendar_days",$map,array(),$page,$pagesize);
    foreach($arrlist as $k=> $v){
        $gjdataT = array();
        $gjdata = db_find("calendar_days_guojia",array('days_id'=>$v['id']),array(),1,20);
        foreach($gjdata as $d){
            $gjdataT[] = $gj[$d['guojia_id']];
        }
//        print_r($gjdataT);
        $arrlist[$k]['guojia'] = implode(',',$gjdataT);
    }
//    exit;
    $c = db_count("calendar_days",$map);
    $pagination = pagination(url("plugin-setting-xl_calendar-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_calendar/view/list.html');
} elseif ($action == 'create') {
    if ($method == 'GET') {
        if (param('toid')) {
            $data = db_find_one("calendar_days", array('id' => param('toid')));
            $gjdata = db_find("calendar_days_guojia", array('days_id' => param('toid')));
            foreach($gjdata as $v){
                $gjs[]=$v['guojia_id'];
            }
        } else {
            $data = array('title'=>'','time'=>'','guojia'=>0,'rexiao'=>'','tips'=>'','id'=>'');
        }
        $input['title'] = form_text('title', $data['title']);
        $input['time'] = form_text('time', date('Y/m/d',$data['time']));
//        $input['guojia'] = form_checkbox('guojia', $gj,$data['guojia']);
        $input['rexiao'] = form_textarea('rexiao',$data['rexiao']);
        $input['tips'] = form_textarea('tips',$data['tips']);
        include _include(APP_PATH . 'plugin/xl_calendar/view/create.html');
    } else {
        $data['title'] = param('title');
        $data['time'] = strtotime(param('time'));
//        $data['guojia'] = param('guojia');
        $data['rexiao'] = param('rexiao');
        $data['tips'] = param('tips');
        if (param('toid')) {
            db_delete("calendar_days_guojia",array('days_id'=>param('toid')));
            db_update("calendar_days", array('id' => param('toid')), $data);
            foreach($_POST['guojia'] as $k =>$v){
                db_insert("calendar_days_guojia",array('days_id'=>param('toid'),'guojia_id'=>$v));
            }
        } else {
            $pid = db_insert("calendar_days", $data);
            foreach($_POST['guojia'] as $k =>$v){
                db_insert("calendar_days_guojia",array('days_id'=>$pid,'guojia_id'=>$v));
            }
        }


        message(0,  jump('保存成功', url('plugin-setting-xl_calendar')));
    }
}elseif($action=='rexiao'){
    $list = db_find("calendar_days_rexiao",array('from_id'=>param('toid')));
    include _include(APP_PATH . 'plugin/xl_calendar/view/rexiao.html');
}elseif($action=='addrexiao'){
    if ($method == 'GET') {
        $from_id = param('toid');
        $input['title'] = form_text('title', '');
        $input['links'] = form_text('links', '');
        include _include(APP_PATH . 'plugin/xl_calendar/view/addrexiao.html');
    }else{
        $data['title'] = param('title');
        $data['from_id'] = param('from_id');
        $data['links'] = param('links');
        db_insert("calendar_days_rexiao", $data);
        message(0,'ok');
    }
}elseif ($action=='delrexiao'){
    db_delete("calendar_days_rexiao",array('id'=>param('toid')));
    message(0,'ok');
}
?>