<?php
$header['title'] = '亚马逊卖家日历，2020年跨境电商营销日历，美国热门节日';
$header['keywords'] = '亚马逊日历,卖家日历,跨境营销日历,美国节日,跨境日历';
$header['description'] = '2020年跨境电商节日营销日历大全，提供了热卖节日大全和对应热门产品及运营小贴士，帮助亚马逊卖家把握运营节奏。只要一个表，读懂2020年热门节日和亚马逊大卖日历，一整年的全球热点节日热点都在这里了。';


$gj = array(
    1 => '美国',
    2 => '日本',
    3 => '欧洲',
    4 => '中东',
    5 => '印度',
    6 => '澳洲',
    7 => '东南亚');
$checkary = explode(',', param('P'));
foreach ($checkary as $v) {
    if ($v > 0) {
        $fidays[] = intval($v);
    }
}

if (!$fidays[0]) {
    $code = 0;
} else {
//    print_r($fidays);
    $code =  implode('A', $fidays);
}
$code =   param('P');

function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0)
{

    $ckey_length = 4;

    $key = md5($key ? $key : UC_KEY);
    $keya = md5(substr($key, 0, 16));
    $keyb = md5(substr($key, 16, 16));
    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';

    $cryptkey = $keya . md5($keya . $keyc);
    $key_length = strlen($cryptkey);

    $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
    $string_length = strlen($string);

    $result = '';
    $box = range(0, 255);

    $rndkey = array();
    for ($i = 0; $i <= 255; $i++) {
        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
    }

    for ($j = $i = 0; $i < 256; $i++) {
        $j = ($j + $box[$i] + $rndkey[$i]) % 256;
        $tmp = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }

    for ($a = $j = $i = 0; $i < $string_length; $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $tmp = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
    }

    if ($operation == 'DECODE') {
        if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
            return substr($result, 26);
        } else {
            return '';
        }
    } else {
        return $keyc . str_replace('=', '', base64_encode($result));
    }
}

include _include(APP_PATH . 'plugin/xl_calendar/view/index.html');
