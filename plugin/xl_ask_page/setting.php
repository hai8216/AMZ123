<?php
!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_ask_page';
$action = param(3) ?: "list";

if ($action == 'list') {

    $page = param(4, 1);
    $pagesize = 50;
    $start = ($page - 1) * $pagesize;
    if(param('keyword')){
        $arrlist = db_find("ask_page", array('ask_name'=>array('LIKE'=>param('keyword'))), array(), $page, $pagesize);
        $c = db_count("ask_page", array('ask_name'=>array('LIKE'=>param('keyword'))));
        $pagination = pagination(url("plugin-setting-xl_ask_page-list-{page}",array('keyword'=>param('keyword'))), $c, $page, $pagesize);
    }elseif(param('eempty')){
        $arrlist = db_find("ask_page", array('e'=>''), array(), $page, $pagesize);
        $c = db_count("ask_page", array('e'=>''));
        $pagination = pagination(url("plugin-setting-xl_ask_page-list-{page}",array('eempty'=>1)), $c, $page, $pagesize);
    }else{
        $arrlist = db_find("ask_page", array(), array(), $page, $pagesize);
        $c = db_count("ask_page");
        $pagination = pagination(url("plugin-setting-xl_ask_page-list-{page}"), $c, $page, $pagesize);
    }

    include _include(APP_PATH . 'plugin/' . $pluginname . '/admin/list.html');
} elseif
($action == 'addtag') {
    if ($method == 'GET') {
        if (param('ask_id')) {
            $data = db_find_one("ask_page", array('ask_id' => param('ask_id')));
        }
        $input['ask_name'] = form_text("ask_name", $data['ask_name']);
        $input['seo_title'] = form_text("seo_title", $data['seo_title']);
        $input['bind_tag'] = form_textarea("bind_tag", $data['bind_tag']);
        $input['seo_keywords'] = form_text("seo_keywords", $data['seo_keywords']);
        $input['seo_desc'] = form_textarea("seo_desc", $data['seo_desc']);
        $input['e'] = form_text("e", $data['e']);
//        $input['sip_desc'] = form_textarea("sip_desc", $data['sip_desc']);
        include _include(APP_PATH . 'plugin/' . $pluginname . '/admin/add.html');
    } else {
        $dataAry = array(
            'ask_name' => param('ask_name'),
            'seo_title' => param('seo_title'),
            'seo_keywords' => param('seo_keywords'),
            'seo_desc' => param('seo_desc'),
            'bind_tag' => param('bind_tag'),
            'e'=>param('e')?:pinyin1(param('ask_name'))
        );
        if (param('ask_id')) {
            $data = db_find_one("ask_page", array('ask_id' => param('ask_id')));
            db_update("ask_page", array('ask_id' => param('ask_id')), $dataAry);
        } else {
            db_insert("ask_page", $dataAry);
        }
        message(0, jump('操作完成', url('plugin-setting-xl_ask_page')));
    }
} elseif ($action == 'del') {
    db_delete("ask_page", array('ask_id'=>param('ask_id')));
    message(0, jump('操作完成', url('plugin-setting-xl_ask_page')));
}elseif ($action == 'addtextarea') {
    if ($method == 'GET') {
        $textarea = form_textarea("textarea", '');
        include _include(APP_PATH . 'plugin/' . $pluginname . '/admin/textarea.html');
    } else {
        $str = $_POST['textarea'];
        $str = explode("\r\n", $str);
        foreach ($str as $v) {
            $strary = explode("|", $v);
            db_insert("ask_page", array('ask_name' => $strary[0],'bind_tag'=>$strary[1],'e'=>pinyin1($strary[0])));
        }
        message(0, jump('导入成功', url('plugin-setting-xl_ask_page')));
    }
}



function getfirstchar($s0){
    $fchar = ord($s0{0});

    if($fchar >= ord("A") and $fchar <= ord("z") )return strtoupper($s0{0});
    $s1 = iconv("UTF-8","gb2312", $s0);
    $s2 = iconv("gb2312","UTF-8", $s1);
    if($s2 == $s0){$s = $s1;}else{$s = $s0;}
    $asc = ord($s{0}) * 256 + ord($s{1}) - 65536;
    if($asc >= -20319 and $asc <= -20284) return "A";
    if($asc >= -20283 and $asc <= -19776) return "B";
    if($asc >= -19775 and $asc <= -19219) return "C";
    if($asc >= -19218 and $asc <= -18711) return "D";
    if($asc >= -18710 and $asc <= -18527) return "E";
    if($asc >= -18526 and $asc <= -18240) return "F";
    if($asc >= -18239 and $asc <= -17923) return "G";
    if($asc >= -17922 and $asc <= -17418) return "H";
    if($asc >= -17417 and $asc <= -16475) return "J";
    if($asc >= -16474 and $asc <= -16213) return "K";
    if($asc >= -16212 and $asc <= -15641) return "L";
    if($asc >= -15640 and $asc <= -15166) return "M";
    if($asc >= -15165 and $asc <= -14923) return "N";
    if($asc >= -14922 and $asc <= -14915) return "O";
    if($asc >= -14914 and $asc <= -14631) return "P";
    if($asc >= -14630 and $asc <= -14150) return "Q";
    if($asc >= -14149 and $asc <= -14091) return "R";
    if($asc >= -14090 and $asc <= -13319) return "S";
    if($asc >= -13318 and $asc <= -12839) return "T";
    if($asc >= -12838 and $asc <= -12557) return "W";
    if($asc >= -12556 and $asc <= -11848) return "X";
    if($asc >= -11847 and $asc <= -11056) return "Y";
    if($asc >= -11055 and $asc <= -10247) return "Z";
    return null;
}

function pinyin1($zh){
    if($zh=='汶川'){
        return "W";
    }else{
        return  getfirstchar($zh);
    }
}
?>