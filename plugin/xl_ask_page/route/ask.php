<?php

$ask = param(1);
if (!$ask) {
    message(0, jump('问题不存在', url('index')));
} else {
    $ask = urldecode($ask);

    $data = db_find_one("ask_page", array('ask_name' => $ask));
    if(!$data['ask_id']){
        message(0, jump('问题不存在', url('index')));
    }
    $header['title'] = $data['seo_title'] ?: $ask . " - AMZ123亚马逊导航";
    $header['keywords'] = $data['seo_keywords'] ?: $ask . ",AMZ123亚马逊导航";
    $header['description'] = $data['seo_desc'] ?: $ask . " - AMZ123亚马逊导航";
    //热门标签
    $hotTag = db_find("thread_tag", array(), array('views' => '-1'), 1, 30);
    //获取相关帖子

    $tagname = explode(",", $data['bind_tag']);
    if(!$tagname[0]){
        $tagname = "亚马逊";
    }
    $tagthraedlist = db_find("thread_tag", array('tag_name' => $tagname), array(), 1, 10);
    foreach ($tagthraedlist as $v) {
        $tag_id[] = $v['tag_id'];
    }
    $page = param(2, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $askthraedlist = db_find("thread_tag_keys", array('tags_id' => $tag_id), array(), $page, $pagesize);
    foreach ($askthraedlist as $v) {
        $tids[] = $v['tid'];
    }
    $pagination = pagination(url("ask-" . $ask . "-{page}"), $c, $page, $pagesize);
    $thraedlist = db_find("thread", array('tid' => $tids), array('create_date' => '-1'));
    foreach ($thraedlist as $k => $v) {
        $thraedlist[$k]['user'] = user_read_cache($v['uid']);
        $thraedlist[$k]['create_date'] = humandate($v['create_date']);
        $thraedlist[$k]['img'] = getPost_img($v['tid']);
        $thraedlist[$k]['jianjie'] = $v['jianjie'] ? $v['jianjie'] : post_message($v['tid']);
    }
    include _include(APP_PATH . 'plugin/xl_ask_page/view/index.html');
}