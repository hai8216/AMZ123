<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') AND exit('Access Denied.');
$pluginname = 'xl_keywordscheck';
$action = param(3) ?: "list";
$kid = param(4);
$ko = trim(xn_urldecode(param(5)));
if ($action == 'list') {
    $page =  param(4,1);
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;
    $list = array();
    if($ko){
        $list = db_find('keywords', array('keywords'=>$ko),array('id'=>'-1'),$page,$pagesize);
        $c =  db_count('keywords', array('keywords'=>$ko));
        $pagination = pagination(url("plugin-setting-xl_keywordscheck-list-{page}-".$ko), $c, $page, $pagesize);
    }else{
        $list = db_find('keywords', array("md5"=>0),array('id'=>'-1'),$page,$pagesize);
        $c =  db_count('keywords');
        $pagination = pagination(url("plugin-setting-xl_keywordscheck-list-{page}"), $c, $page, $pagesize);
    }
//    unset($list);
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/list.htm');

} elseif ($action == 'add') {
    if ($method == 'GET') {
        $val = array();
        $val = db_find_one('keywords', array('id' => $kid));
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/add.htm');
    } else {
        $data['keywords'] = param('keywords');
        $data['name'] = param('name');
        $data['tag'] = param('tag');
        $pl_keywords   = param('pl_keywords');
        include XIUNOPHP_PATH."../plugin/xn_search/model/__pinyin.func.php";
        $data['E'] = strtoupper(substr(pinyin($data['keywords']), 0, 1))?:strtoupper(substr($data['keywords']));
        if (param('kid')) {
            db_update('keywords', array('id' => param('kid')), $data);
        } else {
            if($pl_keywords){
               $plkeywrods = explode("\n",$pl_keywords);
               foreach ($plkeywrods as $v){
                   db_insert('keywords', array('keywords'=>$v));
               }
            }else{
                db_insert('keywords', $data);
            }

        }
//        $navlist = db_find('index_navlist');
//        kv_set('index_nav_cache', serialize($navlist));
        message(0, 'success');
    }
}elseif($action=='delete'){
    db_delete('keywords', array('id' => param('id')));
    message(0, 'success');
}elseif($action=='deletejihuoma'){
    db_delete('chormeTools_jiance_code', array('id' => param('id')));
    message(0, 'success');
}elseif($action=='alldelete'){
    db_delete('keywords', array('deletew' => 0));
    message(0, 'success');
}elseif($action=='jihuoma'){
    $page =  param(4,1);
    $pagesize = 50;
    $start = ($page - 1) * $pagesize;
    $list = array();
    $list = db_find('chormeTools_jiance_code', array(),array('id'=>'-1'),$page,$pagesize);
    $c =  db_count('chormeTools_jiance_code');
    $pagination = pagination(url("plugin-setting-xl_keywordscheck-jihuoma-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/' . $pluginname . '/view/jihuoma.html');
}elseif($action=='addjihuoma'){
    if ($method == 'GET') {
        include _include(APP_PATH . 'plugin/' . $pluginname . '/view/addjihuoma.html');
    } else {
        $count = param('count');
        for($i=0;$i<$count;$i++){

            db_insert("chormeTools_jiance_code",array('code'=>getRandom(32),'status'=>0));
        }
        message(0, 'success');
    }
}elseif($action=='exceljihuoma'){
    require_once APP_PATH . '/plugin/xl_search_baidu/Classes/PHPExcel.php';
    $phpexcel = new PHPExcel();
    $phpexcel->getActiveSheet()->setCellValue('A1', '激活码');
    $phpexcel->getActiveSheet()->setCellValue('B1', '使用情况');
    $phpexcel->getActiveSheet()->setCellValue('C1', '使用人');
    $phpexcel->getActiveSheet()->setCellValue('D1', '使用人URL');
    $Re = db_find("chormeTools_jiance_code", array(), array(), 1, 100000);
    foreach ($Re as $k => $v) {
        $k = $k + 2;
        $phpexcel->getActiveSheet()->setCellValue('A' . $k, $v['code']);
        $phpexcel->getActiveSheet()->setCellValue('B' . $k, $v['status']);
        $phpexcel->getActiveSheet()->setCellValue('C' . $k, $v['use_name']);
        $phpexcel->getActiveSheet()->setCellValue('D' . $k, $v['use_uid']?"https://www.amz123.com/".url("user-".$v['use_uid']):"");
    }



    $obj_Writer = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
    $filename = "【激活码】" . ".xls";//文件名

//设置header

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/download");
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header("Content-Transfer-Encoding: binary");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
    $obj_Writer->save('php://output');//输出
    die();//种植执行
}elseif($action=='excel'){
    require_once APP_PATH . '/plugin/xl_search_baidu/Classes/PHPExcel.php';
    $phpexcel = new PHPExcel();
    $phpexcel->getActiveSheet()->setCellValue('A1', '关键词');
    $Re = db_find("keywords", array(), array(), 1, 100000);
        foreach ($Re as $k => $v) {
            $k = $k + 2;
            $phpexcel->getActiveSheet()->setCellValue('A' . $k, $v['keywords']);
        }



    $obj_Writer = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
    $filename = "【关键词】" . ".xls";//文件名

//设置header

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/download");
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header("Content-Transfer-Encoding: binary");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
    $obj_Writer->save('php://output');//输出
    die();//种植执行
}

function getRandom($param){
    $str="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $key = "";
    for($i=0;$i<$param;$i++)
    {
        $key .= $str{mt_rand(0,32)};    //生成php随机数
    }
    return $key;
}
?>