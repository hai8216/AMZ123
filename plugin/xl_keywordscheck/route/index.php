<?php
$route = param(1, '', FALSE);
$page = param(2, 1);
$keyinfo = db_find_one("keywords", array('md5' => search_keyword_safe($route)));
if(!$keyinfo['id']){
    message(0,jump('不存在',url('index')));
}
$pagesize = 20;
$start = ($page - 1) * $pagesize;
$tagdata = $keyinfo['tag'];
foreach (explode(",", $tagdata) as $v) {
    $tag[] = $v;
}
$threadtagdata = db_find("thread_tag_keys", ["tags_id" => $tag], ["keys_id" => '-1'], $page, $pagesize);
foreach ($threadtagdata as $v) {
    $tids[] = $v['tid'];
    $tags_id[$v['tid']] = $v['tags_id'];
}
unset($threadtagdata);
$threadtagdata = NULL;
$threadlistdata = db_sql_find("SELECT * FROM bbs_thread WHERE `tid` in (" . implode(",", $tids) . ")  limit $page, $pagesize");
//$threadlistdata = db_find("thread", ["tid" => $tids], ["create_date" => '-1'], 1, $pagesize);
$c = db_sql_find("SELECT count(*) as count FROM bbs_thread WHERE `tid` in (" . implode(",", $tids) . ")")['count'];
$pagination = pagination(url("keys-" . $keyinfo['md5'] . "-{page}"), $c, $page, $pagesize);
if ($keyinfo['name']) {
    $etitle = $keyinfo['name'];
    $header['title'] = "". $keyinfo['keywords'] . "侵权吗，最新" . $etitle . "商标专利侵权动态 - AMZ123侵权风险分析频道";
} else {
    $etitle = $keyinfo['keywords'];
    $header['title'] = "". $etitle . "侵权吗，最新" . $etitle . "商标专利侵权动态 - AMZ123侵权风险分析频道";
}

$header['description'] = "AMZ123为您带来最新的" . $etitle . "商标专利侵权新闻动态。";
$header['keywords'] = $etitle . "侵权";
$randTag = db_sql_find("SELECT * FROM bbs_keywords  ORDER BY RAND() LIMIT 30");
include _include(APP_PATH . 'plugin/xl_keywordscheck/view/index.htm');
