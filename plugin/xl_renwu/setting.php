<?php

!defined('DEBUG') and exit('Access Denied.');
$action = param(3) ?: "list";
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $keywords = param('keywords');
    $map=[];
    if($keywords){
        $map['name'] = array('LIKE'=>$keywords);
        $arrlist = db_find("renwu", $map, array(), $page, $pagesize);
    }else{
        $arrlist = db_find("renwu", array(), array(), $page, $pagesize);
    }
    $c = db_count("renwu",$map);
    $pagination = pagination(url("plugin-setting-xl_renwu-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_renwu/admin/list.html');
} elseif ($action == 'create') {
    if ($method == 'GET') {
        if (param('pid')) {
            $pid = param('pid');
            $val = db_find_one("renwu", ["id" => $pid]);
        }
        $input['name'] = form_text("name", $val['name']);
        $input['name2'] = form_text("name2", $val['name2']);
        $input['name3'] = form_text("name3", $val['name3']);
        $input['touxian'] = form_text("touxian", $val['touxian']);
        $input['desc'] = form_textarea("desc", $val['desc'],'','150');
        $input['seo_title'] = form_text("seo_title", $val['seo_title']);
        $input['seo_desc'] = form_text("seo_desc", $val['seo_desc']);
        $input['seo_keywords'] = form_text("seo_keywords", $val['seo_keywords']);
        $input['tag'] = form_text("tag", $val['tag']);
        $input['orderid'] = form_text("orderid", $val['orderid']);
        include _include(APP_PATH . 'plugin/xl_renwu/admin/create.html');
    } else {
        $input['name'] = param("name");
        $input['name2'] = param("name2");
        $input['name3'] = param("name3");
        $input['touxian'] = param("touxian");
        $input['desc'] = param("desc");
        $input['seo_title'] = param("seo_title");
        $input['seo_desc'] = param("seo_desc");
        $input['seo_keywords'] = param("seo_keywords");
        $input['tag'] = param("tag");
        $input['orderid'] = param("orderid");
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['avatar'] = $path . $name;
            }
        }
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl_1'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl_1'))), $path . $name)) {
                $input['qrcoode'] = $path . $name;
            }
        }
        if (param('id')) {
            db_update("renwu", ["id" => param('id')], $input);
        } else {
            db_insert("renwu", $input);
        }

        message(0, 'ok');
    }
}elseif($action=='tagajax'){
    if ($method == 'GET') {
        $input['key'] = form_text("key", '');
        include _include(APP_PATH . 'plugin/xl_pingtai/admin/tagajax.html');
    } else {
        $key = param('key');
        $map['tag_name'] = array('LIKE'=>$key);
        $search = db_find("thread_tag",$map,[],1,600);
        echo json_encode($search);
        exit;
    }
}elseif($action=='delete'){
    $pid = param('pid');
    db_delete("renwu", ["id" => $pid]);
    message(0,jump('操作成功',url('plugin-setting-xl_renwu')));
}

