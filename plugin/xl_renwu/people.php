<?php
$action = param(1);
if ($action == 'view') {
    $rid = param(2);
    if(!$rid){
        message(0,jump('参数错误',url('people')));
    }
    $renwuView = db_find_one("renwu",["id"=>$rid]);
    foreach (explode(",", $renwuView['tag']) as $v) {
        $tag[] = $v;
    }
    $page = param(1, 1);
    $pagesize = 20;
    $start = ($page - 1) * $pagesize;
    $threadtagdata = db_find("thread_tag_keys", ["tags_id" => $tag], ["keys_id" => '-1'], $page, $pagesize);
    foreach ($threadtagdata as $v) {
        $tids[] = $v['tid'];
        $tags_id[$v['tid']] = $v['tags_id'];
    }
    unset($threadtagdata);
    $threadtagdata = NULL;
    if($renwuView['tuid']){
        $threadlistdata = db_sql_find("SELECT * FROM bbs_thread WHERE `tid` in (".implode(",",$tids).") or  `uid` in (".$renwuView['tuid'].") limit $page, $pagesize");
//$threadlistdata = db_find("thread", ["tid" => $tids], ["create_date" => '-1'], 1, $pagesize);
        $c = db_sql_find("SELECT count(*) as count FROM bbs_thread WHERE `tid` in (".implode(",",$tids).") or  `uid` in (".$renwuView['tuid']." )")['count'];
    }else{
        $threadlistdata = db_sql_find("SELECT * FROM bbs_thread WHERE `tid` in (".implode(",",$tids).")  limit $page, $pagesize");
//$threadlistdata = db_find("thread", ["tid" => $tids], ["create_date" => '-1'], 1, $pagesize);
        $c = db_sql_find("SELECT count(*) as count FROM bbs_thread WHERE `tid` in (".implode(",",$tids).")")['count'];
    }
    if($renwuView['name2']){
        $renwuView['name2'] = "_".$renwuView['name2'];
    }
    if($renwuView['name3']){
        $renwuView['name3'] = "_".$renwuView['name3'];
    }
    $title = "".$renwuView['name'].$renwuView['name2'].$renwuView['name3']."简介、联系方式、最新消息-AMZ123跨境导航";
    $keyword = "".$renwuView['name']."简介,".$renwuView['touxian'].",".$renwuView['name']."职业经历,".$renwuView['name']."资讯";
    $desc  = $renwuView['desc'];
    $spdata['seo_title'] = $title;
    $spdata['seo_desc'] = $renwuView['name'].",".$renwuView['touxian'].",".xn_substr(trim($desc), 0, 80);;
    $spdata['seo_keywords'] = $keyword;
//    //更新点击数
//    $count = $this->redis->hGet($hash, $field);
//    if ($count) {
//        $count++;
//    } else {
//        $count = 1;
//    }

    // 数据写入 Redis，字段已存在会被更新
//    $this->redis->hSet($hash, $field, $count);
    include _include(APP_PATH . 'plugin/xl_renwu/view.html');
}else{
    $page = param('page', 1);
    $pagesize = 60;
    $start = ($page - 1) * $pagesize;

    $keywords = param('keyword');
    if($keywords){
        $map['name'] = array("LIKE"=>$keywords);
    }else{
        $map = [];
    }
    $arrlist = db_find("renwu", $map, array('id'=>'-1'), $page, $pagesize);
    $c = db_count("renwu",$map);
    $pagination = pagination(url("people")."?page={page}", $c, $page, $pagesize);
    $spdata['seo_title'] = "跨境人物库_跨境KOL大全-AMZ123跨境导航";
    $spdata['seo_desc'] = "跨境人物库，跨境KOL";
    $spdata['seo_keywords'] = "AMZ123跨境导航为您提供最全的跨境名人大全。";
    include _include(APP_PATH . 'plugin/xl_renwu/index.html');
}