<?php
$tabmenus = [
    "list" => "商品数据",
    "imgcard" => "5图设置",
    "xianshiqiang" => "限时抢设置",
];
!defined('DEBUG') and exit('Access Denied.');
$action = param(3) ?: "list";
if ($action == 'list') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("jingxuan_shop", array(), array(), $page, $pagesize);
    $c = db_count("jingxuan_shop");
    $pagination = pagination(url("plugin-setting-xl_yhjingxuan-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_yhjingxuan/admin/list.html');
} elseif ($action == 'create') {
    if ($method == 'GET') {
        if (param('id')) {
            $pid = param('id');
            $val = db_find_one("jingxuan_shop", ["id" => $pid]);
        }
        $input['title'] = form_text("title", $val['title']);
        $input['title2'] = form_text("title2", $val['title2']);
        $input['old_price'] = form_text("old_price", $val['old_price']);
        $input['now_price'] = form_text("now_price", $val['now_price']);
        $input['views'] = form_text("views", $val['views']);
        $input['links'] = form_text("links", $val['links']);
        $input['orderid'] = form_text("orderid", $val['orderid']);
        include _include(APP_PATH . 'plugin/xl_yhjingxuan/admin/create.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['thumb'] = $path . $name;
            }
        }
        $input['title'] = param("title");
        $input['title2'] = param("title2");
        $input['old_price'] = param("old_price");
        $input['now_price'] = param("now_price");
        $input['views'] = param("views", 0);
        $input['links'] = param("links");
        $input['orderid'] = param("orderid");
        if (param('id')) {
            db_update("jingxuan_shop", ["id" => param('id')], $input);
        } else {
            db_insert("jingxuan_shop", $input);
        }
        message(0, 'ok');
    }
} elseif ($action == 'del') {
    db_delete("jingxuan_shop", ["id" => param('id')]);
    message(0, jump('ok'));
} elseif ($action == 'imgcard') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("jingxuan_imgcard", array(), array(), $page, $pagesize);
    $c = db_count("jingxuan_imgcard");
    $pagination = pagination(url("plugin-setting-xl_yhjingxuan-imgcard-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_yhjingxuan/admin/list_imgcard.html');
} elseif ($action == 'create_imgcard') {
    if ($method == 'GET') {
        if (param('id')) {
            $pid = param('id');
            $val = db_find_one("jingxuan_imgcard", ["id" => $pid]);
        }
        $input['links'] = form_text("links", $val['links']);
        $input['orderid'] = form_text("orderid", $val['orderid']);
        include _include(APP_PATH . 'plugin/xl_yhjingxuan/admin/create_imgcard.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['thumb'] = $path . $name;
            }
        }
        $input['links'] = param("links");
        $input['orderid'] = param("orderid");
        if (param('id')) {
            db_update("jingxuan_imgcard", ["id" => param('id')], $input);
        } else {
            db_insert("jingxuan_imgcard", $input);
        }
        message(0, 'ok');
    }
} elseif ($action == 'del_imgcard') {
    db_delete("jingxuan_imgcard", ["id" => param('id')]);
    message(0, jump('ok'));
} elseif ($action == 'xianshiqiang') {
    $page = param(4, 1);
    $pagesize = 10;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("jingxuan_xianshiqiang", array(), array(), $page, $pagesize);
    $c = db_count("jingxuan_xianshiqiang");
    $pagination = pagination(url("plugin-setting-xl_yhjingxuan-xianshiqiang-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_yhjingxuan/admin/list_xianshiqiang.html');
} elseif ($action == 'create_xianshiqiang') {
    if ($method == 'GET') {
        if (param('id')) {
            $pid = param('id');
            $val = db_find_one("jingxuan_xianshiqiang", ["id" => $pid]);
        }
        $input['name'] = form_text("name", $val['name']);
        $input['star_time'] = form_time("star_time", date('Y-m-d H:i', $val['star_time']));
        $input['end_time'] = form_time("end_time", date('Y-m-d H:i', $val['end_time']));
        include _include(APP_PATH . 'plugin/xl_yhjingxuan/admin/create_xianshiqiang.html');
    } else {
        $input['name'] = param("name");
        $input['star_time'] = strtotime(param("star_time"));
        $input['end_time'] = strtotime(param("end_time"));
        if (param('id')) {
            db_update("jingxuan_xianshiqiang", ["id" => param('id')], $input);
        } else {
            db_insert("jingxuan_xianshiqiang", $input);
        }
        message(0, 'xianshiqiang_del');
    }
} elseif ($action == 'del_imgcard') {
    db_delete("jingxuan_xianshiqiang", ["id" => param('id')]);
    message(0, jump('ok'));
} elseif ($action == 'xianshiqiang_list') {
    $page = param(4, 1);
    $pagesize = 10;
    $qid = param('id');
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find("jingxuan_xianshiqiang_data", ["qid" => $qid], array(), $page, $pagesize);
    $c = db_count("jingxuan_xianshiqiang_data");
    $pagination = pagination(url("plugin-setting-xl_yhjingxuan-xianshiqiang_list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_yhjingxuan/admin/list_xianshiqiang_list.html');
} elseif ($action == 'create_xianshiqiang_list') {
    if ($method == 'GET') {
        if (param('id')) {
            $pid = param('id');
            $val = db_find_one("jingxuan_xianshiqiang_data", ["id" => $pid]);
        }
        $qid = param('qid');
        $input['title'] = form_text("title", $val['title']);
        $input['old_price'] = form_text("old_price", $val['old_price']);
        $input['now_price'] = form_text("now_price", $val['now_price']);
        $input['views'] = form_text("views", $val['views']);
        $input['links'] = form_text("links", $val['links']);
        $input['orderid'] = form_text("orderid", $val['orderid']);
        include _include(APP_PATH . 'plugin/xl_yhjingxuan/admin/create_xianshiqiang_list.html');
    } else {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', param('hidden_img_tpl'), $res)) {
            $type = $res[2];
            $path = "./upload/pingtai-img/" . date('Ymd') . "/";
            $ext = '.' . $type;
            $name = uniqid() . $ext;
            if (saveTencentBase64(base64_decode(str_replace($res[1], '', param('hidden_img_tpl'))), $path . $name)) {
                $input['thumb'] = $path . $name;
            }
        }
        $input['title'] = param("title");
        $input['old_price'] = param("old_price");
        $input['now_price'] = param("now_price");
        $input['views'] = param("views", 0);
        $input['links'] = param("links");
        $input['orderid'] = param("orderid");
        $input['qid'] = param("qid");
        if (param('id')) {
            db_update("jingxuan_xianshiqiang_data", ["id" => param('id')], $input);
        } else {
            db_insert("jingxuan_xianshiqiang_data", $input);
        }
        message(0, 'ok');
    }
} elseif ($action == 'xianshiqiang_list_del') {
    db_delete("jingxuan_xianshiqiang_data", ["id" => param('id')]);
    message(0, jump('ok'));
}