<?php
if (!$uid) {
    message(500, 'login');
} else {

    $type = param('type');
    if ($type == 'thread') {
        $tid = param('tid');

        $digg_log = db_find_one("digg", ["uid" => $uid, "type_value" => $tid, "type" => 0]);
        if (isset($digg_log) && $digg_log['id']) {
            db_delete("digg", ["uid" => $uid, "type_value" => $tid, "type" => 0]);
            $digg_status = 0;
        } else {
            db_insert("digg", ["uid" => $uid, "type_value" => $tid, "dateline" => time(), "type" => 0]);
            $digg_status = 1;
        }
        $thread_digg_count = db_count("digg", ["type_value" => $tid, "type" => 0]);
        db_update("thread", ["tid" => $tid], ["digg" => $thread_digg_count]);
        $thread_digg_info['count'] = $thread_digg_count;
        if ($thread_digg_count > 0) {
            $user_list = db_find("digg", ["type_value" => $tid, "type" => 0],[], 1, 1000);
            foreach ($user_list as $v) {
                $user_list_value[] = $v['uid'];
            }
        }else{
            $user_list_value = [];
        }
        $thread_digg_info['user_list'] = $user_list_value;
        cache_set("thread_digg_info_" . $tid, $thread_digg_info, 3600);
        thread_cache_updateFiled($tid,"digg");
        message(0, ["count"=>$thread_digg_count,'status'=>$digg_status]);
    }
}
