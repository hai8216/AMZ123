<?php

/*
	Xiuno BBS 4.0 插件实例：QQ 登陆插件设置
	admin/plugin-setting-xn_qq_login.htm
*/

!defined('DEBUG') and exit('Access Denied.');
$action = param(3) ?: "list";
$attribute = ["", '跨境卖家', '跨境服务商', "平台"];
$local = ["", "深圳", "广州", "广东其他", "福建", "浙江", "上海", "江苏", "湖南", "四川", "湖北", "江西", "其他","国外"];
$sslocal = ["", "海外", "深圳", "广东", "福建", "浙江", "上海", "江苏", "湖南", "四川", "湖北", "江西", "其他属性：平台", "卖家", "服务商"];
$dmjlocal = ["", "深圳", "广州", "广东其他", "福建", "浙江", "上海", "江苏", "湖南", "四川", "湖北", "江西", "其他上市融资：未融资", "已融资", "已上市"];
$number = ["", "种子", "天使", "A轮", "B轮", "C轮", "D轮", "E轮", "F轮", "上市", "并购", "战投"];
if ($action == 'list') {
    //找服务
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('funding', array(), array('orderid' => '-1', 'dateline' => '-1'), $page, $pagesize);
    $c = db_count("funding");
    $pagination = pagination(url("plugin-setting-xl_funding-list-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_funding/admin/list.html');
} elseif ($action == 'shangshi') {
    //找服务
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('shangshi', array(), array('orderid' => '-1', 'dateline' => '-1'), $page, $pagesize);
    $c = db_count("shangshi");
    $pagination = pagination(url("plugin-setting-xl_funding-shangshi-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_funding/admin/shangshi.html');
} elseif ($action == 'damaijia') {
    //找服务
    $page = param(4, 1);
    $pagesize = 30;
    $start = ($page - 1) * $pagesize;
    $arrlist = db_find('damaijia', array(), array('orderid' => '-1'), $page, $pagesize);
    $c = db_count("damaijia");
    $pagination = pagination(url("plugin-setting-xl_funding-damaijia-{page}"), $c, $page, $pagesize);
    include _include(APP_PATH . 'plugin/xl_funding/admin/damaijia.html');
} elseif ($action == 'create') {
    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("funding", ["id" => param('id')]);
        } else {
            $data = [];
        }
        $data['dateline'] = $data['dateline'] ?: time();
        $input['dateline'] = form_time('dateline', date('Y-m-d', $data['dateline']), '200');
        $input['name'] = form_text('name', $data['name']);
        $input['links'] = form_text('links', $data['links']);
        $input['orderid'] = form_text('orderid', $data['orderid']);
        $input['attribute'] = form_select('attribute', $attribute, $data['attribute']);
        $input['local'] = form_select('local', $local, $data['local']);
        $input['number'] = form_select('number', $number, $data['number']);
        $input['money'] = form_text('money', $data['money'], false, '单位万');
        $input['touzi'] = form_textarea('touzi', $data['touzi'], '', 100);
        include _include(APP_PATH . 'plugin/xl_funding/admin/create.html');
    } else {
        $input['dateline'] = strtotime(param('dateline'));
        $input['name'] = param('name');
        $input['links'] = param('links');
        $input['attribute'] = param('attribute');
        $input['local'] = param('local');
        $input['number'] = param('number');
        $input['money'] = param('money');
        $input['orderid'] = param('orderid');
        $input['touzi'] = param('touzi', '', false);
        if (param('id')) {
            db_update("funding", ["id" => param('id')], $input);
        } else {
            db_insert("funding", $input);
        }
        message("0", jump('保存成功', url('plugin-setting-xl_funding')));
    }
} elseif ($action == 'shangshicreate') {

    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("shangshi", ["id" => param('id')]);
        } else {
            $data = [];
        }

        $data['dateline'] = $data['dateline'] ?: time();
        $input['dateline'] = form_time('dateline', date('Y-m-d', $data['dateline']), '200');
        $input['name'] = form_text('name', $data['name']);
        $input['links'] = form_text('links', $data['links']);
        $input['orderid'] = form_text('orderid', $data['orderid']);
        $input['attribute'] = form_select('attribute', $attribute, $data['attribute']);
        $input['local'] = form_select('local', $sslocal, $data['local']);
        $input['newPrice'] = form_text('newPrice', $data['newPrice']);
        $input['qichacha'] = form_text('qichacha', $data['qichacha']);
        $input['newYinli'] = form_text('newYinli', $data['newYinli']);
        $input['newYear'] = form_text('newYear', $data['newYear']);
        $input['pinpai'] = form_text('pinpai', $data['pinpai']);
        $input['caibao'] = form_text('caibao', $data['caibao']);
        include _include(APP_PATH . 'plugin/xl_funding/admin/shangshicreate.html');
    } else {
        $input['dateline'] = strtotime(param('dateline'));
        $input['name'] = param('name');
        $input['links'] = param('links');
        $input['attribute'] = param('attribute');
        $input['local'] = param('local');
        $input['newPrice'] = param('newPrice');
        $input['qichacha'] = param('qichacha');
        $input['newYinli'] = param('newYinli');
        $input['newYear'] = param('newYear');
        $input['pinpai'] = param('pinpai');
        $input['caibao'] = param('caibao');
        if (param('id')) {
            db_update("shangshi", ["id" => param('id')], $input);
        } else {
            db_insert("shangshi", $input);
        }
        message("0", jump('保存成功', url('plugin-setting-xl_funding-shangshi')));
    }
} elseif ($action == 'dmjcreate') {

    if ($method == 'GET') {
        if (param('id')) {
            $data = db_find_one("damaijia", ["id" => param('id')]);
        } else {
            $data = [];
        }

        $input['name'] = form_text('name', $data['name']);
        $input['name_quancheng'] = form_text('name_quancheng', $data['name_quancheng']);
        $input['links'] = form_text('links', $data['links']);
        $input['local'] = form_select('local', $dmjlocal, $data['local']);
        $input['qichacha'] = form_text('qichacha', $data['qichacha']);
        $input['newYinli'] = form_text('newYinli', $data['newYinli']);
        $input['newNdYinli'] = form_text('newNdYinli', $data['newNdYinli']);
        $input['pinpai'] = form_text('pinpai', $data['pinpai']);
        $input['rongziqingkuang'] = form_text('rongziqingkuang', $data['rongziqingkuang']);
        $input['about_news'] = form_text('about_news', $data['about_news']);
        include _include(APP_PATH . 'plugin/xl_funding/admin/damaijiacreate.html');
    } else {
        $input['name'] = param('name');
        $input['name_quancheng'] = param('name_quancheng');
        $input['links'] = param('links');
        $input['local'] = param('local');
        $input['qichacha'] = param('qichacha');
        $input['newYinli'] = param('newYinli');
        $input['newNdYinli'] = param('newNdYinli');
        $input['pinpai'] = param('pinpai');
        $input['rongziqingkuang'] = param('rongziqingkuang');
        $input['about_news'] = param('about_news');
        if (param('id')) {
            db_update("damaijia", ["id" => param('id')], $input);
        } else {
            db_insert("damaijia", $input);
        }
        message("0", jump('保存成功', url('plugin-setting-xl_funding-damaijia')));
    }
} elseif ($action == 'delete') {
    $type = param('type');
    $id = param('id');
    if ($type == 'funding') {
        db_delete("funding", ["id" => $id]);
        message(0,'ok');
    }elseif ($type == 'shangshi') {
        db_delete("shangshi", ["id" => $id]);
        message(0,'ok');
    }elseif ($type == 'damaijia') {
        db_delete("damaijia", ["id" => $id]);
        message(0,'ok');
    }
}
?>