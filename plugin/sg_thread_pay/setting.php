<?php
/*
	Xiuno BBS 4.0 付费功能设置
	查鸽信息网 http://cha.sgahz.net
*/
!defined('DEBUG') AND exit('Access Denied.');
$action = param(3);

if (empty($action)) {
	
	$kv = setting_get('sg_thread_pay');
	unset($grouplist[0]);
	unset($grouplist[6]);
	unset($grouplist[7]);
	$input = array();
	$input['sg_commission'] = form_text('sg_commission', $kv['sg_commission']);
	$reward5 = arrlist_key_values($forumlist, 'fid', 'name');
	$input['sg_forum']= sg_thread_pay_form_multi_checkbox("sg_forum", $reward5, $kv['sg_forum']);
	$input['sg_type']= sg_thread_pay_form_multi_checkbox("sg_type", array('3'=>'人民币', '2'=>'金币', '1'=>'积分'), $kv['sg_type']);
	$reward2 = arrlist_key_values($grouplist, 'gid', 'name');
	$input['sg_thread_pay']= sg_thread_pay_form_multi_checkbox("sg_thread_pay", $reward2, $kv['sg_thread_pay']);
	$input['sg_attach_pay']= sg_thread_pay_form_multi_checkbox("sg_attach_pay", $reward2, $kv['sg_attach_pay']);

	if($method == 'POST') {
		$kv = array();
		$kv['sg_commission'] = param('sg_commission', 0);
		$kv['sg_forum'] = param('sg_forum', array(0));
		$kv['sg_type'] =param('sg_type', array(0));
		$kv['sg_thread_pay'] =param('sg_thread_pay', array(0));
		$kv['sg_attach_pay'] =param('sg_attach_pay', array(0));
		

		setting_set('sg_thread_pay', $kv);
		message(0, lang('save_successfully'));
	}
	include _include(APP_PATH.'plugin/sg_thread_pay/setting.htm');
	
} elseif ($action == 'list') {
	$page = param(4, 1);
	$pagesize = 10;
	$count = sg_thread_pay_count();
	$pagination = pagination(url("plugin-setting-sg_thread_pay-list-{page}") , $count, $page, $pagesize);
	$thread_paylist = sg_thread_pay_find(array(), array() , $page, $pagesize);
	include _include(APP_PATH.'plugin/sg_thread_pay/list.htm');
} elseif ($action == 'delete') {
	$d_tid = param(4);
	$d_tuid = param(5);
	$r = db_delete('sg_thread_pay', array('tid'=>$d_tid, 'uid'=>$d_tuid));
	message(0, '删除成功');
} elseif ($action == 'truncate') {
	$r = db_truncate('sg_thread_pay');
	message(0, '清空成功');
}
?>