<?php
/*
	Xiuno BBS 4.0 付费主题、付费附件件
	查鸽信息网 http://cha.sgahz.net
*/
function sg_thread_pay_create($arr) {
    $r = db_create('sg_thread_pay', $arr);
    return $r;
}

function sg_thread_pay_update($tid, $uid, $update) {
	$r = db_update('sg_thread_pay', array('tid'=>$tid, 'uid'=>$uid), $update);
	return $r;
}

function sg_thread_pay_read($tid, $uid) {
	$r = db_find_one('sg_thread_pay', array('tid'=>$tid, 'uid'=>$uid));
	return $r;
}

function sg_thread_pay_delete($tid) {
    $r = db_delete('sg_thread_pay', array('tid'=>$tid));
    return $r;
}

function sg_thread_pay_read_by_tid($tid) {
    $r = db_find_one('sg_thread_pay', array('tid' => $tid));
	switch ($r['type']) {
		case '1':
			$r['field'] = 'credits';
			$r['unit'] = '积分';
			break;
		case '2':
			$r['field'] = 'golds';
			$r['unit'] = '金币';
			break;
		case '3':
			$r['field'] = 'rmbs';
			$r['unit'] = '人民币';
			break;
	}
    return $r;
}

function sg_thread_pay_count($cond = array()) {
    $n = db_count('sg_thread_pay', $cond);
    return $n;
}

function sg_thread_pay_find($cond = array() , $orderby = array() , $page = 1, $pagesize = 20) {
    $sg_thread_paylist = db_find('sg_thread_pay', $cond, $orderby, $page, $pagesize);
    if ($sg_thread_paylist) foreach ($sg_thread_paylist as & $card) sg_thread_pay_format($card);
    return $sg_thread_paylist;
}

function sg_thread_pay_format(&$thread_pay) {
	global $time, $gid;
	if (empty($thread_pay)) return;
	$thread = thread__read($thread_pay['tid']);
	$user = user__read($thread_pay['uid']);
	$thread_pay['subject'] = $thread['subject'];
	$thread_pay['username'] = $user['username'];
	$thread_pay['thread'] = $thread_pay['t_create_date'] ? '已买' : '未买';
	$thread_pay['attach'] = $thread_pay['a_create_date'] ? '已买' : '未买';
/* 	if (!empty($thread_pay['use_uid'])) {
		$thread_pay['use_date'] = date("Y-m-d H:i:s", $thread_pay['use_date']);
		$gid == 1 AND $user = user_read_cache($thread_pay['use_uid']);
		$gid == 1 AND $thread_pay['use_username'] = array_value($user, 'username');
		$thread_pay['code'] == '0000' AND $thread_pay['code'] = lang('sg_artificial_recharge');
	} else {
		$thread_pay['expiry_class'] = '';
		if (($thread_pay['expiry_date'] - $time < 0)) {
			$thread_pay['expiry_class'] = empty($thread_pay['expiry_date']) ? "class='text-success'" : "class='text-danger'";
			$thread_pay['expiry_date'] = empty($thread_pay['expiry_date']) ? lang('sg_thread_pay_permanent') : lang('sg_expiry');
		} else {
			$thread_pay['expiry_date'] = date("Y-m-d H:i:s", $thread_pay['expiry_date']);
		}
	} */
}

function sg_thread_pay_type($arr) {
	$new = array();
	foreach($arr as $k=>$v) {
		switch ($k) {
			case '1':
				$new[$k]='积分';
				break;
			case '2':
				$new[$k]= '金币';
				break;
			case '3':
				$new[$k]= '人民币';
				break;
		}
	}
	return $new;
}

function sg_thread_pay_switch_type($type) {
	$r = array();
	switch ($type) {
		case '1':
			$r['field'] = 'credits';
			$r['unit'] = '积分';
			break;
		case '2':
			$r['field'] = 'golds';
			$r['unit'] = '金币';
			break;
		case '3':
			$r['field'] = 'rmbs';
			$r['unit'] = '人民币';
			break;
	}
    return $r;
}

function sg_thread_pay_form_multi_checkbox($name, $arr, $checked = array()) {
	$s = '';
	foreach($arr as $k=>$v) {
		$ischecked = array_key_exists($k, $checked);
		$s .= form_checkbox($name."[$k]", $ischecked, $v).' ';
	}
	return $s;
}

function sg_hide_thread($message) {
    $regex = '/\[sg_hide_thread[^>]*](?<first_message>(?:([\s\S]*)))\[\/sg_hide_thread\]/isU';
 
    if (is_array($message)) {
        $message = '<font class="text-danger">此处有付费内容</font>';
    }
 
    return preg_replace_callback($regex, 'sg_hide_thread', $message);
}

function sg_reveal_thread($message) {
    $regex = '/\[sg_hide_thread[^>]*](?<first_message>(?:([\s\S]*)))\[\/sg_hide_thread\]/isU';
 
    if (is_array($message)) {
        $message = $message[1];
    }
 
    return preg_replace_callback($regex, 'sg_reveal_thread', $message);
}
?>
