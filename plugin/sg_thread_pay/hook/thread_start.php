<?php exit;
$sg_thread_pay = setting_get('sg_thread_pay');

if($action == 'thread_pay'){
	user_login_check();
	$tid = param(2);
	if($method != 'POST') message(0, jump(lang('method_error'), url("thread-$tid")));
	$istype = param(3);
	$pay_uid = sg_thread_pay_read($tid, $uid);
	$thread = thread_read($tid);
	
	if(empty($pay_uid['thread_value']) && $istype && $uid != $thread['uid']) {

		empty($thread['thread_value']) AND message(-1, lang('method_error'));
		$type = sg_thread_pay_switch_type($thread['thread_type']);

		$user[$type['field']] < $thread['thread_value'] AND  message(-1, $type['unit'].'不足，无法购买');

		empty($pay_uid) AND sg_thread_pay_create(array('tid' => $tid, 'uid' => $uid, 't_create_date' => $time, 'thread_type' => $thread['thread_type'], 'thread_value' => $thread['thread_value']));
		!empty($pay_uid) AND sg_thread_pay_update($tid, $uid, array('t_create_date' => $time, 'thread_type' => $thread['thread_type'], 'thread_value' => $thread['thread_value']));
		
		user_update($uid, array($type['field'].'-'=>$thread['thread_value']));
		
		$thread_value = (100 - $sg_thread_pay['sg_commission']) / 100 * $thread['thread_value'];
		
		user_update($thread['uid'], array($type['field'].'+'=>(int)$thread_value));
		
		if (function_exists("notice_send")) {
				
			$notice_message = '<div class="comment-info">购买了你的主题<a href="'.url("thread-$thread[tid]").'">《'.$thread['subject'].'》</a></div><div class="single-comment text-danger">收入 '.(int)$thread_value.$type['unit'].'</div>';

			notice_send($uid, $thread['uid'], $notice_message, 100);
			
			$notice_message = '<div class="comment-info">你购买了主题<a href="'.url("thread-$thread[tid]").'">《'.$thread['subject'].'》</a></div><div class="single-comment text-danger">支付 '.$thread['thread_value'].$type['unit'].'</div>';

			notice_send($thread['uid'], $uid, $notice_message, 3);			
		}
		
		if (function_exists("sg_accounting_create")) {
				
			sg_accounting_create(1, $thread['uid'], $thread['thread_type'], (int)$thread_value, 1, '出售主题<a href="'.url("thread-$thread[tid]").'">《'.$thread['subject'].'》</a>');

			sg_accounting_create(2, $uid, $thread['thread_type'], $thread['thread_value'], 1, '购买主题<a href="'.url("thread-$thread[tid]").'">《'.$thread['subject'].'》</a>');

		}
		
		message(0, '购买成功');
		
	} elseif(empty($pay_uid['attach_value']) && $uid != $thread['uid']) {

		empty($thread['attach_value']) AND message(-1, lang('method_error'));
		$type = sg_thread_pay_switch_type($thread['thread_type']);

		$user[$type['field']] < $thread['attach_value'] AND  message(-1, $type['unit'].'不足，无法购买');

		empty($pay_uid) AND sg_thread_pay_create(array('tid' => $tid, 'uid' => $uid, 'a_create_date' => $time, 'attach_type' => $thread['thread_type'], 'attach_value' => $thread['attach_value']));
		!empty($pay_uid) AND sg_thread_pay_update($tid, $uid, array('a_create_date' => $time, 'attach_type' => $thread['thread_type'], 'attach_value' => $thread['attach_value']));
		
		user_update($uid, array($type['field'].'-'=>$thread['attach_value']));
		$attach_value = (100 - $sg_thread_pay['sg_commission']) / 100 * $thread['attach_value'];
		user_update($thread['uid'], array($type['field'].'+'=>(int)$attach_value));
		
		if (function_exists("notice_send")) {
				
			$notice_message = '<div class="comment-info">购买了你的附件<a href="'.url("thread-$thread[tid]").'">《'.$thread['subject'].'》</a></div><div class="single-comment text-danger">收入 '.(int)$attach_value.$type['unit'].'</div>';

			notice_send($uid, $thread['uid'], $notice_message, 100);
			
			$notice_message = '<div class="comment-info">你购买了附件<a href="'.url("thread-$thread[tid]").'">《'.$thread['subject'].'》</a></div><div class="single-comment text-danger">支付 '.$thread['attach_value'].$type['unit'].'</div>';

			notice_send($thread['uid'], $uid, $notice_message, 3);			
		}
		
		if (function_exists("sg_accounting_create")) {
				
			sg_accounting_create(1, $thread['uid'], $thread['thread_type'], (int)$attach_value, 1, '出售附件<a href="'.url("thread-$thread[tid]").'">《'.$thread['subject'].'》</a>');

			sg_accounting_create(2, $uid, $thread['thread_type'], $thread['attach_value'], 1, '购买附件<a href="'.url("thread-$thread[tid]").'">《'.$thread['subject'].'》</a>');

		}
		
		message(0, '购买成功');
		
	} else {
		message(-1, jump(lang('method_error'), url("thread-$tid")));
	}
}
?>