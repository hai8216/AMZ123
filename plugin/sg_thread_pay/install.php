<?php
/*
	Xiuno BBS 4.0 付费主题、付费附件
	查鸽信息网 http://cha.sgahz.net
*/

!defined('DEBUG') AND exit('Forbidden');
$tablepre = $db->tablepre;
$sql = "CREATE TABLE IF NOT EXISTS {$tablepre}sg_thread_pay (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'tid',
  `uid` int(10) unsigned NOT NULL default '0' COMMENT '用户',
  `t_create_date` int(15) unsigned NOT NULL DEFAULT '0' COMMENT '主题付费时间',
  `a_create_date` int(15) unsigned NOT NULL DEFAULT '0' COMMENT '附件付费时间',
  `thread_type` tinyint(3) unsigned NOT NULL default '0' COMMENT '主题支付类型',
  `thread_value` int(10) unsigned NOT NULL default '0' COMMENT '主题金额', 
  `attach_type` tinyint(3) unsigned NOT NULL default '0' COMMENT '附件支付类型',
  `attach_value` int(10) unsigned NOT NULL default '0' COMMENT '附件金额', 
  PRIMARY KEY (`tid`, `uid`),
  KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
$r = db_exec($sql);
// 主题支付类型
$sql = "ALTER TABLE {$tablepre}thread ADD COLUMN thread_type tinyint(3) unsigned NOT NULL default '0';";
db_exec($sql);
// 主题支付金额
$sql = "ALTER TABLE {$tablepre}thread ADD COLUMN thread_value int(10) unsigned NOT NULL default '0';";
db_exec($sql);
// 附件支付金额
$sql = "ALTER TABLE {$tablepre}thread ADD COLUMN attach_value int(10) unsigned NOT NULL default '0';";
db_exec($sql);

$kv = array('sg_commission'=>'10', 'sg_forum'=>array('1'=>1), 'sg_type'=>array('3'=>1,'2'=>1), 'sg_thread_pay'=> array('1'=>1,'2'=>1,'4'=>1,'5'=>1,'101'=>1,'102'=>1,'103'=>1,'104'=>1,'105'=>1), 'sg_attach_pay'=> array('1'=>1,'2'=>1,'4'=>1,'5'=>1,'101'=>1,'102'=>1,'103'=>1,'104'=>1,'105'=>1));
setting_set('sg_thread_pay', $kv);

?>