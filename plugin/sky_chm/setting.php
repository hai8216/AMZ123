<?php
!defined('DEBUG') AND exit('Access Denied.');

$action = param(3);
if($action == '') {
    if($method == 'GET') {
        $lists = sky_chm_category__find();
        include _include(APP_PATH.'plugin/sky_chm/view/htm/category.htm');
    }
} elseif($action == 'addcategory'){
    $id = param(4);
    if($method == 'GET') {
        $datas = array();
        $id && $datas = sky_chm_category__read($id);
        $name = isset($datas['name']) ? $datas['name'] : '';
        $is_menu = isset($datas['is_menu']) ? $datas['is_menu'] : 0;
        $desc = isset($datas['desc']) ? $datas['desc'] : '';
        $keywords = isset($datas['keywords']) ? $datas['keywords'] : '';
        $description = isset($datas['description']) ? $datas['description'] : '';
        $input = array();
        $input['name'] = form_text('name', $name);
        $input['is_menu'] = form_radio_yes_no('is_menu', $is_menu);
        $input['desc'] = form_textarea('desc', $desc);
        $input['keywords'] = form_textarea('keywords', $keywords);
        $input['description'] = form_textarea('description', $description);
        include _include(APP_PATH.'plugin/sky_chm/view/htm/addcategory.htm');
    } else {
        $data = $_POST;
        if($id) {
            sky_chm_category__update($id, $data);
        } else {
            sky_chm_category__create($data);
        }
        message(0, '操作成功');
    }
} elseif($action == 'deletecategory') {
    $id = param(4);
    sky_chm_category__delete($id);
    $nodes = sky_chm__find(array('cid'=>$id));
    if($nodes) {
        foreach ($nodes as $node) {
            sky_chm_thread__delete_cond(array('node_id'=>$node['id']));
        }
    }
    sky_chm__delete_cond(array('cid'=>$id));
    message(0, '操作成功<script>setTimeout(function(){history.go(-1);},1000)</script>');
} elseif($action == 'node'){
    $cid = param(4);
    $lists = sky_chm__find(array('cid'=>$cid), array('sort'=>1));
    $lists = sky_format_admin($lists);
    $lists = json_encode($lists);
    include _include(APP_PATH.'plugin/sky_chm/view/htm/node.htm');
} else if($action == 'add') {
    $name = param('name');
    $pid = param('pid');
    $cid = param('cid');
    if(!$pid) {
        $arr['name'] = $name;
        $arr['cid'] = $cid;
        $arr['sort'] = 99999;
        $id = sky_chm__create($arr);
        message(0, $id);
    } else {
        $arr['pId'] = $pid;
        $arr['name'] = $name;
        $arr['cid'] = $cid;
        $arr['sort'] = 99999;
        $id = sky_chm__create($arr);
        message(0, $id);
    }
} else if($action == 'edit') {
    $name = param('name');
    $id = param('id');
    sky_chm__update($id, array('name'=>$name));
    message(0, $id);
} else if($action == 'delete') {
    $id = param('id');
    if($id) {
        sky_chm__delete($id);
        sky_chm_thread__delete_cond(array('node_id'=>$id));
        message(0, '删除成功');
    }
} else if($action == 'import') {
    $cid = (int)param(4);
    $node_id = (int)param(5);
    $page = (int)param(6, 1);
    $fid = (int)param('fid');
    $keyword = param('keyword');
    $pagesize = 10;
    if($method == 'GET') {
        $cond = array();
        if($fid) {
            $cond['fid'] = $fid;
        }
        if($keyword) {
            $cond['subject'] = array('LIKE'=>$keyword);
        }
        $forms = forum__find();
        $total = thread_count($cond);
        $lists = thread__find($cond, array('tid'=>-1), $page, $pagesize);
        $pagination = pagination(url("plugin-setting-sky_chm-import-{$cid}-{$node_id}-{page}").'?fid='.$fid.'&keyword='.$keyword, $total, $page, $pagesize);
        include _include(APP_PATH.'plugin/sky_chm/view/htm/import.htm');
    } else {
        $data = $_POST;
        if(isset($data['ids']) && !empty($data['ids'])) {
            foreach ($data['ids'] as $val) {
                $arr = array();
                $arr['tid'] = (int)$val;
                $arr['node_id'] = $node_id;
                sky_chm_thread__create($arr);
            }
            message(0, '操作成功');
        }
        message(-1, '请选择操作的数据');
    }
} else if($action == 'threadmange') {
    $node_id = (int)param(4);
    $page = (int)param(5, 1);
    $pagesize = 10;
    if($method == 'GET') {
        $lists = sky_chm_thread__find(array('node_id'=>$node_id),array('order_id'=>'-1'));
        $tids = array();
        $total = sky_chm_thread_count(array('node_id'=>$node_id));
        $pagination = pagination(url("plugin-setting-sky_chm-threadmange-{$node_id}-{page}"), $total, $page, $pagesize);
        include _include(APP_PATH.'plugin/sky_chm/view/htm/threadmange.htm');
    } else {
        $data = $_POST;
        if(isset($data['ids']) && !empty($data['ids'])) {
            foreach ($data['ids'] as $val) {
                $arr = array();
                $arr['tid'] = (int)$val;
                $arr['node_id'] = $node_id;
                sky_chm_thread__delete_cond($arr);
            }
            message(0, '操作成功');
        }
        message(-1, '请选择操作的数据');
    }
} elseif($action == 'sort') {
    $data = $_POST;
    if($data && $data['nodes']) {
        $nodes = $data['nodes'];
        foreach ($nodes as $val) {
            $arr = array();
            $arr['pId'] = $val['pId'];
            $arr['name'] = $val['name'];
            $arr['sort'] = $val['sort'];
            sky_chm__update($val['id'], $arr);
        }
        message(0, '操作成功');
    }
    message(-1, '操作失败');
}elseif($action == 'editorder') {
    if($method == 'GET') {
        $id = param(4);
//        echo $id;exit;
        $val = db_find_one('sky_chm_thread',array('id'=>$id));
//        print_r($val);exit;
        include _include(APP_PATH.'plugin/sky_chm/view/htm/editorder.htm');
    }else{
        db_update('sky_chm_thread',array('id'=>param('id')),array('order_id'=>param('order_id')));
        message(0, '操作成功');
    }
}




?>