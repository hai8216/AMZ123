<?php
/**
 *
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */

$sky_hide_footer = 'hide';
$action = param(1);
if($action == 'index') {
    $cid = param(2)?:1;
    $lists = sky_chm__find(array('cid'=>$cid), array('sort'=>1));
//    $lists = db_find('sky_chm', $cond, $orderby, $page, $pagesize);
    $lists = sky_format($lists);
    $lists = json_encode($lists);
    $hinfo = db_find_one('sky_chm_category',array('id'=>$cid));
    $header['title'] = $hinfo['name'];
    $header['keywords'] = $hinfo['keywords'];
    $header['description'] = $hinfo['description'];
    include _include(APP_PATH.'plugin/sky_chm/view/htm/chm.htm');
} else if($action == 'child') {
    $id = param('id');
    $cid = param('cid');
    $datas = sky_chm_thread__find(array('node_id'=>$id),array('order_id'=>'-1'));
    $arr = array();
    if($datas) {
        foreach ($datas as $key=>$val) {
            $arr[$key]['id'] = $val['tid'];
            $arr[$key]['pId'] = $id;
            $arr[$key]['name'] = $val['subject'];
            $arr[$key]['icon'] = '/plugin/sky_chm/view/tree/img/dhtmlgoodies_sheet.gif';
            $arr[$key]['url'] = url('chm-thread-'.$val['tid']);
            $arr[$key]['target'] = 'threadFrame';
            $arr[$key]['isParent'] = false;
        }
    }
    echo json_encode($arr);
} else if($action == 'thread') {
    $tid = param(2);
    $page = 1;
    $pagesize = $conf['postlist_pagesize'];
    $thread = thread_read($tid);
    empty($thread) AND message(-1, lang('thread_not_exists'));
    $fid = $thread['fid'];
    $forum = forum_read($fid);
    empty($forum) AND message(3, lang('forum_not_exists'));
    $postlist = post_find_by_tid($tid, $page, $pagesize);
    empty($postlist) AND message(4, lang('post_not_exists'));
    if($page == 1) {
        empty($postlist[$thread['firstpid']]) AND message(-1, lang('data_malformation'));
        $first = $postlist[$thread['firstpid']];
        unset($postlist[$thread['firstpid']]);
        $attachlist = $imagelist = $filelist = array();
        thread_inc_views($tid);
    } else {
        $first = post_read($thread['firstpid']);
    }
    $keywordurl = '';
    $allowpost = forum_access_user($fid, $gid, 'allowpost') ? 1 : 0;
    $allowupdate = forum_access_mod($fid, $gid, 'allowupdate') ? 1 : 0;
    $allowdelete = forum_access_mod($fid, $gid, 'allowdelete') ? 1 : 0;
    forum_access_user($fid, $gid, 'allowread') OR message(-1, lang('user_group_insufficient_privilege'));
    $pagination = pagination(url("thread-$tid-{page}$keywordurl"), $thread['posts'] + 1, $page, $pagesize);



    // hook thread_info_end.php






    include _include(APP_PATH.'plugin/sky_chm/view/htm/thread.htm');
} else if($action == 'default') {
    $cid = param(2);
    $category = sky_chm_category__read($cid);
    include _include(APP_PATH.'plugin/sky_chm/view/htm/default.htm');
} elseif ($action == 'cidnode') {
    $cid = param('cid');
    $cond = array();
    if($cid) {
        $cond['cid'] = $cid;
    }
    $lists = sky_chm__find($cond, array('sort'=>1));
    $lists = sky_format_admin($lists);
    $lists = json_encode($lists);
    echo $lists;
}
?>