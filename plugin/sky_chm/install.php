<?php
$tablepre = $db->tablepre;
$sql = "CREATE TABLE IF NOT EXISTS `{$tablepre}sky_chm_thread` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL DEFAULT '0',
  `tid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
$r = db_exec($sql);

$sql = "CREATE TABLE IF NOT EXISTS `{$tablepre}sky_chm_category` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `desc` text NOT NULL,
  `is_menu` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
$r = db_exec($sql);

$sql = "CREATE TABLE IF NOT EXISTS `{$tablepre}sky_chm` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pId` int(11) NOT NULL DEFAULT '0',
  `cid` smallint(5) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
$r = db_exec($sql);

?>