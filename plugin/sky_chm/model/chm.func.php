<?php
/**
 *
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */

function sky_chm__create($arr) {
    $r = db_create('sky_chm', $arr);
    return $r;
}

function sky_chm__update($id, $arr) {
    $r = db_update('sky_chm', array('id'=>$id), $arr);
    return $r;
}

function sky_chm__read($id) {
    $data = db_find_one('sky_chm', array('id'=>$id));
    return $data;
}

function sky_chm__delete($id) {
    $r = db_delete('sky_chm', array('id'=>$id));
    return $r;
}

function sky_chm__find($cond = array(), $orderby = array(), $page = 1, $pagesize = 2000000) {
    $lists = db_find('sky_chm', $cond, $orderby, $page, $pagesize);
    return $lists;
}

function sky_chm_count($cond = array()) {
    $n = db_count('sky_chm', $cond);
    return $n;
}

function sky_chm__delete_cond($cond) {
    $r = db_delete('sky_chm', $cond);
    return $r;
}


function sky_chm__read_cond($cond) {
    $data = db_find_one('sky_chm', $cond);
    return $data;
}

function sky_format_admin($lists) {
    if(!$lists) {
        return array();
    }
    foreach ($lists as &$val) {
        $is_parent = sky_chm_count(array('pid'=>$val['id']));
        if($is_parent > 0) {
            $val['iconClose'] = '/plugin/sky_chm/view/tree/img/dhtmlgoodies_folder.gif';
            $val['iconOpen'] = '/plugin/sky_chm/view/tree/img/dhtmlgoodies_folderopen.gif';
        } else {
            $val['icon'] = '/plugin/sky_chm/view/tree/img/dhtmlgoodies_sheet.gif';
        }
    }
    return $lists;
}

function sky_format($lists) {
    foreach ($lists as &$val) {
        $val['iconClose'] = '/plugin/sky_chm/view/tree/img/dhtmlgoodies_folder.gif';
        $val['iconOpen'] = '/plugin/sky_chm/view/tree/img/dhtmlgoodies_folderopen.gif';
        $val['isParent'] = true;
        $val['open'] = true;
    }
    return $lists;
}

?>