<?php
/**
 *
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */

function sky_chm_thread__create($arr) {
    $r = db_create('sky_chm_thread', $arr);
    return $r;
}

function sky_chm_thread__update($id, $arr) {
    $r = db_update('sky_chm_thread', array('id'=>$id), $arr);
    return $r;
}

function sky_chm_thread__read($id) {
    $data = db_find_one('sky_chm_thread', array('id'=>$id));
    return $data;
}

function sky_chm_thread__delete($id) {
    $r = db_delete('sky_chm_thread', array('id'=>$id));
    return $r;
}

function sky_chm_thread__find($cond = array(), $orderby = array(), $page = 1, $pagesize = 2000000) {
    $lists = db_find('sky_chm_thread', $cond, $orderby, $page, $pagesize);
    return $lists;
}

function sky_chm_thread_count($cond = array()) {
    $n = db_count('sky_chm_thread', $cond);
    return $n;
}

function sky_chm_thread__delete_cond($cond) {
    $r = db_delete('sky_chm_thread', $cond);
    return $r;
}


function sky_chm_thread__read_cond($cond) {
    $data = db_find_one('sky_chm_thread', $cond);
    return $data;
}

?>