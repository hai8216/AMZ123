<?php
!defined('DEBUG') AND exit('Forbidden');

$tablepre = $db->tablepre;
$sql = "DROP TABLE IF EXISTS {$tablepre}sky_chm_thread;";
$r = db_exec($sql);

$sql = "DROP TABLE IF EXISTS {$tablepre}sky_chm_category;";
$r = db_exec($sql);

$sql = "DROP TABLE IF EXISTS {$tablepre}sky_chm;";
$r = db_exec($sql);

$r === FALSE AND message(-1, '卸载表失败');

?>