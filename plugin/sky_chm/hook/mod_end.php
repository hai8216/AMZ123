<?php exit;
$sky = 'sky';
if($action == 'pushchm') {
    if($method == 'GET') {
        $categorys = sky_chm_category__find();
        $category = array();
        foreach($categorys as $val) {
            $category[$val['id']] = $val['name'];
        }
        include _include(APP_PATH.'plugin/sky_chm/view/htm/mod_pushchm.htm');
    } else {
        $tidarr = param('tidarr', array(0));
        $nodearr = param('nodearr', array(0));
        empty($tidarr) AND message(-1, lang('please_choose_thread'));
        empty($nodearr) AND message(-1, '请选择节点');
        foreach ($nodearr as $nodeid) {
            foreach ($tidarr as $tid) {
                $arr = array();
                $arr['node_id'] = $nodeid;
                $arr['tid'] = $tid;
                $res = sky_chm_thread__read_cond($arr);
                if(!$res) {
                    sky_chm_thread__create($arr);
                }
            }
        }
        message(0, '操作成功');
    }
}

?>