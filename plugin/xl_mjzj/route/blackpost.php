<?php
require_once(APP_PATH . "/plugin/xl_aliopensearch/Config.inc.php");

use OpenSearch\Client\DocumentClient;

$ip = ip();
$longip = ip2long($ip);
$longip < 0 AND $longip = sprintf("%u", $longip); // fix 32 位 OS 下溢出的问题
$id = param('id');
$ucount = db_count('user', array('ask_type' => 1));
$pageall = rand(0, intval(ceil($ucount / 5)) - 1);
$postuid = db_find('user', array('ask_type' => 1), array(), $pageall, 5)[0];
$sid = '';
if ($id) {
    $one = db_find_one("mjzj_data", array('id' => $id));
    if ($one['status'] == '1') {
        message(0, '已发布');
        exit;
    }

    $subject = "";
    $thread = array(
        'fid' => 3,
        'uid' => $postuid['uid'],
        'sid' => $sid,
        'subject' => $subject,
        'message' => $one['reason'],
        'time' => time(),
        'longip' => $longip,
        'doctype' => 0,
    );
    $tid = thread_create($thread, $pid);
    $pid === FALSE AND message(-1, lang('create_post_failed'));
    $tid === FALSE AND message(-1, lang('create_thread_failed'));
    if ($tid) {
        $newonw = array(
            'ax_name' => $one['name'],
            'ax_country' => $one['country'],
            'ax_email' => $one['email'],
            'ax_weixin' => $one['wechat'],
            'ax_ymx' => $one['amz_url'],
            'ax_facebook' => $one['facebook_url']
        );
        db_update("thread", array('tid' => $tid), $newonw);
        db_update("mjzj_data", array('id' => $id), array('status' => 1));
        $user = user_read($postuid['uid']);
        $tableName = 'bbs_post';
        $smsg = strip_tags($one['reason']);
        $documentClient = new DocumentClient($client);
        $docs_to_upload = array();
        $item['cmd'] = 'ADD';
        $item["fields"] = array(
            'pid' => $pid,
            'tid' => $tid,
            'isfirst' => 1,
            'message' => str_replace("&nbsp;", " ", $smsg),
            'author' => $user['username'],
            'authorid' => $postuid['uid'],
            'fid' => 3,
            'fname' => '测评黑名单',
            'back_message' => '',
            'posttime' => time(),
        );
        $item["fields"]['ax_name'] = $one['name'];
        $item["fields"]['ax_email'] = $one['email'];
        $item["fields"]['ax_weixin'] = $one['wechat'];
        $item["fields"]['ax_ymx'] = $one['amz_url'];
        $item["fields"]['ax_facebook'] = $one['facebook_url'];
        $item["fields"]['ax_country'] = $one['country'];
        $item["fields"]['subject'] = $one['name'] . "&emsp;" . $one['country'] . "&emsp;" . $one['email'] . "&emsp;";
        $docs_to_upload[] = $item;
        $json = json_encode($docs_to_upload);
        $ret = $documentClient->push($json, $appName, $tableName);
    }
} else {
    //随机发布，根据事件
    $time = time();
    $h = date('H', $time);
    if (intval($h) >= 9 && intval($h) < 17) {
        randpost(2, 4);
    } elseif (intval($h) >= 17) {
        randpost(1, 1);
    } elseif (intval($h) >= 0 && intval($h) < 9) {
//        randpost(1, 3);
    }
}

function randpost($a, $b)
{
    global $client,$appName;
    $postnum = rand($a, $b);
    for ($i = 0; $i < $postnum; $i++) {
        $ucount = db_count('user', array('ask_type' => 1));
        $pageall = rand(0, $ucount);
        $postuid = db_find('user', array('ask_type' => 1), array(), $pageall, 1)[0];
        //获取随机帖子
        $tcount =  db_count('mjzj_data', array('status' => 0));
        $randpage = rand(1,$tcount);
        $one = db_find("mjzj_data", array('status' => 0),array(),$randpage,1)[0];
        if(!$one){
            exit;
        }
        $thread = array(
            'fid' => 3,
            'uid' => $postuid['uid'],
            'sid' => "",
            'subject' => "",
            'message' => $one['reason'],
            'time' => time(),
            'longip' => "",
            'doctype' => 0,
        );
        $tid = thread_create($thread, $pid);
        $pid === FALSE AND message(-1, lang('create_post_failed'));
        $tid === FALSE AND message(-1, lang('create_thread_failed'));
        if ($tid) {
            $newonw = array(
                'ax_name' => $one['name'],
                'ax_country' => $one['country'],
                'ax_email' => $one['email'],
                'ax_weixin' => $one['wechat'],
                'ax_ymx' => $one['amz_url'],
                'ax_facebook' => $one['facebook_url']
            );
            db_update("thread", array('tid' => $tid), $newonw);
            db_update("mjzj_data", array('id' => $one['id']), array('status' => 1));
            $user = user_read($postuid['uid']);
            $tableName = 'bbs_post';
            $smsg = strip_tags($one['reason']);
            $documentClient = new DocumentClient($client);
            $docs_to_upload = array();
            $item['cmd'] = 'ADD';
            $item["fields"] = array(
                'pid' => $pid,
                'tid' => $tid,
                'isfirst' => 1,
                'message' => str_replace("&nbsp;", " ", $smsg),
                'author' => $user['username'],
                'authorid' => $postuid['uid'],
                'fid' => 3,
                'fname' => '测评黑名单',
                'back_message' => '',
                'posttime' => time(),
            );
            $item["fields"]['ax_name'] = $one['name'];
            $item["fields"]['ax_email'] = $one['email'];
            $item["fields"]['ax_weixin'] = $one['wechat'];
            $item["fields"]['ax_ymx'] = $one['amz_url'];
            $item["fields"]['ax_facebook'] = $one['facebook_url'];
            $item["fields"]['ax_country'] = $one['country'];
            $item["fields"]['subject'] = $one['name'] . "&emsp;" . $one['country'] . "&emsp;" . $one['email'] . "&emsp;";
            $docs_to_upload[] = $item;
            $json = json_encode($docs_to_upload);
            $ret = $documentClient->push($json, $appName, $tableName);
        }
    }

}
