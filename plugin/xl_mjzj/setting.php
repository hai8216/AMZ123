<?php
/**
 * Created by xuxiaolong君临天下.
 * User: xuxiaolong Weibo:@sinnamfyoyo
 * Date: 2018/9/1
 * Time: 13:12
 */
!defined('DEBUG') AND exit('Access Denied.');
if (param('do') == 'caiji' || param('do')=='') {
    $page = param(3, 1);
    $pagesize = 40;
    $keywords = trim(param('keyword'));
    if($keywords){
        $n = db_count('mjzj_data', array('reason'=>array('LIKE'=>$keywords)));
        $pagination = pagination(url("plugin-setting-xl_mjzj-{page}",array('do'=>'caiji','keyword'=>$keywords)), $n, $page, $pagesize);
        $pager = pager(url("plugin-setting-xl_mjzj-{page}",array('do'=>'caiji','keyword'=>$keywords)), $n, $page, $pagesize);
        $speciallist = db_find('mjzj_data',  array('reason'=>array('LIKE'=>$keywords)), array('status'=>1), $page, $pagesize);
    }else{
        $n = db_count('mjzj_data', array());
        $pagination = pagination(url("plugin-setting-xl_mjzj-{page}",array('do'=>'caiji')), $n, $page, $pagesize);
        $pager = pager(url("plugin-setting-xl_mjzj-{page}",array('do'=>'caiji')), $n, $page, $pagesize);
        $speciallist = db_find('mjzj_data',  array(), array('status'=>1), $page, $pagesize);
    }


    include _include(APP_PATH . "/plugin/xl_mjzj/view/caiji.htm");
}elseif (param('do') == 'caijipl') {
    $page = param(3, 1);
    $pagesize = 40;
    $n = db_count('thread_post_tmp', array('tmpid'=>param('tmpid')));
    $pagination = pagination(url("plugin-setting-xl_mjzj-{page}",array('do'=>'caijipl','tmpid'=>param('tmpid'))), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_mjzj-{page}",array('do'=>'caijipl','tmpid'=>param('tmpid'))), $n, $page, $pagesize);
    $speciallist = db_find('thread_post_tmp',  array('tmpid'=>param('tmpid')), array(), $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_mjzj/view/caijipl.htm");
}elseif(param('do')=='delcj'){
    db_delete("mjzj_data",array('id'=>param('id')));
    message(0,'ok');
}else {
    $page = param(3, 1);
    $pagesize = 40;
    $n = db_count('user', array('ask_type'=>1));
    $pagination = pagination(url("plugin-setting-xl_mjzj-{page}"), $n, $page, $pagesize);
    $pager = pager(url("plugin-setting-xl_mjzj-{page}"), $n, $page, $pagesize);
    $speciallist = db_find('user',  array('ask_type'=>1), array(), $page, $pagesize);
    include _include(APP_PATH . "/plugin/xl_mjzj/view/list.htm");
}