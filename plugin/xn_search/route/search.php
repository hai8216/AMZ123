<?php
!defined('DEBUG') AND exit('Forbidden');
$mod = param('mod');
$json = param('json');
$searchlist = array();
require_once(APP_PATH . "plugin/xl_aliopensearch/Config.inc.php");

use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;
use OpenSearch\Client\SuggestClient;
use OpenSearch\Util\SuggestParamsBuilder;

if ($mod == 'ajax') {
    $suggestClient = new SuggestClient($client);
    $params = SuggestParamsBuilder::build("AMZ123_PRO", "bbs_post", '1', 10);

//执行查询并返回下拉提示信息
    $ret = $suggestClient->execute($params);
//打印返回消息
    print_r(json_decode($ret->result, true));
} else {


    $keyword = param('keyword');

    empty($keyword) AND $keyword = param(1);

    $keyword = urldecode($keyword);

    if (strpos($keyword, "https://www") || strpos($keyword, "http://www")) {
        $keyword = str_replace("https://www", "", $keyword);
        $keyword = str_replace("http://www", "", $keyword);
    }
    if (strpos($keyword, "www.facebook")) {
        $keyword = str_replace("www.facebook", "facebook", $keyword);
    }
    $range = param(2, 1);
    $page = param(3, 1);

    $keyword_decode = search_keyword_safe(xn_urldecode($keyword));
    $keyword_arr = explode(' ', $keyword_decode);
    $threadlist = $postlist = array();
    $pagination = '';
    $active = '';


    $search_conf = kv_get('search_conf');
    $search_type = $search_conf['type'];
    $search_range = $search_conf['range'];

//$search_type = 'fulltext';

    $pagesize = 20;;

    if ($keyword) {
// 实例化一个搜索类
        $searchClient = new SearchClient($client);
// 实例化一个搜索参数类
        $params = new SearchParamsBuilder();
        if ($json==1 && $_GET['filter']) {
            $params->setFilter('fid='.intval($_GET['fid']));
        }else{
//            $params->setFilter('tid<');
//            $params->setFilter('');
            if(param('fid')){
                $params->addFilter('fid='.intval(param('fid')));

            }


        }
//设置config子句的start值
        $params->setStart(0);




//设置config子句的hit值
        $params->setHits(100);
// 指定一个应用用于搜索
        $params->setAppName('AMZ123_PRO');
// 指定搜索关键词
        $params->setQuery($keyword_decode);
// 指定返回的搜索结果的格式为json
        $params->setFormat("fulljson");
//添加排序字段
//        $params->addSort('pid', SearchParamsBuilder::SORT_DECREASE);
// 执行搜索，获取搜索结果
        $ret = $searchClient->execute($params->build());
//        print_r($ret);exit;
        $searchlist = json_decode($ret->result, true);
        $searchlist = $searchlist['result']['items'];

        foreach ($searchlist as &$vs) {
            //标红
            $vs['fields']['ax_ymx'] = str_replace($keyword_decode, "<em>" . $keyword_decode . "</em>", $vs['fields']['ax_ymx']);
            $vs['fields']['ax_facebook'] = str_replace($keyword_decode, "<em>" . $keyword_decode . "</em>", $vs['fields']['ax_facebook']);
        }
    }
    if ($uid) {
        //存入搜索记录
        $userlog = array(
            'uid' => $uid,
            'username' => $user['username'],
            'keywords' => $keyword_decode,
            'dateline' => time(),
        );

        db_insert('admincp_search_userlog', $userlog);
        //查询状态值
        $iskey = db_find_one('admincp_search_log', array('keywords' => $keyword_decode));
        if ($iskey) {
            //更次数
            db_update('admincp_search_log', array('keywords' => $keyword_decode), array('count+' => 1));
        } else {
            $userlog = array(
                'keywords' => $keyword_decode,
                'count' => 1,
            );
            db_insert('admincp_search_log', $userlog);
        }
    }
    if ($json == 1) {
        if ($searchlist[0]['fields']) {
            foreach ($searchlist as &$v) {
                //处理HTML形式标签
                $v['fields']['subject'] = str_replace("&emsp;", " ", $v['fields']['subject']);
                $v['fields']['subject'] = str_replace("<em>", "", $v['fields']['subject']);
                $v['fields']['subject'] = str_replace("</em>", "", $v['fields']['subject']);
                $v['fields']['subject'] = str_replace("emsp;", "", $v['fields']['subject']);
                $v['fields']['message'] = str_replace("&emsp;", " ", $v['fields']['message']);
                $v['fields']['message'] = str_replace("<em>", "", $v['fields']['message']);
                $v['fields']['message'] = str_replace("</em>", "", $v['fields']['message']);
                $v['fields']['message'] = str_replace("emsp;", "", $v['fields']['message']);
            };
        }
        echo json_encode(array(
            'errCode' => 200,
            'errMessage' => 'success',
            'data' => $searchlist
        ));
        exit;
    } else {
        $header['title'] = $keyword_decode."-".$header['title'];
        $header['keywords'] = $keyword_decode."文章,".$keyword_decode."资讯,".$keyword_decode."话题";
        $header['description'] = 'AMZ123跨境导航为您精选最新'.$keyword_decode.'文章、'.$keyword_decode.'资讯、'.$keyword_decode.'话题，做跨境电商，就上AMZ123！';
        include _include(APP_PATH . 'plugin/xn_search/htm/search.htm');
    }
}

?>