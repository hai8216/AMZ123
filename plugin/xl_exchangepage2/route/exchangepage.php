<?php

$clist = array(
    array(
        'i' => 'CNY',
        'name' => '中国',
        'fh' => '¥'
    ),
    array(
        'i' => 'TWD',
        'name' => '台湾',
        'fh' => 'NT$'
    ),
    array(
        'i' => 'MYR',
        'name' => '马来西亚',
        'fh' => 'RM'
    ),
    array(
        'i' => 'IDR',
        'name' => '印尼',
        'fh' => 'Rp'
    ),
    array(
        'i' => 'THB',
        'name' => '泰国',
        'fh' => '฿'
    ),

    array(
        'i' => 'PHP',
        'name' => '菲律宾',
        'fh' => '₱'
    ),
    array(
        'i' => 'VND',
        'name' => '越南',
        'fh' => '₫'
    ),


    array(
        'i' => 'SGD',
        'name' => '新加坡',
        'fh' => 'S$'
    ),
    array(
        'i' => 'MXN',
        'name' => '墨西哥',
        'fh' => 'Mex$'
    ),


);
$hdata = db_find_one("exchange_2", array('tw'=>array('>'=>0),'xjp'=>array('>'=>0)), array('id' => '-1'));

$hlist['TWD'] = $hdata['tw'];
$hlist['MYR'] = $hdata['mlxy'];
$hlist['IDR'] = $hdata['yin'];
$hlist['THB'] = $hdata['tz'];
$hlist['PHP'] = $hdata['flb'];
$hlist['VND'] = $hdata['yn'];
$hlist['CNY'] = 1;
$hlist['SGD'] =  $hdata['xjp'];
$hlist['MXN'] =  $hdata['mxg'];
$Hjson = json_encode($hlist);
$header['title'] = "东南亚汇率换算 - ".$header['title'];
include _include(APP_PATH . 'plugin/xl_exchangepage2/exchangepage.htm');