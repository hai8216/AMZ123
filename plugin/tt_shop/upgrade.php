<?php !defined('DEBUG') AND exit('Forbidden');
$tablepre = $db->tablepre;
//20180921 ADD CARD_ALLOW
$sql = "ALTER TABLE {$tablepre}group ADD COLUMN allowcard INT(8) DEFAULT '0'";
db_exec($sql);
//20180922 ADD_Setting
setting_set('tt_shop',array('show_seller'=>1,'show_thread'=>0,'edit_sync'=>1));
//20180924 ADD Cate
$sql="CREATE TABLE IF NOT EXISTS `{$tablepre}shop_cate` (
  `cate_id` INT(20) NOT NULL AUTO_INCREMENT,
  `rank` INT(20) NOT NULL DEFAULT '0',
  `name` CHAR(20) NOT NULL DEFAULT '0',
  `icon` CHAR(30) NOT NULL DEFAULT 'clock-o',
  `num` INT(20) NOT NULL DEFAULT '0',
  `parent` INT(20) NOT NULL DEFAULT '0',
  `time` INT(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (card_id),
	KEY (card_id),
	UNIQUE KEY (card_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}shop_list ADD COLUMN cate INT(10) DEFAULT '0'";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}shop_order ADD COLUMN cate INT(10) DEFAULT '0'";
db_exec($sql);
?>