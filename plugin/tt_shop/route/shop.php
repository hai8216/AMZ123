<?php !defined('DEBUG') AND exit('Access Denied.');
$action = param(1,'');
if($action=='') $action='list';
$data = param(2,'-1');
$set_shop = setting_get('tt_shop');
$shop_cate_list = shop_cate_list_get();
$special_forum = db_find_one('forum',array('name'=>'^商城$'));
if($action == 'create'){
	user_login_check();
    if($group['allowsell']!='1') return message(-1,'您所在的用户组无权出售主题！');
    $template_list = shop_template_list();
    if($method=='GET') {
        $header['title'] = $form_title = '导入商品';
        $form_action = url('shop-create');
        include _include(APP_PATH.'plugin/tt_shop/view/htm/shop_create.htm');
    }elseif($method=='POST'){
        $arr_list = array('tid','title','img','piece','money','money_type', 'money2', 'money_type2','card','cate','template');
        $update_arr = array();
        foreach($arr_list as $k)
            $update_arr[$k]=param($k);
        $_thread = thread_read($update_arr['tid']);
        if(!$_thread) return message(-1,'拉取信息失败！');
        if($_thread['uid']!=$uid && $gid!=1) return message(-1,'您无权将他人的主题发布为商品！');
        if($update_arr['card']=='null') $update_arr['card']='0';
        else $update_arr['card']='1';
        switch($update_arr['money_type']){
            default : $update_arr['money_type'] = '1';break;
            case 'credits': $update_arr['money_type'] = '1';break;
            case 'golds': $update_arr['money_type'] = '2';break;
            case 'rmbs': $update_arr['money_type'] = '3';break;
        }
        switch ($update_arr['money_type2']){
            default :$update_arr['money_type2'] = '0';break;
            case 'null':$update_arr['money_type2'] = '0';break;
            case 'credits':$update_arr['money_type2'] = '1';break;
            case 'golds':$update_arr['money_type2'] = '2';break;
            case 'rmbs':$update_arr['money_type2'] = '3';break;
        }
        if($group['allowcard']!='1') $update_arr['card']='0';
        $update_arr['time']=time();
        $update_arr['uid']=$uid;
        $r = shop_list_add($update_arr);
        if($update_arr['card']!='0') shop_item_count_card_piece($r);
        if($r !== FALSE) message(0,$r);
        else message(-1,'商品创建失败！');
    }
} elseif($action == 'new'){
    user_login_check();
    $template_list = shop_template_list();
    if($group['allowsell']!='1') return message(-1,'您所在的用户组无权出售主题！');
    if($method=='GET') {
        $header['title'] = $form_title = '创建商品';
        $fid = 0;
        $forum = $fid ? forum_read($fid) : array();
        $forumlist_allowthread = forum_list_access_filter($forumlist, $gid, 'allowthread');
        $forumarr = xn_json_encode(arrlist_key_values($forumlist_allowthread, 'fid', 'name'));
        if(empty($forumlist_allowthread))
            message(-1, lang('user_group_insufficient_privilege'));
        $header['title'] = lang('create_thread');
        $header['mobile_title'] = $fid ? $forum['name'] : '';
        $header['mobile_linke'] = url("forum-$fid");
        $form_action = url("shop-new");
        $form_subject = '';
        $form_message = '';
        $form_doctype = 1;
        $isfirst = 1;
        $quotepid = 0;
        $location = url("forum-'+jfid.checked()+'");
        $filelist = array();
        $form_action = url('shop-new');
        include _include(APP_PATH.'plugin/tt_shop/view/htm/shop_new.htm');
    }elseif($method=='POST'){
        $fid = $special_forum['fid'];
        $subject = param('subject');
        empty($subject) AND message('subject', lang('please_input_subject'));
        xn_strlen($subject) > 128 AND message('subject', lang('subject_length_over_limit', array('maxlength'=>128)));
        $message = param('message', '', FALSE);
        empty($message) AND message('message', lang('please_input_message'));
        $doctype = param('doctype', 0);
        $doctype > 10 AND message(-1, lang('doc_type_not_supported'));
        xn_strlen($message) > 2028000 AND message('message', lang('message_too_long'));
        $thread = array (
            'fid'=>$fid,
            'uid'=>$uid,
            'sid'=>$sid,
            'subject'=>$subject,
            'message'=>$message,
            'time'=>$time,
            'longip'=>$longip,
            'doctype'=>$doctype,
        );

        $tid = thread_create($thread, $pid);

        $pid === FALSE AND message(-1, lang('create_post_failed'));
        $tid === FALSE AND message(-1, lang('create_thread_failed'));
        $arr_list = array('piece','money','money_type', 'money2', 'money_type2','card','cate','template');
        $update_arr = array();
        foreach($arr_list as $k)
            $update_arr[$k]=param($k,'0');
        $update_arr['tid'] = $tid;
        $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png]))[\'|\"].*?[\/]?>/";
        $_thread = thread_read($tid);
        $content_db = db_find_one('post', array('tid'=>$tid,'isfirst'=>'1'));
        $content= $content_db['message'];
        thread_update($tid,array('is_shop'=>1));
        preg_match_all($pattern,$content,$matchContent);
        if(isset($matchContent[1][0])) $update_arr['img'] = $matchContent[1][0];
        else $update_arr['img'] = $_thread['user_avatar_url'];
        if($update_arr['card']=='null') $update_arr['card']='0';
        else $update_arr['card']='1';
        switch($update_arr['money_type']){
            default : $update_arr['money_type'] = '1';break;
            case 'credits': $update_arr['money_type'] = '1';break;
            case 'golds': $update_arr['money_type'] = '2';break;
            case 'rmbs': $update_arr['money_type'] = '3';break;
        }
        switch ($update_arr['money_type2']){
            default :$update_arr['money_type2'] = '0';break;
            case 'null':$update_arr['money_type2'] = '0';break;
            case 'credits':$update_arr['money_type2'] = '1';break;
            case 'golds':$update_arr['money_type2'] = '2';break;
            case 'rmbs':$update_arr['money_type2'] = '3';break;
        }
        if($group['allowcard']!='1') $update_arr['card']='0';
        $update_arr['time']=time();
        $update_arr['uid']=$uid;
        $update_arr['title'] = $subject;
        $r = shop_list_add($update_arr);
        if($update_arr['card']!='0') shop_item_count_card_piece($r);
        if($r !== FALSE) message(0,$r);
        else message(-1,'商品创建失败！');
    }
} elseif($action=='update'){
    user_login_check();
    if ($group['allowsell'] != '1') return message(-1, '您所在的用户组无权出售主题！');
    $shop_id = $data;
    if ($data == '-1') return message(-1, '拉取信息失败！');
    $shop_item = shop_item_read($shop_id);
    $open_2_credits = $shop_item['money_type2']!='0';
    $template_list = shop_template_list();
    $tid = $shop_item['tid'];
    $thread = thread_read($tid);
    $__post = db_find_one('post',array('tid'=>$tid,'isfirst'=>1));
    $pid = $__post['pid']?$__post['pid']:'1';
    $post = post_read($pid);
    empty($post) AND message(-1, lang('post_not_exists'));
    empty($thread) AND message(-1, lang('thread_not_exists'));
    $fid = $thread['fid'];
    $forum = db_find_one('forum',array('fid'=>$fid));
    empty($forum) AND message(-1, lang('forum_not_exists'));
    $isfirst = $post['isfirst'];
    if ($method == 'GET') {
        $post['message'] = htmlspecialchars($post['message'] ? $post['message'] : $post['message_fmt']);
        $attachlist = $imagelist = $filelist = array();
        if($post['files'])
            list($attachlist, $imagelist, $filelist) = attach_find_by_pid($pid);
        $header['title'] = $form_title = '编辑商品';
        $form_action = url('shop-update-' . $shop_id);
        //$form_submit_txt = lang('post_update');
        $form_subject = $thread['subject'];
        $form_message = $post['message'];
        $form_doctype = $post['doctype'];
        $isfirst = $post['isfirst'];
        $quotepid = $post['quotepid'];
        $location = url("thread-$tid");
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/shop_new.htm');
    } elseif ($method == 'POST') {
        $subject = htmlspecialchars(param('subject', '', FALSE));
        $message = param('message', '', FALSE);
        $doctype = param('doctype', 0);
        empty($message) AND message('message', lang('please_input_message'));
        mb_strlen($message, 'UTF-8') > 2048000 AND message('message', lang('message_too_long'));
        $arr = array();
        if($isfirst) {
            $newfid = $fid;
            if($subject != $thread['subject']) {
                mb_strlen($subject, 'UTF-8') > 80 AND message('subject', lang('subject_max_length', array('max'=>80)));
                $arr['subject'] = $subject;
            }
            $arr AND thread_update($tid, $arr) === FALSE AND message(-1, lang('update_thread_failed'));
        }
        $r = post_update($pid, array('doctype'=>$doctype, 'message'=>$message));
        $r === FALSE AND message(-1, lang('update_post_failed'));
        $arr_list = array('piece', 'money', 'money_type', 'money2', 'money_type2','card','cate','template');
        $update_arr = array();
        $update_arr['tid']=$tid;
        $update_arr['title']=$subject;
        $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png]))[\'|\"].*?[\/]?>/";
        $content= $message;
        preg_match_all($pattern,$content,$matchContent);
        if(isset($matchContent[1][0])) $update_arr['img'] = $matchContent[1][0];
        else $update_arr['img'] = $thread['user_avatar_url'];
        foreach ($arr_list as $k)
            $update_arr[$k] = param($k);
        //$_thread = thread_read($update_arr['tid']);
        //if (!$_thread) return message(-1, '拉取信息失败！');
        if ($thread['uid'] != $uid && $gid != 1) return message(-1, '您无权将他人的主题发布为商品！');
        if($update_arr['card']=='null') $update_arr['card'] = '0';
        else $update_arr['card']='1';
        switch ($update_arr['money_type']){
            default :$update_arr['money_type'] = '1';break;
            case 'credits':$update_arr['money_type'] = '1';break;
            case 'golds':$update_arr['money_type'] = '2';break;
            case 'rmbs':$update_arr['money_type'] = '3';break;
        }
        switch ($update_arr['money_type2']){
            default :$update_arr['money_type2'] = '0';break;
            case 'null':$update_arr['money_type2'] = '0';break;
            case 'credits':$update_arr['money_type2'] = '1';break;
            case 'golds':$update_arr['money_type2'] = '2';break;
            case 'rmbs':$update_arr['money_type2'] = '3';break;
        }
        if($group['allowcard']!='1') $update_arr['card']='0';
        shop_list_update($shop_id,$update_arr);
        if($update_arr['card']!='0') shop_item_count_card_piece($shop_id);
        message(0, $shop_id);
    }
}elseif($action=='delete' && $method=='POST'){
    $shop_id = param(2,'-1');
    if($shop_id=='-1') return message(-1,'数据获取异常！');
    $shop_item = shop_item_read($shop_id);
    if($uid != $shop_item['uid'] && $group['allowdelete']!='1') return message(-1,'无权操作！');
    shop_list_delete($shop_id);
    message(0,'商品删除成功！');
}elseif($action=='list'){//shop-list-{cate}-{page}
    if($method=='GET') {
        $data2 = param(3,-1);
        $page = $data2=='-1'?1:$data;
        $cate_id = $data=='-1'?'*':$data;
        $order = $conf['order_default'];
        $order != 'tid' AND $order = 'lastpid';
        $pagesize = $conf['pagesize'];
        $active = 'default';
        $threads = shop_list_count($cate_id);
        $shop_list = shop_list_get($page,$pagesize,$cate_id);
        shop_list_to_thread_list($shop_list);
        $threadlist = $shop_list; //thread_list_access_filter($threadlist, $gid);
        $pagination = pagination(url("shop-list-".$cate_id."-{page}"), $threads, $page, $pagesize);
        $header['title'] = $conf['sitename'];
        $header['keywords'] = '';
        $header['description'] = $conf['sitebrief'];
        $active_cate=$cate_id=='*'?'$home':$cate_id;
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/shop_list.htm');
    }
}elseif($action=='item'){
    if($method=='GET'){ // shop-item-{tid}-{page}-{keyword}.htm
        $shop_id = $data;
        $shop_item = db_find_one('shop_list',array('shop_id'=>$shop_id));
        if(!$shop_item) return message(-1,'当前商品不存在或已经被删除！');
        $tid = $shop_item['tid'];
        $page = param(3, 1);
        $pagesize = $conf['postlist_pagesize'];
        $thread = thread_read($tid);
        empty($thread) AND message(-1, lang('thread_not_exists'));
        $fid = $thread['fid'];
        $postlist = post_find_by_tid($tid, $page, $pagesize);
        empty($postlist) AND message(4, lang('post_not_exists'));
        if($page == 1) {
            empty($postlist[$thread['firstpid']]) AND message(-1, lang('data_malformation'));
            $first = $postlist[$thread['firstpid']];
            unset($postlist[$thread['firstpid']]);
            $attachlist = $imagelist = $filelist = array();
            thread_inc_views($tid);
        } else $first = post_read($thread['firstpid']);
        $allowupdate = forum_access_mod($fid, $gid, 'allowupdate') ? 1 : 0;
        $allowdelete = forum_access_mod($fid, $gid, 'allowdelete') ? 1 : 0;
        $pagination = pagination(url("shop-item-$shop_id-{page}"), $thread['posts'], $page, $pagesize);
        $header['title'] = $thread['subject'].'-商城-'.$conf['sitename'];
        $header['mobile_title'] = '商城';;
        $header['mobile_link'] = url("forum-$fid");
        $header['keywords'] = '';
        $header['description'] = $thread['subject'];
        $_SESSION['fid'] = $fid;
        // hook thread_info_end.php
        $__post = db_find_one('post',array('tid'=>$thread['tid'],'isfirst'=>1));
        $thread['pid'] = $__post['pid']?$__post['pid']:'1';
        include _include(APP_PATH.'plugin/tt_shop/view/htm/shop_item.htm');
    }
}elseif($action=='card'){
    user_login_check();
    $shop_id = $data;
    if ($group['allowsell'] != '1') return message(-1, '您所在的用户组无权出售主题！');
    if ($data == '-1') return message(-1, '拉取信息失败！');
    $shop_item = shop_item_read($shop_id);
    if(!$shop_item) return message(-1, '拉取信息失败！');
    if($shop_item['uid'] != $user['uid'] AND $gid!=1) return message(-1, '您无权编辑其他卖家的卡密！');
    if($shop_item['card']!='1')return message(-1, '不是自动发卡商品！');
    if($method=='GET') {
        $num_pid = db_count('shop_card',array('shop_id'=>$shop_id));
        $page = param(3,1);
        $data_card = db_find('shop_card',array('shop_id'=>$shop_id),array('card_id'=>-1),$page,20);
        $pagination = pagination(url("shop-card-$shop_id-{page}"), $num_pid, $page,20);
        $count_all = db_count('shop_card',array('shop_id'=>$shop_id));
        $count_used = db_count('shop_card',array('shop_id'=>$shop_id,'status'=>1));
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/shop_card.htm');
    }
    elseif($method=='POST'){
        $op = param('op','-1');
        if($op=='0'){//set_use
            $card_id=param('card_id','-1');
            if($op=='-1' || $card_id=='-1') return message(-1,'拉取信息失败！');
            $r = shop_card_set_state($shop_id,$card_id,1);
            if($r==1) message(0,'设置成功！');
            elseif($r==-1) message(-1,'设置失败，POST数据异常！');
        }elseif($op=='1'){//set un-use
            $card_id=param('card_id','-1');
            if($op=='-1' || $card_id=='-1') return message(-1,'拉取信息失败！');
            $r = shop_card_set_state($shop_id,$card_id,0);
            if($r==1) message(0,'设置成功！');
            elseif($r==-1) message(-1,'设置失败，POST数据异常！');
        }elseif($op=='2'){//delete
            $card_id=param('card_id','-1');
            if($op=='-1' || $card_id=='-1') return message(-1,'拉取信息失败！');
            shop_card_delete($card_id);
            message(0,'删除成功！');
        }elseif($op=='3'){//add_all_split
            $content=param('content');
            $card_list = explode("\n",$content);
            foreach($card_list as &$c){
                $c = str_replace("\r",'',$c);
                shop_card_add($shop_id,$c,$uid);
            }
            shop_item_count_card_piece($shop_id);
            message(0,'批量添加成功！');
        }elseif($op=='4'){//add_random
            $rand_num = param('rand','0');
            shop_card_add_random($shop_id,$rand_num,$uid);
            shop_item_count_card_piece($shop_id);
            message(0,'添加成功！');
        }elseif($op=='5'){//delete used
            shop_card_truncate_used($shop_id);
            message(0,'操作成功！');
        }elseif($op=='6'){//truncate
            shop_card_truncate($shop_id);
            message(0,'操作成功！');
        }
    }
}elseif($action=='card_output'){ //shop-card_output-xxxxx-type-s
    user_login_check();
    $shop_id = $data;
    if ($group['allowsell'] != '1') return message(-1, '您所在的用户组无权出售主题！');
    if ($data == '-1') return message(-1, '拉取信息失败！');
    $shop_item = shop_item_read($shop_id);
    if(!$shop_item) return message(-1, '拉取信息失败！');
    if($shop_item['uid'] != $user['uid'] AND $gid!=1) return message(-1, '您无权下载其他卖家的卡密！');
    if($shop_item['card']!='1')return message(-1, '不是自动发卡商品！');
    $type = param(3,'txt');
    $type_s = param(4,'all');
    if (isset($type)) {
        if($type_s=='all') $data = db_find('shop_card', array('shop_id'=>$shop_id), array('time' => -1),1,10000);
        else $data = db_find('shop_card', array('shop_id'=>$shop_id,'status'=>'0'), array('time' => -1),1,10000);
        header('Content-Type: application/download');
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Expires:0');
        header('Pragma:public');
        if ($type == 'csv') {
            header("Content-type:text/csv;");
            header("Content-Disposition:attachment;filename=shop_card_expert_$shop_id.csv");
            echo "\xEF\xBB\xBF";
            foreach ($data as $k => $v__) {
                foreach ($v__ as $k => $v___)
                    echo $k, ',';
                echo "\r\n"; break;
            }
            foreach ($data as $v) {
                foreach($v as $v_)
                    echo $v_,',';
                echo "\r\n";
            }
        } elseif ($type == 'txt'){
            header("Content-type:text/txt;");
            header("Content-Disposition:attachment;filename=shop_card_expert_$shop_id.txt");
            foreach($data as $v)
                echo $v['card'], "\r\n";
        }
    }
}elseif($action=='card_add'){
    user_login_check();
    $shop_id = $data;
    if ($group['allowsell'] != '1') return message(-1, '您所在的用户组无权出售主题！');
    if ($data == '-1') return message(-1, '拉取信息失败！');
    $shop_item = shop_item_read($shop_id);
    if(!$shop_item) return message(-1, '拉取信息失败！');
    if($shop_item['uid'] != $user['uid'] AND $gid!=1) return message(-1, '您无权编辑其他卖家的卡密！');
    if($shop_item['card']!='1') return message(-1, '不是自动发卡商品！');
    include _include(APP_PATH . 'plugin/tt_shop/view/htm/shop_card_add.htm');
}elseif($action=='my'){
    user_login_check();
    if($group['allowsell']!='1') return message(-1, '您所在的用户组无权出售商品！');
    if($method=='GET'){
        $num_pid = db_count('shop_list',array('uid'=>$uid));
        $page = param(2,1);
        $data = db_find('shop_list',array('uid'=>$uid),array('shop_id'=>-1),$page,20);
        $pagination = pagination(url("shop-my-{page}"), $num_pid, $page,20);
        shop_list_to_thread_list($data);
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/shop_my.htm');
    }
}elseif($action=='buy'){
    user_login_check();
    $shop_id = $data;
    $shop_item = shop_item_read($shop_id);
    if(!$shop_item) return message(-1,'商品不存在！');
    if($uid AND $shop_item['uid']==$uid)return message(-1,'您不能购买自己的商品！');
    $open_2_credits = $shop_item['money_type2']!='0';
    $template = shop_template_get($shop_item['template']);
    if($method=='GET')
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/shop_buy.htm');
    elseif($method=='POST'){
        $input = array('num','realname','qq','mobile','address','postcode');
        $db_arr = array();
        foreach($input as $i)
            $db_arr[$i] = param($i,'-');
        $db_arr['shop_id']=$shop_id;
        $db_arr['uid']=$uid;
        $db_arr['uid_seller']=$shop_item['uid'];
        $db_arr['title']=$shop_item['title'];
        $db_arr['img']=$shop_item['img'];
        $db_arr['time']=time();
        $db_arr['money']=$shop_item['money'];
        $db_arr['money_type']=$shop_item['money_type'];
        $db_arr['money2']=$shop_item['money2'];
        $db_arr['money_type2']=$shop_item['money_type2'];
        $db_arr['cate'] = $shop_item['cate'];
        $db_arr['template'] = $shop_item['template'];
        $r = shop_order_create($db_arr);
        $save_set = param('save','0');
        if($save_set=='1'){
            $arr = array();
            $arr['mobile']=$db_arr['mobile'];
            $arr['realname']=$db_arr['realname'];
            $arr['qq']=$db_arr['qq'];
            $arr['address']=$db_arr['address'];
            $arr['postcode']=$db_arr['postcode'];
            foreach($arr as $k=>$v)
                db_update('user',array('uid'=>$user['uid']),array($k=>$v));
        }
        if($r>0) message(0,$r);
        elseif($r==-1) message(-1,'购买数量大于库存，创建订单失败！');
        elseif($r==-2) message(-1,'购买数量无效！');
        elseif($r==-3) message(-1,'近10分钟内创建订单、且未完成支付的次数＞5，您的操作过于频繁，请稍后再试！');
        elseif($r==-4) message(-1,'订单无效！');
        elseif($r==-5) message(-1,'创建订单时发生了未知错误！');
    }
}elseif($action=='pay'){
    user_login_check();
    $oid = $data;
    $order = shop_order_read($oid);
    $shop_item = shop_item_read($order['shop_id']);
    $open_2_credits = $order['money_type2']!='0';
    if(!$shop_item) return message(-1,'商品不存在！');
    if(!$order) return message(-1,'订单不存在！');
    if($uid AND $shop_item['uid']==$uid)return message(-1,'您不能购买自己的商品！');
    if($uid != $order['uid'])return message(-1,'您无权支付他人的订单！');
    $template = shop_template_get($shop_item['template']);
    if($method=='GET')
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/shop_pay.htm');
}elseif($action=='auto_input' && $method=='POST'){
    user_login_check();
    if($group['allowsell']!='1') return message(-1,'您所在的用户组无权使用该功能！');
    $rtn = array();
    $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png]))[\'|\"].*?[\/]?>/";
    $content_db = db_find_one('post', array('tid'=>$data,'isfirst'=>'1'));
    $content= $content_db['message'];
    $_thread = thread_read($data);
    preg_match_all($pattern,$content,$matchContent);
    if(isset($matchContent[1][0])) $rtn['img'] = $matchContent[1][0];
    else $rtn['img']=$_thread['user_avatar_url'];
    $rtn['title']=$_thread['subject'];
    message(0,xn_json_encode($rtn));
}elseif($action=='cart_add'&&$method=='POST'){
    user_login_check();
    $shop_id = param('shop_id','-1');
    if($shop_id!=-1){
        $r = shop_cart_add($shop_id);
        if($r==1) message(0,'success');
        elseif($r==-1) message(-3,'max');
        elseif($r==-2) message(-2,'already exist');
        elseif($r==-3) message(-4,'buy too much');
    }else message(-1,'failed');
}elseif($action=='cart_truncate'&&$method=='POST'){
    user_login_check();
    shop_cart_truncate();
    message(1,'清空成功！');
}elseif($action=='cart_setcount'&&$method=='POST'){
    user_login_check();
    $shop_id = param('shop_id','-1');
    $count = param('count','-1');
    if($shop_id==-1 || $count<=0) return message(-1,'failed');
    $r = db_find_one('shop_list',array('shop_id'=>$shop_id));
    if(!$r) return message(-1,'not_exist');
    shop_cart_set($shop_id,$count);
}elseif($action=='cart_delete'&&$method=='POST'){
    user_login_check();
    $shop_id = param('shop_id','-1');
    if($shop_id!=-1){
        shop_cart_remove($shop_id);
        message(1,shop_cart_count_num());
    }else message(-1,'failed');
}elseif($action=='cart'){
    user_login_check();
    $cart_num = shop_cart_count_num();
    $cart_money = shop_cart_calculate();
    $cart_list = shop_cart_fmt();
    if($method=='GET')
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/shop_cart.htm');
    elseif($method=='POST'){
        foreach($cart_list as $k=>$v){
            $v_new= param('cart_input_'.$k,'-1');
            if($v_new<=0) shop_cart_remove($v['item']['shop_id']);
            else shop_cart_set($v['item']['shop_id'],$v_new);
        }
        message(0,shop_cart_count_num());
    }
}elseif($action=='cart_create'){
    user_login_check();
    $cart_num = shop_cart_count_num();
    $cart_money = shop_cart_calculate();
    $cart_list = shop_cart_fmt();
    if($method=='GET')
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/shop_cart_create.htm');
    elseif($method=='POST'){
        $op = param('op','0');//0:pay 1:only_create_order
        if($cart_num<=0) return message(-1,'购物车内无商品！');
        $flag=1;
        foreach($cart_list as $_cart){
            $shop_item = shop_item_read($_cart['item']['shop_id']);
            if($shop_item['piece']<$_cart['num']) {$flag=0;break;}
        }
        if($flag==0) return message(-1,'操作失败，库存不足！');
        $flag=1;
        foreach($cart_money as $k=>$_money)
            if($user[$k]<$_money){$flag=0;break;}
        if($flag==0 && $op=='0') return message(-1,'操作失败，余额不足！');
        foreach($cart_list as $_cart){
            $input = array('realname','qq','mobile','address','postcode');
            $db_arr = array();
            foreach($input as $i)
                $db_arr[$i] = param($i,'-');
            $shop_item = $_cart['item'];
            $db_arr['num'] = $_cart['num'];
            $db_arr['shop_id']=$shop_item['shop_id'];
            $db_arr['uid']=$uid;
            $db_arr['uid_seller']=$shop_item['uid'];
            $db_arr['title']=$shop_item['title'];
            $db_arr['img']=$shop_item['img'];
            $db_arr['time']=time();
            $db_arr['money']=$shop_item['money'];
            $db_arr['money_type']=$shop_item['money_type'];
            $db_arr['money2']=$shop_item['money2'];
            $db_arr['money_type2']=$shop_item['money_type2'];
            $db_arr['cate'] = $shop_item['cate'];
            $db_arr['template'] = $shop_item['template'];
            $r = shop_order_create($db_arr);
            if($op=='0'&&$r>0) shop_order_pay($r);
        }
        $save_set = param('save','0');
        if($save_set=='1'){
            $arr = array();
            $arr['mobile']=$db_arr['mobile'];
            $arr['realname']=$db_arr['realname'];
            $arr['qq']=$db_arr['qq'];
            $arr['address']=$db_arr['address'];
            $arr['postcode']=$db_arr['postcode'];
            foreach($arr as $k=>$v)
                db_update('user',array('uid'=>$user['uid']),array($k=>$v));
        }
        shop_cart_truncate();
        message(0,'操作成功！');
    }
}elseif($action=='template') {
    user_login_check();
    if ($group['allowsell'] != '1') return message(-1, '您所在的用户组无权出售主题！');
    $template_list = db_find('shop_template');
    $template_count = db_count('shop_template');
    if ($method == 'GET')
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/shop_template.htm');
    elseif($method=='POST'){
        $op = param('op','-1');
        $realname=param('realname','-'); $qq=param('qq','-');$mobile=param('mobile','-');$address=param('address','-');$postcode=param('postcode','-');$template_name=param('template_name','myTemplate');
        $update_arr = array('realname'=>$realname,'qq'=>$qq,'mobile'=>$mobile,'address'=>$address,'postcode'=>$postcode,'template_name'=>$template_name);
        if($op==0) db_insert('shop_template',$update_arr); //create
        elseif($op==1){//edit
            $template_id = param('template_id','-1'); if($template_id=='-1') return message(-1,'读取信息失败');
            db_update('shop_template',array('template_id'=>$template_id),$update_arr);
        }elseif($op==2){//delete
            $template_id = param('template_id','-1');
            db_delete('shop_template',array('template_id'=>$template_id));
        }
        message(0,'操作成功！');
    }
}
?>