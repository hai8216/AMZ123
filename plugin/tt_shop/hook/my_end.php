<?php exit;
elseif($action == 'address'){
    if($method == 'GET'){
        $shop_cate_list = shop_cate_list_get();
        $input['realname'] = form_text('realname', $user['realname']);
        $input['qq'] = form_text('qq', $user['qq']);
        $input['mobile'] = form_text('mobile', $user['mobile']);
        $input['address'] = form_text('address', $user['address']);
        $input['postcode'] = form_text('postcode', $user['postcode']);
        include _include(APP_PATH.'plugin/tt_shop/view/htm/my_address.htm');
    } elseif($method == 'POST') {
        $postcode=param('postcode');
        $address=param('address');
        $realname=param('realname');
        $mobile=param('mobile');
        $qq=param('qq');
        $arr['mobile']=$mobile;
        $arr['realname']=$realname;
        $arr['qq']=$qq;
        $arr['address']=$address;
        $arr['postcode']=$postcode;
        foreach($arr as $k=>$v)
            db_update('user',array('uid'=>$user['uid']),array($k=>$v));
        message(0,'资料设定成功！');
    }
}elseif($action=='order'|| $action=='order_unpaid'|| $action=='order_paid'||$action=='order_sent'||$action=='order_rate'||$action=='order_complete'||$action=='order_close'){
    if($method=='GET') {
    $shop_cate_list = shop_cate_list_get();
        if($action=='order') $cond = array();
        else $cond = array('status'=>$g_shop_my_route[$action]);
        $cond['uid']=$uid;
        $num_pid = db_count('shop_order',$cond);
        $page = param(2,1);
        $data = db_find('shop_order',$cond,array('oid'=>-1),$page,20);
        $pagination = pagination(url("my-$action-{page}"), $num_pid, $page,20);
        $act = 'buyer';
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/my_order.htm');
    }elseif($method=='POST'){
        $oid = param('oid','-1'); $act = param('act','-1');
        if($oid==-1 || $act==-1) return message(-1,'拉取信息失败！');
        $order = shop_order_read($oid);
        $r = 0;
        if($act==0) $r = shop_order_pay($oid);
        elseif($action==1) $r=shop_order_confirm($oid);
        if($act==0 && $r==1) notice_send($order['uid'], $order['uid_seller'], '有人购买了您的商品,请尽快发货！', 801);
        if($r==-1) message(-1,'订单不存在！');
        elseif($r==-2) message(-1,'订单状态异常！');
        elseif($r==-3) message(-1,'您的余额不足，请您<a href="'.url('my-credits').'">充值</a>后再支付！');
        elseif($r==1) message(0,'操作成功！');
    }
}elseif($action=='order_detail'){
    if($method=='GET'){
        $shop_cate_list = shop_cate_list_get();
        $oid = param(2);
        $set_shop = setting_get('tt_shop');
        $order = shop_order_read($oid);
        $tid = shop_get_tid_by_shop_id($order['shop_id']);
        if($order) {
            if ($uid && ($gid == 1 || $order['uid'] == $uid || $order['uid_seller'] == $uid)) {
                $act='null';
                if($order['uid'] == $uid) $act='buyer';
                elseif($order['uid_seller'] == $uid) $act='seller';
                $template = shop_template_get($order['template']);
                include _include(APP_PATH . 'plugin/tt_shop/view/htm/my_order_detail.htm');
            }
            else return message(-1, '该订单您无权查看！');
        }else return message(-1,'订单不存在！');
    }
}elseif($action=='order_rate_act'){
    if($method=='GET'){
        $shop_cate_list = shop_cate_list_get();
        $oid = param(2);
        $order = shop_order_read($oid);
        if($order)
            if($uid && $order['uid']==$uid && $order['status']==3)
                include _include(APP_PATH . 'plugin/tt_shop/view/htm/my_order_rate.htm');
            else
                return message(-1,'该订单您无权评价！');
        else
            return message(-1,'订单不存在！');
    }elseif($method=='POST'){
        $rate = param('rate','five'); $message =param('message','',FALSE); $oid=param('oid','-1');
        $oid=='-1' AND message(-1,'数据拉取异常！');
        $order = shop_order_read($oid);
        $tid = shop_get_tid_by_shop_id($order['shop_id']);
        $_thread = thread_read($tid);
        if($order) {
            if (!($uid && $order['uid'] == $uid&& $order['status']==3))
                return message(-1, '该订单您无权评价！');
        }else return message(-1,'订单不存在！');
        empty($message) AND message(-1, lang('please_input_message'));
        xn_strlen($message) > 2028000 AND message(-1, lang('message_too_long'));
        $post = array(
            'tid'=>$tid,
            'uid'=>$uid,
            'create_date'=>time(),
            'userip'=>$longip,
            'isfirst'=>0,
            'doctype'=>0,
            'quotepid'=>0,
            'message'=>$message,
        );
        $pid = post_create($post, $_thread['fid'], $user['gid']);
        $r = 0;
        if($rate=='five') $r = shop_order_rate($oid,5,$pid);
        elseif($rate=='three') $r = shop_order_rate($oid,3,$pid);
        elseif($rate=='one') $r = shop_order_rate($oid,1,$pid);
        else message(-1,'数据拉取异常！');
        if($r==-1) message(-1,'订单不存在！');
        elseif($r==-2) message(-1,'订单状态异常！');
        elseif($r==-3) message(-1,'数据拉取异常！');
        elseif($r==1) { notice_send($order['uid'], $order['uid_seller'], '有人评价了您的商品！', 801); message(0,'评价成功！');}
    }
}elseif($action=='order_send'){
    if($group['allowsell']!='1') return message(-1, '您所在的用户组无权出售商品！');
    if ($method == 'GET') {
        $shop_cate_list = shop_cate_list_get();
        $oid = param(2);
        $order = shop_order_read($oid);
        if ($order)
            if ($uid && $order['uid_seller'] == $uid&& $order['status']==1)
                include _include(APP_PATH . 'plugin/tt_shop/view/htm/my_order_send.htm');
            else
                return message(-1, '该订单您无权发货！');
        else
            return message(-1, '订单不存在！');
    } elseif ($method == 'POST') {
        $oid=param('oid','-1'); $express_name = param('express_name'); $express_code = param('express_code');
        $oid=='-1' AND message(-1,'数据拉取异常！');
        $order = shop_order_read($oid);
        if ($order) {
            if (!($uid && $order['uid_seller'] == $uid&& $order['status']==1))
                return message(-1, '该订单您无权发货！');
        }else
            return message(-1, '订单不存在！');
        if(xn_strlen($express_name)>=30 || xn_strlen($express_code)>=30)
            return message(-1, '长度过长！');
        shop_order_seller_send($oid,array(
            'express_name'=>$express_name,
            'express_code'=>$express_code
        ));
        notice_send($order['uid_seller'], $order['uid'], '您购买的商品已发货，请关注物流等发货信息！', 802);
        message(0,'发货成功！');
    }
}elseif($action=='order_close_act' && $method=='POST'){
        $oid=param('oid','-1');
        $oid=='-1' AND message(-1,'数据拉取异常！');
        $order = shop_order_read($oid);
        if ($order) {
            if (!($uid && $order['uid'] == $uid && $order['status']==0))
                return message(-1, '该订单您无权关闭！');
        }else
            return message(-1, '订单不存在！');
        shop_order_close($oid);
        message(0,'关闭成功！');
}elseif($action=='seller'|| $action=='seller_unpaid'|| $action=='seller_paid'||$action=='seller_sent'||$action=='seller_rate'||$action=='seller_complete'||$action=='seller_close'){
    if($group['allowsell']!='1') return message(-1, '您所在的用户组无权出售商品！');
    if($method=='GET') {
        $shop_cate_list = shop_cate_list_get();
        if($action=='seller') $cond = array();
        else $cond = array('status'=>$g_shop_my_route[$action]);
        $cond['uid_seller']=$uid;
        $num_pid = db_count('shop_order',$cond);
        $page = param(2,1);
        $data = db_find('shop_order',$cond,array('oid'=>-1),$page,20);
        $pagination = pagination(url("my-$action-{page}"), $num_pid, $page,20);
        $act = 'seller';
        include _include(APP_PATH . 'plugin/tt_shop/view/htm/my_seller.htm');
    }
}elseif($action=='order_confirm'&&$method=='POST'){
        $oid=param('oid','-1');
        $oid=='-1' AND message(-1,'数据拉取异常！');
        $order = shop_order_read($oid);
        if ($order) {
            if (!($uid && $order['uid'] == $uid&& $order['status']==2))
                return message(-1, '该订单您无权确认收货！');
        }else
            return message(-1, '订单不存在！');
        $r = shop_order_confirm($oid);
        if($r==1) message(0,'确认收货成功！');
        else message(-1,'确认收货失败！');
}
?>