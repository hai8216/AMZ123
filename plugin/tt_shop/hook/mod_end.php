<?php exit;
elseif($action == 'deleteshop'){
    if($method == 'GET')
        include _include(APP_PATH.'plugin/tt_shop/view/htm/mod_deleteshop.htm');
    else {
        $tidarr = param('tidarr', array(0));
        empty($tidarr) AND message(-1, lang('please_choose_thread'));
        $threadlist = thread_find_by_tids($tidarr);
        foreach($threadlist as &$thread) {
            $fid = $thread['fid'];
            $tid = $thread['tid'];
            if(forum_access_mod($fid, $gid, 'allowdelete'))
                shop_list_delete_by_tid($tid);
            $r = db_find_one('forum',array('name'=>'^商城$'));
            if($thread['fid']==$r['fid'])
                thread_delete($thread['tid']);
        }
        message(0, lang('delete_completely'));
    }
}
?>