<?php
!defined('DEBUG') AND exit('Forbidden');
$tablepre = $db->tablepre;
$sql = "ALTER TABLE {$tablepre}user ADD COLUMN postcode CHAR(20) DEFAULT '100000'";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}user ADD COLUMN address CHAR(150) DEFAULT '地址'";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}thread ADD COLUMN is_shop INT(8) DEFAULT '0'";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}group ADD COLUMN allowsell INT(8) DEFAULT '0'";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}group ADD COLUMN allowcard INT(8) DEFAULT '0'";
db_exec($sql);
group_list_cache_delete();
$sql="CREATE TABLE IF NOT EXISTS `{$tablepre}shop_list` (
  `shop_id` CHAR(20) NOT NULL DEFAULT '0',
  `tid` int(10) NOT NULL DEFAULT '0',
  `uid` int(10) NOT NULL DEFAULT '0',
  `cate` int(10) NOT NULL DEFAULT '0',
  `title` CHAR(150) NOT NULL DEFAULT '-',
  `img` CHAR(150) NOT NULL DEFAULT '-',
  `piece` INT(8) NOT NULL DEFAULT '0',
  `rate` CHAR(16) NOT NULL DEFAULT '0',
  `rate_1` INT(8) NOT NULL DEFAULT '0',
  `rate_3` INT(8) NOT NULL DEFAULT '0',
  `rate_5` INT(8) NOT NULL DEFAULT '0',
  `rate_num` INT(8) NOT NULL DEFAULT '0',
  `card` INT(5) NOT NULL DEFAULT '0',
  `money` INT(10) NOT NULL DEFAULT '0',
  `money_type` INT(5) NOT NULL DEFAULT '0',
  `money2` INT(10) NOT NULL DEFAULT '0',
  `money_type2` INT(5) NOT NULL DEFAULT '0',
  `time` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (shop_id),
	KEY (tid),
	UNIQUE KEY (shop_id, tid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}shop_list ADD COLUMN template INT(8) DEFAULT '-1'";
db_exec($sql);
$sql="CREATE TABLE IF NOT EXISTS `{$tablepre}shop_order` (
  `oid` CHAR(20) NOT NULL DEFAULT '0',
  `shop_id` CHAR(20) NOT NULL DEFAULT '0',
  `num` int(8) NOT NULL DEFAULT '1',
  `uid` int(10) NOT NULL DEFAULT '0',
  `cate` int(10) NOT NULL DEFAULT '0',
  `uid_seller` int(10) NOT NULL DEFAULT '0',
  `title` CHAR(150) NOT NULL DEFAULT '-',
  `img` CHAR(150) NOT NULL DEFAULT '-',
  `realname` CHAR(50) NOT NULL DEFAULT '-',
  `qq` CHAR(50) NOT NULL DEFAULT '-',
  `mobile` CHAR(50) NOT NULL DEFAULT '-',
  `express_name` CHAR(50) NOT NULL DEFAULT '-',
  `express_code` VARCHAR(1000) NOT NULL DEFAULT '-',
  `address` CHAR(150) NOT NULL DEFAULT '-',
  `postcode` CHAR(20) NOT NULL DEFAULT '-',
  `status` int(8) NOT NULL DEFAULT '0',
  `rate` int(8) NOT NULL DEFAULT '0',
  `rate_pid` int(8) NOT NULL DEFAULT '0',
  `money` INT(10) NOT NULL DEFAULT '0',
  `money_type` INT(5) NOT NULL DEFAULT '0',
  `money2` INT(10) NOT NULL DEFAULT '0',
  `money_type2` INT(5) NOT NULL DEFAULT '0',
  `time` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (oid),
	KEY (oid),
	UNIQUE KEY (oid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
db_exec($sql);
$sql = "ALTER TABLE {$tablepre}shop_order ADD COLUMN template INT(8) DEFAULT '-1'";
db_exec($sql);
$sql="CREATE TABLE IF NOT EXISTS `{$tablepre}shop_card` (
  `card_id` INT(20) NOT NULL AUTO_INCREMENT,
  `shop_id` CHAR(20) NOT NULL DEFAULT '0',
  `oid` CHAR(20) NOT NULL DEFAULT '0',
  `card` CHAR(50) NOT NULL DEFAULT '1',
  `uid` INT(10) NOT NULL DEFAULT '0',
  `uid_seller` INT(10) NOT NULL DEFAULT '0',
  `status` INT(5) NOT NULL DEFAULT '0',
  `time` INT(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (card_id),
	KEY (card_id),
	UNIQUE KEY (card_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
db_exec($sql);
$sql="CREATE TABLE IF NOT EXISTS `{$tablepre}shop_cate` (
  `cate_id` INT(20) NOT NULL AUTO_INCREMENT,
  `rank` INT(20) NOT NULL DEFAULT '0',
  `name` CHAR(20) NOT NULL DEFAULT '0',
  `icon` CHAR(30) NOT NULL DEFAULT 'clock-o',
  `num` INT(20) NOT NULL DEFAULT '0',
  `parent` INT(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (cate_id),
	KEY (cate_id),
	UNIQUE KEY (cate_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
db_exec($sql);
$sql="CREATE TABLE IF NOT EXISTS `{$tablepre}shop_template` (
  `template_id` INT(20) NOT NULL AUTO_INCREMENT,
  `template_name` CHAR(30) NOT NULL DEFAULT 'myTemplate',
  `realname` CHAR(30) NOT NULL DEFAULT '-',
  `qq` CHAR(30) NOT NULL DEFAULT '-',
  `mobile` CHAR(30) NOT NULL DEFAULT '-',
  `address` CHAR(30) NOT NULL DEFAULT '-',
  `postcode` CHAR(30) NOT NULL DEFAULT '-',
  PRIMARY KEY (template_id),
	KEY (template_id),
	UNIQUE KEY (template_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
db_exec($sql);
$db = db_find_one('shop_cate',array('name'=>'手机'));
!isset($db) AND db_insert('shop_cate',array('name'=>'手机','icon'=>'phone'));
setting_set('tt_shop',array('show_seller'=>1,'show_thread'=>0,'edit_sync'=>1));

$db = db_find_one('forum',array('name'=>'^商城$'));
if(!$db) {
    $arr = array('name' => '^商城$', 'rank' => '0');
    forum_create($arr);
}

?>