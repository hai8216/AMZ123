<?php
$g_shop_status = array('订单关闭','待支付','已支付,待发货','已发货,待收货','已收货,待评价','交易完毕');
$g_shop_status_short = array('已关闭','待支付','待发货','待收货','待评价','已完毕');
$g_shop_my_route = array('order'=>'*','order_unpaid'=>0,'order_paid'=>1,'order_sent'=>2,'order_rate'=>3,'order_complete'=>4,'order_close'=>-1,'seller'=>'*','seller_unpaid'=>0,'seller_paid'=>1,'seller_sent'=>2,'seller_rate'=>3,'seller_complete'=>4,'seller_close'=>-1);
function shop_cron_5_minutes(){
    $r = db_find('shop_order',array('status'=>0,'time'=>array('<'=>time()-30*60)),array(),1,10000);
    foreach($r as $_r)
        shop_order_close($_r['oid']);
}
function shop_cron_daliy(){
    db_update('shop_order',array('status'=>2,'time'=>array('<'=>time()-15*24*60)),array('status'=>3));
    $not_rate_list = db_find('shop_order',array('status'=>3,'time'=>array('<'=>time()-15*24*60)),array('time'=>-1),1,10000);
    foreach($not_rate_list as $lst)
        shop_order_rate($lst['oid'],5);
    shop_card_truncate_used_all();
}
function shop_order_delete($oid){
    $order = shop_order_read($oid);
    if(!$order) return -1;
    if($order['status']!=-1 && $order['status']!=0 && $order['status']!=4) return -2;
    db_delete('shop_order',array('oid'=>$oid));
}
function shop_item_read($shop_id){
    $r = db_find_one('shop_list',array('shop_id'=>$shop_id));
    return $r;
}
function shop_get_username_by_uid($uid){
    $r = db_find_one('user',array('uid'=>$uid));
    if($r) return $r['username'];
    else return 'USER';
}
function shop_order_create($arr){
    $shop_id = $arr['shop_id'];
    $num = $arr['num'];
    $item = shop_item_read($shop_id);
    if($num>$item['piece']) return -1;
    if($num>=100 || $num<=0) return -2;
    //$count = db_count('shop_order',array('uid'=>$arr['uid'],'time'=>array('>'=>time()-600),'status'=>0));
    //if($count AND $count>5) return -3;
    if($arr['money']<=0) return -4;
    $arr['oid'] =date('YmdHis').mt_rand(1000,9999);
    db_insert('shop_order',$arr);
    db_update('shop_list',array('shop_id'=>$shop_id),array('piece-'=>$arr['num']));
    $r = db_find_one('shop_order',$arr);
    if($r AND isset($r['oid'])) return $r['oid'];
    else return -5;
}
function shop_order_close($oid){
    $order = shop_order_read($oid);
    if($order['status']!=0) return -1;
    shop_order_set_status($oid,-1);
    db_update('shop_list',array('shop_id'=>$order['shop_id']),array('piece+'=>$order['num']));
    return 1;
}
function shop_order_set_status($oid,$status){
    db_update('shop_order',array('oid'=>$oid),array('status'=>$status));
}
function shop_order_read_status($oid){
    global $g_shop_status;
    $r = shop_order_read($oid);
    return $g_shop_status[$r['status']+1];
}
function shop_order_read($oid){
    $r = db_find_one('shop_order',array('oid'=>$oid),array('oid'=>-1));
    return $r;
}
function shop_order_rate($oid,$rate,$pid=0){
    $order = shop_order_read($oid);
    $shop_item = shop_item_read($order['shop_id']);
    if(!$order) return -1;
    if(!$shop_item) return -1;
    if($order['status']!=3) return -2;
    if($rate!=1 && $rate!=3 && $rate !=5) return -3;
    shop_order_set_status($oid,4);
    db_update('shop_order',array('oid'=>$oid),array('rate'=>$rate,'rate_pid'=>$pid));
    db_update('shop_list',array('shop_id'=>$order['shop_id']),array('rate_'.$rate.'+' =>'1','rate_num+'=>'1'));
    shop_list_count_rate($order['shop_id']);
    if($shop_item['card']=='1') shop_item_count_card_piece($order['shop_id']);
    return 1;
}
function shop_list_count_rate($shop_id){
    $item = db_find_one('shop_list',array('shop_id'=>$shop_id));
    $rate1 = $item['rate_1'];
    $rate3 = $item['rate_3'];
    $rate5 = $item['rate_5'];
    $rate_num = $item['rate_num'];
    $rate = ($rate1 + $rate3 * 3.0 + $rate5 * 5.0) / $rate_num;
    db_update('shop_list',array('shop_id'=>$shop_id),array('rate'=>$rate));
}
function shop_order_confirm($oid){
    $order = shop_order_read($oid);
    if(!$order) return -1;
    if($order['status']!=2) return -2;
    shop_order_set_status($oid,3);
    db_update('user',array('uid'=>$order['uid_seller']),array(get_credits_name_by_type($order['money_type']).'+'=>$order['money']*$order['num']));
    if($order['money_type2']!='0')
        db_update('user',array('uid'=>$order['uid_seller']),array(get_credits_name_by_type($order['money_type2']).'+'=>$order['money2']*$order['num']));
    return 1;
}
function shop_order_pay($oid){
    $order = shop_order_read($oid);
    if(!$order) return -1;
    if($order['status']!=0) return -2;
    $shop_item = shop_item_read($order['shop_id']);
    $_user = user_read($order['uid']);
    if($_user[get_credits_name_by_type($order['money_type'])] < $order['money']*$order['num']) return -3;
    if($order['money_type2']!='0')
        if($_user[get_credits_name_by_type($order['money_type2'])] < $order['money2']*$order['num']) return -3;
    shop_order_set_status($oid,1);
    db_update('user',array('uid'=>$order['uid']),array(get_credits_name_by_type($order['money_type']).'-'=>$order['money']*$order['num']));
    if($order['money_type2']!='0')
        db_update('user',array('uid'=>$order['uid']),array(get_credits_name_by_type($order['money_type2']).'-'=>$order['money2']*$order['num']));
    if($shop_item['card']=='1')
        shop_order_auto_card($order['shop_id'],$oid,$order['num']);
    return 1;
}
function shop_order_auto_card($shop_id,$oid,$num){
    $order = shop_order_read($oid);
    $shop_item = shop_item_read($shop_id);
    if((!$order)||(!$shop_item)) return -1;
    $card = '';
    for($i=0;$i<$num;$i++){
        $card_item = db_find_one('shop_card', array('shop_id' => $shop_id, 'status' => 0), array('card_id' => -1));
        db_update('shop_card',array('card_id'=>$card_item['card_id']),array('status'=>'1','uid'=>$order['uid'],'uid_seller'=>$order['uid_seller'],'oid'=>$oid));
        if($i != 0) $card.='|';
        $card .= $card_item['card'];
    }
    shop_order_seller_send($oid,array(
        'express_name'=>'自动发卡',
        'express_code'=>$card
    ));
    shop_order_confirm($oid);
    return 1;
}
function shop_card_add($shop_id,$card,$uid_seller){
    db_insert('shop_card',array('shop_id'=>$shop_id,'status'=>0,'time'=>time(),'card'=>$card,'uid_seller'=>$uid_seller));
}
function shop_card_add_random($shop_id,$num,$uid_seller){
    for($i=0;$i<$num;$i++)
        shop_card_add($shop_id,shop_get_random_chars(16),$uid_seller);
}
function shop_card_truncate($shop_id){
    db_delete('shop_card',array('shop_id'=>$shop_id));
    shop_item_count_card_piece($shop_id);
}
function shop_card_truncate_used($shop_id){
    db_delete('shop_card',array('shop_id'=>$shop_id,'status'=>1));
    shop_item_count_card_piece($shop_id);
}
function shop_card_truncate_used_all(){
    db_delete('shop_card',array('status'=>1));
}
function shop_card_set_state($shop_id,$card_id,$state){
    $card_info = shop_card_read($card_id);
    if($card_info['shop_id'] != $shop_id) return -1;
    db_update('shop_card',array('card_id'=>$card_id),array('status'=>$state));
    shop_item_count_card_piece($shop_id);
    return 1;
}
function shop_card_read($card_id){
    $r = db_find_one('shop_card',array('card_id'=>$card_id));
    return $r;
}
function shop_card_delete($card_id){
    $r = db_find_one('shop_card',array('card_id'=>$card_id));
    if($r) {
        db_delete('shop_card', array('card_id' => $card_id));
        shop_item_count_card_piece($r['shop_id']);
        return TRUE;
    }else return FALSE;
}
function shop_order_seller_send($oid,$arr){
    $order = shop_order_read($oid);
    $shop_item = shop_item_read($order['shop_id']);
    if(!$order) return -1;
    if(!$shop_item) return -1;
    if($order['status']!=1) return -2;
    shop_order_set_status($oid,2);
    db_update('shop_order',array('oid'=>$oid),$arr);
    if($shop_item['card']=='1') shop_item_count_card_piece($order['shop_id']);
    return 1;
}
function shop_list_add($arr){
    if($arr['money']<=0) return FALSE;
    $arr['shop_id'] =date('YmdHis').mt_rand(1000,9999);
    $r = db_count('shop_list',array('tid'=>$arr['tid']));
    if($r>0) return FALSE;
    db_insert('shop_list',$arr);
    $tid = $arr['tid'];
    db_update('thread',array('tid'=>$tid),array('is_shop'=>1));
    shop_cate_num_opt($arr['cate'],1);
    return $arr['shop_id'];
}
function shop_list_delete($shop_id){
    $r = db_find_one('shop_list',array('shop_id'=>$shop_id));
    shop_cate_num_opt($r['cate'],-1);
    db_delete('shop_list',array('shop_id'=>$shop_id));
    shop_card_truncate($shop_id);
    db_update('thread',array('tid'=>$r['tid']),array('is_shop'=>0));
}
function shop_list_delete_by_tid($tid){
    $r = db_find_one('shop_list',array('tid'=>$tid));
    if($r) {
        $shop_id = $r['shop_id'];
        shop_cate_num_opt($r['cate'],-1);
        db_delete('shop_list', array('shop_id' => $shop_id));
        db_update('thread', array('tid' => $tid), array('is_shop' => 0));
        return TRUE;
    }else return FALSE;
}
function shop_list_update($shop_id,$arr){
    $before_arr = db_find_one('shop_list',array('shop_id'=>$shop_id));
    $before_cate = $before_arr['cate'];
    if(isset($arr['cate']) AND $before_cate != $arr['cate']){
        shop_cate_num_opt($before_cate,-1);
        shop_cate_num_opt($arr['cate'],1);
    }
    db_update('shop_list',array('shop_id'=>$shop_id),$arr);
}
function shop_list_get($page=1,$pagesize=20,$cate_id='*'){
    $cond = $cate_id=='*'? array():array('cate'=>$cate_id);
    $rtn = db_find('shop_list',$cond,array('shop_id'=>-1),$page,$pagesize);
    return $rtn;
}
function shop_list_count($cate_id='*'){
    $cond = $cate_id=='*'? array():array('cate'=>$cate_id);
    return db_count('shop_list',$cond);
}
function shop_list_to_thread_list(&$shop_list){
    foreach($shop_list as &$_shop){
        $_thread = thread_read($_shop['tid']);
        $_shop['thread'] = $_thread;
    }
}
function shop_user_get_info($uid){
    $_user = user_read($uid);
    $rtn = array();
    $rtn['realname']=$_user['realname'];
    $rtn['qq']=$_user['qq'];
    $rtn['mobile']=$_user['mobile'];
    $rtn['address']=$_user['address'];
    $rtn['postcode']= $_user['postcode'];
    return $rtn;
}
function shop_get_tid_by_shop_id($shop_id){
    $r = db_find_one('shop_list',array('shop_id'=>$shop_id));
    if($r) return $r['tid'];
    else return '0';
}
function shop_order_price_fmt($order,$add_icon=0,$split='&nbsp;'){
    $open_2_money = $order['money_type2']!='0';
    $rtn = '';
    if($add_icon) $rtn.=credits_get_icon_html($order['money_type']).'&nbsp;';
    $rtn .= credits_get_text($order['money_type'],$order['money']*$order['num']);
    if($open_2_money){
        $rtn.=$split;
        if($add_icon) $rtn.=credits_get_icon_html($order['money_type2']).'&nbsp;';
        $rtn .= credits_get_text($order['money_type2'],$order['money2']*$order['num']);
    }
    return $rtn ;
}
function shop_item_price_fmt($shop_item,$add_icon=0,$show_char=1,$split='&nbsp;'){
    $open_2_money = $shop_item['money_type2']!='0';
    $rtn = '';
    if($add_icon) $rtn.=credits_get_icon_html($shop_item['money_type']).'&nbsp;';
    if($show_char) $rtn .= credits_get_text($shop_item['money_type'],$shop_item['money']);
    else $rtn .= $shop_item['money_type']=='3'?$shop_item['money']/100.0:$shop_item['money'];
    if($open_2_money){
        $rtn.=$split;
        if($add_icon) $rtn.=credits_get_icon_html($shop_item['money_type2']).'&nbsp;';
        if($show_char) $rtn .= credits_get_text($shop_item['money_type2'],$shop_item['money2']);
        else $rtn .= $shop_item['money_type2']=='3'?$shop_item['money2']/100.0:$shop_item['money2'];
    }
    return $rtn ;
}
function shop_get_random_chars($length){
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890@*#";
    return substr(str_shuffle($chars),0,$length);
}
function shop_item_count_card_piece($shop_id){
    $count_card_piece = db_count('shop_card',array('shop_id'=>$shop_id,'status'=>0));
    if($count_card_piece) shop_list_update($shop_id,array('piece'=>$count_card_piece));
    else shop_list_update($shop_id,array('piece'=>0));
}
function shop_cate_list_get($page=1,$pagesize=20){
    return db_find('shop_cate',array(),array('rank'=>-1),$page,$pagesize,'cate_id');
}
function shop_cate_create($arr){
    db_insert('shop_cate',$arr);
}
function shop_cate_delete($cate_id){
    db_delete('shop_cate',array('cate_id'=>$cate_id));
    db_update('shop_list',array('cate'=>$cate_id),array('cate'=>-1));
    db_update('shop_order',array('cate'=>$cate_id),array('cate'=>-1));
}
function shop_cate_get_name_by_id($cate_id){
    if($cate_id==-1) return '过期的分类';
    $r = db_find_one('shop_cate',array('cate_id'=>$cate_id));
    if(isset($r['name'])) return $r['name'];
    else return '过期的分类';
}
function shop_cate_db_count(){
    $cate_list = db_find('shop_cate',array(),array('rank'=>1));
    foreach($cate_list as $_cate){
        $c = db_count('shop_list',array('cate'=>$_cate['cate_id']));
        if($c!==FALSE) db_update('shop_cate',array('cate_id'=>$_cate['cate_id']),array('num'=>$c));
    }
}
function shop_cate_num_opt($cate_id,$num){
    if($cate_id!=-1) db_update('shop_cate',array('cate_id'=>$cate_id),array('num+'=>$num));
}
function shop_cart_add($shop_id,$num=1){
    shop_cart_fmt();
    $shop_item = shop_item_read($shop_id);
    if(shop_cart_count_num()+1>=20) return -1;
    $r = shop_cart_find($shop_id);
    if($r) return -2;
    elseif($shop_item['piece']<1) return -3;
    else {$_SESSION['cart'][$shop_id] = $num; return 1;}
}
function shop_cart_remove($shop_id){
    if(is_array($_SESSION['cart'])){
        foreach($_SESSION['cart'] as $_cart=>$_cart_num)
            if($_cart == $shop_id) {
                unset($_SESSION['cart'][$_cart]); break;
            }
    }
}
function shop_cart_set($shop_id,$num=1){
    shop_cart_fmt();
    $_SESSION['cart'][$shop_id] = $num;
}
function shop_cart_calculate(){
    $rtn=array('credits'=>0,'golds'=>0,'rmbs'=>0);
    if(!isset($_SESSION['cart'])) return $rtn;
    foreach($_SESSION['cart'] as $_cart=>$_cart_num){
        $_shop_list = db_find_one('shop_list',array('shop_id'=>$_cart));
        shop_cart_add_money($rtn,$_shop_list['money'],$_shop_list['money_type'],$_cart_num);
        shop_cart_add_money($rtn,$_shop_list['money2'],$_shop_list['money_type2'],$_cart_num);
    }
    return $rtn;
}
function shop_cart_find($shop_id){
    if(!isset($_SESSION['cart'])) $_SESSION['cart']=array();
    $flag=0;
    foreach($_SESSION['cart'] as $_cart=>$_cart_num)
        if($_cart==$shop_id){
            $flag=1;break;
        }
    return $flag==1;
}
function shop_cart_add_money(&$rtn,$money,$money_type,$num=1){
    $rtn[get_credits_name_by_type($money_type)] += $money  * $num;
}
function shop_cart_count_num(){
    if(!isset($_SESSION['cart'])) return 0;
    if(!is_array($_SESSION['cart'])) return 0;
    $count = 0;
    foreach($_SESSION['cart'] as $k=>$v)
        $count+=$v;
    return $count;
}
function shop_cart_truncate(){
    if(!isset($_SESSION['cart'])) return;
    unset($_SESSION['cart']);
    $_SESSION['cart']=array();
}
function shop_cart_reset(){
    if(!isset($_SESSION['cart'])) return;
    foreach($_SESSION['cart'] as &$_item)
        if($_item<=0) unset($_item);
}
function shop_cart_fmt(){
    $cart_list = array();
    if(!isset($_SESSION['cart'])) return $cart_list;
    foreach($_SESSION['cart'] as $k=>$v){
        $r_item = db_find_one('shop_list',array('shop_id'=>$k));
        if(!isset($r_item['piece'])) continue;
        //if($v >= $r_item['piece']) continue;
        $cart_list[$k]['num']=$v;
        $cart_list[$k]['item'] = $r_item;
    }
    return $cart_list;
}
function shop_cart_price_fmt($money_arr,$add_icon=0,$show_char=1,$split1='&nbsp;',$split2='<br>'){
    $rtn = '';
    $money_type_arr = array('credits'=>1,'golds'=>2,'rmbs'=>3);$flag=1;
    foreach($money_arr as $credits_name => $credits){
        if($credits<=0) continue;
        if($flag) $flag=0; else $rtn.=$split2;
        if($add_icon) $rtn.=credits_get_icon_html($money_type_arr[$credits_name]).$split1;
        if($show_char) $rtn .= credits_get_text($money_type_arr[$credits_name],$credits);
        else $rtn .= $money_type_arr[$credits_name]==3 ? $credits/100.0 : $credits;
    }
    return $rtn;
}
function shop_cart_price_fmt2($cart,$add_icon=0,$show_char=1,$split='&nbsp;',$count_all=0){
    if($count_all) {
        isset($cart['item']['money']) AND $cart['item']['money'] *= $cart['num'];
        isset($cart['item']['money2']) AND $cart['item']['money2'] *= $cart['num'];
    }
    $shop_item = $cart['item'];
    $open_2_money = $shop_item['money_type2']!='0';
    $rtn = '';
    if($add_icon) $rtn.=credits_get_icon_html($shop_item['money_type']).'&nbsp;';
    if($show_char) $rtn .= credits_get_text($shop_item['money_type'],$shop_item['money']);
    else $rtn .= $shop_item['money_type']=='3'?$shop_item['money']/100.0:$shop_item['money'];
    if($open_2_money){
        $rtn.=$split;
        if($add_icon) $rtn.=credits_get_icon_html($shop_item['money_type2']).'&nbsp;';
        if($show_char) $rtn .= credits_get_text($shop_item['money_type2'],$shop_item['money2']);
        else $rtn .= $shop_item['money_type2']=='3'?$shop_item['money2']/100.0:$shop_item['money2'];
    }
    return $rtn ;
}
function shop_my_order_count($uid){
    return db_count('shop_order',array('uid'=>$uid));
}
function shop_my_seller_count($uid){
    return db_count('shop_order',array('uid_seller'=>$uid));
}
function shop_my_goods_count($uid){
    return db_count('shop_list',array('uid'=>$uid));
}
function shop_template_get($template_id){
    $rtn_arr = array('realname'=>'姓名','qq'=>'QQ','mobile'=>'联系电话','address'=>'收货地址','postcode'=>'邮政编码');
    if($template_id!=-1){
        $template = db_find_one('shop_template',array('template_id'=>$template_id));
        if($template) foreach($rtn_arr as $k=>$v) $rtn_arr[$k] = $template[$k];
    }
    return $rtn_arr;
}
function shop_template_list(){
    return db_find('shop_template');
}
?>