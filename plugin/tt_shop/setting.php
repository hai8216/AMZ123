<?php
!defined('DEBUG') AND exit('Access Denied.');
$action = param(3);
if(empty($action)){
    $shop_cate_list = shop_cate_list_get();
    $maxid = db_maxid('shop_cate', 'cate_id');
    if($method == 'GET')
        include _include(APP_PATH.'plugin/tt_shop/setting.htm');
    elseif($method=='POST'){
        $tablepre = $db->tablepre;
        $update_lists2=param('seller',array(0));
        $sql = 'UPDATE '.$tablepre.'group SET allowsell="0";';
        foreach($update_lists2 as $k => $v)
            $sql .= 'UPDATE '.$tablepre.'group SET allowsell="1" WHERE gid="'.$k.'";';
        db_exec($sql);
        $update_lists2=param('card',array(0));
        $sql = 'UPDATE '.$tablepre.'group SET allowcard="0";';
        foreach($update_lists2 as $k => $v)
            $sql .= 'UPDATE '.$tablepre.'group SET allowcard="1" WHERE gid="'.$k.'";';
        db_exec($sql);
        group_list_cache_delete();
        setting_set('tt_shop',array('show_seller'=>param('show_seller','1'),'show_thread'=>param('show_seller','1'),'edit_sync'=>param('edit_sync','1')));

        $rowidarr = param('cate_id', array(0));
        $namearr = param('name', array(''));
        $iconarr = param('icon', array(''));
        $rankarr = param('rank', array(''));
        foreach ($rowidarr as $k => $v) {
            if (empty($namearr[$k]) && empty($iconarr[$k]) && empty($rankarr[$k])) continue;
            $arr = array(
                'cate_id' => $k,
                'name' => $namearr[$k],
                'icon' => $iconarr[$k],
                'rank' => $rankarr[$k],
            );
            if (!isset($shop_cate_list[$k])) shop_cate_create($arr);
            else db_update('shop_cate', array('cate_id' => $k), $arr);
        }

        $deletearr = array_diff_key($shop_cate_list, $rowidarr);
        foreach ($deletearr as $k => $v)
            shop_cate_delete($k);
        shop_cate_db_count();

        message(0,'设置成功！');
    }
}
?>