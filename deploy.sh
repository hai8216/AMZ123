docker buildx build --no-cache=false --platform linux/amd64 -t docker.zfty.work/amz123/amz123-web:v0.1.0 -f Dockerfile .

# 推送内网
docker push docker.zfty.work/amz123/amz123-web:v0.1.0

# 推送腾讯云
docker tag docker.zfty.work/amz123/amz123-web:v0.1.0 ccr.ccs.tencentyun.com/amz123/amz123-web-php:v0.1.4

docker push ccr.ccs.tencentyun.com/amz123/amz123-web:v0.1.0
docker push ccr.ccs.tencentyun.com/amz123/amz123-web:v0.1.0
