<?php
return array(
    'db' =>
        array(
            'type' => 'pdo_mysql',
            'mysql' =>
                array(
                    'master' =>
                        array(
                            'host' => '172.16.0.3',
                            'user' => 'root',
                            'password' => 'lPTFG9jwa@7xClkR8hX!XP',
                            'name' => 'amz123_com',
                            'tablepre' => 'bbs_',
                            'charset' => 'utf8mb4_unicode_ci',
                            'engine' => 'myisam',
                        ),
                    'slaves' =>
                        array(),
                ),
            'pdo_mysql' =>
                array(
                    'master' =>
                        array(
                            'host' => getenv('MYSQL_HOST') ?: '172.16.0.3',
                            'user' => getenv('MYSQL_USER') ?: 'root',
                            'password' => getenv('MYSQL_PASSWORD') ?: 'lPTFG9jwa@7xClkR8hX!XP',
                            'name' => getenv('MYSQL_DB') ?: 'amz123_com',
                            'tablepre' => 'bbs_',
                            'charset' => 'utf8mb4_unicode_ci',
                            'engine' => 'myisam',
                        ),
                    'slaves' =>
                        array(),
                ),
        ),
    'cache' =>
        array(
            'enable' => true,
            'type' => 'redis',
            'memcached' =>
                array(
                    'host' => '127.0.0.1',
                    'port' => '11211',
                    'cachepre' => 'bbs11_',
                ),
            'redis' =>
                array(
                    'host' => getenv('REDIS_HOST') ?: '172.16.0.7',
                    'port' => getenv('REDIS_PORT') ?: '6379',
                    'cachepre' => 'bbs_',
                    'passwd' => getenv('REDIS_PASSWORD') ?: ''
                ),
            'xcache' =>
                array(
                    'cachepre' => 'bbs_',
                ),
            'yac' =>
                array(
                    'cachepre' => 'bbs_',
                ),
            'apc' =>
                array(
                    'cachepre' => 'bbs_',
                ),
            'mysql' =>
                array(
                    'cachepre' => 'bbs_',
                ),
        ),
    'tmp_path' => './tmp/',
    'log_path' => './log/',
    'view_url' => 'view/',
    'upload_url' => 'upload/',
    'upload_path' => './upload/',
    'logo_mobile_url' => 'view/img/logo.png',
    'logo_pc_url' => 'view/img/logo.png',
    'logo_water_url' => 'view/img/water-small.png',
    'sitename' => 'AMZ123亚马逊导航-跨境电商出海门户',
    'sitebrief' => 'AMZ123亚马逊导航是一家致力于服务中国跨境电商从业者的综合平台，以让跨境电商出海更便捷为使命，始终围绕卖家需求，为卖家提供实时的跨境资讯，实用的跨境干货、工具、数据和服务，打造一站式跨境流量入口。做跨境电商，就上AMZ123。',
    'timezone' => 'Asia/Shanghai',
    'lang' => 'zh-cn',
    'runlevel' => 5,
    'runlevel_reason' => 'The site is under maintenance, please visit later.',
    'cookie_domain' => '',
    'cookie_path' => '',
    'auth_key' => 'CUMPTPXXJX7DBKD4YWR2VU86DFRZNGSUGQ2Y7QE2DV34BE48ZJ7UZUVNCK75SV3Z',
    'pagesize' => 20,
    'postlist_pagesize' => 100,
    'cache_thread_list_pages' => 10,
    'online_update_span' => 120,
    'online_hold_time' => 86400,
    'session_delay_update' => 0,
    'upload_image_width' => 927,
    'order_default' => 'lastpid',
    'attach_dir_save_rule' => 'Ym',
    'update_views_on' => 1,
    'user_create_email_on' => 0,
    'user_create_on' => 1,
    'user_resetpw_on' => 1,
    'admin_bind_ip' => 0,
    'cdn_on' => 0,
    'url_rewrite_on' => 1,
    'disabled_plugin' => 0,
    'version' => '4.0.4',
    'static_version' => '?2.0',
    'installed' => 1,
    'reward' => 0,
    'reward_title' => '',
    'reward_button_title' => '',
    'reward_from_title' => '',
    'reward_info' => '',
    'hideqrcode' => '345528,314076,330177,332626,340373,347638,285508,361624,377811,402296,276681,281040,23081,401067,387957, 401473, 18005, 338817, 403417, 281707, 281738, 281737, 281740, 9492,343922, 281057, 403429, 401710, 276940, 314696, 281059, 401717, 402520, 281953, 285508, 22581, 276933, 297777, 313549, 323516, 277998, 347638, 340644, 281906, 281919,281923,281924,281944, 
281946, 10307, 48110, 332742, 283528, 322472, 401497, 302255, 333013, 333629, 281751, 23084, 341606, 362410, 328081, 282429,402511, 402538, 402550, 281742, 402612, 402559, 381243, 63336, 336412, 281112, 281113, 401747, 281276,281278, 281456, 281475, 281482, 401769, 303635, 315463 ,313207, 311273, 307558, 344300, 342955, 346111, 322454, 320342, 316922, 318991,298912, 283786, 283787, 333953, 65148, 63678, 281090, 281091, 281102, 281103, 281105, 281108,401772, 281764, 276980,
276986, 281770, 281771, 281772, 402733, 281781, 402676, 402719,281786, 281899, 281901, 282082, 282087, 282094, 282095,282096, 276704,282119, 282130, 282135, 282328, 282329, 282330, 282331, 282332, 282334, 282347, 346733, 282126, 23232, 401780, 282135, 276668, 281486, 281487, 281526, 48168,8910, 28231, 281518, 281489, 281488, 281521,16682, 281528, 63413, 281516, 331996, 282381, 282384 ,405775, 282409, 405771, 282352, 282385, 307355, 282392, 282395, 282406, 282407, 48844,323928, 279438, 277789, 272467, 272461, 320124, 402049, 282120, 282122, 16283, 282124, 402747, 281930, 281932, 402054, 
281938, 281942, 62567, 275278, 333666, 281546, 281538, 23336, 297592, 13741, 273935, 388157, 277197, 260383, 361889, 377004, 
402926, 285802, 402155, 35602, 285053, 333666, 62567, 317404, 261738, 282129, 402166,15413, 361549, 29559,402916,319502, 376915, 347071, 350977, 336828, 275278, 23115,319789 
',
    'hidemore' => '314076',
    'special_bg' => 'https://www.ikjzd.com/static/website/images/shop/banner.png',
    'special_links' => 'http://www.amz123.com/',
    'pay_type' => 1,
    'alipay_app_id' => '2021001146612549',
    'alipay_private_key' => 'MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCV9MAc7i1GaQ9fc4zy9UXepiUyBsE5iSlQ8XdBBJo6vgp3dk00jfOJ6MPChVwCNCCy3V6Ah+d8tbOvL8RiQ/xx5k2DO7ntEVpL4tTjsr2cl8lnCdeD+UwoGLajEUm/kFjwFK2iOJUNqtJ9yK49gNTk31vyfwV5U1RyS6+8hjHAP1wBhlV6HYxnC3GPMbrLpUrFTI9CUWcq6jO88PZ7x3LoDXtWLG6L9rRQbB7m7NCZwI+FmR3Jd5RVIgxG5N1+iDmJOxXIj08rXm+fOl0wf8+m1i3yKW5z+J/E2UNXZqSC04l6n7vtplFdf0e6mSWgQHOnBptXWxqi0Dz2hHW4lKUZAgMBAAECggEBAIIH+rLbgZ0FqxPtAfMH5q9byPMpUlJO8unCiNNzZz263x6aEfaZhO44TdAWjiNHCnevZ6vyrTPB5+7ob9V5+KTXvK0olF/S5UbaihpsOulX4Ygcy6of+f7m9zBN5HJdfBGrDKmXgQCZ+O6jnxur5HAWkwejR57eOGLn6JqEjw2RSJ6bGXBQHthNHFLO8r2CqhOJ6y4m10V5zti8AA+pnPlIBJq7BhhgemAO6b/mQRNzZOrrHzD4/LtH78dv3mLb9HRVy8ocbFgRtX+8dXTd7b+IBrre9aDnl/K2lMlEilZEe+RqV/VGRNJcg7nZ07CKUih7ID8mpmu8S/qg130y3ZECgYEA8CQUmWbH+sm93559057Q2P18ZLia42DUScGvcJfnlufeEvOUuUT278IU5mhFyJAsS+36l0VX92FRZ8cJBHOEY2QN5YTl5jqWjJSa5neye/iV8rhHJ+PB4lqyZ8edworr4N5saU5W9cnA4noEYNcIMrhPtXCgHoR6wfb+bQRZWd0CgYEAn9v3zfribm9eTEG6c1DKFd1mX6FpjKPJMl/u4ULnSOq3MwAkpO6tMUaK9ohbDM4FFEJwWxlWd/sUAZe/v39zuCkMqepQUU1MiRRs5/qIlOzfUdTUlV6yWu47osMVTrNA4iLcE+U+vmmvmf8ki0t6lAzlGrtOHuOMXO8PPlCVym0CgYEAh2zhjQ0wlBb3lEeOUCyNPQHfB/IvrzEpJxsqdEXGXg0O6QMjCUSCOf3qyXFegVgFEyD3JB4AGMtyHCNby33V2DNRvvEusaKyyj74CCbSqiUrcwFHZ5CDA8MAP5SMPG0ZzuvAmbaLRftsYoxLMc2ymt/kW79THUK68+3118cPGKUCgYBJ3BA6k1hbPgmLojLLZOB6CblrJeDmoiYrqDduzMKf5hKO9dn4em56/eTm00l+ORIPskLHVIWnGdK8iaiGihSlWhIHz8McTWK3U0CeThsc8/SakoNGIGK3sKrNIOWPXzLzSLf2WU0MHTiNJl10HoOcM/kGyNPnYatQpQOd/w22YQKBgQCut70BmNCDHxwgM38RN70O/pfv+TUVZhhrtmX4KkWFLRvA9njsXQ9oeLSVz8/7Z6w/l9AmYDE6AHO+e3VL3vXVNY1eWwL3D67iDJVD5afAv4hISv2/9l/ztQlr/pjBkZviS6X4y0JLwjtGpXK1D+tTQZbZ7AUXfrCmJgCrbh9RMg==',
    'alipay_public_key' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlfTAHO4tRmkPX3OM8vVF3qYlMgbBOYkpUPF3QQSaOr4Kd3ZNNI3ziejDwoVcAjQgst1egIfnfLWzry/EYkP8ceZNgzu57RFaS+LU47K9nJfJZwnXg/lMKBi2oxFJv5BY8BStojiVDarSfciuPYDU5N9b8n8FeVNUckuvvIYxwD9cAYZVeh2MZwtxjzG6y6VKxUyPQlFnKuozvPD2e8dy6A17Vixui/a0UGwe5uzQmcCPhZkdyXeUVSIMRuTdfog5iTsVyI9PK15vnzpdMH/PptYt8iluc/ifxNlDV2akgtOJep+77aZRXX9HupkloEBzpwabV1saotA89oR1uJSlGQIDAQAB',
    'wechat_app_id' => 'wx46da02edf61636f9',
    'wechat_app_secret' => '971b645770a1f6e5ca69bba1608f1375',
    'wechat_business_id' => '1560266451',
    'wechat_business_secret' => '571J9B0ahNlCidsoKvmqqWIA9hqikoD5',
    'mpayid' => '',
    'mpaykey' => '',
    'pay' => '1',
    'exchange' => 0,
    'disexchange' => 0,
    'exchange_bl' => '1',
    'pay_out_info' => '每天统一提现一次。',
    'payout' => 0,
    'alipay' => 1,
    'qqpay' => 0,
    'wechar' => 1,
    'payout_min' => '0',
    'payout_service' => '',
    'topusa' => '2020.12.25',
    'usatopkeywords' => '2020.12.29',
    'topjp' => '2020.12.28',
    'topuk' => '2020.12.30',
    'topfr' => '2020.12.29',
    'topde' => '2020.12.29',
    'JWTSecret' => '48AE5849FDD4A925EBCC5F0AFD072402',
    'user_center' => '172.16.0.46:18440/v1'
);
?>
