$(function () {
    $('.tabs-item strong').click(function () {
        let className = $(this).attr('data-id')
        $('.tabs-item strong.current').removeClass('current')
        $(this).addClass('current')
        $('.tabdiv').hide();
        $('.' + className).css('display', 'flex')
    })
})
