﻿
// 【MT】 ====== idx banner ======

var topBannerAd = new Class({
	
	options : {
		closePic : 'images/close.gif' ,
		width : 980 ,
		height : 100
	} ,
	
	initialize : function(href , pic , title , options) {
		//if(jcookie.get('idxTopBanner') == 'close')
		//	return false ;
		this.setOptions(options) ;
		this.href = href ;
		this.pic = pic ;
		this.title = title ;
		var ele = new Element('div' , {'id':'topBannerBao'}).setHTML('<div id="topBanner"><a target="_blank" href="' + this.href + '"><img width="' + this.options.width + '" height="' + this.options.height + '"  src="' + this.pic + '" title="' + this.title + '" /></a><span style="background:url(' + this.options.closePic + ')"></span></div>').injectTop(document.body) ;
		this.myslide = new Fx.Slide($('topBanner')).hide();
		$('topBanner').getElementsByTagName('span')[0].onclick = this.display.bind(this , 0) ;	
		// load img
		new Image().src = pic ;
	} ,
	
	display : function(type) {
		if(type) {
			if($('topBannerBao'))
				this.myslide.slideIn() ;
		}
		else {
		//	jcookie.set('idxTopBanner' , 'close') ;
			this.myslide.slideOut() ;	
		}
	}
}) ;


topBannerAd.implement(new Events, new Options);


